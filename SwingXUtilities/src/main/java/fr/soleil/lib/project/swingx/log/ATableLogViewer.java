/*
 * This file is part of SwingXUtilities.
 * 
 * SwingXUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingXUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingXUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swingx.log;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.Collator;

import javax.swing.ImageIcon;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.RowSorter;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.table.TableModel;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.log.LogData;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;
import fr.soleil.lib.project.swing.ScrollMagnetism;
import fr.soleil.lib.project.swing.icons.Icons;
import fr.soleil.lib.project.swing.model.LogTableModel;
import fr.soleil.lib.project.swing.panel.ALogViewer;
import fr.soleil.lib.project.swing.renderer.LogTableCellRenderer;

/**
 * An {@link ALogViewer} that uses a {@link LogTable} to display logs.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class ATableLogViewer extends ALogViewer<LogTable> implements MouseListener, RowSorterListener {

    private static final long serialVersionUID = 1L;

    protected static final String LABEL_SPACE = "    ";

    // alternate color
    protected JPanel alternateColorPanel;
    protected JLabel alternateColorTitleLabel;
    protected JLabel alternateColorLabel;
    protected ConstrainedCheckBox alternateColorCheckBox;

    // row space
    protected JPanel rowSpacePanel;
    protected JLabel rowSpaceTitleLabel;
    protected JSpinner rowSpaceSpinner;

    // RowSorterListener
    protected RowSorterListener rowSorterListener;

    /**
     * Creates a new {@link ATableLogViewer}.
     */
    public ATableLogViewer() {
        super();
    }

    /**
     * Creates a new {@link ATableLogViewer} with given {@link MessageManager} and {@link Icons}.
     * 
     * @param messageManager The {@link MessageManager} that knows the messages to display.
     * @param icons The {@link Icons} that knows the {@link ImageIcon}s to display.
     */
    public ATableLogViewer(MessageManager messageManager, Icons icons) {
        super(messageManager, icons);
    }

    /**
     * Returns the cell vertical margin used by the {@link LogTable}.
     * 
     * @return An <code>int</code>.
     */
    public int getTableCellVerticalMargin() {
        return logComponent.getCellVerticalMargin();
    }

    /**
     * Sets the cell vertical margin the {@link LogTable} should use.
     * 
     * @param verticalMargin The cell vertical margin the {@link LogTable} should use.
     *            <p>
     *            Use a value &lt; 0 to reset to default value.
     *            </p>
     */
    public void setTableCellVerticalMargin(int verticalMargin) {
        logComponent.setCellVerticalMargin(verticalMargin);
    }

    /**
     * Adds the alternate color panel in the options panel.
     */
    protected void addAlternateColorPanel() {
        optionsPanel.add(alternateColorPanel, generateOptionsConstraints());
    }

    /**
     * Adds the row space panel in the options panel.
     */
    protected void addRowSpacePanel() {
        optionsPanel.add(rowSpacePanel, generateOptionsConstraints());
    }

    /**
     * Return the alternate color used every second grid line.
     * 
     * @return A {@link Color}.
     */
    public Color getAlternateColor() {
        return logComponent.getAlternateColor();
    }

    /**
     * Sets the alternate color to use every second grid line.
     * 
     * @param color The alternate color to use every second grid line.
     */
    public void setAlternateColor(Color color) {
        Color alternate = color;
        if (alternate == null) {
            alternate = LogTableCellRenderer.DEFAULT_DARKER_COLOR;
        }
        alternateColorLabel.setBackground(alternate);
        logComponent.setAlternateColor(alternate);
    }

    /**
     * Returns whether associated table will automatically resize its columns to fit displayable area.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isAutoResizeColumns() {
        return logComponent.isAutoResizeColumns();
    }

    /**
     * Sets whether associated table should automatically resize its columns to fit displayable area.
     * 
     * @param autoResize Whether associated table should automatically resize its columns to fit displayable area.
     */
    public void setAutoResizeColumns(boolean autoResize) {
        logComponent.setAutoResizeColumns(autoResize);
    }

    protected void listenToRowSorter(RowSorter<?> sorter) {
        if (rowSorterListener == null) {
            rowSorterListener = (e) -> {
                // JAVAAPI-634: deactivate auto scroll magnetism when sort changed
                setAutoScrollMagnetism(false);
            };
        }
        if (sorter != null) {
            sorter.addRowSorterListener(rowSorterListener);
        }
    }

    @Override
    protected void buildComponents() {
        super.buildComponents();
        alternateColorTitleLabel = generateTitleLabel("fr.soleil.lib.project.swing.table.color.alternate");
        alternateColorLabel = new JLabel(LABEL_SPACE);
        alternateColorLabel
                .setToolTipText(messageManager.getMessage("fr.soleil.lib.project.swing.table.color.alternate.click"));
        alternateColorLabel.setBackground(logComponent.getAlternateColor());
        alternateColorLabel.setOpaque(true);
        alternateColorLabel.setBorder(new LineBorder(Color.WHITE, 3));
        alternateColorLabel.addMouseListener(this);
        alternateColorCheckBox = generateCheckBox("fr.soleil.lib.project.swing.table.color.alternate.use",
                logComponent.isUseAlternateColor());
        alternateColorPanel = generatePanel(alternateColorTitleLabel, alternateColorLabel, alternateColorCheckBox);

        rowSpaceTitleLabel = generateTitleLabel("fr.soleil.lib.project.swing.table.row.space");
        rowSpaceSpinner = generateNumberSpinner(logComponent.getCellVerticalMargin(), 0, Integer.MAX_VALUE);
        rowSpacePanel = generatePanel(rowSpaceTitleLabel, rowSpaceSpinner, null);
    }

    @Override
    protected void copyLogs() {
        boolean clearSelection = false;
        if (logComponent.getSelectedRow() < 0) {
            int rowCount = logComponent.getRowCount();
            if (rowCount > 0) {
                logComponent.setRowSelectionInterval(0, rowCount - 1);
                clearSelection = true;
            }
            int columnCount = logComponent.getColumnCount();
            if (columnCount > 0) {
                logComponent.setColumnSelectionInterval(0, columnCount - 1);
                clearSelection = true;
            }
        }
        super.copyLogs();
        if (clearSelection) {
            logComponent.clearSelection();
        }
    }

    @Override
    protected ScrollMagnetism[] getScrollMagnetismChoices() {
        return new ScrollMagnetism[] { ScrollMagnetism.NONE, ScrollMagnetism.BOTTOM, ScrollMagnetism.LEFT,
                ScrollMagnetism.RIGHT, ScrollMagnetism.BOTTOM_LEFT, ScrollMagnetism.BOTTOM_RIGHT };
    }

    @Override
    protected LogTable generateLogComponent() {
        LogTable logTable = new LogTable(new LogTableModel(messageManager));
        RowSorter<? extends TableModel> sorter = logTable.getRowSorter();
        listenToRowSorter(sorter);
        logTable.addPropertyChangeListener("rowSorter", e -> {
            if (!ObjectUtils.sameObject(e.getOldValue(), e.getNewValue())) {
                Object newValue = e.getNewValue();
                if (newValue instanceof RowSorter<?>) {
                    listenToRowSorter((RowSorter<?>) newValue);
                }
            }
        });
        return logTable;
    }

    @Override
    protected void layoutOptionsPanel() {
        super.layoutOptionsPanel();
        addAlternateColorPanel();
        addRowSpacePanel();
    }

    @Override
    protected int computeColumnHeaderMargin() {
        return logComponent.getRowHeight() + 2;
    }

    @Override
    protected int computeRowHeaderMargin() {
        return 0;
    }

    @Override
    protected ScrollMagnetism computeBestScrollMagnetism(LogData... logs) {
        // JAVAAPI-634: compute best scroll magnetism
        ScrollMagnetism magnetism = null;
        if ((logs != null) && (logs.length > 0)) {
            LogData firstLog = null, lastLog = null;
            for (LogData log : logs) {
                if (log != null) {
                    firstLog = log;
                    break;
                }
            }
            for (int i = logs.length - 1; i > -1 && (lastLog == null); i--) {
                LogData log = logs[i];
                if (log != null) {
                    lastLog = log;
                }
            }
            if ((firstLog != null) && (lastLog != null) && (firstLog != lastLog)) {
                if (Collator.getInstance().compare(firstLog.getTimestamp(), lastLog.getTimestamp()) > 0) {
                    magnetism = ScrollMagnetism.TOP;
                } else {
                    magnetism = ScrollMagnetism.BOTTOM;
                }
            }
        }
        return magnetism;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if ((e != null) && (e.getSource() == alternateColorCheckBox)) {
            logComponent.setUseAlternateColor(alternateColorCheckBox.isSelected());
        } else {
            super.itemStateChanged(e);
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if ((e != null) && (e.getSource() == rowSpaceSpinner)) {
            logComponent.setCellVerticalMargin(((Number) rowSpaceSpinner.getValue()).intValue());
        } else {
            super.stateChanged(e);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if ((e != null) && (e.getComponent() == alternateColorLabel)) {
            Color alternate = JColorChooser.showDialog(alternateColorLabel,
                    messageManager.getMessage("fr.soleil.lib.project.swing.table.color.alternate.choose"),
                    logComponent.getAlternateColor());
            if (alternate != null) {
                setAlternateColor(alternate);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // not managed
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // not managed
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // not managed
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // not managed
    }

    @Override
    public void sorterChanged(RowSorterEvent e) {
        if ((e != null) && (e.getSource() == logComponent.getRowSorter())) {
            // auto scroll magnetism becomes invalid when sorted column changed (JAVAAPI-634).
            setAutoScrollMagnetism(false);
        }
    }

}
