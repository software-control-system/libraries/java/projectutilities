/*
 * This file is part of SwingXUtilities.
 * 
 * SwingXUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingXUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingXUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swingx;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import org.jdesktop.swingx.action.AbstractActionExt;

import fr.soleil.lib.project.swing.icons.Icons;

/**
 * A {@link JComponent} that looks like a combo box, and is based on {@link Action}s.
 * The {@link QuickClickComboBox} offers the possibility to redo last {@link Action} by clicking on the corresponding
 * button.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class QuickClickComboBox extends JComponent
        implements ActionListener, ItemListener, PopupMenuListener, MouseListener, PropertyChangeListener {

    private static final long serialVersionUID = -3511848012188822536L;

    protected static final Color FOCUS_GRAY = new Color(150, 150, 150);
    protected static final ImageIcon ARROW_ICON = new Icons(QuickClickComboBox.class.getPackage().getName() + ".icons")
            .getIcon("Button.Down");
    protected static final String EMPTY_TEXT = " ";
    protected static final int GAP = 1;
    protected static final Insets DEFAULT_MARGIN = new Insets(4, 4, 4, 4);
    protected static final Insets NO_MARGIN = new Insets(0, 0, 0, 0);

    protected final JPopupMenu popupMenu;
    protected final JToggleButton actionListButton;
    protected final JToggleButton lastActionButton;
    private Insets margin;

    public QuickClickComboBox() {
        super();
        setLayout(new GridBagLayout());
        popupMenu = new JPopupMenu();
        actionListButton = new JToggleButton(ARROW_ICON);
        actionListButton.setFocusPainted(false);
        actionListButton.setBorder(new EmptyBorder(NO_MARGIN));
        actionListButton.setMargin(NO_MARGIN);
        actionListButton.addItemListener(this);
        popupMenu.addPopupMenuListener(this);
        lastActionButton = new JToggleButton(EMPTY_TEXT);
        lastActionButton.setFocusPainted(false);
        lastActionButton.setBorder(new EmptyBorder(NO_MARGIN));
        lastActionButton.setMargin(NO_MARGIN);
        lastActionButton.addMouseListener(this);
        lastActionButton.addActionListener(this);

        GridBagConstraints lastActionButtonConstraints = new GridBagConstraints();
        lastActionButtonConstraints.fill = GridBagConstraints.BOTH;
        lastActionButtonConstraints.gridx = 0;
        lastActionButtonConstraints.gridy = 0;
        lastActionButtonConstraints.weightx = 1;
        lastActionButtonConstraints.weighty = 1;
        lastActionButtonConstraints.insets = new Insets(0, 0, 0, 0);
        add(lastActionButton, lastActionButtonConstraints);
        GridBagConstraints actionListButtonConstraints = new GridBagConstraints();
        actionListButtonConstraints.fill = GridBagConstraints.BOTH;
        actionListButtonConstraints.gridx = 1;
        actionListButtonConstraints.gridy = 0;
        actionListButtonConstraints.weightx = 0;
        actionListButtonConstraints.weighty = 1;
        actionListButtonConstraints.insets = new Insets(0, GAP, 0, 0);
        add(actionListButton, actionListButtonConstraints);

        setBorder(new LineBorder(Color.GRAY));
        setMargin(DEFAULT_MARGIN);
    }

    /**
     * Adds a {@link JMenuItem} at specified index
     * 
     * @param item The {@link JMenuItem}
     * @param index The index
     */
    public void addItem(JMenuItem item, int index) {
        addItem(item, index, true);
    }

    /**
     * Adds a {@link JMenuItem} at specified index
     * 
     * @param item The {@link JMenuItem}
     * @param index The index
     * @param pack Whether to pack popup menu once done
     */
    protected void addItem(JMenuItem item, int index, boolean pack) {
        if (item != null) {
            item.addActionListener(this);
            if ((index > -1) && (index < popupMenu.getComponentCount())) {
                popupMenu.insert(item, index);
            } else {
                popupMenu.add(item);
            }
            if (item.getAction() != null) {
                item.getAction().addPropertyChangeListener(this);
            }
            if (((lastActionButton.getAction() == null) && (item.getAction() != null))
                    || ((item.getAction() instanceof AbstractActionExt)
                            && ((AbstractActionExt) item.getAction()).isSelected()
                            && (((AbstractActionExt) item.getAction()).getGroup() != null))) {
                updateLastAction(item);
            }
        }
        if (pack) {
            popupMenu.pack();
        }
    }

    /**
     * Adds some {@link JMenuItem}s in the combo box
     * 
     * @param items the {@link JMenuItem}s to add
     */
    public void addItems(JMenuItem... items) {
        if (items != null) {
            for (JMenuItem item : items) {
                addItem(item, -1, false);
            }
        }
        popupMenu.pack();
    }

    /**
     * Removes some {@link JMenuItem}s from this combo box
     * 
     * @param items The {@link JMenuItem}s to remove
     */
    public void removeItems(JMenuItem... items) {
        if (items != null) {
            for (JMenuItem item : items) {
                if (item != null) {
                    if (item.getAction() != null) {
                        item.getAction().removePropertyChangeListener(this);
                        if (item.getAction() == lastActionButton.getAction()) {
                            updateLastAction(null);
                        }
                    }
                    popupMenu.remove(item);
                }
            }
            if (lastActionButton.getAction() == null) {
                for (Component comp : popupMenu.getComponents()) {
                    if (comp instanceof JMenuItem) {
                        JMenuItem item = (JMenuItem) comp;
                        if (item.getAction() != null) {
                            updateLastAction(item);
                            break;
                        }
                    }
                }
            }
        }
        popupMenu.pack();
        lastActionButton.revalidate();
        revalidate();
        repaint();
    }

    /**
     * Adds an {@link Action} at specified index
     * 
     * @param action The {@link Action}
     * @param index The index
     */
    public void addAction(Action action, int index) {
        addAction(action, index, true);
    }

    /**
     * Adds an {@link Action} at specified index
     * 
     * @param action The {@link Action}
     * @param index The index
     * @param pack Whether to pack popup menu once done
     */
    protected void addAction(Action action, int index, boolean pack) {
        if (action != null) {
            boolean indexOk = (index > -1) && (index < popupMenu.getComponentCount());
            JMenuItem item = popupMenu.add(action);
            item.addActionListener(this);
            action.addPropertyChangeListener(this);
            if ((lastActionButton.getAction() == null)
                    || ((action instanceof AbstractActionExt) && ((AbstractActionExt) action).isSelected())) {
                updateLastAction(item);
            }
            if (indexOk) {
                popupMenu.remove(item);
                popupMenu.insert(item, index);
            }
        }
        if (pack) {
            popupMenu.pack();
        }
    }

    /**
     * Adds some {@link Action}s in the combo box
     * 
     * @param actions the {@link Action}s to add
     */
    public void addActions(Action... actions) {
        if (actions != null) {
            for (Action action : actions) {
                addAction(action, -1, false);
            }
        }
        popupMenu.pack();
    }

    /**
     * Removes some {@link Action}s from this combo box
     * 
     * @param actions The {@link Action}s to remove
     */
    public void removeActions(Action... actions) {
        if (actions != null) {
            for (Action action : actions) {
                if (action != null) {
                    action.removePropertyChangeListener(this);
                    if (action == lastActionButton.getAction()) {
                        updateLastAction(null);
                    }
                    List<JMenuItem> toRemove = new ArrayList<JMenuItem>();
                    for (Component comp : popupMenu.getComponents()) {
                        if (comp instanceof JMenuItem) {
                            JMenuItem item = (JMenuItem) comp;
                            if (item.getAction() == action) {
                                toRemove.add(item);
                            }
                        }
                    }
                    for (JMenuItem item : toRemove) {
                        popupMenu.remove(item);
                        item.removeActionListener(this);
                    }
                    toRemove.clear();
                }
            }
            if (lastActionButton.getAction() == null) {
                for (Component comp : popupMenu.getComponents()) {
                    if (comp instanceof JMenuItem) {
                        JMenuItem item = (JMenuItem) comp;
                        if (item.getAction() != null) {
                            updateLastAction(item);
                            break;
                        }
                    }
                }
            }
        }
        popupMenu.pack();
        lastActionButton.revalidate();
        revalidate();
        repaint();
    }

    /**
     * Updates last action button so that it looks and acts like selected {@link JMenuItem}
     * 
     * @param item The selected {@link JMenuItem}
     */
    protected void updateLastAction(JMenuItem item) {
        if (item == null) {
            lastActionButton.setAction(null);
            lastActionButton.setText(EMPTY_TEXT);
            lastActionButton.setToolTipText(null);
            lastActionButton.setIcon(null);
        } else {
            Action action = item.getAction();
            lastActionButton.setAction(action);
            if (action != null) {
                lastActionButton.setEnabled(action.isEnabled() && isEnabled());
            }
            lastActionButton.setText(item.getText());
            lastActionButton.setToolTipText(item.getToolTipText());
            lastActionButton.setIcon(item.getIcon());
            if (action instanceof AbstractActionExt) {
                lastActionButton.setSelected(((AbstractActionExt) action).isSelected());
            } else {
                lastActionButton.setSelected(false);
            }
        }
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (lastActionButton != null) {
            lastActionButton.setBackground(bg);
        }
        if (actionListButton != null) {
            actionListButton.setBackground(bg);
        }
        if (popupMenu != null) {
            popupMenu.setBackground(bg);
        }
    }

    @Override
    public void setOpaque(boolean isOpaque) {
        super.setOpaque(isOpaque);
        if (lastActionButton != null) {
            lastActionButton.setOpaque(isOpaque);
        }
        if (actionListButton != null) {
            actionListButton.setOpaque(isOpaque);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (lastActionButton != null) {
            lastActionButton.setEnabled(enabled);
        }
        if (actionListButton != null) {
            actionListButton.setEnabled(enabled);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e != null) {
            actionListButton.setSelected(false);
            if (e.getSource() instanceof JMenuItem) {
                JMenuItem item = (JMenuItem) e.getSource();
                updateLastAction(item);
            } else {
                lastActionButton.setSelected((lastActionButton.getAction() instanceof AbstractActionExt)
                        && ((AbstractActionExt) lastActionButton.getAction()).isSelected());
            }
            lastActionButton.revalidate();
            revalidate();
            repaint();
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if ((e != null) && (e.getSource() == actionListButton)) {
            if ((e.getStateChange() == ItemEvent.SELECTED) && (!popupMenu.isVisible())) {
                popupMenu.show(actionListButton, -lastActionButton.getWidth() - GAP - lastActionButton.getX(),
                        actionListButton.getHeight());
            }
        }
    }

    @Override
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        // nothing to do
    }

    @Override
    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        actionListButton.setSelected(false);
    }

    @Override
    public void popupMenuCanceled(PopupMenuEvent e) {
        actionListButton.setSelected(false);
    }

    public Insets getMargin() {
        return margin;
    }

    public void setMargin(Insets margin) {
        this.margin = (margin == null ? DEFAULT_MARGIN : margin);
        lastActionButton.setBorder(new EmptyBorder(this.margin));
        actionListButton.setBorder(new EmptyBorder(this.margin.top, Math.min(this.margin.left, 1), this.margin.bottom,
                Math.min(this.margin.right, 1)));
        lastActionButton.revalidate();
        actionListButton.revalidate();
        revalidate();
        repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // nothing to do
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        lastActionButton.setBorder(new CompoundBorder(new EmptyBorder(1, 1, 1, 1),
                new CompoundBorder(new LineBorder(FOCUS_GRAY),
                        new EmptyBorder(Math.max(this.margin.top - 2, 0), Math.max(this.margin.left - 2, 0),
                                Math.max(this.margin.bottom - 2, 0), Math.max(this.margin.right - 2, 0)))));
    }

    @Override
    public void mouseExited(MouseEvent e) {
        lastActionButton.setBorder(new EmptyBorder(this.margin));
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ((evt != null) && (evt.getSource() instanceof Action)) {
            Action action = (Action) evt.getSource();
            if (action == lastActionButton.getAction()) {
                lastActionButton.setEnabled(action.isEnabled() && isEnabled());
                if (action instanceof AbstractActionExt) {
                    lastActionButton.setSelected(((AbstractActionExt) action).isSelected());
                }
            } else if ((action instanceof AbstractActionExt) && ((AbstractActionExt) action).isSelected()) {
                for (Component comp : popupMenu.getComponents()) {
                    if (comp instanceof JMenuItem) {
                        JMenuItem item = (JMenuItem) comp;
                        if (item.getAction() == action) {
                            updateLastAction(item);
                            break;
                        }
                    }
                }
            }
        }
    }

}
