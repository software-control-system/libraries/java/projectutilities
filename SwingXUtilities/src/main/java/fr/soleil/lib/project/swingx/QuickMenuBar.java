/*
 * This file is part of SwingXUtilities.
 * 
 * SwingXUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingXUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingXUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swingx;

import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.action.AbstractActionExt;
import org.jdesktop.swingx.action.ActionContainerFactory;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.ComponentUtils;

/**
 * A {@link JMenuBar} that contains some useful methods to quickly interact with {@link JMenu}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class QuickMenuBar extends JMenuBar {

    private static final long serialVersionUID = 8751013520482496836L;

    private ActionContainerFactory fact;

    /**
     * Default constructor
     */
    public QuickMenuBar() {
        this(new ActionContainerFactory());
    }

    /**
     * Constructs a new {@link QuickMenuBar}, with an {@link ActionContainerFactory} that will help
     * creating some {@link JMenuItem}s
     * 
     * @param fact The {@link ActionContainerFactory}
     */
    public QuickMenuBar(ActionContainerFactory fact) {
        super();
        this.fact = fact;
        if (this.fact == null) {
            this.fact = new ActionContainerFactory();
        }
    }

    /**
     * Returns the {@link ActionContainerFactory} used by this {@link QuickMenuBar}
     * 
     * @return An {@link ActionContainerFactory}
     */
    public ActionContainerFactory getActionContainerFactory() {
        return fact;
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);
        applyFont(font, getComponents());
    }

    protected void applyFont(Font font, Component... comps) {
        if (comps != null) {
            for (Component comp : comps) {
                if (comp != null) {
                    comp.setFont(font);
                    if (comp instanceof JMenu) {
                        applyFont(font, ((JMenu) comp).getMenuComponents());
                    } else if (comp instanceof Container) {
                        Container cont = (Container) comp;
                        applyFont(font, cont.getComponents());
                    }
                }
            }
        }
    }

    /**
     * Sets the {@link Icon} of a {@link JMenu}, identified by a path
     * 
     * @param path The path, separated by <code>'/'</code> (As many menus/sub-menus as there are <code>'/'</code> in
     *            this {@link String}).
     * @param icon The {@link Icon} to set
     */
    public void setMenuIcon(String path, Icon icon) {
        JComponent menu = getMenu(path);
        if (menu instanceof JMenu) {
            ((JMenu) menu).setIcon(icon);
        }
    }

    /**
     * Sets the tooltip text of a {@link JMenu}, identified by a path
     * 
     * @param path The path, separated by <code>'/'</code> (As many menus/sub-menus as there are <code>'/'</code> in
     *            this {@link String}).
     * @param tooltip The tooltip text to set
     */
    public void setMenuToolTipText(String path, String tooltip) {
        JComponent menu = getMenu(path);
        if (menu != null) {
            menu.setToolTipText(tooltip);
        }
    }

    /**
     * Changes the position of a {@link JMenu} identified by a path
     * 
     * @param path The path, separated by <code>'/'</code> (As many menus/sub-menus as there are <code>'/'</code> in
     *            this {@link String}).
     * @param position The new menu position
     */
    public void setMenuPosition(String path, int position) {
        if (position > -1) {
            JComponent menu = getMenu(path);
            if (menu != null) {
                JComponent parent = (JComponent) menu.getParent();
                parent.remove(menu);
                parent.add(menu, position);
            }
        }
    }

    /**
     * Changes the position of a {@link JMenu} identified by a path, to put it to last one
     * 
     * @param path The path, separated by <code>'/'</code> (As many menus/sub-menus as there are <code>'/'</code> in
     *            this {@link String}).
     */
    public void setLast(String path) {
        JComponent menu = getMenu(path);
        JComponent parent = (JComponent) menu.getParent();
        if (menu != null) {
            parent.remove(menu);
            parent.add(menu);
        }
    }

    /**
     * Recovers the {@link JMenu} that corresponds to a given path, creating all missing menus.
     * 
     * @param path The path, separated by <code>'/'</code> (As many menus/sub-menus as there are <code>'/'</code> in
     *            this {@link String}).
     * @return A {@link JMenu}
     */
    protected JComponent getMenu(String path) {
        JComponent menu = null;
        if (path != null) {
            String[] paths = path.split("/");
            menu = this;
            for (String menuName : paths) {
                menu = getMenu(menuName, menu);
            }
        }
        return menu;
    }

    /**
     * Adds an {@link AbstractAction} in this {@link QuickMenuBar}, at a given path.
     * 
     * @param path The path, separated by <code>'/'</code> (As many menus/sub-menus as there are <code>'/'</code> in
     *            this {@link String}).
     * @param action The {@link AbstractAction}
     */
    public void addAction(String path, AbstractAction action) {
        JComponent parent = getMenu(path);
        if (parent != null) {
            JMenuItem item = createMenuItem(action);
            if (item != null) {
                parent.add(item);
            }
        }
    }

    public void setActionVisible(String path, String itemText, boolean visible) {
        JMenuItem item = getMenuItem(getMenu(path), itemText);
        if (item != null) {
            item.setVisible(visible);
        }
    }

    /**
     * Adds a separator in this {@link QuickMenuBar}, at a given path.
     * 
     * @param path The path, separated by <code>'/'</code> (As many menus/sub-menus as there are <code>'/'</code> in
     *            this {@link String}).
     */
    public void addSeparator(String path) {
        addSeparator(path, null);
    }

    /**
     * Adds a titled separator in this {@link QuickMenuBar}, at a given path.
     * 
     * @param path The path, separated by <code>'/'</code> (As many menus/sub-menus as there are <code>'/'</code> in
     *            this {@link String}).
     * @param title The title of the separator
     */
    public void addSeparator(String path, String title) {
        JComponent parent = getMenu(path);
        if (parent instanceof JMenu) {
            JMenu menu = (JMenu) parent;
            if (title == null) {
                menu.addSeparator();
            } else {
                menu.add(new JXTitledSeparator(title, SwingConstants.CENTER));
            }
        }
    }

    /**
     * Removes a separator from this {@link QuickMenuBar}, at a given path.
     * 
     * @param path The path, separated by <code>'/'</code> (As many menus/sub-menus as there are <code>'/'</code> in
     *            this {@link String}).
     */
    public void removeSeparator(String path) {
        removeSeparator(path, null);
    }

    /**
     * Removes a titled separator from this {@link QuickMenuBar}, at a given path.
     * 
     * @param path The path, separated by <code>'/'</code> (As many menus/sub-menus as there are <code>'/'</code> in
     *            this {@link String}).
     * @param title The title of the separator
     */
    public void removeSeparator(String path, String title) {
        JComponent parent = getMenu(path);
        if (parent instanceof JMenu) {
            JMenu menu = (JMenu) parent;
            Component toRemove = null;
            if (title == null) {
                for (int i = menu.getMenuComponentCount() - 1; i >= 0; i--) {
                    Component comp = menu.getMenuComponent(i);
                    if (comp instanceof JPopupMenu.Separator) {
                        toRemove = comp;
                        break;
                    }
                }
            } else {
                for (int i = menu.getMenuComponentCount() - 1; i >= 0; i--) {
                    Component comp = menu.getMenuComponent(i);
                    if ((comp instanceof JXTitledSeparator) && title.equals(((JXTitledSeparator) comp).getTitle())) {
                        toRemove = comp;
                        break;
                    }
                }
            }
            if (toRemove != null) {
                menu.remove(toRemove);
            }
        }
    }

    /**
     * Completely removes a {@link JMenu} from this{@link QuickMenuBar}
     * 
     * @param path The path of the {@link JMenu} to remove
     */
    public void removeMenu(String path) {
        cleanActions(path, true);
        JComponent menu = getMenu(path);
        if (menu instanceof JMenu) {
            cleanMenu((JMenu) menu);
        }
        if (menu != null) {
            Container parent = menu.getParent();
            if (parent != null) {
                parent.remove(menu);
            }
        }
    }

    /**
     * Completely cleans a {@link JMenu}, and all its sub-menus (recursive)
     * 
     * @param menu The {@link JMenu} to clean
     */
    protected void cleanMenu(JMenu menu) {
        if (menu != null) {
            for (int i = 0; i < menu.getItemCount(); i++) {
                if (menu.getItem(i) instanceof JMenu) {
                    cleanMenu((JMenu) menu.getItem(i));
                }
            }
            menu.removeAll();
        }
    }

    /**
     * Cleans all the actions at a given path
     * 
     * @param path The path to clean
     * @param recursive Whether to clean sub-menu actions too
     */
    public void cleanActions(String path, boolean recursive) {
        JComponent parent = getMenu(path);
        if (parent != null) {
            if (parent instanceof JMenu) {
                cleanActions((JMenu) parent, recursive);
            } else {
                List<JMenuItem> itemsToRemove = new ArrayList<JMenuItem>();
                for (Component comp : parent.getComponents()) {
                    if (comp instanceof JMenu) {
                        if (recursive) {
                            cleanActions((JMenu) comp, recursive);
                        }
                    } else if (comp instanceof JMenuItem) {
                        JMenuItem item = (JMenuItem) comp;
                        if (item.getAction() != null) {
                            itemsToRemove.add(item);
                        }
                    }
                }
                for (JMenuItem item : itemsToRemove) {
                    ComponentUtils.safelyRemoveFromParent(item);
                }
                itemsToRemove.clear();
            }
        }
    }

    /**
     * Cleans the actions in a {@link JMenu}
     * 
     * @param menu The {@link JMenu} to clean
     * @param recursive Whether to clean sub-menu actions too
     */
    protected void cleanActions(JMenu menu, boolean recursive) {
        if (menu != null) {
            List<JMenuItem> itemsToRemove = new ArrayList<JMenuItem>();
            for (int i = 0; i < menu.getItemCount(); i++) {
                JMenuItem item = menu.getItem(i);
                if (item instanceof JMenu) {
                    if (recursive) {
                        cleanActions((JMenu) item, recursive);
                    }
                } else if (item.getAction() != null) {
                    itemsToRemove.add(item);
                }
            }
            for (JMenuItem item : itemsToRemove) {
                ComponentUtils.safelyRemoveFromParent(item);
            }
            itemsToRemove.clear();
        }
    }

    /**
     * Removes a {@link JMenuItem} from this {@link QuickMenuBar}
     * 
     * @param path The parent path
     * @param itemText The {@link JMenuItem} text
     */
    public void removeItem(String path, String itemText) {
        JComponent parent = getMenu(path);
        if (parent != null) {
            JMenuItem item = getMenuItem(parent, itemText);
            if (item != null) {
                ComponentUtils.safelyRemoveFromParent(item);
            }
        }
    }

    public JMenuItem getMenuItem(String path, String itemText) {
        JMenuItem item = null;
        JComponent parent = getMenu(path);
        if (parent != null) {
            item = getMenuItem(parent, itemText);
        }
        return item;
    }

    public void setLastItem(String path, String itemText) {
        JComponent parent = getMenu(path);
        if (parent != null) {
            JMenuItem item = getMenuItem(parent, itemText);
            if (item != null) {
                parent.add(item, parent.getComponentCount() - 1);
            }
        }
    }

    protected JMenuItem getMenuItem(JComponent parent, String itemText) {
        JMenuItem item = null;
        if (itemText != null) {
            if (parent instanceof JMenu) {
                JMenu menu = (JMenu) parent;
                for (int i = 0; i < menu.getItemCount(); i++) {
                    JMenuItem temp = menu.getItem(i);
                    if ((temp != null) && ObjectUtils.sameObject(itemText, temp.getText())) {
                        item = temp;
                        break;
                    }
                }
            } else {
                for (int i = 0; i < parent.getComponentCount(); i++) {
                    Component comp = parent.getComponent(i);
                    if (comp instanceof JMenuItem) {
                        JMenuItem temp = (JMenuItem) comp;
                        if (ObjectUtils.sameObject(itemText, temp.getText())) {
                            item = temp;
                            break;
                        }
                    }
                }
            }
        }
        return item;
    }

    /**
     * Creates a {@link JMenuItem}, based on an {@link AbstractAction}
     * 
     * @param action The {@link AbstractAction}
     * @return A {@link JMenuItem}
     */
    protected JMenuItem createMenuItem(AbstractAction action) {
        JMenuItem menuItem;
        if (action instanceof AbstractActionExt) {
            AbstractActionExt actionExt = (AbstractActionExt) action;
            menuItem = fact.createMenuItem(actionExt);
            if (actionExt.isStateAction()) {
                menuItem.setSelected(actionExt.isSelected());
            }
        } else {
            menuItem = new JMenuItem(action);
        }
        menuItem.setFont(getFont());
        return menuItem;
    }

    /**
     * Recovers a {@link JMenu} that is supposed to display a given text, in a parent {@link JComponent}. If such a
     * {@link JMenu} does not exist yet, this method creates it and
     * adds it in the parent.
     * 
     * @param menuText The expected {@link JMenu} text
     * @param parent The parent {@link JComponent}
     * @return A {@link JMenu}
     */
    protected JMenu getMenu(String menuText, JComponent parent) {
        JMenu menu = null;
        if ((menuText != null) && (parent != null)) {
            if (parent instanceof JMenu) {
                JMenu parentMenu = (JMenu) parent;
                for (int i = 0; i < parentMenu.getItemCount(); i++) {
                    menu = checkMenu(parentMenu.getItem(i), menuText);
                    if (menu != null) {
                        break;
                    }
                }
            } else {
                for (int i = 0; i < parent.getComponentCount(); i++) {
                    menu = checkMenu(parent.getComponent(i), menuText);
                    if (menu != null) {
                        break;
                    }
                }
            }
            if (menu == null) {
                menu = new JMenu(menuText);
                menu.setFont(getFont());
                parent.add(menu);
            }
        }
        return menu;
    }

    /**
     * Checks whether a given {@link Component} is a {@link JMenu} that displays an expected text
     * 
     * @param comp The {@link Component} to check
     * @param text The expected text
     * @return The {@link Component} casted as a {@link JMenu}, if it matches a {@link JMenu} displaying expected text,
     *         <code>null</code> otherwise
     */
    protected JMenu checkMenu(Component comp, String text) {
        JMenu checked = null;
        if (comp instanceof JMenu) {
            JMenu temp = (JMenu) comp;
            if (text.equals(temp.getText())) {
                checked = temp;
            }
        }
        return checked;
    }

}
