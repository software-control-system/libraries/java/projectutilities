/*
 * This file is part of SwingXUtilities.
 * 
 * SwingXUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingXUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingXUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swingx.log;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.RowSorter.SortKey;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableModel;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.action.AbstractActionExt;
import org.jdesktop.swingx.action.BoundAction;
import org.jdesktop.swingx.table.ColumnControlButton;
import org.jdesktop.swingx.table.TableColumnExt;

import fr.soleil.lib.project.comparator.ILogComparator;
import fr.soleil.lib.project.comparator.LogLevelComparator;
import fr.soleil.lib.project.comparator.LogMessageComparator;
import fr.soleil.lib.project.comparator.LogTimestampComparator;
import fr.soleil.lib.project.comparator.MultiComparator;
import fr.soleil.lib.project.log.ILogManager;
import fr.soleil.lib.project.log.Level;
import fr.soleil.lib.project.log.LogData;
import fr.soleil.lib.project.swing.SwingUtilitiesConstants;
import fr.soleil.lib.project.swing.listener.TableRowHeightResetter;
import fr.soleil.lib.project.swing.model.LogTableModel;
import fr.soleil.lib.project.swing.renderer.ILevelRenderer;
import fr.soleil.lib.project.swing.renderer.LogTableCellRenderer;

/**
 * A {@link JXTable} dedicated in {@link LogData} display.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LogTable extends JXTable implements ILogManager, ILevelRenderer, SwingUtilitiesConstants {

    private static final long serialVersionUID = 1049210280589428284L;

    protected static final Color DEFAULT_GRID_COLOR = new Color(220, 220, 220);

    // Hack to avoid undesired behavior with auto resize mode
    protected static final String ACTION_TO_REMOVE = HORIZONTALSCROLL_ACTION_COMMAND;
    protected static final String AUTO_RESIZE_COLUMNS = ColumnControlButton.COLUMN_CONTROL_MARKER + "autoresize";
    protected static final String AUTO_RESIZE_COLUMNS_TEXT = DEFAULT_MESSAGE_MANAGER
            .getMessage("fr.soleil.lib.project.swing.table.columns.autoresize");
    protected static final String AUTO_RESIZE_COLUMNS_METHOD = "setAutoResizeColumns";
    protected final LogTableCellRenderer renderer;
    protected final TableRowHeightResetter rowHeightResetter;
    protected boolean pack;
    // This boolean is a hack to avoid undesired wrong colon size behavior when auto-resize is activated
    protected volatile boolean duringResize;

    public LogTable() {
        this(new LogTableModel());
    }

    public LogTable(LogTableModel model) {
        super(model);
        rowHeightResetter = new TableRowHeightResetter(this);
        if (model != null) {
            model.addTableModelListener(rowHeightResetter);
        }
        pack = model.getRowCount() == 0;
        duringResize = false;
        renderer = generateRenderer();
        setDefaultRenderer(Object.class, renderer);
        setDefaultRenderer(Level.class, renderer);
        setDefaultRenderer(String.class, renderer);
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        setColumnControlVisible(true);
        ((JTable) this).setGridColor(DEFAULT_GRID_COLOR);
    }

    /**
     * Returns the cell vertical margin used by this {@link LogTable}.
     * 
     * @return An <code>int</code>.
     */
    public int getCellVerticalMargin() {
        return renderer.getVerticalMargin();
    }

    /**
     * Sets the cell vertical margin this {@link LogTable} should use.
     * 
     * @param verticalMargin The cell vertical margin this {@link LogTable} should use.
     *            <p>
     *            Use a value &lt; 0 to reset to default value.
     *            </p>
     */
    public void setCellVerticalMargin(int verticalMargin) {
        // This forces to reset table row height
        rowHeightResetter.tableChanged(new TableModelEvent(getModel()));
        renderer.setVerticalMargin(verticalMargin);
        revalidate();
        repaint();
    }

    /**
     * Return the alternate color used every second grid line.
     * 
     * @return A {@link Color}.
     */
    public Color getAlternateColor() {
        return renderer.getAlternateColor();
    }

    /**
     * Sets the alternate color to use every second grid line.
     * 
     * @param alternateColor The alternate color to use every second grid line.
     */
    public void setAlternateColor(Color alternateColor) {
        renderer.setAlternateColor(alternateColor);
        repaint();
    }

    /**
     * Returns whether this {@link LogTableCellRenderer} will use alternate color, i.e. a darker color, every second
     * grid line.
     * 
     * @return A <code>boolean></code>.
     */
    public boolean isUseAlternateColor() {
        return renderer.isUseAlternateColor();
    }

    /**
     * Sets whether this {@link LogTableCellRenderer} can use alternate color, i.e. a darker color, every second
     * grid line.
     * 
     * @param useAlternateColor Whether this {@link LogTableCellRenderer} can use alternate color.
     */
    public void setUseAlternateColor(boolean useAlternateColor) {
        renderer.setUseAlternateColor(useAlternateColor);
        repaint();
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        super.tableChanged(e);
        int count = getModel().getRowCount();
        if (pack && (count > 0) && !isAutoResizeColumns()) {
            pack = false;
            packAll();
        }
    }

    /**
     * Generates the {@link LogTableCellRenderer} this {@link LogTable} will use.
     * 
     * @return a {@link LogTableCellRenderer}. Never <code>null</code>;
     */
    protected LogTableCellRenderer generateRenderer() {
        return new LogTableCellRenderer();
    }

    @Override
    protected JComponent createDefaultColumnControl() {
        // Remove this action, because when auto resize mode is off, this action changes auto resize mode,
        // and it is then impossible to go back to auto resize mode off.
        getActionMap().remove(ACTION_TO_REMOVE);
        getActionMap().put(AUTO_RESIZE_COLUMNS, createAutoResizeAction());
        return super.createDefaultColumnControl();
    }

    @Override
    public LogTableModel getModel() {
        return (LogTableModel) super.getModel();
    }

    /**
     * Returns whether columns will automatically be resized to fit displayable area.
     * 
     * @return A <code>boolean<./code>
     */
    public boolean isAutoResizeColumns() {
        return getAutoResizeMode() != AUTO_RESIZE_OFF;
    }

    /**
     * Sets whether columns should automatically be resized to fit displayable area.
     * 
     * @param autoResize Whether columns should automatically be resized to fit displayable area.
     */
    public void setAutoResizeColumns(boolean autoResize) {
        setAutoResizeMode(autoResize ? AUTO_RESIZE_LAST_COLUMN : AUTO_RESIZE_OFF);
    }

    /**
     * Creates and returns the default <code>Action</code> for allowing columns autoresize.
     */
    protected Action createAutoResizeAction() {
        BoundAction action = new BoundAction(AUTO_RESIZE_COLUMNS_TEXT, AUTO_RESIZE_COLUMNS);
        action.setStateAction();
        action.registerCallback(this, AUTO_RESIZE_COLUMNS_METHOD);
        action.setSelected(isAutoResizeColumns());
        return action;
    }

    @Override
    public void setAutoResizeMode(int mode) {
        super.setAutoResizeMode(mode);
        Action action = getActionMap().get(AUTO_RESIZE_COLUMNS);
        if (action instanceof AbstractActionExt) {
            ((AbstractActionExt) action).setSelected(isAutoResizeColumns());
        }
    }

    /**
     * Updates the width of a column.
     * 
     * @param ext The {@link TableColumnExt} that manages the column.
     * @param width The width to apply.
     * @param resizing Whether to use the resize hack (in order to avoid an undesired column resize behavior).
     */
    protected void updateColumnWidth(TableColumnExt ext, int width, boolean resizing) {
        duringResize = resizing;
        ext.setWidth(width);
        ext.setPreferredWidth(width);
    }

    /**
     * Computes the effective column margin to apply.
     * 
     * @param margin The theoretical margin.
     * @return An <code>int</code>: the effective margin.
     */
    protected int getColumnMargin(int margin) {
        return 2 * (margin < 0 ? getColumnFactory().getDefaultPackMargin() : margin);
    }

    /**
     * Computes the best width for a given column.
     * 
     * @param column The column index.
     * @param refMargin The column margin to use.
     * @return An <code>int</code>: the calculated width.
     */
    protected int computeColumnPreferredWidth(int column, int refMargin) {
        int width = 0;
        for (int row = 0; row < getRowCount(); row++) {
            Component c = renderer.getTableCellRendererComponent(this, getValueAt(row, column), false, false, row,
                    column);
            width = Math.max(width, c.getPreferredSize().width);
        }
        width += refMargin;
        return width;
    }

    @Override
    public void packSelected() {
        if (getAutoResizeMode() == AUTO_RESIZE_LAST_COLUMN) {
            // This is a hack to avoid undesired wrong colon size behavior when auto-resize is activated
            int selected = getColumnModel().getSelectionModel().getLeadSelectionIndex();
            if (selected > -1) {
                if (selected == getColumnCount() - 1) {
                    super.packSelected();
                } else {
                    int refMargin = getColumnMargin(-1);
                    int remainingWidth = getWidth();
                    // Just for information, index in getColumnExt(index) follows the view order, not the model order.
                    for (int column = 0; column < getColumnCount() - 1; column++) {
                        if (column != selected) {
                            remainingWidth -= getColumnExt(column).getWidth();
                        }
                    }
                    int width = computeColumnPreferredWidth(selected, refMargin);
                    remainingWidth -= width;
                    updateColumnWidth(getColumnExt(selected), width, true);
                    if (remainingWidth < refMargin + 2) {
                        duringResize = false;
                        super.packSelected();
                    } else {
                        updateColumnWidth(getColumnExt(getColumnCount() - 1), remainingWidth, true);
                    }
                }
            }
        } else {
            super.packSelected();
        }
    }

    @Override
    public void packTable(int margin) {
        if (isAutoResizeColumns()) {
            // This is a hack to avoid undesired wrong colon size behavior when auto-resize is activated
            int refMargin = getColumnMargin(margin);
            int remainingWidth = getWidth();
            // Just for information, index in getColumnExt(index) follows the view order, not the model order.
            for (int column = 0; column < getColumnCount() - 1; column++) {
                int width = computeColumnPreferredWidth(column, refMargin);
                remainingWidth -= width;
                updateColumnWidth(getColumnExt(column), width, true);
            }
            if (getColumnCount() > 0) {
                if (remainingWidth < refMargin + 2) {
                    // too small size : use super pack
                    for (int column = 0; column < getColumnCount() - 1; column++) {
                        updateColumnWidth(getColumnExt(column), 0, false);
                    }
                    super.packTable(margin);
                } else {
                    updateColumnWidth(getColumnExt(getColumnCount() - 1), remainingWidth, true);
                }
            }
        } else {
            super.packTable(margin);
        }
    }

    /**
     * Sets this {@link LogTable} table model.
     * 
     * @param dataModel The table model. Must be a {@link LogTableModel}. Otherwise, an
     *            {@link UnsupportedOperationException} will be raised.
     * @throws UnsupportedOperationException If <code>dataModel</code> is not a {@link LogTableModel}.
     */
    @Override
    public void setModel(TableModel dataModel) throws UnsupportedOperationException {
        if (dataModel instanceof LogTableModel) {
            TableModel former = getModel();
            if ((former != null) && (rowHeightResetter != null)) {
                former.removeTableModelListener(rowHeightResetter);
            }
            super.setModel(dataModel);
            if ((dataModel != null) && (rowHeightResetter != null)) {
                dataModel.addTableModelListener(rowHeightResetter);
            }
        } else {
            throw new UnsupportedOperationException("Only LogTableModel is supported");
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void doLayout() {
        // This is a hack to avoid undesired wrong colon size behavior when auto-resize is activated
        if (duringResize) {
            duringResize = false;
            // layout() is the default method called by Container.doLayout().
            // But JTable overrides doLayout() method to add a behavior we want to avoid in this particular case.
            layout();
        } else {
            super.doLayout();
        }
    }

    @Override
    public boolean isLevelColorEnabled() {
        return renderer.isLevelColorEnabled();
    }

    @Override
    public void setLevelColorEnabled(boolean levelColorEnabled) {
        renderer.setLevelColorEnabled(levelColorEnabled);
        repaint();
    }

    @Override
    public Font getLevelFont() {
        return renderer.getLevelFont();
    }

    @Override
    public void setLevelFont(Font font) {
        rowHeightResetter.tableChanged(new TableModelEvent(getModel()));
        renderer.setLevelFont(font);
        revalidate();
        repaint();
    }

    @Override
    public LogData[] getLogs() {
        LogData[] logs = getModel().getLogs();
        // Try to return logs sorted in the same order as the one displayed in table.
        if (logs != null) {
            List<? extends SortKey> sortKeys = getRowSorter().getSortKeys();
            if ((sortKeys != null) && (!sortKeys.isEmpty())) {
                Collection<ILogComparator> comparators = new ArrayList<>(3);
                for (SortKey sortKey : sortKeys) {
                    Boolean descending;
                    ILogComparator comparator = null;
                    if (sortKey.getSortOrder() == null) {
                        descending = null;
                    } else {
                        switch (sortKey.getSortOrder()) {
                            case ASCENDING:
                                descending = Boolean.FALSE;
                                break;
                            case DESCENDING:
                                descending = Boolean.TRUE;
                                break;
                            default:
                                descending = null;
                                break;
                        }
                    }
                    if (descending != null) {
                        switch (sortKey.getColumn()) {
                            case 0:
                                comparator = new LogTimestampComparator(descending.booleanValue());
                                break;
                            case 1:
                                comparator = new LogLevelComparator(descending.booleanValue());
                                break;
                            case 3:
                                comparator = new LogMessageComparator(descending.booleanValue());
                                break;
                            default:
                                comparator = null;
                                break;
                        }
                    }
                    if (comparator != null) {
                        comparators.add(comparator);
                    }
                }
                if (!comparators.isEmpty()) {
                    MultiComparator<LogData> comparator = new MultiComparator<>(
                            comparators.toArray(new ILogComparator[comparators.size()]));
                    Arrays.sort(logs, comparator);
                }
            }
        }
        return logs;
    }

    @Override
    public void addLogs(LogData... logs) {
        getModel().addLogs(logs);
    }

    @Override
    public void setLogs(LogData... logs) {
        getModel().setLogs(logs);
    }

    @Override
    public void setLogs(boolean keepLastLogs, LogData... logs) {
        getModel().setLogs(keepLastLogs, logs);
    }

    @Override
    public void clearLogs() {
        getModel().clearLogs();
    }

    @Override
    public int getMaxLogs() {
        return getModel().getMaxLogs();
    }

    @Override
    public void setMaxLogs(int maxLogs) {
        getModel().setMaxLogs(maxLogs);
    }
}
