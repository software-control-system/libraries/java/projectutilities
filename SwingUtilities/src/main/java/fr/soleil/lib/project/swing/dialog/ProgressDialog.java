/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.dialog;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.Future;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.awt.adapter.Canceler;
import fr.soleil.lib.project.swing.JDoubleProgressBar;
import fr.soleil.lib.project.swing.icons.Icons;

/**
 * A {@link JAnchoredDialog} that can be used to manage a work progress. It uses a progress bar, and
 * a particular icon in case of indeterminate progress.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ProgressDialog extends JAnchoredDialog {

    private static final long serialVersionUID = -474275017658388421L;

    protected final static Font TITLE_FONT = new Font(Font.DIALOG, Font.BOLD, 12);
    protected final static Font DEFAULT_FONT = new Font(Font.DIALOG, Font.PLAIN, 11);
    protected final static ImageIcon LOADING_ICON = new ImageIcon(Icons.class.getResource("loading.gif"));
    protected static final double DARK_FRACTION = 0.4;
    protected static final Color DARK_PROGRESS_DEFAULT_FG = new Color(110, 110, 160, 200);
    protected static final Color BACK_PROGRESS_DEFAULT_FG = new Color(156, 156, 255, 200);
    protected static final Color LIGHT_PROGRESS_DEFAULT_FG = new Color(200, 200, 255, 200);
    protected static final Color DARK_PROGRESS_DEFAULT_BG = new Color(0, 160, 0, 255);
    protected static final Color BACK_PROGRESS_DEFAULT_BG = new Color(0, 255, 0, 255);
    protected static final Color LIGHT_PROGRESS_DEFAULT_BG = new Color(100, 255, 100, 255);

    protected JDoubleProgressBar progressBar;
    protected JPanel mainPanel;
    protected JLabel mainMessageLabel;
    protected JLabel secondaryMessageLabel;
    protected JLabel tertiaryMessageLabel;
    protected JLabel indeterminateProgressLabel;
    protected GridBagConstraints progressBarConstraints;
    protected Canceler canceler;
    protected boolean closeOnMaxProgress;
    protected volatile boolean indeterminate;

    /**
     * Default constructor.
     */
    public ProgressDialog() {
        this(null);
    }

    public ProgressDialog(Window owner) {
        super(owner, null, false);
        canceler = new Canceler();
        addWindowListener(canceler);
        hiddenOwner.addWindowListener(canceler);
        closeOnMaxProgress = false;
        indeterminate = false;
        initComponents();
        addComponents();
    }

    protected void initComponents() {
        mainPanel = new JPanel(new GridBagLayout());

        mainMessageLabel = new JLabel(" ");
        mainMessageLabel.setFont(TITLE_FONT);

        secondaryMessageLabel = new JLabel();
        secondaryMessageLabel.setFont(DEFAULT_FONT);

        tertiaryMessageLabel = new JLabel();
        tertiaryMessageLabel.setFont(DEFAULT_FONT);

        indeterminateProgressLabel = new JLabel(LOADING_ICON, JLabel.CENTER);

        progressBar = new JDoubleProgressBar();
        progressBar.setDisplayFGProgress(true);
        progressBar.setOpaque(false);
        progressBar.setFont(DEFAULT_FONT);
        progressBar.setFGMaximum(100);
        progressBar.setFGValue(0);
        progressBar.setBGMaximum(100);
        progressBar.setBGValue(0);
        progressBar.setFGProgressBarColors(BACK_PROGRESS_DEFAULT_FG, LIGHT_PROGRESS_DEFAULT_FG,
                DARK_PROGRESS_DEFAULT_FG);
        progressBar.setBGProgressBarColors(BACK_PROGRESS_DEFAULT_BG, LIGHT_PROGRESS_DEFAULT_BG,
                DARK_PROGRESS_DEFAULT_BG);
        progressBar.setFont(DEFAULT_FONT);
        progressBar.setTransparent(true);
        progressBar.setMinimumSize(progressBar.getPreferredSize());
    }

    protected void addComponents() {
        GridBagConstraints mainMessageConstraints = new GridBagConstraints();
        mainMessageConstraints.fill = GridBagConstraints.HORIZONTAL;
        mainMessageConstraints.gridx = 0;
        mainMessageConstraints.gridy = 0;
        mainMessageConstraints.weightx = 1;
        mainMessageConstraints.weighty = 0;
        mainMessageConstraints.insets = new Insets(5, 5, 5, 5);
        mainPanel.add(mainMessageLabel, mainMessageConstraints);
        GridBagConstraints secondaryMessageConstraints = new GridBagConstraints();
        secondaryMessageConstraints.fill = GridBagConstraints.HORIZONTAL;
        secondaryMessageConstraints.gridx = 0;
        secondaryMessageConstraints.gridy = 1;
        secondaryMessageConstraints.weightx = 1;
        secondaryMessageConstraints.weighty = 0;
        secondaryMessageConstraints.insets = new Insets(0, 5, 0, 5);
        mainPanel.add(secondaryMessageLabel, secondaryMessageConstraints);
        progressBarConstraints = new GridBagConstraints();
        progressBarConstraints.fill = GridBagConstraints.HORIZONTAL;
        progressBarConstraints.gridx = 0;
        progressBarConstraints.gridy = 2;
        progressBarConstraints.weightx = 1;
        progressBarConstraints.weighty = 0;
        progressBarConstraints.insets = new Insets(5, 5, 5, 5);
        mainPanel.add(progressBar, progressBarConstraints);

        GridBagConstraints tertiaryMessageConstraints = new GridBagConstraints();
        tertiaryMessageConstraints.fill = GridBagConstraints.HORIZONTAL;
        tertiaryMessageConstraints.gridx = 0;
        tertiaryMessageConstraints.gridy = 3;
        tertiaryMessageConstraints.weightx = 1;
        tertiaryMessageConstraints.weighty = 0;
        tertiaryMessageConstraints.insets = new Insets(0, 5, 0, 5);
        mainPanel.add(tertiaryMessageLabel, tertiaryMessageConstraints);

        setContentPane(mainPanel);
    }

    public void setMainPanelBackground(Color background) {
        mainPanel.setBackground(background);
    }

    /**
     * Sets the border of the content pane of this dialog
     * 
     * @param border the border to set
     */
    public void setMainPanelBorder(Border border) {
        mainPanel.setBorder(border);
    }

    /**
     * Sets the background color of the progress bar
     * 
     * @param background the color to set
     */
    public void setProgressBackground(Color background) {
        progressBar.setBackground(background);
    }

    /**
     * Sets the foreground color of the progress bar
     * 
     * @param foreground the color to set
     */
    public void setProgressForeground(Color foreground) {
        progressBar.setForeground(foreground);
    }

    /**
     * Sets the progress bar opacity
     * 
     * @param opaque a boolean value
     */
    public void setProgressOpaque(boolean opaque) {
        progressBar.setOpaque(opaque);
    }

//    /**
//     * Returns the progress bar minimum value
//     * 
//     * @return An <code>int</code>
//     */
//    public int getMinProgress() {
//        return 0;
//    }
//
//    /**
//     * Sets the progress bar minimum value
//     * 
//     * @param min the value to set
//     */
//    public void setMinProgress(int min) {
//        // progressBar.setfg(min);
//    }

    /**
     * Returns the progress bar maximum value
     * 
     * @return An <code>int</code>
     */
    public int getMaxProgress() {
        return (int) progressBar.getFGMaximum();
    }

    /**
     * Sets the progress bar maximum value
     * 
     * @param max the value to set
     */
    public void setMaxProgress(int max) {
        progressBar.setFGMaximum(max);
    }

    public void setMaxProgress(int maxFirst, int maxSecond) {
        progressBar.setFGMaximum(maxFirst);
        progressBar.setBGMaximum(maxSecond);
    }

    /**
     * Sets the progress bar value (if not indeterminate).
     * 
     * @param progress the progress value
     */
    public void setProgress(final int progress) {
        progressBar.setFGValue(progress);
        if (progress >= progressBar.getFGMaximum() && isCloseOnMaxProgress() && !isProgressIndeterminate()) {
            setVisible(false);
        }
    }

    /**
     * Sets the progress bar value (if not indeterminate).
     * 
     * @param progress the progress value
     */
    public void setProgressSecond(int progress) {
        progressBar.setBGValue(progress);
        if (((progress >= progressBar.getBGMaximum()) && (progressBar.getFGValue() >= progressBar.getFGMaximum()))
                && isCloseOnMaxProgress() && (!isProgressIndeterminate())) {
            setVisible(false);
        }
    }

    /**
     * Sets the progress bar value (if not indeterminate), and displays a message in this dialog.
     * 
     * @param progress the progress value
     * @param messages the messages to display (2 maximum for now, one above and one under)
     */
    public void setProgress(int progress, String... messages) {
        setProgress(progress);
        if ((messages == null) || (messages.length == 0)) {
            messages = new String[] { " ", " " };
        } else {
            for (int i = 0; i < messages.length; i++) {
                if ((messages[i] == null) || messages[i].isEmpty()) {
                    messages[i] = " ";
                }
            }
        }
        secondaryMessageLabel.setText(messages[0]);
        if (messages.length > 1) {
            tertiaryMessageLabel.setText(messages[1]);
        }
    }

    /**
     * Return the current progress value
     * 
     * @return an int
     */
    public int getProgress() {
        return (int) progressBar.getFGValue();
    }

    /**
     * Return the Second progress value
     * 
     * @return an int
     */
    public int getProgressSecond() {
        return (int) progressBar.getBGValue();
    }

    /**
     * Sets the main (highlighted) message to display
     * 
     * @param message the message to display
     */
    public void setMainMessage(String message) {
        if (message == null || message.isEmpty()) {
            message = " ";
        }
        mainMessageLabel.setText(message);
    }

    /**
     * Returns whether the progress is indeterminate
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isProgressIndeterminate() {
        return indeterminate;
    }

    /**
     * Sets whether the progress is indeterminate
     * 
     * @param indeterminate a boolean value
     */
    public void setProgressIndeterminate(boolean indeterminate) {
        this.indeterminate = indeterminate;
        mainPanel.remove(progressBar);
        mainPanel.remove(indeterminateProgressLabel);
        if (indeterminate) {
            tertiaryMessageLabel.setVisible(false);
            mainPanel.add(indeterminateProgressLabel, progressBarConstraints);
        } else {
            tertiaryMessageLabel.setVisible(true);
            mainPanel.add(progressBar, progressBarConstraints);
        }
    }

    /**
     * Associates a {@link Future} with this {@link ProgressDialog}
     * 
     * @param cancelable the {@link Future} to set
     */
    public void setCancelable(Future<?> cancelable) {
        canceler.setCancelable(cancelable);
    }

    /**
     * Associates an {@link ICancelable} with this {@link ProgressDialog}
     * 
     * @param cancelable the {@link ICancelable} to set
     */
    public void setCancelable(ICancelable cancelable) {
        canceler.setCancelable(cancelable);
    }

    /**
     * Explicitly cancels the associated {@link ICancelable} or {@link Future}, if set.
     */
    public void cancel() {
        canceler.cancel();
    }

    public void setCurrentProgressMessage(String message) {
        this.secondaryMessageLabel.setText(message);
    }

    public void setOverallProgressMessage(String message) {
        this.tertiaryMessageLabel.setText(message);
    }

    /**
     * Returns whether this {@link ProgressDialog} should close automatically when maximum progress is obtained and
     * progress bar is displayed
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isCloseOnMaxProgress() {
        return closeOnMaxProgress;
    }

    /**
     * Sets whether this {@link ProgressDialog} should close automatically when maximum progress is obtained and
     * progress bar is displayed
     * 
     * @param closeOnMaxProgress Whether this {@link ProgressDialog} should close automatically when maximum progress is
     *            obtained and progress bar is displayed
     */
    public void setCloseOnMaxProgress(boolean closeOnMaxProgress) {
        this.closeOnMaxProgress = closeOnMaxProgress;
    }

    /**
     * to test the {@link ProgressDialog}
     * 
     * @param args not used
     */
    public static void main(String[] args) {
        ProgressDialog dialog = new ProgressDialog(null);
        dialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        dialog.setVisible(true);
    }

}
