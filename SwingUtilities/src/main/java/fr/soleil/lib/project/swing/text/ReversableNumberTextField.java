/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import fr.soleil.lib.project.resource.MessageManager;

/**
 * A {@link StateFocusNumberTextField} that can put back its previous number value
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ReversableNumberTextField extends StateFocusNumberTextField implements IReversableField {

    private static final long serialVersionUID = 4105986309982376209L;

    private String previousNumberValue = null;

    public ReversableNumberTextField(boolean allowFloatValues, boolean allowScientificValues,
            boolean allowNegativeValues, MessageManager messageManager) {
        super(allowFloatValues, allowScientificValues, allowNegativeValues, messageManager);
    }

    public ReversableNumberTextField(boolean allowFloatValues, boolean allowScientificValues,
            MessageManager messageManager) {
        super(allowFloatValues, allowScientificValues, true, messageManager);
    }

    public ReversableNumberTextField(MessageManager messageManager) {
        super(messageManager);
    }

    @Override
    public synchronized void setNumberValue(String t) {
        previousNumberValue = t;
        super.setNumberValue(t);
    }

    /**
     * Puts its previous number value
     */
    @Override
    public void cancel() {
        if ((previousNumberValue == null) || (previousNumberValue.trim().isEmpty())) {
            clear();
        } else {
            setNumberValue(previousNumberValue);
        }
    }
}
