/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.dialog;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link JFrame} used to display some error messages in a static instance
 * 
 * @author ho, Rapha&euml;l GIRARDOT
 */
public abstract class AErrorMessageDialog extends JFrame implements MouseListener {

    private static final long serialVersionUID = -167156672474196174L;

    // Text lock is used to organize thread access to the static instance
    private final StringBuilder messageBuilder;
    // Message separator
    private static final String SEPARATOR = "\n*****************************************\n";
    // String used to format dates
    private static final String DATE_FORMAT = "MM/dd/yy  HH:mm:ss => ";
    // Strings used to build location messages
    private static final String AT = " at [", IN = " in ";

    private JPanel container;
    private JScrollPane panel;
    private JTextArea texteArea;
    private JPopupMenu popupMenu;
    private JMenuItem clearMenuItem;

    protected AErrorMessageDialog() {
        super();
        messageBuilder = new StringBuilder();
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle(getDefaultTitle());

        texteArea = new JTextArea();
        texteArea.setBackground(Color.WHITE);
        texteArea.setForeground(Color.BLACK);
        texteArea.setEditable(false);
        texteArea.addMouseListener(this);

        panel = new JScrollPane(texteArea);
        panel.setSize(350, 350);
        panel.setPreferredSize(panel.getSize());
        panel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        panel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        clearMenuItem = new JMenuItem();
        clearMenuItem.setText(getClearMenuItemText());
        clearMenuItem.setToolTipText(getClearMenuItemText());
        clearMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                texteArea.setText(ObjectUtils.EMPTY_STRING);
            }
        });

        popupMenu = new JPopupMenu();
        popupMenu.setDoubleBuffered(true);
        popupMenu.add(clearMenuItem);

        container = new JPanel();
        container.setSize(250, 400);
        container.setPreferredSize(container.getSize());
        container.add(panel);

        setContentPane(panel);
        setSize(container.getSize());
        setResizable(true);
    }

    protected abstract String getDefaultTitle();

    protected abstract String getClearMenuItemText();

    /**
     * Returns the maximum number of messages allowed.
     * 
     * @return An <code>int</code>. If returned value is &le; 0, it means there is no such maximum.
     */
    protected abstract int getMaxMessages();

    /**
     * Returns the maximum number of lines allowed.
     * 
     * @return An <code>int</code>. If returned value is &le; 0, it means there is no such maximum.
     */
    protected abstract int getMaxLines();

    /**
     * Displays an error message in an {@link AErrorMessageDialog}.
     * 
     * @param errorDialog The {@link AErrorMessageDialog}.
     * @param comp THe potential {@link Component} concerned by the message.
     * @param e The {@link Throwable} that describes the error.
     * @param message The error message to display.
     * @param withDate Whether to display date before message.
     */
    protected static void showMessageDialog(final AErrorMessageDialog errorDialog, final Component comp,
            final Throwable e, final String message, final boolean withDate) {
        String errorMessage;
        synchronized (errorDialog.messageBuilder) {
            final DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            final String date = dateFormat.format(new Date());
            StringBuilder locationBuilder = new StringBuilder();
            if (comp != null) {
                Point location = comp.getLocation();
                locationBuilder.append(comp.getClass().getName()).append(AT).append(location.x).append(',')
                        .append(location.y).append(']');
            }
            try {
                if (withDate) {
                    errorDialog.messageBuilder.append(date);
                }
                errorDialog.messageBuilder.append(message);
                if (locationBuilder.length() > 0) {
                    errorDialog.messageBuilder.append(IN).append(locationBuilder);
                }
                if (e != null) {
                    errorDialog.messageBuilder.append(ObjectUtils.NEW_LINE).append(e.getMessage());
                }
                errorDialog.messageBuilder.append(SEPARATOR);
            } catch (final Exception exc) {
                errorDialog.messageBuilder.append(date).append(message);
                if (locationBuilder.length() > 0) {
                    errorDialog.messageBuilder.append(IN).append(locationBuilder);
                }
                errorDialog.messageBuilder.append(ObjectUtils.NEW_LINE).append(exc.getMessage()).append(SEPARATOR);
            }
            if (locationBuilder.length() > 0) {
                locationBuilder.delete(0, locationBuilder.length());
            }

            // Limit text size
            int max = errorDialog.getMaxLines();
            if (max > 0) {
                int length = errorDialog.messageBuilder.length();
                int lineCount = 1;
                int indexInBuffer = -1;
                for (int i = length - 1; i >= 0; i--) {
                    if (errorDialog.messageBuilder.charAt(i) == '\n') {
                        lineCount++;
                        if (lineCount > max) {
                            indexInBuffer = i;
                            break;
                        }
                    }
                }
                if (indexInBuffer > 0) {
                    errorDialog.messageBuilder.delete(0, indexInBuffer);
                }
            }
            max = errorDialog.getMaxMessages();
            if (max > 0) {
                List<Integer> indexes = new ArrayList<>();
                int index = -1;
                while (true) {
                    if (index < 0) {
                        index = errorDialog.messageBuilder.indexOf(SEPARATOR);
                    } else {
                        index = errorDialog.messageBuilder.indexOf(SEPARATOR, index + 1);
                    }
                    if (index < 0) {
                        break;
                    } else {
                        indexes.add(Integer.valueOf(index));
                    }
                }
                if (indexes.size() > max) {
                    index = indexes.get(indexes.size() - max).intValue();
                    errorDialog.messageBuilder.delete(0, index + SEPARATOR.length());
                }
            }

            // Define filtered text
            errorMessage = errorDialog.messageBuilder.toString();
        }
        // Display filtered text
        SwingUtilities.invokeLater(() -> {
            if (!errorDialog.isVisible()) {
                errorDialog.pack();
                errorDialog.setLocationRelativeTo(null);
                errorDialog.setVisible(true);
                errorDialog.toBack();
            }
            errorDialog.texteArea.setText(errorMessage);
        });
    }

    @Override
    public void mousePressed(final MouseEvent e) {
        if (e.getClickCount() == 1 && e.getButton() == MouseEvent.BUTTON3) {
            popupMenu.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    @Override
    public void mouseReleased(final MouseEvent e) {
        // Nothing to do
    }

    @Override
    public void mouseClicked(final MouseEvent e) {
        // Nothing to do
    }

    @Override
    public void mouseEntered(final MouseEvent e) {
        // Nothing to do
    }

    @Override
    public void mouseExited(final MouseEvent e) {
        // Nothing to do
    }

}
