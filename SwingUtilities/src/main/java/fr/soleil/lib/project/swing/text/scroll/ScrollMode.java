/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text.scroll;

/**
 * An {@link Enum} that represents the different ways to do an automatic scroll
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public enum ScrollMode {
    /**
     * A loop autoscroll is a classic autoscroll, where the scrolled part is continuously scrolled, as if it were stuck
     * on a wheel (for example: an advertising text).
     */
    LOOP,
    /**
     * A pendular autoscroll is a scrolling mode where the scrolled part goes from start to end, and back to start, in a
     * pendular way.
     */
    PENDULAR,
    /**
     * A reach end autoscroll is like a pendular autoscroll, except that when the end is reached, the view directly goes
     * at its start position, without reverse scrolling.
     */
    REACH_END
}
