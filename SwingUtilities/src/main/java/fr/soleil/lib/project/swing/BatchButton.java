/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;

import fr.soleil.lib.project.file.BatchExecutor;

/**
 * A {@link JButton} that is able to launch a batch in a separated process.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class BatchButton extends JButton implements ActionListener {

    private static final long serialVersionUID = 4009212562138910405L;

    private static final boolean DEFAULT_AUTO_TEXT = true;
    private static final String DEFAULT_TEXT = "No batch";

    private boolean autoText;
    private final BatchExecutor batchExecutor;

    /**
     * Default Constructor
     */
    public BatchButton() {
        this(null, null, null, null, DEFAULT_AUTO_TEXT);
    }

    /**
     * Constructs this {@link BatchButton}, setting its icon and the batch to execute
     * 
     * @param icon The button icon
     * @param batch The batch to execute
     * @see #getBatch()
     * @see #setBatch(String)
     * @see #BatchLauncher(String, Icon, String, List, boolean)
     */
    public BatchButton(Icon icon, String batch) {
        this(null, icon, batch, null, DEFAULT_AUTO_TEXT);
    }

    /**
     * Constructs this {@link BatchButton}, setting the batch to execute
     * 
     * @param batch The batch to execute
     * @see #getBatch()
     * @see #setBatch(String)
     * @see #BatchLauncher(String, Icon, String, List, boolean)
     */
    public BatchButton(String batch) {
        this(null, null, batch, null, DEFAULT_AUTO_TEXT);
    }

    /**
     * Constructs this {@link BatchButton}, setting all its parameters
     * 
     * @param text The text to display in button, if not in auto text mode
     * @param icon The button icon
     * @param batch The batch to execute
     * @param batchParameters The batch parameters
     * @param autoText The auto text mode
     * @see #setAutoText(boolean)
     * @see #isAutoText()
     * @see #setBatchParameters(List)
     * @see #getBatchParameters()
     * @see #setBatch(String)
     * @see #getBatch()
     */
    public BatchButton(String text, Icon icon, String batch, List<String> batchParameters, boolean autoText) {
        super(text, icon);
        batchExecutor = new BatchExecutor(batch, batchParameters);
        this.autoText = autoText;
        addActionListener(this);
    }

    @Override
    public String getText() {
        String text;
        if (autoText) {
            text = batchExecutor.getBatchShortName();
            if ((text == null) || (text.trim().isEmpty())) {
                text = DEFAULT_TEXT;
            }
        } else {
            text = super.getText();
        }
        return text;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        batchExecutor.execute();
    }

    /**
     * Returns whether this button calculates its text depending on the batch
     * 
     * @return A <code>boolean</code> value. <code>TRUE</code> if this button calculates its text
     *         depending on the batch, <code>FALSE</code> if this button uses the text set by {@link #setText(String)}
     * @see #setText(String)
     * @see #getBatch()
     * @see #setBatch(String)
     */
    public boolean isAutoText() {
        return autoText;
    }

    /**
     * Sets whether this button calculates its own text depending on the batch
     * 
     * @param autoText A <code>boolean</code> value.
     * @see #isAutoText()
     */
    public void setAutoText(boolean autoText) {
        this.autoText = autoText;
    }

    /**
     * Returns the path of the batch to execute
     * 
     * @return A {@link String}
     */
    public String getBatch() {
        return batchExecutor.getBatch();
    }

    /**
     * Sets the path of the batch to execute
     * 
     * @param batch The path of the batch to execute
     */
    public void setBatch(String batch) {
        batchExecutor.setBatch(batch);
    }

    /**
     * Returns the batch parameters
     * 
     * @return A {@link String} {@link List}
     */
    public List<String> getBatchParameters() {
        return batchExecutor.getBatchParameters();
    }

    /**
     * Sets the batch parameters
     * 
     * @param batchParameters The parameters to set
     */
    public void setBatchParameters(List<String> batchParameters) {
        batchExecutor.setBatchParameters(batchParameters);
    }

    /**
     * Returns the {@link Writer} used to write the batch standard output
     * 
     * @return A {@link Writer}
     */
    public Writer getStandardOutputWriter() {
        return batchExecutor.getStandardOutputWriter();
    }

    /**
     * Sets the {@link Writer} in which to write the batch standard output
     * 
     * @param standardOutputWriter The desired {@link Writer}
     */
    public void setStandardOutputWriter(Writer standardOutputWriter) {
        batchExecutor.setStandardOutputWriter(standardOutputWriter);
    }

    /**
     * Returns the {@link Writer} used to write the batch error output
     * 
     * @return A {@link Writer}
     */
    public Writer getErrorOutputWriter() {
        return batchExecutor.getErrorOutputWriter();
    }

    /**
     * Sets the {@link Writer} in which to write the batch error output
     * 
     * @param errorOutputWriter The desired {@link Writer}
     */
    public void setErrorOutputWriter(Writer errorOutputWriter) {
        batchExecutor.setErrorOutputWriter(errorOutputWriter);
    }

    public static void main(String[] args) {
        BatchButton button = new BatchButton("jive.bat");
        button.setStandardOutputWriter(new OutputStreamWriter(System.out));
        button.setErrorOutputWriter(new OutputStreamWriter(System.err));
        JFrame testFrame = new JFrame(BatchButton.class.getSimpleName() + " test");
        testFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        testFrame.setContentPane(button);
        testFrame.pack();
        testFrame.setVisible(true);
    }
}
