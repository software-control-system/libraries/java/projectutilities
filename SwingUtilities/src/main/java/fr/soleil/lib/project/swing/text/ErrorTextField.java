/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.Document;

/**
 * A JTextField which can be set in error. This means there will be a different background (light
 * red by default) and a tooltip text to warn user about an error.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ErrorTextField extends JTextField {

    private static final long serialVersionUID = -255388562229481974L;

    private Color backgroundColor = null;
    private String tooltip = null;
    private Color errorColor = new Color(255, 100, 100);
    private String errorTooltip = "Error";
    private boolean inError = false;
    private boolean opaque = false;

    public ErrorTextField() {
        super();
        initialize();
    }

    public ErrorTextField(int columns) {
        super(columns);
        initialize();
    }

    public ErrorTextField(String text) {
        super(text);
        initialize();
    }

    public ErrorTextField(String text, int columns) {
        super(text, columns);
        initialize();
    }

    public ErrorTextField(Document doc, String text, int columns) {
        super(doc, text, columns);
        initialize();
    }

    private void initialize() {
        backgroundColor = getBackground();
        opaque = isOpaque();
        tooltip = getToolTipText();
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        backgroundColor = getBackground();
    }

    @Override
    public void setOpaque(boolean isOpaque) {
        opaque = isOpaque;
        if (!inError) {
            super.setOpaque(isOpaque);
        }
    }

    @Override
    public void setToolTipText(String text) {
        tooltip = text;
        if (!inError) {
            super.setToolTipText(text);
        }
    }

    /**
     * @return the error Color
     */
    public Color getErrorColor() {
        return errorColor;
    }

    /**
     * Sets the color used with errors (light red by default)
     * 
     * @param errorColor the error Color
     */
    public void setErrorColor(Color errorColor) {
        this.errorColor = errorColor;
    }

    /**
     * @return the error tooltip text
     */
    public String getErrorTooltip() {
        return errorTooltip;
    }

    /**
     * Sets the tooltip text used with errors
     * 
     * @param errorTooltip the error tooltip text to set
     */
    public void setErrorTooltip(String errorTooltip) {
        this.errorTooltip = errorTooltip;
    }

    /**
     * @return whether this textfield is in error mode
     */
    public boolean isInError() {
        return inError;
    }

    /**
     * Sets this textfield in error mode or not
     * 
     * @param inError a boolean value
     */
    public void setInError(boolean inError) {
        this.inError = inError;
        if (inError) {
            super.setBackground(errorColor);
            super.setOpaque(true);
            super.setToolTipText(errorTooltip);
        } else {
            super.setBackground(backgroundColor);
            super.setOpaque(opaque);
            super.setToolTipText(tooltip);
        }
    }

    public static void main(String[] args) {
        JFrame testFrame = new JFrame(ErrorTextField.class.getSimpleName() + " test");
        JPanel panel = new JPanel(new GridLayout(1, 2, 5, 5));
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        ErrorTextField field = new ErrorTextField("No error");
        panel.add(field);
        field = new ErrorTextField("Error!");
        field.setInError(true);
        panel.add(field);
        testFrame.setContentPane(panel);
        testFrame.pack();
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }
}
