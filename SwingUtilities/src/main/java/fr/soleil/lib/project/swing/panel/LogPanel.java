/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.panel;

import java.util.logging.ErrorManager;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.swing.JPanel;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link JPanel} that handles logging in a text area, using a {@link Handler}
 * 
 * @see #getHandler()
 * 
 * @author Rapha&euml;l GIRARDOT
 * @deprecated Since ProjectUtilities 1.4.4, there is a LogViewer in SwingXUtilities that you should use instead.
 */
@Deprecated
public class LogPanel extends ALogPanel<Logger, Level, Handler> {

    private static final long serialVersionUID = 1473341691556911234L;

    // levels
    private static final Level[] LEVELS = new Level[] { Level.OFF, Level.SEVERE, Level.WARNING, Level.INFO,
            Level.CONFIG, Level.FINE, Level.FINER, Level.FINEST, Level.ALL };

    // div classes
    private static final String DIV_SEVERE = "<div class=\"severe\">";
    private static final String DIV_WARNING = "<div class=\"warning\">";
    private static final String DIV_INFO = "<div class=\"info\">";
    private static final String DIV_CONFIG = "<div class=\"config\">";
    private static final String DIV_FINE = "<div class=\"fine\">";
    private static final String DIV_FINER = "<div class=\"finer\">";
    private static final String DIV_FINEST = "<div class=\"finest\">";
    private static final String DIV_DEFAULT = "<div>";
    // class descriptor
    private static final String CLASS_SEVERE = "div.severe {color:#FF0000;font-size:14pt;font-weight:bold;}";
    private static final String CLASS_WARNING = "div.warning {color:#FF8000;font-size:13pt;font-style:italic;}";
    private static final String CLASS_INFO = "div.info {color:#000000;font-size:12pt;font-style:normal;}";
    private static final String CLASS_CONFIG = "div.config {color:#0000FF;font-size:12pt;font-style:normal;}";
    private static final String CLASS_FINE = "div.fine {color:#888888;font-size:12pt;font-style:normal;}";
    private static final String CLASS_FINER = "div.finer {color:#888888;font-size:10pt;font-style:normal;}";
    private static final String CLASS_FINEST = "div.finest {color:#888888;font-size:10pt;font-style:italic;}";
    // Log levels (values extracted from corresponding Levels javadoc)
    public static final int SEVERE = 1000;
    public static final int WARNING = 900;
    public static final int INFO = 800;
    public static final int CONFIG = 700;
    public static final int FINE = 500;
    public static final int FINER = 400;
    public static final int FINEST = 300;

    public LogPanel() {
        super(false);
    }

    public LogPanel(boolean html) {
        super(html);
    }

    @Override
    protected Logger generateLogger(String id) {
        return Logger.getLogger(id);
    }

    @Override
    protected void addHandlerToLogger(Logger logger, Handler handler) {
        logger.addHandler(handler);
    }

    @Override
    protected Handler generateHandler(Publisher publisher) {
        return new TextHandler(publisher);
    }

    @Override
    protected void logError(String message, Exception error) {
        logger.log(Level.SEVERE, message, error);
    }

    @Override
    protected Level[] getLevels() {
        return LEVELS;
    }

    @Override
    protected String getDivCSSClass(Level lvl) {
        int level = (lvl == null ? -1 : lvl.intValue());
        String cssClass;
        switch (level) {
            case SEVERE:
                cssClass = CLASS_SEVERE;
                break;
            case WARNING:
                cssClass = CLASS_WARNING;
                break;
            case INFO:
                cssClass = CLASS_INFO;
                break;
            case CONFIG:
                cssClass = CLASS_CONFIG;
                break;
            case FINE:
                cssClass = CLASS_FINE;
                break;
            case FINER:
                cssClass = CLASS_FINER;
                break;
            case FINEST:
                cssClass = CLASS_FINEST;
                break;
            default:
                cssClass = null;
                break;
        }

        return cssClass;
    }

    @Override
    protected String getDiv(Level lvl) {
        int level = (lvl == null ? -1 : lvl.intValue());
        String cssClass;
        switch (level) {
            case SEVERE:
                cssClass = DIV_SEVERE;
                break;
            case WARNING:
                cssClass = DIV_WARNING;
                break;
            case INFO:
                cssClass = DIV_INFO;
                break;
            case CONFIG:
                cssClass = DIV_CONFIG;
                break;
            case FINE:
                cssClass = DIV_FINE;
                break;
            case FINER:
                cssClass = DIV_FINER;
                break;
            case FINEST:
                cssClass = DIV_FINEST;
                break;
            default:
                cssClass = DIV_DEFAULT;
                break;
        }

        return cssClass;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * Handler used to publish logs in text area
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class TextHandler extends Handler {

        protected final Publisher publisher;

        public TextHandler(Publisher publisher) {
            super();
            this.publisher = publisher;
            setFormatter(new LogFormatter());
        }

        @Override
        public void publish(LogRecord record) {
            if (record != null) {
                String message;
                try {
                    message = getFormatter().format(record);
                } catch (Exception ex) {
                    // We don't want to throw an exception here, but we
                    // report the exception to any registered ErrorManager.
                    reportError(null, ex, ErrorManager.FORMAT_FAILURE);
                    message = null;
                }
                publisher.publish(message, record.getLevel());
            } // end if (record != null)
        }

        @Override
        public boolean isLoggable(LogRecord record) {
            return (record != null);
        }

        @Override
        public void flush() {
            // nothing to do
        }

        @Override
        public void close() {
            // nothing to do
        }
    }

    /**
     * Formatter that does not add date at the beginning of formatted text.
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class LogFormatter extends Formatter {

        @Override
        public String format(LogRecord record) {
            StringBuilder sb = new StringBuilder();
            String message = formatMessage(record);
            sb.append(record.getLevel().getLocalizedName());
            sb.append(LEVEL_SEPARATOR);
            if (message == null) {
                sb.append(String.valueOf(message));
            } else if (isHtml()) {
                sb.append(message.replace(NEW_LINE, HTML_NEW_LINE));
            } else {
                sb.append(message);
            }
            if (record.getThrown() != null) {
                try {
                    if (isHtml()) {
                        sb.append(HTML_NEW_LINE).append(
                                ObjectUtils.printStackTrace(record.getThrown()).replace(NEW_LINE, HTML_NEW_LINE));
                    } else {
                        sb.append(NEW_LINE).append(ObjectUtils.printStackTrace(record.getThrown()));
                    }
                } catch (Exception ex) {
                    // nothing to do: ignore exception
                }
            }
            return sb.toString();
        }

    }

}
