/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.ui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

/**
 * A {@link BasicSplitPaneUI} that respects {@link JSplitPane} background color.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ColoredSplitPaneUI extends BasicSplitPaneUI {

    public ColoredSplitPaneUI() {
        super();
        setContinuousLayout(false);
        JPanel dividerPanel = new JPanel();
        dividerPanel.setOpaque(true);
        setNonContinuousLayoutDivider(dividerPanel);
    }

    /**
     * Does as if left/up button was clicked.
     */
    public void oneTouchExpandLeftOrUp() {
        ((ColoredSplitPaneDivider) divider).oneTouchExpandLeftOrUp();
    }

    /**
     * Does as if right/down button was clicked.
     */
    public void oneTouchExpandRightOrDown() {
        ((ColoredSplitPaneDivider) divider).oneTouchExpandRightOrDown();
    }

    /**
     * Creates a new {@link ColoredSplitPaneUI} instance.
     */
    public static ComponentUI createUI(JComponent x) {
        return new ColoredSplitPaneUI();
    }

    public void setDividerColor(Color dividerColor) {
        if (dividerColor != null) {
            nonContinuousLayoutDivider.setBackground(dividerColor);
            if (divider != null) {
                divider.setBackground(dividerColor);
            }
        }
    }

    @Override
    public BasicSplitPaneDivider createDefaultDivider() {
        ColoredSplitPaneDivider defaultDivider = new ColoredSplitPaneDivider(this);
        if (nonContinuousLayoutDivider != null) {
            defaultDivider.setBackground(nonContinuousLayoutDivider.getBackground());
        }
        return defaultDivider;
    }

    @Override
    public void paint(Graphics g, JComponent jc) {
        boolean opaque = splitPane.isOpaque();
        if (opaque != nonContinuousLayoutDivider.isOpaque()) {
            ((JComponent) nonContinuousLayoutDivider).setOpaque(opaque);
        }
        if ((divider != null) && (opaque != divider.isOpaque())) {
            ((ColoredSplitPaneDivider) divider).setOpaque(opaque);
        }
        super.paint(g, jc);
    }

}
