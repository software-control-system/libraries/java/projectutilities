/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.combo;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;

import fr.soleil.lib.project.swing.renderer.AdaptedFontDefaultListCellRenderer;
import fr.soleil.lib.project.swing.renderer.AdaptedFontListCellRenderer;
import fr.soleil.lib.project.swing.ui.OpaqueAdaptedComboBoxUI;

/**
 * A {@link JComboBox} that is really able to adapt its rendering according to its {@link ListCellRenderer}
 * 
 * @param <E> The type of the elements of this combo box
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DynamicRenderingComboBox<E> extends JComboBox<E> implements ActionListener {

    private static final long serialVersionUID = -2861814563801129644L;

    private Font font;
    private Color foreground;
    private String tooltipText;
    private boolean dynamicFont;
    private boolean dynamicForeground;
    private boolean dynamicTooltipText;

    public DynamicRenderingComboBox() {
        super();
        init();
    }

    public DynamicRenderingComboBox(ComboBoxModel<E> aModel) {
        super(aModel);
        init();
    }

    public DynamicRenderingComboBox(E[] items) {
        super(items);
        init();
    }

    public DynamicRenderingComboBox(Vector<E> items) {
        super(items);
        init();
    }

    /**
     * Method called by all constructor
     */
    protected void init() {
        dynamicFont = true;
        dynamicForeground = true;
        dynamicTooltipText = true;
        setRenderer(new AdaptedFontDefaultListCellRenderer(this));
        setUI(buildUI());
        addActionListener(this);
    }

    @Override
    public void updateUI() {
        setUI(buildUI());
        ListCellRenderer<?> renderer = getRenderer();
        if (renderer instanceof Component) {
            SwingUtilities.updateComponentTreeUI((Component) renderer);
        }
    }

    @Override
    public AdaptedFontListCellRenderer<? super E> getRenderer() {
        return getAdaptedFontListCellRenderer(super.getRenderer());
    }

    /**
     * Sets the renderer that paints the list items and the item selected from the list in the JComboBox field. The
     * renderer is used if the JComboBox is not editable. If it is editable, the editor is used to render and edit the
     * selected item. Only compatible with {@link AdaptedFontListCellRenderer}s.
     * 
     * @param aRenderer The renderer to set. Only compatible with {@link AdaptedFontListCellRenderer}s. If
     *            <code>aRenderer</code> is not an {@link AdaptedFontListCellRenderer}, this method will do nothing.
     */
    @Override
    public void setRenderer(ListCellRenderer<? super E> aRenderer) {
        if (aRenderer instanceof AdaptedFontListCellRenderer<?>) {
            super.setRenderer(aRenderer);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public E getSelectedItem() {
        return (E) super.getSelectedItem();
    }

    @Override
    public void setFont(Font font) {
        this.font = font;
        doSetFont(font);
        updateRendering();
    }

    @Override
    public void setForeground(Color fg) {
        this.foreground = fg;
        doSetForeground(fg);
        updateRendering();
    }

    @Override
    public void setToolTipText(String text) {
        this.tooltipText = text;
        doSetToolTipText(text);
        updateRendering();
    }

    @Override
    public void setOpaque(boolean isOpaque) {
        super.setOpaque(isOpaque);
        updateRendering();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ((e != null) && (e.getSource() == this)) {
            updateRendering();
        } else {
            super.actionPerformed(e);
        }
    }

    /**
     * Creates a new {@link OpaqueAdaptedComboBoxUI} for this {@link DynamicRenderingComboBox}
     * 
     * @return An {@link OpaqueAdaptedComboBoxUI}
     */
    protected OpaqueAdaptedComboBoxUI buildUI() {
        return new OpaqueAdaptedComboBoxUI();
    }

    /**
     * Returns whether displayed {@link Font} will be adapted according to {@link #getRenderer()} in
     * {@link #updateRendering()}
     * 
     * @return A <code>boolean</code>
     */
    public boolean isDynamicFont() {
        return dynamicFont;
    }

    /**
     * Sets whether displayed {@link Font} will be adapted according to {@link #getRenderer()} in
     * {@link #updateRendering()}
     * 
     * @param dynamicFont Whether to adapt font
     */
    public void setDynamicFont(boolean dynamicFont) {
        this.dynamicFont = dynamicFont;
    }

    /**
     * Returns whether displayed foreground {@link Color} will be adapted according to {@link #getRenderer()} in
     * {@link #updateRendering()}
     * 
     * @return A <code>boolean</code>
     */
    public boolean isDynamicForeground() {
        return dynamicForeground;
    }

    /**
     * Sets whether displayed foreground {@link Color} will be adapted according to {@link #getRenderer()} in
     * {@link #updateRendering()}
     * 
     * @param dynamicForeground Whether to adapt foreground {@link Color}
     */
    public void setDynamicForeground(boolean dynamicForeground) {
        this.dynamicForeground = dynamicForeground;
    }

    /**
     * Returns whether displayed tooltip text will be adapted according to {@link #getRenderer()} in
     * {@link #updateRendering()}
     * 
     * @return A <code>boolean</code>
     */
    public boolean isDynamicTooltipText() {
        return dynamicTooltipText;
    }

    /**
     * Sets whether displayed tooltip text will be adapted according to {@link #getRenderer()} in
     * {@link #updateRendering()}
     * 
     * @param dynamicTooltipText Whether to adapt tooltip text
     */
    public void setDynamicTooltipText(boolean dynamicTooltipText) {
        this.dynamicTooltipText = dynamicTooltipText;
    }

    /**
     * This method is a hack to get an {@link AdaptedFontListCellRenderer} from a {@link ListCellRenderer}.
     * It is mandatory due to java's bad way to manage generics.
     * 
     * @param renderer The {@link ListCellRenderer}
     * @return An {@link AdaptedFontListCellRenderer}
     */
    protected final <T> AdaptedFontListCellRenderer<T> getAdaptedFontListCellRenderer(ListCellRenderer<T> renderer) {
        AdaptedFontListCellRenderer<T> result;
        if (renderer instanceof AdaptedFontListCellRenderer<?>) {
            result = (AdaptedFontListCellRenderer<T>) renderer;
        } else {
            result = null;
        }
        return result;
    }

    /**
     * This method is a hack to get a {@link JComponent} from an {@link AdaptedFontListCellRenderer}, calling its
     * {@link AdaptedFontListCellRenderer#getListCellRendererComponent(JList, Object, int, boolean, boolean, boolean)}.
     * It is mandatory due to java's bad way to manage generics.
     * 
     * @param renderer The {@link AdaptedFontListCellRenderer}
     * @param model The {@link ComboBoxModel} mandatory to generate a new {@link JList}
     * @param selectedItem The selected item
     * @return A {@link JComponent}
     */
    protected final <T, U extends T> JComponent getBasicListCellRendererComponent(
            AdaptedFontListCellRenderer<T> renderer, ComboBoxModel<U> model, T selectedItem) {
        return renderer.getListCellRendererComponent(new JList<>(model), selectedItem, getSelectedIndex(), true, false,
                false);
    }

    /**
     * Forces this {@link DynamicRenderingComboBox} to update its rendering, according to its current
     * {@link AdaptedFontListCellRenderer}
     * 
     * @see #getRenderer()
     * @see #setRenderer(ListCellRenderer)
     * @see #isDynamicForeground()
     * @see #setDynamicForeground(boolean)
     * @see #isDynamicFont()
     * @see #setDynamicFont(boolean)
     * @see #isDynamicTooltipText()
     * @see #setDynamicTooltipText(boolean)
     */
    public void updateRendering() {
        AdaptedFontListCellRenderer<? super E> renderer = getRenderer();
        if (renderer != null) {
            JComponent comp = getBasicListCellRendererComponent(renderer, getModel(), getSelectedItem());
            if (isDynamicFont()) {
                doSetFont(comp.getFont());
            }
            if (isDynamicForeground()) {
                doSetForeground(comp.getForeground());
            }
            if (isDynamicTooltipText()) {
                doSetToolTipText(comp.getToolTipText());
            }
        }
        repaint();
    }

    /**
     * Returns the {@link Font} that was set through {@link #setFont(Font)}
     * 
     * @return A {@link Font}
     */
    public final Font getTheoreticalFont() {
        return font;
    }

    /**
     * Calls <code>super.setFont(font)</code>
     * 
     * @param font The {@link Font} to set
     */
    protected void doSetFont(Font font) {
        super.setFont(font);
    }

    /**
     * Returns the foreground {@link Color} that was set through {@link #setForeground(Color)}
     * 
     * @return A {@link Color}
     */
    public final Color getTheoreticalForeground() {
        return foreground;
    }

    /**
     * Calls <code>super.setForeground(fg)</code>
     * 
     * @param fg The foreground {@link Color} to set
     */
    protected void doSetForeground(Color fg) {
        super.setForeground(fg);
    }

    /**
     * Returns the tooltip text that was set through {@link #setToolTipText(String)}
     * 
     * @return A {@link String}
     */
    public final String getTheoreticalTooltipText() {
        return tooltipText;
    }

    /**
     * Calls <code>super.setToolTipText(text)</code>
     * 
     * @param text The tooltip text to set
     */
    protected void doSetToolTipText(String text) {
        super.setToolTipText(text);
    }

}
