/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.renderer;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JComponent;
import javax.swing.JList;

/**
 * An {@link AdaptedFontDefaultListCellRenderer} that uses a light selection background by default and that may use a
 * custom one.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LightSelectionBackgroundListCellRenderer extends AdaptedFontDefaultListCellRenderer {

    private static final long serialVersionUID = 4597902297924525421L;

    /**
     * The default selection background color.
     */
    public static final Color DEFAULT_SELECTION_BACKGROUND = new Color(230, 230, 255);

    protected Color selectionBackground;

    public LightSelectionBackgroundListCellRenderer(JComponent componentToRender) {
        super(componentToRender);
        this.selectionBackground = DEFAULT_SELECTION_BACKGROUND;
    }

    /**
     * Returns the background color used for selected values.
     * 
     * @return A {@link Color}.
     */
    public Color getSelectionBackground() {
        return selectionBackground;
    }

    /**
     * Sets the background color to use for selected values.
     * 
     * @param selectionBackground The background color to use for selected values.
     */
    public void setSelectionBackground(Color selectionBackground) {
        this.selectionBackground = (selectionBackground == null ? DEFAULT_SELECTION_BACKGROUND : selectionBackground);
    }

    /**
     * Starts applying some {@link Font}, foreground {@link Color} and tooltip text to a {@link JComponent}, knowing
     * rendering context, but first updates the selection background color of given {@link JList}.
     * 
     * @param comp The {@link JComponent}
     * @param font The {@link Font} to apply;
     * @param foreground The foreground {@link Color} to apply
     * @param tooltipText the tooltip text to apply
     * @param list The JList we're painting.
     * @param value The value returned by list.getModel().getElementAt(index).
     * @param index The cells index.
     * @param isSelected True if the specified cell was selected.
     * @param cellHasFocus True if the specified cell has the focus.
     * @param derive Whether to derive Font to adapt it, if {@link #isAutoAdaptFont(boolean)} is <code>true</code>.
     */
    @Override
    protected final void applyToComponent(JComponent comp, Font font, Color foreground, String tooltipText,
            JList<? extends Object> list, Object value, int index, boolean isSelected, boolean cellHasFocus,
            boolean derive) {
        // Hack to ensure displaying the expected selection color in collapsed JCombobox.
        // We do want to check for strict equality in this hack.
        if (list.getSelectionBackground() != selectionBackground) {
            list.setSelectionBackground(selectionBackground);
        }
        if (isSelected) {
            comp.setBackground(selectionBackground);
        }
        doApplyToComponent(comp, font, foreground, tooltipText, list, value, index, isSelected, cellHasFocus, derive);
    }

    /**
     * Applies some {@link Font}, foreground {@link Color} and tooltip text to a {@link JComponent}, knowing rendering
     * context
     * 
     * @param comp The {@link JComponent}
     * @param font The {@link Font} to apply;
     * @param foreground The foreground {@link Color} to apply
     * @param tooltipText the tooltip text to apply
     * @param list The JList we're painting.
     * @param value The value returned by list.getModel().getElementAt(index).
     * @param index The cells index.
     * @param isSelected True if the specified cell was selected.
     * @param cellHasFocus True if the specified cell has the focus.
     * @param derive Whether to derive Font to adapt it, if {@link #isAutoAdaptFont(boolean)} is <code>true</code>.
     */
    protected void doApplyToComponent(JComponent comp, Font font, Color foreground, String tooltipText,
            JList<? extends Object> list, Object value, int index, boolean isSelected, boolean cellHasFocus,
            boolean derive) {
        super.applyToComponent(comp, font, foreground, tooltipText, list, value, index, isSelected, cellHasFocus,
                derive);
    }

}
