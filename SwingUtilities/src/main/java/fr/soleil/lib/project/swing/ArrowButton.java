/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.plaf.ButtonUI;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.ColorUtils;

/**
 * An arrow button that also have auto repeat capabilities.
 */
public class ArrowButton extends JButton implements MouseListener {

    private static final long serialVersionUID = -1363309152147062372L;

    public static final Color DEFAULT_BACKGROUND = new Color(102, 102, 153);
    public static final int DEFAULT_SIZE = 5;
    protected static final double BRIGHTNESS_FACTOR = 0.3;
    protected static final double STATE_FACTOR = 0.2;

    /** UP arrow */
    public static final int UP = 1;
    /** Down arrow */
    public static final int DOWN = 2;
    /** Left arrow */
    public static final int LEFT = 3;
    /** Right arrow */
    public static final int RIGHT = 4;

    private int orientation;
    private Color lightColor;
    private Color darkColor;

    public ArrowButton() {
        this(UP);
    }

    // General constructor
    public ArrowButton(int orientation) {
        super();
        this.orientation = orientation;
        setOpaque(true);
        setBorder(null);
        setBorderPainted(false);
        addMouseListener(this);
        setPreferredSize(null);
        setBackground(DEFAULT_BACKGROUND);
    }

    /**
     * This method will always return <code>false</code>, in order to avoid strange behaviors.
     * <p>
     * If you really want to know whether the arrow is opaque, call {@link #isArrowOpaque()}.
     * </p>
     * 
     * @return <code>false</code>.
     */
    @Override
    public boolean isOpaque() {
        // If you want to understand this, run main() method, and try to move cursor over the various button,
        // and click on them. You will see a different (undesired) behavior if you deactivate this method...
        return false;
    }

    /**
     * Returns whether the arrow is opaque
     * 
     * @return A <code>boolean</code>
     */
    public boolean isArrowOpaque() {
        return super.isOpaque();
    }

    @Override
    public void updateUI() {
        // do not manage ui
    }

    @Override
    public void setUI(ButtonUI ui) {
        // do not manage ui
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (isEnabled()) {
            fireActionPerformed(
                    new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null, System.currentTimeMillis(), 0));
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (isEnabled()) {
            getModel().setRollover(true);
            repaint();
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (getModel().isRollover()) {
            getModel().setRollover(false);
        }
        if (isEnabled()) {
            repaint();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (isEnabled()) {
            getModel().setPressed(true);
            repaint();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (isEnabled()) {
            getModel().setPressed(false);
            repaint();
        }
    }

    @Override
    public void setPreferredSize(Dimension preferredSize) {
        if (preferredSize == null) {
            super.setPreferredSize(new Dimension(DEFAULT_SIZE, DEFAULT_SIZE));
        } else {
            super.setPreferredSize(preferredSize);
        }
    }

    /**
     * Gets the orientation
     * 
     * @return an <code>int</code>
     * @see #UP
     * @see #DOWN
     * @see #LEFT
     * @see #RIGHT
     */
    public int getOrientation() {
        return orientation;
    }

    /**
     * Sets the orientation.
     * 
     * @param orientation The orientation to set.
     * @see #UP
     * @see #DOWN
     * @see #LEFT
     * @see #RIGHT
     */
    public void setOrientation(int orientation) {
        if (orientation != this.orientation) {
            this.orientation = orientation;
            repaint();
        }
    }

    /**
     * Returns the dark {@link Color} used to draw arrow border.
     * 
     * @return A {@link Color}.
     */
    protected Color getDarkColor() {
        return darkColor;
    }

    /**
     * Returns the light {@link Color} used to draw arrow border.
     * 
     * @return A {@link Color}.
     */
    protected Color getLightColor() {
        return lightColor;
    }

    /**
     * Returns whether mouse is over this {@link ArrowButton}
     * 
     * @return Whether mouse is over this {@link ArrowButton}
     */
    protected boolean isMouseOver() {
        return getModel().isRollover();
    }

    @Override
    public void setBackground(final Color c) {
        Color bg = (c == null ? DEFAULT_BACKGROUND : c);
        if (!ObjectUtils.sameObject(bg, getBackground())) {
            super.setBackground(bg);
            lightColor = ColorUtils.brighter(bg, BRIGHTNESS_FACTOR);
            darkColor = ColorUtils.darker(bg, BRIGHTNESS_FACTOR);
        }
    }

    // Paint the component
    @Override
    protected void paintComponent(Graphics g) {
        Color c = g.getColor();
        int w = getWidth() - 1;
        int h = getHeight() - 1;
        if (w < 0) {
            w = 0;
        }
        if (h < 0) {
            h = 0;
        }
        int m;
        // Draw background
        int pointx[] = new int[3];
        int pointy[] = new int[3];
        Color fillColor = getBackground();
        Color lightBorderColor = getLightColor();
        Color darkBorderColor = getDarkColor();
        boolean pressed = getModel().isPressed();
        if (isEnabled()) {
            if (pressed) {
                fillColor = ColorUtils.darker(fillColor, STATE_FACTOR);
            } else if (isMouseOver()) {
                fillColor = ColorUtils.brighter(fillColor, STATE_FACTOR);
                lightBorderColor = ColorUtils.brighter(lightBorderColor, STATE_FACTOR);
                darkBorderColor = ColorUtils.brighter(darkBorderColor, STATE_FACTOR);
            }
        } else {
            fillColor = fillColor.brighter();
            lightBorderColor = lightBorderColor.brighter();
            darkBorderColor = darkBorderColor.brighter();
        }
        int orientation = getOrientation();
        switch (orientation) {
            case UP:
                m = w / 2;
                pointx[0] = 0;
                pointy[0] = h;
                pointx[1] = m;
                pointy[1] = 0;
                pointx[2] = w;
                pointy[2] = h;
                if (isArrowOpaque()) {
                    g.setColor(fillColor);
                    g.fillPolygon(pointx, pointy, 3);
                }
                // Draw border
                if (!pressed) {
                    g.setColor(lightBorderColor);
                    g.drawLine(0, h, m, 0);
                    g.setColor(darkBorderColor);
                    g.drawLine(m, 0, w, h);
                    g.drawLine(w, h, 0, h);
                } else {
                    g.setColor(darkBorderColor);
                    g.drawLine(0, h, m, 0);
                    g.setColor(lightBorderColor);
                    g.drawLine(m, 0, w, h);
                    g.drawLine(w, h, 0, h);
                }
                break;
            case DOWN:
                m = w / 2;
                pointx[0] = 0;
                pointy[0] = 0;
                pointx[1] = m;
                pointy[1] = h;
                pointx[2] = w;
                pointy[2] = 0;
                if (isArrowOpaque()) {
                    g.setColor(fillColor);
                    g.fillPolygon(pointx, pointy, 3);
                }
                // Draw border
                if (!pressed) {
                    g.setColor(lightBorderColor);
                    g.drawLine(0, 0, m, h);
                    g.setColor(darkBorderColor);
                    g.drawLine(m, h, w, 0);
                    g.drawLine(w, 0, 0, 0);
                } else {
                    g.setColor(darkBorderColor);
                    g.drawLine(0, 0, m, h);
                    g.setColor(lightBorderColor);
                    g.drawLine(m, h, w, 0);
                    g.drawLine(w, 0, 0, 0);
                }
                break;
            case LEFT:
                m = h / 2;
                pointx[0] = 0;
                pointy[0] = m;
                pointx[1] = w;
                pointy[1] = 0;
                pointx[2] = w;
                pointy[2] = h;
                if (isArrowOpaque()) {
                    g.setColor(fillColor);
                    g.fillPolygon(pointx, pointy, 3);
                }
                // Draw border
                if (!pressed) {
                    g.setColor(lightBorderColor);
                    g.drawLine(0, m, w, 0);
                    g.setColor(darkBorderColor);
                    g.drawLine(w, 0, w, h);
                    g.drawLine(w, h, 0, m);
                } else {
                    g.setColor(darkBorderColor);
                    g.drawLine(0, m, w, 0);
                    g.setColor(lightBorderColor);
                    g.drawLine(w, 0, w, h);
                    g.drawLine(w, h, 0, m);
                }
                break;
            case RIGHT:
                m = h / 2;
                pointx[0] = 0;
                pointy[0] = 0;
                pointx[1] = w;
                pointy[1] = m;
                pointx[2] = 0;
                pointy[2] = h;
                if (isArrowOpaque()) {
                    g.setColor(fillColor);
                    g.fillPolygon(pointx, pointy, 3);
                }
                // Draw border
                if (!pressed) {
                    g.setColor(lightBorderColor);
                    g.drawLine(0, 0, w, m);
                    g.setColor(darkBorderColor);
                    g.drawLine(w, m, 0, h);
                    g.drawLine(0, h, 0, 0);
                } else {
                    g.setColor(darkBorderColor);
                    g.drawLine(0, 0, w, m);
                    g.setColor(lightBorderColor);
                    g.drawLine(w, m, 0, h);
                    g.drawLine(0, h, 0, 0);
                }
                break;
            default:
                break;
        }
        g.setColor(c);
    }

    public static void main(String[] args) {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        Dimension size = new Dimension(30, 30);
        ArrowButton upbutton = new ArrowButton(UP);
        upbutton.setPreferredSize(size);
        panel.add(upbutton);
        ArrowButton downbutton = new ArrowButton(DOWN);
        downbutton.setPreferredSize(size);
        panel.add(downbutton);
        ArrowButton leftbutton = new ArrowButton(LEFT);
        leftbutton.setPreferredSize(size);
        panel.add(leftbutton);
        ArrowButton rightbutton = new ArrowButton(RIGHT);
        rightbutton.setPreferredSize(size);
        panel.add(rightbutton);
        JFrame testFrame = new JFrame(ArrowButton.class.getSimpleName() + " test");
        testFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        testFrame.setContentPane(panel);
        testFrame.pack();
        testFrame.setSize(300, testFrame.getHeight());
        testFrame.setVisible(true);
    }
}
