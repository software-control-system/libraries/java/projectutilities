/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link JAnchoredDialog} that can display an error message with access to error details.
 * 
 * @author Huriez
 */
public class ErrorDialog extends JAnchoredDialog implements ActionListener {

    private static final long serialVersionUID = 4827026246089012263L;

    public static final String SHOW_DETAILS_STRING = "Details >>";
    public static final String HIDE_DETAILS_STRING = "Details <<";
    private static final Insets BUTTON_INSETS = new Insets(2, 0, 2, 0);

    private JPanel errorPanel;

    private JLabel errorIcon;
    private final List<JLabel> errorMessageLabels = new ArrayList<JLabel>();

    private JTextArea errorDetailsArea;
    private JScrollPane scrollDetails;

    private JButton closeButton;
    private JButton detailsButton;

    private boolean detailsShown;

    private final String[] errorDetails;
    private final String errorMessage;

    /**
     * Constructs this {@link ErrorDialog} with an error to display
     * 
     * @param owner This dialog's owner
     * @param title This dialog's title
     * @param modal Whether this dialog should be modal
     * @param error The error to display
     */
    public ErrorDialog(Window owner, String title, boolean modal, Throwable error) {
        super((owner == null ? new JFrame() : owner), title, modal, null);
        if (error == null) {
            errorMessage = ObjectUtils.EMPTY_STRING;
            errorDetails = new String[0];
        } else {
            errorMessage = error.toString();
            List<String> details = buildDetail(error, false, null);
            errorDetails = details.toArray(new String[details.size()]);
        }
        detailsShown = true;
        initComponents();
    }

    /**
     * Constructs this {@link ErrorDialog} with a custom error to display
     * 
     * @param owner This dialog's owner
     * @param title This dialog's title
     * @param modal Whether this dialog should be modal
     * @param message The custom error message
     * @param details The custom error stack trace
     */
    public ErrorDialog(Window owner, String title, String message, boolean modal, StackTraceElement[] details) {
        super((owner == null ? new JFrame() : owner), title, modal, null);
        errorMessage = (message == null ? ObjectUtils.EMPTY_STRING : message);
        if (details == null) {
            errorDetails = new String[0];
        } else {
            errorDetails = new String[details.length];
            for (int i = 0; i < details.length; i++) {
                errorDetails[i] = buildDetail(details[i]);
            }
        }
        detailsShown = true;
        initComponents();
    }

    private List<String> buildDetail(Throwable error, boolean addError, List<String> previousList) {
        if (previousList == null) {
            previousList = new ArrayList<String>();
        }
        if (error != null) {
            if (addError) {
                previousList.add("caused by " + error);
            }
            if (error.getStackTrace() != null) {
                for (StackTraceElement element : error.getStackTrace()) {
                    previousList.add(buildDetail(element));
                }
            }
            buildDetail(error.getCause(), true, previousList);
        }
        return previousList;
    }

    private String buildDetail(StackTraceElement element) {
        return (element == null ? ObjectUtils.EMPTY_STRING : ("  at " + element));
    }

    private void initComponents() {
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        BorderLayout errorLayout = new BorderLayout();
        errorLayout.setHgap(5);
        errorLayout.setVgap(10);
        errorPanel = new JPanel(errorLayout);

        errorDetailsArea = new JTextArea();
        errorDetailsArea.setOpaque(false);
        StringBuilder detailsBuilder = new StringBuilder();
        for (String errorDetail : errorDetails) {
            detailsBuilder.append(errorDetail).append(ObjectUtils.NEW_LINE);
        }
        if (detailsBuilder.length() > 0) {
            detailsBuilder.deleteCharAt(detailsBuilder.length() - 1);
        }
        errorDetailsArea.setText(detailsBuilder.toString());
        errorDetailsArea.setEditable(false);
        errorDetailsArea.setForeground(Color.RED);

        scrollDetails = new JScrollPane(errorDetailsArea);

        errorIcon = new JLabel(UIManager.getIcon("OptionPane.errorIcon"));

        StringTokenizer tokens = new StringTokenizer(errorMessage, ObjectUtils.NEW_LINE);
        JPanel labelPanel = new JPanel(new GridLayout(tokens.countTokens(), 1));
        while (tokens.hasMoreTokens()) {
            JLabel errorMessageLabel = new JLabel(tokens.nextToken());
            errorMessageLabel.setHorizontalAlignment(JLabel.LEFT);
            errorMessageLabel.setVerticalAlignment(JLabel.TOP);
            errorMessageLabels.add(errorMessageLabel);
            labelPanel.add(errorMessageLabel);
        }

        closeButton = new JButton("Close");
        closeButton.setMargin(BUTTON_INSETS);
        closeButton.setToolTipText("Close the dialog box");
        closeButton.addActionListener(this);
        closeButton.setActionCommand("close");

        detailsButton = new JButton(SHOW_DETAILS_STRING);
        detailsButton.setMargin(BUTTON_INSETS);
        detailsButton.setToolTipText("Show or hide error details");
        detailsButton.addActionListener(this);
        detailsButton.setActionCommand("details");

        BorderLayout buttonLayout = new BorderLayout();
        buttonLayout.setVgap(5);
        JPanel buttonPanel = new JPanel(buttonLayout);
        buttonPanel.add(closeButton, BorderLayout.NORTH);
        buttonPanel.add(detailsButton, BorderLayout.SOUTH);

        errorPanel.add(errorIcon, BorderLayout.WEST);
        errorPanel.add(labelPanel, BorderLayout.CENTER);
        errorPanel.add(buttonPanel, BorderLayout.EAST);
        errorPanel.add(scrollDetails, BorderLayout.SOUTH);

        setContentPane(errorPanel);

        updateDetails();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == closeButton) {
            processWindowEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        } else if (e.getSource() == detailsButton) {
            updateDetails();
        }
    }

    private void updateDetails() {
        detailsShown = !detailsShown;
        if (detailsShown) {
            showDetails();
        } else {
            hideDetails();
        }
    }

    private void hideDetails() {
        detailsButton.setText(SHOW_DETAILS_STRING);
        errorPanel.remove(scrollDetails);
        errorPanel.revalidate();
        pack();
    }

    private void showDetails() {
        detailsButton.setText(HIDE_DETAILS_STRING);
        errorPanel.add(scrollDetails, BorderLayout.SOUTH);
        errorPanel.revalidate();
        pack();
    }

    public static void main(String[] args) {
        ErrorDialog dialog = new ErrorDialog(null, "Test Error", false,
                new Exception("This is an error detail", new Throwable("This is an error cause")));
        dialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        dialog.setVisible(true);
    }

}
