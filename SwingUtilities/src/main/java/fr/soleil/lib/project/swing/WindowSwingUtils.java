/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.Component;
import java.awt.Window;

import javax.swing.JPopupMenu;

import fr.soleil.lib.project.awt.WindowUtils;

/**
 * A class used to interact with {@link Window}s, knowing some java swing particular cases.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class WindowSwingUtils extends WindowUtils {

    private static final WindowSwingUtils INSTANCE = new WindowSwingUtils();

    @Override
    public Component getParent(Component component) {
        Component parent;
        if (component == null) {
            parent = null;
        } else {
            parent = component.getParent();
            if (parent == null) {
                if (component instanceof JPopupMenu) {
                    parent = ((JPopupMenu) component).getInvoker();
                }
            }
        }
        return parent;
    }

    /**
     * Returns the parent {@link Window} of the given {@link Component}.
     * 
     * @param component Child {@link Component}.
     * @return The expected {@link Window}.
     */
    public static Window getWindowForComponent(Component component) {
        return WindowUtils.getWindowForComponent(component, INSTANCE);
    }

}
