/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.listener;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import fr.soleil.lib.project.swing.renderer.MultiLinesCellRenderer;

/**
 * A {@link TableModelListener} that resets a {@link JTable}'s row height to 1 every time a change occurs.
 * Combined with a {@link MultiLinesCellRenderer}, this allows to have a {@link JTable} that renders multiple lines in a
 * cell and dynamically adapts its row height to each cell.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class TableRowHeightResetter implements TableModelListener {

    protected final JTable table;

    public TableRowHeightResetter(JTable table) {
        super();
        this.table = table;
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        table.setRowHeight(1);
    }

}