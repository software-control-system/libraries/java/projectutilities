/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.border;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Path2D;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.ref.WeakReference;

import javax.swing.ButtonModel;
import javax.swing.JComponent;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.ArrowButton;

/**
 * A {@link ColoredLineTitledBorder} with a button to fold/unfold some content.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class FoldableBorder extends ColoredLineTitledBorder implements MouseListener, MouseMotionListener {

    private static final long serialVersionUID = 5793692903373431423L;

    protected static final Dimension ZERO_SIZE = new Dimension(0, 0);

    /** Property key when bolder folded aspect changed */
    public static final String BORDER_FOLDED = "borderFolded";

    protected final PropertyChangeSupport changeSupport;
    protected final ArrowButton arrowButton;
    protected boolean titleColorAsButtonColor;
    protected int buttonPosition;

    public FoldableBorder() {
        this(ObjectUtils.EMPTY_STRING, LEADING, DEFAULT_POSITION, null, null);
    }

    public FoldableBorder(String title) {
        this(title, LEADING, DEFAULT_POSITION, null, null);
    }

    public FoldableBorder(String title, int titleJustification, int titlePosition) {
        this(title, titleJustification, titlePosition, null, null);
    }

    public FoldableBorder(String title, int titleJustification, int titlePosition, Font titleFont) {
        this(title, titleJustification, titlePosition, titleFont, null);
    }

    public FoldableBorder(String title, int titleJustification, int titlePosition, Font titleFont, Color titleColor) {
        super(title, titleJustification, titlePosition, titleFont, titleColor);
        changeSupport = new PropertyChangeSupport(this);
        arrowButton = new ArrowButton(ArrowButton.DOWN);
        buttonPosition = RIGHT;
        updateButtonSize();
    }

    protected void updateButtonSize() {
        arrowButton.setSize(titleFont.getSize(), titleFont.getSize());
    }

    protected Dimension getLabelSize(String title) {
        return title == null || title.isEmpty() ? ZERO_SIZE : label.getPreferredSize();
    }

    protected int getRefHeight(Dimension size) {
        return Math.max(size.height, arrowButton.getHeight());
    }

    protected int getRefHeight(String title) {
        return getRefHeight(getLabelSize(title));
    }

    protected void updateButtonColor(Color titleColor, boolean titleColorAsButtonColor) {
        if (titleColorAsButtonColor) {
            arrowButton.setForeground(titleColor == null ? ArrowButton.DEFAULT_BACKGROUND : titleColor);
        } else {
            arrowButton.setForeground(ArrowButton.DEFAULT_BACKGROUND);
        }
    }

    /**
     * Returns whether title color will be applied to button too.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isTitleColorAsButtonColor() {
        return titleColorAsButtonColor;
    }

    /**
     * Sets whether title color should be applied to button too.
     * 
     * @param titleColorAsButtonColor Whether title color should be applied to button too.
     */
    public void setTitleColorAsButtonColor(boolean titleColorAsButtonColor) {
        this.titleColorAsButtonColor = titleColorAsButtonColor;
        updateButtonColor(getTitleColor(), titleColorAsButtonColor);
    }

    @Override
    public void setTitleColor(Color titleColor) {
        updateButtonColor(titleColor, isTitleColorAsButtonColor());
        super.setTitleColor(titleColor);
    }

    /**
     * Returns the button position relative to title.
     * 
     * @return An <code>int</code>.
     *         <p>
     *         Can be any of these:
     *         <ul>
     *         <li>{@link ColoredLineTitledBorder#LEFT} or {@link ColoredLineTitledBorder#LEADING}: the button
     *         is placed before the title, on its left.</li>
     *         <li>{@link ColoredLineTitledBorder#RIGHT} or {@link ColoredLineTitledBorder#TRAILING}: the button
     *         is placed after the title, on its right.</li>
     *         </ul>
     *         </p>
     */
    public int getButtonPosition() {
        return buttonPosition;
    }

    /**
     * Sets the button position.
     * 
     * @param buttonPosition The button position to set.
     *            <p>
     *            Can be any of these:
     *            <ul>
     *            <li>{@link ColoredLineTitledBorder#LEFT} or {@link ColoredLineTitledBorder#LEADING}: the button
     *            will be placed before the title, on its left.</li>
     *            <li>{@link ColoredLineTitledBorder#RIGHT} or {@link ColoredLineTitledBorder#TRAILING}: the button
     *            will be placed after the title, on its right.</li>
     *            </ul>
     *            </p>
     */
    public void setButtonPosition(int buttonPosition) {
        if (buttonPosition != this.buttonPosition) {
            switch (buttonPosition) {
                case LEFT:
                case RIGHT:
                case LEADING:
                case TRAILING:
                    this.buttonPosition = buttonPosition;
                    updateButtonOrientation(isFolded());
                    repaintLastComponent();
                    break;
            }
        }
    }

    /**
     * Reinitialize the insets parameter with this Border's current Insets.
     * 
     * @param c the component for which this border insets value applies
     * @param insets the object to be reinitialized
     */
    @Override
    public Insets getBorderInsets(Component c, Insets insets) {
        insets = border.getBorderInsets(c, insets);

        String title = getTitle();
        updateLabel(c);
        int refHeight = getRefHeight(title);
        switch (getPosition()) {
            case ABOVE_TOP:
                insets.top += refHeight - EDGE_SPACING;
                break;
            case TOP: {
                if (insets.top < refHeight) {
                    insets.top = refHeight - EDGE_SPACING;
                }
                break;
            }
            case BELOW_TOP:
                insets.top += refHeight;
                break;
            case ABOVE_BOTTOM:
                insets.bottom += refHeight;
                break;
            case BOTTOM: {
                if (insets.bottom < refHeight) {
                    insets.bottom = refHeight - EDGE_SPACING;
                }
                break;
            }
            case BELOW_BOTTOM:
                insets.bottom += refHeight - EDGE_SPACING;
                break;
        }
        insets.top += EDGE_SPACING + TEXT_SPACING;
        insets.left += EDGE_SPACING + TEXT_SPACING;
        insets.right += EDGE_SPACING + TEXT_SPACING;
        insets.bottom += EDGE_SPACING + TEXT_SPACING;
        return insets;
    }

    @Override
    public int getBaseline(Component c, int width, int height) {
        if (c == null) {
            throw new NullPointerException("Component can't be null");
        }
        if (width < 0) {
            throw new IllegalArgumentException("Width must be \u2265 0");
        }
        if (height < 0) {
            throw new IllegalArgumentException("Height must be \\u2265 0");
        }
        String title = getTitle();
        int baseline;
        int edge = EDGE_SPACING;
        updateLabel(c);
        Dimension size = getLabelSize(title);
        int refHeight = getRefHeight(size);
        Insets insets = border.getBorderInsets(c, new Insets(0, 0, 0, 0));
        baseline = label.getBaseline(size.width == 0 ? arrowButton.getWidth() : size.width, refHeight);
        switch (getPosition()) {
            case TOP:
                // TOP: label is vertically centered with top line
                insets.top = edge + (insets.top - refHeight) / 2;
                baseline = (insets.top < edge) ? baseline : baseline + insets.top;
                break;
            case BELOW_TOP:
                // BELOW_TOP: label is below the top line
                baseline += insets.top + edge;
                break;
            case ABOVE_BOTTOM:
                // ABOVE_BOTTOM: label is above the bottom line.
                baseline += height - refHeight - insets.bottom - edge;
                break;
            case BOTTOM:
                // BOTTOM: label is vertically centered with bottom line
                insets.bottom = edge + (insets.bottom - refHeight) / 2;
                baseline = (insets.bottom < edge) ? baseline + height - refHeight
                        : baseline + height - refHeight + insets.bottom;
                break;
            case BELOW_BOTTOM:
                // BELOW_BOTTOM: label is below the bottom line.
                baseline += height - refHeight;
                break;
            case ABOVE_TOP:
            default:
                // nothing to do: baseline is label's baseline (label is vertically at 0 position).
                break;
        }
        return baseline;
    }

    @Override
    public Component.BaselineResizeBehavior getBaselineResizeBehavior(Component c) {
        Component.BaselineResizeBehavior behavior;
        switch (getPosition()) {
            case ABOVE_TOP:
            case TOP:
            case BELOW_TOP:
                // When label is at the top, its baseline is relative to y-origin.
                behavior = Component.BaselineResizeBehavior.CONSTANT_ASCENT;
                break;
            case ABOVE_BOTTOM:
            case BOTTOM:
            case BELOW_BOTTOM:
                // When label is at the bottom, its baseline is relative to the height.
                behavior = JComponent.BaselineResizeBehavior.CONSTANT_DESCENT;
                break;
            default:
                // Other cases.
                behavior = Component.BaselineResizeBehavior.OTHER;
                break;
        }
        return behavior;
    }

    /**
     * 
     * @param c the component for which this border is being painted
     * @param x the x position of the painted border
     * @param y the y position of the painted border
     * @param width the width of the painted border
     * @param height the height of the painted border
     * @return An <code>int[]</code>: <code>{borderX, borderY, borderWidth, borderHeight, labelX, labelY, labelWith,
     *         labelHeight, buttonX, buttonY, buttonWidth, buttonHeight, position}</code>, where
     *         <code>position</code> is the result of {@link #getPosition()}.
     */
    @Override
    protected int[] computeBorderBounds(Component c, int x, int y, int width, int height) {
        updateLabel(c);
        Dimension size = label.getPreferredSize();
        Insets insets = border.getBorderInsets(c, new Insets(0, 0, 0, 0));

        int borderX, borderY, borderWidth, borderHeight;
        int labelX, labelY, labelWidth, labelHeight;
        int buttonX, buttonY, buttonWidth, buttonHeight;

        borderX = x + EDGE_SPACING;
        borderY = y + EDGE_SPACING;
        borderWidth = width - EDGE_SPACING - EDGE_SPACING;
        borderHeight = height - EDGE_SPACING - EDGE_SPACING;

        labelY = y;
        labelHeight = size.height;
        int refHeight = getRefHeight(size);
        int position = getPosition();
        switch (position) {
            case ABOVE_TOP:
                insets.left = 0;
                insets.right = 0;
                borderY += refHeight - EDGE_SPACING;
                borderHeight -= refHeight - EDGE_SPACING;
                break;
            case TOP:
                insets.top = EDGE_SPACING + insets.top / 2 - refHeight / 2;
                if (insets.top < EDGE_SPACING) {
                    borderY -= insets.top;
                    borderHeight += insets.top;
                } else {
                    labelY += insets.top;
                }
                break;
            case BELOW_TOP:
                labelY += insets.top + EDGE_SPACING;
                break;
            case ABOVE_BOTTOM:
                labelY += height - refHeight - insets.bottom - EDGE_SPACING;
                break;
            case BOTTOM:
                labelY += height - refHeight;
                insets.bottom = EDGE_SPACING + (insets.bottom - refHeight) / 2;
                if (insets.bottom < EDGE_SPACING) {
                    borderHeight += insets.bottom;
                } else {
                    labelY -= insets.bottom;
                }
                break;
            case BELOW_BOTTOM:
                insets.left = 0;
                insets.right = 0;
                labelY += height - refHeight;
                borderHeight -= refHeight - EDGE_SPACING;
                break;
        }
        insets.left += EDGE_SPACING + TEXT_HORIZONTAL_INSETS;
        insets.right += EDGE_SPACING + TEXT_HORIZONTAL_INSETS;

        labelX = x;
        labelWidth = width - insets.left - insets.right;
        if (labelWidth > size.width) {
            labelWidth = size.width;
        }
        if (labelWidth < 0) {
            labelWidth = 0;
        }
        switch (getJustification(c)) {
            case LEFT:
                labelX += insets.left;
                break;
            case RIGHT:
                labelX += width - insets.right - labelWidth;
                break;
            case CENTER:
                labelX += (width - labelWidth) / 2;
                break;
        }
        if (labelX < 0) {
            labelX = 0;
        }
        int buttonPosition = getButtonPosition();
        buttonWidth = arrowButton.getWidth();
        buttonHeight = arrowButton.getHeight();
        if (buttonHeight < labelHeight) {
            buttonY = labelY + (labelHeight - buttonHeight) / 2;
        } else {
            if (buttonHeight > labelHeight && labelHeight > 0) {
                buttonHeight = labelHeight;
                buttonWidth = buttonHeight;
                arrowButton.setSize(buttonWidth, buttonHeight);
            }
            buttonY = labelY;
        }
        if (buttonPosition == TRAILING || buttonPosition == RIGHT) {
            buttonX = labelX;
            if (labelWidth > 0) {
                buttonX += labelWidth + TEXT_SPACING;
            }
        } else {
            buttonX = labelX;
            labelX = buttonX + buttonWidth;
            if (labelWidth > 0) {
                labelX += TEXT_SPACING;
            }
        }
        return new int[] { borderX, borderY, borderWidth, borderHeight, labelX, labelY, labelWidth, labelHeight,
                buttonX, buttonY, buttonWidth, buttonHeight, position };
    }

    /**
     * Returns the {@link Rectangle} that represents the title label bounds for given {@link Component} at given
     * coordinates.
     * 
     * @param c The {@link Component}.
     * @param x The border start abscissa.
     * @param y The border start ordinate.
     * @param width The border width.
     * @param height The border height.
     * @return A {@link Rectangle}.
     */
    @Override
    public Rectangle getLabelBounds(Component c, int x, int y, int width, int height) {
        Rectangle labelBounds;
        int[] bounds = computeBorderBounds(c, x, y, width, height);
        labelBounds = new Rectangle(bounds[4], bounds[5], bounds[6], bounds[7]);
        return labelBounds;
    }

    /**
     * Returns the {@link Rectangle} that represents the fold button bounds for given {@link Component} at given
     * coordinates.
     * 
     * @param c The {@link Component}.
     * @param x The border start abscissa.
     * @param y The border start ordinate.
     * @param width The border width.
     * @param height The border height.
     * @return A {@link Rectangle}.
     */
    public Rectangle getButtonBounds(Component c, int x, int y, int width, int height) {
        Rectangle buttonBounds;
        int[] bounds = computeBorderBounds(c, x, y, width, height);
        buttonBounds = new Rectangle(bounds[8], bounds[9], bounds[10], bounds[11]);
        return buttonBounds;
    }

    protected void updateButtonOrientation(boolean folded) {
        int orientation, ref;
        if (folded) {
            ref = getJustification(getLastComponent());
            if (ref == CENTER) {
                ref = getButtonPosition();
            }
            if (ref == LEFT || ref == LEADING) {
                orientation = ArrowButton.RIGHT;
            } else {
                orientation = ArrowButton.LEFT;
            }
        } else {
            ref = getPosition();
            if (ref == ABOVE_BOTTOM || ref == BOTTOM || ref == BELOW_BOTTOM) {
                orientation = ArrowButton.UP;
            } else {
                orientation = ArrowButton.DOWN;
            }
        }
        if (arrowButton != null) {
            arrowButton.setOrientation(orientation);
        }
    }

    public boolean isFolded() {
        return (arrowButton != null) && (arrowButton.getOrientation() == ArrowButton.LEFT
                || arrowButton.getOrientation() == ArrowButton.RIGHT);
    }

    public void setFolded(boolean folded) {
        boolean formerFolded = isFolded();
        if (formerFolded != folded) {
            updateButtonOrientation(folded);
            changeSupport.firePropertyChange(BORDER_FOLDED, formerFolded, folded);
            repaintLastComponent();
        }
    }

    protected boolean isButtonEvent(MouseEvent e) {
        boolean buttonEvent;
        Component c = e.getComponent();
        if (c == null) {
            buttonEvent = false;
        } else {
            Rectangle bounds = getButtonBounds(c, 0, 0, c.getWidth(), c.getHeight());
            int buttonX = bounds.x, buttonY = bounds.y, buttonWidth = bounds.width, buttonHeight = bounds.height;
            int x = e.getX(), y = e.getY();
            buttonEvent = x >= buttonX && x < buttonX + buttonWidth && y >= buttonY && y < buttonY + buttonHeight;
        }
        return buttonEvent;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(propertyName, listener);
    }

    protected void setButtonPressed(boolean pressed) {
        ButtonModel model = arrowButton.getModel();
        if ((model != null) && (model.isPressed() != pressed)) {
            model.setPressed(pressed);
            repaintLastComponent();
        }
    }

    protected void setButtonHover(boolean hover) {
        ButtonModel model = arrowButton.getModel();
        if ((model != null) && (model.isRollover() != hover)) {
            model.setRollover(hover);
            repaintLastComponent();
        }
    }

    @Override
    public void setTitlePosition(int titlePosition) {
        super.setTitlePosition(titlePosition);
        updateButtonOrientation(isFolded());
        repaintLastComponent();
    }

    @Override
    public void setTitleJustification(int titleJustification) {
        super.setTitleJustification(titleJustification);
        updateButtonOrientation(isFolded());
        repaintLastComponent();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (isButtonEvent(e) && arrowButton.isEnabled()) {
            setFolded(!isFolded());
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (isButtonEvent(e) && arrowButton.isEnabled()) {
            setButtonPressed(true);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        setButtonPressed(false);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (isButtonEvent(e) && arrowButton.isEnabled()) {
            setButtonHover(true);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        setButtonPressed(false);
        setButtonHover(false);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        boolean button = isButtonEvent(e) && arrowButton.isEnabled();
        setButtonPressed(button);
        setButtonHover(button);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        boolean button = isButtonEvent(e) && arrowButton.isEnabled();
        setButtonHover(button);
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        arrowButton.setEnabled(c == null ? true : c.isEnabled());
        int[] bounds = computeBorderBounds(c, x, y, width, height);
        int borderX = bounds[0], borderY = bounds[1], borderWidth = bounds[2], borderHeight = bounds[3];
        int labelX = bounds[4], labelY = bounds[5], labelWidth = bounds[6], labelHeight = bounds[7];
        int buttonX = bounds[8], buttonY = bounds[9], buttonWidth = bounds[10], buttonHeight = bounds[11];
        int position = getPosition();
        int refHeight = Math.max(labelHeight, buttonHeight);
        if (border != null) {
            if ((position != TOP) && (position != BOTTOM)) {
                border.paintBorder(c, g, borderX, borderY, borderWidth, borderHeight);
            } else {
                Graphics g2 = g.create();
                if (g2 instanceof Graphics2D) {
                    int minX, maxX, lastWidth;
                    if (labelX < buttonX) {
                        minX = labelX;
                        maxX = buttonX;
                        lastWidth = buttonWidth;
                    } else {
                        minX = buttonX;
                        maxX = labelX;
                        lastWidth = labelWidth;
                    }
                    Graphics2D g2d = (Graphics2D) g2;
                    Path2D path = new Path2D.Float();
                    path.append(new Rectangle(borderX, borderY, borderWidth, labelY - borderY), false);
                    path.append(new Rectangle(borderX, labelY, minX - borderX - TEXT_SPACING, refHeight), false);
                    path.append(new Rectangle(maxX + lastWidth + TEXT_SPACING, labelY,
                            borderX - maxX + borderWidth - lastWidth - TEXT_SPACING, refHeight), false);
                    path.append(new Rectangle(borderX, labelY + refHeight, borderWidth,
                            borderY - labelY + borderHeight - refHeight), false);
                    g2d.clip(path);
                }
                border.paintBorder(c, g2, borderX, borderY, borderWidth, borderHeight);
                g2.dispose();
            }
        }
        g.translate(labelX, labelY);
        label.setSize(labelWidth, labelHeight);
        label.paint(g);
        g.translate(-labelX, -labelY);
        g.translate(buttonX, buttonY);
        arrowButton.paint(g);
        g.translate(-buttonX, -buttonY);
        lastComponentRef = c == null ? null : new WeakReference<>(c);
    }

}
