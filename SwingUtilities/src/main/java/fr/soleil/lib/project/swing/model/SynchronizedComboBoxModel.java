/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.MutableComboBoxModel;

import fr.soleil.lib.project.ObjectUtils;

/**
 * This class is like a {@link javax.swing.DefaultComboBoxModel DefaultComboBoxModel} with synchronized methods
 * 
 * @author Rapha&euml;l GIRARDOT
 * @param <E> the type of the elements of this model
 */
public class SynchronizedComboBoxModel<E> extends AbstractListModel<E>
        implements MutableComboBoxModel<E>, Serializable {

    private static final long serialVersionUID = 7021063942021105182L;

    protected List<E> objects;
    protected Object selectedObject;

    /**
     * Constructs an empty {@link SynchronizedComboBoxModel} object.
     */
    public SynchronizedComboBoxModel() {
        super();
        objects = new ArrayList<E>();
    }

    /**
     * Set the value of the selected item. The selected item may be <code>null</code>.
     * 
     * @param anObject The combo box value or null for no selection.
     */
    @Override
    public void setSelectedItem(Object anObject) {
        if (!ObjectUtils.sameObject(anObject, selectedObject)) {
            selectedObject = anObject;
            fireContentsChanged(this, -1, -1);
        }
    }

    @Override
    public Object getSelectedItem() {
        return selectedObject;
    }

    @Override
    public int getSize() {
        int size = 0;
        synchronized (objects) {
            size = objects.size();
        }
        return size;
    }

    @Override
    public E getElementAt(int index) {
        E element = null;
        synchronized (objects) {
            if (index > -1 && index < objects.size()) {
                element = objects.get(index);
            }
        }
        return element;
    }

    /**
     * Returns the index-position of the specified object in the list.
     * 
     * @param anObject
     * @return an int representing the index position, where 0 is the first position
     */
    public int getIndexOf(Object anObject) {
        int index = -1;
        synchronized (objects) {
            index = objects.indexOf(anObject);
        }
        return index;
    }

    @Override
    public void addElement(E anObject) {
        int size = 0;
        synchronized (objects) {
            objects.add(anObject);
            size = objects.size();
        }
        fireIntervalAdded(this, size - 1, size - 1);
        if (size == 1 && selectedObject == null && anObject != null) {
            setSelectedItem(anObject);
        }
    }

    @Override
    public void insertElementAt(E anObject, int index) {
        synchronized (objects) {
            objects.add(index, anObject);
        }
        fireIntervalAdded(this, index, index);
    }

    @Override
    public void removeElementAt(int index) {
        if (getElementAt(index) == selectedObject) {
            if (index == 0) {
                setSelectedItem(getSize() == 1 ? null : getElementAt(index + 1));
            } else {
                setSelectedItem(getElementAt(index - 1));
            }
        }

        synchronized (objects) {
            objects.remove(index);
        }

        fireIntervalRemoved(this, index, index);
    }

    @Override
    public void removeElement(Object anObject) {
        int index = -1;
        synchronized (objects) {
            index = objects.indexOf(anObject);
        }
        if (index != -1) {
            removeElementAt(index);
        }
    }

    /**
     * Empties the list.
     */
    public void removeAllElements() {
        int firstIndex = 0;
        int lastIndex = 0;
        boolean fireEvent = false;
        synchronized (objects) {
            if (objects.size() > 0) {
                lastIndex = objects.size() - 1;
                objects.clear();
                fireEvent = true;
            }
        }
        selectedObject = null;
        if (fireEvent) {
            fireIntervalRemoved(this, firstIndex, lastIndex);
        }
    }
}
