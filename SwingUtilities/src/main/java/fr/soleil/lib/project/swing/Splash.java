/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.Color;

import javax.swing.ImageIcon;

import fr.soleil.lib.project.swing.panel.SplashPanel;

public class Splash extends ASplash<JSmoothProgressBar, SplashPanel> {

    private static final long serialVersionUID = 3367109351114148780L;

    /**
     * Creates and displays a panel using the default splash image.
     */
    public Splash() {
        this(null, null, null, true);
    }

    /**
     * Creates a panel using the default splash image.<br />
     * This constructor offers the possibility to not display {@link Splash} immediately.
     * 
     * @param startVisible Whether to display the {@link Splash} immediately.
     */
    public Splash(boolean startVisible) {
        this(null, null, null, startVisible);
    }

    /**
     * Creates and displays a splash panel using the given image.<br />
     * The created {@link Splash} will be immediately displayed.
     * 
     * @param splashImage Splash image.
     */
    public Splash(ImageIcon splashImage) {
        this(splashImage, null, null, true);
    }

    /**
     * Creates a splash panel using the given image.<br />
     * The created {@link Splash} will be immediately displayed.<br />
     * This constructor offers the possibility to not display {@link Splash} immediately.
     * 
     * @param splashImage Splash image.
     * @param startVisible Whether to display the {@link Splash} immediately.
     */
    public Splash(ImageIcon splashImage, boolean startVisible) {
        this(splashImage, null, null, startVisible);
    }

    /**
     * Creates and displays a splash panel using the given image and text color.<br />
     * The textColor param does not affect the ProgressBar.
     * 
     * @param splashImage Splash image.
     * @param textColor Text color.
     */
    public Splash(ImageIcon splashImage, Color textColor) {
        this(splashImage, textColor, null, true);
    }

    /**
     * Creates a splash panel using the given image and text color.<br />
     * The textColor param does not affect the ProgressBar.<br />
     * This constructor offers the possibility to not display {@link Splash} immediately.
     * 
     * @param splashImage Splash image.
     * @param textColor Text color.
     * @param startVisible Whether to display the {@link Splash} immediately.
     */
    public Splash(ImageIcon splashImage, Color textColor, boolean startVisible) {
        this(splashImage, textColor, null, startVisible);
    }

    /**
     * Creates and displays a splash panel using the given image, text color and JSmoothProgressBar.<br />
     * 
     * @param splashImage Splash image.
     * @param textColor Text color.
     * @param newBar ProgressBar which will be used by this splah window.
     */
    public Splash(ImageIcon splashImage, Color textColor, JSmoothProgressBar newBar) {
        this(splashImage, textColor, newBar, true);
    }

    /**
     * Creates a splash panel using the given image, text color and JSmoothProgressBar.<br />
     * This constructor offers the possibility to not display {@link Splash} immediately.
     * 
     * @param splashImage Splash image.
     * @param textColor Text color.
     * @param newBar ProgressBar which will be used by this splash window.
     * @param startVisible Whether to display the {@link Splash} immediately.
     */
    public Splash(ImageIcon splashImage, Color textColor, JSmoothProgressBar newBar, boolean startVisible) {
        super(splashImage, textColor, newBar, startVisible);
    }

    @Override
    protected SplashPanel generateSplashPanel(ImageIcon icon, Color textColor, JSmoothProgressBar newBar) {
        return new SplashPanel(icon, textColor, newBar);
    }

    public void setMessage(String message) {
        splashPanel.setMessage(message);
    }

    public String getMessage() {
        return splashPanel.getMessage();
    }

    @Override
    public void setTitle(String title) {
        splashPanel.setTitle(title);
    }

    @Override
    public String getTitle() {
        return splashPanel.getTitle();
    }

    public JSmoothProgressBar getProgressBar() {
        return splashPanel.getProgress();
    }

    @Override
    public void initProgress() {
        splashPanel.setProgress(0);
    }

    public void setMaxProgress(int i) {
        splashPanel.getProgress().setMaximum(i);
    }

    public void progress(int i) {
        splashPanel.setProgress(i);
    }

    public static void main(String[] args) {
        Splash s = new Splash();
        s.setTitle("SplashScreen");
        s.setMessage("This is the free message line");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        for (int i = 0; i <= 100; i++) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
            s.progress(i);
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        System.exit(0);
    }

}
