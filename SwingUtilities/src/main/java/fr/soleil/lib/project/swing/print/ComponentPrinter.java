/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.print;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Pageable;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.JobName;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.JOptionPane;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.IDateConstants;

/**
 * <p>
 * A class that allows to print any kind of {@link Component}.
 * </p>
 * <p>
 * Based on code of <a href= "http://www.javafr.com/codes/IMPRESSION-JPANEL-SANS-COUPURE-COMPOSANTS-BAS-PAGE_31743.aspx"
 * >MPanelPrinter developed by mep</a>
 * </p>
 * <table border=1>
 * <tr>
 * <th><u>Use case example:</u> <small>(<i>myComponent</i> represents the component you want to print)</small></th>
 * </tr>
 * <tr>
 * <td>
 * <code>
 * String jobName = "My job name";<br/>
 * String docTitle = "My document title";<br/>
 * ComponentPrinter printer = new ComponentPrinter(<i>myComponent</i>);<br/>
 * printer.setOrientation(ComponentPrinter.LANDSCAPE);<br/>
 * printer.setFitMode(ComponentPrinter.FIT_PAGE);<br/>
 * printer.setDocumentTitle(docTitle);<br/>
 * printer.setJobName(jobName);<br/>
 * printer.print();
 * </code></td>
 * </tr>
 * </tbody>
 * </table>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ComponentPrinter implements Printable, Pageable, IDateConstants {

    // ------------ Messages ------------ //
    protected static final String ERROR_WHILE_PRINTING_DOCUMENT = "Error while printing document:\n";
    protected static final String ERROR_SEPARATOR = ":\n";
    protected static final String PRINT_ERROR = "Print Error";
    protected static final String PAGE_COUNT_SEPARATOR = "/";
    protected static final String PAGE_INDEX_START = " - [";
    protected static final String DATE_START = "] (";
    protected static final String DATE_END = ") ";
    // ---------------------------------- //

    // ------------ Print configuration constants ------------ //
    /**
     * Used with setOrientation(). If you use this value with setOrientation(), your Component will
     * be printed with "Portrait" orientation.
     * 
     * @see #setOrientation(int)
     */
    public static final int PORTRAIT = PageFormat.PORTRAIT;

    /**
     * Used with setOrientation(). If you use this value with setOrientation(), your Component will
     * be printed with "Landscape" orientation.
     * 
     * @see #setOrientation(int)
     */
    public static final int LANDSCAPE = PageFormat.LANDSCAPE;

    /**
     * Used with setFitMode(). If you use this value with setFitMode(), your component will be
     * scaled in order to fit page width. If your Component is bigger than page height, it will be
     * splitted into as many pages as necessary.
     * 
     * @see #setFitMode(int)
     */
    public static final int FIT_PAGE_WIDTH = 0;

    /**
     * Used with setFitMode(). If you use this value with setFitMode(), your component will be
     * scaled in order to fit page (width and height).
     * 
     * @see #setFitMode(int)
     */
    public static final int FIT_PAGE = 1;

    /**
     * Used with setFitMode(). If you use this value with setFitMode(), your component will not be
     * scaled. However, if your Component is bigger than page height, it will be splitted into as
     * many pages as necessary.
     * 
     * @see #setFitMode(int)
     */
    public static final int NO_FIT = 2;
    // ------------------------------------------------------- //

    // ------------ Variables ------------ //
    protected WeakReference<Component> componentRef;
    protected PageFormat pageFormat;
    protected PrinterJob printJob;
    protected String documentTitle;
    protected BufferedImage toPrint;
    protected int fitMode;
    protected int xCount, yCount, pageCount;
    protected double ratio;
    protected Date date;
    protected final SimpleDateFormat dateFormat;
    protected boolean changeBackgroundToPrint;
    // ----------------------------------- //

    /**
     * Constructor
     * 
     * @param component . The component you wish to print.
     */
    public ComponentPrinter(Component component) {
        this.componentRef = component == null ? null : new WeakReference<>(component);
        // by default, force white background color to print component
        changeBackgroundToPrint = true;
        dateFormat = new SimpleDateFormat(DATE_TIME_SHORT_FORMAT);
        initPrintablePanel();
    }

    protected void initPrintablePanel() {
        toPrint = null;
        documentTitle = ObjectUtils.EMPTY_STRING;
        xCount = yCount = pageCount = 0;
        ratio = 1;
        fitMode = NO_FIT;
        printJob = PrinterJob.getPrinterJob();
        pageFormat = printJob.defaultPage();

        Paper paper = (Paper) pageFormat.getPaper().clone();
        paper.setImageableArea(paper.getImageableX(), paper.getImageableY(), paper.getWidth() - 30,
                paper.getHeight() - 30);
        pageFormat.setPaper(paper);
        paper = null;
    }

    /**
     * Sets paper orientation
     * 
     * @see #PORTRAIT
     * @see #LANDSCAPE
     * @see #getOrientation()
     */
    public void setOrientation(int orientation) {
        pageFormat.setOrientation(orientation);
    }

    /**
     * Returns chosen paper orientation
     * 
     * @return Chosen paper orientation
     * @see #PORTRAIT
     * @see #LANDSCAPE
     * @see #setOrientation(int)
     */
    public int getOrientation() {
        return pageFormat.getOrientation();
    }

    /**
     * Sets whether your Component should be scaled (and how) before printing.<br>
     * <i>(Has no effect on the Component itself, but just on how it is rendered in printing)</i><br>
     * default value : <code><b>NO_FIT</b></code>
     * 
     * @see #FIT_PAGE
     * @see #FIT_PAGE_WIDTH
     * @see #NO_FIT
     * @see #getFitMode()
     */
    public void setFitMode(int fitMode) {
        this.fitMode = fitMode;
    }

    /**
     * Returns chosen fit mode
     * 
     * @return Chosen fit mode
     * @see #FIT_PAGE
     * @see #FIT_PAGE_WIDTH
     * @see #NO_FIT
     * @see #setFitMode(int)
     */
    public int getFitMode() {
        return fitMode;
    }

    /**
     * Returns whether this {@link ComponentPrinter} will change component background color (i.e. force white color) to
     * print component.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isChangeBackgroundToPrint() {
        return changeBackgroundToPrint;
    }

    /**
     * Sets whether this {@link ComponentPrinter} can change component background color (i.e. force white color) to
     * print component.
     * 
     * @param changeBackgroundToPrint Whether this {@link ComponentPrinter} can change component background color (i.e.
     *            force white color) to print component.
     */
    public void setChangeBackgroundToPrint(boolean changeBackgroundToPrint) {
        this.changeBackgroundToPrint = changeBackgroundToPrint;
    }

    /**
     * Returns page width
     * 
     * @return Page with
     */
    public int getPageWidth() {
        return (int) pageFormat.getImageableWidth();
    }

    /**
     * Returns top margin
     * 
     * @return Top margin
     */
    public double getMarginTop() {
        return pageFormat.getImageableY();
    }

    /**
     * Returns left margin
     * 
     * @return Left margin
     */
    public double getMarginLeft() {
        return pageFormat.getImageableX();
    }

    /**
     * Sets Left and Right margins (in pixel).
     * 
     * @param margin The wished margin
     */
    public void setLRMargins(double margin) {
        Paper paper = pageFormat.getPaper();
        paper.setImageableArea(margin, paper.getImageableY(), paper.getWidth() - margin, paper.getImageableHeight());
        pageFormat.setPaper(paper);
    }

    /**
     * Sets Top and Bottom margins (in pixel).
     * 
     * @param margin The wished margin
     */
    public void setTBMargins(double margin) {
        Paper paper = pageFormat.getPaper();
        paper.setImageableArea(paper.getImageableX(), margin, paper.getImageableWidth(), paper.getHeight() - margin);
        pageFormat.setPaper(paper);
    }

    /**
     * Sets a title to your page. This title is used with bottom assessment :<br>
     * <i>documentTitle - [pageNumber/pageCount]</i>
     * 
     * @param title the wished document title
     */
    public void setDocumentTitle(String title) {
        documentTitle = title;
    }

    /**
     * Sets a name to your print job.
     * 
     * @param jobName the wished print job name
     */
    public void setJobName(String jobName) {
        printJob.setJobName(jobName);
    }

    @Override
    public int print(Graphics g, PageFormat pf, int pageIndex) throws PrinterException {
        int returnCode = PAGE_EXISTS;
        if (pageIndex >= pageCount) {
            returnCode = NO_SUCH_PAGE;
        } else {
            Graphics2D g2d = (Graphics2D) g;
            g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2d.translate(pf.getImageableX(), pf.getImageableY());
            if (ratio != 1) {
                g2d.scale(ratio, ratio);
            }
            int xPosition = 0, yPosition = 0;
            int xIndex = pageIndex % xCount, yIndex = pageIndex / xCount;
            if (xIndex > 0) {
                double pageWidth = g2d.getClip().getBounds().width;
                xPosition = (int) Math.rint(-xIndex * pageWidth);
            }
            if (yIndex > 0) {
                double pageHeight = g2d.getClip().getBounds().height;
                yPosition = (int) Math.rint(-yIndex * pageHeight);
            }
            g2d.drawImage(toPrint, xPosition, yPosition, null);

            // write the document information
            StringBuilder details = new StringBuilder(documentTitle);
            details.append(PAGE_INDEX_START).append(pageIndex + 1).append(PAGE_COUNT_SEPARATOR);
            details.append(pageCount).append(DATE_START).append(dateFormat.format(date)).append(DATE_END);
            Font formerPrintFont = g2d.getFont();
            Font newPrintFont = formerPrintFont.deriveFont((float) Math.rint(formerPrintFont.getSize() / ratio));
            g2d.setFont(newPrintFont);
            g2d.drawString(details.toString(), 0, g2d.getClip().getBounds().height - (int) Math.rint(5 / ratio));
            g2d.setFont(formerPrintFont);
            formerPrintFont = null;
            newPrintFont = null;
            details = null;
            g2d.dispose();
            g2d = null;
        }
        return returnCode;
    }

    /**
     * Call this method to print your Component, and if you want to manage exceptions yourself
     * 
     * @throws PrinterException if a problem occurred
     */
    public void printWithException() throws PrinterException {
        Component component = ObjectUtils.recoverObject(componentRef);
        if (component != null) {
            Color formerBackground = component.getBackground();
            pageFormat = printJob.validatePage(pageFormat);
            printJob.setPrintable(this, pageFormat);// linux compatible
            if (pageCount == 0) {
                calculatePages();
            }
            boolean printOk = false;
            boolean changeBackground = isChangeBackgroundToPrint();
            try {
                // set some specific attributes
                PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
                pras.add(new JobName(printJob.getJobName(), null));
                pras.add(MediaSizeName.ISO_A4);
                pras.add(new MediaPrintableArea(5, 5, 210 - 2 * 5, 297 - 2 * 5, MediaPrintableArea.MM));
                switch (getOrientation()) {
                    case LANDSCAPE:
                        pras.add(OrientationRequested.LANDSCAPE);
                        break;

                    case PORTRAIT:
                        pras.add(OrientationRequested.PORTRAIT);
                        break;
                }

                if (printJob.printDialog(pras)) {
                    // Print dialog may have changed paper dimensions -> recalculate pages
                    pageFormat = printJob.getPageFormat(pras);
                    calculatePages();
                    date = new Date();
                    if (changeBackground) {
                        component.setBackground(Color.WHITE);
                    }
                    toPrint = new BufferedImage(component.getWidth(), component.getHeight(),
                            BufferedImage.TYPE_INT_ARGB);
                    component.printAll(toPrint.getGraphics());
                    printJob.print(pras);
                }
                printOk = true;
            } finally {
                if (!printOk) {
                    printJob.cancel();
                }
                date = null;
                if (toPrint != null) {
                    toPrint.flush();
                    toPrint = null;
                }
                if (changeBackground) {
                    component.setBackground(formerBackground);
                }
            }
        }
    }

    /**
     * Call this method to print your Component
     */
    public void print() {
        try {
            printWithException();
        } catch (PrinterException pe) {
            JOptionPane.showMessageDialog(ObjectUtils.recoverObject(componentRef),
                    ERROR_WHILE_PRINTING_DOCUMENT + pe.getClass().getName() + ERROR_SEPARATOR + pe.getMessage(),
                    PRINT_ERROR, JOptionPane.ERROR_MESSAGE);
        }
    }

    protected void calculatePages() {
        Component component = ObjectUtils.recoverObject(componentRef);
        if (component == null) {
            ratio = 1;
            xCount = yCount = 1;
        } else {
            double pageHeight = pageFormat.getImageableHeight();
            double pageWidth = pageFormat.getImageableWidth();
            double documentWidth = component.getWidth();
            double documentHeight = component.getHeight();
            double scaleX = pageWidth / documentWidth;
            double scaleY = pageHeight / documentHeight;
            double documentScaledHeight;
            double documentScaledWidth;
            switch (fitMode) {
                case FIT_PAGE:
                    ratio = Math.min(scaleX, scaleY);
                    break;
                case FIT_PAGE_WIDTH:
                    ratio = scaleX;
                    break;
                case NO_FIT:
                    ratio = 1;
                    break;
            }
            documentScaledHeight = documentHeight * ratio;
            documentScaledWidth = documentWidth * ratio;
            if (documentScaledWidth > pageWidth) {
                xCount = (int) Math.ceil(documentScaledWidth / pageWidth);
            } else {
                xCount = 1;
            }
            if (documentScaledHeight > pageHeight) {
                yCount = (int) Math.ceil(documentScaledHeight / pageHeight);
            } else {
                yCount = 1;
            }
        }
        pageCount = xCount * yCount;
    }

    @Override
    public int getNumberOfPages() {
        if (pageCount == 0) {
            calculatePages();
        }
        return pageCount;
    }

    @Override
    public PageFormat getPageFormat(int pageIndex) throws IndexOutOfBoundsException {
        return pageFormat;
    }

    @Override
    public Printable getPrintable(int pageIndex) throws IndexOutOfBoundsException {
        return this;
    }

    @Override
    protected void finalize() {
        componentRef = null;
        toPrint = null;
        date = null;
        documentTitle = null;
        pageFormat = null;
        printJob = null;
    }

}
