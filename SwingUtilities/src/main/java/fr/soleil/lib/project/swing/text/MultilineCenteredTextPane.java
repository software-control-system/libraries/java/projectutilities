/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import javax.swing.JTextPane;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

/**
 * use a JTextArea as rendering component.
 */
public class MultilineCenteredTextPane extends JTextPane {

    private static final long serialVersionUID = -2624662132532053990L;

    public MultilineCenteredTextPane() {
        // Create text pane with the document
        super();
        decorateTextPane(this);
    }

    public static JTextPane decorateTextPane(final JTextPane pane) {
        pane.setOpaque(true);

        // Add a CenteredBoxView to center text vertically
        pane.setEditorKit(new CenteredEditorKit());

        // Create document to center text horizontally
        StyledDocument document = new DefaultStyledDocument();
        Style defaultStyle = document.getStyle(StyleContext.DEFAULT_STYLE);
        StyleConstants.setAlignment(defaultStyle, StyleConstants.ALIGN_CENTER);
        pane.setStyledDocument(document);

        return pane;
    }
}
