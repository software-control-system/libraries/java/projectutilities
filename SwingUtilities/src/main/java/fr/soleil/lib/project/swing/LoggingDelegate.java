/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.Container;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;

import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

/**
 * A class delegated to manage log panels scrolling.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
@Deprecated
public class LoggingDelegate {

    private final JComponent component;
    private final ComponentVisibilityListener listener;

    private volatile boolean ensureScrolling;
    private boolean firstDisplay;
    private boolean reversed;

    public LoggingDelegate(JComponent component, boolean autoScroll) {
        this.component = component;
        listener = new ComponentVisibilityListener();
        resetScrollInfo();
        reversed = false;
        firstDisplay = true;
        if (component != null) {
            component.addHierarchyListener(listener);
            if (autoScroll) {
                component.addComponentListener(new ComponentAdapter() {
                    @Override
                    public void componentResized(ComponentEvent e) {
                        prepareScroll();
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                updateScroll();
                            }
                        });
                    }
                });
            }
        }
    }

    public boolean isReversed() {
        return reversed;
    }

    public void setReversed(boolean reversed) {
        if (this.reversed != reversed) {
            this.reversed = reversed;
            resetScrollInfo();
        }
    }

    protected void resetScrollInfo() {
        ensureScrolling = true;
    }

    /**
     * Calculates whether it will be necessary to update the {@link JComponent}'s surrounding {@link JScrollPane} scroll
     * position.
     */
    public void prepareScroll() {
        JScrollBar scrollBar = recoverVerticalScrollBar();
        boolean forceScroll;
        int position;
        if ((scrollBar == null) || (!scrollBar.isVisible()) || (!scrollBar.isEnabled())) {
            forceScroll = true;
            ensureScrolling = true;
            position = -1;
        } else {
            position = scrollBar.getValue();
            if (reversed) {
                forceScroll = (position == scrollBar.getMinimum());
            } else {
                Container c = component.getParent();
                if (c instanceof JViewport) {
                    if (scrollBar.getValueIsAdjusting()) {
                        forceScroll = false;
                    } else {
                        JViewport viewport = (JViewport) c;
                        int y = component.getY();
                        int height = component.getHeight();
                        forceScroll = (y + height) == viewport.getHeight();
                    }
                } else {
                    forceScroll = true;
                }
            }
        }
        ensureScrolling = forceScroll;
    }

    /**
     * Updates the {@link JComponent}'s surrounding {@link JScrollPane} scroll position, if necessary.
     */
    public void updateScroll() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JScrollBar scrollBar = recoverVerticalScrollBar();
                if (scrollBar != null) {
                    if (ensureScrolling) {
                        if (reversed) {
                            if (component instanceof JTextComponent) {
                                ((JTextComponent) component).setCaretPosition(0);
                            }
                            scrollBar.setValue(0);
                        } else {
                            if (component instanceof JTextComponent) {
                                JTextComponent textComponent = (JTextComponent) component;
                                textComponent.setCaretPosition(textComponent.getDocument().getLength());
                            }
                            scrollBar.setValue(scrollBar.getMaximum());
                        }
                    }
                }
            }
        });
    }

    public JScrollBar recoverVerticalScrollBar() {
        JScrollBar scrollBar = null;
        Container p = (component == null ? null : component.getParent());
        if (p instanceof JViewport) {
            Container gp = p.getParent();
            if (gp instanceof JScrollPane) {
                JScrollPane scrollPane = (JScrollPane) gp;
                // Make certain we are the viewPort's view and not, for
                // example, the rowHeaderView of the scrollPane -
                // an implementor of fixed columns might do this.
                JViewport viewport = scrollPane.getViewport();
                if (viewport != null && viewport.getView() == component) {
                    scrollBar = scrollPane.getVerticalScrollBar();
                }
            }
        }
        return scrollBar;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class ComponentVisibilityListener implements HierarchyListener {
        @Override
        public void hierarchyChanged(HierarchyEvent e) {
            if ((firstDisplay) && (e.getID() == HierarchyEvent.HIERARCHY_CHANGED) && (component != null)
                    && component.isShowing()) {
                firstDisplay = false;
                ensureScrolling = true;
                updateScroll();
            }
        }
    }

}
