/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;

/**
 * A {@link JMenu} that can be clicked
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class JClickableMenu extends JMenu {

    private static final long serialVersionUID = 6957290520997674694L;

    public JClickableMenu() {
        super();
        initialize();
    }

    public JClickableMenu(String s) {
        super(s);
        initialize();
    }

    public JClickableMenu(Action a) {
        super(a);
        initialize();
    }

    public JClickableMenu(String s, boolean b) {
        super(s, b);
        initialize();
    }

    protected void initialize() {
        addMouseListener(new MenuClicker());
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class MenuClicker extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            if ((e != null) && (e.getSource() == JClickableMenu.this)) {
                boolean hasActionListener = false;
                for (Object listener : listenerList.getListenerList()) {
                    if (ActionListener.class.equals(listener)) {
                        hasActionListener = true;
                        break;
                    }
                }
                if (hasActionListener) {
                    hideMenu(JClickableMenu.this);
                }
                fireActionPerformed(new ActionEvent(e.getSource(), ActionEvent.ACTION_PERFORMED, null, e.getWhen(), 0));
            }
        }

        protected void hideMenu(Component menu) {
            if (menu != null) {
                if (menu instanceof JMenu) {
                    JMenu jMenu = (JMenu) menu;
                    jMenu.setPopupMenuVisible(false);
                    jMenu.setSelected(false);
                    hideMenu(menu.getParent());
                } else if (menu instanceof JPopupMenu) {
                    JPopupMenu popupMenu = (JPopupMenu) menu;
                    popupMenu.setVisible(false);
                    hideMenu(popupMenu.getInvoker());
                } else {
                    hideMenu(menu.getParent());
                }
            }
        }
    }

}
