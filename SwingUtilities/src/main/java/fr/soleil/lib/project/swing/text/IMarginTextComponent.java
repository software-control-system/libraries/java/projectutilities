/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Insets;

/**
 * An {@link ITextComponent} that has an inner margin.
 * <p>
 * Typically, you can force a <code>JTextButton</code> or a <code>JTextfield</code> to implement that interface with no
 * code impact.
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 *
 */
public interface IMarginTextComponent extends ITextComponent {

    /**
     * Returns the inner margin.
     * 
     * @return An {@link Insets}.
     */
    public Insets getMargin();

    /**
     * Sets the inner margin.
     * 
     * @param margins The inner margin to set.
     */
    public void setMargin(Insets margin);

}
