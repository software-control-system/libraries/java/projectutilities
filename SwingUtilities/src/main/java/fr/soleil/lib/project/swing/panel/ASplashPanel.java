/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.text.JSmoothLabel;

/**
 * A {@link JPanel} that renders some progression.
 *
 * @param <P> The type of progress bar used by this {@link ASplashPanel}.
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class ASplashPanel<P extends JComponent> extends ImagePanel {

    private static final long serialVersionUID = 3482602589156073549L;

    /** Font to use for title. */
    protected static final Font TITLE_FONT = new Font(Font.DIALOG, Font.BOLD, 18);
    /** Font to use for messages. */
    protected static final Font MESSAGE_FONT = new Font(Font.DIALOG, Font.BOLD, 12);
    /** Font to use for copyright. */
    protected static final Font COPYRIGHT_FONT = new Font(Font.DIALOG, Font.BOLD, 10);
    /** Default text foreground color. */
    protected static final Color DEFAULT_FOREGROUND = new Color(204, 204, 204);
    /** Default size in case of <code>null</code> image. */
    protected static final Dimension DEFAULT_SIZE = new Dimension(300, 300);
    /** Default copyright. */
    protected static final String DEFAULT_COPYRIGHT = "\u00a9 SOLEIL Synchrotron";
    /** Text to display for initialization. */
    protected static final String INITIALIZING = "Initializing...";

    protected JLabel title, copyright;
    protected P progressBar;
    protected Dimension imgSize;
    protected ImageIcon img;
    protected Color textForeground;

    /**
     * Creates a new {@link ASplashPanel} with given image, text foreground color, and progress bar.
     * 
     * @param image The image. Can be <code>null</code>.
     * @param textForeground The text foreground color. Can be <code>null</code>.
     * @param bar The progress bar. Can be <code>null</code>.
     */
    protected ASplashPanel(ImageIcon image, Color textForeground, P bar) {
        super();
        img = image;
        this.textForeground = textForeground == null ? DEFAULT_FOREGROUND : textForeground;
        if (bar != null) {
            progressBar = bar;
        }
        initComponents();
        layoutComponents();
    }

    /**
     * Generates a {@link JSmoothLabel} with given text and {@link Font}.
     * 
     * @param text The text.
     * @param font The {@link Font}.
     * @return A {@link JSmoothLabel}.
     */
    protected JSmoothLabel generateLabel(String text, Font font) {
        JSmoothLabel message = new JSmoothLabel(text);
        message.setFont(font);
        message.setForeground(textForeground);
        message.setHorizontalAlignment(SwingConstants.LEFT);
        message.setOpaque(false);
        message.setDoubleBuffered(false);
        return message;
    }

    /**
     * Generates a progress bar.
     * 
     * @return A progress bar.
     */
    protected abstract P generateProgressBar();

    /**
     * Returns the default size to use when image is <code>null</code>.
     * 
     * @return A {@link Dimension}.
     */
    protected Dimension getDefaultSize() {
        return DEFAULT_SIZE;
    }

    @Override
    public void setBackgroundImage(ImageIcon img) {
        super.setBackgroundImage(img);
        if (img == null) {
            imgSize = getDefaultSize();
        } else {
            imgSize = new Dimension(img.getIconWidth(), img.getIconHeight());
        }
        setPreferredSize(imgSize);
        setMinimumSize(imgSize);
        setMaximumSize(imgSize);
    }

    /**
     * Initializes the different components of this {@link ASplashPanel}.
     */
    protected void initComponents() {
        setImageAlpha(1);
        setBackgroundImage(img);
        setDoubleBuffered(false);
        title = generateLabel(ObjectUtils.EMPTY_STRING, TITLE_FONT);
        copyright = generateLabel(DEFAULT_COPYRIGHT, COPYRIGHT_FONT);
        if (progressBar == null) {
            progressBar = generateProgressBar();
        }
        progressBar.setDoubleBuffered(false);
    }

    /**
     * Layout management.
     */
    protected abstract void layoutComponents();

    /**
     * Returns the title.
     * 
     * @return A {@link String}: the title.
     */
    public String getTitle() {
        return title.getText();
    }

    /**
     * Sets the title.
     * 
     * @param title The title to set.
     */
    public void setTitle(String title) {
        this.title.setText(title);
        if (isShowing()) {
            repaint();
        }
    }

    /**
     * Returns the copyright.
     * 
     * @return A {@link String}: the copyright.
     */
    public String getCopyright() {
        return copyright.getText();
    }

    /**
     * Sets the copyright.
     * 
     * @param copyright The copyright to set.
     */
    public void setCopyright(String copyright) {
        this.copyright.setText(copyright);
        if (isShowing()) {
            repaint();
        }
    }

}
