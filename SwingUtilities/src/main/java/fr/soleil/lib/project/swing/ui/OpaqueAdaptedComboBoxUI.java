/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.ui;

import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.plaf.metal.MetalComboBoxUI;

import fr.soleil.lib.project.swing.combo.OpaqueAdaptedComboPopup;

/**
 * A {@link MetalComboBoxUI} that really applies the opacity and foreground color of the associated {@link JComboBox}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class OpaqueAdaptedComboBoxUI extends MetalComboBoxUI {

    public OpaqueAdaptedComboBoxUI() {
        super();
    }

    // The @SuppressWarnings is here for compatibility with ComboBoxUI signature
    @SuppressWarnings("unchecked")
    @Override
    public void setPopupVisible(JComboBox c, boolean v) {
        listBox.setOpaque(comboBox.isOpaque());
        super.setPopupVisible(c, v);
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        arrowButton.setOpaque(comboBox.isOpaque());
        super.paint(g, c);
    }

    @Override
    public void paintCurrentValue(Graphics g, Rectangle bounds, boolean hasFocus) {
        listBox.setSelectionForeground(comboBox.getForeground());
        super.paintCurrentValue(g, bounds, hasFocus);
    }

    @Override
    protected OpaqueAdaptedComboPopup createPopup() {
        return new OpaqueAdaptedComboPopup(comboBox);
    }
}
