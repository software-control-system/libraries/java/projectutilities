/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.ui;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSlider;
import javax.swing.SwingConstants;

import fr.soleil.lib.project.ObjectUtils;

public class ToolTipSliderUI extends ColoredTickMetalSliderUI {
    private JPopupMenu pop;
    private JLabel lblPop;

    public ToolTipSliderUI() {
        super();
        pop = new JPopupMenu();
        lblPop = new JLabel(ObjectUtils.EMPTY_STRING, SwingConstants.CENTER);
        JPanel jp = new JPanel();
        jp.add(lblPop);
        pop.add(jp);
    }

    public void updatePopup(MouseEvent me) {
        lblPop.setBackground(slider.getBackground());
        lblPop.setText(slider.getToolTipText());
        pop.pack();
        Rectangle thumb = thumbRect;
        int x, y;
        if (thumb == null) {
            x = me.getX();
            y = me.getY();
        } else {
            x = thumb.x + thumb.width / 2;
            y = thumb.y;
        }
        pop.setLocation(slider.getLocationOnScreen().x + x - (pop.getWidth() / 2),
                slider.getLocationOnScreen().y + y - (pop.getHeight() + 2));
    }

    @Override
    protected TrackListener createTrackListener(JSlider slider) {
        return new TrackListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                pop.setVisible(false);
                super.mouseReleased(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                pop.setVisible(true);
                updatePopup(e);
                super.mousePressed(e);
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                updatePopup(e);
                super.mouseDragged(e);
            }
        };
    }

}
