/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.tree;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.WindowConstants;
import javax.swing.text.JTextComponent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

/**
 * A {@link JTree} for which you can recursively expand/collapse its nodes
 * 
 * @author claisse, Rapha&euml;l GIRARDOT
 */
public class ExpandableTree extends JTree {

    private static final long serialVersionUID = -1986597841832730530L;

    private final List<TreeNode> foundTreeNodes;
    private int actualIndex;
    private String actualSearchString;

    /**
     * Returns an {@link ExpandableTree} with a sample model. The default model
     * used by the tree defines a leaf node as any node without children.
     * 
     * @see JTree#JTree()
     */
    public ExpandableTree() {
        super();
        foundTreeNodes = new ArrayList<TreeNode>();
        actualIndex = 0;
        actualSearchString = null;
    }

    /**
     * Returns an {@link ExpandableTree} which displays the root node -- the
     * tree is created using the specified data model.
     * 
     * @param newModel
     *            the {@link TreeModel} to use as the data model
     * @see JTree#JTree(TreeModel)
     */
    public ExpandableTree(TreeModel newModel) {
        super(newModel);
        foundTreeNodes = new ArrayList<TreeNode>();
        actualIndex = 0;
        actualSearchString = null;
    }

    /**
     * Returns an {@link ExpandableTree} created from a {@link Hashtable} which
     * does not display with root. Each value-half of the key/value pairs in the
     * HashTable becomes a child of the new root node. By default, the tree
     * defines a leaf node as any node without children.
     * 
     * @param value
     *            a {@link Hashtable}
     * @see JTree#JTree(Hashtable)
     */
    public ExpandableTree(Hashtable<?, ?> value) {
        super(value);
        foundTreeNodes = new ArrayList<TreeNode>();
        actualIndex = 0;
        actualSearchString = null;
    }

    /**
     * Returns an {@link ExpandableTree} with each element of the specified
     * array as the child of a new root node which is not displayed. By default,
     * the tree defines a leaf node as any node without children.
     * 
     * @param value
     *            an array of Objects
     * @see JTree#JTree(Object[])
     */
    public ExpandableTree(Object[] value) {
        super(value);
        foundTreeNodes = new ArrayList<TreeNode>();
        actualIndex = 0;
        actualSearchString = null;
    }

    /**
     * Returns an {@link ExpandableTree} with the specified {@link TreeNode} as
     * its root, which displays the root node and which decides whether a node
     * is a leaf node in the specified manner.
     * 
     * @param root
     *            a {@link TreeNode} object
     * @param asksAllowsChildren
     *            if <code>false</code>, any node without children is a leaf
     *            node; if <code>true</code>, only nodes that do not allow
     *            children are leaf nodes
     * @see JTree#JTree(TreeNode, boolean)
     */
    public ExpandableTree(TreeNode root, boolean asksAllowsChildren) {
        super(root, asksAllowsChildren);
        foundTreeNodes = new ArrayList<TreeNode>();
        actualIndex = 0;
        actualSearchString = null;
    }

    /**
     * Returns an {@link ExpandableTree} with the specified {@link TreeNode} as
     * its root, which displays the root node. By default, the tree defines a
     * leaf node as any node without children.
     * 
     * @param root
     *            a {@link TreeNode} object
     * @see JTree#JTree(TreeNode)
     */
    public ExpandableTree(TreeNode root) {
        super(root);
        foundTreeNodes = new ArrayList<TreeNode>();
        actualIndex = 0;
        actualSearchString = null;
    }

    /**
     * Returns an {@link ExpandableTree} with each element of the specified
     * {@link Vector} as the child of a new root node which is not displayed. By
     * default, the tree defines a leaf node as any node without children.
     * 
     * @param value
     *            a {@link Vector}
     * @see JTree#JTree(Vector)
     */
    public ExpandableTree(Vector<?> value) {
        super(value);
        foundTreeNodes = new ArrayList<TreeNode>();
        actualIndex = 0;
        actualSearchString = null;
    }

    /**
     * Expands/collapses the whole tree. If you choose to collapse the whole
     * tree, this method will keep the root node expanded.
     * 
     * @param expand
     *            <code>true</code> to expand, <code>false</code> to collapse
     */
    public void expandAll(boolean expand) {
        TreeModel model = getModel();
        if (model != null) {
            Object rootObject = model.getRoot();
            if (rootObject instanceof TreeNode) {
                TreeNode root = (TreeNode) rootObject;
                if (!expand) {
                    // clear selection before collapsing to avoid multiple
                    // selection events
                    clearSelection();
                }
                TreePath rootPath = new TreePath(root);
                expandAll(rootPath, expand);
                if (!expand) {
                    // Keep at least root node expanded
                    expandPath(rootPath);
                }
                treeDidChange();
            }
        }
    }

    /**
     * Fully expands/collapses selected node
     * 
     * @param expand
     *            <code>true</code> to expand, <code>false</code> to collapse
     */
    public void expandSelectedNode(boolean expand) {
        expandAll(getSelectionPath(), expand);
    }

    /**
     * Fully expands/collapses a sub part of the tree, starting from a path.
     * 
     * @param parent
     *            the path from which to start
     * @param expand
     *            <code>true</code> to expand, <code>false</code> to collapse
     */
    public void expandAll(TreePath parent, boolean expand) {
        // Traverse children
        TreeNode node = (TreeNode) parent.getLastPathComponent();
        if (node.getChildCount() >= 0) {
            Enumeration<?> e = node.children();
            while (e.hasMoreElements()) {
                TreeNode n = (TreeNode) e.nextElement();
                TreePath path = parent.pathByAddingChild(n);
                expandAll(path, expand);
            }
        }
        // Expansion or collapse must be done bottom-up
        if (expand) {
            expandPath(parent);
        } else {
            collapsePath(parent);
        }
    }

    /**
     * Returns a {@link List} of {@link TreePath}s, corresponding to the sorted
     * selection path
     * 
     * @return a {@link List} of {@link TreePath}s
     */
    public List<TreePath> getSortedSelectionPaths() {
        TreePath[] selected = getSelectionPaths();

        ArrayList<TreePath> results = new ArrayList<TreePath>();
        if (selected != null) {
            for (TreePath element : selected) {
                results.add(element);
            }
        }

        // order the selected paths as displayed in the tree, from top to bottom
        Collections.sort(results, new Comparator<TreePath>() {
            @Override
            public int compare(TreePath o1, TreePath o2) {
                return (getRowForPath(o1) - getRowForPath(o2));
            }
        });

        return results;
    }

    /**
     * Returns a {@link List} of {@link TreePath}s, corresponding to the list of
     * selected {@link TreePath} which {@link TreePath#getLastPathComponent()}
     * is of a particular type. If a {@link TreePath} is selected, but it does
     * not contain this type of component, it will look for all its children
     * {@link TreePath}s that may contain this type of component.
     * 
     * @param componentClass
     *            The type of component to find in {@link TreePath}
     * @return a {@link List} of {@link TreePath}s
     */
    public List<TreePath> getSortedFilteredSelectionPaths(Class<?> componentClass) {
        TreePath[] selected = getSelectionPaths();
        List<TreePath> paths = new ArrayList<TreePath>();
        if (selected != null) {
            for (TreePath element : selected) {
                addFilteredTreePaths(element, componentClass, paths);
            }
        }
        // order the selected paths as displayed in the tree, from top to bottom
        Collections.sort(paths, new Comparator<TreePath>() {
            @Override
            public int compare(TreePath o1, TreePath o2) {
                return (getRowForPath(o1) - getRowForPath(o2));
            }
        });
        return paths;
    }

    /**
     * Adds all {@link TreePath}s, that contains components of a particular
     * {@link Class}, to a {@link List}
     * 
     * @param selectedPath
     *            The {@link TreePath} from which to begin the search
     * @param componentClass
     *            The expected component {@link Class}
     * @param pathList
     *            The {@link TreePath} {@link List}
     */
    protected void addFilteredTreePaths(TreePath selectedPath, Class<?> componentClass, List<TreePath> pathList) {
        if (componentClass == null) {
            componentClass = Object.class;
        }
        if (selectedPath.getLastPathComponent() != null
                && componentClass.isAssignableFrom(selectedPath.getLastPathComponent().getClass())) {
            if (!pathList.contains(selectedPath)) {
                // the path can be added
                pathList.add(selectedPath);
            }
        } else if (selectedPath.getLastPathComponent() instanceof DefaultMutableTreeNode) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectedPath.getLastPathComponent();
            // look for children
            for (int i = 0; i < node.getChildCount(); i++) {
                if (node.getChildAt(i) instanceof DefaultMutableTreeNode) {
                    DefaultMutableTreeNode child = (DefaultMutableTreeNode) node.getChildAt(i);
                    TreePath childPath = new TreePath(child.getPath());
                    addFilteredTreePaths(childPath, componentClass, pathList);
                }
            }
        }
    }

    /**
     * Select the next node that contains the given String
     */
    public void searchNextNode(String contains) {
        searchNode(contains, true);
    }

    /**
     * Select the previous node that contains the given String
     */
    public void searchPreviousNode(String contains) {
        searchNode(contains, false);
    }

    private void searchNode(String contains, boolean next) {
        synchronized (foundTreeNodes) {
            if ((contains != null) && (!contains.trim().isEmpty())) {
                if (!contains.trim().equalsIgnoreCase(actualSearchString)) {
                    foundTreeNodes.clear();
                    actualIndex = -1;
                    actualSearchString = contains.trim().toLowerCase();
                    // Search all the match tree path.
                    // If several response do nothing
                    // If one match Expend the match node
                    // ITreeNode rootNode = configTree.getRootNode();
                    TreeModel model = getModel();
                    if (model != null) {
                        Object rootObject = model.getRoot();
                        if (rootObject != null && rootObject instanceof TreeNode) {
                            TreeNode root = (TreeNode) rootObject;
                            buildFoundNode(root, actualSearchString);
                        }
                    }
                }
            } else {
                foundTreeNodes.clear();
            }

            if (!foundTreeNodes.isEmpty()) {
                if (next) {
                    if (actualIndex < (foundTreeNodes.size() - 1)) {
                        actualIndex++;
                    } else {
                        actualIndex = 0;
                    }
                } else {
                    if (actualIndex > 0) {
                        actualIndex--;
                    } else {
                        actualIndex = foundTreeNodes.size() - 1;
                    }
                }

                TreeNode node = foundTreeNodes.get(actualIndex);
                if (node != null && node instanceof DefaultMutableTreeNode) {
                    TreePath toSelect = new TreePath(((DefaultMutableTreeNode) node).getPath());
                    expandPath(toSelect);
                    setSelectionPath(toSelect);
                    setLeadSelectionPath(toSelect);
                }
            }
        }
    }

    private void buildFoundNode(TreeNode node, String searchString) {
        if ((node != null)) {
            String nodeName;
            if (node instanceof DefaultMutableTreeNode) {
                nodeName = getNodeName((DefaultMutableTreeNode) node);
            } else {
                nodeName = getDefaultName(node);
            }
            if (nodeMatch(nodeName, searchString)) {
                foundTreeNodes.add(node);
            }

            int childCount = node.getChildCount();
            TreeNode childNode = null;
            for (int i = 0; i < childCount; i++) {
                childNode = node.getChildAt(i);
                buildFoundNode(childNode, searchString);
            }
        }
    }

    protected String getNodeName(DefaultMutableTreeNode node) {
        String result;
        TreeCellRenderer renderer = getCellRenderer();
        if ((renderer == null) || (node == null)) {
            result = getDefaultName(node);
        } else {
            Component comp = renderer.getTreeCellRendererComponent(this, node, false, false, node.isLeaf(),
                    getRowForPath(new TreePath(node.getPath())), false);
            if (comp instanceof JLabel) {
                result = ((JLabel) comp).getText();
            } else if (comp instanceof JTextComponent) {
                result = ((JTextComponent) comp).getText();
            } else {
                result = getDefaultName(node);
            }
        }
        return result;
    }

    protected String getDefaultName(TreeNode node) {
        return (node == null ? null : node.toString());
    }

    private boolean nodeMatch(String nodeName, String searchString) {
        boolean match = false;
        if ((searchString != null) && (!searchString.isEmpty()) && (nodeName != null)) {
            String lowerNodeName = nodeName.toLowerCase();
            if (lowerNodeName.indexOf(searchString) > -1) {
                match = true;
            }
        }
        return match;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame(ExpandableTree.class.getSimpleName() + " test");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());

        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("ROOT");
        rootNode.add(new DefaultMutableTreeNode("child 1"));
        rootNode.add(new DefaultMutableTreeNode("child 2"));
        rootNode.add(new DefaultMutableTreeNode("child 3"));
        DefaultMutableTreeNode node = new DefaultMutableTreeNode("child 4");
        node.add(new DefaultMutableTreeNode("toto"));
        node.add(new DefaultMutableTreeNode("tata"));
        node.add(new DefaultMutableTreeNode("titi"));
        rootNode.add(node);

        final ExpandableTree tree = new ExpandableTree(rootNode);

        mainPanel.add(tree, BorderLayout.CENTER);
        JPanel buttonPanel = new JPanel();

        JButton expandAll = new JButton("expandAll");
        expandAll.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                tree.expandAll(true);
            }
        });

        JButton collapseAll = new JButton("collapseAll");
        collapseAll.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                tree.expandAll(false);
            }
        });

        final JTextField textField = new JTextField();
        textField.setPreferredSize(new Dimension(80, 20));
        JButton searchNext = new JButton("searchNext");
        searchNext.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String text = textField.getText();
                tree.searchNextNode(text);
            }
        });

        JButton searchPrevious = new JButton("searchPrevious");
        searchPrevious.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String text = textField.getText();
                tree.searchPreviousNode(text);
            }
        });

        buttonPanel.add(expandAll);
        buttonPanel.add(collapseAll);
        buttonPanel.add(new JLabel(" search : "));
        buttonPanel.add(textField);
        buttonPanel.add(searchNext);
        buttonPanel.add(searchPrevious);

        mainPanel.add(buttonPanel, BorderLayout.NORTH);
        mainPanel.setPreferredSize(new Dimension(700, 500));

        frame.setContentPane(mainPanel);
        frame.setVisible(true);
        frame.pack();
    }
}
