/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.dialog;

import java.awt.Component;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * A {@link JDialog} that has an anchor, just like a {@link JFrame}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class JAnchoredDialog extends JDialog {

    private static final long serialVersionUID = 5099529640728095857L;

    protected final HiddenOwner hiddenOwner;

    public JAnchoredDialog() {
        this(null, null, false, null);
    }

    public JAnchoredDialog(Window owner) {
        this(owner, null, false, null);
    }

    public JAnchoredDialog(Window owner, boolean modal) {
        this(owner, null, modal, null);
    }

    public JAnchoredDialog(Window owner, String title) {
        this(owner, title, false, null);
    }

    public JAnchoredDialog(Window owner, String title, boolean modal) {
        this(owner, title, modal, null);
    }

    public JAnchoredDialog(Window owner, String title, boolean modal, GraphicsConfiguration gc) {
        super((owner == null ? new JFrame() : owner), title, (modal ? DEFAULT_MODALITY_TYPE : ModalityType.MODELESS),
                gc);
        hiddenOwner = new HiddenOwner();
        updateOwnerAfterInitialization();
    }

    private void updateOwnerAfterInitialization() {
        hiddenOwner.setDialog(this);
        hiddenOwner.setTitle(getTitle());
        hiddenOwner.setUndecorated(true);
        hiddenOwner.setSize(0, 0);
    }

    /**
     * Sets the image to be displayed in the minimized icon for this {@link JAnchoredDialog}. Not
     * all platforms support the concept of minimizing a window.
     * 
     * @param image the icon image to be displayed. If this parameter is <code>null</code> then the
     *            icon image is set to the default image, which may vary with platform.
     * @see #getIconImage
     */
    @Override
    public void setIconImage(Image image) {
        hiddenOwner.setIconImage(image);
        super.setIconImage(image);
        if (isVisible()) {
            repaint();
        }
    }

    @Override
    public void setLocation(int x, int y) {
        super.setLocation(x, y);
        if (hiddenOwner != null) {
            hiddenOwner.setLocation(x, y);
        }
    }

    @Override
    public void setLocation(Point p) {
        super.setLocation(p);
        if (hiddenOwner != null) {
            hiddenOwner.setLocation(p);
        }
    }

    @Override
    public void setLocationRelativeTo(Component c) {
        super.setLocationRelativeTo(c);
        hiddenOwner.setLocationRelativeTo(c);
    }

    @Override
    public void setTitle(String title) {
        super.setTitle(title);
        if (hiddenOwner != null) {
            hiddenOwner.setTitle(title);
        }
    }

    @Override
    public void setVisible(boolean visible) {
        try {
            if (visible) {
                // be sure the frame is hidden behind the dialog
                if ((hiddenOwner != null) && (!hiddenOwner.isVisible())) {
                    hiddenOwner.setVisible(visible);
                }
                setVisibleNoCheck(visible);
            } else {
                // be sure the dialog is hidden and releases modality
                setVisibleNoCheck(visible);
                if ((hiddenOwner != null) && hiddenOwner.isVisible()) {
                    hiddenOwner.setVisible(visible);
                }
            }
        } catch (NullPointerException npe) {
            // Raphaël GIRARDOT: Sometimes, such an exception is thrown because
            // getGraphics() returns null, and Java was not programmed to handle
            // this. Though, it does not avoid the dialog to be displayed or
            // hidden. That's why this exception is silently caught.
        }
    }

    @Override
    public void dispose() {
        hiddenOwner.dispose();
        super.dispose();
    }

    protected void setVisibleNoCheck(boolean visible) {
        super.setVisible(visible);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    // This class is used to force dialog visibility when clicking on anchor
    protected final class HiddenOwner extends JFrame {

        private static final long serialVersionUID = 27415134835040624L;

        protected JAnchoredDialog dialog;

        public HiddenOwner() {
            super();
            HiddenOwner.this.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    if (dialog != null) {
                        dialog.setVisibleNoCheck(false);
                    }
                }

                @Override
                public void windowIconified(WindowEvent e) {
                    if (dialog != null) {
                        if (dialog.isModal()) {
                            dialog.toBack();
                        } else {
                            dialog.setVisibleNoCheck(false);
                        }
                    }
                }

                @Override
                public void windowDeiconified(WindowEvent e) {
                    if (dialog != null) {
                        if (dialog.isModal()) {
                            dialog.toFront();
                        } else {
                            dialog.setVisibleNoCheck(true);
                        }
                    }
                }

                @Override
                public void windowOpened(WindowEvent e) {
                    if (dialog != null) {
                        dialog.setVisibleNoCheck(true);
                    }
                }
            });
        }

        public void setDialog(JAnchoredDialog dialog) {
            this.dialog = dialog;
        }

        @Override
        public void toFront() {
            if (dialog != null) {
                dialog.toFront();
            }
        }
    }

}
