/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.icons;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A class which allows to have decorated icons
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DecorableIcon extends ImageIcon implements SwingConstants {

    private static final long serialVersionUID = -2723600527087712705L;

    private boolean decorated;
    private ImageIcon decoration;
    private int decorationHorizontalAlignment;
    private int decorationVerticalAlignment;
    private BufferedImage adaptedImage;

    /**
     * Constructor
     * 
     * @see ImageIcon#ImageIcon()
     */
    public DecorableIcon() {
        super();
        init();
    }

    /**
     * Constructor
     * 
     * @param location The URL of the image
     * 
     * @see ImageIcon#ImageIcon(URL)
     */
    public DecorableIcon(URL location) {
        super(location);
        init();
    }

    /**
     * Constructor
     * 
     * @param imageData an array of pixels in an image format supported by the AWT Toolkit, such as
     *            GIF, JPEG, or (as of 1.3) PNG
     * @param description a brief textual description of the image
     * 
     * @see ImageIcon#ImageIcon(byte[], String)
     */
    public DecorableIcon(byte[] imageData, String description) {
        super(imageData, description);
        init();
    }

    /**
     * Constructor
     * 
     * @param imageData an array of pixels in an image format supported by the AWT Toolkit, such as
     *            GIF, JPEG, or (as of 1.3) PNG
     * 
     * @see ImageIcon#ImageIcon(byte[])
     */
    public DecorableIcon(byte[] imageData) {
        super(imageData);
        init();
    }

    /**
     * Constructor
     * 
     * @param image the image
     * @param description a brief textual description of the image
     * 
     * @see ImageIcon#ImageIcon(Image, String)
     */
    public DecorableIcon(Image image, String description) {
        super(image, description);
        init();
    }

    /**
     * Constructor
     * 
     * @param image the image
     * 
     * @see ImageIcon#ImageIcon(Image)
     */
    public DecorableIcon(Image image) {
        super(image);
        init();
    }

    /**
     * Constructor
     * 
     * @param filename the name of the file containing the image
     * @param description a brief textual description of the image
     * 
     * @see ImageIcon#ImageIcon(String, String)
     */
    public DecorableIcon(String filename, String description) {
        super(filename, description);
        init();
    }

    /**
     * Constructor
     * 
     * @param filename a String specifying a filename or path
     * 
     * @see ImageIcon#ImageIcon(String)
     */
    public DecorableIcon(String filename) {
        super(filename);
        init();
    }

    /**
     * Constructor
     * 
     * @param location the URL for the image
     * @param description a brief textual description of the image
     * 
     * @see ImageIcon#ImageIcon(URL, String)
     */
    public DecorableIcon(URL location, String description) {
        super(location, description);
        init();
    }

    protected void init() {
        adaptedImage = null;
        decorated = false;
        decoration = null;
        decorationHorizontalAlignment = RIGHT;
        decorationVerticalAlignment = TOP;
        redrawImage();
    }

    @Override
    public Image getImage() {
        return adaptedImage == null ? super.getImage() : adaptedImage;
    }

    @Override
    public void setImage(Image image) {
        super.setImage(image);
        redrawImage();
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        if (getImageObserver() == null) {
            g.drawImage(getImage(), x, y, c);
        } else {
            g.drawImage(getImage(), x, y, getImageObserver());
        }
    }

    protected void redrawImage() {
        BufferedImage tmp = new BufferedImage(getIconWidth(), getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics g2 = tmp.createGraphics();
        g2.drawImage(super.getImage(), 0, 0, null);
        // buffer decoration to avoid concurrency problems
        ImageIcon tempDecoration = decoration;
        if (decorated && (tempDecoration != null)) {
            int decorationX, decorationY;
            switch (decorationHorizontalAlignment) {
                case LEFT:
                    decorationX = 0;
                    break;
                case CENTER:
                    decorationX = (getIconWidth() - tempDecoration.getIconWidth()) / 2;
                    break;
                case RIGHT:
                default:
                    decorationX = getIconWidth() - tempDecoration.getImage().getWidth(null);
                    break;
            }
            switch (decorationVerticalAlignment) {
                case BOTTOM:
                    decorationY = getIconHeight() - tempDecoration.getIconHeight();
                    break;
                case CENTER:
                    decorationY = (getIconHeight() - tempDecoration.getIconHeight()) / 2;
                    break;
                case TOP:
                default:
                    decorationY = 0;
                    break;
            }
            g2.drawImage(tempDecoration.getImage(), decorationX, decorationY, getImageObserver());
        }
        adaptedImage = tmp;
    }

    /**
     * Returns whether the icon will be decorated
     * 
     * @return a <code>boolean</code> to know whether the icon will be decorated
     */
    public boolean isDecorated() {
        return decorated;
    }

    /**
     * Sets whether you want to have a decorated icon or not. To take effect, the associated
     * decoration icon must not be <code>null</code>.
     * 
     * @param decorated a <code>boolean</code> to know whether you want to have a decorated icon or
     *            not.
     * @see #setDecoration(ImageIcon)
     */
    public void setDecorated(boolean decorated) {
        if (decorated != this.decorated) {
            this.decorated = decorated;
            redrawImage();
        }
    }

    /**
     * Returns the {@link ImageIcon} used as decoration of this icon
     * 
     * @return the {@link ImageIcon} used as decoration of this icon
     */
    public ImageIcon getDecoration() {
        return decoration;
    }

    /**
     * Sets the {@link ImageIcon} used as decoration of this icon
     * 
     * @param decoration The {@link ImageIcon} used as decoration of this icon. If <code>null</code> , this icon will
     *            never be decorated.
     */
    public void setDecoration(ImageIcon decoration) {
        if (!ObjectUtils.sameObject(decoration, this.decoration)) {
            this.decoration = decoration;
            redrawImage();
        }
    }

    /**
     * Returns the decoration icon horizontal alignment
     * 
     * @return An int
     * @see SwingConstants#LEFT
     * @see SwingConstants#CENTER
     * @see SwingConstants#RIGHT
     */
    public int getDecorationHorizontalAlignment() {
        return decorationHorizontalAlignment;
    }

    /**
     * Sets the decoration icon horizontal alignment
     * 
     * @param decorationHorizontalAlignment The alignment to set.
     * @see SwingConstants#LEFT
     * @see SwingConstants#CENTER
     * @see SwingConstants#RIGHT
     */
    public void setDecorationHorizontalAlignment(int decorationHorizontalAlignment) {
        if (this.decorationHorizontalAlignment != decorationHorizontalAlignment) {
            this.decorationHorizontalAlignment = decorationHorizontalAlignment;
            redrawImage();
        }
    }

    /**
     * Returns the decoration vertical alignment
     * 
     * @return An int
     * @see SwingConstants#TOP
     * @see SwingConstants#CENTER
     * @see SwingConstants#BOTTOM
     */
    public int getDecorationVerticalAlignment() {
        return decorationVerticalAlignment;
    }

    /**
     * Sets the decoration vertical alignment
     * 
     * @param decorationVerticalAlignment The alignment to set.
     * @see SwingConstants#TOP
     * @see SwingConstants#CENTER
     * @see SwingConstants#BOTTOM
     */
    public void setDecorationVerticalAlignment(int decorationVerticalAlignment) {
        if (this.decorationVerticalAlignment != decorationVerticalAlignment) {
            this.decorationVerticalAlignment = decorationVerticalAlignment;
            redrawImage();
        }
    }

    // TODO readObject and writeObject (serialization) methods not yet defined

}
