/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 * A basic tool used to monitor memory use
 * 
 * @author Rapha&euml;l GIRARDOT
 * @deprecated You should use LoggedMemoryChecker from ApplicationUtilities instead.
 */
@Deprecated
public class MemoryChecker extends AMemoryChecker {

    private static final long serialVersionUID = -2921199003616771238L;

    public MemoryChecker() {
        super();
    }

    @Override
    protected void treatError(Throwable t) {
        t.printStackTrace();
    }

    public static void main(String[] args) {
        MemoryChecker checker = new MemoryChecker();
        checker.setGCButtonVisible(true);
        JFrame testFrame = new JFrame(checker.getClass().getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setContentPane(checker);
        testFrame.pack();
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
        checker.start();
    }

}
