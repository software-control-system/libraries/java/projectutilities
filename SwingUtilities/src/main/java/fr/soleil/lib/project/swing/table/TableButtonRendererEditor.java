/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.table;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

public class TableButtonRendererEditor extends AbstractCellEditor
        implements TableCellRenderer, TableCellEditor, ActionListener {

    private static final long serialVersionUID = -1495657778268985676L;
    private final JButton button;
    private final List<ITableButtonActionListener> actionListenerList;
    private TableEvent currentEvent = null;

    public TableButtonRendererEditor() {
        button = new JButton();
        actionListenerList = new ArrayList<ITableButtonActionListener>();
        button.addActionListener(this);
    }

    public JButton getButton() {
        return button;
    }

    public void setCellRendererEditor(TableColumn tableCol) {
        if (tableCol != null) {
            tableCol.setCellRenderer(this);
            tableCol.setCellEditor(this);
        }
    }

    public void addTableButtonActionListener(ITableButtonActionListener actionListener) {
        if (!actionListenerList.contains(actionListener)) {
            actionListenerList.add(actionListener);
        }
    }

    public void removeTableButtonActionListener(ITableButtonActionListener actionListener) {
        if (actionListenerList.contains(actionListener)) {
            actionListenerList.remove(actionListener);
        }
    }

    @Override
    public Object getCellEditorValue() {
        return currentEvent != null ? currentEvent.getObject() : null;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        return button;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        setAction(table, value, row, column);
        return button;
    }

    private void setAction(JTable table, Object value, int row, int column) {
        currentEvent = new TableEvent(table, value, row, column);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for (ITableButtonActionListener actionListener : actionListenerList) {
            actionListener.actionPerformed(currentEvent);
        }
        stopCellEditing();
    }
}
