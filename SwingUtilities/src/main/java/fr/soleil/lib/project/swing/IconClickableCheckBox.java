/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import javax.swing.Icon;
import javax.swing.JCheckBox;

/**
 * A {@link JCheckBox} that is only clickable in the icon zone
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class IconClickableCheckBox extends AIconClickableCheckBox<JCheckBox> {

    private static final long serialVersionUID = -992884200441704326L;

    public IconClickableCheckBox() {
        super();
    }

    public IconClickableCheckBox(Icon icon, boolean selected) {
        super(icon, selected);
    }

    public IconClickableCheckBox(Icon icon) {
        super(icon);
    }

    public IconClickableCheckBox(String text, boolean selected) {
        super(text, selected);
    }

    public IconClickableCheckBox(String text, Icon icon, boolean selected) {
        super(text, icon, selected);
    }

    public IconClickableCheckBox(String text, Icon icon) {
        super(text, icon);
    }

    public IconClickableCheckBox(String text) {
        super(text);
    }

    @Override
    protected JCheckBox generateCheckBox() {
        return new JCheckBox();
    }

}
