/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.renderer;

import java.awt.Font;

import fr.soleil.lib.project.log.Level;

/**
 * An interface for something able to render {@link Level}s.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface ILevelRenderer {

    /**
     * Returns whether text coloring according to {@link Level} is enabled.
     * 
     * @return A <code>boolean</code>. <code>true</code> by default.
     */
    public boolean isLevelColorEnabled();

    /**
     * Enables/disables text coloring according to {@link Level}.
     * 
     * @param levelColorEnabled Whether text coloring according to {@link Level} should be enabled.
     */
    public void setLevelColorEnabled(boolean levelColorEnabled);

    /**
     * Returns the {@link Font} used to display levels.
     * 
     * @return A {@link Font}.
     */
    public Font getLevelFont();

    /**
     * Sets the {@link Font} that can be used to display levels.
     * 
     * @param font The {@link Font} to use.
     */
    public void setLevelFont(Font font);

}
