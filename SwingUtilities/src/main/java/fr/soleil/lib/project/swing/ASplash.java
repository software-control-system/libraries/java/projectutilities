package fr.soleil.lib.project.swing;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JWindow;

import fr.soleil.lib.project.swing.panel.ASplashPanel;

/**
 * A {@link JWindow} that renders some progression.
 *
 * @param <P> The type of progress bar used by known {@link ASplashPanel}.
 * @param <S> The type of {@link ASplashPanel} used by this {@link ASplash}.
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class ASplash<P extends JComponent, S extends ASplashPanel<P>> extends JWindow {

    private static final long serialVersionUID = 8777296820665444989L;

    /** Default background color */
    protected static final Color DEFAULT_BACKGROUND = new Color(100, 110, 140);

    /** The {@link ASplashPanel} that renders progression */
    protected S splashPanel;

    /**
     * Creates a splash panel using the given image, text color and JSmoothProgressBar.<br />
     * This constructor offers the possibility to not display {@link Splash} immediately.
     * 
     * @param splashImage Splash image.
     * @param textColor Text color.
     * @param progressBar progress bar which will be used by this splash window.
     * @param startVisible Whether to display the {@link Splash} immediately.
     */
    protected ASplash(ImageIcon splashImage, Color textColor, P progressBar, boolean startVisible) {
        super();
        initComponents(splashImage, textColor, progressBar, startVisible);
    }

    protected abstract S generateSplashPanel(ImageIcon icon, Color textColor, P progressBar);

    /**
     * Initializes the different components of this {@link ASplash}.
     * 
     * @param splashImage The splash image to use. Can be <code>null</code>.
     * @param textColor The text foreground color to use. Can be <code>null</code>.
     * @param progressBar The progress bar to use. Can be <code>null</code>.
     * @param startVisible Whether to display the {@link ASplash} immediately.
     */
    protected void initComponents(ImageIcon splashImage, Color textColor, P progressBar, boolean startVisible) {
        setBackground(DEFAULT_BACKGROUND);
        splashPanel = generateSplashPanel(splashImage, textColor, progressBar);
        setContentPane(splashPanel);
        pack();
        // Center the splash window
        setLocationRelativeTo(null);
        if (startVisible) {
            setVisible(true);
        }
    }

    /**
     * Returns the copyright.
     * 
     * @return A {@link String}: the copyright.
     */
    public String getCopyright() {
        return splashPanel.getCopyright();
    }

    /**
     * Sets the copyright.
     * 
     * @param copyright The copyright to set.
     */
    public void setCopyright(String copyright) {
        splashPanel.setCopyright(copyright);
    }

    /**
     * Returns the title.
     * 
     * @return A {@link String}: the title.
     */
    public String getTitle() {
        return splashPanel.getTitle();
    }

    /**
     * Sets the title.
     * 
     * @param title The title to set.
     */
    public void setTitle(String title) {
        splashPanel.setTitle(title);
    }

    /**
     * Initializes the progress to default.
     */
    public abstract void initProgress();

}
