/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.model;

import java.util.Arrays;

import javax.swing.table.DefaultTableModel;

import fr.soleil.lib.project.log.ILogManager;
import fr.soleil.lib.project.log.Level;
import fr.soleil.lib.project.log.LogData;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.SwingUtilitiesConstants;

/**
 * A {@link DefaultTableModel} used to represent some {@link LogData}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LogTableModel extends DefaultTableModel implements ILogManager, SwingUtilitiesConstants {

    private static final long serialVersionUID = 7021489451017330049L;

    protected static final String TIMESTAMP_KEY = "fr.soleil.lib.project.log.timestamp";
    protected static final String LEVEL_KEY = "fr.soleil.lib.project.log.level";
    protected static final String MESSAGE_KEY = "fr.soleil.lib.project.log.message";

    protected static final String DEFAULT_TIMESTAMP = "Timestamp";
    protected static final String DEFAULT_LEVEL = "Level";
    protected static final String DEFAULT_MESSAGE = "Message";

    protected MessageManager messageManager;
    protected final Object logsLock;
    protected volatile LogData[] data;
    protected volatile int maxLogs;

    /**
     * Constructs a new {@link LogTableModel}.
     */
    public LogTableModel() {
        this(DEFAULT_MESSAGE_MANAGER);
    }

    /**
     * Constructs a new {@link LogTableModel}, using a {@link MessageManager} to recover column names.
     * 
     * @param messageManager The {@link MessageManager} to use to recover column names.
     */
    public LogTableModel(MessageManager messageManager) {
        super();
        this.messageManager = messageManager;
        this.logsLock = new Object();
        this.data = null;
        this.maxLogs = Integer.MAX_VALUE;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Class<?> columnClass;
        switch (columnIndex) {
            case 0:
            case 2:
                columnClass = String.class;
                break;
            case 1:
                columnClass = Level.class;
                break;
            default:
                columnClass = Object.class;
                break;
        }
        return columnClass;
    }

    @Override
    public String getColumnName(int column) {
        String columnName;
        switch (column) {
            case 0:
                columnName = getMessage(TIMESTAMP_KEY, DEFAULT_TIMESTAMP);
                break;
            case 1:
                columnName = getMessage(LEVEL_KEY, DEFAULT_LEVEL);
                break;
            case 2:
                columnName = getMessage(MESSAGE_KEY, DEFAULT_MESSAGE);
                break;
            default:
                columnName = null;
                break;
        }
        return columnName;
    }

    @Override
    public int getRowCount() {
        LogData[] data = this.data;
        return data == null ? 0 : data.length;
    }

    /**
     * Utility method to get a message from {@link MessageManager}, returning a default value when not found.
     * 
     * @param key The key to search for in {@link MessageManager}.
     * @param defaultValue The default value to return when not found.
     * @return A {@link String}.
     */
    protected final String getMessage(String key, String defaultValue) {
        String message;
        MessageManager messageManager = this.messageManager;
        if ((messageManager == null) || (key == null)) {
            message = defaultValue;
        } else {
            message = messageManager.getMessage(key);
            // Here, we do want to test for strict equality, as MessageManager returns key when not found.
            if (message == key) {
                message = defaultValue;
            }
        }
        return message;
    }

    @Override
    public LogData[] getLogs() {
        LogData[] logs = this.data;
        return logs == null ? null : logs.clone();
    }

    protected int getLength(LogData... logs) {
        return logs == null ? 0 : logs.length;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    @Override
    public Object getValueAt(int row, int column) {
        Object value;
        LogData[] logs = this.data;
        if ((logs != null) && (row > -1) && (row < logs.length)) {
            LogData log = logs[row];
            if (log == null) {
                value = null;
            } else {
                switch (column) {
                    case 0:
                        value = log.getTimestamp();
                        break;
                    case 1:
                        value = log.getLevel();
                        break;
                    case 2:
                        value = log.getMessage();
                        break;
                    default:
                        value = null;
                        break;
                }
            }
        } else {
            value = null;
        }
        return value;
    }

    @Override
    public void setLogs(LogData... logs) {
        setLogs(false, logs);
    }

    @Override
    public void setLogs(boolean keepLastLogs, LogData... logs) {
        boolean changed;
        synchronized (logsLock) {
            changed = setLogsAndCheckChangeNoLock(keepLastLogs, logs);
        }
        if (changed) {
            fireTableDataChanged();
        }
    }

    protected boolean setLogsAndCheckChangeNoLock(boolean keepLastLogs, LogData... logs) {
        boolean changed = false;
        LogData[] newLogs = ArrayUtils.getLimitedArray(logs, maxLogs, keepLastLogs);
        if (this.data != newLogs) {
            this.data = newLogs;
            changed = true;
        }
        return changed;
    }

    @Override
    public void addLogs(LogData... logs) {
        boolean changed;
        synchronized (logsLock) {
            changed = setLogsAndCheckChangeNoLock(true, ArrayUtils.addDataToArray(data, logs));
        }
        if (changed) {
            fireTableDataChanged();
        }
    }

    @Override
    public void clearLogs() {
        setLogs((LogData[]) null);
    }

    @Override
    public int getMaxLogs() {
        return maxLogs;
    }

    @Override
    public void setMaxLogs(int maxLogs) {
        if (this.maxLogs != maxLogs) {
            this.maxLogs = maxLogs;
            boolean changed = false;
            LogData[] logs = this.data;
            if ((logs != null) && (logs.length > maxLogs)) {
                synchronized (logsLock) {
                    changed = setLogsAndCheckChangeNoLock(true, Arrays.copyOf(logs, maxLogs));
                }
            }
            if (changed) {
                fireTableDataChanged();
            }
        }
    }

}
