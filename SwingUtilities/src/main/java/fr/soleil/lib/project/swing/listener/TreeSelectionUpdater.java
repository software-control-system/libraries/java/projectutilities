/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.listener;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTree;

/**
 * A {@link MouseAdapter} that is able to update a {@link JTree} selection paths on any mouse button
 * pressed, not only the 1st one.
 * <p>
 * You can choose whether to use advanced selection. In this case, when clicking on tree without
 * pressing [control] or [shift], if the corresponding node is already selected, nothing is done.
 * Otherwise, the selection is replaced by the corresponding node. By default, advanced selection is
 * not used (i.e. the selection is always replaced)
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class TreeSelectionUpdater extends MouseAdapter {

    private boolean useAdvancedSelection;

    /**
     * Default contructor
     */
    public TreeSelectionUpdater() {
        this(false);
    }

    /**
     * Constructs a new {@link TreeSelectionUpdater} with the possibility to use (or not) advanced
     * selection
     * 
     * @param useAdvancedSelection Whether to use advanced selection.
     */
    public TreeSelectionUpdater(boolean useAdvancedSelection) {
        super();
        this.useAdvancedSelection = useAdvancedSelection;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if ((e != null) && (e.getSource() instanceof JTree)) {
            JTree tree = (JTree) e.getSource();
            // Manage selection for other buttons than 1st one
            // (the 1st one is already managed by the JTree)
            if (e.getButton() != MouseEvent.BUTTON1) {
                int rowToSelect = tree.getRowForLocation(e.getX(), e.getY());
                int[] selectedRows = tree.getSelectionRows();
                if (e.isShiftDown()) {
                    if ((selectedRows == null) || (selectedRows.length == 0)) {
                        tree.setSelectionRow(rowToSelect);
                    } else {
                        int[] rowsToSelect = null;
                        if (selectedRows[0] < rowToSelect) {
                            rowsToSelect = new int[1 + rowToSelect - selectedRows[0]];
                            for (int i = 0; i < rowsToSelect.length; i++) {
                                rowsToSelect[i] = i + selectedRows[0];
                            }
                        } else {
                            rowsToSelect = new int[1 + selectedRows[0] - rowToSelect];
                            for (int i = 0; i < rowsToSelect.length; i++) {
                                rowsToSelect[i] = selectedRows[0] - i;
                            }
                        }
                        tree.setSelectionRows(rowsToSelect);
                    }
                } else if (e.isControlDown()) {
                    boolean canAdd = true;
                    if (selectedRows != null) {
                        for (int i = 0; i < selectedRows.length; i++) {
                            if (selectedRows[i] == rowToSelect) {
                                canAdd = false;
                                break;
                            }
                        }
                        if (canAdd) {
                            tree.addSelectionRow(rowToSelect);
                        } else {
                            tree.removeSelectionRow(rowToSelect);
                        }
                    }
                } else {
                    boolean canReplace = true;
                    if (useAdvancedSelection) {
                        int[] currentSelection = tree.getSelectionRows();
                        if (currentSelection != null) {
                            for (int row : currentSelection) {
                                if (row == rowToSelect) {
                                    canReplace = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (canReplace) {
                        tree.setSelectionRow(rowToSelect);
                    }
                }
                tree.grabFocus();
                tree.setLeadSelectionPath(tree.getPathForRow(rowToSelect));
            }
        }
    }

}
