/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.renderer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import fr.soleil.lib.project.log.Level;
import fr.soleil.lib.project.swing.SwingUtilitiesConstants;

/**
 * A {@link MultiLinesCellRenderer} dedicated in log data displaying.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LogTableCellRenderer extends MultiLinesCellRenderer implements ILevelRenderer, SwingUtilitiesConstants {

    private static final long serialVersionUID = -317908810083347422L;

    /**
     * Default darker color to use every second grid line.
     */
    public static final Color DEFAULT_DARKER_COLOR = new Color(245, 245, 245);

    protected static final int DEFAULT_VERTICAL_MARGIN = 1;

    protected final JLabel centerLabel, topLabel;
    protected final JPanel topLabelPanel;
    protected int verticalMargin;
    protected Border verticalMarginBorder;
    protected volatile boolean colorEnabled;
    protected Font levelFont;
    protected Color alternateColor;
    protected boolean useAlternateColor;

    /**
     * Default constructor.
     */
    public LogTableCellRenderer() {
        this(false);
    }

    /**
     * Constructor.
     * 
     * @param alwaysUseTextArea Whether this {@link LogTableCellRenderer} should always use a {@link JTextArea} to
     *            render data.
     */
    public LogTableCellRenderer(boolean alwaysUseTextArea) {
        super(alwaysUseTextArea);
        centerLabel = new JLabel();
        centerLabel.setVerticalAlignment(SwingConstants.CENTER);
        centerLabel.setOpaque(true);
        topLabel = new JLabel();
        topLabel.setVerticalAlignment(SwingConstants.TOP);
        topLabelPanel = new JPanel(new BorderLayout());
        topLabelPanel.add(topLabel, BorderLayout.CENTER);
        verticalMargin = DEFAULT_VERTICAL_MARGIN;
        updateVeticalMarginBorder();
        colorEnabled = true;
        levelFont = DEFAULT_LOG_FONT;
        immediatelyComputeBestHeight = false;
        useAlternateColor = true;
        alternateColor = DEFAULT_DARKER_COLOR;
    }

    /**
     * Return the alternate color used every second grid line.
     * 
     * @return A {@link Color}.
     */
    public Color getAlternateColor() {
        return alternateColor;
    }

    /**
     * Sets the alternate color to use every second grid line.
     * 
     * @param alternateColor The alternate color to use every second grid line.
     */
    public void setAlternateColor(Color alternateColor) {
        this.alternateColor = alternateColor == null ? DEFAULT_DARKER_COLOR : alternateColor;
    }

    /**
     * Returns whether this {@link LogTableCellRenderer} will use alternate color, i.e. a darker color, every second
     * grid line.
     * 
     * @return A <code>boolean></code>.
     */
    public boolean isUseAlternateColor() {
        return useAlternateColor;
    }

    /**
     * Sets whether this {@link LogTableCellRenderer} can use alternate color, i.e. a darker color, every second
     * grid line.
     * 
     * @param useAlternateColor Whether this {@link LogTableCellRenderer} can use alternate color.
     */
    public void setUseAlternateColor(boolean useAlternateColor) {
        this.useAlternateColor = useAlternateColor;
    }

    /**
     * Returns the cell vertical margin used by this {@link LogTableCellRenderer}.
     * 
     * @return An <code>int</code>.
     */
    public int getVerticalMargin() {
        return verticalMargin;
    }

    /**
     * Sets the cell vertical margin this {@link LogTableCellRenderer} should use.
     * 
     * @param verticalMargin The cell vertical margin this {@link LogTableCellRenderer} should use.
     *            <p>
     *            Use a value &lt; 0 to reset to default value.
     *            </p>
     */
    public void setVerticalMargin(int verticalMargin) {
        if (verticalMargin != this.verticalMargin) {
            this.verticalMargin = verticalMargin < 0 ? DEFAULT_VERTICAL_MARGIN : verticalMargin;
            updateVeticalMarginBorder();
        }
    }

    /**
     * Computes the best verticalMarginBorder according to verticalMargin.
     */
    protected void updateVeticalMarginBorder() {
        int top = verticalMargin / 2, bottom = verticalMargin - top;
        verticalMarginBorder = new EmptyBorder(top, 0, bottom, 0);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        JComponent comp = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                column);
        Color bg = comp.getBackground();
        int realRow = table.convertRowIndexToModel(row);
        int realCol = table.convertColumnIndexToModel(column);
        if (comp instanceof JLabel) {
            JLabel tmp = (JLabel) comp;
            if (realCol == 2) {
                prepareLabel(tmp, centerLabel, realCol);
                comp = centerLabel;
            } else {
                String message = String.valueOf(table.getModel().getValueAt(realRow, 2));
                if (message.indexOf('\n') > -1) {
                    prepareLabel(tmp, topLabel, realCol);
                    topLabelPanel.setBorder(topLabel.getBorder());
                    topLabel.setBorder(verticalMarginBorder);
                    comp = topLabelPanel;
                } else {
                    prepareLabel(tmp, centerLabel, realCol);
                    comp = centerLabel;
                }
            }
            comp.setToolTipText(tmp.getToolTipText());
        }
        if (isUseAlternateColor() && (!isSelected) && (row % 2 == 1)) {
            bg = alternateColor;
        }
        Level level = null;
        if (value instanceof Level) {
            level = (Level) value;
        }
        try {
            Object tmp = table.getModel().getValueAt(realRow, 1);
            if (tmp instanceof Level) {
                level = (Level) tmp;
            } else if (tmp instanceof String) {
                level = Level.parseLevel((String) tmp);
            }
        } catch (Exception e) {
            level = null;
        }
        Color fg = isSelected ? table.getSelectionForeground() : table.getForeground();
        Font f = levelFont;
        if (level != null) {
            if (colorEnabled) {
                switch (level) {
                    case ERROR:
                        fg = ERROR_COLOR;
                        break;
                    case WARNING:
                        fg = WARNING_COLOR;
                        break;
                    case INFO:
                        fg = INFO_COLOR;
                        break;
                    case DEBUG:
                        fg = DEBUG_COLOR;
                        break;
                    case TRACE:
                        fg = TRACE_COLOR;
                        break;
                    default:
                        // nothing to do
                        break;
                }
            }
        }
        updateFontAndColors(comp, f, fg, bg);
        updateFontAndColors(centerLabel, f, fg, bg);
        updateFontAndColors(topLabel, f, fg, bg);
        updateFontAndColors(area, f, fg, bg);
        updateFontAndColors(centeringPanel, f, fg, bg);
        applyBestHeight(table, row, comp);
        return comp;
    }

    @Override
    protected int computeBestHeight(JComponent comp, JTable table) {
        return super.computeBestHeight(comp, table) + (comp == topLabelPanel ? 0 : verticalMargin);
    }

    @Override
    public boolean isLevelColorEnabled() {
        return colorEnabled;
    }

    @Override
    public void setLevelColorEnabled(boolean levelColorEnabled) {
        this.colorEnabled = levelColorEnabled;
    }

    @Override
    public Font getLevelFont() {
        return levelFont;
    }

    @Override
    public void setLevelFont(Font levelFont) {
        this.levelFont = levelFont == null ? DEFAULT_LOG_FONT : levelFont;
    }

    /**
     * Prepares a {@link JLabel} to render a table, according to default renderer {@link JLabel}.
     * 
     * @param tmp The default renderer {@link JLabel}.
     * @param label The {@link JLabel} to prepare.
     * @param realCol The column in table model coordinates.
     */
    protected void prepareLabel(JLabel tmp, JLabel label, int realCol) {
        label.setForeground(tmp.getForeground());
        label.setBackground(tmp.getBackground());
        label.setOpaque(tmp.isOpaque());
        label.setText(tmp.getText());
        label.setToolTipText(tmp.getToolTipText());
        label.setBorder(tmp.getBorder());
        label.setFont(levelFont);
        if (realCol == 1) {
            label.setHorizontalAlignment(CENTER);
        } else {
            label.setHorizontalAlignment(LEFT);
        }
    }

    /**
     * Changes a {@link Component}'s font, foreground color and background color.
     * 
     * @param component The {@link Component}.
     * @param f The font to set.
     * @param fg The foreground color to set.
     * @param bg the background color to set.
     */
    protected void updateFontAndColors(Component component, Font f, Color fg, Color bg) {
        component.setForeground(fg);
        component.setFont(f);
        component.setBackground(bg);
    }

}
