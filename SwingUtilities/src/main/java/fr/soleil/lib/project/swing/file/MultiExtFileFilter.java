/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.filechooser.FileFilter;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.file.FileUtils;

/**
 * A file filter that can accept multiple extensions (as for jpg, jpe, jpeg). The description is
 * automatically computed with the prefix followed by the list of extensions.
 * 
 * @author MAINGUY
 * 
 */
public class MultiExtFileFilter extends FileFilter {

    protected String prefix;
    protected List<String> extensions;
    protected String description;

    /**
     * @param filterDescription the description of the type of accepted files
     * @param defaultExtension a default extension. It ensures we have at least one extension.
     * @param otherExtensions more extensions for this type of files
     */
    public MultiExtFileFilter(String filterDescription, String defaultExtension, String... otherExtensions) {
        super();

        if (filterDescription == null) {
            filterDescription = ObjectUtils.EMPTY_STRING;
        }
        prefix = filterDescription;

        extensions = new ArrayList<String>();
        if (defaultExtension != null) {
            extensions.add(defaultExtension);
        }
        if (otherExtensions != null) {
            for (String ext : otherExtensions) {
                if (ext != null) {
                    extensions.add(ext);
                }
            }
        }

        computeDescription();
    }

    @Override
    public boolean accept(File f) {
        boolean result = false;
        if (f != null) {
            // we accept directories
            result |= f.isDirectory();

            // file extension must
            String fileExtension = FileUtils.getExtension(f);
            if (fileExtension != null) {
                result |= isAccepted(fileExtension);
            }
        }
        return result;
    }

    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Return true if the specified extension is accepted by the filter.
     * 
     * @param fileExtension the fileExtension to test
     * @return true if the filter would accept this extension, false otherwise or if the extension
     *         is null
     */
    public boolean isAccepted(String fileExtension) {
        boolean result = false;
        for (String extension : extensions) {
            result |= extension.equalsIgnoreCase(fileExtension);
        }
        return result;
    }

    /**
     * Return the default extension. It can be useful when saving a file whose filename has no
     * extension specified.
     * 
     * @return the default extension.
     */
    public String getDefaultExtension() {
        return extensions.get(0);
    }

    /**
     * Compute the whole description for the filter. User description is followed by allowed
     * extensions as a list.
     */
    protected void computeDescription() {
        StringBuilder sb = new StringBuilder(prefix);
        sb.append(" (");
        // there is at least the default one
        for (String ext : extensions) {
            sb.append("*.").append(ext);
            sb.append(", ");
        }
        if (extensions.size() > 0) {
            sb.delete(sb.length() - 2, sb.length());
        }

        sb.append(")");
        description = sb.toString();
    }

    @Override
    public String toString() {
        return description;
    }

}
