/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import fr.soleil.lib.project.resource.MessageManager;

/**
 * This class is a {@link JTextField} that uses a {@link DocumentNumber} to validate user input. It
 * offers the possibility to display a special text (no value, error) depending on the state it is
 * in, and changes background color to indicate a user modification.
 * 
 * @author MAINGUY, Rapha&euml;l GIRARDOT
 */
public class StateFocusNumberTextField extends AStateFocusTextField {

    private static final long serialVersionUID = -7741658312810880829L;

    protected DocumentNumber document;
    protected int precision;

    /**
     * Constructs a new StateFocusNumberTextField that only accept integer values.
     */
    public StateFocusNumberTextField(MessageManager messageManager) {
        this(false, false, false, messageManager);
    }

    /**
     * Constructs a new StateFocusNumberTextField.
     * 
     * @param allowFloatValues true to allow float values, false otherwise
     * @param allowScientificValues true to allow scientific values, false otherwise
     */
    public StateFocusNumberTextField(boolean allowFloatValues, boolean allowScientificValues,
            MessageManager messageManager) {
        this(allowFloatValues, allowScientificValues, false, messageManager);
    }

    /**
     * Constructs a new StateFocusNumberTextField.
     * 
     * @param allowFloatValues true to allow float values, false otherwise
     * @param allowScientificValues true to allow scientific values, false otherwise
     */
    public StateFocusNumberTextField(boolean allowFloatValues, boolean allowScientificValues, int significantValue,
            MessageManager messageManager) {
        this(allowFloatValues, allowScientificValues, true, significantValue, messageManager);
    }

    /**
     * Constructs a new StateFocusNumberTextField.
     * 
     * @param allowFloatValues true to allow float values, false otherwise
     * @param allowScientificValues true to allow scientific values, false otherwise
     * @param allowNegativeValues true to allow negative values, false otherwise
     */
    public StateFocusNumberTextField(boolean allowFloatValues, boolean allowScientificValues,
            boolean allowNegativeValues, int significantValue, MessageManager messageManager) {
        this(allowFloatValues, allowScientificValues, allowNegativeValues, messageManager);
        this.precision = significantValue;
    }

    /**
     * Constructs a new StateFocusNumberTextField.
     * 
     * @param allowFloatValues true to allow float values, false otherwise
     * @param allowScientificValues true to allow scientific values, false otherwise
     * @param allowNegativeValues true to allow negative values, false otherwise
     */
    public StateFocusNumberTextField(boolean allowFloatValues, boolean allowScientificValues,
            boolean allowNegativeValues, MessageManager messageManager) {
        super(messageManager);
        document = new DocumentNumber();
        document.setAllowFloatValues(allowFloatValues);
        document.setAllowScientificValues(allowScientificValues);
        document.setAllowNegativeValues(allowNegativeValues);
        setDocument(document);
    }

    @Override
    protected void initialize(MessageManager messageManager) {
        precision = 2;
        super.initialize(messageManager);
    }

    @Override
    protected void beforeSetStringValue() {
        if (document != null) {
            document.setEnabled(false);
        }
    }

    @Override
    protected void afterSetStringValue() {
        if (document != null) {
            document.setEnabled(true);
        }
    }

    /**
     * Sets a number value that should fit the desired format
     * 
     * @param t the String representation of the value
     */
    public void setNumberValue(String t) {
        setValidValue(t);
    }

    /**
     * Returns the background {@link Color} used when the text represents a number
     * 
     * @return A {@link Color}
     */
    public Color getNumberBackground() {
        return getValidBackground();
    }

    /**
     * Sets the background {@link Color} to use when the text represents a number
     * 
     * @param numberBackground The {@link Color} to use
     */
    public void setNumberBackground(Color numberBackground) {
        setValidBackground(numberBackground);
    }

    /**
     * Returns the number value of this {@link StateFocusNumberTextField}, represented as a <code>byte</code>
     * 
     * @param errorValue The value to return in case of error
     * 
     * @return A <code>byte</code>
     */
    public byte getByteValue(byte errorValue) {
        byte value;
        try {
            value = Byte.parseByte(getText());
        } catch (Exception e) {
            value = errorValue;
        }
        return value;
    }

    /**
     * Returns the number value of this {@link StateFocusNumberTextField}, represented as a <code>short</code>
     * 
     * @param errorValue The value to return in case of error
     * 
     * @return A <code>short</code>
     */
    public short getShortValue(short errorValue) {
        short value;
        try {
            value = Short.parseShort(getText().trim());
        } catch (Exception e) {
            value = errorValue;
        }
        return value;
    }

    /**
     * Returns the number value of this {@link StateFocusNumberTextField}, represented as a <code>int</code>
     * 
     * @param errorValue The value to return in case of error
     * 
     * @return A <code>int</code>
     */
    public int getIntValue(int errorValue) {
        int value;
        try {
            value = Integer.parseInt(getText().trim());
        } catch (Exception e) {
            value = errorValue;
        }
        return value;
    }

    /**
     * Returns the number value of this {@link StateFocusNumberTextField}, represented as a <code>long</code>
     * 
     * @param errorValue The value to return in case of error
     * 
     * @return A <code>long</code>
     */
    public long getLongValue(long errorValue) {
        long value;
        try {
            value = Long.parseLong(getText().trim());
        } catch (Exception e) {
            value = errorValue;
        }
        return value;
    }

    /**
     * Returns the number value of this {@link StateFocusNumberTextField}, represented as a <code>float</code>
     * 
     * @return A <code>float</code>
     */
    public float getFloatValue() {
        return getFloatValue(Float.NaN);
    }

    /**
     * Returns the number value of this {@link StateFocusNumberTextField}, represented as a <code>float</code>
     * 
     * @param errorValue The value to return in case of error
     * 
     * @return A <code>float</code>
     */
    public float getFloatValue(float errorValue) {
        float value;
        try {
            value = Float.parseFloat(getText().trim());
        } catch (Exception e) {
            value = errorValue;
        }
        return value;
    }

    /**
     * Returns the number value of this {@link StateFocusNumberTextField}, represented as a <code>double</code>
     * 
     * @return A <code>double</code>
     */
    public double getDoubleValue() {
        return getDoubleValue(Double.NaN);
    }

    /**
     * Returns the number value of this {@link StateFocusNumberTextField}, represented as a <code>double</code>
     * 
     * @param errorValue The value to return in case of error
     * 
     * @return A <code>double</code>
     */
    public double getDoubleValue(double errorValue) {
        double value;
        try {
            value = Double.parseDouble(getText().trim());
        } catch (Exception e) {
            value = errorValue;
        }
        return value;
    }

    public int getPrecisionValue() {
        return precision;
    }

    /* In order to change precision of the field after instanciation */
    public void setPrecisionValue(int precision) {
        this.precision = precision;
    }

    public Font getNumberFont() {
        return getValidFont();
    }

    public void setNumberFont(Font numberFont) {
        setValidFont(numberFont);
    }

    public static void main(String[] args) throws Exception {
        JFrame mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        final StateFocusNumberTextField stateFocusNumberTextField = new StateFocusNumberTextField(true, false,
                DEFAULT_MESSAGE_MANAGER);

        final JTextField textField = new JTextField(10);
        final String es = "e", ns = "n";

        textField.addActionListener((e) -> {
            if (es.equals(textField.getText())) {
                stateFocusNumberTextField.setError();
            } else if (ns.equals(textField.getText())) {
                stateFocusNumberTextField.clear();
            } else {
                stateFocusNumberTextField.setNumberValue(textField.getText());
            }
        });

        JPanel panel = new JPanel();
        panel.add(textField);
        panel.add(stateFocusNumberTextField);

        mainFrame.getContentPane().add(panel);
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

}
