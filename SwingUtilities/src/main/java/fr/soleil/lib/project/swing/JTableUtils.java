/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.util.Arrays;

import javax.swing.JTable;

/**
 * A utility class used to interact with {@link JTable}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class JTableUtils implements SwingUtilitiesConstants {

    /**
     * Creates a new {@link StringBuilder} containing the CSV representation of a {@link JTable}, and returns it.
     * 
     * @param table The {@link JTable} from which you want to get the CSV representation
     * @return A {@link StringBuilder} containing the expected CSV representation
     */
    public static StringBuilder tableToStringBuilder(JTable table) {
        return tableToStringBuilder(table, null);
    }

    /**
     * Appends the CSV representation of a {@link JTable} to a {@link StringBuilder} and returns it.
     * 
     * @param table The {@link JTable} from which you want to get the CSV representation
     * @param tableBuilder The {@link StringBuilder} tp which to append the CSV representation. If <code>null</code>, a
     *            new one will be created and returned.
     * @return A {@link StringBuilder} containing the expected CSV representation
     */
    public static StringBuilder tableToStringBuilder(JTable table, StringBuilder tableBuilder) {
        return tableToStringBuilder(table, null, null, tableBuilder);
    }

    /**
     * Appends the CSV representation of a {@link JTable} to a {@link StringBuilder} and returns it.
     * 
     * @param table The {@link JTable} from which you want to get the CSV representation
     * @param csvColumnSeparator The column separator to use. If <code>null</code> or empty,
     *            {@link #DEFAULT_CSV_COLUMN_SEPARATOR} will be used.
     * @param csvRowSeparator The row separator to use If <code>null</code> or empty, {@link #DEFAULT_CSV_ROW_SEPARATOR}
     *            will be used.
     * @param tableBuilder The {@link StringBuilder} tp which to append the CSV representation. If <code>null</code>, a
     *            new one will be created and returned.
     * @return A {@link StringBuilder} containing the expected CSV representation
     */
    public static StringBuilder tableToStringBuilder(JTable table, String csvColumnSeparator, String csvRowSeparator,
            StringBuilder tableBuilder) {
        return tableToStringBuilder(table, csvColumnSeparator, csvRowSeparator, tableBuilder, false);
    }

    /**
     * Appends the CSV representation of a {@link JTable} to a {@link StringBuilder} and returns it.
     * 
     * @param table The {@link JTable} from which you want to get the CSV representation
     * @param csvColumnSeparator The column separator to use. If <code>null</code> or empty,
     *            {@link #DEFAULT_CSV_COLUMN_SEPARATOR} will be used.
     * @param csvRowSeparator The row separator to use If <code>null</code> or empty, {@link #DEFAULT_CSV_ROW_SEPARATOR}
     *            will be used.
     * @param tableBuilder The {@link StringBuilder} tp which to append the CSV representation. If <code>null</code>, a
     *            new one will be created and returned.
     * @param selectedRowsOnly Whether to only export selected rows in table
     * 
     * @return A {@link StringBuilder} containing the expected CSV representation
     */
    public static StringBuilder tableToStringBuilder(JTable table, String csvColumnSeparator, String csvRowSeparator,
            StringBuilder tableBuilder, boolean selectedRowsOnly) {
        StringBuilder stringValue = tableBuilder;
        if (stringValue == null) {
            stringValue = new StringBuilder();
        }
        String columnSeparator = csvColumnSeparator;
        if ((columnSeparator == null) || columnSeparator.isEmpty()) {
            columnSeparator = DEFAULT_CSV_COLUMN_SEPARATOR;
        }
        String rowSeparator = csvRowSeparator;
        if ((rowSeparator == null) || rowSeparator.isEmpty()) {
            rowSeparator = DEFAULT_CSV_ROW_SEPARATOR;
        }
        if ((table != null) && (table.getColumnCount() > 0)) {
            for (int col = 0; col < table.getColumnCount(); col++) {
                stringValue.append(table.getColumnName(col)).append(columnSeparator);
            }
            // remove last column separator
            cleanLastColumnSeparator(table, columnSeparator, stringValue);
            stringValue.append(rowSeparator);
            if (selectedRowsOnly) {
                int[] rows = table.getSelectedRows();
                if (rows != null) {
                    Arrays.sort(rows);
                    for (int row : rows) {
                        appendRow(table, row, columnSeparator, rowSeparator, stringValue);
                    }
                }
            } else {
                if (table.getRowCount() > 0) {
                    for (int row = 0; row < table.getRowCount(); row++) {
                        appendRow(table, row, columnSeparator, rowSeparator, stringValue);
                    }
                }
            }
            // remove last row separator
            int length = stringValue.length();
            stringValue.delete(length - rowSeparator.length(), length);
        }
        return stringValue;
    }

    protected static StringBuilder appendRow(JTable table, int row, String columnSeparator, String rowSeparator,
            StringBuilder stringValue) {
        for (int col = 0; col < table.getColumnCount(); col++) {
            stringValue.append(table.getValueAt(row, col)).append(columnSeparator);
        }
        // remove last column separator
        cleanLastColumnSeparator(table, columnSeparator, stringValue);
        stringValue.append(rowSeparator);
        return stringValue;
    }

    protected static StringBuilder cleanLastColumnSeparator(JTable table, String columnSeparator,
            StringBuilder builder) {
        if (table.getColumnCount() > 0) {
            int index = builder.lastIndexOf(columnSeparator);
            if (index > -1) {
                builder.delete(index, builder.length());
            }
        }
        return builder;
    }

}
