/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.renderer;

import java.awt.Color;
import java.awt.Font;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.log.Level;
import fr.soleil.lib.project.swing.SwingUtilitiesConstants;

/**
 * An {@link AdaptedFontDefaultListCellRenderer} dedicated to {@link Level} values.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LevelListCellRenderer extends LightSelectionBackgroundListCellRenderer
        implements ILevelRenderer, SwingUtilitiesConstants {

    private static final long serialVersionUID = 2548432262986415834L;

    protected static final ImageIcon ERROR_ICON = DEFAULT_ICONS.getIcon("fr.soleil.lib.project.swing.log.error");
    protected static final ImageIcon WARNING_ICON = DEFAULT_ICONS.getIcon("fr.soleil.lib.project.swing.log.warning");
    protected static final ImageIcon INFO_ICON = DEFAULT_ICONS.getIcon("fr.soleil.lib.project.swing.log.info");
    protected static final ImageIcon DEBUG_ICON = DEFAULT_ICONS.getIcon("fr.soleil.lib.project.swing.log.debug");
    protected static final ImageIcon TRACE_ICON = DEFAULT_ICONS.getIcon("fr.soleil.lib.project.swing.log.trace");
    protected static final ImageIcon ALL_ICON = DEFAULT_ICONS.getIcon("fr.soleil.lib.project.swing.log.all");

    protected boolean levelColorEnabled;
    protected Font levelFont;

    public LevelListCellRenderer(JComponent componentToRender) {
        super(componentToRender);
        this.levelColorEnabled = true;
        this.levelFont = DEFAULT_LOG_FONT;
    }

    @Override
    public boolean isLevelColorEnabled() {
        return levelColorEnabled;
    }

    @Override
    public void setLevelColorEnabled(boolean levelColorEnabled) {
        this.levelColorEnabled = levelColorEnabled;
    }

    @Override
    protected void doApplyToComponent(final JComponent comp, final Font font, final Color foreground,
            final String tooltipText, final JList<? extends Object> list, final Object value, final int index,
            final boolean isSelected, final boolean cellHasFocus, final boolean derive) {
        Icon icon = null;
        String text = null;
        Color fg = foreground;
        if (value instanceof Level) {
            Level level = (Level) value;
            text = level.getLowerCaseName();
            switch (level) {
                case ERROR:
                    fg = ERROR_COLOR;
                    icon = ERROR_ICON;
                    break;
                case WARNING:
                    fg = WARNING_COLOR;
                    icon = WARNING_ICON;
                    break;
                case INFO:
                    fg = INFO_COLOR;
                    icon = INFO_ICON;
                    break;
                case DEBUG:
                    fg = DEBUG_COLOR;
                    icon = DEBUG_ICON;
                    break;
                case TRACE:
                    fg = TRACE_COLOR;
                    icon = TRACE_ICON;
                    break;
                case ALL:
                    icon = ALL_ICON;
                    break;
                default:
                    break;
            }
            if (!isLevelColorEnabled()) {
                fg = foreground;
            }
        }
        super.doApplyToComponent(comp, levelFont, fg, tooltipText, list, value, index, isSelected, cellHasFocus,
                derive);
        if (comp instanceof JLabel) {
            JLabel label = (JLabel) comp;
            label.setIcon(icon);
            if (text != null) {
                label.setText(text);
            }
        }
    }

    @Override
    public Font getLevelFont() {
        return levelFont;
    }

    @Override
    public void setLevelFont(Font levelFont) {
        this.levelFont = levelFont == null ? DEFAULT_LOG_FONT : levelFont;
        JComponent component = ObjectUtils.recoverObject(componentToRenderRef);
        if (component != null) {
            component.setFont(this.levelFont);
        }
    }

}
