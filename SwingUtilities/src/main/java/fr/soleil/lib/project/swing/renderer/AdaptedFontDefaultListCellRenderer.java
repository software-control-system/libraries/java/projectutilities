/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.renderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.font.TextAttribute;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JList;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.combo.DynamicRenderingComboBox;

/**
 * A {@link DefaultListCellRenderer} which also is an {@link AdaptedFontListCellRenderer}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AdaptedFontDefaultListCellRenderer extends DefaultListCellRenderer
        implements AdaptedFontListCellRenderer<Object> {

    private static final long serialVersionUID = -2419645194261542196L;

    protected boolean autoAdaptFont;
    protected final WeakReference<JComponent> componentToRenderRef;

    public AdaptedFontDefaultListCellRenderer(JComponent componentToRender) {
        super();
        autoAdaptFont = true;
        componentToRenderRef = componentToRender == null ? null : new WeakReference<>(componentToRender);
    }

    @Override
    public final Component getListCellRendererComponent(JList<? extends Object> list, Object value, int index,
            boolean isSelected, boolean cellHasFocus) {
        return getListCellRendererComponent(list, value, index, isSelected, cellHasFocus, true);
    }

    @Override
    public final JComponent getListCellRendererComponent(JList<? extends Object> list, Object value, int index,
            boolean isSelected, boolean cellHasFocus, boolean derive) {
        JComponent comp = (JComponent) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        Font font;
        Color foreground;
        String tooltipText;
        JComponent componentToRender = ObjectUtils.recoverObject(componentToRenderRef);
        boolean opaque;
        if (componentToRender instanceof DynamicRenderingComboBox<?>) {
            DynamicRenderingComboBox<?> combo = (DynamicRenderingComboBox<?>) componentToRender;
            font = combo.getTheoreticalFont();
            foreground = combo.getTheoreticalForeground();
            tooltipText = combo.getTheoreticalTooltipText();
            opaque = combo.isOpaque();
        } else if (componentToRender == null) {
            font = comp.getFont();
            foreground = comp.getForeground();
            tooltipText = comp.getToolTipText();
            opaque = comp.isOpaque();
        } else {
            font = componentToRender.getFont();
            foreground = componentToRender.getForeground();
            tooltipText = componentToRender.getToolTipText();
            opaque = componentToRender.isOpaque();
        }
        comp.setOpaque(opaque);
        if (derive && isSelected) {
            if ((componentToRender != null) && (!componentToRender.isOpaque()) && isAutoAdaptFont()) {
                font = font.deriveFont(Font.ITALIC | Font.BOLD);
                Map<TextAttribute, ?> tmpAttributes = font.getAttributes();
                Map<TextAttribute, Object> attributes = new HashMap<>();
                attributes.putAll(tmpAttributes);
                attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
                font = new Font(attributes);
            }
        }
        applyToComponent(comp, font, foreground, tooltipText, list, value, index, isSelected, cellHasFocus, derive);
        return comp;
    }

    /**
     * Applies some {@link Font}, foreground {@link Color} and tooltip text to a {@link JComponent}, knowing rendering
     * context
     * 
     * @param comp The {@link JComponent}
     * @param font The {@link Font} to apply;
     * @param foreground The foreground {@link Color} to apply
     * @param tooltipText the tooltip text to apply
     * @param list The JList we're painting.
     * @param value The value returned by list.getModel().getElementAt(index).
     * @param index The cells index.
     * @param isSelected True if the specified cell was selected.
     * @param cellHasFocus True if the specified cell has the focus.
     * @param derive Whether to derive Font to adapt it, if {@link #isAutoAdaptFont(boolean)} is <code>true</code>.
     */
    protected void applyToComponent(JComponent comp, Font font, Color foreground, String tooltipText,
            JList<? extends Object> list, Object value, int index, boolean isSelected, boolean cellHasFocus,
            boolean derive) {
        comp.setFont(font);
        comp.setForeground(foreground);
        comp.setToolTipText(tooltipText);
    }

    @Override
    public boolean isAutoAdaptFont() {
        return autoAdaptFont;
    }

    @Override
    public void setAutoAdaptFont(boolean autoAdaptFont) {
        this.autoAdaptFont = autoAdaptFont;
    }

}
