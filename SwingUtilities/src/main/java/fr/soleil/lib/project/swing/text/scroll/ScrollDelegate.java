/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text.scroll;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.lang.ref.WeakReference;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.FontUtils;
import fr.soleil.lib.project.swing.text.IIconTextComponent;
import fr.soleil.lib.project.swing.text.IMarginTextComponent;
import fr.soleil.lib.project.swing.text.ITextComponent;

/**
 * A ScrollDelegate does the effective common work to paint a scrollable text component and to manage its scroll
 * configuration.
 * <p>
 * It does the intermediate between {@link IAutoScrolledTextComponent} and {@link ScrollManager}.
 * </p>
 * <p>
 * It is highly recommended to use a {@link ScrollDelegate} for the development a a new
 * {@link IAutoScrolledTextComponent}.
 * </p>
 *
 * @param <C> The type of {@link ITextComponent} managed by this {@link ScrollDelegate}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ScrollDelegate<C extends JComponent & IAutoScrolledTextComponent> implements SwingConstants {

    /**
     * DRAW_LINES is a <code>boolean</code> used for visual debugging.
     * <p>
     * It is always <code>false</code>, unless you decided to run java with <b><code>-DdrawLines=true</code></b> option,
     * in which case it becomes <code>true</code> and the {@link #paintComponent(Graphics2D)} method will trace the
     * lines surrounding text, icon and drawing zone, + the icon center line.
     * </p>
     * <p>
     * The lines will have following colors:
     * <ul>
     * <li>drawing zone surrounding lines are red</li>
     * <li>icon surrounding lines are blue</li>
     * <li>icon center line is cyan</li>
     * <li>text surrounding lines are black</li>
     * <li>text base line is gray</li>
     * <li>text center line, descent ignored, is gray</li>
     * <li>text exact center line, descent included, is dark gray</li>
     * </ul>
     * </p>
     */
    private static final boolean DRAW_LINES = "true"
            .equalsIgnoreCase(System.getProperty("drawLines", ObjectUtils.EMPTY_STRING));

    /**
     * The known {@link IAutoScrolledTextComponent}.
     */
    protected final WeakReference<C> componentRef;

    /**
     * The {@link ScrollManager} that manages the scrolling.
     */
    protected ScrollManager scrollManager;
    /**
     * The desired {@link ScrollMode}.
     */
    protected volatile ScrollMode scrollMode;

    /**
     * The time (in milliseconds) to wait between 2 scroll positions. A value less than or equal to 0 indicates a
     * stopped scrolling mode.
     */
    protected volatile int stepTime;
    /**
     * The time (in milliseconds) to wait once an extremity is reached. This should be used
     * only in case of {@link ScrollMode#PENDULAR} or {@link ScrollMode#REACH_END} scrolling mode.
     */
    protected volatile int waitingTime;

    /**
     * The stored {@link FontRenderContext}, updated at each {@link #paintComponent(Graphics2D)}.
     */
    protected volatile FontRenderContext frc;
    /**
     * The stored {@link TextLayout}, updated at each {@link #paintComponent(Graphics2D)}.
     */
    protected volatile TextLayout textLayout;

    /**
     * Default {@link ScrollMode} to use when given {@link ScrollMode} is <code>null</code> in
     * {@link #setScrollMode(ScrollMode)}.
     */
    protected final ScrollMode defaultScrollMode;

    /**
     * Constructs a new {@link ScrollDelegate} for given {@link IAutoScrolledTextComponent}.
     * 
     * @param component The {@link IAutoScrolledTextComponent}.
     * @param defaultScrollMode Default {@link ScrollMode} to use when given {@link ScrollMode} is <code>null</code> in
     *            {@link #setScrollMode(ScrollMode)}. If <code>null</code>, {@link ScrollMode#LOOP} will be used.
     *            <p>
     *            This is also the initial {@link ScrollMode}.
     *            </p>
     */
    public ScrollDelegate(C component, ScrollMode defaultScrollMode) {
        this.componentRef = component == null ? null : new WeakReference<>(component);
        this.defaultScrollMode = defaultScrollMode == null ? ScrollMode.LOOP : defaultScrollMode;
        init();
    }

    /**
     * Constructs a new {@link ScrollDelegate} for given {@link IAutoScrolledTextComponent}.
     * 
     * @param component The {@link IAutoScrolledTextComponent}.
     */
    public ScrollDelegate(C component) {
        this(component, ScrollMode.LOOP);
    }

    /**
     * Returns the {@link FontRenderContext} that may be used to measure strings.
     * 
     * @return A {@link FontRenderContext}, never <code>null</code>.
     */
    public FontRenderContext getFontRenderContext() {
        FontRenderContext frc = this.frc;
        return frc == null ? FontUtils.getDefaultRenderContext() : frc;
    }

    /**
     * Generates a {@link ScrollManager}.
     * <p>
     * This method is called at {@link #init()}.
     * </p>
     * 
     * @return A {@link ScrollManager}, never <code>null</code>.
     */
    protected ScrollManager generateScrollManager() {
        return new ScrollManager(ObjectUtils.recoverObject(componentRef));
    }

    /**
     * Initializes this {@link ScrollDelegate}, setting up the different variables.
     */
    protected void init() {
        scrollMode = defaultScrollMode;
        stepTime = 5;
        waitingTime = 2000;
        scrollManager = generateScrollManager();
    }

    /**
     * Returns the time (in milliseconds) to wait between 2 scroll positions. A value less than or equal to 0 indicates
     * a stopped scrolling mode.
     * <p>
     * An {@link IAutoScrolledTextComponent} can call this method for its own
     * {@link IAutoScrolledTextComponent#getScrollStepsTime()} method.
     * </p>
     * 
     * @return An <code>int</code>.
     */
    public int getScrollStepsTime() {
        return stepTime;
    }

    /**
     * Sets the time (in milliseconds) to wait between 2 scroll positions (for example: for the scrolled part moving
     * from 1 pixel).
     * <p>
     * An {@link IAutoScrolledTextComponent} can call this method for its own
     * {@link IAutoScrolledTextComponent#setScrollStepsTime(int)} method.
     * </p>
     * 
     * @param stepTime The time to set (the bigger the value, the slower the scroll speed). A value less than or equal
     *            to <code>0</code> stops the scrolling mode.
     */
    public void setScrollStepsTime(int stepTime) {
        if (stepTime < 0) {
            stepTime = 0;
        }
        if (this.stepTime != stepTime) {
            boolean restartScroll = ((this.stepTime == 0) || (stepTime == 0));
            this.stepTime = stepTime;
            if (restartScroll) {
                scrollManager.restartAutoScrollIfPossible();
            }
        }
    }

    /**
     * Returns the time (in milliseconds) to wait once an extremity is reached. This should be used
     * only in case of {@link ScrollMode#PENDULAR} or {@link ScrollMode#REACH_END} scrolling mode.
     * <p>
     * An {@link IAutoScrolledTextComponent} can call this method for its own
     * {@link IAutoScrolledTextComponent#getReachEndWaitingTime()} method.
     * </p>
     * 
     * @return An <code>int</code>.
     */
    public int getReachEndWaitingTime() {
        return waitingTime;
    }

    /**
     * Sets the time (in milliseconds) to wait once an extremity is reached.
     * <p>
     * An {@link IAutoScrolledTextComponent} can call this method for its own
     * {@link IAutoScrolledTextComponent#setReachEndWaitingTime(int)} method.
     * </p>
     * 
     * @param waitingTime The waiting time to set
     */
    public void setReachEndWaitingTime(int waitingTime) {
        if (waitingTime < 0) {
            waitingTime = 0;
        }
        this.waitingTime = waitingTime;
    }

    /**
     * Returns the known {@link IAutoScrolledTextComponent}'s text width.
     * <p>
     * The {@link IAutoScrolledTextComponent} can call this method for its own
     * {@link IAutoScrolledTextComponent#getTextWidth(FontRenderContext)} method.
     * </p>
     * 
     * @param frc The {@link FontRenderContext} that may be used to calculate text width.
     * 
     * @return An <code>int</code>.
     */
    public int getTextWidth(FontRenderContext frc) {
        int textWidth;
        C component = ObjectUtils.recoverObject(componentRef);
        if (component == null) {
            textWidth = 0;
        } else {
            // TextLayout is the best way to measure string, as described in Font javadoc.
            Font f = component.getFont();
            String text = component.getText();
            textWidth = FontUtils.getTextWidth(f, frc, text, textLayout);
        }
        return textWidth;
    }

    /**
     * Returns the width reduced due to some margin.
     * 
     * @param myWidth The original width.
     * @param insets The margin.
     * @return The reduced width.
     */
    protected int getReducedWidth(int myWidth, Insets insets) {
        if (insets != null) {
            myWidth -= (insets.left + insets.right);
        }
        return myWidth;
    }

    /**
     * Returns the width available to display some text.
     * 
     * @return An <code>int</code>.
     */
    public int getDisplayableWidth() {
        int myWidth;
        C component = ObjectUtils.recoverObject(componentRef);
        if (component == null) {
            myWidth = 0;
        } else {
            myWidth = component.getWidth();
            if (component instanceof IMarginTextComponent) {
                myWidth = getReducedWidth(myWidth, ((IMarginTextComponent) component).getMargin());
            }
            Border border = component.getBorder();
            if (border != null) {
                myWidth = getReducedWidth(myWidth, border.getBorderInsets(component));
            }
            if (component instanceof IIconTextComponent) {
                IIconTextComponent iconComponent = (IIconTextComponent) component;
                Icon icon = iconComponent.getIcon();
                if ((icon != null) && (iconComponent.getHorizontalTextPosition() != CENTER)) {
                    myWidth -= iconComponent.getIconTextGap() + icon.getIconWidth();
                }
            }
            if (myWidth < 0) {
                myWidth = 0;
            }
        }
        return myWidth;
    }

    /**
     * Returns the scrolling mode.
     * <p>
     * An {@link IAutoScrolledTextComponent} can call this method for its own
     * {@link IAutoScrolledTextComponent#getScrollMode()} method.
     * </p>
     * 
     * @return A {@link ScrollMode}.
     */
    public ScrollMode getScrollMode() {
        return scrollMode;
    }

    /**
     * Sets the scrolling mode.
     * <p>
     * An {@link IAutoScrolledTextComponent} can call this method for its own
     * {@link IAutoScrolledTextComponent#setScrollMode(ScrollMode)} method.
     * </p>
     * 
     * @param scrollMode The {@link ScrollMode} to set.
     */
    public void setScrollMode(ScrollMode scrollMode) {
        if (this.scrollMode != scrollMode) {
            if (scrollMode == null) {
                scrollMode = defaultScrollMode;
            }
            this.scrollMode = scrollMode;
            scrollManager.restartAutoScrollIfPossible();
        }
    }

    /**
     * Computes whether scrolling is allowed for known {@link IAutoScrolledTextComponent}.
     * <p>
     * This method will in fact call {@link ScrollManager#isScrollingAllowed()}.
     * </p>
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isScrollingAllowed() {
        ScrollManager scrollManager = this.scrollManager;
        return scrollManager != null && scrollManager.isScrollingAllowed();
    }

    /**
     * Configures the {@link ScrollManager} according to known {@link IAutoScrolledTextComponent}, registering the
     * necessary listeners.
     * <p>
     * This method will in fact call {@link ScrollManager#setup()}.
     * </p>
     */
    public void setup() {
        ScrollManager scrollManager = this.scrollManager;
        if (scrollManager != null) {
            scrollManager.setup();
        }
    }

    /**
     * Undo what was done in {@link #setup()}.
     * <p>
     * This method will in fact call {@link ScrollManager#unconfigure()}.
     * </p>
     */
    public void unconfigure() {
        ScrollManager scrollManager = this.scrollManager;
        if (scrollManager != null) {
            scrollManager.unconfigure();
        }
    }

    /**
     * Returns the horizontal position at which the text should be displayed.
     * <p>
     * This method will in fact call {@link ScrollManager#getCurrentPosition()}.
     * </p>
     * 
     * @return An <code>int</code>.
     */
    public int getCurrentPosition() {
        ScrollManager scrollManager = this.scrollManager;
        return scrollManager == null ? 0 : scrollManager.getCurrentPosition();
    }

    /**
     * Cancels the scroll thread.
     * <p>
     * This method will in fact call {@link ScrollManager#cancelAutoScroll())}.
     * </p>
     */
    public void cancelAutoScroll() {
        ScrollManager scrollManager = this.scrollManager;
        if (scrollManager != null) {
            scrollManager.cancelAutoScroll();
        }
    }

    /**
     * Restarts the scroll thread, if necessary, resetting text position and
     * scroll direction.
     * <p>
     * This method will in fact call {@link ScrollManager#restartAutoScrollIfPossible())}.
     * </p>
     */
    public void restartAutoScrollIfPossible() {
        ScrollManager scrollManager = this.scrollManager;
        if (scrollManager != null) {
            scrollManager.restartAutoScrollIfPossible();
        }
    }

    /**
     * Paints the known {@link IAutoScrolledTextComponent}. Only compatible with {@link Graphics2D}.
     * <p>
     * If your {@link IAutoScrolledTextComponent} is a {@link JComponent}, you should ask its
     * {@link JComponent#paintComponents(java.awt.Graphics)} to call this method, if the {@link java.awt.Graphics} are
     * {@link Graphics2D} (which is normally the case).
     * </p>
     * 
     * @param g The {@link Graphics2D} in which to paint the component.
     */
    public void paintComponent(Graphics2D g) {
        C component = ObjectUtils.recoverObject(componentRef);
        Font oldFont = g.getFont();
        Color oldColor = g.getColor();
        Shape oldClip = g.getClip();
        Stroke oldStroke = g.getStroke();
        if (component != null) {
            int currentPosition = getCurrentPosition();
            int width = component.getWidth();
            int height = component.getHeight();
            if (component.isOpaque()) {
                g.setColor(component.getBackground());
                g.fillRect(0, 0, width, height);
            }
            String text = component.getText();
            // Compute minimum text to use TextLayout.
            Font font = component.getFont();
            g.setFont(font);
            FontRenderContext frc = g.getFontRenderContext();
            this.frc = frc;
            g.setColor(component.getForeground());
            // Use TextLayout to compute text bounds
            if (text == null) {
                text = ObjectUtils.EMPTY_STRING;
            }
            int textFullHeight, descent, textWidth;
            textLayout = new TextLayout(text.isEmpty() ? FontUtils.TEXTLAYOUT_DEFAULT_TEXT : text, font, frc);
            Rectangle2D bounds = textLayout.getBounds();
            textFullHeight = FontUtils.getFontHeight(bounds);
            descent = FontUtils.getEffectiveDescent(bounds);
            textWidth = FontUtils
                    .getTextWidth(text.isEmpty() || !text.trim().isEmpty() ? bounds : font.getStringBounds(text, frc));
            int textHeight = textFullHeight - descent; // text height without descent
            Insets borderMargin = null;
            // Recover border and inner margins to compute available text space
            Border border = component.getBorder();
            if (border != null) {
                borderMargin = border.getBorderInsets(component);
            }
            if (component instanceof IMarginTextComponent) {
                Insets margin = ((IMarginTextComponent) component).getMargin();
                if (margin != null) {
                    if (borderMargin == null) {
                        borderMargin = margin;
                    } else {
                        borderMargin.top += margin.top;
                        borderMargin.left += margin.left;
                        borderMargin.bottom += margin.bottom;
                        borderMargin.right += margin.right;
                    }
                }
            }
            // Compute drawable coordinates
            int minX = 0, maxX = component.getWidth();
            int minY = 0, maxY = component.getHeight();
            int drawWidth = maxX, drawHeight = maxY;
            if (borderMargin != null) {
                minX += borderMargin.left;
                maxX -= borderMargin.right;
                minY += borderMargin.top;
                maxY -= borderMargin.bottom;
                drawWidth -= borderMargin.left + borderMargin.right;
                drawHeight -= borderMargin.top + borderMargin.bottom;
            }
            int y;
            int verticalAlignment = TOP;
            // Get icon information
            Icon icon = null;
            int horizontalTextPosition = LEFT;
            int verticalTextPosition = BOTTOM;
            int horizontalAlignment = LEFT;
            int gap = 0;
            if (component instanceof IIconTextComponent) {
                IIconTextComponent iconComponent = (IIconTextComponent) component;
                icon = iconComponent.getIcon();
                horizontalTextPosition = iconComponent.getHorizontalTextPosition();
                horizontalAlignment = iconComponent.getHorizontalAlignment();
                verticalAlignment = iconComponent.getVerticalAlignment();
                verticalTextPosition = iconComponent.getVerticalTextPosition();
                gap = iconComponent.getIconTextGap();
            }
            if (DRAW_LINES) {
                g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10.0f,
                        new float[] { 1.0f, 6.0f }, 0.0f));
                g.setColor(Color.RED);
                g.drawLine(minX, minY, maxX, minY);
                g.drawLine(minX, maxY, maxX, maxY);
                g.drawLine(minX, minY, minX, maxY);
                g.drawLine(maxX, minY, maxX, maxY);
                g.setStroke(oldStroke);
            }
            int minTextY = minY + textHeight, maxTextY = maxY - descent;
            switch (verticalAlignment) {
                case TOP:
                    y = minTextY;
                    break;
                case BOTTOM:
                    y = maxTextY;
                    break;
                case CENTER:
                default:
                    y = minY + (drawHeight + textFullHeight) / 2 - descent;
                    if (y < minY + textHeight) {
                        y = minY + textHeight;
                    }
                    break;
            }
            int x = minX;
            g.clipRect(minX, minY, drawWidth, drawHeight);
            // Compute icon position
            boolean hasIcon;
            int iconX = 0, iconY = 0, iconWidth = 0, iconRealWidth = 0, iconHeight = 0;
            if ((icon instanceof ImageIcon) && (((ImageIcon) icon).getImage() != null)) {
                // Compute icon position and adapt clip
                iconRealWidth = icon.getIconWidth();
                iconWidth = iconRealWidth + gap;
                iconHeight = icon.getIconHeight();
                hasIcon = true;
            } else {
                hasIcon = false;
                gap = 0;
            }
            boolean scrollDisabled = (maxX - minX >= iconWidth + textWidth);
            if (scrollDisabled) {
                // Particular case where scroll is not active as there is enough space.
                // In this case, horizontal alignment has to be managed.
                switch (horizontalAlignment) {
                    case RIGHT:
                    case TRAILING:
                        // text and icon are at the right
                        switch (horizontalTextPosition) {
                            case LEFT:
                            case LEADING:
                                iconX = maxX - iconRealWidth;
                                x = iconX - (textWidth + gap);
                                break;
                            case CENTER:
                                if (textWidth < iconRealWidth) {
                                    // icon is bigger than text: align text according to icon
                                    iconX = maxX - iconRealWidth;
                                    x = iconX + (iconRealWidth - textWidth) / 2;
                                } else {
                                    // text is bigger than icon: align icon according to text
                                    x = maxX - textWidth;
                                    iconX = x + (textWidth - iconRealWidth) / 2;
                                }
                                break;
                            case RIGHT:
                            case TRAILING:
                            default:
                                x = maxX - textWidth;
                                iconX = x - iconWidth;
                                break;
                        }
                        break;
                    case CENTER:
                        // text and icon are centered
                        switch (horizontalTextPosition) {
                            case LEFT:
                            case LEADING:
                                x = minX + (drawWidth - (textWidth + iconWidth)) / 2;
                                iconX = x + textWidth + gap;
                                break;
                            case CENTER:
                                x = minX + (drawWidth - textWidth) / 2;
                                iconX = minX + (drawWidth - iconRealWidth) / 2;
                                break;
                            case RIGHT:
                            case TRAILING:
                            default:
                                iconX = minX + (drawWidth - (textWidth + iconWidth)) / 2;
                                x = iconX + iconWidth;
                                break;
                        }
                        break;
                    case LEFT:
                    case LEADING:
                    default:
                        // text and icon are at the right
                        switch (horizontalTextPosition) {
                            case LEFT:
                            case LEADING:
                                x = minX;
                                iconX = x + textWidth + gap;
                                break;
                            case CENTER:
                                if (textWidth < iconRealWidth) {
                                    // icon is bigger than text: align text according to icon
                                    iconX = minX;
                                    x = iconX + (iconRealWidth - textWidth) / 2;
                                } else {
                                    // text is bigger than icon: align icon according to text
                                    x = minX;
                                    iconX = x + (textWidth - iconRealWidth) / 2;
                                }
                                break;
                            case RIGHT:
                            case TRAILING:
                            default:
                                iconX = minX;
                                x = iconX + iconWidth;
                                break;
                        }
                        break;
                }
            } else if (hasIcon) {
                // Text is scrolling: horizontal alignment is ignored
                // However, icon position has to be managed
                switch (horizontalTextPosition) {
                    case LEADING:
                    case LEFT:
                        iconX = maxX - iconRealWidth;
                        if (iconX < minX) {
                            iconX = minX;
                        }
                        break;
                    case TRAILING:
                    case RIGHT:
                        iconX = minX;
                        x = iconX + iconWidth;
                        break;
                    case CENTER:
                    default:
                        iconX = (drawWidth - iconRealWidth) / 2;
                        break;
                }
            } // end if (scrollDisabled) ... else if (hasIcon)
            if (hasIcon) {
                if (!scrollDisabled) {
                    // Adapt clip
                    switch (horizontalTextPosition) {
                        case LEADING:
                        case LEFT:
                            g.clipRect(minX, minY, drawWidth - iconWidth, drawHeight);
                            break;
                        case TRAILING:
                        case RIGHT:
                            g.clipRect(minX + iconWidth, minY, drawWidth - iconWidth, drawHeight);
                            break;
                        case CENTER:
                        default:
                            // no particular clip
                            break;
                    }
                }

                // Text & Icon vertical alignment management
                switch (verticalAlignment) {
                    case BOTTOM:
                        iconY = Math.max(minY, maxY - iconHeight);
                        break;
                    case CENTER:
                        iconY = Math.max(minY, minY + (drawHeight - iconHeight) / 2);
                        break;
                    case TOP:
                    default:
                        iconY = minY;
                        break;
                }
                // Text & Icon vertical position management
                switch (verticalTextPosition) {
                    case BOTTOM:
                        if (textFullHeight < iconHeight) {
                            // icon is bigger than text: align text according to icon
                            y = iconY + iconHeight - descent;
                        } else {
                            // text is bigger than icon: align icon according to text
                            iconY = y + descent - iconHeight;
                        }
                        break;
                    case CENTER:
                        if (textFullHeight < iconHeight) {
                            // icon is bigger than text: align text according to icon
                            y = iconY + (iconHeight + textFullHeight) / 2 - descent;
                        } else {
                            // text is bigger than icon: align icon according to text
                            iconY = y + descent - (iconHeight + textFullHeight) / 2;
                        }
                        break;
                    case TOP:
                    default:
                        if (textFullHeight < iconHeight) {
                            // icon is bigger than text: align text according to icon
                            y = iconY + textHeight;
                        } else {
                            // text is bigger than icon: align icon according to text
                            iconY = y - textHeight;
                        }
                        break;
                }
            } // end if (hasIcon)

            // Draw icon before text, so that text might be painted over icon
            if (icon instanceof ImageIcon) {
                ImageIcon imgIcon = (ImageIcon) icon;
                if (imgIcon.getImage() != null) {
                    // Draw icon
                    Shape clip = g.getClip();
                    g.setClip(oldClip);
                    g.clipRect(0, 0, component.getWidth(), component.getHeight());
                    g.drawImage(imgIcon.getImage(), iconX, iconY, iconRealWidth, iconHeight, null);
                    g.setClip(clip);
                }
            }
            // Draw text
            g.setColor(component.getForeground());
            g.drawString(text, x + currentPosition, y);
            // Potentially draw text lines for debugging
            if (DRAW_LINES) {
                g.setColor(Color.GRAY);
                g.drawLine(minX, y, maxX, y);
                g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10.0f,
                        new float[] { 1.0f, 6.0f }, 0.0f));
                g.drawLine(minX, y - textHeight / 2, maxX, y - textHeight / 2);
                g.setColor(Color.DARK_GRAY);
                g.drawLine(minX, y + descent - textFullHeight / 2, maxX, y + descent - textFullHeight / 2);
                g.setColor(Color.BLACK);
                g.setStroke(oldStroke);
                g.drawLine(minX, y + descent, maxX, y + descent);
                g.drawLine(minX, y - textHeight, maxX, y - textHeight);
                g.drawLine(x, minY, x, maxY);
                g.drawLine(x + textWidth, minY, x + textWidth, maxY);
            }
            // Potentially draw icon lines for debugging
            if (hasIcon && DRAW_LINES) {
                g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10.0f,
                        new float[] { 6.0f }, 0.0f));
                g.setColor(Color.BLUE);
                g.drawLine(minX, iconY, drawWidth, iconY);
                g.drawLine(minX, iconY + iconHeight, drawWidth, iconY + iconHeight);
                g.drawLine(iconX, minY, iconX, maxY);
                g.drawLine(iconX + iconRealWidth, minY, iconX + iconRealWidth, maxY);
                switch (horizontalTextPosition) {
                    case LEFT:
                    case LEADING:
                        g.drawLine(iconX - gap, minY, iconX - gap, maxY);
                        break;
                    case RIGHT:
                    case TRAILING:
                        g.drawLine(iconX + iconWidth, minY, iconX + iconWidth, maxY);
                        break;
                    default:
                        // no gap line to draw
                        break;
                }
                g.setColor(Color.CYAN);
                g.drawLine(minX, iconY + iconHeight / 2, drawWidth, iconY + iconHeight / 2);
                g.setStroke(oldStroke);
            }
        } // end if (component != null)
        g.setFont(oldFont);
        g.setColor(oldColor);
        g.setClip(oldClip);
    }

    @Override
    protected void finalize() {
        ScrollManager scrollManager = this.scrollManager;
        if (scrollManager != null) {
            scrollManager.unconfigure();
            scrollManager.cancelAutoScroll();
        }
        this.scrollManager = null;
        this.scrollMode = null;
        this.frc = null;
        this.textLayout = null;
    }
}
