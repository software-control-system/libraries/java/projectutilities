/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.file.log;

import java.awt.Color;
import java.awt.Font;

import fr.soleil.lib.project.file.log.ALogTextFileWriter;
import fr.soleil.lib.project.log.Level;
import fr.soleil.lib.project.log.LogData;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.SwingUtilitiesConstants;
import fr.soleil.lib.project.swing.renderer.ILevelRenderer;
import fr.soleil.lib.project.xmlhelpers.HtmlEscape;

/**
 * An {@link ALogTextFileWriter} that can write html files.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class HtmlLogFileWriter extends ALogTextFileWriter implements SwingUtilitiesConstants, ILevelRenderer {

    public static final String HTML = "html";
    public static final String HTM = "htm";

    protected static final String HTML_START = "<html>\n<head>\n<style>table {border: 1px solid #DCDCDC; border-collapse: collapse;}\nth, tr, td {border: 1px solid #DCDCDC;}</style>\n</head><body>\n<table>\n\n";
    protected static final String ROW_HEADER_START = "<tr style=\"background:#E0E0E0\">\n";
    protected static final String ROW_START = "<tr style=\"font-family: %s; font-size: %spt; color: #%s%s%s;\">\n";
    protected static final String ROW_END = "</tr>\n";
    protected static final String COLUMN_HEADER_START = "<th>";
    protected static final String COLUMN_HEADER_END = "</th>\n";
    protected static final String COLUMN_START = "<td>\n";
    protected static final String COLUMN_END = "</td>\n";
    protected static final String HTML_END = "</table>\n</body>\n</html>";

    protected static final String[] EXTENSIONS = { HTML, HTM };

    protected Font font;
    protected Color defaultColor;
    protected boolean colorEnabled;

    public HtmlLogFileWriter(MessageManager messageManager) {
        super(messageManager);
        font = DEFAULT_LOG_FONT;
        defaultColor = Color.BLACK;
        colorEnabled = true;
    }

    /**
     * Sets the default text color to use in html files.
     * 
     * @return A {@link Color}.
     */
    public Color getDefaultColor() {
        return defaultColor;
    }

    /**
     * Sets the default text color to use in html files.
     * 
     * @param defaultColor The default text color to use in html files.
     */
    public void setDefaultColor(Color defaultColor) {
        this.defaultColor = defaultColor;
    }

    @Override
    public Font getLevelFont() {
        return font;
    }

    @Override
    public void setLevelFont(Font font) {
        this.font = font == null ? DEFAULT_LOG_FONT : font;
    }

    @Override
    public boolean isLevelColorEnabled() {
        return colorEnabled;
    }

    @Override
    public void setLevelColorEnabled(boolean levelColorEnabled) {
        this.colorEnabled = levelColorEnabled;
    }

    /**
     * Returns the hexadecimal {@link String} representation of an <code>int</code>.
     * 
     * @param valueThe <code>int</code>.
     * @return A {@link String}.
     */
    protected String getHexString(final int value) {
        String hex = Integer.toHexString(Math.abs(value));
        if (hex.length() == 1) {
            hex = '0' + hex;
        }
        if (value < 0) {
            hex = '-' + hex;
        }
        return hex;
    }

    /**
     * Returns the best html tr tag according to given {@link Level}.
     * 
     * @param level The {@link Level}.
     * @return A {@link String}.
     */
    protected String getTrStart(final Level level) {
        Font font = getLevelFont();
        if (font == null) {
            font = DEFAULT_LOG_FONT;
        }
        Color color;
        if (isLevelColorEnabled()) {
            if (level == null) {
                color = getDefaultColor();
            } else {
                switch (level) {
                    case ERROR:
                        color = ERROR_COLOR;
                        break;
                    case WARNING:
                        color = WARNING_COLOR;
                        break;
                    case INFO:
                        color = INFO_COLOR;
                        break;
                    case DEBUG:
                        color = DEBUG_COLOR;
                        break;
                    case TRACE:
                        color = TRACE_COLOR;
                        break;
                    default:
                        color = getDefaultColor();
                        break;
                }
            }
        } else {
            color = getDefaultColor();
        }
        if (color == null) {
            color = Color.BLACK;
        }
        return String.format(ROW_START, font.getFamily(), font.getSize(), getHexString(color.getRed()),
                getHexString(color.getGreen()), getHexString(color.getBlue()));
    }

    @Override
    public String[] getManagedFileExtensions() {
        return EXTENSIONS;
    }

    @Override
    protected StringBuilder appendLogsToStringBuilder(StringBuilder builder, LogData... logs) {
        builder.append(HTML_START).append(ROW_HEADER_START);
        builder.append(COLUMN_HEADER_START).append(messageManager.getMessage("fr.soleil.lib.project.log.timestamp"))
                .append(COLUMN_HEADER_END);
        builder.append(COLUMN_HEADER_START).append(messageManager.getMessage("fr.soleil.lib.project.log.level"))
                .append(COLUMN_HEADER_END);
        builder.append(COLUMN_HEADER_START).append(messageManager.getMessage("fr.soleil.lib.project.log.message"))
                .append(COLUMN_HEADER_END);
        builder.append(ROW_END);
        if ((logs != null) && (logs.length > 0)) {
            for (LogData log : logs) {
                if (log != null) {
                    builder.append(getTrStart(log.getLevel()));
                    builder.append(COLUMN_START).append(log.getTimestamp()).append(COLUMN_END);
                    builder.append(COLUMN_START).append(log.getLevel()).append(COLUMN_END);
                    builder.append(COLUMN_START).append(HtmlEscape.escape(log.getMessage(), true, false))
                            .append(COLUMN_END);
                    builder.append(ROW_END);
                }
            }
        }
        builder.append(HTML_END);
        return builder;
    }

}
