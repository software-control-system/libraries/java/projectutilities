/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.ui;

import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.plaf.basic.BasicLabelUI;

/**
 * A {@link BasicLabelUI} that does not change {@link JLabel}'s text color when disabled
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class VisibleTextLabelUI extends BasicLabelUI {

    /**
     * Constructs a new {@link VisibleTextLabelUI}
     */
    public VisibleTextLabelUI() {
        super();
    }

    @Override
    protected void paintDisabledText(JLabel l, Graphics g, String s, int textX, int textY) {
        paintEnabledText(l, g, s, textX, textY);
    }

}
