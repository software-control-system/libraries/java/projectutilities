/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

/**
 * A single selection JList that listen for double-clicks on elements (not blank part) to activate a
 * command. Typically a doClick() on a button.
 * 
 * @param <E> the type of the elements of this list
 * @author MAINGUY
 */
public abstract class JListDoubleClick<E> extends JList<E> implements MouseListener {

    private static final long serialVersionUID = -7276306256501211563L;

    /**
     * Constructs a {@link JListDoubleClick} with an empty, read-only, model.
     */
    public JListDoubleClick() {
        super();
        initList();
    }

    /**
     * Constructs a {@code JListDoubleClick} that displays elements from the specified, {@code non-null}, model.
     * <p>
     * This constructor registers the list with the {@code ToolTipManager}, allowing for tooltips to be provided by the
     * cell renderers.
     * </p>
     * 
     * @param dataModel the model for the list
     * @exception IllegalArgumentException if the model is {@code null}
     */
    public JListDoubleClick(ListModel<E> dataModel) {
        super(dataModel);
        initList();
    }

    /**
     * Constructs a {@link JListDoubleClick} that displays the elements in the specified array. This
     * constructor creates a read-only model for the given array, and then delegates to the
     * constructor that takes a {@code ListModel}.
     * <p>
     * Attempts to pass a {@code null} value to this method results in undefined behavior and, most likely, exceptions.
     * The created model references the given array directly. Attempts to modify the array after constructing the list
     * results in undefined behavior.
     * </p>
     * 
     * @param listData the array of Objects to be loaded into the data model, {@code non-null}
     */
    public JListDoubleClick(E[] listData) {
        super(listData);
        initList();
    }

    /**
     * Constructs a {@link JListDoubleClick} that displays the elements in the specified {@link Vector}. This
     * constructor creates a read-only model for the given {@code Vector}, and
     * then delegates to the constructor that takes a {@code ListModel}.
     * <p>
     * Attempts to pass a {@code null} value to this method results in undefined behavior and, most likely, exceptions.
     * The created model references the given {@code Vector} directly. Attempts to modify the {@code Vector} after
     * constructing the list results in undefined behavior.
     * </p>
     * 
     * @param listData the <code>Vector</code> to be loaded into the data model, {@code non-null}
     */
    public JListDoubleClick(Vector<? extends E> listData) {
        super(listData);
        initList();
    }

    /**
     * Init the JList
     */
    private void initList() {
        // single selection is better, otherwise the first selected item would be used.
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        addMouseListener(this);
    }

    /**
     * The action to be executed on double-click
     */
    protected abstract void onDoubleClick(int index);

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                int index = locationToIndex(e.getPoint());
                if (index != -1) {
                    // we ensure the user clicked on an item, not in the blank part below last item
                    if (getCellBounds(index, index).contains(e.getPoint())) {
                        onDoubleClick(index);
                    }
                }
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // Nothing
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // Nothing
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // Nothing
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // Nothing
    }

}
