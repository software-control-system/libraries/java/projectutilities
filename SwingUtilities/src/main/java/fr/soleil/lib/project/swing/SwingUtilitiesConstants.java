/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.icons.Icons;

/**
 * An interface that contains constants that can be used in any class of SwingUtilities project.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface SwingUtilitiesConstants {

    /**
     * Default {@link MessageManager} to use in SwingUtilities.
     */
    public static final MessageManager DEFAULT_MESSAGE_MANAGER = new MessageManager("fr.soleil.lib.project.swing")
            .pushProperties(MessageManager.DEFAULT_MESSAGE_MANAGER);

    /**
     * Default {@link Icons} to use in SwingUtilities.
     */
    public static final Icons DEFAULT_ICONS = new Icons();

    /**
     * Default column separator to use in CSV representations
     */
    public static final String DEFAULT_CSV_COLUMN_SEPARATOR = ";";

    /**
     * Default row separator to use in CSV representations
     */
    public static final String DEFAULT_CSV_ROW_SEPARATOR = ObjectUtils.NEW_LINE;

    /**
     * Color used to represent an error log level.
     */
    public static final Color ERROR_COLOR = Color.RED;

    /**
     * Color used to represent a warning log level.
     */
    public static final Color WARNING_COLOR = new Color(255, 128, 0);

    /**
     * Color used to represent an info log level.
     */
    public static final Color INFO_COLOR = Color.BLACK;

    /**
     * Color used to represent a debug log level.
     */
    public static final Color DEBUG_COLOR = new Color(0, 153, 0);

    /**
     * Color used to represent a trace log level.
     */
    public static final Color TRACE_COLOR = new Color(136, 136, 136);

    /**
     * The default font that can be used with log components
     */
    public static final Font DEFAULT_LOG_FONT = new Font(Font.MONOSPACED, Font.PLAIN, 12);

    /**
     * {@link Insets} for no margin
     */
    public static final Insets NO_MARGIN = new Insets(0, 0, 0, 0);
}
