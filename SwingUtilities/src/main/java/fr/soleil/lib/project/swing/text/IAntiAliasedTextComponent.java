/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Graphics2D;

/**
 * An {@link ITextComponent} that may use anti-aliasing to display text.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IAntiAliasedTextComponent extends ITextComponent {

    /**
     * Returns whether anti-aliasing is enabled.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isAntiAliasingEnabled();

    /**
     * Enables or disables anti-aliasing.
     * 
     * @param antiAliasingEnabled Whether to enable anti-aliasing.
     */
    public void setAntiAliasingEnabled(boolean antiAliasingEnabled);

    /**
     * Sets some {@link Graphics2D} up to use or not anti-aliasing.
     * 
     * @param g The {@link Graphics2D}.
     */
    public void setupGraphics(Graphics2D g);
}
