/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;

/**
 * A {@link DefaultTreeModel} that supports {@link TreeNode}s sorting, relying on a {@link TreeNode} {@link Comparator}.
 * 
 * This class is based on SortTreeModel defined in Java Swing, 2nd Edition
 * By Marc Loy, Robert Eckstein, Dave Wood, James Elliott, Brian Cole
 * ISBN: 0-596-00408-7
 * Publisher: O'Reilly
 * 
 * @see <a
 *      href="http://www.java2s.com/Code/Java/Swing-JFC/AtreemodelusingtheSortTreeModelwithaFilehierarchyasinput.htm">A
 *      tree model using the SortTreeModel with a File hierarchy as input</a>
 * @author Rapha&euml;l GIRARDOT
 *
 */
public class SortTreeModel extends DefaultTreeModel {

    private static final long serialVersionUID = -7964246940593981829L;

    protected final Object sortLock;
    protected Comparator<DefaultMutableTreeNode> comparator;

    public SortTreeModel(DefaultMutableTreeNode node) {
        super(node);
        sortLock = new Object();
        comparator = null;
    }

    public SortTreeModel(DefaultMutableTreeNode node, boolean asksAllowsChildren) {
        super(node, asksAllowsChildren);
        sortLock = new Object();
        comparator = null;
    }

    @Override
    public DefaultMutableTreeNode getRoot() {
        return (DefaultMutableTreeNode) super.getRoot();
    }

    public Comparator<DefaultMutableTreeNode> getComparator() {
        return comparator;
    }

    public void setComparator(Comparator<DefaultMutableTreeNode> comparator) {
        synchronized (sortLock) {
            if (this.comparator != comparator) {
                this.comparator = comparator;
                sort(getRoot());
            }
        }
    }

    @Override
    public void setRoot(TreeNode root) {
        if (root instanceof DefaultMutableTreeNode) {
            synchronized (sortLock) {
                doSetRoot((DefaultMutableTreeNode) root, true);
            }
        }
    }

    protected void doSetRoot(DefaultMutableTreeNode root, boolean sort) {
        super.setRoot(root);
        if (sort) {
            sort(root);
        }
    }

    protected void sort(DefaultMutableTreeNode node) {
        List<DefaultMutableTreeNode> children = new ArrayList<DefaultMutableTreeNode>();
        while (node.getChildCount() > 0) {
            try {
                DefaultMutableTreeNode child = (DefaultMutableTreeNode) node.getChildAt(0);
                children.add(child);
                removeNodeFromParent(child);
                sort(child);
            } catch (Exception e) {
                continue;
            }
        }
        Comparator<DefaultMutableTreeNode> comparator = getDefaultComparator(node);
        if (comparator != null) {
            Collections.sort(children, comparator);
        }
        for (DefaultMutableTreeNode child : children) {
            try {
                insertNodeInto(child, node, node.getChildCount());
            } catch (Exception e) {
                continue;
            }
        }
    }

    protected Comparator<DefaultMutableTreeNode> getDefaultComparator(DefaultMutableTreeNode parent) {
        return null;
    }

    @Override
    public void insertNodeInto(MutableTreeNode child, MutableTreeNode parent, int index) {
        int sortIndex = findIndexFor((DefaultMutableTreeNode) child, (DefaultMutableTreeNode) parent, index);
        doInsertNodeInto((DefaultMutableTreeNode) child, (DefaultMutableTreeNode) parent, sortIndex);
    }

    // Call to super.insertNodeInto, so that children classes may have access to this method
    protected void doInsertNodeInto(DefaultMutableTreeNode child, DefaultMutableTreeNode parent, int index) {
        super.insertNodeInto(child, parent, index);
    }

    // Perform a recursive binary search on the children to find the right insertion point for the next node.
    protected int findIndexFor(DefaultMutableTreeNode child, DefaultMutableTreeNode parent, int defaultIndex) {
        int cc = parent.getChildCount();
        int index;
        Comparator<DefaultMutableTreeNode> comparator = this.comparator;
        if (comparator == null) {
            index = defaultIndex;
        } else if (cc == 0) {
            index = 0;
        } else if (cc == 1) {
            index = comparator.compare(child, (DefaultMutableTreeNode) parent.getChildAt(0)) <= 0 ? 0 : 1;
        } else {
            index = findIndexFor(child, parent, 0, cc - 1, defaultIndex, comparator); // First & last index
        }
        return index;
    }

    protected int findIndexFor(DefaultMutableTreeNode child, DefaultMutableTreeNode parent, int i1, int i2,
            int defaultIndex, Comparator<DefaultMutableTreeNode> comparator) {
        int index;
        if (comparator == null) {
            index = defaultIndex;
        } else if (i1 == i2) {
            index = comparator.compare(child, (DefaultMutableTreeNode) parent.getChildAt(i1)) <= 0 ? i1 : i1 + 1;
        } else {
//            int half = (defaultIndex + i2) / 2; //bug to modify //javier substract
            int half = (i1 + i2) / 2;
            if (comparator.compare(child, (DefaultMutableTreeNode) parent.getChildAt(half)) <= 0) {
                index = findIndexFor(child, parent, i1, half, defaultIndex, comparator);
            } else {
                index = findIndexFor(child, parent, half + 1, i2, defaultIndex, comparator);
            }
        }
        return index;
    }
}