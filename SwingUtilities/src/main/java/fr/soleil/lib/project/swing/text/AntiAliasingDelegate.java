/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.lang.ref.WeakReference;

import javax.swing.JComponent;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.text.scroll.IAutoScrolledTextComponent;

/**
 * A class to which to delegate anti-aliasing management.
 * <p>
 * It is recommended to use an {@link AntiAliasingDelegate} with any {@link IAutoScrolledTextComponent}.
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AntiAliasingDelegate {

    /**
     * The {@link WeakReference} that stores the known {@link JComponent}.
     */
    protected final WeakReference<JComponent> componentRef;
    /**
     * Whether anti-aliasing is enabled.
     */
    protected boolean antiAliasingEnabled;

    /**
     * Constructs a new {@link AntiAliasingDelegate} for given {@link JComponent} and sets whether anti-aliasing should
     * be enabled.
     * 
     * @param antiAliasingEnabled Whether anti-aliasing should be enabled.
     * @param component The {@link JComponent} (which can be an {@link IAntiAliasedTextComponent}).
     */
    public AntiAliasingDelegate(boolean antiAliasingEnabled, JComponent component) {
        super();
        this.antiAliasingEnabled = antiAliasingEnabled;
        this.componentRef = component == null ? null : new WeakReference<>(component);
    }

    /**
     * Constructs a new {@link AntiAliasingDelegate} for given {@link JComponent}.
     * 
     * @param component The {@link JComponent} (which can be an {@link IAntiAliasedTextComponent}).
     */
    public AntiAliasingDelegate(JComponent component) {
        this(true, component);
    }

    /**
     * Returns whether anti-aliasing is enabled.
     * <p>
     * An {@link IAntiAliasedTextComponent} can call this method for its own
     * {@link IAntiAliasedTextComponent#isAntiAliasingEnabled()} method.
     * </p>
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isAntiAliasingEnabled() {
        return antiAliasingEnabled;
    }

    /**
     * Enables or disables anti-aliasing.
     * <p>
     * An {@link IAntiAliasedTextComponent} can call this method for its own
     * {@link IAntiAliasedTextComponent#setAntiAliasingEnabled(boolean)} method.
     * </p>
     * 
     * @param antiAliasingEnabled Whether to enable anti-aliasing.
     */
    public void setAntiAliasingEnabled(boolean antiAliasingEnabled) {
        this.antiAliasingEnabled = antiAliasingEnabled;
        JComponent component = ObjectUtils.recoverObject(componentRef);
        if ((component != null) && component.isVisible() && component.isShowing()) {
            component.repaint();
        }
    }

    /**
     * Sets some {@link Graphics2D} up to use or not anti-aliasing.
     * <p>
     * An {@link IAntiAliasedTextComponent} can call this method for its own
     * {@link IAntiAliasedTextComponent#setupGraphics(Graphics2D)} method.
     * </p>
     * 
     * @param g The {@link Graphics2D}.
     */
    public void setupGraphics(Graphics2D g) {
        if (g != null) {
            if (isAntiAliasingEnabled()) {
                // Apply anti-aliasing
                g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            } else {
                // Reset to default behavior
                g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_DEFAULT);
                g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,
                        RenderingHints.VALUE_FRACTIONALMETRICS_DEFAULT);
            }
        }
    }

}
