/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text.scroll;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;

import javax.swing.Icon;

import fr.soleil.lib.project.swing.text.DynamicForegroundLabel;
import fr.soleil.lib.project.swing.text.IIconTextComponent;

/**
 * A {@link DynamicForegroundLabel} for which the text is automatically scrolled if too long.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AutoScrolledLabel extends DynamicForegroundLabel
        implements IAutoScrolledTextComponent, IIconTextComponent {

    private static final long serialVersionUID = -2997211075874370414L;

    protected ScrollDelegate<AutoScrolledLabel> scrollDelegate;

    public AutoScrolledLabel() {
        super();
    }

    public AutoScrolledLabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
    }

    public AutoScrolledLabel(Icon image) {
        super(image);
    }

    public AutoScrolledLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
    }

    public AutoScrolledLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
    }

    public AutoScrolledLabel(String text) {
        super(text);
    }

    protected ScrollDelegate<AutoScrolledLabel> generateScrollDelegate() {
        return new ScrollDelegate<>(this);
    }

    @Override
    protected void init() {
        super.init();
        scrollDelegate = generateScrollDelegate();
    }

    @Override
    public ScrollMode getScrollMode() {
        return scrollDelegate.getScrollMode();
    }

    @Override
    public void setScrollMode(ScrollMode scrollMode) {
        scrollDelegate.setScrollMode(scrollMode);
    }

    @Override
    public int getScrollStepsTime() {
        return scrollDelegate.getScrollStepsTime();
    }

    @Override
    public void setScrollStepsTime(int stepTime) {
        scrollDelegate.setScrollStepsTime(stepTime);
    }

    @Override
    public int getReachEndWaitingTime() {
        return scrollDelegate.getReachEndWaitingTime();
    }

    @Override
    public void setReachEndWaitingTime(int waitingTime) {
        scrollDelegate.setReachEndWaitingTime(waitingTime);
    }

    @Override
    public int getTextWidth(FontRenderContext frc) {
        return scrollDelegate.getTextWidth(frc);
    }

    @Override
    public int getDisplayableWidth() {
        return scrollDelegate.getDisplayableWidth();
    }

    @Override
    public FontRenderContext getFontRenderContext() {
        return scrollDelegate.getFontRenderContext();
    }

    @Override
    protected void paintComponent(Graphics g) {
        ScrollDelegate<AutoScrolledLabel> scrollDelegate = this.scrollDelegate;
        if ((g instanceof Graphics2D) && (scrollDelegate != null) /*&& (scrollDelegate.getCurrentPosition() != 0)*/) {
            Graphics2D g2d = (Graphics2D) g;
            setupGraphics(g2d);
            scrollDelegate.paintComponent(g2d);
        } else {
            super.paintComponent(g);
        }
    }

    @Override
    protected void finalize() {
        super.finalize();
        ScrollDelegate<AutoScrolledLabel> scrollDelegate = this.scrollDelegate;
        if (scrollDelegate != null) {
            scrollDelegate.unconfigure();
            scrollDelegate.cancelAutoScroll();
        }
        this.scrollDelegate = null;
    }

}
