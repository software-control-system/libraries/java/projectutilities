/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import fr.soleil.lib.project.ObjectUtils;

/**
 * Scrolling text.
 * <p>
 * Based on the code of the class
 * <a href="http://b.kostrzewa.free.fr/java/td-processus/defil.html">DefilText</a> developped by Bruno Kostrzewa.
 * </p>
 * 
 * @author Bruno Kostrzewa, Rapha&euml;l GIRARDOT
 * @deprecated It is recommended to use {@link fr.soleil.lib.project.swing.text.scroll.AutoScrolledLabel} instead
 */
@Deprecated
public class ScrollingText extends JLabel {

    private static final long serialVersionUID = -4349078235966106682L;

    protected String msg = null; // displayed message
    protected Process process = null; // refreshing process
    protected int xPos = 0; // display position
    protected int yPos = 0;
    protected int len = 0; // message length
    protected int speed = 20; // scrolling speed
    protected int textAlignment = LEFT;

    public final static int LEFT = 0; // text is initially on the left
    public final static int CENTER = 1; // text is initially centered
    public final static int RIGHT = 2; // text is initially on the right
    public final static int AWAY = 3; // text is initially away on the right

    // double buffering
    protected Image img = null; // stored image
    protected Graphics gi = null; // graphics of the stored image

    /**
     * Constructor
     * 
     * @param text The text to display
     */
    public ScrollingText(String text) {
        super();
        setText(text);
        setFont(new Font("TimesRoman", Font.BOLD, 36));
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                resetTextPosition();
            }
        });
    }

    /**
     * Starts the automatic scrolling
     */
    public void start() {
        if (process == null) {
            process = new Process(this);
            process.setSpeed(speed);
            process.start();
        }
    }

    /**
     * Stops the automatic scrolling
     */
    public void stop() {
        if (process != null) {
            process.stopProcess();
        }
        process = null;
    }

    /**
     * Calculates and draws the image representing the message
     * 
     * @param initTextPosition The initial message position if first draw
     */
    public void drawImage(int initTextPosition) {
        if (len == 0) {
            // text dimension evaluation
            if (getWidth() > 0 && getHeight() > 0) {
                img = createImage(getSize().width, getSize().height);
                if (img != null) {
                    gi = img.getGraphics();
                    gi.setFont(getFont());
                    FontMetrics fm = gi.getFontMetrics();
                    len = fm.stringWidth(msg);
                    switch (initTextPosition) {
                        case CENTER:
                            xPos = (getSize().width - len) / 2;
                            break;
                        case AWAY:
                            xPos = getSize().width;
                            break;
                        case RIGHT:
                            xPos = getSize().width - len;
                            break;
                        case LEFT:
                        default:
                            xPos = 0;
                    }
                    yPos = (getSize().height - fm.getHeight()) / 2 + fm.getAscent();
                }
            }
        }
        gi.setColor(getBackground());
        gi.fillRect(0, 0, getSize().width, getSize().height);
        gi.setColor(getForeground());
        gi.drawString(msg, xPos, yPos);
    }

    @Override
    public void paint(Graphics g) {
        if (img != null) {
            g.drawImage(img, 0, 0, this);
        }
    }

    @Override
    public void update(Graphics g) {
        paint(g);
    }

    /**
     * Returns the message x position
     * 
     * @return the message x position
     */
    public int getXPos() {
        return xPos;
    }

    /**
     * Sets the message x position
     * 
     * @param xPos the message x position
     */
    public void setXPos(int xPos) {
        this.xPos = xPos;
    }

    /**
     * Returns the message y position
     * 
     * @return the message y position
     */
    public int getYPos() {
        return yPos;
    }

    /**
     * Sets the message y position
     * 
     * @param yPos the message y position
     */
    public void setYPos(int yPos) {
        this.yPos = yPos;
    }

    /**
     * Reinitializes message position
     */
    public void resetTextPosition() {
        len = 0;
    }

    /**
     * Returns the scrolling speed
     * 
     * @return The scrolling speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * Sets the scrolling speed
     * 
     * @param speed the scrolling speed
     */
    public void setSpeed(int speed) {
        this.speed = speed;
        if (process != null) {
            process.setSpeed(speed);
        }
    }

    /**
     * Returns the current message length
     * 
     * @return The current message length. This values equals 0 at initialization and on Component
     *         resizing.
     */
    public int getCurrentMessageLength() {
        return len;
    }

    /**
     * Returns the evaluated message length
     * 
     * @return The evaluated message length
     */
    public int getEvaluatedMessageLength() {
        int eval = 0;
        eval = len;
        if (eval <= 0) {
            if (getWidth() > 0 && getHeight() > 0) {
                Image img = createImage(getSize().width, getSize().height);
                if (img != null) {
                    Graphics g = img.getGraphics();
                    g.setFont(getFont());
                    FontMetrics fm = g.getFontMetrics();
                    eval = fm.stringWidth(msg);
                    g.dispose();
                }
            }
        }
        return eval;
    }

    /**
     * Sets the text of the displayed message
     * 
     * @param text the text of the displayed message
     */
    @Override
    public void setText(String text) {
        if (!ObjectUtils.sameObject(msg, text)) {
            msg = text;
            if (msg == null) {
                msg = "No Text";
            }
            resetTextPosition();
        }
    }

    public int getTextAlignment() {
        return textAlignment;
    }

    public void setTextAlignment(int textAlignment) {
        this.textAlignment = textAlignment;
    }

    public static void main(String[] args) throws Exception {
        // use example
        String text = "This is a long test example so try to display it..................";
        if (args.length > 0)
            text = args[0];
        ScrollingText defil = new ScrollingText(text);
        JFrame f = new JFrame(ScrollingText.class.getSimpleName());
        f.setContentPane(defil);
        f.setSize(200, 200);
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setVisible(true);
        defil.setSpeed(30);
        defil.setBackground(Color.BLUE);
        defil.setForeground(Color.CYAN);
        defil.start();
        defil.setFont(new Font("Times New Roman", Font.ITALIC, 26));
    }

    // The scrolling Thread
    protected class Process extends Thread {
        protected boolean stop = false;
        protected int speed = 20;
        protected ScrollingText scroller;

        public Process(ScrollingText defil) {
            super();
            this.scroller = defil;
        }

        @Override
        public void run() {
            while (!stop) {
                if (scroller.getEvaluatedMessageLength() < scroller.getSize().width) {
                    if (scroller.getCurrentMessageLength() == 0) {
                        scroller.drawImage(textAlignment);
                        scroller.repaint();
                    }
                } else {
                    scroller.drawImage(scroller.getSize().width);
                    scroller.repaint();
                    scroller.setXPos(scroller.getXPos() - 1);
                    if (scroller.getXPos() <= -scroller.getCurrentMessageLength()) {
                        scroller.setXPos(scroller.getSize().width);
                    }
                }
                try {
                    sleep(speed);
                } catch (InterruptedException e) {
                    // No need to treat this exception;
                }
            }
        }

        public void stopProcess() {
            stop = true;
        }

        public int getSpeed() {
            return speed;
        }

        public void setSpeed(int speed) {
            this.speed = speed;
        }
    }
}
