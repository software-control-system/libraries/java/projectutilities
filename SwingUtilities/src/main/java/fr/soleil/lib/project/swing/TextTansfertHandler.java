/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Collection;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

/**
 * A Transfert handler that is settable on any JComponent in order to drag and
 * drop a text
 * 
 * @author saintin
 */
public class TextTansfertHandler extends TransferHandler {

    private static final long serialVersionUID = 5803636818774193675L;

    protected String defaultTextToTransfert;
    private final ITextTransferable transferable;
    private ITextCollectionTransferable listTransferable;
    protected MouseTransferManager mouseAdapter;
    private static DataFlavor uriListFlavor;

    public static DataFlavor getUriListFlavor() {
        // On Linux and OS X, file explorers don't take javaFileListFlavor
        // but do take this
        if (uriListFlavor == null) {
            try {
                uriListFlavor = new DataFlavor("text/uri-list;class=java.lang.String");
            } catch (ClassNotFoundException e) {
                // cannot happen
                e.printStackTrace();
            }
        }
        return uriListFlavor;
    }

    protected TextTansfertHandler(ITextTransferable transferable, String defaultTextToTransfert) {
        super("name");
        this.transferable = transferable;
        this.defaultTextToTransfert = defaultTextToTransfert;
        mouseAdapter = new MouseTransferManager();
    }

    protected void setTextCollectionTransferable(ITextCollectionTransferable listTransferable) {
        this.listTransferable = listTransferable;
    }

    /**
     * Registers a {@link TextTansfertHandler} for a given {@link JComponent},
     * if and only if the {@link JComponent} does not already have a
     * {@link TextTansfertHandler} as {@link TransferHandler}
     * 
     * @param comp
     *            The {@link JComponent}
     */
    public static void registerTransferHandler(JComponent comp) {
        registerTransferHandler(comp, null);
    }

    /**
     * Registers a {@link TextTansfertHandler} for a given {@link JComponent},
     * if and only if the {@link JComponent} does not already have a
     * {@link TextTansfertHandler} as {@link TransferHandler}
     * 
     * @param comp
     *            The {@link JComponent}
     */
    public static void registerTransferHandler(JComponent comp, String defaultTextToTransfer) {
        registerTransferHandler(null, comp, defaultTextToTransfer);
    }

    /**
     * Registers a {@link TextTansfertHandler} for a given {@link JComponent},
     * if and only if the {@link JComponent} does not already have a
     * {@link TextTansfertHandler} as {@link TransferHandler}
     * 
     * @param comp
     *            The {@link JComponent}
     */
    public static void registerTransferHandler(ITextTransferable transferable, JComponent comp) {
        registerTransferHandler(transferable, comp, null);
    }

    /**
     * Registers a {@link TextTansfertHandler} for a given {@link JComponent},
     * if and only if the {@link JComponent} does not already have a {@link TextTansfertHandler} as
     * {@link TransferHandler}
     * 
     * @param comp
     *            The {@link JComponent}
     */
    public static void registerTransferHandler(ITextCollectionTransferable transferable, JComponent comp) {
        registerTransferHandler(null, transferable, comp, null);
    }

    /**
     * Registers a {@link TextTansfertHandler} for a given {@link JComponent},
     * if and only if the {@link JComponent} does not already have a
     * {@link TextTansfertHandler} as {@link TransferHandler}
     * 
     * @param comp
     *            The {@link JComponent}
     * @param defaultTextToTransfer
     *            The default text to use as transfer text, when a transfer text
     *            could not be created
     */
    public static void registerTransferHandler(ITextTransferable transferable, JComponent comp,
            String defaultTextToTransfer) {
        registerTransferHandler(transferable, null, comp, defaultTextToTransfer);
    }

    /**
     * Registers a {@link TextTansfertHandler} for a given {@link JComponent},
     * if and only if the {@link JComponent} does not already have a {@link TextTansfertHandler} as
     * {@link TransferHandler}
     * 
     * @param comp
     *            The {@link JComponent}
     * @param defaultTextToTransfer
     *            The default text to use as transfer text, when a transfer text
     *            could not be created
     */
    public static void registerTransferHandler(ITextTransferable transferable,
            ITextCollectionTransferable listTransferable, JComponent comp, String defaultTextToTransfer) {
        if (comp != null) {
            if (!(comp.getTransferHandler() instanceof TextTansfertHandler)) {
                ITextTransferable tmpTransferable = transferable;
                TextTansfertHandler handler = null;
                if ((comp instanceof ITextTransferable) && (transferable == null)) {
                    tmpTransferable = (ITextTransferable) comp;
                }
                handler = new TextTansfertHandler(tmpTransferable, defaultTextToTransfer);
                handler.setTextCollectionTransferable(listTransferable);
                comp.setTransferHandler(handler);
                comp.addMouseListener(handler.getMouseAdapter());
            }
        }
    }

    /**
     * Cleans a given {@link JComponent} if its {@link TransferHandler} is a
     * {@link TextTansfertHandler}
     * 
     * @param comp
     *            The {@link JComponent}
     */
    public static void cleanTransferHandler(JComponent comp) {
        if (comp != null) {
            TransferHandler temp = comp.getTransferHandler();
            if (temp instanceof TextTansfertHandler) {
                TextTansfertHandler handler = (TextTansfertHandler) temp;
                comp.setTransferHandler(null);
                comp.removeMouseListener(handler.getMouseAdapter());
            }
        }
    }

    /**
     * Returns the {@link MouseAdapter} associated with this
     * {@link TextTansfertHandler} to ensure the correct drag'n'drop management
     * 
     * @return A {@link MouseAdapter}
     */
    public MouseAdapter getMouseAdapter() {
        return mouseAdapter;
    }

    @Override
    protected Transferable createTransferable(JComponent c) {
        Transferable result = null;

        if (listTransferable != null) {
            Collection<String> uriList = listTransferable.getTextCollectionToTransfert();
            result = new StringCollectionSelection(uriList);
        } else {
            String tmpText = null;
            if (transferable != null) {
                tmpText = transferable.getTextToTransfert();
            }
            if (tmpText == null) {
                tmpText = defaultTextToTransfert;
            }
            if ((tmpText != null) && !tmpText.trim().isEmpty()) {
                result = new StringSelection(tmpText.trim());
            }
        }

        return result;
    }

    private class StringCollectionSelection implements Transferable {

        private String data;

        public StringCollectionSelection(Collection<String> stringList) {
            if (stringList != null) {
                if (stringList.size() == 1) {
                    data = stringList.iterator().next();
                } else if (stringList.size() > 1) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (String uri : stringList) {
                        stringBuilder.append(uri);
                        stringBuilder.append("\r");
                    }
                    data = stringBuilder.toString();
                }
            }
        }

        @Override
        public DataFlavor[] getTransferDataFlavors() {
            return new DataFlavor[] { TextTansfertHandler.getUriListFlavor() };
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            boolean isSupported = false;
            DataFlavor urListFlavor = TextTansfertHandler.getUriListFlavor();
            if ((flavor != null) && (urListFlavor != null)) {
                isSupported = flavor.equals(urListFlavor);
                if (!isSupported) {
                    isSupported = flavor.equals(DataFlavor.stringFlavor);
                }
            }
            return isSupported;
        }

        @Override
        public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
            // JCK Test StringSelection0007: if 'flavor' is null, throw NPE
            if (isDataFlavorSupported(flavor) && (data != null)) {
                return data;
            } else {
                throw new UnsupportedFlavorException(flavor);
            }
        }

    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class MouseTransferManager extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            Object source = e.getSource();
            if (source instanceof JComponent) {
                JComponent comp = (JComponent) source;
                TransferHandler handler = comp.getTransferHandler();
                handler.exportAsDrag(comp, e, TransferHandler.COPY);
            }
        }
    }

}
