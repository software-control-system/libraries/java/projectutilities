/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

/**
 * An {@link ITextComponent} for which one can interact with its editability.
 * <p>
 * Typically, you can force a <code>JTextComponent</code> to implement that interface without code impact.
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IEditableTextComponent extends ITextComponent {

    /**
     * Returns whether this {@link IEditableTextComponent} is editable.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isEditable();

    /**
     * Sets whether this {@link IEditableTextComponent} should be editable.
     * 
     * @param editable Whether this {@link IEditableTextComponent} should be editable.
     */
    public void setEditable(boolean editable);

}
