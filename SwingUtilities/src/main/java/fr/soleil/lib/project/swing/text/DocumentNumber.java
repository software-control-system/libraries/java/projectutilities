/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

/**
 * Used to filter textfields with number formats
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DocumentNumber extends AFilteredDocument {

    private static final long serialVersionUID = 1031277852234184282L;

    private boolean allowNegativeValues = false;
    private boolean allowFloatValues = false;
    private boolean allowScientificValues = false;

    public DocumentNumber() {
        super();
    }

    /**
     * Returns whether there is an 'e' or an 'E' before given index in document.
     * 
     * @param index The index.
     * @return A <code>boolean</code>.
     */
    protected boolean hasScientificBeforeIndex(int index) {
        boolean scientific;
        if (index < 0) {
            scientific = false;
        } else {
            try {
                String text = getText(0, index).toLowerCase();
                scientific = text.indexOf('e') > -1;
            } catch (Exception e) {
                scientific = false;
            }
        }
        return scientific;
    }

    /**
     * @return the allowNegativeValues
     */
    public boolean isAllowNegativeValues() {
        return allowNegativeValues;
    }

    /**
     * @param allowNegativeValues the allowNegativeValues to set
     */
    public void setAllowNegativeValues(boolean allowNegativeValues) {
        this.allowNegativeValues = allowNegativeValues;
    }

    /**
     * @return the allowFloatValues
     */
    public boolean isAllowFloatValues() {
        return allowFloatValues;
    }

    /**
     * @param allowFloatValues the allowFloatValues to set
     */
    public void setAllowFloatValues(boolean allowFloatValues) {
        this.allowFloatValues = allowFloatValues;
    }

    /**
     * @return the allowScientificValues
     */
    public boolean isAllowScientificValues() {
        return allowScientificValues;
    }

    /**
     * @param allowScientificValues the allowScientificValues to set
     */
    public void setAllowScientificValues(boolean allowScientificValues) {
        this.allowScientificValues = allowScientificValues;
    }

    // Numbers only
    @Override
    protected StringBuilder buildFilteredValue(String str, int offs) {
        StringBuilder sb = new StringBuilder();
        char car;
        boolean e = false;
        for (int i = 0; i < str.length(); i++) {
            car = str.charAt(i);
            if (Character.isDigit(car)) {
                sb.append(car);
            } else if (Character.toLowerCase(car) == 'e' && isAllowScientificValues()) {
                e = true;
                sb.append(car);
            } else if (car == '.' && isAllowFloatValues()) {
                sb.append(car);
            } else if (car == '-' && (isAllowNegativeValues() || e || hasScientificBeforeIndex(offs))) {
                sb.append(car);
            }
        }
        return sb;
    }

}
