/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.border;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Area;

import javax.swing.border.Border;

/**
 * A {@link Border} with a particular {@link Shape}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class ShapedBorder implements Border {

    protected final Color color;
    protected final BasicStroke stroke;

    public ShapedBorder(Color color, BasicStroke stroke) {
        super();
        this.color = color == null ? Color.BLACK : color;
        this.stroke = stroke == null ? new BasicStroke() : stroke;
    }

    /**
     * Builds the {@link Area} of this {@link ShapedBorder}.
     * That {@link Area} contains the shape of this {@link ShapedBorder} and will be used in
     * {@link #paintBorder(Component, Graphics, int, int, int, int)} in order to both draw shape and erase what is
     * outside of the shape <i>(because component's background color is painted before the border)</i>.
     * 
     * @param x the x position of the painted border
     * @param y the y position of the painted border
     * @param width the width of the painted border
     * @param height the height of the painted border
     * @return An {@link Area}, that represents this border's shape.
     */
    protected abstract Area buildShapedArea(int x, int y, int width, int height);

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        Graphics2D g2d = (Graphics2D) g;
        // Note: the initial solution proposed an offset in x and y, but it is not useful with anti aliasing.
        Area area = buildShapedArea(x, y, width, height);

        // Paint the BG color of the parent, everywhere outside the clip of the shape.
        // This must be done because component's background color is painted before the border.
        Component parent = c.getParent();
        while ((parent != null) && (!parent.isOpaque())) {
            parent = parent.getParent();
        }
        if (parent != null) {
            Color bg = parent.getBackground();
            Rectangle rect = new Rectangle(0, 0, width, height);
            Area borderRegion = new Area(rect);
            borderRegion.subtract(area);
            g2d.setClip(borderRegion);
            g2d.setColor(bg);
            g2d.fillRect(0, 0, width, height);
            g2d.setClip(null);
        }
        // Paint the border, using anti aliasing
        g2d.setColor(color);
        Object storedAntiAliasing = g2d.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setStroke(stroke);
        drawShapedArea(g2d, area, x, y, width, height);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, storedAntiAliasing);
    }

    /**
     * Draws this borders shape in previously configured {@link Graphics2D}.
     * 
     * @param g2D The {@link Graphics2D}, in which border stroke and anti aliasing are already configured. Must not be
     *            <code>null</code>.
     * @param area The shaped area, returned by {@link #buildShapedArea(int, int, int, int)}. Must not be
     *            <code>null</code>.
     * @param x the x position of the painted border.
     * @param y the y position of the painted border.
     * @param width the width of the painted border.
     * @param height the height of the painted border.
     */
    protected void drawShapedArea(Graphics2D g2D, Area area, int x, int y, int width, int height) {
        g2D.draw(area);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        // By default, let's consider there is no particular limit except the line width.
        // It can be supposed so, because paintBorder erases what is outside of the Shape.
        int gap = Math.round(stroke.getLineWidth());
        return new Insets(gap, gap, gap, gap);
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }

}
