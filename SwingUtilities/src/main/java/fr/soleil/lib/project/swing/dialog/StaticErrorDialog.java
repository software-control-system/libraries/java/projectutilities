/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.dialog;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;

/**
 * A {@link JFrame} used to display some error messages in a static instance
 * 
 * @author ho
 * 
 *         To change the template for this generated type comment go to
 *         Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class StaticErrorDialog extends AErrorMessageDialog {

    private static final long serialVersionUID = -6553888319594153949L;

    private static final StaticErrorDialog STATIC_ERROR_DIALOG = new StaticErrorDialog();

    // maximum text lines
    private static final int MAX_LINES = 300;

    // Default title
    private static final String DEFAULT_TITLE = "Data Connection Error";
    // "Clear" menu item text
    private static final String CLEAR_HISTORY = "Clear history";

    private StaticErrorDialog() {
        super();
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle(DEFAULT_TITLE);
    }

    @Override
    protected String getDefaultTitle() {
        return DEFAULT_TITLE;
    }

    @Override
    protected String getClearMenuItemText() {
        return CLEAR_HISTORY;
    }

    @Override
    protected int getMaxLines() {
        return MAX_LINES;
    }

    @Override
    protected int getMaxMessages() {
        return -1;
    }

    /**
     * Displays an error message in the static instance of {@link StaticErrorDialog}
     * 
     * @param e The {@link Throwable} that describes the error
     * @param message The error message to display
     */
    public static void showMessageDialog(final Throwable e, final String message) {
        showMessageDialog(STATIC_ERROR_DIALOG, null, e, message, false);
    }

    public static void main(String[] args) {
        STATIC_ERROR_DIALOG.setDefaultCloseOperation(EXIT_ON_CLOSE);
        showMessageDialog(null, new Date().toString());
        Thread timeThread = new Thread(StaticErrorDialog.class.getSimpleName() + " time thread test") {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

            @Override
            public void run() {
                int count = MAX_LINES * 10;
                int index = 0;
                while (index < count) {
                    STATIC_ERROR_DIALOG.setTitle(DEFAULT_TITLE + " (" + ++index + "/" + count + ")");
                    showMessageDialog(null, format.format(new Date()));
                    try {
                        sleep(200);
                    } catch (Exception e) {
                        e.printStackTrace();
                        index = count;
                    }
                }
            }
        };
        timeThread.start();
    }
}
