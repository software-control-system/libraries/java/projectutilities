/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JSplitPane;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

import fr.soleil.lib.project.swing.ArrowButton;

/**
 * A {@link BasicSplitPaneDivider} that respects {@link JSplitPane} background color.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ColoredSplitPaneDivider extends BasicSplitPaneDivider {

    private static final long serialVersionUID = -7296337603656709804L;

    public ColoredSplitPaneDivider(BasicSplitPaneUI ui) {
        super(ui);
        setLayout(new AdaptedButtonsDividerLayout());
    }

    @Override
    public void setDividerSize(int newSize) {
        super.setDividerSize(newSize);
        if (leftButton instanceof ArrowButton && rightButton instanceof ArrowButton) {
            ((ArrowButton) leftButton).setSize(newSize, newSize);
            ((ArrowButton) rightButton).setSize(newSize, newSize);
            revalidateSplitPane();
        }
    }

    /**
     * Does as if left/up button was clicked.
     */
    public void oneTouchExpandLeftOrUp() {
        ((ArrowButton) leftButton).mouseClicked(new MouseEvent(leftButton, MouseEvent.MOUSE_CLICKED,
                System.currentTimeMillis(), 0, 0, 0, 0, 0, 1, false, MouseEvent.BUTTON1));
    }

    /**
     * Does as if right/down button was clicked.
     */
    public void oneTouchExpandRightOrDown() {
        ((ArrowButton) rightButton).mouseClicked(new MouseEvent(rightButton, MouseEvent.MOUSE_CLICKED,
                System.currentTimeMillis(), 0, 0, 0, 0, 0, 1, false, MouseEvent.BUTTON1));
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        if ((e != null) && (e.getSource() == splitPane)
                && (JSplitPane.ORIENTATION_PROPERTY.equals(e.getPropertyName()))) {
            ((ArrowButton) leftButton).setOrientation(getLeftButtonOrientation((Integer) e.getNewValue()));
            ((ArrowButton) rightButton).setOrientation(getRightButtonOrientation((Integer) e.getNewValue()));
        }
        super.propertyChange(e);
    }

    /**
     * Gets the best left {@link ArrowButton} orientation for given {@link JSplitPane} orientation.
     * 
     * @param splitPaneOrientation The {@link JSplitPane} orientation.
     * @return An <code>int</code>.
     */
    protected int getLeftButtonOrientation(int splitPaneOrientation) {
        return splitPaneOrientation == JSplitPane.VERTICAL_SPLIT ? ArrowButton.UP : ArrowButton.LEFT;
    }

    /**
     * Gets the best right {@link ArrowButton} orientation for given {@link JSplitPane} orientation.
     * 
     * @param splitPaneOrientation The {@link JSplitPane} orientation.
     * @return An <code>int</code>.
     */
    protected int getRightButtonOrientation(int splitPaneOrientation) {
        return splitPaneOrientation == JSplitPane.VERTICAL_SPLIT ? ArrowButton.DOWN : ArrowButton.RIGHT;
    }

    @Override
    protected JButton createLeftOneTouchButton() {
        ArrowButton leftButton = new ArrowButton(getLeftButtonOrientation(splitPane.getOrientation()));
        leftButton.setMinimumSize(new Dimension(ArrowButton.DEFAULT_SIZE, ArrowButton.DEFAULT_SIZE));
        leftButton.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        leftButton.setFocusPainted(false);
        leftButton.setBorderPainted(false);
        leftButton.setRequestFocusEnabled(false);
        return leftButton;
    }

    @Override
    protected JButton createRightOneTouchButton() {
        ArrowButton rightButton = new ArrowButton(getRightButtonOrientation(splitPane.getOrientation()));
        rightButton.setMinimumSize(new Dimension(ArrowButton.DEFAULT_SIZE, ArrowButton.DEFAULT_SIZE));
        rightButton.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        rightButton.setFocusPainted(false);
        rightButton.setBorderPainted(false);
        rightButton.setRequestFocusEnabled(false);
        return rightButton;
    }

    @Override
    public boolean isOpaque() {
        boolean opaque;
        if (hiddenDivider instanceof JComponent) {
            opaque = hiddenDivider.isOpaque();
        } else {
            opaque = super.isOpaque();
        }
        return opaque;
    }

    public void setOpaque(boolean opaque) {
        if (hiddenDivider instanceof JComponent) {
            ((JComponent) hiddenDivider).setOpaque(opaque);
        }
    }

    protected void revalidateSplitPane() {
        invalidate();
        if (splitPane != null) {
            splitPane.revalidate();
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link LayoutManager} that always keeps maximum size for arrow buttons
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class AdaptedButtonsDividerLayout implements LayoutManager {
        @Override
        public void layoutContainer(Container c) {
            if ((leftButton != null) && (rightButton != null) && (c == ColoredSplitPaneDivider.this)) {
                if (splitPane.isOneTouchExpandable()) {
                    Insets insets = getInsets();
                    int margin, size;
                    if (orientation == JSplitPane.VERTICAL_SPLIT) {
                        margin = (insets == null) ? 0 : insets.left;
                        size = getHeight();

                        if (insets != null) {
                            size -= (insets.top + insets.bottom);
                            size = Math.max(size, 0);
                        }

                        int y = (c.getSize().height - size) / 2;

                        leftButton.setBounds(margin, y, size * 2, size);
                        rightButton.setBounds(margin + size * 2 + 2, y, size * 2, size);
                    } else {
                        margin = (insets == null) ? 0 : insets.top;
                        size = getWidth();

                        if (insets != null) {
                            size -= (insets.left + insets.right);
                            size = Math.max(size, 0);
                        }

                        int x = (c.getSize().width - size) / 2;

                        leftButton.setBounds(x, margin, size, size * 2);
                        rightButton.setBounds(x, margin + size * 2 + 2, size, size * 2);
                    }
                } else {
                    leftButton.setBounds(-5, -5, 1, 1);
                    rightButton.setBounds(-5, -5, 1, 1);
                }
            }
        }

        @Override
        public Dimension minimumLayoutSize(Container c) {
            Dimension minimumSize;
            if ((splitPane == null) || (c != ColoredSplitPaneDivider.this)) {
                minimumSize = new Dimension(0, 0);
            } else {
                int size = getDividerSize();
                if (orientation == JSplitPane.VERTICAL_SPLIT) {
                    minimumSize = new Dimension(size * 4 + 4, size);
                } else {
                    minimumSize = new Dimension(size, size * 4 + 4);
                }
            }
            return minimumSize;
        }

        @Override
        public Dimension preferredLayoutSize(Container c) {
            return minimumLayoutSize(c);
        }

        @Override
        public void removeLayoutComponent(Component c) {
            // not managed
        }

        @Override
        public void addLayoutComponent(String string, Component c) {
            // not managed
        }
    }
}
