/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.border;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;

import javax.swing.border.Border;

/**
 * An outlined round-cornered rectangle {@link Border}.
 * <p>
 * Code inspired by <a href=
 * "http://www.developpez.net/forums/d249953/java/interfaces-graphiques-java/awt-swing/fenetres-dialogues/bordure-arrondie-jpanel/">
 * this solution</a>.
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT, michel18
 * @see Graphics2D#drawRoundRect(int, int, int, int, int, int)
 */
public class RoundedBorder extends ShapedBorder {

    private final int arcWidth;
    private final int arcHeight;

    /**
     * Created a new {@link RoundedBorder}
     * 
     * @param color The border {@link Color}
     * @param arcWidth The horizontal diameter of the arc at the four corners
     * @param arcHeight The vertical diameter of the arc at the four corners
     */
    public RoundedBorder(Color color, int arcWidth, int arcHeight) {
        this(color, arcWidth, arcHeight, null);
    }

    /**
     * Created a new {@link RoundedBorder}
     * 
     * @param color The border {@link Color}
     * @param arcWidth The horizontal diameter of the arc at the four corners
     * @param arcHeight The vertical diameter of the arc at the four corners
     * @param stroke The stroke to use
     */
    public RoundedBorder(Color color, int arcWidth, int arcHeight, BasicStroke stroke) {
        super(color, stroke);
        this.arcWidth = arcWidth;
        this.arcHeight = arcHeight;
    }

    @Override
    protected Area buildShapedArea(int x, int y, int width, int height) {
        // Note: the initial solution proposed an offset in x and y, but it is not useful with anti aliasing.
        RoundRectangle2D.Double roundRect = new RoundRectangle2D.Double(x, y, width, height, arcWidth, arcHeight);
        Area area = new Area(roundRect);
        return area;
    }

    @Override
    protected void drawShapedArea(Graphics2D g2d, Area area, int x, int y, int width, int height) {
        g2d.drawRoundRect(x, y, width, height, arcWidth, arcHeight);
    }

}