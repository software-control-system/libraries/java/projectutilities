/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.listener;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyBoundsListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.FontUtils;
import fr.soleil.lib.project.awt.IBestFontSizeCalculator;
import fr.soleil.lib.project.awt.listener.IComponentMultiListener;
import fr.soleil.lib.project.map.ConcurrentWeakHashMap;

/**
 * A listener that dynamically changes a {@link JLabel}, {@link JTextComponent} or {@link AbstractButton} font size.
 * <p>
 * Font size algorithm found on <a href=
 * "https://stackoverflow.com/questions/2715118/how-to-change-the-size-of-the-font-of-a-jlabel-to-take-the-maximum-size">stackoverflow.com</a>
 * </p>
 * <table>
 * <tr>
 * <th style="text-align:left">Use case example:</th>
 * </tr>
 * <tr>
 * <td >
 * <code>
 * DynamicFontSizeManager manager = new DynamicFontSizeManager();<br />
 * JLabel label = new JLabel();<br />
 * manager.setupAndListenToComponent(label);
 * </code>
 * </td>
 * </tr>
 * </table>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DynamicFontSizeManager implements IComponentMultiListener, ComponentListener, PropertyChangeListener,
        HierarchyBoundsListener, HierarchyListener, DocumentListener {

    protected static final String UPDATE_COMPONENT_FONT_LATER = "updateComponentFontLater";
    protected static final String FONT = "font";
    protected static final String BORDER = "border";
    protected static final String ANCESTOR = "ancestor";
    protected static final String UI = "UI";
    protected static final String DOCUMENT = "document";
    // Map that stores the known "updateComponent" runnables to limit their creation
    protected static final Map<Component, Runnable> COMPONENT_UPDATERS = new ConcurrentWeakHashMap<>();
    // Collection that stores the runnables currently running in EDT, in order to limit their accumulation.
    protected static final Collection<Runnable> IN_EDT = Collections.newSetFromMap(new ConcurrentHashMap<>());

    // textComponents is used to recover JTextComponents Documents
    protected final Collection<WeakReference<JTextComponent>> textComponents = Collections
            .newSetFromMap(new ConcurrentHashMap<>());

    /**
     * Default constructor. Creates a new {@link DynamicFontSizeManager}.
     */
    public DynamicFontSizeManager() {
        super();
    }

    /**
     * Recovers the margin used by a {@link Component} for its {@link Border} and {@link Icon}.
     * 
     * @param component The {@link Component}.
     * @return Some {@link Insets}.
     */
    public static Dimension getBorderAndIconMargin(JComponent component) {
        int width = 0, height = 0;
        if (component != null) {
            Insets insets = component.getInsets();
            if (insets != null) {
                width += Math.abs(insets.left + insets.right);
                height += Math.abs(insets.top + insets.bottom);
            }
            if (component instanceof AbstractButton) {
                insets = ((AbstractButton) component).getMargin();
                if (insets != null) {
                    width += Math.abs(insets.left + insets.right);
                    height += Math.abs(insets.top + insets.bottom);
                }
            } else if (component instanceof JTextComponent) {
                insets = ((JTextComponent) component).getMargin();
                if (insets != null) {
                    width += Math.abs(insets.left + insets.right);
                    height += Math.abs(insets.top + insets.bottom);
                }
            }
            if (component instanceof JLabel) {
                JLabel label = (JLabel) component;
                Icon icon = label.getIcon();
                if (icon != null) {
                    switch (label.getHorizontalTextPosition()) {
                        case SwingConstants.LEFT:
                        case SwingConstants.RIGHT:
                        case SwingConstants.LEADING:
                        case SwingConstants.TRAILING:
                            int iconWidth = icon.getIconWidth();
                            if (iconWidth > 0) {
                                width += iconWidth + label.getIconTextGap();
                            }
                            break;
                        default:
                            break;
                    }
                }
            } else if (component instanceof AbstractButton) {
                AbstractButton button = (AbstractButton) component;
                Icon icon = button.getIcon();
                if (icon != null) {
                    switch (button.getHorizontalTextPosition()) {
                        case SwingConstants.LEFT:
                        case SwingConstants.RIGHT:
                        case SwingConstants.LEADING:
                        case SwingConstants.TRAILING:
                            int iconWidth = icon.getIconWidth();
                            if (iconWidth > 0) {
                                width += iconWidth + button.getIconTextGap();
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        return new Dimension(width, height);
    }

    public static Font deriveFont(Font font, int newSize) {
        return font.deriveFont((float) newSize);
    }

    /**
     * Adapts ratio according to floating error margin.
     * 
     * @param ratio The ratio to adapt.
     * @return The adapted ratio.
     */
    protected static float getBestRatio(float ratio) {
        float newRatio = ratio;
        float intRatio = Math.round(newRatio);
        if (intRatio > 0 && Math.abs(newRatio - intRatio) < 1e-10) {
            newRatio = intRatio;
        }
        return newRatio;
    }

    /**
     * Returns the maximum text width allowed inside a {@link Component}.
     * 
     * @param component The {@link Component}.
     * @param borderAndIconMargin The previously calculated margins, using {@link #getBorderAndIconMargin(JComponent)}.
     * @param compWidth The previously calculated component width. A value &lt;0 can be used to force component width
     *            calculation.
     * @return An <code>int</code>: the maximum text width.
     */
    protected static int getMaxAllowedWidth(Component component, Dimension borderAndIconMargin, int compWidth) {
        if (compWidth < 0) {
            compWidth = component.getWidth();
        }
        if (borderAndIconMargin != null) {
            compWidth -= borderAndIconMargin.width;
        }
        if (compWidth < 0) {
            compWidth = 0;
        }
        return compWidth;
    }

    /**
     * Returns the text width inside a {@link Component}, according to given {@link Font}.
     * 
     * @param textBounds The previously calculated text bounds, using
     *            {@link Font#getStringBounds(String, FontRenderContext)}
     * @param component The {@link Component}
     * @param font The {@link Font}.
     * @param text The text.
     * @return An <code>int</code>: the text width.
     */
    protected static int getWidth(Rectangle2D textBounds, Component component, Font font, String text) {
        int width;
        if (textBounds == null) {
            width = component.getFontMetrics(font).stringWidth(text);
        } else {
            width = Math.max((int) Math.ceil(textBounds.getWidth()), component.getFontMetrics(font).stringWidth(text));
            if ((component instanceof JLabel) || (component instanceof JTextComponent)) {
                // for labels and text components, an horizontal margin must be taken
                // (don't ask me why, experience showed it so)
                width += 1 + (int) Math.ceil(Math.abs(textBounds.getMinX()));
            }
        }
        return width;
    }

    /**
     * Returns the text height inside a {@link Component}.
     * 
     * @param textBounds The previously calculated text bounds, using
     *            {@link Font#getStringBounds(String, FontRenderContext)}
     * @return An <code>int</code>: the text height.
     * @deprecated You should use {@link FontUtils#getFontHeight(Rectangle2D)} instead.
     */
    @Deprecated
    public static int getHeight(Rectangle2D textBounds) {
        return FontUtils.getFontHeight(textBounds);
    }

    /**
     * Returns the {@link Font} height inside a {@link Component}.
     * 
     * @param font The {@link Font}.
     * @param frc The {@link FontRenderContext}.
     * @param component The {@link Component}.
     * @param text The text.
     * @param textBounds The previously calculated text bounds. Can be <code>null</code>.
     * @return A <code>float</code>: the font height.
     * @deprecated You should use
     *             {@link FontUtils#getFontHeight(Font, FontRenderContext, String, Component, Rectangle2D)} instead.
     */
    @Deprecated
    public static int getFontHeight(Font font, FontRenderContext frc, String text, Component component,
            Rectangle2D textBounds) {
        return FontUtils.getFontHeight(font, frc, text, component, textBounds);
    }

    /**
     * Computes the best {@link Font} size for a {@link Component}, according to given text.
     * <p>
     * Callq
     * {@link #computeBestFontSizeAccordingHeight(Font, FontRenderContext, String, Component, int, Rectangle2D, int)},
     * computing the text bounds first.
     * </p>
     * 
     * @param component The {@link Component}.
     * @param font The referent {@link Font}.
     * @param text The text.
     * @param fixedTextWidth The previously calculated text width. Use a value &gt; 0 to avoid calculating text width.
     * @param compWidth The previously calculated component width. Use a value &ge; 0 to avoid asking component's width.
     * @param borderAndIconMargin The previously calculated border and icon margin (using
     *            {@link #getBorderAndIconMargin(JComponent)}).
     * @return An <code>int</code>: the calculated best font size. <code>-1</code> if such a font size could not be
     *         calculated. You should avoid <code>0</code> too.
     * @see #computeBestFontSizeIgnoreHeight(Component, Font, String, int, int, Rectangle2D, Dimension)
     */
    public static int computeBestFontSizeIgnoreHeight(Component component, Font font, String text, int fixedTextWidth,
            int compWidth, Dimension borderAndIconMargin) {
        Rectangle2D textBounds = null;
        FontRenderContext frc = getFontRenderContext(component, text);
        if (frc != null) {
            textBounds = font.getStringBounds(text, frc);
        }
        // Take account of maximum size only if programmatically set (JAVAAPI-644)
        Dimension maxSize = (component == null) || (!component.isMaximumSizeSet()) ? null : component.getMaximumSize();
        int maxWidth = maxSize == null ? 0 : maxSize.width;
        if (borderAndIconMargin != null) {
            maxWidth -= borderAndIconMargin.width;
        }
        return computeBestFontSizeIgnoreHeight(component, font, text, fixedTextWidth, compWidth, textBounds,
                borderAndIconMargin, maxWidth);
    }

    /**
     * Computes the best {@link Font} size for a {@link Component}, according to given text.
     * 
     * @param component The {@link Component}.
     * @param font The referent {@link Font}.
     * @param text The text.
     * @param fixedTextWidth The previously calculated text width. Use a value &gt; 0 to avoid calculating text width.
     * @param compWidth The previously calculated component width. Use a value &ge; 0 to avoid asking component's width.
     * @param textBounds The previously calculated text bounds.
     * @param borderAndIconMargin The previously calculated border and icon margin (using
     *            {@link #getBorderAndIconMargin(JComponent)}).
     * @param knownMaxWidth The known maximum width for the component (already reduced by border and icon margin)
     * @return An <code>int</code>: the calculated best font size. <code>-1</code> if such a font size could not be
     *         calculated. You should avoid <code>0</code> too.
     */
    public static int computeBestFontSizeIgnoreHeight(Component component, Font font, String text, int fixedTextWidth,
            int compWidth, Rectangle2D textBounds, Dimension borderAndIconMargin, int knownMaxWidth) {
        int fontSizeToUse;
        if (text == null || text.isEmpty()) {
            // No text found: no best font size.
            fontSizeToUse = -1;
        } else {
            // Step 2: Recover component and font sizes.
            compWidth = getMaxAllowedWidth(component, borderAndIconMargin, compWidth);
            int textWidth = fixedTextWidth < 1 ? getWidth(textBounds, component, font, text) : fixedTextWidth;
            // Step 3: Find out how much the font can grow in width.
            float widthRatio = getBestRatio((float) compWidth / (float) textWidth);
            fontSizeToUse = (int) Math.floor(font.getSize() * widthRatio);
            // Step 4: Limit font size according to known maximum text width if it is > 0 (JAVAAPI-644)
            if (knownMaxWidth > 0) {
                int newWidth = textWidth * Math.round(fontSizeToUse / font.getSize2D());
                if (newWidth > knownMaxWidth) {
                    widthRatio = getBestRatio(knownMaxWidth / (float) newWidth);
                    fontSizeToUse = Math.round(fontSizeToUse * widthRatio);
                    if (fontSizeToUse == 0) {
                        fontSizeToUse = -1;
                    }
                }
            }
        }
        return fontSizeToUse;
    }

    /**
     * Computes the best {@link Font} size for a {@link Component}, according to given text.
     * 
     * @param component The {@link Component}.
     * @param font The referent {@link Font}.
     * @param text The text.
     * @param fixedTextWidth The previously calculated text width. Use a value &gt; 0 to avoid calculating text width.
     * @param compWidth The previously calculated component width. Use a value &ge; 0 to avoid asking component's width.
     * @param textBounds The previously calculated text bounds.
     * @param borderAndIconMargin The previously calculated border and icon margin (using
     *            {@link #getBorderAndIconMargin(JComponent)}).
     * @return An <code>int</code>: the calculated best font size. <code>-1</code> if such a font size could not be
     *         calculated. You should avoid <code>0</code> too.
     */
    public static int computeBestFontSizeIgnoreHeight(Component component, Font font, String text, int fixedTextWidth,
            int compWidth, Rectangle2D textBounds, Dimension borderAndIconMargin) {
        return computeBestFontSizeIgnoreHeight(component, font, text, fixedTextWidth, compWidth, textBounds,
                borderAndIconMargin, 0);
    }

    /**
     * Adapts best {@link Font} size according to {@link Component} height.
     * 
     * @param font The {@link Font}.
     * @param frc The {@link FontRenderContext}.
     * @param text The known text.
     * @param component The {@link Component}.
     * @param compHeight The previously calculated maximum allowed text height.
     * @param textBounds The previously calculated text bounds.
     * @param fontSizeToUse The previously calculated best font size (ignoring height).
     * @param knownMaxHeight The known maximum height allowed for the {@link Component} (already reduced by border and
     *            icon margin).
     * @return An <code>int</code>: the readapted best font size.
     */
    protected static int computeBestFontSizeAccordingHeight(Font font, FontRenderContext frc, String text,
            Component component, int compHeight, Rectangle2D textBounds, int fontSizeToUse, int knownMaxHeight) {
        float currentFontHeight;
        if ((text != null) && (!text.isEmpty()) && (frc != null)
                && ((component instanceof JLabel) || (component instanceof JTextComponent))) {
            TextLayout layout = new TextLayout(text, font, frc);
            currentFontHeight = FontUtils.getFontHeight(layout);
            // for labels and text components, a vertical margin must be taken
            // (don't ask me why, experience proved it so)
            currentFontHeight += Math.ceil(layout.getDescent()) + 1;
        } else {
            currentFontHeight = FontUtils.getFontHeight(font, frc, text, component, textBounds);
        }
        float heightRatio = getBestRatio(compHeight / currentFontHeight);
        int bestSize = Math.min(fontSizeToUse, Math.round(font.getSize() * heightRatio));
        // Try to limit to maximum height if it is > 0 (JAVAAPI-644)
        if (knownMaxHeight > 0) {
            int newHeight = Math.round(currentFontHeight * (bestSize / font.getSize2D()));
            if (newHeight > knownMaxHeight) {
                heightRatio = getBestRatio(knownMaxHeight / (float) newHeight);
                bestSize *= Math.round(bestSize * heightRatio);
                if (bestSize == 0) {
                    bestSize = -1;
                }
            }
        }
        return bestSize;
    }

    /**
     * Adapts best {@link Font} size according to {@link Component} height.
     * 
     * @param font The {@link Font}.
     * @param frc The {@link FontRenderContext}.
     * @param text The known text.
     * @param component The {@link Component}.
     * @param compHeight The previously calculated maximum allowed text height.
     * @param textBounds The previously calculated text bounds.
     * @param fontSizeToUse The previously calculated best font size (ignoring height).
     * @return An <code>int</code>: the readapted best font size.
     */
    protected static int computeBestFontSizeAccordingHeight(Font font, FontRenderContext frc, String text,
            Component component, int compHeight, Rectangle2D textBounds, int fontSizeToUse) {
        return computeBestFontSizeAccordingHeight(font, frc, text, component, compHeight, textBounds, fontSizeToUse, 0);
    }

    /**
     * Recovers the {@link FontRenderContext} for a {@link Component} with given text.
     * 
     * @param component The {@link Component}. Must not be <code>null</code>.
     * @param text The text.
     * @return A {@link FontRenderContext}. Might be <code>null</code>.
     */
    public static FontRenderContext getFontRenderContext(Component component, String text) {
        FontRenderContext frc = null;
        if ((text != null) && (!text.isEmpty())) {
            Graphics g = component.getGraphics();
            if (g instanceof Graphics2D) {
                frc = ((Graphics2D) g).getFontRenderContext();
            } else {
                frc = FontUtils.getDefaultRenderContext();
            }
        }
        return frc;
    }

    /**
     * Returns the estimated size of a {@link JComponent} for given text with given {@link Font} readapted to given font
     * size.
     * 
     * @param comp The {@link JComponent}.
     * @param text The text.
     * @param font The {@link Font}.
     * @param fontSize The font size.
     * @param frc The known {@link FontRenderContext} (which can be obtained with
     *            {@link #getFontRenderContext(Component, String)}).
     * @param borderAndIconMargin The known border and icon margin (which can be obtained with
     *            {@link #getBorderAndIconMargin(JComponent)}).
     * @return A {@link Dimension}: the estimated size.
     */
    public static Dimension getEstimatedSize(JComponent comp, String text, Font font, int fontSize,
            FontRenderContext frc, Dimension borderAndIconMargin) {
        Dimension size;
        if ((comp == null) || (font == null) || (frc == null)) {
            size = null;
        } else {
            TextLayout layout = new TextLayout(text == null ? ObjectUtils.EMPTY_STRING : text,
                    deriveFont(font, fontSize), frc);
            Rectangle2D bounds = layout.getBounds();
            int width = (int) Math.round(bounds.getWidth());
            int height = (int) Math.round(bounds.getHeight());
            if (borderAndIconMargin != null) {
                width += borderAndIconMargin.width;
                height += borderAndIconMargin.height;
            }
            if (comp instanceof JLabel) {
                // Experience proved their is a 5 pixels margin for JLabel
                width += 5;
                height += 5;
            }
            size = new Dimension(width, height);
        }
        return size;
    }

    /**
     * Computes the best font size for a {@link Component}, according to given {@link Font} and component's current
     * dimensions.
     * 
     * @param component The {@link Component}.
     * @param font The {@link Font}.
     * @return An <code>int</code>. <code>-1</code> if such a font size could not be calculated. You should avoid
     *         <code>0</code> too.
     */
    public static int computeBestFontSize(Component component, Font font) {
        int fontSizeToUse;
        // Otherwise, best font size may be calculated according to component's current font, text and dimensions.
        String text = null;
        Dimension borderAndIconMargin = null;
        Rectangle2D textBounds = null;
        // Step 1: Recover text and border margin.
        if (component instanceof JLabel) {
            JLabel label = (JLabel) component;
            text = label.getText();
            borderAndIconMargin = getBorderAndIconMargin(label);
        } else if (component instanceof JTextComponent) {
            JTextComponent textComponent = (JTextComponent) component;
            text = textComponent.getText();
            borderAndIconMargin = getBorderAndIconMargin(textComponent);
        } else if (component instanceof AbstractButton) {
            AbstractButton button = (AbstractButton) component;
            text = button.getText();
            borderAndIconMargin = getBorderAndIconMargin(button);
        }
        int knownMaxWidth, knownMaxHeight;
        Dimension maxSize;
        // Take account of maximum size only if programmatically set (JAVAAPI-644)
        if ((component == null) || !component.isMaximumSizeSet()) {
            maxSize = null;
        } else {
            maxSize = component.getMaximumSize();
        }
        if (maxSize == null) {
            knownMaxWidth = knownMaxHeight = 0;
        } else {
            knownMaxWidth = maxSize.width;
            knownMaxHeight = maxSize.height;
            if (knownMaxWidth == Integer.MAX_VALUE) {
                knownMaxWidth = 0;
            }
            if (knownMaxHeight == Integer.MAX_VALUE) {
                knownMaxHeight = 0;
            }
            if (borderAndIconMargin != null) {
                knownMaxWidth -= borderAndIconMargin.width;
                knownMaxHeight -= borderAndIconMargin.height;
            }
        }
        FontRenderContext frc = getFontRenderContext(component, text);
        if (frc != null) {
            textBounds = font.getStringBounds(text, frc);
        }
        // Step 2 and Step 3: see computeBestFontSizeIgnoreHeight
        fontSizeToUse = computeBestFontSizeIgnoreHeight(component, font, text, -1, -1, textBounds, borderAndIconMargin,
                knownMaxWidth);
        if (fontSizeToUse > -1) {
            int compHeight = component.getHeight();
            if (borderAndIconMargin != null) {
                compHeight -= borderAndIconMargin.height;
            }

            // Step 4: Pick a new font size so it will not be larger than the height of label.
            fontSizeToUse = computeBestFontSizeAccordingHeight(font, frc, text, component, compHeight, textBounds,
                    fontSizeToUse, knownMaxHeight);
            // Check twice whether font size was misestimated.
            if (frc != null) {
                int fWidth, fHeight;
                int compWidth = getMaxAllowedWidth(component, borderAndIconMargin, -1);
                Font tmp;
                for (int i = 0; i < 2; i++) {
                    tmp = deriveFont(font, fontSizeToUse);
                    textBounds = tmp.getStringBounds(text, frc);
                    fWidth = getWidth(textBounds, component, tmp, text);
                    fHeight = FontUtils.getFontHeight(textBounds);
                    if ((fHeight < compHeight) && (fWidth < compWidth)) {
                        fontSizeToUse++;
                    } else {
                        break;
                    }
                }
                tmp = deriveFont(font, fontSizeToUse);
                textBounds = tmp.getStringBounds(text, frc);
                fWidth = getWidth(textBounds, component, tmp, text);
                fHeight = FontUtils.getFontHeight(textBounds);
                if ((fHeight > compHeight) || fWidth > compWidth) {
                    fontSizeToUse--;
                }
            }
        }
        return fontSizeToUse;
    }

    /**
     * Updates a {@link Component} {@link Font} to display the best text size.
     * 
     * @param component The {@link Component}. Must not be <code>null</code>.
     */
    public void updateComponentFont(final Component component) {
        component.removePropertyChangeListener(this);
        if (component.isShowing()) {
            int fontSizeToUse;
            Font font = component.getFont();
            if (component instanceof IBestFontSizeCalculator) {
                // If component is an IBestFontSizeCalculator, it is able to calculate its own best font size.
                fontSizeToUse = ((IBestFontSizeCalculator) component).computeBestFontSize();
            } else {
                // Otherwise, best font size should be calculated
                fontSizeToUse = computeBestFontSize(component, font);
            }
            if ((fontSizeToUse > 0) && (fontSizeToUse != font.getSize())) {
                // Step 5: If font size really changed, apply new font
                component.setFont(deriveFont(font, fontSizeToUse));
                component.repaint();
            }
        }
        component.addPropertyChangeListener(this);
    }

    /**
     * Updates a {@link Component} {@link Font} later (i.e. in EDT) to display the best text size.
     * 
     * @param component The {@link Component}. Must not be <code>null</code>.
     */
    protected void updateComponentFontLater(final Component component) {
        if (component != null) {
            // Here, we try to register runnable in EventQueue only if it is not already registered,
            // and to limit the creation of runnables.
            Runnable runnable = COMPONENT_UPDATERS.get(component);
            if (runnable == null) {
                // Can't use lambda expression due to the need to access this Runnable
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        IN_EDT.remove(this);
                        // Update component font only if it is showing (optimization discovered during JAVAAPI-644).
                        if (component.isShowing()) {
                            updateComponentFont(component);
                        }
                    }
                };
                Runnable tmp = COMPONENT_UPDATERS.putIfAbsent(component, runnable);
                if (tmp != null) {
                    runnable = tmp;
                }
            }
            if (IN_EDT.add(runnable)) {
                SwingUtilities.invokeLater(runnable);
            }
        }
    }

    /**
     * Forgets a {@link JTextComponent} so that it won't be checked/updated when a {@link DocumentEvent} occurs.
     * 
     * @param textComponent The {@link JTextComponent} to forget.
     */
    public void forgetTextComponent(JTextComponent textComponent) {
        if (textComponent != null) {
            Collection<WeakReference<JTextComponent>> toRemove = new ArrayList<>();
            for (WeakReference<JTextComponent> ref : textComponents) {
                JTextComponent tmp = ref.get();
                if ((tmp == null) || (tmp == textComponent)) {
                    toRemove.add(ref);
                }
            }
            textComponents.removeAll(toRemove);
            toRemove.clear();
        }
    }

    /**
     * Remembers a {@link JTextComponent} so that it can be checked/updated when a {@link DocumentEvent} occurs.
     * 
     * @param textComponent The {@link JTextComponent} to remember.
     */
    public void rememberTextComponent(JTextComponent textComponent) {
        forgetTextComponent(textComponent);
        if (textComponent != null) {
            textComponents.add(new WeakReference<>(textComponent));
        }
    }

    /**
     * Utility method used to stop listening to a {@link Component}, so that its font size won't be dynamically
     * changed anymore.
     * <p>
     * Typically, this method calls:
     * <ul>
     * <li>{@link Component#removeComponentListener(ComponentListener)}</li>
     * <li>{@link Component#removePropertyChangeListener(PropertyChangeListener)}</li>
     * <li>{@link Document#removeDocumentListener(DocumentListener)}</li>
     * <li>{@link #forgetTextComponent(JTextComponent)}</li>
     * </ul>
     * </p>
     * 
     * @param component The {@link Component}.
     */
    @Override
    public void stopListeningToComponent(Component component) {
        if (component != null) {
            component.removeComponentListener(this);
            component.removePropertyChangeListener(this);
            component.removeHierarchyBoundsListener(this);
            component.removeHierarchyListener(this);
            if ((component instanceof JTextComponent)) {
                JTextComponent textComponent = (JTextComponent) component;
                textComponent.getDocument().removeDocumentListener(this);
                forgetTextComponent(textComponent);
            }
        }
    }

    /**
     * Utility method used to change a {@link Component}'s font size and to listen to that {@link Component} in
     * order to dynamically change its font size when needed.
     * <p>
     * Typically, this method calls:
     * <ul>
     * <li>{@link #stopListeningToComponent(Component)} <i>(to avoid registering the same listener twice)</i></li>
     * <li>{@link Component#addComponentListener(ComponentListener)}</li>
     * <li>{@link Component#addPropertyChangeListener(PropertyChangeListener)}</li>
     * <li>{@link #rememberTextComponent(JTextComponent)}</li>
     * <li>{@link Document#addDocumentListener(DocumentListener)}</li>
     * <li>{@link #updateComponentFont(Component)}</li>
     * </ul>
     * </p>
     * 
     * @param component The {@link Component}.
     */
    @Override
    public void setupAndListenToComponent(Component component) {
        stopListeningToComponent(component);
        if (component != null) {
            component.addComponentListener(this);
            component.addPropertyChangeListener(this);
            component.addHierarchyBoundsListener(this);
            component.addHierarchyListener(this);
            if (component instanceof JTextComponent) {
                JTextComponent textComponent = (JTextComponent) component;
                rememberTextComponent(textComponent);
                textComponent.getDocument().addDocumentListener(this);
            }
            updateComponentFontLater(component);
        }
    }

    /**
     * Checks known {@link JTextComponent} to recover which one uses given {@link Document}.
     * 
     * @param doc The {@link Document}.
     * @return A {@link JTextComponent}.
     */
    protected JTextComponent getTextComponent(Document doc) {
        JTextComponent textComponent = null;
        if (doc != null) {
            Collection<WeakReference<JTextComponent>> toRemove = new ArrayList<>();
            for (WeakReference<JTextComponent> ref : textComponents) {
                JTextComponent tmp = ref.get();
                if (tmp == null) {
                    toRemove.add(ref);
                } else if (tmp.getDocument() == doc) {
                    textComponent = tmp;
                }
            }
            textComponents.removeAll(toRemove);
            toRemove.clear();
        }
        return textComponent;
    }

    /**
     * Recovers the {@link JTextComponent} for which text changed and tries to update its font size.
     * 
     * @param e The {@link DocumentEvent} that notifies for text change.
     */
    protected void treatDocumentEvent(DocumentEvent e) {
        if (e != null) {
            updateComponentFontLater(getTextComponent(e.getDocument()));
        }
    }

    @Override
    public void componentResized(ComponentEvent e) {
        if (e != null) {
            updateComponentFontLater(e.getComponent());
        }
    }

    @Override
    public void componentMoved(ComponentEvent e) {
        // nothing to do
    }

    @Override
    public void componentShown(ComponentEvent e) {
        // nothing to do
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        // nothing to do
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        treatDocumentEvent(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        treatDocumentEvent(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        treatDocumentEvent(e);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt != null) {
            boolean treatChange = false;
            switch (evt.getPropertyName()) {
                case DOCUMENT:
                    // document changed: stop listening to old one and listen to new one.
                    Document oldDoc = (Document) evt.getOldValue();
                    Document newDoc = (Document) evt.getNewValue();
                    if (oldDoc != null) {
                        oldDoc.removeDocumentListener(this);
                    }
                    if (newDoc != null) {
                        newDoc.removeDocumentListener(this);
                        newDoc.addDocumentListener(this);
                    }
                    // We don't break as we do want to set treatChange to true
                case FONT:
                case BORDER:
                case ANCESTOR:
                case UI:
                case AbstractButton.MARGIN_CHANGED_PROPERTY:
                case AbstractButton.BORDER_PAINTED_CHANGED_PROPERTY:
                case AbstractButton.ICON_CHANGED_PROPERTY:
                case AbstractButton.HORIZONTAL_TEXT_POSITION_CHANGED_PROPERTY:
                case AbstractButton.VERTICAL_TEXT_POSITION_CHANGED_PROPERTY:
                case AbstractButton.TEXT_CHANGED_PROPERTY:
                case AbstractButton.MODEL_CHANGED_PROPERTY:
                    treatChange = true;
                    break;

                default:
                    // nothing to do
                    break;
            }
            if (treatChange) {
                Object src = evt.getSource();
                if (src instanceof Component) {
                    updateComponentFontLater((Component) src);
                }
            }
        }
    }

    @Override
    public void ancestorMoved(HierarchyEvent e) {
        // nothing to do
    }

    @Override
    public void ancestorResized(HierarchyEvent e) {
        if (e != null) {
            updateComponentFontLater(e.getComponent());
        }
    }

    @Override
    public void hierarchyChanged(HierarchyEvent e) {
        if (e != null) {
            updateComponentFontLater(e.getComponent());
        }
    }

}
