/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.ui;

import java.awt.FontMetrics;
import java.awt.Rectangle;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicLabelUI;

/**
 * A {@link BasicLabelUI} that uses a reversed text in order to ensure always displaying the end of the text instead of
 * the beginning when text is too long ("..." will be at the beginning instead of the end).
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ReverseLabelUI extends BasicLabelUI {

    private JComponent parent = null;

    public ReverseLabelUI() {
        super();
    }

    public ReverseLabelUI(JComponent parent) {
        this.parent = parent;
    }

    @Override
    protected String layoutCL(JLabel label, FontMetrics fontMetrics, String text, Icon icon, Rectangle viewR,
            Rectangle iconR, Rectangle textR) {
        // Step 1: reverse text
        String textRevert = new StringBuilder(text).reverse().toString();

        // Step 2: compute best view size
        Rectangle parentRectangle = viewR;
        if (parent != null) {
            int width = parent.getVisibleRect().width - 20;
            parentRectangle.width = width;
        }

        // Step 3: call layoutCompoundLabel with reversed text
        String lCompound = SwingUtilities.layoutCompoundLabel(label, fontMetrics, textRevert, icon,
                label.getVerticalAlignment(), label.getHorizontalAlignment(), label.getVerticalTextPosition(),
                label.getHorizontalTextPosition(), parentRectangle, iconR, textR, label.getIconTextGap());

        // Step 4: Reverse result, so that it is in the right order and the final ... is at the beginning.
        String returnString = new StringBuilder(lCompound).reverse().toString();

        // Step 5: Update tooltip text, so that user may see the original complete text.
        String tooltip = label.getToolTipText();
        if ((tooltip == null) || tooltip.isEmpty()) {
            label.setToolTipText(text);
        }
        return returnString;
    }

}
