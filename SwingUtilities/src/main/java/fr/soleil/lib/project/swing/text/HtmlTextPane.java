/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Desktop;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;

import javax.swing.JTextPane;
import javax.swing.ToolTipManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkEvent.EventType;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A viewer that can be used to display HTML formated text
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class HtmlTextPane extends JTextPane {

    private static final long serialVersionUID = -3159630608935411736L;

    private String tooltipText;

    /**
     * Default constructor
     */
    public HtmlTextPane() {
        super();
        tooltipText = null;
        setEditorKit(new HTMLEditorKit());
        addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                EventType eventType = e.getEventType();
                if (eventType == EventType.ACTIVATED) {
                    try {
                        Desktop.getDesktop().browse(e.getURL().toURI());
                    } catch (Exception ex) {
                        // ignore
                    }
                    updateToolTipText(null);
                } else if (eventType == EventType.ENTERED) {
                    updateToolTipText(e.getURL().toString());
                } else if (eventType == EventType.EXITED) {
                    updateToolTipText(null);
                }
            }
        });
    }

    /**
     * Updates this {@link HtmlTextPane}'s tooltip text
     * 
     * @param tooltiptext The tooltip text to set
     */
    protected void updateToolTipText(String tooltiptext) {
        this.tooltipText = tooltiptext;
        String text = getToolTipText();
        ToolTipManager toolTipManager = ToolTipManager.sharedInstance();
        toolTipManager.unregisterComponent(this);
        if (text != null) {
            toolTipManager.registerComponent(this);
        }
    }

    @Override
    public String getToolTipText() {
        return tooltipText == null ? super.getToolTipText() : tooltipText;
    }

    @Override
    public void setText(String t) {
        setText(t, true);
    }

    /**
     * Sets the text of this {@link HtmlTextPane}. If a comparison is asked,
     * then the text will be set only if it is not the same as current one.
     * 
     * @param t The text to set
     * @param compareWithPrevious
     *            Whether to use text comparison
     */
    protected void setText(String t, boolean compareWithPrevious) {
        String text = t;
        if (text == null) {
            text = ObjectUtils.EMPTY_STRING;
        }
        boolean canSetText = true;
        if (compareWithPrevious) {
            canSetText = !isMyText(text);
        }
        if (canSetText) {
            super.setText(text);
        }
    }

    /**
     * Returns whether a given text represents the same text as previously set one
     * 
     * @param text The text to compare
     * @return A <code>boolean</code> value
     */
    protected boolean isMyText(String text) {
        boolean sameText = false;
        HTMLEditorKit kit = (HTMLEditorKit) getEditorKit();
        Document doc = kit.createDefaultDocument();
        try {
            doc.remove(0, doc.getLength());
            Reader r = new StringReader(text);
            kit.read(r, doc, 0);
            StringWriter writer = new StringWriter();
            kit.write(writer, doc, 0, doc.getLength());
            sameText = ObjectUtils.sameObject(getText(), writer.toString());
        } catch (Exception e) {
            // ignore Exceptions: it is just a test;
        }
        return sameText;
    }

}
