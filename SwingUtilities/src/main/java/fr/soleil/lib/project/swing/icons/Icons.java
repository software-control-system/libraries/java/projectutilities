/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.icons;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import fr.soleil.lib.project.awt.ColorUtils;

/**
 * {@link Icons} is an icon manager. It allows to recover some icons identified by some keys in a
 * property file
 * 
 * @author hardion, Rapha&euml;l GIRARDOT
 */
public class Icons implements SwingConstants {

    private final String bundleName;
    private final ResourceBundle resourceBundle;

    /**
     * Constructs this manager with a given bundle name that identifies the property file
     * 
     * @param bundle_name The bundle name.
     */
    public Icons(String bundle_name) {
        this.bundleName = bundle_name;
        resourceBundle = ResourceBundle.getBundle(bundleName);
    }

    /**
     * Default constructor. Constructs this manager with a default property file.
     * 
     * @see #Icons(String)
     */
    public Icons() {
        this(Icons.class.getPackage().getName());
    }

    protected URL getURL(String key) {
        URL url = null;
        if (key != null) {
            url = Icons.class.getClassLoader().getResource(resourceBundle.getString(key));
        }
        return url;
    }

    /**
     * Recovers an icon from a key
     * 
     * @param key the key that identifies the icon
     * @return The {@link ImageIcon} that corresponds to the given key, or <code>null</code> if
     *         there is no such icon.
     */
    public ImageIcon getIcon(String key) {
        ImageIcon icon = null;
        try {
            URL url = getURL(key);
            if (url != null) {
                icon = new ImageIcon(url);
            }
        } catch (MissingResourceException e) {
            icon = null;
        }
        return icon;
    }

    /**
     * Constructs a {@link DecorableIcon} on the base of 2 keys:
     * <ul>
     * <li>The 1st one identifies the main icon</li>
     * <li>The 2nd one identifies the decoration icon</li>
     * </ul>
     * 
     * @param key The key that identifies the main icon
     * @param modifier The key that identifies the decoration icon
     * @return The expected {@link DecorableIcon}, or <code>null</code> if there was no way
     *         constructing such an icon (because at least 1 of the 2 keys can't be associated with
     *         an icon)
     */
    public DecorableIcon getDecorableIcon(String key, String modifier) {
        DecorableIcon icon = null;
        try {
            URL url = getURL(key);
            if (url != null) {
                icon = new DecorableIcon(url);
                icon.setDecoration(getIcon(modifier));
                icon.setDecorated(true);
            }
        } catch (MissingResourceException e) {
            icon = null;
        }
        return icon;
    }

    /**
     * Creates a {@link Cursor} that will represent a given {@link ImageIcon}
     * 
     * @param icon The {@link Cursor} {@link ImageIcon}
     * @param name The {@link Cursor} name
     * @return A {@link Cursor}
     */
    public static Cursor generateCursorFromIcon(ImageIcon icon, String name) {
        return generateCursorFromIcon(icon, name, Color.BLACK);
    }

    /**
     * Creates a {@link Cursor} that will represent a given {@link ImageIcon}
     * 
     * @param icon The {@link Cursor} {@link ImageIcon}
     * @param name The {@link Cursor} name
     * @param arrowColor The {@link Color} used to draw an additional arrow, indicating user click position.
     *            <code>null</code> if you don't want such an arrow.
     * @return A {@link Cursor}
     */
    public static Cursor generateCursorFromIcon(ImageIcon icon, String name, Color arrowColor) {
        return generateCursorFromIcon(icon, name, arrowColor, LEFT, TOP);
    }

    /**
     * Creates a {@link Cursor} that will represent a given {@link ImageIcon}
     * 
     * @param icon The {@link Cursor} {@link ImageIcon}
     * @param name The {@link Cursor} name
     * @param arrowColor The {@link Color} used to draw an additional arrow, indicating user click position.
     *            <code>null</code> if you don't want such an arrow.
     * @param hAlign Horizontal alignment of the click point of the generated {@link Cursor}. Can be any of these:
     *            <ol>
     *            <li>{@link SwingConstants#LEFT}</li>
     *            <li>{@link SwingConstants#RIGHT}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            </ol>
     * @param vAlign Vertical alignment of the click point of the generated {@link Cursor}. Can be any of these:
     *            <ol>
     *            <li>{@link SwingConstants#TOP}</li>
     *            <li>{@link SwingConstants#BOTTOM}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            </ol>
     * @return A {@link Cursor}
     */
    public static Cursor generateCursorFromIcon(ImageIcon icon, String name, Color arrowColor, int hAlign, int vAlign) {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        // size: dimension of the cursor drawing zone
        Dimension size = toolkit.getBestCursorSize(0, 0);
        // XXX linux bug (CONTROLGUI-168)
        if ((size == null) || ((size.width < 5) && (size.height < 5))) {
            size = new Dimension(20, 20);
        }
        int width = icon.getIconWidth(), height = icon.getIconHeight();
        // px, py: x and y coordinates of the click point
        // ix, iy: x and y coordinates of the image in the cursor drawing zone
        int px, py, ix, iy;
        int hw = getHalf(size.width), hh = getHalf(size.height);
        int transparentX = -1, transparentY = -1;
        int[][] arrow;
        switch (hAlign) {
            case CENTER:
                ix = getPositionToAlignCenter(width, size.width);
                px = hw;
                switch (vAlign) {
                    case CENTER:
                        // center center case
                        iy = getPositionToAlignCenter(height, size.height);
                        py = hh;
                        arrow = new int[][] { { px - 1, py }, { px - 2, py }, { px + 1, py }, { px + 2, py },
                                { px, py - 1 }, { px, py - 2 }, { px, py + 1 }, { px, py + 2 } };
                        transparentX = px;
                        transparentY = py;
                        break;
                    case BOTTOM:
                        // bottom center case
                        iy = Math.max(0, size.height - height - 4);
                        py = size.height - 1;
                        arrow = new int[][] { { px, py }, { px - 1, py - 1 }, { px, py - 1 }, { px + 1, py - 1 },
                                { px - 2, py - 2 }, { px - 1, py - 2 }, { px, py - 2 }, { px + 1, py - 2 },
                                { px + 2, py - 2 } };
                        break;
                    default:
                        // top center case
                        iy = Math.max(0, size.height - height);
                        iy = Math.min(4, iy);
                        py = 0;
                        arrow = new int[][] { { px, 0 }, { px - 1, 1 }, { px, 1 }, { px + 1, 1 }, { px - 2, 2 },
                                { px - 1, 2 }, { px, 2 }, { px + 1, 2 }, { px + 2, 2 } };
                        break;
                }
                break;
            case RIGHT:
                ix = Math.max(0, size.width - width - 4);
                px = size.width - 1;
                switch (vAlign) {
                    case CENTER:
                        // center right case
                        iy = getPositionToAlignCenter(height, size.height);
                        py = hh;
                        arrow = new int[][] { { px, py }, { px - 1, py - 1 }, { px - 1, py }, { px - 1, py + 1 },
                                { px - 2, py - 2 }, { px - 2, py - 1 }, { px - 2, py }, { px - 2, py + 1 },
                                { px - 2, py + 2 } };
                        break;
                    case BOTTOM:
                        // bottom right case
                        iy = Math.max(0, size.height - height - 4);
                        py = size.height - 1;
                        arrow = new int[][] { { px, py }, { px - 1, py }, { px - 2, py }, { px, py - 1 },
                                { px - 1, py - 1 }, { px, py - 2 } };
                        break;
                    default:
                        // top right case
                        iy = Math.max(0, size.height - height);
                        iy = Math.min(4, iy);
                        py = 0;
                        arrow = new int[][] { { px, 0 }, { px - 1, 0 }, { px - 2, 0 }, { px, 1 }, { px - 1, 1 },
                                { px, 2 } };
                        break;
                }
                break;
            default:
                ix = Math.max(0, size.width - width);
                ix = Math.min(4, ix);
                px = 0;
                switch (vAlign) {
                    case CENTER:
                        // center left case
                        iy = getPositionToAlignCenter(height, size.height);
                        py = hh;
                        arrow = new int[][] { { 0, py }, { 1, py - 1 }, { 1, py }, { 1, py + 1 }, { 2, py - 2 },
                                { 2, py - 1 }, { 2, py }, { 2, py + 1 }, { 2, py + 2 } };
                        break;
                    case BOTTOM:
                        // bottom left case
                        iy = Math.max(0, size.height - height - 4);
                        py = size.height - 1;
                        arrow = new int[][] { { 0, py }, { 1, py }, { 2, py }, { 0, py - 1 }, { 1, py - 1 },
                                { 0, py - 2 } };
                        break;
                    default:
                        // top left case
                        iy = Math.max(0, size.height - height);
                        iy = Math.min(4, iy);
                        py = 0;
                        arrow = new int[][] { { 0, 0 }, { 0, 1 }, { 0, 2 }, { 1, 0 }, { 1, 1 }, { 2, 0 } };
                        break;
                }
                break;
        }
        BufferedImage image = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_ARGB);
        icon.paintIcon(null, image.getGraphics(), ix, iy);
        if (arrowColor != null) {
            for (int[] pos : arrow) {
                image.setRGB(pos[0], pos[1], arrowColor.getRGB());
            }
            if ((transparentX > -1) && (transparentY > -1)) {
                // in the center-center case, the arrow is a cross, with a hole in its center
                image.setRGB(transparentX, transparentY,
                        ColorUtils.toRGBA(arrowColor.getRed(), arrowColor.getGreen(), arrowColor.getBlue(), 0));
            }
        }
        return toolkit.createCustomCursor(image, new Point(px, py), name);
    }

    protected static int getHalf(int value) {
        int half = value / 2;
        if (value % 2 != 0) {
            half++;
        }
        return half;
    }

    protected static int getPositionToAlignCenter(int s1, int s2) {
        int h1 = getHalf(s1), h2 = getHalf(s2);
        return h2 - h1;
    }

}
