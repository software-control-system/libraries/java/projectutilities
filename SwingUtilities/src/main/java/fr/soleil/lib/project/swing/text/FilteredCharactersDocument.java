/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.util.Arrays;

/**
 * An {@link AFilteredDocument} that may accept only some characters, or refuse these characters.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class FilteredCharactersDocument extends AFilteredDocument {

    private static final long serialVersionUID = 1012512913754903130L;

    private static final char[] EMPTY = new char[0];

    private char[] filteredCharacters;
    private boolean escaped;

    public FilteredCharactersDocument() {
        super();
    }

    /**
     * Returns whether a character belongs to a list of filtered characters.
     * 
     * @param character The character.
     * @param filteredCharacters The list of filtered characters, represented by an ellipse.
     * @return A <code>boolean</code>. <code>true</code>if the character belongs to the list, <code>false</code>
     *         otherwise.
     */
    protected boolean isFiltered(char character, char... filteredCharacters) {
        boolean filtered = false;
        for (char c : filteredCharacters) {
            if (c == character) {
                filtered = true;
                break;
            }
        }
        return filtered;
    }

    /**
     * Returns the characters filtered by this {@link FilteredCharactersDocument}.
     * 
     * @return A <code>char</code> array, never <code>null</code>.
     */
    public char[] getFilteredCharacters() {
        return filteredCharacters.clone();
    }

    /**
     * Sets the characters filtered by this {@link FilteredCharactersDocument}.
     * 
     * @param filteredCharacters The characters that should be filtered by this {@link FilteredCharactersDocument}.
     */
    public void setFilteredCharacters(char... filteredCharacters) {
        this.filteredCharacters = filteredCharacters == null ? EMPTY : filteredCharacters.clone();
    }

    /**
     * Adds some characters in the list of filtered characters.
     * 
     * @param characters The characters to add.
     * @see #getFilteredCharacters()
     * @see #setFilteredCharacters(char...)
     */
    public void addFilteredCharacters(char... characters) {
        if ((characters != null) && (characters.length > 0)) {
            char[] filteredCharacters = this.filteredCharacters;
            int count = 0, index = 0;
            boolean[] allowed = new boolean[characters.length];
            boolean valid;
            for (char character : characters) {
                valid = !isFiltered(character, filteredCharacters);
                allowed[index++] = valid;
                if (valid) {
                    count++;
                }
            }
            if (count > 0) {
                char[] result = Arrays.copyOf(filteredCharacters, filteredCharacters.length + count);
                index = 0;
                for (int i = 0; i < characters.length; i++) {
                    if (allowed[i]) {
                        result[filteredCharacters.length + index++] = characters[i];
                    }
                }
                this.filteredCharacters = result;
            }
        }
    }

    /**
     * Returns whether the filtered characters are characters that should be escaped or whether these characters are the
     * only accepted ones.
     * 
     * @return A <code>boolean</code>:
     *         <ul>
     *         <li><code>true</code>: The filtered characters are characters that should be avoided in text.</li>
     *         <li><code>false</code>: The filtered characters are the only ones that should be accepted in text.</li>
     *         </ul>
     * @see #setEscaped(boolean)
     * @see #getFilteredCharacters()
     * @see #setFilteredCharacters(char...)
     * @see #addFilteredCharacters(char...)
     */
    public boolean isEscaped() {
        return escaped;
    }

    /**
     * Sets whether the filtered characters are characters that should be escaped or whether these characters are the
     * only accepted ones.
     * 
     * @param escaped Whether the filtered characters are characters that should be escaped or whether these characters
     *            are the only accepted ones.
     *            <ul>
     *            <li><code>true</code>: The filtered characters are characters that should be avoided in text.</li>
     *            <li><code>false</code>: The filtered characters are the only ones that should be accepted in
     *            text.</li>
     *            </ul>
     * @see #isEscaped()
     * @see #getFilteredCharacters()
     * @see #setFilteredCharacters(char...)
     * @see #addFilteredCharacters(char...)
     */
    public void setEscaped(boolean escaped) {
        this.escaped = escaped;
    }

    @Override
    protected StringBuilder buildFilteredValue(String str, int offs) {
        char c;
        char[] filteredCharacters = this.filteredCharacters;
        boolean escaped = this.escaped;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            c = str.charAt(i);
            if (isFiltered(c, filteredCharacters)) {
                if (!escaped) {
                    sb.append(c);
                }
            } else if (escaped) {
                sb.append(c);
            }
        }
        return sb;
    }

}
