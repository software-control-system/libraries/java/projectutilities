/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.panel;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import fr.soleil.lib.project.swing.JSmoothProgressBar;

/**
 * An {@link ASplashPanel} using {@link JSmoothProgressBar}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class SplashPanel extends ASplashPanel<JSmoothProgressBar> {

    private static final long serialVersionUID = 6317327883185800051L;

    // Panel components
    protected JLabel message;

    // Splah panel constructor
    public SplashPanel() {
        this(null, null, null);
    }

    public SplashPanel(ImageIcon icon, Color textForeground, JSmoothProgressBar bar) {
        super(icon, textForeground, bar);
    }

    @Override
    protected JSmoothProgressBar generateProgressBar() {
        JSmoothProgressBar progress = new JSmoothProgressBar();
        progress.setStringPainted(true);
        return progress;
    }

    @Override
    protected void initComponents() {
        super.initComponents();
        setDoubleBuffered(false);
        message = generateLabel(INITIALIZING, MESSAGE_FONT);
    }

    @Override
    protected void layoutComponents() {
        setLayout(null);
        add(title);
        add(copyright);
        add(progressBar);
        add(message);
        title.setBounds(5, imgSize.height - 95, imgSize.width - 10, 23);
        copyright.setBounds(5, imgSize.height - 75, imgSize.width - 10, 15);
        progressBar.setBounds(5, imgSize.height - 55, imgSize.width - 10, 21);
        message.setBounds(5, imgSize.height - 30, imgSize.width - 10, 18);
    }

    public void setProgress(int p) {
        if (progressBar.getValue() != p) {
            progressBar.setValue(p);
            if (isShowing()) {
                repaint();
            }
        }
    }

    public JSmoothProgressBar getProgress() {
        return progressBar;
    }

    public void setMessage(String s) {
        message.setText(s);
        if (isShowing()) {
            repaint();
        }
    }

    public String getMessage() {
        return message.getText();
    }

    @Override
    public void setCopyright(String s) {
        copyright.setText(s);
        if (isShowing()) {
            repaint();
        }
    }

    @Override
    public String getCopyright() {
        return copyright.getText();
    }

}
