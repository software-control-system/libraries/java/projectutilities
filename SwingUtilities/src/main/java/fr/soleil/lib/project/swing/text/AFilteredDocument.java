/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * A {@link PlainDocument} that filter received strings.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AFilteredDocument extends PlainDocument {

    private static final long serialVersionUID = -1301444256024766504L;

    // This allows to temporarily deactivate the validation, useful for setting text that doesn't match
    private boolean enabled = true;

    protected AFilteredDocument() {
        super();
    }

    /**
     * Builds a {@link StringBuilder} containing the filtered string value, from given {@link String} at given starting
     * offset in this document.
     * 
     * @param str The {@link String}.
     * @param offs The starting offset &ge; 0.
     * @return A {@link StringBuilder}, never <code>null</code>.
     */
    protected abstract StringBuilder buildFilteredValue(String str, int offs);

    /**
     * Returns whether text validation is turned on.
     * 
     * @return true if the input validation is turned on.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Enables or disables the validation. This is useful for setting text that would not match.
     * 
     * @param enabled true to enable validation, false otherwise.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public final void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
        if (enabled) {
            StringBuilder sb = buildFilteredValue(str, offs);
            if (sb.length() > 0) {
                super.insertString(offs, sb.toString(), a);
                sb.delete(0, sb.length());
            }
        } else {
            super.insertString(offs, str, a);
        }
    }

}
