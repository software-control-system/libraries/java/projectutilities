/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * A {@link JLabel} used to represent the fact that there is a problem. This {@link JLabel} draws a cross over or under
 * the text and icon
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ErrorLabel extends JLabel {

    private static final long serialVersionUID = 2731233558890042671L;

    /**
     * Default error cross {@link Color}
     */
    public static final Color DEFAULT_ERROR_COLOR = Color.RED;

    /**
     * The error cross color
     */
    protected Color errorColor = DEFAULT_ERROR_COLOR;
    /**
     * Whether error cross is drawn over text and icon
     */
    protected boolean drawCrossOverText = false;

    public ErrorLabel() {
        super();
    }

    public ErrorLabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
    }

    public ErrorLabel(Icon image) {
        super(image);
    }

    public ErrorLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
    }

    public ErrorLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
    }

    public ErrorLabel(String text) {
        super(text);
    }

    /**
     * Returns the error cross {@link Color}
     * 
     * @return A {@link Color}
     */
    public Color getErrorColor() {
        return errorColor;
    }

    /**
     * Sets the error cross {@link Color}
     * 
     * @param errorColor The {@link Color} to set. If <code>null</code>, {@link #DEFAULT_ERROR_COLOR} is used
     */
    public void setErrorColor(Color errorColor) {
        if (errorColor == null) {
            this.errorColor = DEFAULT_ERROR_COLOR;
        } else {
            this.errorColor = errorColor;
        }
        repaint();
    }

    /**
     * Returns whether error cross is drawn over text and icon
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isDrawCrossOverText() {
        return drawCrossOverText;
    }

    /**
     * Sets whether error cross should be drawn over text and icon
     * 
     * @param drawCrossOverText Whether error cross should be drawn over text and icon
     */
    public void setDrawCrossOverText(boolean drawCrossOverText) {
        this.drawCrossOverText = drawCrossOverText;
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        if (!drawCrossOverText) {
            drawCross(g);
        }
        super.paintComponent(g);
        if (drawCrossOverText) {
            drawCross(g);
        }
    }

    /**
     * Draws the error cross
     * 
     * @param g The {@link Graphics} in which to draw the cross
     */
    protected void drawCross(Graphics g) {
        if (g != null) {
            Color formerColor = g.getColor();
            g.setColor(errorColor);
            int width = getWidth(), height = getHeight();
            g.drawLine(0, 0, width, height);
            g.drawLine(0, height, width, 0);
            if (formerColor != null) {
                g.setColor(formerColor);
            }
        }
    }

    public static void main(String[] args) {
        JFrame testFrame = new JFrame(ErrorLabel.class.getSimpleName() + " test");
        ErrorLabel errorLabel = new ErrorLabel("There is an error!");
        errorLabel.setDrawCrossOverText(true);
        errorLabel.setFont(new Font(Font.DIALOG, Font.BOLD, 14));
        testFrame.setContentPane(errorLabel);
        testFrame.pack();
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }

}
