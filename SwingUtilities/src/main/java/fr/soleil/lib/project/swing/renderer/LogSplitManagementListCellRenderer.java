/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.renderer;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.soleil.lib.project.swing.SwingUtilitiesConstants;
import fr.soleil.lib.project.swing.panel.ALogViewer.SplitManagement;

/**
 * A {@link LightSelectionBackgroundListCellRenderer} dedicated in rendering {@link SplitManagement} values.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LogSplitManagementListCellRenderer extends LightSelectionBackgroundListCellRenderer
        implements SwingUtilitiesConstants {

    private static final long serialVersionUID = -4982130733280759299L;

    protected static final String TOP_BOTTOM_TEXT = "fr.soleil.lib.project.swing.log.split.top-bottom.text";
    protected static final String TOP_BOTTOM_TOOLTIP = "fr.soleil.lib.project.swing.log.split.top-bottom.tooltip";
    protected static final String BOTTOM_TOP_TEXT = "fr.soleil.lib.project.swing.log.split.bottom-top.text";
    protected static final String BOTTOM_TOP_TOOLTIP = "fr.soleil.lib.project.swing.log.split.bottom-top.tooltip";
    protected static final String LEFT_RIGHT_TEXT = "fr.soleil.lib.project.swing.log.split.left-right.text";
    protected static final String LEFT_RIGHT_TOOLTIP = "fr.soleil.lib.project.swing.log.split.left-right.tooltip";
    protected static final String RIGHT_LEFT_TEXT = "fr.soleil.lib.project.swing.log.split.right-left.text";
    protected static final String RIGHT_LEFT_TOOLTIP = "fr.soleil.lib.project.swing.log.split.right-left.tooltip";

    public LogSplitManagementListCellRenderer(JComponent componentToRender) {
        super(componentToRender);
    }

    @Override
    protected void doApplyToComponent(JComponent comp, Font font, Color foreground, String tooltipText,
            JList<? extends Object> list, Object value, int index, boolean isSelected, boolean cellHasFocus,
            boolean derive) {
        super.doApplyToComponent(comp, font, foreground, tooltipText, list, value, index, isSelected, cellHasFocus,
                derive);
        if (comp instanceof JLabel) {
            JLabel label = (JLabel) comp;
            String textKey, tooltipKey;
            if (value instanceof SplitManagement) {
                SplitManagement management = (SplitManagement) value;
                switch (management) {
                    case TOP_BOTTOM:
                        textKey = TOP_BOTTOM_TEXT;
                        tooltipKey = TOP_BOTTOM_TOOLTIP;
                        break;
                    case BOTTOM_TOP:
                        textKey = BOTTOM_TOP_TEXT;
                        tooltipKey = BOTTOM_TOP_TOOLTIP;
                        break;
                    case LEFT_RIGHT:
                        textKey = LEFT_RIGHT_TEXT;
                        tooltipKey = LEFT_RIGHT_TOOLTIP;
                        break;
                    case RIGHT_LEFT:
                        textKey = RIGHT_LEFT_TEXT;
                        tooltipKey = RIGHT_LEFT_TOOLTIP;
                        break;
                    default:
                        textKey = null;
                        tooltipKey = null;
                        break;
                }
                if (textKey != null) {
                    label.setText(DEFAULT_MESSAGE_MANAGER.getMessage(textKey));
                }
                if (tooltipKey != null) {
                    label.setToolTipText(DEFAULT_MESSAGE_MANAGER.getMessage(tooltipKey));
                }
            }
        }
    }
}
