/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text.scroll;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;

import javax.swing.SwingUtilities;

import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.text.IEditableTextComponent;

/**
 * A class able to manage an {@link IAutoScrolledTextComponent}'s text position and the way to scroll that text.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ScrollManager implements ComponentListener, FocusListener, PropertyChangeListener {

    // Thread names
    /**
     * Scroll thread name
     */
    protected static final String SCROLL_THREAD_NAME = "Scroll Thread";
    /**
     * Property change reaction thread name
     */
    protected static final String UPDATE_LATER_THREAD_NAME = "Update scroll later";

    /**
     * Default sleeping period on property change
     */
    protected static final int DEFAULT_THREAD_SLEEPING_PERIOD = 150;

    // properties
    protected static final String TEXT = "text";
    protected static final String BORDER = "border";
    protected static final String MARGIN = "margin";
    protected static final String FONT = "font";
    protected static final String ICON = "icon";
    protected static final String ICON_TEXT_GAP = "iconTextGap";

    /**
     * The {@link WeakReference} that stores the {@link IAutoScrolledTextComponent}.
     */
    protected final WeakReference<IAutoScrolledTextComponent> componentRef;
    /**
     * The current text position.
     */
    protected volatile int currentPosition;
    /**
     * Whether text should go to the right instead of to the left.
     */
    protected volatile boolean reversed;
    /**
     * The text scrolling thread.
     */
    protected ScrollThread scrollThread;

    /**
     * Creates a new {@link ScrollManager} for given {@link IAutoScrolledTextComponent}.
     * 
     * @param component The {@link IAutoScrolledTextComponent}.
     * @param autoSetup Whether {@link #setup()} should be called immediately.
     */
    public ScrollManager(IAutoScrolledTextComponent component, boolean autoSetup) {
        this.componentRef = component == null ? null : new WeakReference<IAutoScrolledTextComponent>(component);
        currentPosition = 0;
        reversed = false;
        scrollThread = null;
        if (autoSetup) {
            setup();
        }
    }

    /**
     * Creates a new {@link ScrollManager} for given {@link IAutoScrolledTextComponent}.
     * 
     * @param component The {@link IAutoScrolledTextComponent}.
     */
    public ScrollManager(IAutoScrolledTextComponent component) {
        this(component, true);
    }

    /**
     * Configures this {@link ScrollManager} according to known {@link IAutoScrolledTextComponent}, registering the
     * necessary listeners.
     */
    public void setup() {
        IAutoScrolledTextComponent component = ObjectUtils.recoverObject(componentRef);
        if (component != null) {
            component.addComponentListener(this);
            component.addPropertyChangeListener(this);
            if (component instanceof IEditableTextComponent) {
                component.addFocusListener(this);
            }
        }
    }

    /**
     * Undo what was done in {@link #setup()}.
     * <p>
     * Warning! does not cancel auto scroll. You should call {@link #cancelAutoScroll()} for that.
     * </p>
     */
    public void unconfigure() {
        IAutoScrolledTextComponent component = ObjectUtils.recoverObject(componentRef);
        if (component != null) {
            component.removeComponentListener(this);
            component.removePropertyChangeListener(this);
            if (component instanceof IEditableTextComponent) {
                component.removeFocusListener(this);
            }
        }
    }

    /**
     * Returns whether scrolling is allowed according to given {@link IAutoScrolledTextComponent}.
     * 
     * @param component The {@link IAutoScrolledTextComponent}.
     * @return A <code>boolean</code>.
     */
    protected boolean isScrollingAllowed(IAutoScrolledTextComponent component) {
        boolean scrollingAllowed;
        if (component == null) {
            // Can't scroll if component is null.
            scrollingAllowed = false;
        } else if ((component.getScrollStepsTime() <= 0)
                || ((component instanceof IEditableTextComponent) && component.hasFocus() && component.isEnabled()
                        && ((IEditableTextComponent) component).isEditable())) {
            // Can't scroll if the text is being edited or if scroll steps time is invalid.
            scrollingAllowed = false;
        } else {
            int displayableWidth = component.getDisplayableWidth();
            if (displayableWidth <= 0) {
                // Can't scroll if displayable width is invalid.
                scrollingAllowed = false;
            } else if (component.getTextWidth(component.getFontRenderContext()) <= displayableWidth) {
                // No need to scroll is there is enough space available.
                scrollingAllowed = false;
            } else {
                scrollingAllowed = true;
            }
        }
        return scrollingAllowed;
    }

    /**
     * Returns whether scrolling is allowed, according to known {@link IAutoScrolledTextComponent}.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isScrollingAllowed() {
        return isScrollingAllowed(ObjectUtils.recoverObject(componentRef));
    }

    /**
     * Returns the horizontal position at which the text should be displayed.
     * 
     * @return An <code>int</code>.
     */
    public int getCurrentPosition() {
        return currentPosition;
    }

    /**
     * Cancels the scroll thread.
     */
    public void cancelAutoScroll() {
        ScrollThread scrollThread = this.scrollThread;
        if (scrollThread != null) {
            scrollThread.setCanceled(true);
        }
        this.scrollThread = null;

        currentPosition = 0;
        reversed = false;
    }

    /**
     * Restarts the scroll thread, if necessary, resetting text position and
     * scroll direction.
     */
    public void restartAutoScrollIfPossible() {
        // Stop scroll thread
        cancelAutoScroll();
        IAutoScrolledTextComponent component = ObjectUtils.recoverObject(componentRef);
        // Restart scroll thread if scrolling is needed
        if (isScrollingAllowed(component)) {
            ScrollThread scrollThread = new ScrollThread();
            this.scrollThread = scrollThread;
            scrollThread.start();
        } else if (component != null) {
            component.repaint();
        }
    }

    /**
     * Returns whether given {@link FocusEvent} should impact scrolling.
     * 
     * @param e The {@link FocusEvent}.
     * @return A <code>boolean</code>.
     */
    protected boolean isScrollFocusEvent(FocusEvent e) {
        IAutoScrolledTextComponent component = ObjectUtils.recoverObject(componentRef);
        return (component instanceof IEditableTextComponent) && ((IEditableTextComponent) component).isEditable()
                && (e != null) && (e.getSource() == component) && component.isEnabled();
    }

    /**
     * Common way to treat a {@link FocusEvent}.
     * <p>
     * Typically, this method is called by {@link #focusGained(FocusEvent)} and {@link #focusLost(FocusEvent)}.
     * </p>
     * 
     * @param e The {@link FocusEvent}.
     */
    protected void treatFocusEvent(FocusEvent e) {
        if (isScrollFocusEvent(e)) {
            restartAutoScrollIfPossible();
        }
    }

    /**
     * Returns the time to wait in milliseconds before trying to restart auto scroll on property change.
     * 
     * @return An <code>int</code>.
     */
    protected int getPropertiesWaitingTime() {
        return DEFAULT_THREAD_SLEEPING_PERIOD;
    }

    @Override
    public void focusGained(FocusEvent e) {
        treatFocusEvent(e);
    }

    @Override
    public void focusLost(FocusEvent e) {
        treatFocusEvent(e);
    }

    @Override
    public void componentResized(ComponentEvent e) {
        IAutoScrolledTextComponent component = ObjectUtils.recoverObject(componentRef);
        if ((component != null) && (e != null) && (e.getSource() == component)) {
            restartAutoScrollIfPossible();
        }
    }

    @Override
    public void componentMoved(ComponentEvent e) {
        // Nothing to do
    }

    @Override
    public void componentShown(ComponentEvent e) {
        // Nothing to do
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        // Nothing to do
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        IAutoScrolledTextComponent component = ObjectUtils.recoverObject(componentRef);
        if ((component != null) && (e != null) && (e.getSource() == component)) {
            switch (e.getPropertyName()) {
                case TEXT:
                case BORDER:
                case MARGIN:
                case FONT:
                case ICON:
                case ICON_TEXT_GAP:
                    new RestartLaterThread(getPropertiesWaitingTime()).start();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        unconfigure();
        cancelAutoScroll();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * This is the {@link Thread} that calculates the text positions when scrolling is activated.
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class ScrollThread extends Thread implements ICancelable {

        protected volatile boolean canceled;
        protected volatile int sleepTime;

        public ScrollThread() {
            super(SCROLL_THREAD_NAME);
            IAutoScrolledTextComponent component = ObjectUtils.recoverObject(componentRef);
            if (component == null) {
                canceled = true;
                sleepTime = 0;
            } else {
                canceled = false;
                sleepTime = component.getScrollStepsTime();
            }
        }

        @Override
        public void run() {
            while (!canceled) {
                IAutoScrolledTextComponent component = ObjectUtils.recoverObject(componentRef);
                if (component == null) {
                    canceled = true;
                } else {
                    try {
                        component.repaint();
                        if (sleepTime > 0) {
                            sleep(sleepTime);
                        }
                        sleepTime = component.getScrollStepsTime();
                        if (reversed) {
                            // reversed case: text goes to the right
                            currentPosition++;
                            switch (component.getScrollMode()) {
                                case PENDULAR:
                                    // left limit of the text is visible: text can go to the left.
                                    if (currentPosition >= 0) {
                                        currentPosition = 0;
                                        reversed = false;
                                        sleepTime = component.getReachEndWaitingTime();
                                    }
                                    break;
                                case REACH_END:
                                    // right limit of the text is visible: go back to initial position.
                                    if (currentPosition <= 0) {
                                        currentPosition = 0;
                                        reversed = false;
                                        sleepTime = component.getReachEndWaitingTime();
                                    }
                                    break;
                                case LOOP:
                                    // nothing to do: loop mode is never reversed
                                    break;
                            }
                        } else {
                            // classic case: text goes to the left
                            currentPosition--;
                            int textWidth = component.getTextWidth(component.getFontRenderContext());
                            int myWidth = component.getDisplayableWidth();
                            switch (component.getScrollMode()) {
                                case LOOP:
                                    // text is fully hidden on the left: go to the extreme right
                                    if (currentPosition <= -textWidth) {
                                        currentPosition = myWidth;
                                    }
                                    break;
                                case PENDULAR:
                                case REACH_END:
                                    // right limit of the text is visible: text can go to the right.
                                    if (currentPosition <= (myWidth - textWidth)) {
                                        reversed = true;
                                        sleepTime = component.getReachEndWaitingTime();
                                    }
                                    break;
                            }
                        }
                        component.repaint();
                    } catch (InterruptedException ie) {
                        canceled = true;
                    }
                } // end if (component == null) ... else
            } // end while (!canceled)
        }

        @Override
        public boolean isCanceled() {
            return canceled;
        }

        @Override
        public void setCanceled(boolean canceled) {
            this.canceled = canceled;
            if (canceled) {
                try {
                    interrupt();
                } catch (SecurityException e) {
                    // Ignore: the "canceled" boolean will do the job
                }
            }
        }
    }

    /**
     * A Thread that will wait a little bit before trying to restart auto scroll if possible.
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class RestartLaterThread extends Thread {
        protected int sleepingPeriod;

        public RestartLaterThread(int sleepingPeriod) {
            super(UPDATE_LATER_THREAD_NAME);
            this.sleepingPeriod = sleepingPeriod < 1 ? DEFAULT_THREAD_SLEEPING_PERIOD : sleepingPeriod;
        }

        @Override
        public void run() {
            try {
                // Sleep for 150ms because event may have arrived too soon.
                sleep(150);
                // Invoke later to ensure all graphical actions are done.
                SwingUtilities.invokeLater(() -> {
                    restartAutoScrollIfPossible();
                });
            } catch (InterruptedException e) {
                // Nothing to do. Thread is interrupted.
            }
        }
    }

}
