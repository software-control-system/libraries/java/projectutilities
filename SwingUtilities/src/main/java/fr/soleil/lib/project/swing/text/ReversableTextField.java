/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import javax.swing.text.Document;

import fr.soleil.lib.project.resource.MessageManager;

/**
 * A {@link StateFocusTextField} that can put back its previous string value
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ReversableTextField extends StateFocusTextField implements IReversableField {

    private static final long serialVersionUID = -7184796725575792144L;

    private String previousValue = null;

    public ReversableTextField(MessageManager messageManager) {
        super(messageManager);
    }

    public ReversableTextField(Document doc, String text, int columns, MessageManager messageManager) {
        super(doc, text, columns, messageManager);
    }

    public ReversableTextField(int columns, MessageManager messageManager) {
        super(columns, messageManager);
    }

    public ReversableTextField(String text, int columns, MessageManager messageManager) {
        super(text, columns, messageManager);
    }

    public ReversableTextField(String text, MessageManager messageManager) {
        super(text, messageManager);
    }

    @Override
    public synchronized void setValidValue(String t) {
        previousValue = t;
        super.setValidValue(t);
    }

    /**
     * Puts its previous number value
     */
    @Override
    public void cancel() {
        if ((previousValue == null) || (previousValue.trim().isEmpty())) {
            clear();
        } else {
            setValidValue(previousValue);
        }
    }

}
