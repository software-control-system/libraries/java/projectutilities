/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.Icon;
import javax.swing.JLabel;

/**
 * A {@link JLabel} with anti-aliased text
 */
public class JSmoothLabel extends JLabel implements IAntiAliasedTextComponent {

    private static final long serialVersionUID = -4513622413833247508L;

    protected AntiAliasingDelegate antiAliasingDelegate;

    public JSmoothLabel() {
        super();
        init();
    }

    public JSmoothLabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
        init();
    }

    public JSmoothLabel(Icon image) {
        super(image);
        init();
    }

    public JSmoothLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
        init();
    }

    public JSmoothLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
        init();
    }

    public JSmoothLabel(String text) {
        super(text);
        init();
    }

    protected AntiAliasingDelegate generateAntiAliasingDelegate() {
        return new AntiAliasingDelegate(true, this);
    }

    protected void init() {
        antiAliasingDelegate = generateAntiAliasingDelegate();
    }

    @Override
    public boolean isAntiAliasingEnabled() {
        return antiAliasingDelegate.isAntiAliasingEnabled();
    }

    @Override
    public void setAntiAliasingEnabled(boolean antiAliasingEnabled) {
        antiAliasingDelegate.setAntiAliasingEnabled(antiAliasingEnabled);
    }

    @Override
    public void setupGraphics(Graphics2D g) {
        antiAliasingDelegate.setupGraphics(g);
    }

    @Override
    public void paint(Graphics g) {
        if (g instanceof Graphics2D) {
            setupGraphics((Graphics2D) g);
        }
        super.paint(g);
    }

    @Override
    protected void finalize() {
        antiAliasingDelegate = null;
    }
}
