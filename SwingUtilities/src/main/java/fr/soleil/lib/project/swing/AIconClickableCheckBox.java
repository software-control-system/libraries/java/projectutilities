/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.ItemSelectable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.lib.project.ObjectUtils;

/**
 * An abstract class for a {@link JCheckBox} that is only clickable in the icon zone
 *
 * @param <C> The type of {@link JCheckBox} managed by this class.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AIconClickableCheckBox<C extends JCheckBox> extends JPanel
        implements ItemSelectable, ActionListener, ItemListener {

    private static final long serialVersionUID = 8099603525051415717L;

    protected final C checkBox;
    protected final JLabel label;
    protected final GridBagConstraints checkBoxConstraints;
    protected final GridBagConstraints labelConstraints;
    protected int iconTextGap;
    protected boolean editable;

    protected final Collection<ActionListener> actionListeners;
    protected final Collection<ItemListener> itemListeners;

    public AIconClickableCheckBox() {
        this(null, null, false);
    }

    public AIconClickableCheckBox(Icon icon) {
        this(null, icon, false);
    }

    /**
     * Creates a check box with an icon and specifies whether or not it is initially selected.
     *
     * @param icon the Icon image to display
     * @param selected a boolean value indicating the initial selection state. If <code>true</code> the check box is
     *            selected
     */
    public AIconClickableCheckBox(Icon icon, boolean selected) {
        this(null, icon, selected);
    }

    /**
     * Creates an initially unselected check box with text.
     *
     * @param text the text of the check box.
     */
    public AIconClickableCheckBox(String text) {
        this(text, null, false);
    }

    /**
     * Creates a check box with text and specifies whether
     * or not it is initially selected.
     *
     * @param text the text of the check box.
     * @param selected a boolean value indicating the initial selection state. If <code>true</code> the check box is
     *            selected
     */
    public AIconClickableCheckBox(String text, boolean selected) {
        this(text, null, selected);
    }

    /**
     * Creates an initially unselected check box with
     * the specified text and icon.
     *
     * @param text the text of the check box.
     * @param icon the Icon image to display
     */
    public AIconClickableCheckBox(String text, Icon icon) {
        this(text, icon, false);
    }

    /**
     * Creates a check box with text and icon, and specifies whether or not it is initially selected.
     *
     * @param text the text of the check box.
     * @param icon the Icon image to display
     * @param selected a boolean value indicating the initial selection state. If <code>true</code> the check box is
     *            selected
     */
    public AIconClickableCheckBox(String text, Icon icon, boolean selected) {
        super(new GridBagLayout());
        actionListeners = Collections.newSetFromMap(new ConcurrentHashMap<ActionListener, Boolean>());
        itemListeners = Collections.newSetFromMap(new ConcurrentHashMap<ItemListener, Boolean>());
        editable = true;
        iconTextGap = 0;
        checkBox = generateCheckBox();
        if (text != null) {
            checkBox.setText(text);
        }
        if (icon != null) {
            checkBox.setIcon(icon);
        }
        checkBox.setSelected(selected);
        checkBox.addItemListener(this);
        checkBox.addActionListener(this);
        checkBoxConstraints = new GridBagConstraints();
        checkBoxConstraints.fill = GridBagConstraints.NONE;
        checkBoxConstraints.gridx = 0;
        checkBoxConstraints.gridy = 0;
        checkBoxConstraints.weightx = 0;
        checkBoxConstraints.weighty = 0;
        checkBoxConstraints.anchor = GridBagConstraints.CENTER;
        add(checkBox, checkBoxConstraints);
        label = new JLabel();
        labelConstraints = new GridBagConstraints();
        labelConstraints.fill = GridBagConstraints.BOTH;
        labelConstraints.gridx = 1;
        labelConstraints.gridy = 0;
        labelConstraints.weightx = 1;
        labelConstraints.weighty = 1;
        add(label, labelConstraints);
    }

    /**
     * Generates a new {@link JCheckBox} of expected calss.
     * 
     * @return A {@link JCheckBox}.
     */
    protected abstract C generateCheckBox();

    /**
     * Returns whether the {@link JCheckBox} is selected
     * 
     * @return A <code>boolean</code>
     */
    public boolean isSelected() {
        return checkBox.isSelected();
    }

    /**
     * Selects or unselects the {@link JCheckBox}
     * 
     * @param selected Whether to select the {@link JCheckBox}
     */
    public void setSelected(boolean selected) {
        checkBox.setSelected(selected);
    }

    @Override
    public void setBackground(Color color) {
        if (checkBox != null) {
            checkBox.setBackground(color);
        }
        if (label != null) {
            label.setBackground(color);
        }
        super.setBackground(color);
    }

    @Override
    public Color getBackground() {
        return checkBox == null ? super.getBackground() : checkBox.getBackground();
    }

    @Override
    public void setForeground(Color color) {
        if (checkBox != null) {
            checkBox.setForeground(color);
        }
        if (label != null) {
            label.setForeground(color);
        }
        super.setForeground(color);
    }

    @Override
    public Color getForeground() {
        return label == null ? null : label.getForeground();
    }

    @Override
    public void setFont(Font font) {
        if (label != null) {
            label.setFont(font);
        }
        super.setFont(font);
    }

    @Override
    public Font getFont() {
        return label == null ? super.getFont() : label.getFont();
    }

    /**
     * Sets the text horizontal alignment
     * 
     * @param halign The alignment to set
     */
    public void setHorizontalAlignment(int halign) {
        if (checkBox != null) {
            checkBox.setHorizontalAlignment(halign);
        }
        if (label != null) {
            label.setHorizontalAlignment(halign);
        }
    }

    /**
     * Returns the text horizontal alignment
     * 
     * @return An <code>int</code>
     */
    public int getHorizontalAlignment() {
        return checkBox.getHorizontalAlignment();
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled() && (checkBox == null || checkBox.isEnabled());
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (checkBox != null) {
            checkBox.setEnabled(enabled);
        }
        if (label != null) {
            label.setEnabled(enabled);
        }
        super.setEnabled(enabled);
    }

    /**
     * Sets the {@link JCheckBox} editable or not.
     * 
     * @param editable Whether the {@link JCheckBox} should be editable.
     */
    public void setEditable(boolean editable) {
        if (this.editable != editable) {
            this.editable = editable;
            if (checkBox != null) {
                checkBox.setEnabled(editable);
            }
        }
    }

    public boolean isEditable() {
        return editable;
    }

    /**
     * Adds an {@link ItemListener} to the {@link JCheckBox}
     * 
     * @param listener The {@link ItemListener} to be added
     */
    @Override
    public void addItemListener(ItemListener listener) {
        if (listener != null) {
            itemListeners.add(listener);
        }
    }

    /**
     * Removes an ItemListener from the {@link JCheckBox}
     * 
     * @param listener The {@link ItemListener} to be removed
     */
    @Override
    public void removeItemListener(ItemListener listener) {
        if (listener != null) {
            itemListeners.remove(listener);
        }
    }

    /**
     * Adds an {@link ActionListener} to the {@link JCheckBox}
     * 
     * @param listener The {@link ActionListener} to be added
     */
    public void addActionListener(ActionListener listener) {
        if (listener != null) {
            actionListeners.add(listener);
        }
    }

    /**
     * Removes an ActionListener from the {@link JCheckBox}
     * 
     * @param listener The {@link ActionListener} to be removed
     */
    public void removeActionListener(ActionListener listener) {
        if (listener != null) {
            actionListeners.remove(listener);
        }
    }

    @Override
    public void setToolTipText(String text) {
        super.setToolTipText(text);
        if (checkBox != null) {
            checkBox.setToolTipText(text);
        }
        if (label != null) {
            label.setToolTipText(text);
        }
    }

    /**
     * Returns the text string that the {@link JCheckBox} displays.
     * 
     * @return A {@link String}
     */
    public String getText() {
        return label == null ? ObjectUtils.EMPTY_STRING : label.getText();
    }

    /**
     * Defines the single line of text this component will display. If the value of text is null or empty string,
     * nothing is displayed.
     * 
     * The default value of this property is null.
     * 
     * This is a JavaBeans bound property.
     * 
     * @param text The text to display
     */
    public void setText(String text) {
        if (label != null) {
            label.setText(text);
        }
    }

    /**
     * Returns the amount of space between the text and the icon displayed in this button.
     * 
     * @return An <code>int</code> equal to the number of pixels between the text and the icon.
     */
    public int getIconTextGap() {
        return iconTextGap;
    }

    /**
     * If both the icon and text properties are set, this property defines the space between them.
     * The default value of this property is 4 pixels.
     * 
     * This is a JavaBeans bound property.
     * 
     * @param iconTextGap The gap to set
     */
    public void setIconTextGap(int iconTextGap) {
        if (this.iconTextGap != iconTextGap) {
            this.iconTextGap = iconTextGap;
            Insets insets;
            remove(checkBox);
            if (checkBoxConstraints.gridx == labelConstraints.gridx) {
                if (checkBoxConstraints.gridy > labelConstraints.gridy) {
                    insets = new Insets(iconTextGap, 0, 0, 0);
                } else {
                    insets = new Insets(0, 0, iconTextGap, 0);
                }
            } else if (checkBoxConstraints.gridx < labelConstraints.gridx) {
                insets = new Insets(0, 0, 0, iconTextGap);
            } else {
                insets = new Insets(0, iconTextGap, 0, 0);
            }
            checkBoxConstraints.insets = insets;
            add(checkBox, checkBoxConstraints);
        }
    }

    @Override
    public Object[] getSelectedObjects() {
        return checkBox.getSelectedObjects();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if ((e != null) && (e.getSource() == checkBox)) {
            ItemEvent event = new ItemEvent(this, e.getID(), e.getItem(), e.getStateChange());
            for (ItemListener listener : itemListeners) {
                listener.itemStateChanged(event);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ((e != null) && (e.getSource() == checkBox)) {
            ActionEvent event = new ActionEvent(this, e.getID(), e.getActionCommand(), e.getWhen(), e.getModifiers());
            for (ActionListener listener : actionListeners) {
                listener.actionPerformed(event);
            }
        }

    }

}
