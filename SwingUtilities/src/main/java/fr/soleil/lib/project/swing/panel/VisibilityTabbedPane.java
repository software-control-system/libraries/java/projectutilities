/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

/**
 * 
 * @author SAINTIN
 * 
 *         This class extend JTabbedPane and provide method to hide and show a
 *         tabbed. All the property visible, enable, icon, title and tooltype
 *         can be get and set according to a specified component and not the
 *         index
 * 
 */
public class VisibilityTabbedPane extends JTabbedPane {

    private static final long serialVersionUID = -3662082241207072254L;

    private Component[] componentList = null;
    private String[] titleList = null;
    private Icon[] iconList = null;
    private String[] tipList = null;
    private Color[] backgroundList = null;
    private Color[] foregroundList = null;
    private boolean[] enableList = null;

    /*
     * @see javax.swing.JTabbedPane#add(java.awt.Component)
     */
    @Override
    public Component add(Component component) {
        return add((String) null, component);
    }

    /*
     * @see javax.swing.JTabbedPane#add(java.lang.String,java.awt.Component)
     */
    @Override
    public Component add(String title, Component component) {
        Component comp = null;
        int oldIndex = getInitIndex(component);
        if (oldIndex < 0) {
            if (title == null) {
                comp = super.add(component);
            } else {
                comp = super.add(title, component);
            }
            insertComponent(component);
        } else {
            setComponentVisible(component, true);
            setComponentTitle(component, title);
            setComponentTip(component, getInitTip(component));
            setComponentEnable(component, isInitEnable(component));
            setComponentBackground(component, getInitBackground(component));
            setComponentForeground(component, getInitForeground(component));
        }
        return comp;
    }

    private void insertComponentAt(String title, Icon icon, Component component, String tip, int index) {
        super.insertTab(title, icon, component, tip, index);
        int oldIndex = getInitIndex(component);
        if (oldIndex < 0) {
            insertComponent(component);
        } else {
            setComponentEnable(component, isInitEnable(component));
            setComponentTitle(component, title);
            setComponentTip(component, tip);
            setComponentIcon(component, icon);
            setComponentBackground(component, getInitBackground(component));
            setComponentForeground(component, getInitForeground(component));
        }
    }

    /**
     * Get the visibility of a given component
     * 
     * @return true if the component is added to the container
     */
    public boolean isComponentVisible(Component component) {
        int index = getComponentIndex(component);
        return index > -1;
    }

    /**
     * Set the visibility to a given component. If the component not exist
     * before it will be added in a new tabbed at last
     */
    public void setComponentVisible(Component component, boolean visible) {
        // If it is a new component add at last
        int oldIndex = getInitIndex(component);
        // System.out.println("oldIndex=" + oldIndex);
        if (oldIndex < 0 && visible) {
            super.add(component);
            insertComponent(component);
        } else {
            int index = getComponentIndex(component);
            if (visible && index < 0) {
                // System.out.println("componentList=" +
                // Arrays.toString(componentList));
                if (componentList != null && oldIndex < componentList.length) {
                    int newIndex = -1;
                    Component oldComponent = null;
                    for (int i = 0; i <= oldIndex; i++) {
                        oldComponent = componentList[i];
                        // System.out.println("oldComponent=" + oldComponent);
                        if (isComponentVisible(oldComponent) || oldComponent == component) {
                            newIndex++;
                            // System.out.println("newIndex=" + newIndex);
                        }
                    }
                    insertComponentAt(getInitTitle(component), getInitIcon(component), component, getInitTip(component),
                            newIndex);
                }
            } else if (!visible && index > -1) {
                remove(component);
            }
        }
    }

    /**
     * Return the background color tab for a given component
     */
    public Color getComponentBackground(Component component) {
        return getInitBackground(component);
    }

    /**
     * Set the background color tab for a given component
     * 
     * @see javax.swing.JTabbedPane#setBackgroundAt(int, java.awt.Color)
     */
    public void setComponentBackground(Component component, Color color) {
        int index = getComponentIndex(component);
        if (index > -1) {
            setBackgroundAt(index, color);
        }
        setInitBackground(component, color);
    }

    /**
     * Return the foreground color tab for a given component
     */
    public Color getComponentForeground(Component component) {
        return getInitForeground(component);
    }

    /**
     * Set the foreground color tab for a given component
     * 
     * @see javax.swing.JTabbedPane#setForegroundAt(int, java.awt.Color)
     */
    public void setComponentForeground(Component component, Color color) {
        int index = getComponentIndex(component);
        if (index > -1) {
            setForegroundAt(index, color);
        }
        setInitForeground(component, color);
    }

    /**
     * Return true if the tab for a given component is enable
     */
    public boolean isComponentEnable(Component component) {
        return isInitEnable(component);
    }

    /**
     * Disable or enable a tab for a given component
     * 
     * @see javax.swing.JTabbedPane#setEnabledAt(int, boolean)
     */
    public void setComponentEnable(Component component, boolean enable) {
        int index = getComponentIndex(component);
        if (index > -1) {
            setEnabledAt(index, enable);
        }
        setInitEnable(component, enable);
    }

    /**
     * Return the Icon tab for a given component
     */
    public Icon getComponentIcon(Component component) {
        return getInitIcon(component);
    }

    /**
     * Set the Icon tab for a given component
     * 
     * @see javax.swing.JTabbedPane#setIconAt(int, javax.swing.Icon)
     */
    public void setComponentIcon(Component component, Icon icon) {
        int index = getComponentIndex(component);
        if (index > -1) {
            setIconAt(index, icon);
        }
        setInitIcon(component, icon);
    }

    /**
     * Return the title tab for a given component
     */
    public String getComponentTitle(Component component) {
        return getInitTitle(component);
    }

    /**
     * Set the title tab for a given component
     * 
     * @see javax.swing.JTabbedPane#setTitleAt(int, java.lang.String)
     */
    public void setComponentTitle(Component component, String title) {
        int index = getComponentIndex(component);
        if (index > -1) {
            setTitleAt(index, title);
        }
        setInitTitle(component, title);
    }

    /**
     * Return the tool type text tab for a given component
     */
    public String getComponentTip(Component component) {
        return getInitTip(component);
    }

    /**
     * Set the tool type text tab for a given component
     * 
     * @see javax.swing.JTabbedPane#setToolTipTextAt(int, java.lang.String)
     */
    public void setComponentTip(Component component, String tip) {
        int index = getComponentIndex(component);
        if (index > -1) {
            setToolTipTextAt(index, tip);
        }
        setInitTip(component, tip);
    }

    /**
     * Return the tab index for a given component
     */
    public int getComponentIndex(Component component) {
        int index = -1;
        int count = getComponentCount();
        if (component != null && count > 0) {
            for (int i = 0; i < count; i++) {
                if (component == getComponentAt(i)) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    private void initComponentList() {
        if (componentList == null || componentList.length == 0) {
            int count = getComponentCount();
            componentList = new Component[count];
            titleList = new String[count];
            iconList = new Icon[count];
            tipList = new String[count];
            backgroundList = new Color[count];
            foregroundList = new Color[count];
            enableList = new boolean[count];

            for (int i = 0; i < count; i++) {
                componentList[i] = getComponentAt(i);
                titleList[i] = getTitleAt(i);
                iconList[i] = getIconAt(i);
                tipList[i] = getToolTipTextAt(i);
                backgroundList[i] = getBackgroundAt(i);
                foregroundList[i] = getForegroundAt(i);
                enableList[i] = isEnabledAt(i);
            }
        }
    }

    private void insertComponent(Component component) {
        if (componentList == null || componentList.length == 0) {
            initComponentList();
        } else {
            Component[] oldComponentList = Arrays.copyOf(componentList, componentList.length);
            String[] oldTitleList = Arrays.copyOf(titleList, titleList.length);
            Icon[] oldIconList = Arrays.copyOf(iconList, iconList.length);
            String[] oldTipList = Arrays.copyOf(tipList, tipList.length);
            Color[] oldBackgroundList = Arrays.copyOf(backgroundList, backgroundList.length);
            Color[] oldForegroundList = Arrays.copyOf(foregroundList, foregroundList.length);
            boolean[] oldEnableList = Arrays.copyOf(enableList, enableList.length);

            int newCount = oldComponentList.length + 1;
            componentList = new Component[newCount];
            titleList = new String[newCount];
            iconList = new Icon[newCount];
            tipList = new String[newCount];
            backgroundList = new Color[newCount];
            foregroundList = new Color[newCount];
            enableList = new boolean[newCount];

            Component currentComponent = null;
            // At last by default
            int insertIndex = componentList.length - 1;
            int currentCount = getComponentCount();
            for (int currentIndex = currentCount - 1; currentIndex > -1; currentIndex--) {
                currentComponent = getComponentAt(currentIndex);
                if (component == currentComponent) {
                    insertIndex = currentIndex;
                    break;
                }
            }

            int currentIndex = getComponentIndex(component);
            int oldIndex = -1;
            for (int i = 0; i < componentList.length; i++) {
                if (i == currentIndex) {
                    componentList[insertIndex] = component;
                    titleList[insertIndex] = getTitleAt(currentIndex);
                    iconList[insertIndex] = getIconAt(currentIndex);
                    tipList[insertIndex] = getToolTipTextAt(currentIndex);
                    backgroundList[insertIndex] = getBackgroundAt(currentIndex);
                    foregroundList[insertIndex] = getForegroundAt(currentIndex);
                    enableList[insertIndex] = isEnabledAt(currentIndex);
                } else {
                    if (i < currentIndex) {
                        oldIndex = i;
                    } else {
                        oldIndex = i - 1;
                    }
                    componentList[i] = oldComponentList[oldIndex];
                    titleList[i] = oldTitleList[oldIndex];
                    iconList[i] = oldIconList[oldIndex];
                    tipList[i] = oldTipList[oldIndex];
                    backgroundList[i] = oldBackgroundList[oldIndex];
                    foregroundList[i] = oldForegroundList[oldIndex];
                    enableList[i] = oldEnableList[oldIndex];
                }
            }
            // System.out.println("componentList = " +
            // Arrays.toString(componentList));
        }
    }

    private int getInitIndex(Component component) {
        int index = -1;
        initComponentList();
        if (component != null && componentList != null) {
            for (int i = 0; i < componentList.length; i++) {
                if (component == componentList[i]) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    private boolean isInitEnable(Component component) {
        boolean enable = true;
        initComponentList();
        int index = getInitIndex(component);
        if (index > -1 && enableList != null && index < enableList.length) {
            enable = enableList[index];
        }
        return enable;
    }

    private void setInitEnable(Component component, boolean enable) {
        initComponentList();
        int index = getInitIndex(component);
        if (index > -1 && enableList != null && index < enableList.length) {
            enableList[index] = enable;
        }
    }

    private String getInitTitle(Component component) {
        String title = null;
        initComponentList();
        int index = getInitIndex(component);
        if (index > -1 && titleList != null && index < titleList.length) {
            title = titleList[index];
        }
        return title;
    }

    private void setInitTitle(Component component, String title) {
        initComponentList();
        int index = getInitIndex(component);
        if (index > -1 && titleList != null && index < titleList.length) {
            titleList[index] = title;
        }
    }

    private String getInitTip(Component component) {
        String tip = null;
        initComponentList();
        int index = getInitIndex(component);
        if (index > -1 && tipList != null && index < tipList.length) {
            tip = tipList[index];
        }
        return tip;
    }

    private void setInitTip(Component component, String tip) {
        initComponentList();
        int index = getInitIndex(component);
        if (index > -1 && tipList != null && index < tipList.length) {
            tipList[index] = tip;
        }
    }

    private Icon getInitIcon(Component component) {
        Icon icon = null;
        initComponentList();
        int index = getInitIndex(component);
        if (index > -1 && iconList != null && index < iconList.length) {
            icon = iconList[index];
        }
        return icon;
    }

    private void setInitIcon(Component component, Icon icon) {
        int index = getInitIndex(component);
        if (index > -1 && iconList != null && index < iconList.length) {
            iconList[index] = icon;
        }
    }

    private Color getInitBackground(Component component) {
        Color color = null;
        initComponentList();
        int index = getInitIndex(component);
        if (index > -1 && backgroundList != null && index < backgroundList.length) {
            color = backgroundList[index];
        }
        return color;
    }

    private void setInitBackground(Component component, Color color) {
        int index = getInitIndex(component);
        if (index > -1 && backgroundList != null && index < backgroundList.length) {
            backgroundList[index] = color;
        }
    }

    private Color getInitForeground(Component component) {
        Color color = null;
        initComponentList();
        int index = getInitIndex(component);
        if (index > -1 && foregroundList != null && index < foregroundList.length) {
            color = foregroundList[index];
        }
        return color;
    }

    private void setInitForeground(Component component, Color color) {
        int index = getInitIndex(component);
        if (index > -1 && foregroundList != null && index < foregroundList.length) {
            foregroundList[index] = color;
        }
    }

    /**
     * Main test
     * 
     * @param args
     */
    @SuppressWarnings("serial")
    public static void main(String[] args) {
        JPanel mainPanel = new JPanel();
        mainPanel.setSize(500, 300);
        mainPanel.setPreferredSize(mainPanel.getSize());
        mainPanel.setLayout(new BorderLayout());
        final VisibilityTabbedPane tabbedPanel = new VisibilityTabbedPane();
        mainPanel.add(tabbedPanel, BorderLayout.CENTER);
        JPanel southPanel = new JPanel();
        mainPanel.add(southPanel, BorderLayout.SOUTH);

        JPanel redPanel = new JPanel() {
            @Override
            public String toString() {
                return "RED";
            };
        };
        redPanel.setBackground(Color.RED);

        JPanel bluePanel = new JPanel() {
            @Override
            public String toString() {
                return "BLUE";
            };
        };
        bluePanel.setBackground(Color.BLUE);

        JPanel yellowPanel = new JPanel() {
            @Override
            public String toString() {
                return "YELLOW";
            };
        };

        yellowPanel.setBackground(Color.YELLOW);

        final JComboBox<JPanel> combo = new JComboBox<>();
        combo.setEditable(false);
        combo.addItem(redPanel);
        combo.addItem(bluePanel);
        combo.addItem(yellowPanel);

        southPanel.add(combo);

        final JCheckBox visible = new JCheckBox("Visible");
        southPanel.add(visible);

        final JCheckBox enable = new JCheckBox("Enable");
        enable.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JPanel panel = (JPanel) combo.getSelectedItem();
                tabbedPanel.setComponentEnable(panel, enable.isSelected());
            }
        });

        southPanel.add(enable);

        final JTextField title = new JTextField("No Title");
        title.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JPanel panel = (JPanel) combo.getSelectedItem();
                tabbedPanel.setComponentTitle(panel, title.getText());
            }
        });

        southPanel.add(title);

        final JTextField tip = new JTextField("No Tip");
        tip.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JPanel panel = (JPanel) combo.getSelectedItem();
                tabbedPanel.setComponentTip(panel, tip.getText());
            }
        });

        southPanel.add(tip);

        visible.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JPanel panel = (JPanel) combo.getSelectedItem();
                tabbedPanel.setComponentVisible(panel, visible.isSelected());
                visible.setSelected(tabbedPanel.isComponentVisible(panel));
                enable.setSelected(tabbedPanel.isComponentEnable(panel));
                String atitle = tabbedPanel.getComponentTitle(panel);
                if (atitle == null || atitle.isEmpty()) {
                    atitle = panel.toString();
                    tabbedPanel.setComponentTitle(panel, atitle);
                    title.setText(atitle);
                }
                String aTip = tabbedPanel.getComponentTip(panel);
                if (aTip == null || aTip.isEmpty()) {
                    aTip = panel.toString() + " panel";
                    tabbedPanel.setComponentTip(panel, aTip);
                    tip.setText(aTip);
                }
            }
        });

        combo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JPanel panel = (JPanel) combo.getSelectedItem();
                visible.setSelected(tabbedPanel.isComponentVisible(panel));
                enable.setSelected(tabbedPanel.isComponentEnable(panel));
                title.setText(tabbedPanel.getComponentTitle(panel));
                tip.setText(tabbedPanel.getComponentTip(panel));
            }
        });

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setTitle("Tabbed Panel");
        frame.setContentPane(mainPanel);
        frame.pack();
        frame.setVisible(true);
    }
}
