/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.listener;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;
import java.util.WeakHashMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.border.Border;

/**
 * A {@link MouseAdapter} that listens to a {@link JButton} in order to change its border when mouse is over it.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ButtonDecorationListener extends MouseAdapter {

    /**
     * The default {@link Border} this {@link ButtonDecorationListener} will apply to a Comp
     */
    public static final Border DEFAULT_OVER_BORDER = BorderFactory.createLineBorder(Color.DARK_GRAY, 1);

    protected final Border overBorder;
    protected final Map<JButton, Border> borderMap;

    public ButtonDecorationListener() {
        this(DEFAULT_OVER_BORDER);
    }

    public ButtonDecorationListener(Border overBorder) {
        super();
        this.overBorder = overBorder;
        borderMap = new WeakHashMap<JButton, Border>();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        JButton button = recoverButton(e);
        if ((button != null) && button.isEnabled()) {
            synchronized (borderMap) {
                Border border = button.getBorder();
                if (border == null) {
                    borderMap.remove(button);
                } else {
                    borderMap.put(button, border);
                }
            }
            button.setBorder(overBorder);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        JButton button = recoverButton(e);
        if ((button != null) && button.isEnabled()) {
            Border border;
            synchronized (borderMap) {
                border = borderMap.get(button);
            }
            button.setBorder(border);
        }
    }

    protected JButton recoverButton(MouseEvent e) {
        JButton button = null;
        if ((e != null) && (e.getSource() instanceof JButton)) {
            button = (JButton) e.getSource();
        }
        return button;
    }

}
