/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text.scroll;

import java.awt.font.FontRenderContext;

import fr.soleil.lib.project.swing.text.ITextComponent;

public interface IAutoScrolledTextComponent extends ITextComponent {

    /**
     * Returns this {@link IAutoScrolledComponent}'s scrolling mode.
     * 
     * @return A {@link ScrollMode}.
     * @see #setScrollMode(ScrollMode)
     */
    public ScrollMode getScrollMode();

    /**
     * Sets this {@link IAutoScrolledComponent}'s scrolling mode.
     * 
     * @param scrollMode The {@link ScrollMode} to set.
     * @see #getScrollMode()
     */
    public void setScrollMode(ScrollMode scrollMode);

    /**
     * Returns the time (in milliseconds) to wait between 2 scroll positions. A value less than or equal to
     * <code>0</code> indicates a stopped scrolling mode.
     * 
     * @return An <code>int</code>.
     * @see #setScrollStepsTime(int)
     */
    public int getScrollStepsTime();

    /**
     * Sets the time (in milliseconds) to wait between 2 scroll positions (for example: for the scrolled part moving
     * from 1 pixel).
     * 
     * @param stepTime The time to set (the bigger the value, the slower the scroll speed). A value less than or equal
     *            to <code>0</code> stops the scrolling mode.
     * @see #getScrollStepsTime()
     */
    public void setScrollStepsTime(int stepTime);

    /**
     * Returns the time (in milliseconds) to wait once an extremity is reached. This should be used
     * only in case of {@link ScrollMode#PENDULAR} or {@link ScrollMode#REACH_END} scrolling mode
     * 
     * @return An <code>int</code>
     * @see #setExtremeWaitingTime(int)
     * @see #getScrollMode()
     * @see #setScrollMode(ScrollMode)
     */
    public int getReachEndWaitingTime();

    /**
     * Sets the time (in milliseconds) to wait once an extremity is reached.
     * 
     * @param waitingTime The waiting time to set
     * @see #getExtremeWaitingTime()
     */
    public void setReachEndWaitingTime(int waitingTime);

    /**
     * Returns this {@link IAutoScrolledTextComponent}'s text width.
     * 
     * @param frc The {@link FontRenderContext} that may be used to calculate text width.
     * 
     * @return An <code>int</code>.
     */
    public int getTextWidth(FontRenderContext frc);

    /**
     * Returns the width available to display some text.
     * 
     * @return An <code>int</code>.
     */
    public int getDisplayableWidth();

    /**
     * Returns the {@link FontRenderContext} that may be used to measure strings.
     * 
     * @return A {@link FontRenderContext}, never <code>null</code>.
     */
    public FontRenderContext getFontRenderContext();

}
