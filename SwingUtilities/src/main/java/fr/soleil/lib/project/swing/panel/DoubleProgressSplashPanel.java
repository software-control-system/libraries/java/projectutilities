/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import fr.soleil.lib.project.swing.JDoubleProgressBar;

/**
 * An {@link ASplashPanel} using {@link JDoubleProgressBar}. Based on ATK SplashPanel
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DoubleProgressSplashPanel extends ASplashPanel<JDoubleProgressBar> implements MouseListener {

    private static final long serialVersionUID = 6722654786690539663L;

    // Panel components
    protected JLabel firstMessage;
    protected JLabel secondMessage;

    // Splash panel constructor
    public DoubleProgressSplashPanel() {
        this(null, null);
    }

    public DoubleProgressSplashPanel(ImageIcon icon, Color textForeground) {
        super(icon, textForeground, null);
    }

    @Override
    protected JDoubleProgressBar generateProgressBar() {
        JDoubleProgressBar progressBar = new JDoubleProgressBar();
        progressBar.setDisplayFGProgress(true);
        progressBar.setTransparent(false);
        return progressBar;
    }

    @Override
    protected Dimension getDefaultSize() {
        return null;
    }

    @Override
    protected void initComponents() {
        super.initComponents();
        title.setVerticalAlignment(SwingConstants.BOTTOM);
        firstMessage = generateLabel(INITIALIZING, MESSAGE_FONT);
        secondMessage = generateLabel(INITIALIZING, MESSAGE_FONT);
        addMouseListener(this);
    }

    @Override
    protected void layoutComponents() {
        setLayout(new GridBagLayout());
        GridBagConstraints titleConstraints = new GridBagConstraints();
        titleConstraints.fill = GridBagConstraints.BOTH;
        titleConstraints.gridx = 0;
        titleConstraints.gridy = 0;
        titleConstraints.weightx = 1;
        titleConstraints.weighty = 1;
        titleConstraints.insets = new Insets(0, 5, 0, 5);
        add(title, titleConstraints);
        GridBagConstraints copyrightConstraints = new GridBagConstraints();
        copyrightConstraints.fill = GridBagConstraints.BOTH;
        copyrightConstraints.gridx = 0;
        copyrightConstraints.gridy = 1;
        copyrightConstraints.weightx = 1;
        copyrightConstraints.weighty = 0;
        copyrightConstraints.insets = new Insets(0, 5, 5, 5);
        add(copyright, copyrightConstraints);
        GridBagConstraints progressConstraints = new GridBagConstraints();
        progressConstraints.fill = GridBagConstraints.BOTH;
        progressConstraints.gridx = 0;
        progressConstraints.gridy = 2;
        progressConstraints.weightx = 1;
        progressConstraints.weighty = 0;
        progressConstraints.insets = new Insets(0, 6, 5, 5);
        add(progressBar, progressConstraints);
        GridBagConstraints firstMessageConstraints = new GridBagConstraints();
        firstMessageConstraints.fill = GridBagConstraints.BOTH;
        firstMessageConstraints.gridx = 0;
        firstMessageConstraints.gridy = 3;
        firstMessageConstraints.weightx = 1;
        firstMessageConstraints.weighty = 0;
        firstMessageConstraints.insets = new Insets(0, 6, 0, 5);
        add(firstMessage, firstMessageConstraints);
        GridBagConstraints secondMessageConstraints = new GridBagConstraints();
        secondMessageConstraints.fill = GridBagConstraints.BOTH;
        secondMessageConstraints.gridx = 0;
        secondMessageConstraints.gridy = 4;
        secondMessageConstraints.weightx = 1;
        secondMessageConstraints.weighty = 0;
        secondMessageConstraints.insets = new Insets(0, 6, 10, 5);
        add(secondMessage, secondMessageConstraints);
        revalidate();
        if (isShowing()) {
            repaint();
        }
    }

    public void setBGProgress(int p) {
        if (progressBar.getBGValue() != p) {
            progressBar.setBGValue(p);
        }
    }

    public void setFGProgress(int p) {
        if (progressBar.getFGValue() != p) {
            progressBar.setFGValue(p);
        }
    }

    public JDoubleProgressBar getDoubleProgressBar() {
        return progressBar;
    }

    public void setFirstMessage(String s) {
        firstMessage.setText(s);
    }

    public String getFirstMessage() {
        return firstMessage.getText();
    }

    public void setSecondMessage(String s) {
        secondMessage.setText(s);
    }

    public String getSecondMessage() {
        return secondMessage.getText();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }
}
