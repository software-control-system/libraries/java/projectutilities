/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Window;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.swing.file.FileChooserUtils;

/**
 * A class used to make some snapshots.
 * 
 * @author saintin, Rapha&euml;l GIRARDOT
 */
public class SnapshotUtil implements SwingUtilitiesConstants {

    // <String thread id, thread>
    private static final Map<String, SnapshotThread> THREAD_MAP = new ConcurrentHashMap<>();
    private static Robot robot = null;
    private static final Object ROBOT_LOCK = new Object();

    // These are the really supported file extensions
    // Due to a java bug, "jpeg" and "wbmp" are not supported
    protected static final String BMP = "bmp";
    protected static final String GIF = "gif";
    protected static final String PNG = "png";
    protected static final String JPG = "jpg";

    // All supported image extensions
    protected static final String[] IMAGE_EXTENSIONS = { PNG, JPG, GIF, BMP };
    public static final List<String> IMAGE_EXTENSION_LIST = Collections
            .unmodifiableList(Arrays.asList(IMAGE_EXTENSIONS));

    // Supported lossless image extensions
    protected static final String[] LOSSLESS_IMAGE_EXTENSIONS = { PNG, GIF, BMP };
    public static final List<String> LOSSLESS_IMAGE_EXTENSION_LIST = Collections
            .unmodifiableList(Arrays.asList(LOSSLESS_IMAGE_EXTENSIONS));

    /**
     * Opens a file chooser to select target image file, and save a snapshot of the parametered {@link Component} in the
     * selected file
     * 
     * @param comp The {@link Component} from which to make a snapshot
     * @return The absolute path of the saved file, if successfully written. <code>null</code> otherwise
     */
    public static String snapshot(Component comp) {
        return snapshot(comp, FileChooserUtils.showFileChooserDialog(comp, false, false, null, IMAGE_EXTENSIONS));
    }

    /**
     * Makes a snapshot of a {@link Component}, and saves the result in a file, that may be changed through a file
     * chooser. If the file already exists, this method asks user for confirmation
     * 
     * @param comp The {@link Component} from which to make a snapshot
     * @param fileName The path of the file
     * @param useFileChooser whether to use a file chooser to change the output file
     * @return The absolute path of the saved file, if successfully written. <code>null</code> otherwise
     */
    public static String snapshot(Component comp, String fileName, boolean useFileChooser) {
        return snapshot(comp, fileName, null, useFileChooser);
    }

    /**
     * Makes a snapshot of a {@link Component}, and saves the result in a file, that may be changed through a file
     * chooser. If the file already exists, this method asks user for confirmation
     * 
     * @param comp The {@link Component} from which to make a snapshot
     * @param fileName The path of the file
     * @param preferredExtension The preferred extension for fileChooser (the 1st one that should be seen in file
     *            extension list)
     * @param useFileChooser whether to use a file chooser to change the output file
     * @return The absolute path of the saved file, if successfully written. <code>null</code> otherwise
     */
    public static String snapshot(Component comp, String fileName, String preferredExtension, boolean useFileChooser) {
        String result;
        if (useFileChooser) {
            result = doSnapshot(comp, FileChooserUtils.showFileChooserDialog(comp, false, false, fileName,
                    preferredExtension, IMAGE_EXTENSIONS), true);
        } else {
            result = doSnapshot(comp, fileName, true);
        }
        return result;
    }

    /**
     * Makes a snapshot of a {@link Component}, and saves the result in a file, overwriting it if it already exists
     * 
     * @param comp The {@link Component} from which to make a snapshot
     * @param fileName The path of the file
     * @return The absolute path of the saved file, if successfully written. <code>null</code> otherwise
     */
    public static String snapshot(Component comp, String fileName) {
        return doSnapshot(comp, fileName, false);
    }

    /**
     * Makes a snapshot of a {@link Component}, and saves the result in a file.
     * 
     * @param comp The {@link Component} from which to make a snapshot.
     * @param fileName The path of the file.
     * @param askConfirmationWhenExist Whether to ask user for confirmation if the output file already exists.
     * @return The absolute path of the saved file, if successfully written. <code>null</code> otherwise.
     */
    protected static String doSnapshot(Component comp, String fileName, boolean askConfirmationWhenExist) {
        String filePath = null;
        if ((comp != null) && (fileName != null)) {
            File f = new File(fileName);
            if (!f.isDirectory()) {
                String extension = FileUtils.getExtension(f);
                if (isImage(extension)) {
                    BufferedImage img = getSnapshot(comp);
                    try {
                        if ((img != null) && (extension != null) && (f != null)) {
                            int ok = JOptionPane.YES_OPTION;
                            if (!f.exists()) {
                                File parent = f.getParentFile();
                                if ((parent != null) && (!parent.exists())) {
                                    parent.mkdirs();
                                }
                                f.createNewFile();
                            } else {
                                ok = JOptionPane.showConfirmDialog(comp,
                                        String.format(
                                                DEFAULT_MESSAGE_MANAGER
                                                        .getMessage("fr.soleil.lib.project.swing.file.overwrite"),
                                                f.getAbsolutePath()),
                                        DEFAULT_MESSAGE_MANAGER
                                                .getMessage("fr.soleil.lib.project.swing.file.overwrite.confirm"),
                                        JOptionPane.YES_NO_OPTION);
                            }
                            if (ok == JOptionPane.YES_OPTION) {
                                ImageIO.write(img, extension, f);
                                img.flush();
                                filePath = f.getAbsolutePath();
                            }
                        }

                    } catch (IOException ioe) {
                        JOptionPane.showMessageDialog(comp,
                                DEFAULT_MESSAGE_MANAGER.getMessage("fr.soleil.lib.project.swing.file.save.error"),
                                DEFAULT_MESSAGE_MANAGER.getMessage("fr.soleil.lib.project.swing.error"),
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        }
        return filePath;
    }

    /**
     * Gets the bounds of a {@link Component} even if we are in virtual desktop case.
     * 
     * @param comp The {@link Component} from which to get bounds.
     * @return A {@link Rectangle}.
     */
    public static Rectangle getBounds(Component comp) {
        Rectangle result = comp.getBounds();
        result = computeBounds(result);
        return result;
    }

    /**
     * Gets a bound with screen location even if we are in virtual desktop case.
     * 
     * @param comp The {@link Component} from which to get bounds.
     * @return A {@link Rectangle}.
     */
    public static Rectangle getScreenLocation(Component comp) {
        Rectangle result = new Rectangle(comp.getLocationOnScreen(), comp.getSize());
        result = computeBounds(result);
        return result;
    }

    private static Rectangle computeBounds(Rectangle originalRectangle) {
        Rectangle bounds;
        Rectangle virtualBounds = new Rectangle();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();
        if ((gs != null) && (gs.length == 1) && (originalRectangle != null)) {
            bounds = originalRectangle;
        } else {
            for (GraphicsDevice gd : gs) {
                GraphicsConfiguration[] gc = gd.getConfigurations();
                for (GraphicsConfiguration element : gc) {
                    virtualBounds = virtualBounds.union(element.getBounds());
                }
            }
            if (originalRectangle == null) {
                bounds = virtualBounds;
            } else {
                int xLocation = originalRectangle.x + virtualBounds.x;
                int yLocation = originalRectangle.y + virtualBounds.y;
                int width = originalRectangle.width;
                int height = originalRectangle.height;
                if ((width == 0) || (height == 0)) {
                    bounds = originalRectangle;
                } else {
                    bounds = new Rectangle(xLocation, yLocation, width, height);
                }
            }
        }
        return bounds;
    }

    /**
     * Makes a snapshot of a {@link Component} and returns it as a {@link BufferedImage}.
     * 
     * @param comp The {@link Component} from which to make a snapshot.
     * @return A {@link BufferedImage}.
     */
    public static BufferedImage getSnapshot(Component comp) {
        BufferedImage img = null;
        if (comp != null) {
            if (comp instanceof Window) {
                // workaround to avoid an undesired bug (black image)
                img = getRobotSnapshot(comp);
            } else {
                Dimension size = comp.getSize();
                img = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
                comp.paint(img.getGraphics());
            }
        }
        return img;
    }

    /**
     * Makes a snapshot of a {@link Component}, using a {@link Robot}, and returns it as a {@link BufferedImage}.
     * 
     * @param comp The {@link Component} from which to make a snapshot.
     * @return A {@link BufferedImage}.
     */
    private synchronized static BufferedImage getRobotSnapshot(Component comp) {
        Rectangle screenArea = getBounds(comp);
        return getRobotSnapshot(screenArea);
    }

    /**
     * Makes a snapshot of the screen sector defined by the donate {@link Rectangle}, using a {@link Robot}, and returns
     * it as a {@link BufferedImage}.
     * 
     * @param comp The {@link Component} from which to make a snapshot.
     * @return A {@link BufferedImage}.
     */
    public synchronized static BufferedImage getRobotSnapshot(Rectangle bounds) {
        BufferedImage img = null;
        try {
            if (robot == null) {
                synchronized (ROBOT_LOCK) {
                    if (robot == null) {
                        robot = new Robot();
                    }
                }
            }
        } catch (AWTException e) {
            e.printStackTrace();
        }
        // Creation
        if (robot != null) {
            img = robot.createScreenCapture(bounds);
        }
        return img;
    }

    /**
     * Starts a {@link Thread} that will regularly make a snapshot of a {@link Component} and save the result in a file.
     * 
     * @param component The concerned {@link Component}.
     * @param periodMs The period in ms between 2 snapshots.
     * @param fileName The path of the target file.
     */
    public static void startSnapshot(Component component, long periodMs, String fileName) {
        if ((component != null) && (fileName != null) && (!fileName.trim().isEmpty()) && (!alreadyStarted(component))) {
            SnapshotThread snapshotThread = new SnapshotThread(component, fileName, periodMs);
            THREAD_MAP.put(snapshotThread.getID(), snapshotThread);
            snapshotThread.start();
        }
    }

    /**
     * Stops the previously started snapshot Thread for a {@link Component}.
     * 
     * @param component The concerned {@link Component}.
     * @see #startSnapshot(Component, long, String).
     */
    public static void stopSnapshot(Component component) {
        if (alreadyStarted(component)) {
            String key = String.valueOf(component.hashCode());
            SnapshotThread snapshotThread = THREAD_MAP.get(key);
            THREAD_MAP.remove(key);
            snapshotThread.stopSnapshot();
        }
    }

    /**
     * Returns whether a file extension represents an image file.
     * 
     * @param extension The file extension.
     * @return A <code>boolean</code> value.
     */
    public static boolean isImage(String extension) {
        boolean image = false;
        for (String ext : IMAGE_EXTENSIONS) {
            if (ext.equalsIgnoreCase(extension)) {
                image = true;
                break;
            }
        }
        return image;
    }

    /**
     * Returns whether a file extension represents a lossless image file.
     * 
     * @param extension The file extension.
     * @return A <code>boolean</code> value.
     */
    public static boolean isLosslessImage(String extension) {
        boolean image = false;
        for (String ext : LOSSLESS_IMAGE_EXTENSIONS) {
            if (ext.equalsIgnoreCase(extension)) {
                image = true;
                break;
            }
        }
        return image;
    }

    /**
     * Returns whether a snapshot {@link Thread} is already started for a given {@link Component}.
     * 
     * @param comp The concerned {@link Component}.
     * @return A <code>boolean</code> value.
     */
    private static boolean alreadyStarted(Component comp) {
        boolean started = false;
        if (comp != null) {
            String key = String.valueOf(comp.hashCode());
            if (THREAD_MAP.containsKey(key)) {
                started = true;
            }
        }
        return started;
    }

    public static void main(String[] args) {
        String fileName = "D:\\tmp\\CometeSnapshotUtil.jpg";
        if (args != null && args.length > 0) {
            fileName = args[0];
        }
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(new JLabel("Test de snapshot"), BorderLayout.CENTER);
        JFrame frame = new JFrame();
        frame.setContentPane(panel);
        frame.pack();
        frame.setVisible(true);
        frame.setLocation(20, 20);
        SnapshotUtil.startSnapshot(frame, 1000, fileName);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * This is the {@link Thread} that does regular snapshots of a {@link Component} to save the result in a file.
     * 
     * @author saintin
     */
    private static class SnapshotThread extends Thread {

        private final Component component;
        private final String fileName;
        private final long periodMs;
        private volatile boolean stop;

        /**
         * @param name
         */
        public SnapshotThread(Component comp, String name, long millis) {
            super(DateUtil.elapsedTimeToStringBuilder(
                    new StringBuilder("Snapshot ")
                            .append(comp == null ? "application" : comp.getClass().getSimpleName()).append(" every "),
                    millis).append(" in ").append(name).toString());
            this.component = comp;
            this.fileName = name;
            this.periodMs = millis;
            this.stop = false;
        }

        public void stopSnapshot() {
            stop = true;
        }

        public String getID() {
            String id;
            if (component == null) {
                id = String.valueOf(super.getId());
            } else {
                id = String.valueOf(component.hashCode());
            }
            return id;
        }

        @Override
        public void run() {
            while (!stop) {
                try {
                    sleep(periodMs);
                    if (!stop) {
                        SnapshotUtil.snapshot(component, fileName);
                    }
                } catch (InterruptedException e) {
                    stop = true;
                }
            }
        }
    }

}
