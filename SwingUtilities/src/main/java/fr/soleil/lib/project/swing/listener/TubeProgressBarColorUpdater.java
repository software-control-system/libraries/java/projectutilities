/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.listener;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;

import javax.swing.JProgressBar;
import javax.swing.plaf.ProgressBarUI;

import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.progress.IProgressable;
import fr.soleil.lib.project.progress.IProgressionListener;
import fr.soleil.lib.project.swing.ui.TubeProgressBarUI;

/**
 * An {@link IProgressionListener} that is able to change a {@link JProgressBar}'s {@link TubeProgressBarUI} progression
 * color, according to current progression.
 * <p>
 * The {@link TubeProgressBarColorUpdater} also updates the {@link JProgressBar} text and tooltip text.
 * <ul>
 * <li>You can choose whether to update progression text through {@link #setChangeProgressionText(boolean)}.</li>
 * <li>If allowed, the progression text will be updated with the current progression percent.</li>
 * <li>The number of progression percent decimal digits can be set through {@link #setDecimalDigits(int)}</li>
 * <li>The progression colors can be set through {@link #setProgressionColors(Color...)}.</li>
 * <li>You can choose whether to update tooltip text through {@link #setChangeTooltipText(boolean)}.</li>
 * <li>If allowed, the tooltip text <ill be updated with the progression message</li>
 * </ul>
 * 
 * @see #progressionChanged(IProgressable, int, String)
 * @author Rapha&euml;l GIRARDOT
 */
public class TubeProgressBarColorUpdater implements IProgressionListener, PropertyChangeListener {

    protected static final String UI = "UI";
    protected static final String FORMAT_START = "%.";
    protected static final String FORMAT_END = "f%%";
    protected static final String DEFAULT_FORMAT = "%.2f%%";

    protected ProgressionContainer progressionContainer;
    protected final WeakReference<JProgressBar> progressBarRef;
    protected volatile WeakReference<TubeProgressBarUI> tubeProgressBarUIRef;
    protected volatile boolean changeTooltipText;
    protected volatile boolean changeProgressionText;
    protected volatile String format;

    /**
     * Creates a new {@link TubeProgressBarColorUpdater} that will listen to a {@link JProgressBar}
     * 
     * @param progressBar The {@link JProgressBar}
     */
    public TubeProgressBarColorUpdater(JProgressBar progressBar) {
        this(progressBar, TubeProgressBarUI.DEFAULT_PROGRESS_COLOR);
    }

    /**
     * Creates a new {@link TubeProgressBarColorUpdater} that will listen to a {@link JProgressBar}, customizing its
     * progression colors
     * 
     * @param progressBar The {@link JProgressBar}
     * @param colors The progression colors.
     */
    public TubeProgressBarColorUpdater(JProgressBar progressBar, Color... colors) {
        super();
        if (progressBar == null) {
            progressBarRef = null;
        } else {
            progressBarRef = new WeakReference<JProgressBar>(progressBar);
            progressBar.addPropertyChangeListener(this);
        }
        updateKnownUI(progressBar);
        setProgressionColors(colors);
        changeTooltipText = true;
        changeProgressionText = true;
        format = DEFAULT_FORMAT;
    }

    /**
     * Returns the number of progression percent decimal digits.
     * 
     * @return An <code>int</code>. <code>-1</code> if there was a problem calculating them.
     */
    public int getDecimalDigits() {
        int digits;
        String format = this.format;
        if (format == null) {
            digits = -1;
        } else {
            try {
                int index = format.indexOf('.');
                if (index < 0) {
                    digits = -1;
                } else {
                    index++;
                    digits = Integer.parseInt(format.substring(index, format.indexOf('f', index)).trim());
                }
            } catch (Exception e) {
                digits = -1;
            }
        }
        return digits;
    }

    /**
     * Sets the number of progression percent decimal digits.
     * 
     * @param digit The number of progression percent decimal digits to set.
     */
    public void setDecimalDigits(int digit) {
        String format;
        if (digit < 0) {
            format = DEFAULT_FORMAT;
        } else {
            format = new StringBuilder(FORMAT_START).append(digit).append(FORMAT_END).toString();
        }
        this.format = format;
    }

    /**
     * Returns whether this {@link TubeProgressBarColorUpdater} will change the progression text.
     * 
     * @return A <code>boolean</code>. <code>true</code> by default;
     */
    public boolean isChangeProgressionText() {
        return changeProgressionText;
    }

    /**
     * Sets whether this {@link TubeProgressBarColorUpdater} should change the progression text.
     * 
     * @param changeProgressionText Whether this {@link TubeProgressBarColorUpdater} should change the progression text.
     */
    public void setChangeProgressionText(boolean changeProgressionText) {
        this.changeProgressionText = changeProgressionText;
    }

    /**
     * Returns whether this {@link TubeProgressBarColorUpdater} will change the {@link JProgressBar}'s tooltip text.
     * 
     * @return A <code>boolean</code>. <code>true</code> by default;
     */
    public boolean isChangeTooltipText() {
        return changeTooltipText;
    }

    /**
     * Sets whether this {@link TubeProgressBarColorUpdater} should change the {@link JProgressBar}'s tooltip text.
     * 
     * @param changeTooltipText Whether this {@link TubeProgressBarColorUpdater} should change the
     *            {@link JProgressBar}'s tooltip text.
     */
    public void setChangeTooltipText(boolean changeTooltipText) {
        this.changeTooltipText = changeTooltipText;
    }

    /**
     * Returns the progression colors this {@link TubeProgressBarColorUpdater} will use.
     * 
     * @return A {@link Color} array.
     */
    public Color[] getProgressionColors() {
        Color[] progressionColors;
        ProgressionContainer progressionContainer = this.progressionContainer;
        if (progressionContainer == null) {
            progressionColors = null;
        } else {
            Color[] colors = progressionContainer.getColors();
            if (colors == null) {
                progressionColors = null;
            } else {
                progressionColors = colors.clone();
            }
        }
        return progressionColors;
    }

    /**
     * Sets the progression colors this {@link TubeProgressBarColorUpdater} should use.
     * 
     * @param colors The progression colors to use.
     */
    public final void setProgressionColors(Color... colors) {
        Color[] newColors;
        double[] progresses;
        if ((colors == null) || (colors.length < 1)) {
            newColors = new Color[] { TubeProgressBarUI.DEFAULT_PROGRESS_COLOR };
        } else {
            newColors = new Color[colors.length];
            int i = 0;
            for (Color color : colors) {
                if (color == null) {
                    newColors[i++] = TubeProgressBarUI.DEFAULT_PROGRESS_COLOR;
                } else {
                    newColors[i++] = color;
                }
            }
        }
        int length = newColors.length;
        progresses = new double[length];
        double proportion = 100.0d / length;
        for (int i = 0; i < length; i++) {
            progresses[i] = (i + 1) * proportion;
        }
        progressionContainer = new ProgressionContainer(progresses, newColors);
    }

    /**
     * Updates the known {@link TubeProgressBarUI} according to a {@link JProgressBar}.
     * 
     * @param progressBar The {@link JProgressBar}.
     */
    protected final void updateKnownUI(JProgressBar progressBar) {
        if (progressBar == null) {
            tubeProgressBarUIRef = null;
        } else {
            ProgressBarUI ui = progressBar.getUI();
            if (ui instanceof TubeProgressBarUI) {
                tubeProgressBarUIRef = new WeakReference<>((TubeProgressBarUI) ui);
            } else {
                tubeProgressBarUIRef = null;
            }
        }
    }

    @Override
    public void progressionChanged(IProgressable source, int progression, String info) {
        if (progression > -1) {
            JProgressBar progressBar = ObjectUtils.recoverObject(progressBarRef);
            TubeProgressBarUI ui = ObjectUtils.recoverObject(tubeProgressBarUIRef);
            progressBar.setValue(progression);
            if (progressBar != null) {
                if (isChangeProgressionText()) {
                    double percent = progression * 100.0d / progressBar.getMaximum();
                    if (ui != null) {
                        Color progressColor = null;
                        double[] progresses = progressionContainer.getProgresses();
                        Color[] colors = progressionContainer.getColors();
                        if ((progresses != null) && (colors != null)) {
                            int length = Math.min(progresses.length, colors.length);
                            for (int i = 0; i < length; i++) {
                                double progress = progresses[i];
                                if (percent <= progress) {
                                    progressColor = colors[i];
                                    break;
                                }
                            }
                            if ((progressColor != null) && (!progressColor.equals(ui.getProgressColor()))) {
                                ui.setProgressColor(progressColor);
                                progressBar.repaint();
                            }
                        }
                    }
                    String format = this.format;
                    if (format == null) {
                        format = DEFAULT_FORMAT;
                    }
                    progressBar.setString(Format.formatValue(percent, format));
                }
                if (isChangeTooltipText()) {
                    progressBar.setToolTipText(info);
                }
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        JProgressBar progressBar = ObjectUtils.recoverObject(progressBarRef);
        if ((evt != null) && (evt.getSource() == progressBar) && UI.equals(evt.getPropertyName())) {
            updateKnownUI(progressBar);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        JProgressBar progressBar = ObjectUtils.recoverObject(progressBarRef);
        if (progressBar != null) {
            progressBar.removePropertyChangeListener(this);
        }
        super.finalize();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A class to store both progression colors and values.
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class ProgressionContainer {

        private final double[] progresses;
        private final Color[] colors;

        public ProgressionContainer(double[] progresses, Color[] colors) {
            super();
            this.progresses = progresses;
            this.colors = colors;
        }

        public double[] getProgresses() {
            return progresses;
        }

        public Color[] getColors() {
            return colors;
        }

    }
}
