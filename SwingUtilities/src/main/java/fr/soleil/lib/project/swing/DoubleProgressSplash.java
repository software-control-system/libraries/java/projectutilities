/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.Color;

import javax.swing.ImageIcon;

import fr.soleil.lib.project.swing.panel.DoubleProgressSplashPanel;

/**
 * A Splash using JDoubleProgressBar. Based on ATK Splash.
 * 
 * @author Rapha&euml;l GIRARDOT
 */

public class DoubleProgressSplash extends ASplash<JDoubleProgressBar, DoubleProgressSplashPanel> {

    private static final long serialVersionUID = -6031342728599496837L;

    /**
     * Creates and displays a {@link DoubleProgressSplash} using the default ATK splash image.
     */
    public DoubleProgressSplash() {
        this(null, null, true);
    }

    /**
     * Creates a {@link DoubleProgressSplash} using the default ATK splash image.<br />
     * This constructor offers the possibility to not display {@link DoubleProgressSplash} immediately.
     * 
     * @param startVisible Whether to display the {@link DoubleProgressSplash} immediately.
     */
    public DoubleProgressSplash(boolean startVisible) {
        this(null, null, startVisible);
    }

    /**
     * Creates and displays a {@link DoubleProgressSplash} using the given image.
     * 
     * @param splashImage Splash image.
     */
    public DoubleProgressSplash(ImageIcon splashImage) {
        this(splashImage, null, true);
    }

    /**
     * Creates a {@link DoubleProgressSplash} using the given image.<br />
     * This constructor offers the possibility to not display {@link DoubleProgressSplash} immediately.
     * 
     * @param splashImage Splash image.
     * @param startVisible Whether to display the {@link DoubleProgressSplash} immediately.
     */
    public DoubleProgressSplash(ImageIcon splashImage, boolean startVisible) {
        this(splashImage, null, startVisible);
    }

    /**
     * Creates and displays a {@link DoubleProgressSplash} using the given image and text color.<br />
     * The textColor param does not affect the ProgressBar.
     * 
     * @param splashImage Splash image.
     * @param textColor Text color.
     */
    public DoubleProgressSplash(ImageIcon splashImage, Color textColor) {
        this(splashImage, textColor, true);
    }

    /**
     * Creates a {@link DoubleProgressSplash} using the given image and text color.<br />
     * The textColor param does not affect the ProgressBar.<br />
     * This constructor offers the possibility to not display {@link DoubleProgressSplash} immediately.
     * 
     * @param splashImage Splash image.
     * @param textColor Text color.
     * @param startVisible Whether to display the {@link DoubleProgressSplash} immediately.
     */
    public DoubleProgressSplash(ImageIcon splashImage, Color textColor, boolean startVisible) {
        super(splashImage, textColor, null, startVisible);
    }

    @Override
    protected DoubleProgressSplashPanel generateSplashPanel(ImageIcon icon, Color textColor,
            JDoubleProgressBar progressBar) {
        return new DoubleProgressSplashPanel(icon, textColor);
    }

    public void setFirstMessage(String message) {
        splashPanel.setFirstMessage(message);
    }

    public String getFirstMessage() {
        return splashPanel.getFirstMessage();
    }

    public void setSecondMessage(String message) {
        splashPanel.setSecondMessage(message);
    }

    public String getSecondMessage() {
        return splashPanel.getSecondMessage();
    }

    public JDoubleProgressBar getDoubleProgressBar() {
        return splashPanel.getDoubleProgressBar();
    }

    @Override
    public void initProgress() {
        splashPanel.setBGProgress(0);
        splashPanel.setFGProgress(0);
        revalidate();
        pack();
        setLocationRelativeTo(null);
        if (isShowing()) {
            repaint();
        }
    }

    public void setBGMaxProgress(int i) {
        splashPanel.getDoubleProgressBar().setBGMaximum(i);
    }

    public void setFGMaxProgress(int i) {
        splashPanel.getDoubleProgressBar().setFGMaximum(i);
    }

    public void setBGProgress(int i) {
        splashPanel.setBGProgress(i);
    }

    public void setFGProgress(int i) {
        splashPanel.setFGProgress(i);
    }

    public DoubleProgressSplashPanel getSplashPanel() {
        return splashPanel;
    }

    public static void main(String[] args) {
        DoubleProgressSplash s = new DoubleProgressSplash();
        s.setTitle("DoubleProgressSplashScreen");
        s.setFirstMessage("First message line");
        s.setSecondMessage("Second message line");
        s.initProgress();
        s.setFGMaxProgress(100);
        s.setBGMaxProgress(100);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        for (int i = 0; i <= 100; i++) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
            }
            for (int j = 0; j <= 100; j++) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                }
                s.setBGProgress(j);
            }
            s.setFGProgress(i);
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        System.exit(0);
    } // end of main ()

}
