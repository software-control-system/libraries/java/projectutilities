/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.SwingUtilitiesConstants;

/**
 * This class is a {@link JTextField} that offers the possibility to display a special text (no value, error) depending
 * on the state it is in, and changes background color to indicate a user modification.
 * 
 * @author Rapha&euml;l GIRARDOT, MAINGUY
 */
public abstract class AStateFocusTextField extends JTextField implements DocumentListener, SwingUtilitiesConstants {

    private static final long serialVersionUID = 5725465577117057608L;

    /**
     * State change event property name.
     */
    public static final String STATE = "State";

    /**
     * Small font size.
     */
    public static final int SMALL_FONT_SIZE = 11;
    /**
     * Medium font size.
     */
    public static final int MEDIUM_FONT_SIZE = 12;

    /**
     * Default background for User state.
     */
    public static final Color DEFAULT_USER_BACKGROUND = new Color(255, 255, 175);
    /**
     * Default background for Valid state.
     */
    public static final Color DEFAULT_VALID_BACKGROUND = Color.WHITE;
    /**
     * Default background for Error and NoValue states.
     */
    public static final Color DEFAULT_ERROR_BACKGROUND = new Color(255, 220, 150);

    /**
     * Default {@link Font} for Valid and User states.
     */
    public static final Font DEFAULT_VALID_FONT = new Font(Font.DIALOG, Font.PLAIN, SMALL_FONT_SIZE);
    /**
     * Default {@link Font} for Error state.
     */
    public static final Font DEFAULT_ERROR_FONT = new Font(Font.DIALOG, Font.ITALIC | Font.BOLD, MEDIUM_FONT_SIZE);
    /**
     * Default {@link Font} for NoValue state.
     */
    public static final Font DEFAULT_NO_VALUE_FONT = new Font(Font.DIALOG, Font.ITALIC, MEDIUM_FONT_SIZE);

    /**
     * Current state.
     */
    protected State currentState;

    // avoid firing an insert document event
    protected volatile boolean activateDocListener;

    // NoValue state
    protected Color noValueBackground;
    protected Font noValueFont;
    protected String noValueText;

    // Valid state
    protected Color validBackground;
    protected Font validFont;

    // User state
    protected Color userBackground;
    protected Font userFont;

    // Error state
    protected Color errorBackground;
    protected Font errorFont;
    protected String errorText;

    protected MessageManager messageManager;

    public AStateFocusTextField(MessageManager messageManager) {
        super();
        initialize(messageManager);
    }

    public AStateFocusTextField(Document doc, String text, int columns, MessageManager messageManager) {
        super(doc, text, columns);
        initialize(messageManager);
    }

    public AStateFocusTextField(int columns, MessageManager messageManager) {
        super(columns);
        initialize(messageManager);
    }

    public AStateFocusTextField(String text, int columns, MessageManager messageManager) {
        super(text, columns);
        initialize(messageManager);
    }

    public AStateFocusTextField(String text, MessageManager messageManager) {
        super(text);
        initialize(messageManager);
    }

    protected void initialize(MessageManager messageManager) {
        this.messageManager = messageManager == null ? DEFAULT_MESSAGE_MANAGER : messageManager;
        currentState = State.NoValue;
        activateDocListener = true;
        noValueBackground = DEFAULT_ERROR_BACKGROUND;
        noValueFont = DEFAULT_NO_VALUE_FONT;
        noValueText = this.messageManager.getMessage("fr.soleil.lib.project.swing.data.value.no");
        validBackground = DEFAULT_VALID_BACKGROUND;
        validFont = DEFAULT_VALID_FONT;
        userBackground = DEFAULT_USER_BACKGROUND;
        userFont = DEFAULT_VALID_FONT;
        errorBackground = DEFAULT_ERROR_BACKGROUND;
        errorFont = DEFAULT_ERROR_FONT;
        errorText = this.messageManager.getMessage("fr.soleil.lib.project.swing.error");
        // addFocusListener(this);
        setState(State.NoValue, true);
        getDocument().addDocumentListener(this);
    }

    @Override
    public void setDocument(Document doc) {
        Document document = getDocument();
        if (document != null) {
            document.removeDocumentListener(this);
        }
        super.setDocument(doc);
        if (doc != null) {
            doc.addDocumentListener(this);
        }
    }

    /**
     * Changes the state. If it is a "message" state, it sets the message.
     * 
     * @param newState The {@link State} to set.
     * @param setValue Whether to set adapted text too.
     */
    protected void setState(State newState, boolean setValue) {
        if (newState != null) {
            State oldState = currentState;
            currentState = newState;

            switch (newState) {
                case NoValue:
                    setBackground(noValueBackground);
                    setFont(noValueFont);
                    if (setValue) {
                        setStringValue(noValueText);
                    }
                    break;

                case Valid:
                    setBackground(validBackground);
                    setFont(validFont);
                    break;

                case User:
                    setBackground(userBackground);
                    setFont(userFont);
                    break;

                case Error:
                    setBackground(errorBackground);
                    setFont(errorFont);
                    if (setValue) {
                        setStringValue(errorText);
                    }
                    break;
            }
            firePropertyChange(STATE, oldState, newState);
        }
    }

    protected void beforeSetStringValue() {
        // nothing to do by default
    }

    protected void afterSetStringValue() {
        // nothing to do by default
    }

    /**
     * Sets a text that typically doesn't match the desired format
     * 
     * @param text the text to set
     */
    protected synchronized void setStringValue(String text) {
        beforeSetStringValue();
        activateDocListener = false;
        setText(text);
        afterSetStringValue();
        activateDocListener = true;
    }

    /**
     * Sets a valid value
     * 
     * @param t the valid value
     */
    protected synchronized void setValidValue(String t) {
        setState(State.Valid, true);
        // disable insert/remove update changes when setting a value
        // we only want to listen to user modifications in order to change the state
        activateDocListener = false;
        setText(t);
        activateDocListener = true;
    }

    /**
     * Sets a User state without change Text
     */
    public void setUserState() {
        if (isEditable()) {
            activateDocListener = false;
            setState(State.User, false);
        }
    }

    /**
     * Sets a Valid state without change Text
     */
    public void setValidState() {
        if (isEditable()) {
            activateDocListener = false;
            setState(State.Valid, false);
        }
    }

    /**
     * Sets a Error state without change Text
     */
    public void setErrorState() {
        if (isEditable()) {
            activateDocListener = false;
            setState(State.Error, false);
        }
    }

    public void clear() {
        setState(State.NoValue, true);
    }

    public void setError() {
        setState(State.Error, true);
    }

    public boolean isErrorOrNoValue() {
        return isErrorOrNoValue(false);
    }

    public boolean isErrorOrNoValue(boolean ignoreEmptyText) {
        boolean isError;
        if (State.Valid.equals(currentState) && (!ignoreEmptyText)) {
            if (getText().trim().isEmpty()) {
                isError = true;
            } else {
                isError = false;
            }
        } else {
            isError = (State.Error.equals(currentState) || State.NoValue.equals(currentState));
        }
        return isError;
    }

    /**
     * Returns whether this {@link AStateFocusTextField} is being edited.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isEditing() {
        return (currentState == State.User);
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        documentUpdate();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        documentUpdate();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        // nothing
    }

    /**
     * Updates state on document change
     */
    protected void documentUpdate() {
        if (activateDocListener) {
            if (currentState != State.User) {
                setState(State.User, true);
            }
        }
    }

    /**
     * Returns the background {@link Color} used when there is no value
     * 
     * @return A {@link Color}
     */
    public Color getNoValueBackground() {
        return noValueBackground;
    }

    /**
     * Sets the background {@link Color} to use when there is no value
     * 
     * @param noValueBackground The {@link Color} to use
     */
    public void setNoValueBackground(Color noValueBackground) {
        if (noValueBackground == null) {
            noValueBackground = DEFAULT_ERROR_BACKGROUND;
        }
        this.noValueBackground = noValueBackground;
    }

    /**
     * Returns the background {@link Color} used when the text is valid
     * 
     * @return A {@link Color}
     */
    protected Color getValidBackground() {
        return validBackground;
    }

    /**
     * Sets the background {@link Color} to use when the text is valid
     * 
     * @param numberBackground The {@link Color} to use
     */
    protected void setValidBackground(Color validBackground) {
        if (validBackground == null) {
            validBackground = DEFAULT_VALID_BACKGROUND;
        }
        this.validBackground = validBackground;
    }

    /**
     * Returns the background {@link Color} used when user is editing text
     * 
     * @return A {@link Color}
     */
    public Color getUserBackground() {
        return userBackground;
    }

    /**
     * Sets the background {@link Color} to use when user is editing text
     * 
     * @param userBackground The {@link Color} to use
     */
    public void setUserBackground(Color userBackground) {
        if (userBackground == null) {
            userBackground = DEFAULT_USER_BACKGROUND;
        }
        this.userBackground = userBackground;
    }

    /**
     * Returns the background {@link Color} used when an error text is displayed
     * 
     * @return A {@link Color}
     */
    public Color getErrorBackground() {
        return errorBackground;
    }

    /**
     * Sets the background {@link Color} to use when an error text is displayed
     * 
     * @param errorBackground The {@link Color} to use
     */
    public void setErrorBackground(Color errorBackground) {
        if (errorBackground == null) {
            errorBackground = DEFAULT_ERROR_BACKGROUND;
        }
        this.errorBackground = errorBackground;
    }

    /**
     * Returns the font used in case of no value.
     * 
     * @return A {@link Font}
     */
    public Font getNoValueFont() {
        return noValueFont;
    }

    /**
     * Sets the font to use in case of no value.
     * 
     * @param noValueFont The font to use in case of no value.
     */
    public void setNoValueFont(Font noValueFont) {
        this.noValueFont = noValueFont == null ? DEFAULT_NO_VALUE_FONT : noValueFont;
    }

    /**
     * Returns the text used in case of no value.
     * 
     * @return A {@link String}.
     */
    public String getNoValueText() {
        return noValueText;
    }

    /**
     * Sets the text to use in case of no value.
     * 
     * @param noValueText The text to use in case of no value.
     */
    public void setNoValueText(String noValueText) {
        this.noValueText = noValueText == null ? messageManager.getMessage("fr.soleil.lib.project.swing.data.value.no")
                : noValueText;
    }

    /**
     * Returns the font used in case of valid value.
     * 
     * @return A {@link Font}.
     */
    protected Font getValidFont() {
        return validFont;
    }

    /**
     * Sets the font to use in case of valid value.
     * 
     * @param validFont The font to use in case of valid value.
     */
    protected void setValidFont(Font validFont) {
        this.validFont = validFont == null ? DEFAULT_VALID_FONT : validFont;
    }

    /**
     * Returns the font used in case of change done by user.
     * 
     * @return A {@link Font}.
     */
    public Font getUserFont() {
        return userFont;
    }

    /**
     * Sets the font to use in case of change done by user.
     * 
     * @param userFont The font to use in case of change done by user.
     */
    public void setUserFont(Font userFont) {
        this.userFont = userFont == null ? DEFAULT_VALID_FONT : userFont;
    }

    /**
     * Returns the font used in case of error.
     * 
     * @return A {@link Font}.
     */
    public Font getErrorFont() {
        return errorFont;
    }

    /**
     * Sets the font to use in case of error.
     * 
     * @param errorFont The font to use in case of error.
     */
    public void setErrorFont(Font errorFont) {
        this.errorFont = errorFont == null ? DEFAULT_ERROR_FONT : errorFont;
    }

    /**
     * Returns the text used in case of error.
     * 
     * @return A {@link String}.
     */
    public String getErrorText() {
        return errorText;
    }

    /**
     * Sets the text to use in case of error.
     * 
     * @param errorText The text to use in case of error.
     */
    public void setErrorText(String errorText) {
        this.errorText = errorText == null ? messageManager.getMessage("fr.soleil.lib.project.swing.error") : errorText;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * An {@link Enum} that represents the potential states of an {@link AStateFocusTextField}.
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    public static enum State {
        NoValue, Valid, User, Error;
    }

}
