/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JSlider;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicSliderUI;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.MetalSliderUI;
import javax.swing.plaf.metal.OceanTheme;

/**
 * A {@link BasicSliderUI} for which you can change ticks colors. This class
 * code is based on {@link MetalSliderUI}. However, this class does not extend {@link MetalSliderUI} due to some fields
 * accessibility problems.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ColoredTickMetalSliderUI extends BasicSliderUI {

    protected final int TICK_BUFFER = 4;
    protected boolean filledSlider = false;
    // NOTE: these next five variables are currently unused.
    // safeLength was private in MetalSliderUI
    protected int safeLength;

    /**
     * A default horizontal thumb <code>Icon</code>. This field might not be
     * used. To change the <code>Icon</code> used by this delgate directly set
     * it using the <code>Slider.horizontalThumbIcon</code> UIManager property.
     */
    protected static Icon horizThumbIcon;

    /**
     * A default vertical thumb <code>Icon</code>. This field might not be used.
     * To change the <code>Icon</code> used by this delgate directly set it
     * using the <code>Slider.verticalThumbIcon</code> UIManager property.
     */
    protected static Icon vertThumbIcon;

    private static Icon SAFE_HORIZ_THUMB_ICON;
    private static Icon SAFE_VERT_THUMB_ICON;

    protected final String SLIDER_FILL = "JSlider.isFilled";

    private Color customMinorTickColor;
    private Color customMajorTickColor;

    public ColoredTickMetalSliderUI() {
        super(null);
        customMinorTickColor = null;
        customMajorTickColor = null;
    }

    /**
     * Returns the custom minor tick color to use
     * 
     * @return A {@link Color}. Can be <code>null</code>
     */
    public Color getCustomMinorTickColor() {
        return customMinorTickColor;
    }

    /**
     * Sets the custom minor tick color to use
     * 
     * @param minorTickColor
     *            The {@link Color} to use. If <code>null</code>, minor ticks
     *            will be drawn the default way
     */
    public void setCustomMinorTickColor(Color minorTickColor) {
        this.customMinorTickColor = minorTickColor;
    }

    /**
     * Returns the custom major tick color to use
     * 
     * @return A {@link Color}. Can be <code>null</code>
     */
    public Color getCustomMajorTickColor() {
        return customMajorTickColor;
    }

    /**
     * Sets the custom major tick color to use
     * 
     * @param majorTickColor
     *            The {@link Color} to use. If <code>null</code>, major ticks
     *            will be drawn the default way
     */
    public void setCustomMajorTickColor(Color majorTickColor) {
        this.customMajorTickColor = majorTickColor;
    }

    private static Icon getHorizThumbIcon() {
        Icon icon;
        if (System.getSecurityManager() == null) {
            icon = horizThumbIcon;
        } else {
            icon = SAFE_HORIZ_THUMB_ICON;
        }
        return icon;
    }

    private static Icon getVertThumbIcon() {
        Icon icon;
        if (System.getSecurityManager() == null) {
            icon = vertThumbIcon;
        } else {
            icon = SAFE_VERT_THUMB_ICON;
        }
        return icon;
    }

    @Override
    public void installUI(JComponent c) {
        safeLength = getIntFromUIManager("Slider.majorTickLength", 6);
        horizThumbIcon = SAFE_HORIZ_THUMB_ICON = UIManager.getIcon("Slider.horizontalThumbIcon");
        vertThumbIcon = SAFE_VERT_THUMB_ICON = UIManager.getIcon("Slider.verticalThumbIcon");
        super.installUI(c);
        scrollListener.setScrollByBlock(false);
        Object sliderFillProp = c.getClientProperty(SLIDER_FILL);
        if (sliderFillProp != null) {
            filledSlider = ((Boolean) sliderFillProp).booleanValue();
        }
    }

    protected int getIntFromUIManager(String id, int defaultValue) {
        int result;
        if ((id == null) || id.trim().isEmpty()) {
            result = defaultValue;
        } else {
            Integer tmp = (Integer) UIManager.get(id);
            if (tmp == null) {
                result = defaultValue;
            } else {
                result = tmp.intValue();
            }
        }
        return result;
    }

    protected Color getColorFromUIManager(String id, Color defaultValue) {
        Color result;
        if ((id == null) || id.trim().isEmpty()) {
            result = defaultValue;
        } else {
            Color tmp = (Color) UIManager.get(id);
            if (tmp == null) {
                result = defaultValue;
            } else {
                result = tmp;
            }
        }
        return result;
    }

    @Override
    protected PropertyChangeListener createPropertyChangeListener(JSlider slider) {
        return new MetalPropertyListener();
    }

    protected class MetalPropertyListener extends BasicSliderUI.PropertyChangeHandler {
        @Override
        public void propertyChange(PropertyChangeEvent e) { // listen for slider fill
            super.propertyChange(e);
            String name = e.getPropertyName();
            if (name.equals(SLIDER_FILL)) {
                if (e.getNewValue() != null) {
                    filledSlider = ((Boolean) e.getNewValue()).booleanValue();
                } else {
                    filledSlider = false;
                }
            }
        }
    }

    @Override
    public void paintThumb(Graphics g) {
        Rectangle knobBounds = thumbRect;
        g.translate(knobBounds.x, knobBounds.y);
        JSlider slider = this.slider;
        if (slider != null) {
            Icon icon;
            if (slider.getOrientation() == JSlider.HORIZONTAL) {
                icon = getHorizThumbIcon();
            } else {
                icon = getVertThumbIcon();
            }
            if (icon != null) {
                icon.paintIcon(slider, g, 0, 0);
            }
        }
        g.translate(-knobBounds.x, -knobBounds.y);
    }

    /**
     * If <code>chooseFirst</code>is true, <code>c1</code> is returned,
     * otherwise <code>c2</code>.
     */
    private Color chooseColor(boolean chooseFirst, Color c1, Color c2) {
        if (chooseFirst) {
            return c2;
        }
        return c1;
    }

    /**
     * Returns a rectangle enclosing the track that will be painted.
     */
    private Rectangle getPaintTrackRect() {
        int trackLeft = 0, trackRight = 0, trackTop = 0, trackBottom = 0;
        if (slider.getOrientation() == JSlider.HORIZONTAL) {
            trackBottom = (trackRect.height - 1) - getThumbOverhang();
            trackTop = trackBottom - (getTrackWidth() - 1);
            trackRight = trackRect.width - 1;
        } else {
            if (slider.getComponentOrientation().isLeftToRight()) {
                trackLeft = (trackRect.width - getThumbOverhang()) - getTrackWidth();
                trackRight = (trackRect.width - getThumbOverhang()) - 1;
            } else {
                trackLeft = getThumbOverhang();
                trackRight = getThumbOverhang() + getTrackWidth() - 1;
            }
            trackBottom = trackRect.height - 1;
        }
        return new Rectangle(trackRect.x + trackLeft, trackRect.y + trackTop, trackRight - trackLeft,
                trackBottom - trackTop);
    }

    @Override
    public void paintTrack(Graphics g) {
        if (MetalLookAndFeel.getCurrentTheme() instanceof OceanTheme) {
            oceanPaintTrack(g);
            return;
        }
        Color trackColor = !slider.isEnabled() ? MetalLookAndFeel.getControlShadow() : slider.getForeground();

        boolean leftToRight = slider.getComponentOrientation().isLeftToRight();

        g.translate(trackRect.x, trackRect.y);

        int trackLeft = 0;
        int trackTop = 0;
        int trackRight = 0;
        int trackBottom = 0;

        // Draw the track
        if (slider.getOrientation() == JSlider.HORIZONTAL) {
            trackBottom = (trackRect.height - 1) - getThumbOverhang();
            trackTop = trackBottom - (getTrackWidth() - 1);
            trackRight = trackRect.width - 1;
        } else {
            if (leftToRight) {
                trackLeft = (trackRect.width - getThumbOverhang()) - getTrackWidth();
                trackRight = (trackRect.width - getThumbOverhang()) - 1;
            } else {
                trackLeft = getThumbOverhang();
                trackRight = getThumbOverhang() + getTrackWidth() - 1;
            }
            trackBottom = trackRect.height - 1;
        }

        if (slider.isEnabled()) {
            g.setColor(MetalLookAndFeel.getControlDarkShadow());
            g.drawRect(trackLeft, trackTop, (trackRight - trackLeft) - 1, (trackBottom - trackTop) - 1);

            g.setColor(MetalLookAndFeel.getControlHighlight());
            g.drawLine(trackLeft + 1, trackBottom, trackRight, trackBottom);
            g.drawLine(trackRight, trackTop + 1, trackRight, trackBottom);

            g.setColor(trackColor);
            g.drawLine(trackLeft + 1, trackTop + 1, trackRight - 2, trackTop + 1);
            g.drawLine(trackLeft + 1, trackTop + 1, trackLeft + 1, trackBottom - 2);
        } else {
            g.setColor(trackColor);
            g.drawRect(trackLeft, trackTop, (trackRight - trackLeft) - 1, (trackBottom - trackTop) - 1);
        }

        // Draw the fill
        if (filledSlider) {
            int middleOfThumb = 0;
            int fillTop = 0;
            int fillLeft = 0;
            int fillBottom = 0;
            int fillRight = 0;

            if (slider.getOrientation() == JSlider.HORIZONTAL) {
                middleOfThumb = thumbRect.x + (thumbRect.width / 2);
                middleOfThumb -= trackRect.x; // To compensate for the
                                              // g.translate()
                fillTop = !slider.isEnabled() ? trackTop : trackTop + 1;
                fillBottom = !slider.isEnabled() ? trackBottom - 1 : trackBottom - 2;

                if (!drawInverted()) {
                    fillLeft = !slider.isEnabled() ? trackLeft : trackLeft + 1;
                    fillRight = middleOfThumb;
                } else {
                    fillLeft = middleOfThumb;
                    fillRight = !slider.isEnabled() ? trackRight - 1 : trackRight - 2;
                }
            } else {
                middleOfThumb = thumbRect.y + (thumbRect.height / 2);
                middleOfThumb -= trackRect.y; // To compensate for the
                                              // g.translate()
                fillLeft = !slider.isEnabled() ? trackLeft : trackLeft + 1;
                fillRight = !slider.isEnabled() ? trackRight - 1 : trackRight - 2;

                if (!drawInverted()) {
                    fillTop = middleOfThumb;
                    fillBottom = !slider.isEnabled() ? trackBottom - 1 : trackBottom - 2;
                } else {
                    fillTop = !slider.isEnabled() ? trackTop : trackTop + 1;
                    fillBottom = middleOfThumb;
                }
            }

            if (slider.isEnabled()) {
                g.setColor(slider.getBackground());
                g.drawLine(fillLeft, fillTop, fillRight, fillTop);
                g.drawLine(fillLeft, fillTop, fillLeft, fillBottom);

                g.setColor(trackColor);
                g.fillRect(fillLeft + 1, fillTop + 1, fillRight - fillLeft, fillBottom - fillTop);
            } else {
                g.setColor(trackColor);
                g.fillRect(fillLeft, fillTop, fillRight - fillLeft, trackBottom - trackTop);
            }
        }

        g.translate(-trackRect.x, -trackRect.y);
    }

    private void oceanPaintTrack(Graphics g) {
        boolean leftToRight = slider.getComponentOrientation().isLeftToRight();
        boolean drawInverted = drawInverted();
        Color sliderAltTrackColor = (Color) UIManager.get("Slider.altTrackColor");

        // Translate to the origin of the painting rectangle
        Rectangle paintRect = getPaintTrackRect();
        g.translate(paintRect.x, paintRect.y);

        // Width and height of the painting rectangle.
        int w = paintRect.width;
        int h = paintRect.height;

        if (!slider.isEnabled()) {
            g.setColor(MetalLookAndFeel.getControlShadow());
            g.drawRect(0, 0, w - 1, h - 1);
        } else if (slider.getOrientation() == JSlider.HORIZONTAL) {
            int middleOfThumb = thumbRect.x + (thumbRect.width / 2) - paintRect.x;
            int fillMinX;
            int fillMaxX;

            if (middleOfThumb > 0) {
                g.setColor(chooseColor(drawInverted, MetalLookAndFeel.getPrimaryControlDarkShadow(),
                        MetalLookAndFeel.getControlDarkShadow()));
                g.drawRect(0, 0, middleOfThumb - 1, h - 1);
            }
            if (middleOfThumb < w) {
                g.setColor(chooseColor(drawInverted, MetalLookAndFeel.getControlDarkShadow(),
                        MetalLookAndFeel.getPrimaryControlDarkShadow()));
                g.drawRect(middleOfThumb, 0, w - middleOfThumb - 1, h - 1);
            }
            g.setColor(MetalLookAndFeel.getPrimaryControlShadow());
            if (drawInverted) {
                fillMinX = middleOfThumb;
                fillMaxX = w - 2;
                g.drawLine(1, 1, middleOfThumb, 1);
            } else {
                fillMinX = 1;
                fillMaxX = middleOfThumb;
                g.drawLine(middleOfThumb, 1, w - 1, 1);
            }
            if (h == 6) {
                g.setColor(MetalLookAndFeel.getWhite());
                g.drawLine(fillMinX, 1, fillMaxX, 1);
                g.setColor(sliderAltTrackColor);
                g.drawLine(fillMinX, 2, fillMaxX, 2);
                g.setColor(MetalLookAndFeel.getControlShadow());
                g.drawLine(fillMinX, 3, fillMaxX, 3);
                g.setColor(MetalLookAndFeel.getPrimaryControlShadow());
                g.drawLine(fillMinX, 4, fillMaxX, 4);
            }
        } else {
            int middleOfThumb = thumbRect.y + (thumbRect.height / 2) - paintRect.y;
            int fillMinY;
            int fillMaxY;

            if (middleOfThumb > 0) {
                g.setColor(chooseColor(drawInverted, MetalLookAndFeel.getControlDarkShadow(),
                        MetalLookAndFeel.getPrimaryControlDarkShadow()));
                g.drawRect(0, 0, w - 1, middleOfThumb - 1);
            }
            if (middleOfThumb < h) {
                g.setColor(chooseColor(drawInverted, MetalLookAndFeel.getPrimaryControlDarkShadow(),
                        MetalLookAndFeel.getControlDarkShadow()));
                g.drawRect(0, middleOfThumb, w - 1, h - middleOfThumb - 1);
            }
            g.setColor(MetalLookAndFeel.getPrimaryControlShadow());
            if (drawInverted()) {
                fillMinY = 1;
                fillMaxY = middleOfThumb;
                if (leftToRight) {
                    g.drawLine(1, middleOfThumb, 1, h - 1);
                } else {
                    g.drawLine(w - 2, middleOfThumb, w - 2, h - 1);
                }
            } else {
                fillMinY = middleOfThumb;
                fillMaxY = h - 2;
                if (leftToRight) {
                    g.drawLine(1, 1, 1, middleOfThumb);
                } else {
                    g.drawLine(w - 2, 1, w - 2, middleOfThumb);
                }
            }
            if (w == 6) {
                g.setColor(chooseColor(!leftToRight, MetalLookAndFeel.getWhite(),
                        MetalLookAndFeel.getPrimaryControlShadow()));
                g.drawLine(1, fillMinY, 1, fillMaxY);
                g.setColor(chooseColor(!leftToRight, sliderAltTrackColor, MetalLookAndFeel.getControlShadow()));
                g.drawLine(2, fillMinY, 2, fillMaxY);
                g.setColor(chooseColor(!leftToRight, MetalLookAndFeel.getControlShadow(), sliderAltTrackColor));
                g.drawLine(3, fillMinY, 3, fillMaxY);
                g.setColor(chooseColor(!leftToRight, MetalLookAndFeel.getPrimaryControlShadow(),
                        MetalLookAndFeel.getWhite()));
                g.drawLine(4, fillMinY, 4, fillMaxY);
            }
        }

        g.translate(-paintRect.x, -paintRect.y);
    }

    @Override
    public void paintFocus(Graphics g) {
    }

    @Override
    protected Dimension getThumbSize() {
        Dimension size = new Dimension();
        Icon vertThumbIcon = getVertThumbIcon();
        Icon horizThumbIcon = getHorizThumbIcon();

        if (horizThumbIcon != null && vertThumbIcon != null) {
            if (slider.getOrientation() == JSlider.VERTICAL) {
                size.width = getVertThumbIcon().getIconWidth();
                size.height = getVertThumbIcon().getIconHeight();
            } else {
                size.width = getHorizThumbIcon().getIconWidth();
                size.height = getHorizThumbIcon().getIconHeight();
            }
        } else {
            // If one of the two icons is not present
            // return the following values wich are consistent to each other
            size.width = 15;
            size.height = 16;
        }

        return size;
    }

    /**
     * Gets the height of the tick area for horizontal sliders and the width of
     * the tick area for vertical sliders. BasicSliderUI uses the returned value
     * to determine the tick area rectangle.
     */
    @Override
    public int getTickLength() {
        return slider.getOrientation() == JSlider.HORIZONTAL ? safeLength + TICK_BUFFER + 1
                : safeLength + TICK_BUFFER + 3;
    }

    /**
     * Returns the shorter dimension of the track.
     */
    protected int getTrackWidth() {
        // This strange calculation is here to keep the
        // track in proportion to the thumb.
        final double kIdealTrackWidth = 7.0;
        final double kIdealThumbHeight = 16.0;
        final double kWidthScalar = kIdealTrackWidth / kIdealThumbHeight;

        if (slider.getOrientation() == JSlider.HORIZONTAL) {
            return (int) (kWidthScalar * thumbRect.height);
        } else {
            return (int) (kWidthScalar * thumbRect.width);
        }
    }

    /**
     * Returns the longer dimension of the slide bar. (The slide bar is only the
     * part that runs directly under the thumb)
     */
    protected int getTrackLength() {
        if (slider.getOrientation() == JSlider.HORIZONTAL) {
            return trackRect.width;
        }
        return trackRect.height;
    }

    /**
     * Returns the amount that the thumb goes past the slide bar.
     */
    protected int getThumbOverhang() {
        return (int) (getThumbSize().getHeight() - getTrackWidth()) / 2;
    }

    @Override
    protected void scrollDueToClickInTrack(int dir) {
        scrollByUnit(dir);
    }

    @Override
    protected void paintMinorTickForHorizSlider(Graphics g, Rectangle tickBounds, int x) {
        if (customMinorTickColor == null) {
            g.setColor(slider.isEnabled() ? slider.getForeground() : MetalLookAndFeel.getControlShadow());
        } else {
            g.setColor(customMinorTickColor);
        }
        g.drawLine(x, TICK_BUFFER, x, TICK_BUFFER + (safeLength / 2));
    }

    @Override
    protected void paintMajorTickForHorizSlider(Graphics g, Rectangle tickBounds, int x) {
        if (customMajorTickColor == null) {
            g.setColor(slider.isEnabled() ? slider.getForeground() : MetalLookAndFeel.getControlShadow());
        } else {
            g.setColor(customMajorTickColor);
        }
        g.drawLine(x, TICK_BUFFER, x, TICK_BUFFER + (safeLength - 1));
    }

    @Override
    protected void paintMinorTickForVertSlider(Graphics g, Rectangle tickBounds, int y) {
        if (customMinorTickColor == null) {
            g.setColor(slider.isEnabled() ? slider.getForeground() : MetalLookAndFeel.getControlShadow());
        } else {
            g.setColor(customMinorTickColor);
        }
        if (slider.getComponentOrientation().isLeftToRight()) {
            g.drawLine(TICK_BUFFER, y, TICK_BUFFER + (safeLength / 2), y);
        } else {
            g.drawLine(0, y, safeLength / 2, y);
        }
    }

    @Override
    protected void paintMajorTickForVertSlider(Graphics g, Rectangle tickBounds, int y) {
        if (customMajorTickColor == null) {
            g.setColor(slider.isEnabled() ? slider.getForeground() : MetalLookAndFeel.getControlShadow());
        } else {
            g.setColor(customMajorTickColor);
        }
        if (slider.getComponentOrientation().isLeftToRight()) {
            g.drawLine(TICK_BUFFER, y, TICK_BUFFER + safeLength, y);
        } else {
            g.drawLine(0, y, safeLength, y);
        }
    }
}
