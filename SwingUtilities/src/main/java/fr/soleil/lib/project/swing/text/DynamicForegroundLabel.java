/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import fr.soleil.lib.project.awt.ColorUtils;

/**
 * A {@link JSmoothLabel} that is able to adapt its foreground color when its background color changes
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DynamicForegroundLabel extends JSmoothLabel {

    private static final long serialVersionUID = 2642478170041611520L;

    protected Color fg;

    public DynamicForegroundLabel() {
        super();
    }

    public DynamicForegroundLabel(String text) {
        super(text);
    }

    public DynamicForegroundLabel(Icon image) {
        super(image);
    }

    public DynamicForegroundLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
    }

    public DynamicForegroundLabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
    }

    public DynamicForegroundLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
    }

    @Override
    protected AntiAliasingDelegate generateAntiAliasingDelegate() {
        return new AntiAliasingDelegate(false, this);
    }

    @Override
    protected void init() {
        super.init();
        fg = null;
    }

    protected Color getAutoForeground(Color bg) {
        return ColorUtils.getBestForeground(bg);
    }

    protected void updateForeground() {
        if (fg == null) {
            if (isOpaque()) {
                super.setForeground(getAutoForeground(getBackground()));
            } else {
                super.setForeground(Color.BLACK);
            }
        } else {
            super.setForeground(fg);
        }
    }

    @Override
    public void setForeground(Color fg) {
        this.fg = fg;
        updateForeground();
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        updateForeground();
    }

    @Override
    public void setOpaque(boolean isOpaque) {
        super.setOpaque(isOpaque);
        updateForeground();
    }

    @Override
    protected void finalize() {
        super.finalize();
        fg = null;
    }

    public static void main(String[] args) {
        JFrame testFrame = new JFrame(DynamicForegroundLabel.class.getSimpleName() + " test");
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        String[] colors = { "WHITE", "LIGHT_GRAY", "GRAY", "DARK_GRAY", "BLACK", "RED", "PINK", "ORANGE", "YELLOW",
                "GREEN", "MAGENTA", "CYAN", "BLUE" };
        int cols = (int) Math.round(Math.sqrt(colors.length));
        int rows = (int) Math.ceil(colors.length / (double) cols);
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        panel.setLayout(new GridLayout(rows, cols, 5, 5));
        for (String color : colors) {
            DynamicForegroundLabel label = new DynamicForegroundLabel(color);
            label.setOpaque(true);
            try {
                label.setBackground((Color) Color.class.getDeclaredField(color).get(Color.class));
            } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
                // ignore
            }
            panel.add(label);
        }
        testFrame.setContentPane(panel);
        testFrame.pack();
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }
}
