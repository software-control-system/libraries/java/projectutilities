/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.border;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;

import javax.swing.border.LineBorder;

/**
 * A dashed {@link LineBorder}. Dashes are only compatible with {@link Graphics2D}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DashedBorder extends LineBorder {

    private static final long serialVersionUID = 8657609982941933580L;

    protected Stroke dashedStroke;
    protected float dashLength;

    /**
     * Constructor
     * 
     * @param color the color of the border
     * @param dashLength the dash length
     */
    public DashedBorder(Color color, float dashLength) {
        super(color);
        initStroke(dashLength);
    }

    /**
     * Constructor
     * 
     * @param color the color of the border
     * @param thickness the thickness of the border
     * @param dashLength the dash length
     */
    public DashedBorder(Color color, int thickness, float dashLength) {
        super(color, thickness);
        initStroke(dashLength);
    }

    /**
     * Constructor
     * 
     * @param color the color of the border
     * @param thickness the thickness of the border
     * @param roundedCorners whether or not border corners should be round
     * @param dashLength the dash length
     */
    public DashedBorder(Color color, int thickness, boolean roundedCorners, float dashLength) {
        super(color, thickness, roundedCorners);
        initStroke(dashLength);
    }

    protected void initStroke(float dashLength) {
        dashedStroke = new BasicStroke(thickness, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
                (dashLength < 1 ? 1 : dashLength), new float[] { dashLength }, 0);
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        if (g instanceof Graphics2D) {
            Graphics2D g2d = (Graphics2D) g;
            Stroke formerStroke = g2d.getStroke();
            Color formerColor = g2d.getColor();
            g2d.setColor(lineColor);
            g2d.setStroke(dashedStroke);
            if (roundedCorners) {
                g2d.drawRoundRect(x + thickness / 2, y + thickness / 2, width - thickness, height - thickness,
                        thickness, thickness);
            } else {
                g2d.drawRect(x + thickness / 2, y + thickness / 2, width - thickness, height - thickness);
            }
            g2d.setColor(formerColor);
            g2d.setStroke(formerStroke);
        } else {
            super.paintBorder(c, g, x, y, width, height);
        }
    }

}
