/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.geom.AffineTransform;

import javax.swing.Icon;

/**
 * A {@link DynamicForegroundLabel} that car be rotated by 90°.<br />
 * Code found at <a href="https://stackoverflow.com/questions/620929/rotate-a-swing-jlabel">stackoverflow</a> and
 * adapted for custom needs.
 * 
 * @author <a href="https://stackoverflow.com/users/73070/joey">Joey</a>, GIRARDOT
 */
public class RotatedLabel extends DynamicForegroundLabel {

    private static final long serialVersionUID = -3411943183792318984L;

    private boolean needsRotate;

    public enum Direction {
        HORIZONTAL, VERTICAL_UP, VERTICAL_DOWN
    }

    private Direction direction;

    public RotatedLabel() {
        super();
        direction = Direction.HORIZONTAL;
    }

    public RotatedLabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
        direction = Direction.HORIZONTAL;
    }

    public RotatedLabel(Icon image) {
        super(image);
        direction = Direction.HORIZONTAL;
    }

    public RotatedLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
        direction = Direction.HORIZONTAL;
    }

    public RotatedLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
        direction = Direction.HORIZONTAL;
    }

    public RotatedLabel(String text) {
        super(text);
        direction = Direction.HORIZONTAL;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        if (this.direction != direction) {
            this.direction = direction;
            revalidate();
            repaint();
        }
    }

    protected Insets getAdaptedInsets(Insets insets) {
        if (insets != null) {
            int tmp;
            Direction direction = getDirection();
            if (direction != null) {
                switch (direction) {
                    case VERTICAL_UP:
                        tmp = insets.left;
                        insets.left = insets.top;
                        insets.top = insets.right;
                        insets.right = insets.bottom;
                        insets.bottom = tmp;
                        break;
                    case VERTICAL_DOWN:
                        tmp = insets.left;
                        insets.left = insets.bottom;
                        insets.bottom = insets.right;
                        insets.right = insets.top;
                        insets.top = tmp;
                    default:
                        break;
                }
            }
        }
        return insets;
    }

    protected Dimension getRotatedDimension(Dimension size) {
        return size == null ? null : new Dimension(size.height, size.width);
    }

    protected Dimension getAdaptedDimension(Dimension size) {
        Dimension adaptedSize = size;
        Direction direction = getDirection();
        if (direction == Direction.VERTICAL_UP || direction == Direction.VERTICAL_DOWN) {
            adaptedSize = getRotatedDimension(size);
        } else {
            adaptedSize = size;
        }
        return adaptedSize;
    }

    @Override
    public Insets getInsets() {
        return getAdaptedInsets(super.getInsets());
    }

    @Override
    public Insets getInsets(Insets insets) {
        return getAdaptedInsets(super.getInsets(insets));
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension preferredSize = super.getPreferredSize();
        if (!isPreferredSizeSet()) {
            preferredSize = getAdaptedDimension(preferredSize);
        }
        return preferredSize;
    }

    @Override
    public Dimension getMinimumSize() {
        Dimension minimumSize = super.getMinimumSize();
        if (!isMinimumSizeSet()) {
            minimumSize = getAdaptedDimension(minimumSize);
        }
        return minimumSize;
    }

    @Override
    public Dimension getMaximumSize() {
        Dimension maximumSize = super.getMaximumSize();
        if (!isMaximumSizeSet()) {
            maximumSize = getAdaptedDimension(maximumSize);
        }
        return maximumSize;
    }

    @Override
    public Dimension getSize() {
        Dimension size = super.getSize();
        if (needsRotate) {
            size = getAdaptedDimension(size);
        }
        return size;
    }

    @Override
    public int getHeight() {
        return needsRotate ? super.getWidth() : super.getHeight();
    }

    @Override
    public int getWidth() {
        return needsRotate ? super.getHeight() : super.getWidth();
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (g instanceof Graphics2D) {
            Graphics2D g2d = (Graphics2D) g;
            AffineTransform tr = g2d.getTransform();
            Direction direction = getDirection();
            if (direction != null) {
                switch (direction) {
                    case VERTICAL_UP:
                        g2d.rotate(-Math.PI / 2);
                        g2d.translate(-getHeight(), 0);
                        break;
                    case VERTICAL_DOWN:
                        g2d.rotate(Math.PI / 2);
                        g2d.translate(0, -getWidth());
                        break;
                    default:
                        break;
                }
            }
            needsRotate = true;
            super.paintComponent(g2d);
            needsRotate = false;
            g2d.setTransform(tr);
        } else {
            super.paintComponent(g);
        }
    }
}
