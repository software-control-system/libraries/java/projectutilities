/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.ItemSelectable;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.Action;
import javax.swing.Box;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ButtonUI;

/**
 * A check box for which the clickable zone is constrained, as much as possible, to the visible box.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ConstrainedCheckBox extends JPanel implements ItemSelectable, SwingConstants {

    private static final long serialVersionUID = -6656033161550535562L;

    protected final JCheckBox checkBox;
    protected final JLabel textLabel;
    protected final Component topGlue, bottomGlue, leftGlue, rightGlue;
    private final ActionListener actionListener;
    private final ItemListener itemListener;
    private final ChangeListener changeListener;
    private final Collection<ActionListener> actionListeners;
    private final Collection<ItemListener> itemListeners;
    private final Collection<ChangeListener> changeListeners;
    private int iconTextGap;
    private int horizontalTextPosition;
    private int horizontalAlignment;
    private String text;
    private boolean textInCheckBox;
    private Action action;

    /**
     * Creates an initially unselected check box button with no text, no icon.
     */
    public ConstrainedCheckBox() {
        this(null, null, false);
    }

    /**
     * Creates a check box where properties are taken from the {@link Action} supplied.
     * 
     * @param a The {@link Action}.
     */
    public ConstrainedCheckBox(Action a) {
        this();
        setAction(a);
    }

    /**
     * Creates an initially unselected check box with text.
     *
     * @param text the text of the check box.
     */
    public ConstrainedCheckBox(String text) {
        this(text, null, false);
    }

    /**
     * Creates a check box with text and specifies whether or not it is initially selected.
     *
     * @param text the text of the check box.
     * @param selected a boolean value indicating the initial selection state. If <code>true</code> the check box is
     *            selected.
     */
    public ConstrainedCheckBox(String text, boolean selected) {
        this(text, null, selected);
    }

    /**
     * Creates an initially unselected check box with an icon.
     *
     * @param icon the Icon image to display.
     */
    public ConstrainedCheckBox(Icon icon) {
        this(null, icon, false);
    }

    /**
     * Creates an initially unselected check box with the specified text and icon.
     *
     * @param text the text of the check box.
     * @param icon the Icon image to display.
     */
    public ConstrainedCheckBox(String text, Icon icon) {
        this(text, icon, false);
    }

    /**
     * Creates a check box with text and icon, and specifies whether or not it is initially selected.
     *
     * @param text the text of the check box.
     * @param icon the Icon image to display.
     * @param selected a boolean value indicating the initial selection state. If <code>true</code> the check box is
     *            selected.
     */
    public ConstrainedCheckBox(String text, Icon icon, boolean selected) {
        super();
        checkBox = new JCheckBox(null, icon, selected);
        textLabel = new JLabel();
        textLabel.setFont(checkBox.getFont());
        super.setFont(checkBox.getFont());
        textLabel.setForeground(checkBox.getForeground());
        super.setForeground(checkBox.getForeground());
        textLabel.setHorizontalAlignment(checkBox.getHorizontalAlignment());
        topGlue = Box.createGlue();
        bottomGlue = Box.createGlue();
        leftGlue = Box.createGlue();
        rightGlue = Box.createGlue();
        this.text = text;
        iconTextGap = checkBox.getIconTextGap();
        horizontalTextPosition = checkBox.getHorizontalTextPosition();
        horizontalAlignment = checkBox.getHorizontalAlignment();
        setBorder(new EmptyBorder(5, 5, 5, 5));
        checkBox.setBorder(null);
        textLabel.setBorder(null);
        actionListeners = Collections.newSetFromMap(new ConcurrentHashMap<ActionListener, Boolean>());
        itemListeners = Collections.newSetFromMap(new ConcurrentHashMap<ItemListener, Boolean>());
        changeListeners = Collections.newSetFromMap(new ConcurrentHashMap<ChangeListener, Boolean>());
        actionListener = (e) -> {
            transmitActionEvent(e);
        };
        checkBox.addActionListener(actionListener);
        itemListener = (e) -> {
            transmitItemEvent(e);
        };
        checkBox.addItemListener(itemListener);
        changeListener = (e) -> {
            transmitChangeEvent(e);
        };
        checkBox.addChangeListener(changeListener);
        doSetLayout(new GridBagLayout());
        updateLayout();
    }

    public ButtonUI getButtonUI() {
        return checkBox.getUI();
    }

    public void setButtonUI(ButtonUI newUI) {
        checkBox.setUI(newUI);
    }

    protected void transmitActionEvent(ActionEvent evt) {
        if (evt != null) {
            ActionEvent event = new ActionEvent(this, evt.getID(), evt.getActionCommand(), evt.getWhen(),
                    evt.getModifiers());
            Action action = this.action;
            if (action != null) {
                action.actionPerformed(event);
            }
            for (ActionListener listener : actionListeners) {
                listener.actionPerformed(event);
            }
        }
    }

    protected void transmitItemEvent(ItemEvent evt) {
        if (evt != null) {
            ItemEvent event = new ItemEvent(this, evt.getID(), this, evt.getStateChange());
            for (ItemListener listener : itemListeners) {
                listener.itemStateChanged(event);
            }
        }
    }

    protected void transmitChangeEvent(ChangeEvent evt) {
        if (evt != null) {
            ChangeEvent event = new ChangeEvent(this);
            for (ChangeListener listener : changeListeners) {
                listener.stateChanged(event);
            }
        }
    }

    /**
     * Sets the layout manager for this container.
     * <p>
     * This method changes layout-related information, and therefore, invalidates the component hierarchy.
     *
     * @param mgr The specified layout manager.
     * @see #doLayout
     * @see #getLayout
     * @see #invalidate
     */
    protected void doSetLayout(LayoutManager mgr) {
        super.setLayout(mgr);
    }

    /**
     * Revalidates and repaints this {@link ConstrainedCheckBox}.
     */
    protected void refresh() {
        revalidate();
        repaint();
    }

    /**
     * Generates a {@link GridBagConstraints} that can be used by top or bottom glue.
     * 
     * @return A {@link GridBagConstraints}.
     */
    protected GridBagConstraints generateTopBottomConstraints() {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = 0;
        constraints.weightx = 1;
        constraints.weighty = 0.5;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        return constraints;
    }

    /**
     * Generates a {@link GridBagConstraints} that can be used by left glue, right glue, checkbox or textLabel.
     * 
     * @param weightx The {@link GridBagConstraints}'s weightx.
     * @return A {@link GridBagConstraints}.
     */
    protected GridBagConstraints generate2ndLineConstraints(double weightx) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridy = 1;
        constraints.weightx = weightx;
        constraints.weighty = 0;
        return constraints;
    }

    /**
     * Updates the layout to display checkbox and label as expected.
     */
    protected final void updateLayout() {
        removeAll();
        GridBagConstraints topGlueConstraints, bottomGlueConstraints, leftGlueConstraints, rightGlueConstraints;
        GridBagConstraints checkBoxConstraints, textLabelConstraints;
        topGlueConstraints = generateTopBottomConstraints();
        topGlueConstraints.gridy = 0;
        add(topGlue, topGlueConstraints);
        switch (horizontalTextPosition) {
            case RIGHT:
            case TRAILING:
                // checkbox then text
                textInCheckBox = false;
                checkBox.setText(null);
                textLabel.setText(text);
                checkBoxConstraints = generate2ndLineConstraints(0);
                checkBoxConstraints.insets = new Insets(0, 0, 0, iconTextGap);
                textLabelConstraints = generate2ndLineConstraints(0);
                switch (horizontalAlignment) {
                    case RIGHT:
                    case TRAILING:
                        // checkbox then text on the right
                        leftGlueConstraints = generate2ndLineConstraints(1);
                        leftGlueConstraints.gridx = 0;
                        add(leftGlue, leftGlueConstraints);
                        checkBoxConstraints.gridx = 1;
                        add(checkBox, checkBoxConstraints);
                        textLabelConstraints.gridx = 2;
                        add(textLabel, textLabelConstraints);
                        break;
                    case CENTER:
                        // checkbox then text at center
                        leftGlueConstraints = generate2ndLineConstraints(0.5);
                        leftGlueConstraints.gridx = 0;
                        add(leftGlue, leftGlueConstraints);
                        checkBoxConstraints.gridx = 1;
                        add(checkBox, checkBoxConstraints);
                        textLabelConstraints.gridx = 2;
                        add(textLabel, textLabelConstraints);
                        rightGlueConstraints = generate2ndLineConstraints(0.5);
                        rightGlueConstraints.gridx = 3;
                        add(rightGlue, rightGlueConstraints);
                        break;
                    case LEFT:
                    case LEADING:
                        // checkbox then text on the left
                        checkBoxConstraints.gridx = 0;
                        add(checkBox, checkBoxConstraints);
                        textLabelConstraints.gridx = 1;
                        add(textLabel, textLabelConstraints);
                        rightGlueConstraints = generate2ndLineConstraints(1);
                        rightGlueConstraints.gridx = 2;
                        add(rightGlue, rightGlueConstraints);
                        break;
                    default:
                        // invalid case
                        break;
                }
                break;
            case LEFT:
            case LEADING:
                // text then checkbox
                textInCheckBox = false;
                checkBox.setText(null);
                textLabel.setText(text);
                textLabelConstraints = generate2ndLineConstraints(0);
                checkBoxConstraints = generate2ndLineConstraints(0);
                checkBoxConstraints.insets = new Insets(0, iconTextGap, 0, 0);
                switch (horizontalAlignment) {
                    case RIGHT:
                    case TRAILING:
                        // text then checkbox on the right
                        leftGlueConstraints = generate2ndLineConstraints(1);
                        leftGlueConstraints.gridx = 0;
                        add(leftGlue, leftGlueConstraints);
                        textLabelConstraints.gridx = 1;
                        add(textLabel, textLabelConstraints);
                        checkBoxConstraints.gridx = 2;
                        add(checkBox, checkBoxConstraints);
                        break;
                    case CENTER:
                        // text then checkbox at center
                        leftGlueConstraints = generate2ndLineConstraints(0.5);
                        leftGlueConstraints.gridx = 0;
                        add(leftGlue, leftGlueConstraints);
                        textLabelConstraints.gridx = 1;
                        add(textLabel, textLabelConstraints);
                        checkBoxConstraints.gridx = 2;
                        add(checkBox, checkBoxConstraints);
                        rightGlueConstraints = generate2ndLineConstraints(0.5);
                        rightGlueConstraints.gridx = 3;
                        add(rightGlue, rightGlueConstraints);
                        break;
                    case LEFT:
                    case LEADING:
                        // text then checkbox on the left
                        textLabelConstraints.gridx = 0;
                        add(textLabel, textLabelConstraints);
                        checkBoxConstraints.gridx = 1;
                        add(checkBox, checkBoxConstraints);
                        rightGlueConstraints = generate2ndLineConstraints(1);
                        rightGlueConstraints.gridx = 2;
                        add(rightGlue, rightGlueConstraints);
                        break;
                    default:
                        // invalid case
                        break;
                }
                break;
            case CENTER:
                // text over checkbox
                textInCheckBox = true;
                textLabel.setText(null);
                checkBox.setText(text);
                checkBoxConstraints = generate2ndLineConstraints(0);
                switch (horizontalAlignment) {
                    case RIGHT:
                    case TRAILING:
                        // text over checkbox on the right
                        leftGlueConstraints = generate2ndLineConstraints(1);
                        leftGlueConstraints.gridx = 0;
                        add(leftGlue, leftGlueConstraints);
                        checkBoxConstraints.gridx = 1;
                        add(checkBox, checkBoxConstraints);
                        break;
                    case CENTER:
                        // text over checkbox at center
                        leftGlueConstraints = generate2ndLineConstraints(0.5);
                        leftGlueConstraints.gridx = 0;
                        add(leftGlue, leftGlueConstraints);
                        checkBoxConstraints.gridx = 1;
                        add(checkBox, checkBoxConstraints);
                        rightGlueConstraints = generate2ndLineConstraints(0.5);
                        rightGlueConstraints.gridx = 2;
                        add(rightGlue, rightGlueConstraints);
                        break;
                    case LEFT:
                    case LEADING:
                        // text over checkbox on the left
                        checkBoxConstraints.gridx = 0;
                        add(checkBox, checkBoxConstraints);
                        rightGlueConstraints = generate2ndLineConstraints(1);
                        rightGlueConstraints.gridx = 1;
                        add(rightGlue, rightGlueConstraints);
                        break;
                    default:
                        // invalid case
                        break;
                }
                break;
            default:
                break;
        }
        bottomGlueConstraints = generateTopBottomConstraints();
        bottomGlueConstraints.gridy = 2;
        add(bottomGlue, bottomGlueConstraints);
        refresh();
    }

    /**
     * Returns the model that this button represents.
     * 
     * @return the <code>model</code> property.
     * @see #setModel
     */
    public ButtonModel getModel() {
        return checkBox.getModel();
    }

    /**
     * Sets the model that this button represents.
     * 
     * @param newModel the new <code>ButtonModel</code>.
     * @see #getModel
     */
    public void setModel(ButtonModel newModel) {
        checkBox.setModel(newModel);
    }

    @Override
    public void setLayout(LayoutManager mgr) {
        // Nothing to do: don't allow layout changes
    }

    /**
     * Returns the currently set <code>Action</code> for this <code>ActionEvent</code> source, or <code>null</code> if
     * no <code>Action</code> is set.
     *
     * @return the <code>Action</code> for this <code>ActionEvent</code> source, or <code>null</code>
     * @see Action
     * @see #setAction
     */
    public Action getAction() {
        return action;
    }

    /**
     * Sets the <code>Action</code>.
     * The new <code>Action</code> replaces any previously set <code>Action</code> but does not affect
     * <code>ActionListeners</code> independently added with <code>addActionListener</code>. If the <code>Action</code>
     * is already a registered <code>ActionListener</code> for the button, it is not re-registered.
     * <p>
     * Setting the <code>Action</code> results in immediately changing all the properties described in
     * <a href="Action.html#buttonActions"> Swing Components Supporting <code>Action</code></a>.
     * </p>
     * <p>
     * This method uses three other methods to set and help track the <code>Action</code>'s property values. It uses the
     * <code>configurePropertiesFromAction</code> method to immediately change the button's properties. To track changes
     * in the <code>Action</code>'s property values, this method registers the <code>PropertyChangeListener</code>
     * returned by <code>createActionPropertyChangeListener</code>. The default {@code PropertyChangeListener} invokes
     * the {@code actionPropertyChanged} method when a property in the {@code Action} changes.
     * </p>
     * 
     * @param action the <code>Action</code> for the <code>AbstractButton</code>, or <code>null</code>
     * @see Action
     * @see #getAction
     * @see #configurePropertiesFromAction
     * @see #createActionPropertyChangeListener
     * @see #actionPropertyChanged
     */
    public void setAction(Action action) {
        if (action != this.action) {
            Action previous = this.action;
            removeActionListener(previous, true);
            this.action = action;
            checkBox.setAction(action);
            text = checkBox.getText();
            String tooltip = checkBox.getToolTipText();
            Icon icon = checkBox.getIcon();
            Icon selectedIcon = checkBox.getSelectedIcon();
            Icon rolloverIcon = checkBox.getRolloverIcon();
            Icon rolloverSelectedIcon = checkBox.getRolloverSelectedIcon();
            Icon pressedIcon = checkBox.getPressedIcon();
            Icon disabledIcon = checkBox.getDisabledIcon();
            Icon disabledSelectedIcon = checkBox.getDisabledSelectedIcon();
            boolean rolloverEnabled = checkBox.isRolloverEnabled();
            if (action != null) {
                checkBox.setAction(null);
            }
            addActionListener(action);
            setToolTipText(tooltip);
            setIcon(icon);
            setPressedIcon(pressedIcon);
            setSelectedIcon(selectedIcon);
            setRolloverIcon(rolloverIcon);
            setRolloverSelectedIcon(rolloverSelectedIcon);
            setDisabledIcon(disabledIcon);
            setDisabledSelectedIcon(disabledSelectedIcon);
            setRolloverEnabled(rolloverEnabled);
            if (textInCheckBox) {
                checkBox.setText(text);
            } else {
                textLabel.setText(text);
            }
            refresh();
        }
    }

    /**
     * Returns an array (length 1) containing the label or <code>null</code> if the button is not selected.
     *
     * @return An array containing 1 Object: the text of the button, if the item is selected; otherwise
     *         <code>null</code>.
     */
    @Override
    public Object[] getSelectedObjects() {
        Object[] selectedItems;
        if (isSelected()) {
            selectedItems = new Object[] { text };
        } else {
            selectedItems = null;
        }
        return selectedItems;
    }

    @Override
    public void addItemListener(ItemListener listener) {
        if (listener != null) {
            itemListeners.add(listener);
        }
    }

    @Override
    public void removeItemListener(ItemListener listener) {
        if (listener != null) {
            itemListeners.remove(listener);
        }
    }

    /**
     * Adds an <code>ActionListener</code> to the button.
     * 
     * @param listener the <code>ActionListener</code> to be added.
     */
    public void addActionListener(ActionListener listener) {
        if (listener != null) {
            actionListeners.add(listener);
        }
    }

    /**
     * Removes an <code>ActionListener</code> from the button. If the listener is the currently set <code>Action</code>
     * for the button, then the <code>Action</code> is set to <code>null</code>.
     * 
     * @param listener the <code>ActionListener</code> to be removed.
     */
    public void removeActionListener(ActionListener listener) {
        if (listener != null) {
            removeActionListener(listener, false);
        }
    }

    /**
     * Removes an <code>ActionListener</code> from the button. If the listener is the currently set <code>Action</code>
     * for the button, then the <code>Action</code> is set to <code>null</code>.
     * 
     * @param listener the <code>ActionListener</code> to be removed.
     * @param fromSetAction Whether this call comes from {@link #setAction(Action)}.
     * 
     */
    protected void removeActionListener(ActionListener listener, boolean fromSetAction) {
        if (listener != null) {
            actionListeners.remove(listener);
            if ((listener == action) && (!fromSetAction)) {
                setAction(null);
            }
        }
    }

    /**
     * Adds a <code>ChangeListener</code> to the button.
     * 
     * @param listener the listener to be added.
     */
    public void addChangeListener(ChangeListener listener) {
        if (listener != null) {
            changeListeners.add(listener);
        }
    }

    /**
     * Removes a ChangeListener from the button.
     * 
     * @param listener the listener to be removed.
     */
    public void removeChangeListener(ChangeListener listener) {
        if (listener != null) {
            changeListeners.remove(listener);
        }
    }

    /**
     * Returns an array of all the <code>ChangeListener</code>s added to this AbstractButton with addChangeListener().
     *
     * @return all of the <code>ChangeListener</code>s added or an empty array if no listeners have been added.
     */
    public ChangeListener[] getChangeListeners() {
        return changeListeners.toArray(new ChangeListener[changeListeners.size()]);
    }

    /**
     * Returns the horizontal position of the text relative to the icon.
     * 
     * @return the <code>horizontalTextPosition</code> property, one of the following values:
     *         <ul>
     *         <li>{@code SwingConstants.LEFT}</li>
     *         <li>{@code SwingConstants.RIGHT}</li>
     *         <li>{@code SwingConstants.CENTER}</li>
     *         <li>{@code SwingConstants.LEADING}</li>
     *         <li>{@code SwingConstants.TRAILING} (the default)</li>
     *         </ul>
     */
    public int getHorizontalTextPosition() {
        return horizontalTextPosition;
    }

    /**
     * Sets the horizontal position of the text relative to the icon.
     * 
     * @param textPosition one of the following values:
     *            <ul>
     *            <li>{@code SwingConstants.LEFT}</li>
     *            <li>{@code SwingConstants.RIGHT}</li>
     *            <li>{@code SwingConstants.CENTER}</li>
     *            <li>{@code SwingConstants.LEADING}</li>
     *            <li>{@code SwingConstants.TRAILING} (the default)</li>
     *            </ul>
     * @throws IllegalArgumentException if <code>textPosition</code> is not one of the legal values listed above.
     */
    public void setHorizontalTextPosition(int horizontalTextPosition) throws IllegalArgumentException {
        if (this.horizontalTextPosition != horizontalTextPosition) {
            switch (horizontalTextPosition) {
                case RIGHT:
                case TRAILING:
                case CENTER:
                case LEFT:
                case LEADING:
                    this.horizontalTextPosition = horizontalTextPosition;
                    checkBox.setHorizontalTextPosition(horizontalTextPosition);
                    updateLayout();
                    break;
                default:
                    throw new IllegalArgumentException("Invalid horizontal text position");
            }
        }
    }

    /**
     * Returns the alignment of the text along the X axis.
     *
     * @return The value of the horizontalAlignment property, one of the
     *         following constants:
     *         <ul>
     *         <li>{@code SwingConstants.LEFT}</li>
     *         <li>{@code SwingConstants.RIGHT}</li>
     *         <li>{@code SwingConstants.CENTER}</li>
     *         <li>{@code SwingConstants.LEADING}</li>
     *         <li>{@code SwingConstants.TRAILING}</li>
     *         </ul>
     *
     * @see #setHorizontalAlignment
     * @see SwingConstants
     */
    public int getHorizontalAlignment() {
        return horizontalAlignment;
    }

    /**
     * Sets the alignment of the text along the X axis.
     * <p>
     * This is a JavaBeans bound property.
     *
     * @param alignment One of the following constants:
     *            <ul>
     *            <li>{@code SwingConstants.LEFT}</li>
     *            <li>{@code SwingConstants.RIGHT}</li>
     *            <li>{@code SwingConstants.CENTER}</li>
     *            <li>{@code SwingConstants.LEADING}</li>
     *            <li>{@code SwingConstants.TRAILING}</li>
     *            </ul>
     *
     * @see #getHorizontalAlignment
     * @see SwingConstants
     * @throws IllegalArgumentException if <code>alignment</code> is not one of the legal values listed above.
     */
    public void setHorizontalAlignment(int alignment) {
        if (this.horizontalAlignment != alignment) {
            switch (alignment) {
                case RIGHT:
                case TRAILING:
                case CENTER:
                case LEFT:
                case LEADING:
                    this.horizontalAlignment = alignment;
                    textLabel.setHorizontalAlignment(alignment);
                    checkBox.setHorizontalAlignment(alignment);
                    updateLayout();
                    break;
                default:
                    throw new IllegalArgumentException("Invalid horizontal alignment");
            }
        }
    }

    /**
     * Returns the button's text.
     * 
     * @return A {@link String}.
     * @see #setText(String)
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the button's text.
     * 
     * @param text The string used to set the text.
     * @see #getText()
     */
    public void setText(String text) {
        this.text = text;
        if (textInCheckBox) {
            checkBox.setText(text);
        } else {
            textLabel.setText(text);
        }
        refresh();
    }

    @Override
    public void setToolTipText(String text) {
        super.setToolTipText(text);
        if (checkBox != null) {
            checkBox.setToolTipText(text);
        }
        if (textLabel != null) {
            textLabel.setToolTipText(text);
        }
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);
        if (checkBox != null) {
            checkBox.setFont(font);
        }
        if (textLabel != null) {
            textLabel.setFont(font);
        }
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (checkBox != null) {
            checkBox.setBackground(bg);
        }
        if (textLabel != null) {
            textLabel.setBackground(bg);
        }
    }

    @Override
    public void setForeground(Color fg) {
        super.setForeground(fg);
        if (checkBox != null) {
            checkBox.setForeground(fg);
        }
        if (textLabel != null) {
            textLabel.setForeground(fg);
        }
    }

    @Override
    public void setOpaque(boolean isOpaque) {
        super.setOpaque(isOpaque);
        if (checkBox != null) {
            checkBox.setOpaque(isOpaque);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (checkBox != null) {
            checkBox.setEnabled(enabled);
        }
        if (textLabel != null) {
            textLabel.setEnabled(enabled);
        }
    }

    /**
     * Gets the <code>rolloverEnabled</code> property.
     *
     * @return the value of the <code>rolloverEnabled</code> property.
     * @see #setRolloverEnabled
     */
    public boolean isRolloverEnabled() {
        return checkBox.isRolloverEnabled();
    }

    /**
     * Sets the <code>rolloverEnabled</code> property, which must be <code>true</code> for rollover effects to occur.
     * The default value for the <code>rolloverEnabled</code> property is <code>false</code>.
     * Some look and feels might not implement rollover effects; they will ignore this property.
     *
     * @param b if <code>true</code>, rollover effects should be painted.
     * @see #isRolloverEnabled
     */
    public void setRolloverEnabled(boolean b) {
        checkBox.setRolloverEnabled(b);
    }

    /**
     * Returns the state of the button. True if the toggle button is selected, false if it's not.
     * 
     * @return true if the toggle button is selected, otherwise false.
     */
    public boolean isSelected() {
        return checkBox.isSelected();
    }

    /**
     * Sets the state of the button. Note that this method does not trigger an <code>actionEvent</code>.
     * Call <code>doClick</code> to perform a programatic action change.
     *
     * @param b true if the button is selected, otherwise false.
     */
    public void setSelected(boolean b) {
        checkBox.setSelected(b);
    }

    /**
     * Returns the default icon.
     * 
     * @return the default <code>Icon</code>.
     * @see #setIcon
     */
    public Icon getIcon() {
        return checkBox.getIcon();
    }

    /**
     * Sets the button's default icon. This icon is also used as the "pressed" and "disabled" icon if there is no
     * explicitly set pressed icon.
     *
     * @param defaultIcon the icon used as the default image.
     * @see #getIcon
     * @see #setPressedIcon
     */
    public void setIcon(Icon defaultIcon) {
        checkBox.setIcon(defaultIcon);
    }

    /**
     * Returns the pressed icon for the button.
     * 
     * @return the <code>pressedIcon</code> property.
     * @see #setPressedIcon
     */
    public Icon getPressedIcon() {
        return checkBox.getPressedIcon();
    }

    /**
     * Sets the pressed icon for the button.
     * 
     * @param pressedIcon the icon used as the "pressed" image.
     * @see #getPressedIcon
     */
    public void setPressedIcon(Icon pressedIcon) {
        checkBox.setPressedIcon(pressedIcon);
    }

    /**
     * Returns the selected icon for the button.
     * 
     * @return the <code>selectedIcon</code> property.
     * @see #setSelectedIcon
     */
    public Icon getSelectedIcon() {
        return checkBox.getSelectedIcon();
    }

    /**
     * Sets the selected icon for the button.
     * 
     * @param selectedIcon the icon used as the "selected" image.
     * @see #getSelectedIcon
     */
    public void setSelectedIcon(Icon selectedIcon) {
        checkBox.setSelectedIcon(selectedIcon);
    }

    /**
     * Returns the rollover icon for the button.
     * 
     * @return the <code>rolloverIcon</code> property.
     * @see #setRolloverIcon
     */
    public Icon getRolloverIcon() {
        return checkBox.getRolloverIcon();
    }

    /**
     * Sets the rollover icon for the button.
     * 
     * @param rolloverIcon the icon used as the "rollover" image.
     * @see #getRolloverIcon
     */
    public void setRolloverIcon(Icon rolloverIcon) {
        checkBox.setRolloverIcon(rolloverIcon);
    }

    /**
     * Returns the rollover selection icon for the button.
     * 
     * @return the <code>rolloverSelectedIcon</code> property.
     * @see #setRolloverSelectedIcon
     */
    public Icon getRolloverSelectedIcon() {
        return checkBox.getRolloverSelectedIcon();
    }

    /**
     * Sets the rollover selected icon for the button.
     * 
     * @param rolloverSelectedIcon the icon used as the "selected rollover" image.
     * @see #getRolloverSelectedIcon
     */
    public void setRolloverSelectedIcon(Icon rolloverSelectedIcon) {
        checkBox.setRolloverSelectedIcon(rolloverSelectedIcon);
    }

    /**
     * Returns the icon used by the button when it's disabled. If no disabled icon has been set this will forward the
     * call to the look and feel to construct an appropriate disabled Icon.
     * <p>
     * Some look and feels might not render the disabled Icon, in which case they will ignore this.
     *
     * @return the <code>disabledIcon</code> property.
     * @see #getPressedIcon
     * @see #setDisabledIcon
     * @see javax.swing.LookAndFeel#getDisabledIcon
     */
    public Icon getDisabledIcon() {
        return checkBox.getDisabledIcon();
    }

    /**
     * Sets the disabled icon for the button.
     * 
     * @param disabledIcon the icon used as the disabled image.
     * @see #getDisabledIcon
     */
    public void setDisabledIcon(Icon disabledIcon) {
        checkBox.setDisabledIcon(disabledIcon);
    }

    /**
     * Returns the icon used by the button when it's disabled and selected.
     * If no disabled selection icon has been set, this will forward the call to the LookAndFeel to construct an
     * appropriate disabled Icon from the selection icon if it has been set and to <code>getDisabledIcon()</code>
     * otherwise.
     * <p>
     * Some look and feels might not render the disabled selected Icon, in which case they will ignore this.
     *
     * @return the <code>disabledSelectedIcon</code> property.
     * @see #getDisabledIcon
     * @see #setDisabledSelectedIcon
     * @see javax.swing.LookAndFeel#getDisabledSelectedIcon
     */
    public Icon getDisabledSelectedIcon() {
        return checkBox.getDisabledSelectedIcon();
    }

    /**
     * Sets the disabled selection icon for the button.
     * 
     * @param disabledSelectedIcon the icon used as the disabled selection image.
     * @see #getDisabledSelectedIcon
     */
    public void setDisabledSelectedIcon(Icon disabledSelectedIcon) {
        checkBox.setDisabledSelectedIcon(disabledSelectedIcon);
    }
}
