/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.awt.ColorUtils;

/**
 * A tool used to monitor memory use
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AMemoryChecker extends JPanel implements SwingUtilitiesConstants {

    private static final long serialVersionUID = -7517055294197500596L;

    protected static final String[] UNITS = DEFAULT_MESSAGE_MANAGER
            .getMessage("fr.soleil.lib.project.swing.memory.units").split(",");
    protected static final int UNIT_FACTOR = 1024;
    protected static final String MEMORY_FORMAT = "%.2f";
    protected static final double DARK_FRACTION = 0.4;
    protected static final String MEMORY_CHECK = DEFAULT_MESSAGE_MANAGER
            .getMessage("fr.soleil.lib.project.swing.memory.check");
    protected static final String MEMORY_SEPARATOR = ", ";
    protected static final String DEFAULT_USED_MEMORY_TEXT = DEFAULT_MESSAGE_MANAGER
            .getMessage("fr.soleil.lib.project.swing.memory.used");
    protected static final String DEFAULT_MAX_MEMORY_TEXT = DEFAULT_MESSAGE_MANAGER
            .getMessage("fr.soleil.lib.project.swing.memory.max");
    protected static final String DEFAULT_ALLOCATED_MEMORY_TEXT = DEFAULT_MESSAGE_MANAGER
            .getMessage("fr.soleil.lib.project.swing.memory.allocated");
    protected static final String DEFAULT_FREE_MEMORY_TEXT = DEFAULT_MESSAGE_MANAGER
            .getMessage("fr.soleil.lib.project.swing.memory.free");
    protected static final String GC = DEFAULT_MESSAGE_MANAGER.getMessage("fr.soleil.lib.project.swing.memory.gc");

    protected static final ImageIcon GC_ICON = DEFAULT_ICONS.getIcon("fr.soleil.lib.project.swing.clear");

    protected static final Color DEFAULT_DANGER_COLOR = ColorUtils.brighter(Color.RED);
    protected static final Color DEFAULT_WARNING_COLOR = Color.ORANGE;
    protected static final Color DEFAULT_OK_COLOR = Color.GREEN;
    protected static final Color DEFAULT_BG_COLOR = new Color(0xBBD2EE);
    protected static final Color DEFAULT_BG_DARK_COLOR = ColorUtils.darker(DEFAULT_BG_COLOR, DARK_FRACTION);
    protected static final Color DEFAULT_BG_BRIGHT_COLOR = ColorUtils.brighter(DEFAULT_BG_COLOR);

    protected final JLabel usedMemoryLabel;
    protected final JDoubleProgressBar usedMemoryProgressBar;
    protected final JButton gcButton;
    protected Timer timer;
    protected final Object timerLock;
    protected volatile boolean started;
    protected Color okColor, darkOk, brightOk;
    protected Color warningColor, darkWarning, brightWarning;
    protected Color dangerColor, darkDanger, brightDanger;
    protected int warningPercent, dangerPercent;
    protected String usedMemoryText, maxMemoryText, allocatedMemoryText, freeMemoryText;
    protected long refreshingPeriod;

    public AMemoryChecker() {
        super(new BorderLayout(5, 5));
        setOkColor(null);
        setWarningColor(null);
        setDangerColor(null);
        warningPercent = 50;
        dangerPercent = 75;
        usedMemoryText = DEFAULT_USED_MEMORY_TEXT;
        maxMemoryText = DEFAULT_MAX_MEMORY_TEXT;
        allocatedMemoryText = DEFAULT_ALLOCATED_MEMORY_TEXT;
        freeMemoryText = DEFAULT_FREE_MEMORY_TEXT;
        usedMemoryProgressBar = new JDoubleProgressBar();
        usedMemoryProgressBar.setDisplayFGProgress(true);
        usedMemoryProgressBar.setOpaque(false);
        usedMemoryProgressBar.setFont(getFont());
        usedMemoryProgressBar.setFGMaximum(100);
        usedMemoryProgressBar.setFGValue(0);
        usedMemoryProgressBar.setBGMaximum(100);
        usedMemoryProgressBar.setBGValue(0);
        usedMemoryProgressBar.setBGProgressBarColors(DEFAULT_BG_COLOR, DEFAULT_BG_BRIGHT_COLOR, DEFAULT_BG_DARK_COLOR);
        updateProgressBarColor(okColor, brightOk, darkOk);
        usedMemoryProgressBar.setMinimumSize(usedMemoryProgressBar.getPreferredSize());
        usedMemoryLabel = new JLabel(usedMemoryText);
        usedMemoryLabel.setFont(getFont());
        gcButton = new JButton(GC_ICON);
        gcButton.setMargin(NO_MARGIN);
        gcButton.setToolTipText(GC);
        gcButton.addActionListener((e) -> {
            new Thread(GC) {
                @Override
                public void run() {
                    // Note: Calling it twice is a solution found here:
                    // https://stackoverflow.com/questions/13305813/system-gc-vs-gc-button-in-jvisualvm-jconsole
                    System.gc();
                    try {
                        sleep(500);
                        System.gc();
                    } catch (InterruptedException e) {
                        // ignore
                    }
                }
            }.start();
        });
        gcButton.setVisible(false);
        add(usedMemoryLabel, BorderLayout.WEST);
        add(usedMemoryProgressBar, BorderLayout.CENTER);
        add(gcButton, BorderLayout.EAST);
        refreshingPeriod = 1;
        timer = null;
        timerLock = new Object();
        started = false;
    }

    /**
     * Returns whether gc button (button to call garbage collector) is visible.
     * 
     * @return A <code>boolean</code>. <code>false</code> by default.
     */
    public boolean isGCButtonVisible() {
        return gcButton.isVisible();
    }

    /**
     * Sets whether gc button (button to call garbage collector) should be visible.
     * 
     * @param visible Whether gc button (button to call garbage collector) should be visible.
     */
    public void setGCButtonVisible(boolean visible) {
        gcButton.setVisible(visible);
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);
        if (usedMemoryProgressBar != null) {
            usedMemoryProgressBar.setFont(font);
        }
        if (usedMemoryLabel != null) {
            usedMemoryLabel.setFont(font);
        }
    }

    /**
     * Returns the {@link Color} used in progress bar when memory usage is under warning level
     * 
     * @return A {@link Color}
     */
    public Color getOkColor() {
        return okColor;
    }

    /**
     * Sets the {@link Color} to use in progress bar when memory usage is under warning level
     * 
     * @param okColor The {@link Color} to use in progress bar when memory usage is under warning level
     */
    public void setOkColor(Color okColor) {
        this.okColor = okColor == null ? DEFAULT_OK_COLOR : okColor;
        this.darkOk = ColorUtils.darker(this.okColor, DARK_FRACTION);
        this.brightOk = ColorUtils.brighter(this.okColor);
    }

    /**
     * Returns the {@link Color} used in progress bar when memory usage is above warning level and under danger level
     * 
     * @return A {@link Color}
     */
    public Color getWarningColor() {
        return warningColor;
    }

    /**
     * Sets the {@link Color} to use in progress bar when memory usage is above warning level and under danger level
     * 
     * @param warningColor The {@link Color} to use in progress bar when memory usage is above warning level and under
     *            danger level
     */
    public void setWarningColor(Color warningColor) {
        this.warningColor = warningColor == null ? DEFAULT_WARNING_COLOR : warningColor;
        this.darkWarning = ColorUtils.darker(this.warningColor, DARK_FRACTION);
        this.brightWarning = ColorUtils.brighter(this.warningColor);
    }

    /**
     * Returns the {@link Color} used in progress bar when memory usage is above danger level
     * 
     * @return A {@link Color}
     */
    public Color getDangerColor() {
        return dangerColor;
    }

    /**
     * Sets the {@link Color} to use in progress bar when memory usage is above danger level
     * 
     * @param dangerColor The {@link Color} to use in progress bar when memory usage is above danger level
     */
    public void setDangerColor(Color dangerColor) {
        this.dangerColor = dangerColor == null ? DEFAULT_DANGER_COLOR : dangerColor;
        this.darkDanger = ColorUtils.darker(this.dangerColor, DARK_FRACTION);
        this.brightDanger = ColorUtils.brighter(this.dangerColor);
    }

    public int getWarningPercent() {
        return warningPercent;
    }

    public int getDangerPercent() {
        return dangerPercent;
    }

    /**
     * Changes the values at which to change state color
     * 
     * @param warningPercent The value above which to display warning color in progress bar
     * @param dangerPercent The value above which to display danger color in progress bar
     */
    public void setWarningAndDangerPercents(int warningPercent, int dangerPercent) {
        if ((warningPercent > 0) && (warningPercent < 100) && (dangerPercent > 0) && (dangerPercent < 100)
                && (warningPercent < dangerPercent)) {
            this.warningPercent = warningPercent;
            this.dangerPercent = dangerPercent;
        }
    }

    /**
     * Returns the text displayed in tooltip and as progress bar title for used memory.
     * 
     * @return A {@link String}. Default: "<code>Used Memory:</code>"
     */
    public String getUsedMemoryText() {
        return usedMemoryText;
    }

    /**
     * Sets the text to display in tooltip and as progress bar title for used memory.
     * 
     * @param usedMemoryText The text to display in tooltip and as progress bar title for used memory.
     */
    public void setUsedMemoryText(String usedMemoryText) {
        this.usedMemoryText = usedMemoryText == null ? DEFAULT_USED_MEMORY_TEXT : usedMemoryText;
        this.usedMemoryLabel.setText(this.usedMemoryText);
    }

    /**
     * Returns the text displayed in tooltip for maximum memory.
     * 
     * @return A {@link String}. Default: "<code>Max Memory:</code>"
     */
    public String getMaxMemoryText() {
        return maxMemoryText;
    }

    /**
     * Sets the text to display in tooltip for maximum memory.
     * 
     * @param usedMemoryText The text to display in tooltip for maximum memory.
     */
    public void setMaxMemoryText(String maxMemoryText) {
        this.maxMemoryText = maxMemoryText == null ? DEFAULT_MAX_MEMORY_TEXT : maxMemoryText;
    }

    /**
     * Returns the text displayed in tooltip for allocated memory.
     * 
     * @return A {@link String}. Default: "<code>Allocated Memory:</code>"
     */
    public String getAllocatedMemoryText() {
        return allocatedMemoryText;
    }

    /**
     * Sets the text to display in tooltip for allocated memory.
     * 
     * @param allocatedMemoryText The text to display in tooltip for allocated memory.
     */
    public void setAllocatedMemoryText(String allocatedMemoryText) {
        this.allocatedMemoryText = allocatedMemoryText == null ? DEFAULT_ALLOCATED_MEMORY_TEXT : allocatedMemoryText;
    }

    /**
     * Returns the text displayed in tooltip for free memory.
     * 
     * @return A {@link String}. Default: "<code>Free Memory:</code>"
     */
    public String getFreeMemoryText() {
        return freeMemoryText;
    }

    /**
     * Sets the text to display in tooltip for free memory.
     * 
     * @param usedMemoryText The text to display in tooltip for free memory.
     */
    public void setFreeMemoryText(String freeMemoryText) {
        this.freeMemoryText = freeMemoryText == null ? DEFAULT_FREE_MEMORY_TEXT : freeMemoryText;
    }

    /**
     * Returns the period, in seconds, at which to update memory usage information.
     * 
     * @return A <code>long</code>
     */
    public long getRefreshingPeriod() {
        return refreshingPeriod;
    }

    protected final void doStart() {
        timer = new Timer(MEMORY_CHECK);
        timer.schedule(new RefreshUsedMemoryTask(), 0, refreshingPeriod * 1000);
        started = true;
    }

    protected final void doStop() {
        timer.cancel();
        timer.purge();
        started = false;
    }

    /**
     * Sets the period, in seconds, at which to update memory usage information.
     * 
     * @param refreshingPeriod The period, in seconds, at which to update memory usage information.
     */
    public void setRefreshingPeriod(long refreshingPeriod) {
        if ((refreshingPeriod > 0) && (this.refreshingPeriod != refreshingPeriod)) {
            this.refreshingPeriod = refreshingPeriod;
            synchronized (timerLock) {
                if (started) {
                    doStop();
                    doStart();
                }
            }
        }
    }

    /**
     * Starts memory monitoring
     */
    public void start() {
        synchronized (timerLock) {
            if (!started) {
                doStart();
            }
        }
    }

    /**
     * Stops memory monitoring
     */
    public void stop() {
        synchronized (timerLock) {
            if (started) {
                doStop();
            }
        }
    }

    protected StringBuilder appendMemoryToStringBuilder(final StringBuilder builder, final long memory) {
        StringBuilder result = builder;
        if (result == null) {
            result = new StringBuilder();
        }
        double memd = memory;
        int index = 0;
        while ((index < UNITS.length - 1) && (memd >= UNIT_FACTOR)) {
            memd /= UNIT_FACTOR;
            index++;
        }
        if (index == 0) {
            result.append(memory);
        } else {
            result.append(Format.formatValue(memd, MEMORY_FORMAT));
        }
        result.append(UNITS[index]);
        return result;
    }

    protected void updateProgressBarColor(Color bg, Color bright, Color dark) {
        usedMemoryProgressBar.setFGProgressBarColors(bg, bright, dark);
    }

    /**
     * Treats an error that occurred during {@link RefreshUsedMemoryTask} execution.
     * 
     * @param t The error.
     */
    protected abstract void treatError(Throwable t);

    protected void updateMemoryInfo(long usedMemory, long allocatedMemory, long maxMemory) {
        long freeMemory = allocatedMemory - usedMemory;
        double usagePercent = 100 * usedMemory / (double) maxMemory;
        double allocatedPercent = 100 * allocatedMemory / (double) maxMemory;
        StringBuilder fgBuilder = new StringBuilder(usedMemoryText);
        fgBuilder.append(' ');
        appendMemoryToStringBuilder(fgBuilder, usedMemory);
        fgBuilder.append(MEMORY_SEPARATOR).append(maxMemoryText).append(' ');
        appendMemoryToStringBuilder(fgBuilder, maxMemory);
        StringBuilder bgBuilder = new StringBuilder(allocatedMemoryText);
        bgBuilder.append(' ');
        appendMemoryToStringBuilder(bgBuilder, allocatedMemory);
        bgBuilder.append(MEMORY_SEPARATOR).append(freeMemoryText).append(' ');
        appendMemoryToStringBuilder(bgBuilder, freeMemory);
        Color bg, bright, dark;
        if (usagePercent >= dangerPercent) {
            bg = dangerColor;
            bright = brightDanger;
            dark = darkDanger;
        } else if (usagePercent >= warningPercent) {
            bg = warningColor;
            bright = brightWarning;
            dark = darkWarning;
        } else {
            bg = okColor;
            bright = brightOk;
            dark = darkOk;
        }
        usedMemoryProgressBar.setFGValue(usagePercent);
        usedMemoryProgressBar.setBGValue(allocatedPercent);
        updateProgressBarColor(bg, bright, dark);
        usedMemoryProgressBar.setFGTooltip(fgBuilder.toString());
        usedMemoryProgressBar.setBGTooltip(bgBuilder.toString());
        String tooltip = new StringBuilder(fgBuilder).append(" - ").append(bgBuilder).toString();
        setToolTipText(tooltip);
        usedMemoryLabel.setToolTipText(tooltip);
        repaint();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * {@link TimerTask} that will check memory usage and update displayed information.
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class RefreshUsedMemoryTask extends TimerTask {
        @Override
        public void run() {
            try {
                MemoryMXBean mxBean = ManagementFactory.getMemoryMXBean();
                MemoryUsage usage = mxBean.getHeapMemoryUsage();
                long used = usage.getUsed();
                long max = usage.getMax();
                long allocated = usage.getCommitted();
                updateMemoryInfo(used, allocated, max);
            } catch (Throwable t) {
                treatError(t);
            }
        }
    }

}
