/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.Container;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultButtonModel;

public class ComponentUtils {

    /**
     * Safely removes an {@link AbstractButton} from its parent
     * 
     * @param button The {@link AbstractButton} to remove
     */
    public static void safelyRemoveFromParent(AbstractButton button) {
        if (button != null) {
            Container parent = button.getParent();
            if (parent != null) {
                if (button.getModel() instanceof DefaultButtonModel) {
                    ButtonGroup group = ((DefaultButtonModel) button.getModel()).getGroup();
                    if (group != null) {
                        group.remove(button);
                    }
                }
                parent.remove(button);
            }
        }
    }

}
