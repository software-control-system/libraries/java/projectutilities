/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.ui;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JProgressBar;
import javax.swing.plaf.basic.BasicProgressBarUI;

import fr.soleil.lib.project.awt.ColorUtils;

/**
 * A {@link BasicProgressBarUI} for which progress color can be set and that applies a tube effect on the progress
 * bar.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class TubeProgressBarUI extends BasicProgressBarUI {

    public static final Color DEFAULT_PROGRESS_COLOR = new Color(110, 240, 255);
    protected static final int DEFAULT_CORNER_LENGTH = 5;

    private Color progressColor;
    private int cornerArcDiameter;
    private boolean useTubeEffect;

    public TubeProgressBarUI() {
        super();
        this.progressColor = DEFAULT_PROGRESS_COLOR;
        this.cornerArcDiameter = DEFAULT_CORNER_LENGTH;
        this.useTubeEffect = true;
    }

    /**
     * Returns the progress color
     * 
     * @return A {@link Color}
     */
    public Color getProgressColor() {
        return progressColor;
    }

    /**
     * Sets the progress color
     * 
     * @param progressColor The {@link Color} to set
     */
    public void setProgressColor(Color progressColor) {
        this.progressColor = progressColor == null ? DEFAULT_PROGRESS_COLOR : progressColor;
    }

    /**
     * Returns the diameter (both horizontal and vertical) of the arc at the four corners of the {@link JProgressBar}
     * 
     * @return The diameter (both horizontal and vertical) of the arc at the four corners of the {@link JProgressBar}
     */
    public int getCornerArcDiameter() {
        return cornerArcDiameter;
    }

    /**
     * Sets the diameter (both horizontal and vertical) of the arc at the four corners of the {@link JProgressBar}
     * 
     * @param cornerLength the diameter to set
     */
    public void setCornerArcDiameter(int cornerLength) {
        this.cornerArcDiameter = cornerLength > -1 ? cornerLength : DEFAULT_CORNER_LENGTH;
    }

    /**
     * Returns whether a tube effect will be used inside the {@link JProgressBar}
     * 
     * @return a <code>boolean</code>
     */
    public boolean isUseTubeEffect() {
        return useTubeEffect;
    }

    /**
     * Sets whether to use a tube effect inside the {@link JProgressBar}
     * 
     * @param effect Whether to use a tube effect inside the {@link JProgressBar}
     */
    public void setUseTubeEffect(boolean effect) {
        this.useTubeEffect = effect;
    }

    @Override
    protected Color getSelectionBackground() {
        return getAutoForeground(progressBar.getBackground());
    }

    @Override
    protected Color getSelectionForeground() {
        return getAutoForeground(progressColor);
    }

    protected Color getAutoForeground(Color bg) {
        Color fg;
        if (bg == null) {
            fg = Color.BLACK;
        } else {
            fg = Math.round(ColorUtils.getRealGray(bg)) < 128 ? Color.WHITE : Color.BLACK;
        }
        return fg;
    }

    protected int getBarWidth(Insets b) {
        return progressBar.getWidth() - (b.right + b.left + 1);
    }

    protected int getBarHeight(Insets b) {
        return progressBar.getHeight() - (b.top + b.bottom + 1);
    }

    protected void drawShadow(Graphics2D g2, JComponent c, Insets b, int barRectWidth, int barRectHeight,
            int cornerLength) {
        if (isUseTubeEffect()) {
            int endX, endY;
            if (progressBar.getOrientation() == JProgressBar.HORIZONTAL) {
                endX = b.left;
                endY = c.getHeight() / 2;
            } else {
                endY = b.top;
                endX = c.getWidth() / 2;
            }
            GradientPaint grad = new GradientPaint(b.left, b.top, new Color(0, 0, 0, 0), endX, endY,
                    new Color(0, 0, 0, 30), true);
            g2.setPaint(grad);
            g2.fillRoundRect(b.left, b.top, barRectWidth, barRectHeight, cornerLength, cornerLength);
            grad = new GradientPaint(b.left, b.top, new Color(255, 255, 255, 30), endX, endY,
                    new Color(255, 255, 255, 0), true);
            g2.setPaint(grad);
            g2.fillRoundRect(b.left, b.top, barRectWidth, barRectHeight, cornerLength, cornerLength);
            g2.setColor(progressBar.getForeground());
        }
    }

    @Override
    protected void paintIndeterminate(Graphics g, JComponent c) {
        if (g instanceof Graphics2D) {
            Graphics2D g2 = (Graphics2D) g;
            Insets b = progressBar.getInsets(); // area for border
            int barRectWidth = getBarWidth(b);
            int barRectHeight = getBarHeight(b);
            if ((barRectWidth > 0) && (barRectHeight > 0)) {
                int cornerLength = this.cornerArcDiameter;
                boolean stringPainted = progressBar.isStringPainted();
                // getBox is already called in paintString method
                if (!stringPainted) {
                    // Paint the bouncing box.
                    boxRect = getBox(boxRect);
                }
                if (boxRect != null) {
                    g2.setColor(progressColor);
                    g2.fillRoundRect(boxRect.x, boxRect.y, boxRect.width, boxRect.height, cornerLength, cornerLength);
                }
                drawShadow(g2, c, b, barRectWidth, barRectHeight, cornerLength);
                g2.setColor(progressBar.getForeground());
                g2.drawRoundRect(b.left, b.top, barRectWidth, barRectHeight, cornerLength, cornerLength);
                // Deal with possible text painting
                if (stringPainted) {
                    paintString(g, b.left, b.top, barRectWidth, barRectHeight, 0, b);
                }
            }
        }
    }

    @Override
    public void paintDeterminate(Graphics g, JComponent c) {
        if (g instanceof Graphics2D) {
            Graphics2D g2 = (Graphics2D) g;
            Insets b = progressBar.getInsets(); // area for border
            int barRectWidth = getBarWidth(b);
            int barRectHeight = getBarHeight(b);
            if ((barRectWidth > 0) && (barRectHeight > 0)) {
                int cornerLength = this.cornerArcDiameter;
                // amount of progress to draw
                int amountFull = getAmountFull(b, barRectWidth, barRectHeight);
                g2.setColor(progressColor);
                if (progressBar.getOrientation() == JProgressBar.HORIZONTAL) {
                    g2.fillRoundRect(b.left, b.top, amountFull, barRectHeight, cornerLength, cornerLength);
                } else {
                    g2.fillRoundRect(b.left, b.top, barRectWidth, amountFull, cornerLength, cornerLength);
                }
                drawShadow(g2, c, b, barRectWidth, barRectHeight, cornerLength);
                g2.drawRoundRect(b.left, b.top, barRectWidth, barRectHeight, cornerLength, cornerLength);
                // Deal with possible text painting
                if (progressBar.isStringPainted()) {
                    paintString(g, b.left, b.top, barRectWidth, barRectHeight, amountFull, b);
                }
            }
        }
    }
}