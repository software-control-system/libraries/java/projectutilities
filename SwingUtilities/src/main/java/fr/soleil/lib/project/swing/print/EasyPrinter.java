/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.print;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.awt.print.PrinterJob;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.SimpleDoc;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

import fr.soleil.lib.project.swing.SnapshotUtil;

/**
 * A class used to print any kind of {@link Component}
 * 
 * @author saintin
 */
public class EasyPrinter {

    /**
     * Prints the desired {@link Component}
     * 
     * @param comp The {@link Component} to print. If <code>null</code>, does nothing at all.
     * @throws PrintException If a problem occurred while trying to print the {@link Component}
     */
    public static void print(Component comp) throws PrintException {
        if (comp != null) {
            BufferedImage bi = SnapshotUtil.getSnapshot(comp);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                ImageIO.write(bi, "PNG", baos);
                byte[] data = baos.toByteArray();
                ByteArrayInputStream bais = new ByteArrayInputStream(data);
                PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
                DocFlavor flavor = DocFlavor.INPUT_STREAM.PNG;
                PrinterJob pj = PrinterJob.getPrinterJob();
                boolean okay = pj.printDialog(pras);
                if (okay) {
                    PrintService service = pj.getPrintService();
                    DocPrintJob job = service.createPrintJob();
                    DocAttributeSet das = new HashDocAttributeSet();
                    Doc doc = new SimpleDoc(bais, flavor, das);
                    job.print(doc, pras);
                }
            } catch (IOException e) {
                throw new PrintException("Failed to print desired component", e);
            }
        }
    }
}
