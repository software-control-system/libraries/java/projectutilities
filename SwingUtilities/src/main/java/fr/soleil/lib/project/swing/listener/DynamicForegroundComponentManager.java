/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.listener;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComponent;
import javax.swing.UIManager;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.ColorUtils;
import fr.soleil.lib.project.awt.listener.IComponentMultiListener;

/**
 * A {@link PropertyChangeListener} and {@link HierarchyListener}, that is able to dynamically change a
 * {@link Component}'s foreground color so that texts are readable in that {@link Component}, according to its visible
 * background color. This means that the {@link DynamicForegroundComponentManager} takes account of {@link Component}'s
 * opacity. The calculated foreground color will always be black or white.
 * <table>
 * <tr>
 * <th style="text-align:left">Use case example:</th>
 * </tr>
 * <tr>
 * <td >
 * <code>
 * DynamicForegroundComponentManager manager = new DynamicForegroundComponentManager();<br />
 * JLabel label = new JLabel();<br />
 * manager.setupAndListenToComponent(label);
 * </code>
 * </td>
 * </tr>
 * </table>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DynamicForegroundComponentManager
        implements IComponentMultiListener, PropertyChangeListener, HierarchyListener {

    // default background to use when no background is found. This background is JPanels default background.
    protected static final Color DEFAULT_BACKGROUND = UIManager.getColor("Panel.background");
    // opaque property name.
    protected static final String OPAQUE = "opaque";
    // background property name.
    protected static final String BACKGROUND = "background";

    /**
     * Default constructor. Creates a new {@link DynamicForegroundComponentManager}.
     */
    public DynamicForegroundComponentManager() {
        super();
    }

    /**
     * Stops listening to a {@link Component}'s properties and returns whether this
     * {@link DynamicForegroundComponentManager} was listening to {@link Component}'s properties.
     * <p>
     * Typically, this method should be called when applying a change to the {@link Component}, that will result in a
     * {@link PropertyChangeEvent}.
     * </p>
     * 
     * @param component The {@link Component}.
     * 
     * @return A <code>boolean</code>.
     */
    protected boolean stopListeningToComponentBeforeApplyingChange(Component component) {
        boolean isListening = false;
        if (component != null) {
            PropertyChangeListener[] listeners = component.getPropertyChangeListeners();
            if (listeners != null) {
                for (PropertyChangeListener listener : listeners) {
                    if (listener == this) {
                        isListening = true;
                        break;
                    }
                }
                if (isListening) {
                    component.removePropertyChangeListener(this);
                }
            }
        }
        return isListening;
    }

    /**
     * Computes the best foreground color for a given background color.
     * 
     * @param bg The background color.
     * @return A {@link Color}. Never <code>null</code>: black or white.
     */
    protected Color getAutoForeground(Color bg) {
        Color fg;
        if (bg == null) {
            fg = Color.BLACK;
        } else {
            fg = Math.round(ColorUtils.getRealGray(bg)) < 128 ? Color.WHITE : Color.BLACK;
        }
        return fg;
    }

    /**
     * Returns whether a {@link Component} can be considered as transparent (i.e. not opaque).
     * 
     * @param component The {@link Component}.
     * @return A <code>boolean</code>.
     */
    protected boolean isTransparent(Component component) {
        return (component instanceof JComponent) && (!((JComponent) component).isOpaque());
    }

    /**
     * Returns the visible background of a {@link Component}, searching in its parents when not opaque.
     * 
     * @param component The {@link Component},
     * @return A {@link Color}. Never <code>null</code>.
     */
    protected Color getBackground(Component component) {
        Color bg;
        if (component == null) {
            bg = DEFAULT_BACKGROUND;
        } else if (isTransparent(component)) {
            bg = getBackground(component.getParent());

        } else {
            bg = component.getBackground();
        }
        if (bg == null) {
            bg = DEFAULT_BACKGROUND;
        }
        return bg;
    }

    /**
     * Updates the foreground color of a {@link Component} according to its visible background color.
     * 
     * @param component The {@link Component}.
     */
    public void updateForeground(Component component) {
        if (component != null) {
            Color fg = getAutoForeground(getBackground(component));
            // change foreground only when not same one
            if (!ObjectUtils.sameObject(fg, component.getForeground())) {
                // avoid listening to component when changing its foreground color
                boolean isListening = stopListeningToComponentBeforeApplyingChange(component);
                // change component foreground color
                component.setForeground(fg);
                // listen back to component
                if (isListening) {
                    component.addPropertyChangeListener(this);
                }
            }
        }
    }

    /**
     * Utility method used to stop listening to a {@link Component}, so that its foreground color won't be dynamically
     * changed anymore.
     * <p>
     * Typically, this method calls:
     * <ul>
     * <li>{@link Component#removePropertyChangeListener(PropertyChangeListener)}</li>
     * <li>{@link Component#removeHierarchyListener(HierarchyListener)}</li>
     * </ul>
     * </p>
     * 
     * @param component The {@link Component}.
     */
    @Override
    public void stopListeningToComponent(Component component) {
        if (component != null) {
            component.removePropertyChangeListener(this);
            component.removeHierarchyListener(this);
        }
    }

    /**
     * Utility method used to change a {@link Component}'s foreground color and to listen to that {@link Component} in
     * order to dynamically change its foreground color when needed.
     * <p>
     * Typically, this method calls:
     * <ul>
     * <li>{@link #stopListeningToComponent(Component)} <i>(to avoid registering the same listener twice)</i></li>
     * <li>{@link Component#addPropertyChangeListener(PropertyChangeListener)}</li>
     * <li>{@link Component#addHierarchyListener(HierarchyListener)}</li>
     * <li>{@link #updateForeground(Component)}</li>
     * </ul>
     * </p>
     * 
     * @param component The {@link Component}.
     */
    @Override
    public void setupAndListenToComponent(Component component) {
        stopListeningToComponent(component);
        if (component != null) {
            component.addPropertyChangeListener(this);
            component.addHierarchyListener(this);
            updateForeground(component);
        }
    }

    @Override
    public void hierarchyChanged(HierarchyEvent e) {
        if (e != null) {
            Component component = e.getComponent();
            if (isTransparent(component)) {
                updateForeground(component);
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ((evt != null) && (OPAQUE.equalsIgnoreCase(evt.getPropertyName())
                || BACKGROUND.equalsIgnoreCase(evt.getPropertyName()))) {
            Object src = evt.getSource();
            if (src instanceof Component) {
                updateForeground((Component) src);
            }
        }
    }

}
