/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.table;

import java.awt.event.ActionEvent;

import javax.swing.JTable;

public class TableEvent extends ActionEvent {

    private static final long serialVersionUID = 4883993243635451593L;

    private Object object;
    private Object value;
    private int row;
    private int column;

    public TableEvent(JTable table, Object value, int row, int column) {
        super(table, -1, null);
        this.value = value;
        this.row = row;
        this.column = column;
    }

    public Object getObject() {
        return object;
    }

    public JTable getTable() {
        return getSource() instanceof JTable ? (JTable) getSource() : null;
    }

    public Object getValue() {
        return value;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

}
