/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.panel;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListCellRenderer;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.TransferHandler;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;

import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.IDataComponent;
import fr.soleil.lib.project.awt.layout.VerticalFlowLayout;
import fr.soleil.lib.project.awt.listener.DataComponentDelegate;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.file.log.CsvLogFileWriter;
import fr.soleil.lib.project.file.log.ILogFileWriter;
import fr.soleil.lib.project.file.log.TextLogFileWriter;
import fr.soleil.lib.project.log.ILogManager;
import fr.soleil.lib.project.log.Level;
import fr.soleil.lib.project.log.LogData;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;
import fr.soleil.lib.project.swing.ScrollMagnetism;
import fr.soleil.lib.project.swing.SwingUtilitiesConstants;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.combo.DynamicRenderingComboBox;
import fr.soleil.lib.project.swing.file.ExtensionFileFilter;
import fr.soleil.lib.project.swing.file.FileChooserUtils;
import fr.soleil.lib.project.swing.file.log.HtmlLogFileWriter;
import fr.soleil.lib.project.swing.icons.Icons;
import fr.soleil.lib.project.swing.renderer.ILevelRenderer;
import fr.soleil.lib.project.swing.renderer.LevelListCellRenderer;
import fr.soleil.lib.project.swing.renderer.LogSplitManagementListCellRenderer;
import fr.soleil.lib.project.swing.renderer.ScrollMagnetismListCellRenderer;
import fr.soleil.lib.project.swing.ui.ColoredSplitPaneUI;
import fr.soleil.lib.project.swing.ui.MetalSpinnerUI;

/**
 * A {@link JPanel} used to manage log display.
 * 
 * @author Rapha&euml;l GIRARDOT
 *
 * @param <L> The type of component this {@link ALogViewer} uses to display logs.
 */
public abstract class ALogViewer<L extends JComponent & ILogManager & ILevelRenderer> extends JPanel
        implements ActionListener, ItemListener, DocumentListener, ChangeListener, ComponentListener, ILogManager,
        ILevelRenderer, IDataComponent, SwingUtilitiesConstants {

    private static final long serialVersionUID = 8530934091410060774L;

    // CardLayout constants
    protected static final String PLAY = "play";
    protected static final String PAUSE = "pause";

    // file management
    private static final String DEFAULT_DIRECTORY = FileChooserUtils.EXTENSION_SEPARATOR;

    /**
     * Default splitpane proportion (the closest to <code>1</code>, the more proportion for the log component).
     */
    protected static final double DEFAULT_SPLIT_PROPORTION = 0.95;

    /**
     * Default splitpane resize weight (the closest to <code>1</code>, the more resize weight for the log component).
     */
    protected static final double DEFAULT_SPLIT_WEIGHT = 1;

    // GUI management
    protected final MessageManager messageManager;
    protected final Icons icons;

    // managed logs
    protected final Object logsLock;
    protected volatile LogData[] logs, logsToFilter;
    protected volatile int maxLogs;
    protected volatile boolean paused;
    protected volatile boolean keepLastLogs;

    // save logs
    private final JFileChooser logSaver;
    protected final String fileFormat;
    protected final Map<String, ILogFileWriter> logFileWriters;
    protected final HtmlLogFileWriter htmlFileFilter;
    protected final TextLogFileWriter defaultFileFilter;

    // main splitpane
    protected JSplitPane mainSplitPane;

    // main component
    protected L logComponent;
    protected JScrollPane logScrollPane;
    protected volatile boolean scrollTop, scrollBottom, scrollLeft, scrollRight;

    // magnetism
    protected JPanel magnetismPanel;
    protected JLabel magnetismLabel;
    protected JComboBox<ScrollMagnetism> magnetismComboBox;

    // splitpane management
    protected JPanel splitManagementPanel;
    protected JLabel splitManagementLabel;
    protected JComboBox<SplitManagement> splitManagementComboBox;

    // management panel
    protected JPanel managementPanel;
    protected JScrollPane managementScrollPane;

    // level filter
    protected JPanel levelPanel;
    protected JLabel levelLabel;
    protected JComboBox<Level> levelComboBox;
    protected ConstrainedCheckBox levelCheckBox;

    // text filter
    protected JPanel filterPanel;
    protected JLabel filterLabel;
    protected JTextField filterTextField;
    protected ConstrainedCheckBox filterCheckBox;

    // max logs
    protected JPanel maxLogsPanel;
    protected JLabel maxLogsLabel;
    protected JSpinner maxLogsSpinner;

    // font size
    protected JPanel fontPanel;
    protected JLabel fontSizeLabel;
    protected JSpinner fontSizeSpinner;

    // color management
    protected ConstrainedCheckBox colorsEnabledCheckBox;

    // filter process
    protected volatile FilterWorker filterWorker;

    // buttons
    protected JPanel playPausePanel;
    protected CardLayout playPausePanelLayout;
    protected JButton clearButton, saveButton, copyButton, pauseButton, resumeButton, optionsButton;

    // options
    protected JPanel optionsPanel;
    protected JDialog optionsDialog;

    // listeners to warn once clear log button clicked
    protected final Set<ActionListener> clearLogListeners;

    /**
     * The split management to use
     */
    protected SplitManagement splitManagement;

    // Variables only used to decide when to really refresh logs (JAVAAPI-617)
    protected volatile boolean wasMinimumLevel, wasCaseSensitive;
    protected volatile Level lastFilteredLevel;
    protected volatile String lastFilteredText;
    protected final DataComponentDelegate dataComponentDelegate;
    protected boolean autoScrollMagnetism, fromAutoScrollMagnetism;

    /**
     * Constructs a new {@link ALogViewer}
     */
    public ALogViewer() {
        this(DEFAULT_MESSAGE_MANAGER, DEFAULT_ICONS);
    }

    /**
     * Constructs a new {@link ALogViewer} with given {@link MessageManager} and {@link Icons}.
     * 
     * @param messageManager The {@link MessageManager}.
     * @param icons The {@link Icons}.
     */
    public ALogViewer(MessageManager messageManager, Icons icons) {
        super();
        this.messageManager = messageManager == null ? DEFAULT_MESSAGE_MANAGER : messageManager;
        this.icons = icons == null ? DEFAULT_ICONS : icons;
        this.logsLock = new Object();
        this.logs = null;
        this.logsToFilter = null;
        this.logSaver = new JFileChooser(DEFAULT_DIRECTORY);
        this.logSaver.setAcceptAllFileFilterUsed(false);
        autoScrollMagnetism = true;
        fromAutoScrollMagnetism = false;
        fileFormat = this.messageManager.getMessage("fr.soleil.lib.project.swing.log.files");
        logFileWriters = new ConcurrentHashMap<>();
        htmlFileFilter = new HtmlLogFileWriter(this.messageManager);
        defaultFileFilter = new TextLogFileWriter(this.messageManager);
        clearLogListeners = Collections.newSetFromMap(new ConcurrentHashMap<ActionListener, Boolean>());
        registerDefaultLogFileWriters();
        paused = false;
        keepLastLogs = false;
        maxLogs = Integer.MAX_VALUE;
        buildComponents();
        wasMinimumLevel = levelCheckBox.isSelected();
        lastFilteredLevel = getFilteredLevel();
        if (lastFilteredLevel == null) {
            lastFilteredLevel = Level.ALL;
        }
        wasCaseSensitive = filterCheckBox.isSelected();
        lastFilteredText = getFilteringText(wasCaseSensitive);
        setLevelFont(DEFAULT_LOG_FONT);
        layoutManagmentPanel();
        layoutOptionsPanel();
        setSplitManagement(getPreferredSplitManagement());
        // by default, log limitation components are not visible
        setLogLimitationVisible(false);

        // JAVAAPI-617: refresh logs only when showing
        dataComponentDelegate = new DataComponentDelegate();
        dataComponentDelegate.setupAndListenToComponent(this);
        dataComponentDelegate.setUpdateOnDataChanged(this, false);
    }

    /**
     * Registers an {@link ILogFileWriter} that can be used for file saving.
     * 
     * @param writer The {@link ILogFileWriter}.
     */
    public void registerLogFileWriter(ILogFileWriter writer) {
        if (writer != null) {
            String[] extensions = writer.getManagedFileExtensions();
            if (extensions != null) {
                for (String extension : extensions) {
                    if (extension != null) {
                        extension = extension.toLowerCase();
                        if (logFileWriters.put(extension, writer) == null) {
                            ExtensionFileFilter filter = new ExtensionFileFilter(extension,
                                    String.format(fileFormat, extension));
                            FileFilter[] filters = logSaver.getChoosableFileFilters();
                            logSaver.addChoosableFileFilter(filter);
                            if ((filters == null) || (filters.length == 0)
                                    || (filters.length == 1 && logSaver.isAcceptAllFileFilterUsed())) {
                                logSaver.setFileFilter(filter);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Registers the default {@link ILogFileWriter}s to use for file saving.
     */
    protected void registerDefaultLogFileWriters() {
        registerLogFileWriter(defaultFileFilter);
        registerLogFileWriter(new CsvLogFileWriter(messageManager));
        registerLogFileWriter(htmlFileFilter);
    }

    /**
     * Calculates the {@link JScrollPane} column header height.
     * 
     * @return An <code>int</code>.
     */
    protected abstract int computeColumnHeaderMargin();

    /**
     * Calculates the {@link JScrollPane} row header width.
     * 
     * @return An <code>int</code>.
     */
    protected abstract int computeRowHeaderMargin();

    /**
     * Computes best scroll magnetism for given logs.
     * 
     * @param logs The logs.
     * @return A {@link ScrollMagnetism}.
     */
    protected abstract ScrollMagnetism computeBestScrollMagnetism(LogData... logs);

    /**
     * Returns the currently used {@link ScrollMagnetism}.
     * 
     * @return A {@link ScrollMagnetism}.
     */
    public ScrollMagnetism getScrollMagnetism() {
        return (ScrollMagnetism) magnetismComboBox.getSelectedItem();
    }

    /**
     * Sets the {@link ScrollMagnetism} to use.
     * 
     * @param magnetism The {@link ScrollMagnetism} to use.
     */
    public void setScrollMagnetism(ScrollMagnetism magnetism) {
        if (SwingUtilities.isEventDispatchThread()) {
            magnetismComboBox.setSelectedItem(magnetism);
        } else {
            SwingUtilities.invokeLater(() -> {
                magnetismComboBox.setSelectedItem(magnetism);
            });
        }
    }

    /**
     * Returns the {@link ScrollMagnetism} choices available in {@link JComboBox}.
     * 
     * @return A {@link ScrollMagnetism} array.
     */
    protected ScrollMagnetism[] getScrollMagnetismChoices() {
        return ScrollMagnetism.values();
    }

    /**
     * Builds the components of this {@link ALogViewer}, except for the {@link JSplitPane}, which will be built in
     * {@link #buildSplitPanel()}.
     */
    protected void buildComponents() {
        // main component
        logComponent = generateLogComponent();
        logScrollPane = new JScrollPane(logComponent);
        logComponent.addComponentListener(this);

        // magnetism
        magnetismLabel = generateTitleLabel("fr.soleil.lib.project.swing.magnet");
        magnetismComboBox = generateDynamicComboBox(ScrollMagnetism.NONE, getScrollMagnetismChoices());
        magnetismComboBox.setRenderer(new ScrollMagnetismListCellRenderer(magnetismComboBox));
        magnetismPanel = generatePanel(magnetismLabel, magnetismComboBox, null);

        // splitpane management
        splitManagementLabel = generateTitleLabel("fr.soleil.lib.project.swing.log.split.title");
        splitManagementComboBox = generateDynamicComboBox(getPreferredSplitManagement(), SplitManagement.values());
        splitManagementComboBox.setRenderer(new LogSplitManagementListCellRenderer(splitManagementComboBox));
        splitManagementPanel = generatePanel(splitManagementLabel, splitManagementComboBox, null);

        // level filter
        levelLabel = generateTitleLabel("fr.soleil.lib.project.swing.log.filter.level");
        levelComboBox = generateDynamicComboBox(Level.INFO, Level.ERROR, Level.WARNING, Level.INFO, Level.DEBUG,
                Level.ALL);
        levelComboBox.setRenderer(new LevelListCellRenderer(levelComboBox));
        levelCheckBox = generateCheckBox("fr.soleil.lib.project.swing.log.filter.level.minimum", true);
        levelPanel = generatePanel(levelLabel, levelComboBox, levelCheckBox);

        // text filter
        filterLabel = generateTitleLabel("fr.soleil.lib.project.swing.log.filter.text");
        filterLabel.setToolTipText(messageManager.getMessage("fr.soleil.lib.project.swing.log.filter.tooltip"));
        filterTextField = new JTextField(10);
        filterTextField.getDocument().addDocumentListener(this);
        filterCheckBox = generateCheckBox("fr.soleil.lib.project.swing.log.filter.text.case.sensitive", false);
        filterPanel = generatePanel(filterLabel, filterTextField, filterCheckBox);

        // max logs
        maxLogsLabel = generateTitleLabel("fr.soleil.lib.project.swing.log.max");
        maxLogsSpinner = generateNumberSpinner(0, 0, Integer.MAX_VALUE);
        maxLogsSpinner.setToolTipText(messageManager.getMessage("fr.soleil.lib.project.swing.log.max.help"));
        maxLogsPanel = generatePanel(maxLogsLabel, maxLogsSpinner, null);

        // font size
        fontSizeLabel = generateTitleLabel("fr.soleil.lib.project.swing.log.font.size");
        fontSizeSpinner = generateNumberSpinner(DEFAULT_LOG_FONT.getSize(), 1, Integer.MAX_VALUE);
        // color management
        colorsEnabledCheckBox = generateCheckBox("fr.soleil.lib.project.swing.log.colors.enabled", true);
        colorsEnabledCheckBox.setSelected(true);
        fontPanel = generatePanel(fontSizeLabel, fontSizeSpinner, colorsEnabledCheckBox);

        // buttons
        clearButton = generateButton("fr.soleil.lib.project.swing.log.clear");
        saveButton = generateButton("fr.soleil.lib.project.swing.log.save");
        copyButton = generateButton("fr.soleil.lib.project.swing.log.copy");
        pauseButton = generateButton("fr.soleil.lib.project.swing.log.pause");
        resumeButton = generateButton("fr.soleil.lib.project.swing.log.resume");
        // play/pause buttons
        playPausePanelLayout = new CardLayout();
        playPausePanel = new JPanel();
        playPausePanel.setBorder(null);
        playPausePanel.setLayout(playPausePanelLayout);
        playPausePanel.add(resumeButton, PLAY);
        playPausePanel.add(pauseButton, PAUSE);
        playPausePanelLayout.show(playPausePanel, PAUSE);
        optionsButton = generateButton("fr.soleil.lib.project.swing.log.options.tooltip",
                "fr.soleil.lib.project.swing.log.options");
        optionsPanel = new JPanel();
        optionsPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

        // management panel
        managementPanel = new JPanel();
        managementPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        managementScrollPane = new JScrollPane(managementPanel);
        managementScrollPane.setBorder(new TitledBorder(new LineBorder(Color.BLACK),
                messageManager.getMessage("fr.soleil.lib.project.swing.log.management")));
    }

    /**
     * Layouts the options panel.
     */
    protected void layoutOptionsPanel() {
        optionsPanel.setLayout(new GridBagLayout());
        // magnetism
        addMagnetismPanel();
        // splitpane management
        addSplitManagementPanel();
        // font size and color
        addFontManagement();
        // max logs
        addMaxLogsManagement();
    }

    /**
     * Layouts the management panel.
     */
    protected void layoutManagmentPanel() {
        managementPanel.setLayout(new VerticalFlowLayout(VerticalFlowLayout.LEFT, 10, 5));
        managementPanel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                managementPanel.revalidate();
                Container parent = managementPanel.getParent();
                if (parent instanceof JComponent) {
                    JComponent parentComponent = (JComponent) parent;
                    parentComponent.revalidate();
                    parentComponent.repaint();
                }
                repaint();
            }
        });

        // level filter
        addLevelFilter();

        // text filter
        addTextFilter();

        // buttons
        addButtons();
    }

    /**
     * Adds level filter panel in management panel.
     */
    protected void addLevelFilter() {
        managementPanel.add(levelPanel);
    }

    /**
     * Adds text filter panel in management panel.
     */
    protected void addTextFilter() {
        managementPanel.add(filterPanel);
    }

    /**
     * Generates a {@link GridBagConstraints} to put a component in options panel.
     * 
     * @return A {@link GridBagConstraints}.
     */
    protected GridBagConstraints generateOptionsConstraints() {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = optionsPanel.getComponentCount();
        constraints.weightx = 1;
        constraints.weighty = 0;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.insets = new Insets(constraints.gridy == 0 ? 0 : 10, 0, 0, 0);
        return constraints;
    }

    /**
     * Adds font management panel in options panel.
     */
    protected void addFontManagement() {
        optionsPanel.add(fontPanel, generateOptionsConstraints());
    }

    /**
     * Adds max logs management panel in options panel.
     */
    protected void addMaxLogsManagement() {
        optionsPanel.add(maxLogsPanel, generateOptionsConstraints());
    }

    /**
     * Adds scroll magnetism panel in options panel.
     */
    protected void addMagnetismPanel() {
        optionsPanel.add(magnetismPanel, generateOptionsConstraints());
    }

    /**
     * Adds scroll splitManagement panel in options panel.
     */
    protected void addSplitManagementPanel() {
        optionsPanel.add(splitManagementPanel, generateOptionsConstraints());
    }

    /**
     * Adds the buttons in management panel.
     */
    protected void addButtons() {
        managementPanel.add(clearButton);
        managementPanel.add(saveButton);
        managementPanel.add(copyButton);
        managementPanel.add(playPausePanel);
        managementPanel.add(optionsButton);
    }

    /**
     * Returns the preferred proportion to set to log component in split pane.
     * 
     * @return A <code>double</code>: <code>0 &lt; proportion &lt; 1</code>.
     */
    protected double getLogComponentPreferredSplitProportion() {
        return DEFAULT_SPLIT_PROPORTION;
    }

    /**
     * Returns the preferred resize weight to set to log component in split pane.
     * 
     * @return A <code>double</code>: <code>0 &le; weight &le; 1</code>.
     */
    protected double getLogComponentPreferredResizeWeight() {
        return DEFAULT_SPLIT_WEIGHT;
    }

    /**
     * Returns this {@link ALogViewer}'s preferred {@link SplitManagement} for main split pane.
     * 
     * @return A {@link SplitManagement}.
     */
    protected SplitManagement getPreferredSplitManagement() {
        return SplitManagement.TOP_BOTTOM;
    }

    /**
     * Returns the {@link SplitManagement} used in mane split pane.
     * 
     * @return A {@link SplitManagement}.
     * @see SplitManagement#TOP_BOTTOM
     * @see SplitManagement#BOTTOM_TOP
     * @see SplitManagement#LEFT_RIGHT
     * @see SplitManagement#RIGHT_LEFT
     */
    public SplitManagement getSplitManagement() {
        return splitManagement;
    }

    /**
     * Sets the {@link SplitManagement} in main split pane.
     * 
     * @param splitManagement The {@link SplitManagement}.
     * @see SplitManagement#TOP_BOTTOM
     * @see SplitManagement#BOTTOM_TOP
     * @see SplitManagement#LEFT_RIGHT
     * @see SplitManagement#RIGHT_LEFT
     */
    public void setSplitManagement(SplitManagement splitManagement) {
        SplitManagement effective = splitManagement == null ? getPreferredSplitManagement() : splitManagement;
        if (effective != null) {
            this.splitManagement = effective;
            if (effective != splitManagementComboBox.getSelectedItem()) {
                splitManagementComboBox.setSelectedItem(effective);
            }
            buildSplitPanel();
            layoutMainPanel();
            revalidate();
            SwingUtilities.invokeLater(() -> {
                mainSplitPane.setDividerLocation(mainSplitPane.getResizeWeight());
            });
            repaint();
        }
    }

    /**
     * Creates a new main split pane.
     * 
     * @param management The preferred {@link SplitManagement}.
     * @param proportion The preferred proportion for log component (<code>0 &lt; proportion &lt; 1</code>). If
     *            <code>proportion</code> is out of bounds, {@link #DEFAULT_SPLIT_PROPORTION} will be used.
     * @param weight The preferred resize weight for log component (<code>0 &le; weight &le; 1</code>). If
     *            <code>weight</code> is out of bounds, <code>proportion</code> will be used.
     * @return A {@link JSplitPane}.
     */
    protected JSplitPane createSplitPane(SplitManagement management, double proportion, double weight) {
        double proportionToUse = proportion;
        if ((proportionToUse <= 0) || (proportionToUse >= 1) || Double.isNaN(proportionToUse)) {
            proportionToUse = DEFAULT_SPLIT_PROPORTION;
        }
        double weightToUse = weight;
        if ((weightToUse < 0) || (weightToUse > 1) || Double.isNaN(weightToUse)) {
            weightToUse = proportionToUse;
        }
        JSplitPane splitPane;
        switch (management) {
            case LEFT_RIGHT:
                splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
                splitPane.setLeftComponent(logScrollPane);
                splitPane.setRightComponent(managementScrollPane);
                break;
            case RIGHT_LEFT:
                proportionToUse = 1 - proportionToUse;
                weightToUse = 1 - weightToUse;
                splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
                splitPane.setLeftComponent(managementScrollPane);
                splitPane.setRightComponent(logScrollPane);
                break;
            case BOTTOM_TOP:
                proportionToUse = 1 - proportionToUse;
                weightToUse = 1 - weightToUse;
                splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
                splitPane.setTopComponent(managementScrollPane);
                splitPane.setBottomComponent(logScrollPane);
                break;
            case TOP_BOTTOM:
            default:
                splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
                splitPane.setTopComponent(logScrollPane);
                splitPane.setBottomComponent(managementScrollPane);
                break;
        }
        splitPane.setDividerLocation(proportionToUse);
        splitPane.setResizeWeight(weightToUse);
        return splitPane;
    }

    /**
     * Configures a {@link JSplitPane} so that its display is user friendly.
     * 
     * @param splitPane The {@link JSplitPane} to configure.
     */
    protected void configureSplitPane(JSplitPane splitPane) {
        splitPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        splitPane.setDividerSize(8);
        splitPane.setOneTouchExpandable(true);
        splitPane.setUI(new ColoredSplitPaneUI());
    }

    /**
     * Builds the main {@link JSplitPane}.
     */
    protected void buildSplitPanel() {
        if (mainSplitPane != null) {
            mainSplitPane.removeAll();
        }
        mainSplitPane = createSplitPane(getSplitManagement(), getLogComponentPreferredSplitProportion(),
                getLogComponentPreferredResizeWeight());
        configureSplitPane(mainSplitPane);
    }

    /**
     * Calls the "one touch expandable" aspects of the {@link JSplitPane} to hide management panel or show log
     * component.
     */
    public void hideManagementPanelOrShowLogComponent() {
        ((ColoredSplitPaneUI) mainSplitPane.getUI()).oneTouchExpandRightOrDown();
    }

    /**
     * Calls the "one touch expandable" aspects of the {@link JSplitPane} to hide log component or show management
     * panel.
     */
    public void hideLogComponentOrShowManagementPanel() {
        ((ColoredSplitPaneUI) mainSplitPane.getUI()).oneTouchExpandLeftOrUp();
    }

    /**
     * Layouts the components in this {@link ALogViewer}.
     */
    protected void layoutMainPanel() {
        removeAll();
        setLayout(new BorderLayout(0, 0));
        add(mainSplitPane, BorderLayout.CENTER);
    }

    /**
     * Generates the component that will display the logs.
     * 
     * @return A log component.
     */
    protected abstract L generateLogComponent();

    /**
     * Generates a {@link JPanel} that will contain a title {@link JLabel} and 2 components.
     * 
     * @param titleLabel The {@link JLabel}.
     * @param mainComponent The main component.
     * @param lastComponent The secondary component.
     * @return A {@link JPanel}.
     */
    protected JPanel generatePanel(JLabel titleLabel, JComponent mainComponent, JComponent lastComponent) {
        JPanel panel = new JPanel(new BorderLayout(5, 5));
        if (titleLabel != null) {
            panel.add(titleLabel, BorderLayout.WEST);
        }
        if (mainComponent != null) {
            panel.add(mainComponent, BorderLayout.CENTER);
        }
        if (lastComponent != null) {
            panel.add(lastComponent, BorderLayout.EAST);
        }
        return panel;
    }

    /**
     * Generates a {@link JLabel} used to display a tile.
     * 
     * @param key The key to recover the title in {@link MessageManager}.
     * @return A {@link JLabel}.
     */
    protected JLabel generateTitleLabel(String key) {
        return new JLabel(messageManager.getMessage(key), SwingConstants.RIGHT);
    }

    /**
     * Sets {@link JComboBox} selected item and renderer and listens to that {@link JComboBox}.
     * 
     * @param comboBox The {@link JComboBox}.
     * @param renderer The renderer.
     * @param selectedItem The item to select.
     */
    protected <T> void setupComboBox(JComboBox<T> comboBox, ListCellRenderer<? super T> renderer, T selectedItem) {
        comboBox.setSelectedItem(selectedItem);
        if (renderer != null) {
            comboBox.setRenderer(renderer);
        }
        comboBox.addActionListener(this);
    }

    /**
     * Generates a {@link JComboBox} and listens to its changes.
     * 
     * @param renderer The renderer to set to the combobox.
     * @param selectedItem The item that should immediately be selected.
     * @param choices The choices.
     * @return A {@link JComboBox}.
     */
    protected <T> JComboBox<T> generateComboBox(ListCellRenderer<? super T> renderer, T selectedItem,
            @SuppressWarnings("unchecked") T... choices) {
        JComboBox<T> comboBox = new JComboBox<>(choices);
        setupComboBox(comboBox, renderer, selectedItem);
        return comboBox;
    }

    /**
     * Generates a {@link DynamicRenderingComboBox} and listens to its changes.
     * 
     * @param selectedItem The item that should immediately be selected.
     * @param choices The choices.
     * @return A {@link DynamicRenderingComboBox}.
     */
    protected <T> DynamicRenderingComboBox<T> generateDynamicComboBox(T selectedItem,
            @SuppressWarnings("unchecked") T... choices) {
        DynamicRenderingComboBox<T> comboBox = new DynamicRenderingComboBox<>(choices);
        setupComboBox(comboBox, null, selectedItem);
        return comboBox;
    }

    /**
     * Creates a {@link JToggleButton} with given icon and tooltip text, adds it to a {@link ButtonGroup}, and listens
     * to its selection changes.
     * 
     * @param imageKey The key to recover the icon from {@link Icons}.
     * @param tooltipKey The key to recover the tooltip text from {@link MessageManager}.
     * @param selected Whether the {@link JToggleButton} should be selected.
     * @param group The {@link ButtonGroup}.
     * @return A {@link JToggleButton}.
     */
    protected JToggleButton generateToggleButton(String imageKey, String tooltipKey, boolean selected,
            ButtonGroup group) {
        JToggleButton button = new JToggleButton(icons.getIcon(imageKey));
        button.setMargin(NO_MARGIN);
        button.setToolTipText(messageManager.getMessage(tooltipKey));
        button.setSelected(selected);
        group.add(button);
        button.addItemListener(this);
        return button;
    }

    /**
     * Generates a {@link JCheckBox} with given text and listens to it.
     * 
     * @param key The key to recover the {@link JCheckBox} text in {@link MessageManager}.
     * @param selected Whether the {@link JCheckBox} should be selected.
     * @return A {@link JCheckBox}.
     */
    protected ConstrainedCheckBox generateCheckBox(String key, boolean selected) {
        ConstrainedCheckBox checkBox = new ConstrainedCheckBox(messageManager.getMessage(key));
        checkBox.setToolTipText(checkBox.getText());
        checkBox.setOpaque(false);
        checkBox.setSelected(selected);
        checkBox.addItemListener(this);
        return checkBox;
    }

    /**
     * Generates a {@link JSpinner} that displays <code>int</code> values and listens to it.
     * 
     * @param value The spinner value.
     * @param minimum The spinner minimum value.
     * @param maximum The spinner maximum value.
     * @return A {@link JSpinner}.
     */
    protected JSpinner generateNumberSpinner(int value, int minimum, int maximum) {
        JSpinner spinner = new JSpinner(new SpinnerNumberModel(value, minimum, maximum, 1));
        spinner.setUI(new MetalSpinnerUI());
        spinner.addChangeListener(this);
        return spinner;
    }

    /**
     * Generates a {@link JButton} with given {@link ImageIcon} and tooltip text, and registers this {@link ALogViewer}
     * as an {@link ActionListener} of resulting {@link JButton}.
     * 
     * @param textKey The key to recover the tooltip text in {@link MessageManager}.
     * @param iconKey The key to recover the {@link ImageIcon} in {@link Icons}.
     *            <p>
     *            If corresponding {@link ImageIcon} is <code>null</code>, then tooltip text becomes text.
     *            </p>
     * @return A {@link JButton}.
     */
    protected JButton generateButton(final String textKey, final String iconKey) {
        ImageIcon icon = icons.getIcon(iconKey);
        String text = messageManager.getMessage(textKey);
        JButton button;
        if (icon == null) {
            button = new JButton(text);
        } else {
            button = new JButton(icon);
            button.setToolTipText(text);
        }
        button.setMargin(NO_MARGIN);
        button.addActionListener(this);
        return button;
    }

    /**
     * Generates a {@link JButton} with given {@link ImageIcon} and tooltip text, and registers this {@link ALogViewer}
     * as an {@link ActionListener} of resulting {@link JButton}.
     * 
     * @param key The key to recover both the tooltip text in {@link MessageManager} and the {@link ImageIcon} in
     *            {@link Icons}.
     *            <p>
     *            If corresponding {@link ImageIcon} is <code>null</code>, then tooltip text becomes text.
     *            </p>
     * @return A {@link JButton}.
     */
    protected JButton generateButton(final String key) {
        return generateButton(key, key);
    }

    /**
     * Utility method to create a {@link GridBagConstraints}.
     * 
     * @param gridx The {@link GridBagConstraints} gridx.
     * @param gridy The {@link GridBagConstraints} gridy.
     * @param weightx The {@link GridBagConstraints} weightx.
     * @param weighty The {@link GridBagConstraints} weighty.
     * @param top The {@link GridBagConstraints} top margin.
     * @param left The {@link GridBagConstraints} left margin.
     * @param bottom The {@link GridBagConstraints} bottom margin.
     * @param right The {@link GridBagConstraints} right margin.
     * @return A {@link GridBagConstraints}.
     */
    protected GridBagConstraints generateGridBagConstraints(int gridx, int gridy, double weightx, double weighty,
            int top, int left, int bottom, int right) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = gridx;
        constraints.gridy = gridy;
        constraints.weightx = weightx;
        constraints.weighty = weighty;
        constraints.insets = new Insets(top, left, bottom, right);
        constraints.anchor = GridBagConstraints.WEST;
        return constraints;
    }

    /**
     * Returns the filtered log level.
     * 
     * @return A {@link Level}.
     */
    public Level getFilteredLevel() {
        return (Level) levelComboBox.getSelectedItem();
    }

    /**
     * Sets the filtered log level.
     * 
     * @param level The filtered log level.
     */
    public void setFilteredLevel(Level level) {
        Level adaptedLevel = level == null ? Level.ALL : level;
        if (SwingUtilities.isEventDispatchThread()) {
            levelComboBox.setSelectedItem(adaptedLevel);
        } else {
            SwingUtilities.invokeLater(() -> {
                levelComboBox.setSelectedItem(adaptedLevel);
            });
        }
    }

    /**
     * Extracts the text to search for in logs.
     * 
     * @param caseSensitive Whether text is case sensitive. If <code>false</code>, the returned text will always be
     *            lower cased.
     * @return A {@link String}. <code>null</code> if there is no filter to apply.
     */
    protected String getFilteringText(boolean caseSensitive) {
        String text = filterTextField.getText();
        if (text != null) {
            text = text.trim();
            if (text.isEmpty()) {
                text = null;
            } else if (!caseSensitive) {
                text = text.toLowerCase();
            }
        }
        return text;
    }

    /**
     * Treats an error, using a custom message.
     * 
     * @param customMessage The custom message.
     * @param t The error.
     * @param Whether the error is not very important and should be considered as a warning.
     */
    protected abstract void treatError(String customMessage, Throwable t, boolean warning);

    /**
     * Returns whether a {@link LogData} matches filtered {@link Level}.
     * 
     * @param log The {@link LogData}.
     * @param level The filtered {@link Level}.
     * @param minimumLevel Whether filtered {@link Level} should be considered as minimum log level.
     * @return A <code>boolean</code>.
     */
    protected boolean hasGoodLevel(LogData log, Level level, boolean minimumLevel) {
        boolean levelOk;
        if ((level == null) || (level == Level.ALL)) {
            levelOk = true;
        } else {
            Level logLevel = log.getLevel();
            if (logLevel == level) {
                levelOk = true;
            } else if ((logLevel != null) && minimumLevel && (logLevel.compareTo(level) > 0)) {
                levelOk = true;
            } else {
                levelOk = false;
            }
        }
        return levelOk;
    }

    /**
     * Returns whether a {@link LogData} matches filtered text.
     * 
     * @param log The {@link LogData}.
     * @param filteredText The filtered text.
     * @param caseSensitive Whether text filtering should be case sensitive. If <code>false</code>,
     *            <code>filteredText</code> is expected to already be lower cased.
     * @return A <code>boolean</code>.
     */
    protected boolean hasGoodText(LogData log, String filteredText, boolean caseSensitive) {
        boolean textOk;
        if (filteredText == null) {
            textOk = true;
        } else {
            String message = log.getMessage();
            if (message == null) {
                textOk = false;
            } else {
                if (!caseSensitive) {
                    message = message.toLowerCase();
                }
                textOk = message.indexOf(filteredText) > -1;
            }
        }
        return textOk;
    }

    /**
     * Applies all filters in a {@link SwingWorker}.
     * 
     * @param forceUpdate Whether to force the update of the log component.
     * @param filteredLevel The filtered {@link Level}.
     * @param minimumLevel Whether <code>filteredLevel</code> should be considered as minimum level.
     * @param filteredText The filtered text.
     * @param caseSensitive Whether text filtering should be case sensitive.
     */
    protected void applyFilters(boolean forceUpdate, Level filteredLevel, boolean minimumLevel, String filteredText,
            boolean caseSensitive) {
        Level level = filteredLevel == null ? Level.ALL : filteredLevel;
        Level lastFilteredLevel = this.lastFilteredLevel;
        boolean wasCaseSensitive = this.wasCaseSensitive;
        String lastFilteredText = this.lastFilteredText;
        // JAVAAPI-617: refresh logs only when showing and something really did change in logs or filters.
        this.lastFilteredLevel = level;
        this.wasCaseSensitive = caseSensitive;
        this.lastFilteredText = filteredText;
        if (forceUpdate || (level != lastFilteredLevel) || (caseSensitive != wasCaseSensitive)
                || (!ObjectUtils.sameObject(filteredText, lastFilteredText))) {
            FilterWorker former = filterWorker;
            if (former != null) {
                former.setCanceled(true);
            }
            FilterWorker worker = new FilterWorker(logsToFilter, level, minimumLevel, filteredText, caseSensitive);
            filterWorker = worker;
            worker.execute();
        }
    }

    /**
     * Applies all filters in a {@link SwingWorker}.
     */
    protected void applyFilters(boolean forceUpdate) {
        boolean caseSensitive = filterCheckBox.isSelected();
        applyFilters(forceUpdate, getFilteredLevel(), levelCheckBox.isSelected(), getFilteringText(caseSensitive),
                caseSensitive);
    }

    @Override
    public boolean dataChanged() {
        return dataComponentDelegate.dataChanged(this);
    }

    @Override
    public void updateFromData() {
        applyFilters(true);
    }

    /**
     * Treats a {@link DocumentEvent} in order to launch expected text filtering.
     * 
     * @param e The {@link DocumentEvent}.
     */
    protected void treatDocumentEvent(DocumentEvent e) {
        if ((e != null) && (e.getDocument() == filterTextField.getDocument())) {
            applyFilters(false);
        }
    }

    /**
     * Asks user where to save logs and then saves them.
     */
    protected void saveLogs() {
        final LogData[] logs = logComponent.getLogs(); // save displayed logs
        if ((logs != null) && (logs.length > 0) && (logSaver.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)) {
            File toSave = logSaver.getSelectedFile();
            if (toSave != null) {
                FileFilter filter = logSaver.getFileFilter();
                if (filter instanceof ExtensionFileFilter) {
                    ExtensionFileFilter extensionFilter = (ExtensionFileFilter) filter;
                    if (!extensionFilter.getExtension().equalsIgnoreCase(FileUtils.getExtension(toSave))) {
                        toSave = new File(toSave.getAbsoluteFile() + FileChooserUtils.EXTENSION_SEPARATOR
                                + extensionFilter.getExtension());
                        logSaver.setSelectedFile(toSave);
                    }
                }
                int choice = JOptionPane.YES_OPTION;
                if (toSave.exists()) {
                    choice = JOptionPane.showConfirmDialog(this,
                            String.format(messageManager.getMessage("fr.soleil.lib.project.swing.log.file.overwrite"),
                                    toSave),
                            messageManager.getMessage("fr.soleil.lib.project.swing.warning"),
                            JOptionPane.YES_NO_OPTION);
                }
                final File output = toSave;
                if (choice == JOptionPane.YES_OPTION) {
                    // save log in a separated thread
                    new Thread("Save log in " + output.getAbsolutePath()) {
                        @Override
                        public void run() {
                            String extension = FileUtils.getExtension(output);
                            if (extension != null) {
                                ILogFileWriter writer = logFileWriters.get(extension.toLowerCase());
                                if (writer != null) {
                                    try {
                                        writer.writeLogs(output, logs);
                                    } catch (IOException e) {
                                        treatError(String.format(
                                                messageManager.getMessage("fr.soleil.lib.project.swing.log.save.error"),
                                                output.getAbsolutePath()), e, false);
                                    }
                                }
                            }
                        }
                    }.start();
                }
            }
        }
    }

    /**
     * Utility methods to copy logs to clipboard.
     */
    protected void copyLogs() {
        logComponent.getTransferHandler().exportToClipboard(logComponent,
                Toolkit.getDefaultToolkit().getSystemClipboard(), TransferHandler.COPY);
    }

    /**
     * Utility method to force a {@link JScrollBar} to scroll to its minimum value.
     * 
     * @param scrollBar The {@link JScrollBar}.
     */
    protected void scrollToMin(JScrollBar scrollBar) {
        if ((scrollBar.getValue() != scrollBar.getMinimum())) {
            scrollBar.setValue(scrollBar.getMinimum());
        }
    }

    /**
     * Utility method to force a {@link JScrollBar} to scroll to its maxnimum value.
     * 
     * @param scrollBar The {@link JScrollBar}.
     */
    protected void scrollToMax(JScrollBar scrollBar) {
        if ((scrollBar.getValue() < scrollBar.getMaximum() - scrollBar.getModel().getExtent())) {
            scrollBar.setValue(scrollBar.getMaximum());
        }
    }

    /**
     * Updates the background color of a {@link JScrollPane} and its sub components.
     * 
     * @param scrollPane The {@link JScrollPane}.
     * @param bg The background color to set.
     */
    protected void setScrollPaneBackground(JScrollPane scrollPane, Color bg) {
        if (scrollPane != null) {
            scrollPane.setBackground(bg);
            scrollPane.getViewport().setBackground(bg);
            scrollPane.getHorizontalScrollBar().setBackground(bg);
            scrollPane.getVerticalScrollBar().setBackground(bg);
        }
    }

    /**
     * Sets the general background color of this {@link ALogViewer} and its sub components.
     * 
     * @param bg The background color to set.
     */
    public void setAllBackgrounds(Color bg) {
        setBackground(bg);
        if (mainSplitPane != null) {
            mainSplitPane.setBackground(bg);
        }
        if (magnetismComboBox != null) {
            magnetismComboBox.setBackground(bg);
        }
        if (splitManagementComboBox != null) {
            splitManagementComboBox.setBackground(bg);
        }
        if (levelPanel != null) {
            levelPanel.setBackground(bg);
        }
        if (filterPanel != null) {
            filterPanel.setBackground(bg);
        }
        if (maxLogsPanel != null) {
            maxLogsPanel.setBackground(bg);
        }
        if (fontPanel != null) {
            fontPanel.setBackground(bg);
        }
        if (optionsPanel != null) {
            optionsPanel.setBackground(bg);
        }
        setScrollPaneBackground(logScrollPane, bg);
        if (managementPanel != null) {
            managementPanel.setBackground(bg);
        }
        setScrollPaneBackground(managementScrollPane, bg);
        if (levelComboBox != null) {
            levelComboBox.setBackground(bg);
        }
    }

    /**
     * Returns whether the tools to limit the number of logs are visible.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isLogLimitationVisible() {
        return maxLogsPanel.isVisible();
    }

    /**
     * Sets whether the tools to limit the number of logs should be visible.
     * 
     * @param visible whether the tools to limit the number of logs should be visible.
     */
    public void setLogLimitationVisible(boolean visible) {
        maxLogsPanel.setVisible(visible);
        repaint();
    }

    /**
     * Returns whether the tools to manage logs font size are visible.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isLogFontSizeManagementVisible() {
        return fontSizeLabel.isVisible();
    }

    /**
     * Sets visible or invisible the tools to manage logs font size are visible.
     * 
     * @param visible Whether the tools to manage logs font size should be visible visible.
     */
    public void setLogFontSizeManagementVisible(boolean visible) {
        fontSizeLabel.setVisible(visible);
        fontSizeSpinner.setVisible(visible);
        repaint();
    }

    /**
     * Returns whether the checkbox to enable/disable log colors is visible.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isLogColorManagementVisible() {
        return colorsEnabledCheckBox.isVisible();
    }

    /**
     * Sets visible or invisible the checkbox to enable/disable log colors is visible.
     * 
     * @param visible Whether the checkbox to enable/disable log colors should be visible.
     */
    public void setLogColorManagementVisible(boolean visible) {
        colorsEnabledCheckBox.setVisible(visible);
        repaint();
    }

    /**
     * Returns whether the tools to filter log levels are visible.
     * 
     * @return A <code>boolean</code>
     */
    public boolean isLogLevelFilteringVisible() {
        return levelLabel.isVisible();
    }

    /**
     * Sets visible or invisible the tools to filter log levels.
     * 
     * @param visible >hether the tools to filter log levels should be visible.
     */
    public void setLogLevelFilteringVisible(boolean visible) {
        levelLabel.setVisible(visible);
        levelComboBox.setVisible(visible);
        levelCheckBox.setVisible(visible);
        repaint();
    }

    /**
     * Returns whether the tools to filter log texts are visible.
     * 
     * @return A <code>boolean</code>
     */
    public boolean isLogTextFilteringVisible() {
        return filterLabel.isVisible();
    }

    /**
     * Sets visible or invisible the tools to filter log texts.
     * 
     * @param visible >hether the tools to filter log texts should be visible.
     */
    public void setLogTextFilteringVisible(boolean visible) {
        filterLabel.setVisible(visible);
        filterTextField.setVisible(visible);
        filterCheckBox.setVisible(visible);
        repaint();
    }

    /**
     * Return whether this {@link ALogViewer} should automatically compute best log magnetism when receiving logs.
     * 
     * @return Whether this {@link ALogViewer} should automatically compute best log magnetism when receiving logs.
     */
    public boolean isAutoScrollMagnetism() {
        return autoScrollMagnetism;
    }

    /**
     * Sets whether this {@link ALogViewer} should automatically compute best log magnetism when receiving logs.
     * 
     * @param autoScrollMagnetism Whether this {@link ALogViewer} should automatically compute best log magnetism when
     *            receiving logs.
     */
    public void setAutoScrollMagnetism(boolean autoScrollMagnetism) {
        this.autoScrollMagnetism = autoScrollMagnetism;
    }

    /**
     * Adds an {@link ActionListener} to clear log button.
     * 
     * @param listener The {@link ActionListener} to add.
     */
    public void addClearButtonListener(ActionListener listener) {
        if (listener != null) {
            clearLogListeners.add(listener);
        }
    }

    /**
     * Removes an {@link ActionListener} from clear log button.
     * 
     * @param listener The {@link ActionListener} to remove.
     */
    public void removeClearButtonListener(ActionListener listener) {
        if (listener != null) {
            clearLogListeners.remove(listener);
        }
    }

    @Override
    public void setForeground(Color fg) {
        super.setForeground(fg);
        if (logComponent != null) {
            logComponent.setForeground(fg);
        }
        if (htmlFileFilter != null) {
            htmlFileFilter.setDefaultColor(fg);
        }
    }

    @Override
    public Font getLevelFont() {
        Font font = DEFAULT_LOG_FONT;
        if (logComponent != null) {
            font = logComponent.getLevelFont();
        }
        return font;
    }

    @Override
    public void setLevelFont(Font font) {
        Font oldFont = getLevelFont();
        if (logComponent != null) {
            logComponent.setLevelFont(font);
            if (logScrollPane != null) {
                logScrollPane.revalidate();
            }
        }
        if (htmlFileFilter != null) {
            htmlFileFilter.setLevelFont(font);
        }
        if (levelComboBox != null) {
            ((LevelListCellRenderer) levelComboBox.getRenderer()).setLevelFont(font);
        }
        if (font != oldFont) {
            if ((font != null) && (fontSizeSpinner != null)) {
                fontSizeSpinner.setValue(font.getSize());
            }
            revalidate();
            repaint();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e != null) {
            if (e.getSource() == levelComboBox) {
                applyFilters(false);
            } else if (e.getSource() == magnetismComboBox) {
                // JAVAAPI-634: deactivate auto scroll magnetism when defined by user or programmer
                if (!fromAutoScrollMagnetism) {
                    setAutoScrollMagnetism(false);
                }
                fromAutoScrollMagnetism = false;
                ScrollMagnetism magnetism = getScrollMagnetism();
                if (magnetism == null) {
                    magnetism = ScrollMagnetism.NONE;
                }
                switch (magnetism) {
                    case TOP:
                        scrollTop = true;
                        scrollBottom = false;
                        scrollLeft = false;
                        scrollRight = false;
                        break;
                    case BOTTOM:
                        scrollTop = false;
                        scrollBottom = true;
                        scrollLeft = false;
                        scrollRight = false;
                        break;
                    case LEFT:
                        scrollTop = false;
                        scrollBottom = false;
                        scrollLeft = true;
                        scrollRight = false;
                        break;
                    case RIGHT:
                        scrollTop = false;
                        scrollBottom = false;
                        scrollLeft = false;
                        scrollRight = true;
                        break;
                    case TOP_LEFT:
                        scrollTop = true;
                        scrollBottom = false;
                        scrollLeft = true;
                        scrollRight = false;
                        break;
                    case TOP_RIGHT:
                        scrollTop = true;
                        scrollBottom = false;
                        scrollLeft = false;
                        scrollRight = true;
                        break;
                    case BOTTOM_LEFT:
                        scrollTop = false;
                        scrollBottom = true;
                        scrollLeft = true;
                        scrollRight = false;
                        break;
                    case BOTTOM_RIGHT:
                        scrollTop = false;
                        scrollBottom = true;
                        scrollLeft = false;
                        scrollRight = true;
                        break;
                    case NONE:
                    default:
                        scrollTop = false;
                        scrollBottom = false;
                        scrollLeft = false;
                        scrollRight = false;
                        break;
                }
            } else if (e.getSource() == pauseButton) {
                paused = true;
                playPausePanelLayout.show(playPausePanel, PLAY);
            } else if (e.getSource() == resumeButton) {
                paused = false;
                playPausePanelLayout.show(playPausePanel, PAUSE);
                logsToFilter = logs;
                applyFilters(false);
            } else if (e.getSource() == clearButton) {
                clearLogs();
                for (ActionListener listener : clearLogListeners) {
                    listener.actionPerformed(e);
                }
            } else if (e.getSource() == saveButton) {
                saveLogs();
            } else if (e.getSource() == copyButton) {
                copyLogs();
            } else if (e.getSource() == optionsButton) {
                if (optionsDialog == null) {
                    optionsDialog = new JDialog(WindowSwingUtils.getWindowForComponent(optionsButton),
                            optionsButton.getToolTipText());
                    optionsDialog.setModal(false);
                    optionsDialog.setContentPane(optionsPanel);
                }
                if (!optionsDialog.isVisible()) {
                    optionsDialog.pack();
                    optionsDialog.setLocationRelativeTo(optionsButton);
                    optionsDialog.setVisible(true);
                }
            } else if (e.getSource() == splitManagementComboBox) {
                setSplitManagement((SplitManagement) splitManagementComboBox.getSelectedItem());
            }
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e != null) {
            if (e.getSource() == levelCheckBox) {
                Level level = getFilteredLevel();
                if ((level != null) && (level != Level.ALL)) {
                    boolean caseSensitive = filterCheckBox.isSelected();
                    applyFilters(false, level, levelCheckBox.isSelected(), getFilteringText(caseSensitive),
                            caseSensitive);
                }
            } else if (e.getSource() == filterCheckBox) {
                boolean caseSensitive = filterCheckBox.isSelected();
                String text = getFilteringText(caseSensitive);
                if (text != null) {
                    applyFilters(false, getFilteredLevel(), levelCheckBox.isSelected(), text, caseSensitive);
                }
            } else if (e.getSource() == colorsEnabledCheckBox) {
                setLevelColorEnabled(colorsEnabledCheckBox.isSelected());
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        treatDocumentEvent(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        treatDocumentEvent(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        treatDocumentEvent(e);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (e != null) {
            if (e.getSource() == fontSizeSpinner) {
                int newSize = ((Number) fontSizeSpinner.getValue()).intValue();
                Font font = getLevelFont();
                if ((font != null) && (newSize != font.getSize())) {
                    setLevelFont(font.deriveFont((float) newSize));
                }
            } else if (e.getSource() == maxLogsSpinner) {
                setMaxLogs(((Number) maxLogsSpinner.getValue()).intValue());
            }
        }
    }

    @Override
    public void componentResized(ComponentEvent e) {
        if ((e != null) && (e.getComponent() == logComponent)) {
            if (scrollTop) {
                scrollToMin(logScrollPane.getVerticalScrollBar());
            } else if (scrollBottom) {
                scrollToMax(logScrollPane.getVerticalScrollBar());
            }
            if (scrollLeft) {
                scrollToMin(logScrollPane.getHorizontalScrollBar());
            } else if (scrollRight) {
                scrollToMax(logScrollPane.getHorizontalScrollBar());
            }
        }
    }

    @Override
    public void componentMoved(ComponentEvent e) {
        // not managed
    }

    @Override
    public void componentShown(ComponentEvent e) {
        // not managed
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        // not managed
    }

    @Override
    public LogData[] getLogs() {
        LogData[] logs = this.logs;
        return logs == null ? null : logs.clone();
    }

    @Override
    public void addLogs(LogData... logs) {
        synchronized (logsLock) {
            setLogs(true, ArrayUtils.addDataToArray(this.logs, logs));
        }
    }

    @Override
    public void setLogs(LogData... logs) {
        setLogs(false, logs);
    }

    @Override
    public void setLogs(boolean keepLastLogs, LogData... logs) {
        boolean filter = false;
        ScrollMagnetism bestScrollMagnetism = null;
        synchronized (logsLock) {
            LogData[] newLogs = ArrayUtils.getLimitedArray(logs, maxLogs, keepLastLogs);
            this.keepLastLogs = keepLastLogs;
            if (this.logs != newLogs) {
                this.logs = newLogs;
                // JAVAAPI-617: remember logs changed
                dataComponentDelegate.setDataChanged(this, true);
                if (!paused) {
                    LogData[] formerLogs = this.logsToFilter;
                    this.logsToFilter = this.logs;
                    LogData[] toFilter = this.logsToFilter;
                    filter = true;
                    // JAVAAPI-634: compute best scroll magnetism
                    if (isAutoScrollMagnetism() && (formerLogs == null || formerLogs.length < 2) && (toFilter != null)
                            && toFilter.length > 1) {
                        bestScrollMagnetism = computeBestScrollMagnetism(toFilter);
                    }
                }
            }
        }
        if (filter) {
            if (bestScrollMagnetism != null) {
                fromAutoScrollMagnetism = true;
                setScrollMagnetism(bestScrollMagnetism);
            }
            dataComponentDelegate.updateContent(this);
        }
    }

    @Override
    public void clearLogs() {
        logs = null;
        logsToFilter = null;
        dataComponentDelegate.setDataChanged(this, false);
        if (SwingUtilities.isEventDispatchThread()) {
            logComponent.clearLogs();
        } else {
            SwingUtilities.invokeLater(() -> {
                logComponent.clearLogs();
            });
        }
    }

    @Override
    public int getMaxLogs() {
        int max = ((Number) maxLogsSpinner.getValue()).intValue();
        return max < 1 ? Integer.MAX_VALUE : max;
    }

    @Override
    public void setMaxLogs(int maxLogs) {
        logComponent.setMaxLogs(maxLogs < 1 ? Integer.MAX_VALUE : maxLogs);
        Integer max = Integer.valueOf(maxLogs == Integer.MAX_VALUE ? 0 : maxLogs);
        if (!max.equals(maxLogsSpinner.getValue())) {
            maxLogsSpinner.setValue(max);
        }
    }

    @Override
    public boolean isLevelColorEnabled() {
        return logComponent.isLevelColorEnabled();
    }

    @Override
    public void setLevelColorEnabled(boolean levelColorEnabled) {
        logComponent.setLevelColorEnabled(levelColorEnabled);
        htmlFileFilter.setLevelColorEnabled(levelColorEnabled);
        ListCellRenderer<?> renderer = levelComboBox.getRenderer();
        if (renderer instanceof LevelListCellRenderer) {
            ((LevelListCellRenderer) renderer).setLevelColorEnabled(levelColorEnabled);
        }
        if (levelColorEnabled != colorsEnabledCheckBox.isSelected()) {
            colorsEnabledCheckBox.setSelected(levelColorEnabled);
        }
        repaint();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link SwingWorker} that will apply all log filters.
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class FilterWorker extends SwingWorker<LogData[], Void> implements ICancelable {
        protected LogData[] logs;
        protected Level level;
        protected boolean minimumLevel;
        protected String filteredText;
        protected boolean caseSensitive;
        protected volatile boolean canceled;

        public FilterWorker(LogData[] logs, Level level, boolean minimumLevel, String filteredText,
                boolean caseSensitive) {
            super();
            this.logs = logs;
            this.level = level;
            this.minimumLevel = minimumLevel;
            this.filteredText = filteredText;
            this.caseSensitive = caseSensitive;
            this.canceled = false;
        }

        @Override
        protected LogData[] doInBackground() throws Exception {
            LogData[] filteredLogs = null;
            if ((logs != null) && (logs.length > 0)) {
                Collection<LogData> filteredList = new ArrayList<>(logs.length);
                try {
                    for (LogData log : logs) {
                        if (canceled) {
                            break;
                        }
                        if ((log != null) && hasGoodLevel(log, level, minimumLevel)
                                && hasGoodText(log, filteredText, caseSensitive)) {
                            filteredList.add(log);
                        }
                    }
                    if ((!canceled) && (!filteredList.isEmpty())) {
                        filteredLogs = filteredList.toArray(new LogData[filteredList.size()]);
                    }
                } finally {
                    filteredList.clear();
                }
            }
            return filteredLogs;
        }

        @Override
        protected void done() {
            try {
                if (!canceled) {
                    logComponent.setLogs(keepLastLogs, get());
                }
            } catch (InterruptedException e) {
                treatError(messageManager.getMessage("fr.soleil.lib.project.swing.log.filter.interrupted"), e, true);
            } catch (ExecutionException e) {
                treatError(messageManager.getMessage("fr.soleil.lib.project.swing.log.filter.error"), e.getCause(),
                        false);
            } finally {
                logs = null;
                filteredText = null;
                level = null;
            }
        }

        @Override
        public void setCanceled(boolean canceled) {
            this.canceled = canceled;
        }

        @Override
        public boolean isCanceled() {
            return canceled;
        }
    }

    /**
     * An {@link Enum} used to represent how UI is splitted.
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    public static enum SplitManagement {
        /**
         * Logs are at the top, controls are at the bottom
         */
        TOP_BOTTOM,
        /**
         * Logs are at the left, controls are at the right
         */
        LEFT_RIGHT,
        /**
         * Logs are at the bottom, controls are at the top
         */
        BOTTOM_TOP,
        /**
         * Logs are at the right, controls are at the left
         */
        RIGHT_LEFT
    }

}
