/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.combo;

import javax.swing.JComboBox;
import javax.swing.plaf.basic.BasicComboPopup;

/**
 * A {@link BasicComboPopup} that takes account of associated {@link JComboBox}'s opacity
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class OpaqueAdaptedComboPopup extends BasicComboPopup {

    private static final long serialVersionUID = -3799879801455110463L;

    public OpaqueAdaptedComboPopup(JComboBox<?> combo) {
        super(combo);
    }

    @Override
    protected void togglePopup() {
        boolean opaque = comboBox.isOpaque();
        setOpaque(opaque);
        list.setOpaque(opaque);
        super.togglePopup();
    }
}
