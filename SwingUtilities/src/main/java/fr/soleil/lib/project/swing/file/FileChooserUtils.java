/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.file;

import java.awt.Component;
import java.io.File;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JFileChooser;

import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.swing.SwingUtilitiesConstants;

/**
 * A class that interacts with a {@link JFileChooser}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class FileChooserUtils implements SwingUtilitiesConstants {

    public static final String EXTENSION_SEPARATOR = ".";

    protected static final String OPEN_KEY = "fr.soleil.lib.project.swing.file.chooser.open";
    protected static final String SAVE_KEY = "fr.soleil.lib.project.swing.file.chooser.save";
    protected static final String FILE_KEY = "fr.soleil.lib.project.swing.file.chooser.file";
    protected static final String DIRECTORY_KEY = "fr.soleil.lib.project.swing.file.chooser.directory";

    /**
     * Adds a filtered file extension to a {@link JFileChooser}.
     * 
     * @param chooser The {@link JFileChooser}.
     * @param extension The filtered file extension.
     * @param select Whether to select corresponding filter in {@link JFileChooser}.
     */
    public static void addExtensionFileFilter(JFileChooser chooser, String extension, boolean select) {
        if ((chooser != null) && (extension != null) && (!extension.isEmpty())) {
            ExtensionFileFilter filter = new ExtensionFileFilter(extension);
            chooser.addChoosableFileFilter(filter);
            if (select) {
                chooser.setFileFilter(filter);
            }
        }
    }

    /**
     * Opens a {@link JFileChooser} popup and returns the selected file path.
     * 
     * @param source The {@link Component} relatively to which to open a file choosing popup.
     * @param open whether to use an "open" or "save" file chooser.
     * @param selectDirectories whether to ask the {@link JFileChooser} to selected directories instead of files.
     * @param directory the file chooser start directory.
     * @return A {@link String}.
     */
    public static String showFileChooserDialog(Component source, boolean open, boolean selectDirectories,
            String directory, String... allowedExtensions) {
        return showFileChooserDialog(source, open, selectDirectories, directory, null, allowedExtensions);
    }

    /**
     * Opens a {@link JFileChooser} popup and returns the selected file path.
     * 
     * @param source The {@link Component} relatively to which to open a file choosing popup.
     * @param open whether to use an "open" or "save" file chooser.
     * @param selectDirectories whether to ask the {@link JFileChooser} to selected directories instead of files.
     * @param directory the file chooser start directory.
     * @param preferredExtension The preferred extension (the 1st one that should be seen in file extension list). Used
     *            only if <code>selectDirectories</code> is <code>false</code>.
     * @param allowedExtensions The allowed file extension list. Used only if <code>selectDirectories</code> is
     *            <code>false</code>.
     * @return A {@link String}.
     */
    public static String showFileChooserDialog(Component source, boolean open, boolean selectDirectories,
            String directory, String preferredExtension, String... allowedExtensions) {
        String filename = null;
        JFileChooser fileChooser;
        if (directory == null) {
            fileChooser = new JFileChooser();
        } else {
            File temp = new File(directory);
            if (temp.exists()) {
                if (temp.isDirectory()) {
                    fileChooser = new JFileChooser(temp);
                } else {
                    fileChooser = new JFileChooser(temp.getParentFile());
                    if (!selectDirectories) {
                        fileChooser.setSelectedFile(temp);
                    }
                }
            } else {
                fileChooser = new JFileChooser();
            }
        }
        fileChooser.setFileSelectionMode(selectDirectories ? JFileChooser.DIRECTORIES_ONLY : JFileChooser.FILES_ONLY);
        List<String> extensions = new ArrayList<String>();
        String extensionToUse = preferredExtension;
        boolean contains = false;
        if (open) {
            fileChooser.setDialogTitle(String.format(DEFAULT_MESSAGE_MANAGER.getMessage(OPEN_KEY),
                    DEFAULT_MESSAGE_MANAGER.getMessage(selectDirectories ? DIRECTORY_KEY : FILE_KEY)));
            if ((!selectDirectories) && (allowedExtensions != null) && (allowedExtensions.length > 0)) {
                for (String extension : allowedExtensions) {
                    if (extension != null) {
                        extensions.add(extension);
                        if (extension.equalsIgnoreCase(extensionToUse)) {
                            extensionToUse = extension;
                            contains = true;
                        }
                    }
                }
            }
        } else {
            fileChooser.setDialogTitle(String.format(DEFAULT_MESSAGE_MANAGER.getMessage(SAVE_KEY),
                    DEFAULT_MESSAGE_MANAGER.getMessage(selectDirectories ? DIRECTORY_KEY : FILE_KEY)));
            if ((!selectDirectories) && (allowedExtensions != null) && (allowedExtensions.length > 0)) {
                for (String extension : allowedExtensions) {
                    extensions.add(extension);
                    if (extension.equalsIgnoreCase(extensionToUse)) {
                        extensionToUse = extension;
                        contains = true;
                    }
                }
            }
        }
        Collections.sort(extensions, Collator.getInstance());
        if (contains) {
            extensions.remove(extensionToUse);
            extensions.add(0, extensionToUse);
        }
        boolean select = true;
        for (String extension : extensions) {
            addExtensionFileFilter(fileChooser, extension, select);
            select = false;
        }
        if (!select) {
            fileChooser.setAcceptAllFileFilterUsed(false);
        }
        extensions.clear();
        int returnVal = JFileChooser.CANCEL_OPTION;
        if (open) {
            returnVal = fileChooser.showOpenDialog(source);
        } else {
            returnVal = fileChooser.showSaveDialog(source);
        }

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = fileChooser.getSelectedFile();
            if (f != null) {
                filename = f.getAbsolutePath();
                if (fileChooser.getFileFilter() instanceof ExtensionFileFilter) {
                    ExtensionFileFilter selector = (ExtensionFileFilter) fileChooser.getFileFilter();
                    String extension = selector.getExtension();
                    if ((extension != null) && (!extension.trim().isEmpty())) {
                        String currentExtension = FileUtils.getExtension(filename);
                        if (!extension.equalsIgnoreCase(currentExtension)) {
                            filename = filename + EXTENSION_SEPARATOR + extension;
                        }
                    }
                }
            }
        }
        return filename;
    }

}
