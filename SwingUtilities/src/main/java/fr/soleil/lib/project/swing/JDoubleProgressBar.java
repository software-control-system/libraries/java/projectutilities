/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.image.BufferedImage;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.ToolTipManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A double progress bar. Based on ATK JSmoothProgressar
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class JDoubleProgressBar extends JComponent {

    private static final long serialVersionUID = -8890760185411003283L;

    protected static final Color DARK_PROGRESS_DEFAULT_1 = new Color(110, 110, 160, 255);
    protected static final Color BACK_PROGRESS_DEFAULT_1 = new Color(156, 156, 206, 255);
    protected static final Color LIGHT_PROGRESS_DEFAULT_1 = new Color(200, 200, 240, 255);
    protected static final Color DARK_PROGRESS_DEFAULT_2 = new Color(110, 160, 110, 200);
    protected static final Color BACK_PROGRESS_DEFAULT_2 = new Color(156, 206, 156, 200);
    protected static final Color LIGHT_PROGRESS_DEFAULT_2 = new Color(200, 240, 200, 200);

    // Local declarations
    protected int off_x;
    protected int off_y;
    protected double currentValueBG;
    protected double maxValueBG;
    protected double currentValueFG;
    protected double maxValueFG;
    protected Color darkProgressBG;
    protected Color backProgressBG;
    protected Color lightProgressBG;
    protected Color darkProgressFG;
    protected Color backProgressFG;
    protected Color lightProgressFG;
    protected Insets borderMargin;
    protected double ratioBG;
    protected double ratioFG;
    protected FontRenderContext frc;
    protected String bgName;
    protected String fgName;
    private int[] dgSize;
    private int ascent;
    private int prSize;
    private boolean displayText;
    private String[] digit;
    private int totalSize;
    private String bgTooltip;
    private String fgTooltip;
    private String text;
    protected boolean transparent;

    /**
     * Construct a progress bar.
     */
    public JDoubleProgressBar() {
        super();
        darkProgressBG = DARK_PROGRESS_DEFAULT_1;
        backProgressBG = BACK_PROGRESS_DEFAULT_1;
        lightProgressBG = LIGHT_PROGRESS_DEFAULT_1;
        darkProgressFG = DARK_PROGRESS_DEFAULT_2;
        backProgressFG = BACK_PROGRESS_DEFAULT_2;
        lightProgressFG = LIGHT_PROGRESS_DEFAULT_2;
        frc = null;
        bgName = null;
        fgName = null;
        digit = new String[] { ObjectUtils.EMPTY_STRING, ObjectUtils.EMPTY_STRING, ObjectUtils.EMPTY_STRING };
        bgTooltip = null;
        fgTooltip = null;
        text = null;
        transparent = true;
        init();
        off_x = 0;
        off_y = 0;
        currentValueBG = 0;
        maxValueBG = 100;
        setOpaque(false);
        setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        setFont(new Font("Dialog", Font.BOLD, 12));
        updateDigit();
        ToolTipManager toolTipManager = ToolTipManager.sharedInstance();
        toolTipManager.registerComponent(this);
    }

    /**
     * Sets the shadow and background colors used to paint the back progress bar.
     * 
     * @param back Background color
     * @param light Light color used for shadow
     * @param dark Dark color used for shadow
     */
    public void setBGProgressBarColors(Color back, Color light, Color dark) {
        darkProgressBG = new Color(dark.getRed(), dark.getGreen(), dark.getBlue(), 255);
        lightProgressBG = new Color(light.getRed(), light.getGreen(), light.getBlue(), 255);
        backProgressBG = new Color(back.getRed(), back.getGreen(), back.getBlue(), 255);
    }

    /**
     * Sets the shadow and background colors used to paint the front progress bar.
     * 
     * @param back Background color
     * @param light Light color used for shadow
     * @param dark Dark color used for shadow
     */
    public void setFGProgressBarColors(Color back, Color light, Color dark) {
        darkProgressFG = new Color(dark.getRed(), dark.getGreen(), dark.getBlue(), 200);
        lightProgressFG = new Color(light.getRed(), light.getGreen(), light.getBlue(), 200);
        backProgressFG = new Color(back.getRed(), back.getGreen(), back.getBlue(), 200);
    }

    @Override
    public void setBorder(Border b) {
        super.setBorder(b);
        if (b == null) {
            borderMargin = new Insets(0, 0, 0, 0);
        } else {
            borderMargin = getBorder().getBorderInsets(this);
        }
    }

    /**
     * Sets the progress value.
     * 
     * @param v Progress value
     */
    public void setBGValue(double v) {
        if (v >= maxValueBG) {
            currentValueBG = maxValueBG;
        } else {
            currentValueBG = v;
        }
        updateDigit();
        repaint();
    }

    /**
     * Returns the current progress value.
     * 
     * @see #setValue
     */
    public double getBGValue() {
        return currentValueBG;
    }

    /**
     * Sets the maximum progress value.
     * 
     * @param v Maximum
     */
    public void setBGMaximum(double v) {
        maxValueBG = v;
        if (maxValueBG <= currentValueFG) {
            currentValueBG = maxValueBG;
        }
        updateDigit();
        repaint();
    }

    /**
     * Returns the maximum progress value.
     */
    public double getBGMaximum() {
        return maxValueBG;
    }

    /**
     * Sets the progress value.
     * 
     * @param v Progress value
     */
    public void setFGValue(double v) {
        if (v >= maxValueFG) {
            currentValueFG = maxValueFG;
        } else {
            currentValueFG = v;
        }
        updateDigit();
        repaint();
    }

    /**
     * Returns the current progress value.
     * 
     * @see #setValue
     */
    public double getFGValue() {
        return currentValueFG;
    }

    /**
     * Sets the maximum progress value.
     * 
     * @param v Maximum
     */
    public void setFGMaximum(double v) {
        maxValueFG = v;
        if (maxValueFG <= currentValueFG) {
            currentValueFG = maxValueFG;
        }
        updateDigit();
        repaint();
    }

    /**
     * Returns the maximum progress value.
     */
    public double getFGMaximum() {
        return maxValueFG;
    }

    /**
     * Sets an offset (in pixels) for drawing the progress string.
     * 
     * @param x Horizontal offset
     * @param y Vertical offset
     */
    public void setValueOffsets(int x, int y) {
        off_x = x;
        off_y = y;
        repaint();
    }

    // Paint the component
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        int w = getWidth();
        int h = getHeight();
        Color color = g.getColor();
        g.setColor(getBackground());
        if (!isTransparent()) {
            g.fillRect(0, 0, w, h);
        }
        g.setColor(color);
        int wr = w - (borderMargin.left + borderMargin.right + 1);
        int hr = h - (borderMargin.bottom + borderMargin.top + 1);
        // Draw the progress bar
        int bpos1 = 0;
        if (!Double.isNaN(ratioBG)) {
            bpos1 = (int) Math.rint(wr * ratioBG);
        }
        g.setColor(backProgressBG);
        g.fillRect(borderMargin.left, borderMargin.top, bpos1, hr);
        g.setColor(lightProgressBG);
        g.drawLine(borderMargin.left, borderMargin.top, borderMargin.left + bpos1, borderMargin.top);
        g.drawLine(borderMargin.left, borderMargin.top, borderMargin.left, borderMargin.top + hr);
        g.setColor(darkProgressBG);
        g.drawLine(borderMargin.left + bpos1, borderMargin.top, borderMargin.left + bpos1, borderMargin.top + hr);
        g.drawLine(borderMargin.left + bpos1, borderMargin.top + hr, borderMargin.left, borderMargin.top + hr);
        int bpos2 = 0;
        if (!Double.isNaN(ratioFG)) {
            bpos2 = (int) Math.rint(wr * ratioFG);
        }
        g.setColor(backProgressFG);
        g.fillRect(borderMargin.left, borderMargin.top, bpos2, hr);
        g.setColor(lightProgressFG);
        g.drawLine(borderMargin.left, borderMargin.top, borderMargin.left + bpos2, borderMargin.top);
        g.drawLine(borderMargin.left, borderMargin.top, borderMargin.left, borderMargin.top + hr);
        g.setColor(darkProgressFG);
        g.drawLine(borderMargin.left + bpos2, borderMargin.top, borderMargin.left + bpos2, borderMargin.top + hr);
        g.drawLine(borderMargin.left + bpos2, borderMargin.top + hr, borderMargin.left, borderMargin.top + hr);

        // Draw the string
        if (displayText) {
            synchronized (this) {
                Graphics2D g2 = (Graphics2D) g;
                g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                g.setFont(getFont());
                if (text == null) {
                    int xpos = ((w - totalSize) / 2) + off_x;
                    int ypos = ((hr + ascent) / 2) + off_y + borderMargin.top;
                    int sum = 0;
                    if (Double.isNaN(ratioFG)) {
                        g.setColor(getForeground());
                        g.drawString(digit[0], xpos + sum, ypos);
                        sum += dgSize[10];
                        g.drawString(digit[1], xpos + sum, ypos);
                        sum += dgSize[11];
                        g.drawString(digit[2], xpos + sum, ypos);
                        sum += dgSize[10];
                    } else {
                        for (int i = 0; i < digit.length; i++) {
                            g.setColor(getForeground());
                            g.drawString(digit[i], xpos + sum, ypos);
                            sum += dgSize[Integer.parseInt(digit[i])];
                        }
                    }
                    g.drawString("%", xpos + sum, ypos);
                } else {
                    int totalSize = 0;
                    int[] size = new int[text.length()];
                    for (int i = 0; i < text.length(); i++) {
                        String character = ObjectUtils.EMPTY_STRING + text.charAt(i);
                        size[i] = measureString(character).width + 1;
                        totalSize += size[i];
                    }
                    int xpos = ((w - totalSize) / 2) + off_x;
                    int ypos = ((hr + ascent) / 2) + off_y + borderMargin.top;
                    int sum = 0;
                    for (int i = 0; i < text.length(); i++) {
                        String character = ObjectUtils.EMPTY_STRING + text.charAt(i);
                        g.setColor(getForeground());
                        g.drawString(character, xpos + sum, ypos);
                        sum += size[i];
                    }
                    size = null;
                }
            }
        }

        g.setColor(color);

        // Draw the border
        paintBorder(g);
    }

    protected Color selectColor(boolean getFg) {
        Color selected;
        if (getFg) {
            selected = getForeground();
        } else {
            selected = getBackground();
        }
        return selected;
    }

    protected synchronized void updateDigit() {
        ratioBG = currentValueBG / maxValueBG;
        ratioFG = currentValueFG / maxValueFG;
        // Check limits
        if (ratioBG < 0.0) {
            ratioBG = 0.0;
        }
        if (ratioBG > 1.0) {
            ratioBG = 1.0;
        }
        if (ratioFG < 0.0) {
            ratioFG = 0.0;
        }
        if (ratioFG > 1.0) {
            ratioFG = 1.0;
        }

        totalSize = prSize;
        if (Double.isNaN(ratioFG)) {
            digit = new String[] { "N", "a", "N" };
            totalSize += dgSize[11] + 2 * dgSize[10];
        } else {
            // ratio is in [0,1]
            int ratio = (int) Math.rint(ratioFG * 100);
            int c = ratio / 100;
            int d = (ratio % 100) / 10;
            int u = (ratio % 100) % 10;
            if (c == 0) {
                if (d == 0) {
                    digit = new String[] { Integer.toString(u) };
                } else {
                    digit = new String[] { Integer.toString(d), Integer.toString(u) };
                }
            } else {
                digit = new String[] { Integer.toString(c), Integer.toString(d), Integer.toString(u) };
            }
            for (int i = 0; i < digit.length; i++) {
                totalSize += dgSize[Integer.parseInt(digit[i])];
            }
        }
    }

    public LineMetrics getLineMetrics(String s) {
        return getFont().getLineMetrics(s, frc);
    }

    public Dimension measureString(String s) {
        Rectangle bounds = getFont().getStringBounds(s.replaceAll(" ", "-"), frc).getBounds();
        int w = bounds.width + 1;
        int h = bounds.height + 1;
        return new Dimension(w, h);
    }

    protected void init() {
        // Init FontRenderContext
        if (frc == null) {
            BufferedImage img = new BufferedImage(500, 500, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = (Graphics2D) img.getGraphics();
            g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            frc = g.getFontRenderContext();
            g.dispose();
            img.flush();
            img = null;
        }
    }

    @Override
    public void setFont(Font f) {
        super.setFont(f);
        // Measures digit of this font.
        dgSize = new int[12];
        for (int i = 0; i < 10; i++) {
            dgSize[i] = measureString(Integer.toString(i)).width + 1;
        }
        dgSize[10] = measureString("N").width + 1;
        dgSize[11] = measureString("a").width + 1;
        prSize = measureString("%").width;
        ascent = (int) (getLineMetrics("100%").getAscent() + 0.5);
    }

    /**
     * @return the bgName
     */
    public String getBGName() {
        return bgName;
    }

    /**
     * @param bgName the bgName to set
     */
    public void setBGName(String bgName) {
        this.bgName = bgName;
    }

    /**
     * @return the fgName
     */
    public String getFGName() {
        return fgName;
    }

    /**
     * @param fgName the fgName to set
     */
    public void setFGName(String fgName) {
        this.fgName = fgName;
    }

    @Override
    public String getToolTipText(MouseEvent event) {
        StringBuilder buffer = new StringBuilder();
        boolean displayBgName = (bgName != null);
        boolean displayFgName = (fgName != null);
        int w = getWidth();
        int wr = w - (borderMargin.left + borderMargin.right + 1);
        // Draw the progress bar
        int bpos1 = 0;
        if (!Double.isNaN(ratioBG)) {
            bpos1 = (int) Math.rint(wr * ratioBG);
        }
        int bpos2 = 0;
        if (!Double.isNaN(ratioFG)) {
            bpos2 = (int) Math.rint(wr * ratioFG);
        }

        String formatedBGRatio = "NaN";
        if (!Double.isNaN(ratioBG)) {
            formatedBGRatio = String.format(Locale.UK, "%5.2f", Double.valueOf(ratioBG * 100));
        }
        String formatedFGRatio = "NaN";
        if (!Double.isNaN(ratioFG)) {
            formatedFGRatio = String.format(Locale.UK, "%5.2f", Double.valueOf(ratioFG * 100));
        }
        if (event.getX() <= bpos2 + borderMargin.left) {
            if (fgTooltip != null) {
                buffer.append(fgTooltip);
            } else {
                if (displayFgName) {
                    buffer.append(fgName).append(": ");
                }
                buffer.append(formatedFGRatio).append("%");
            }
        } else if (event.getX() <= bpos1 + borderMargin.left) {
            if (bgTooltip != null) {
                buffer.append(bgTooltip);
            } else {
                if (displayBgName) {
                    buffer.append(bgName).append(": ");
                }
                buffer.append(formatedBGRatio).append("%");
            }
        } else {
            if (fgTooltip != null) {
                buffer.append(fgTooltip);
            } else {
                if (displayFgName) {
                    buffer.append(fgName).append(": ");
                }
                buffer.append(formatedFGRatio).append("%");
            }
            buffer.append(" - ");
            if (bgTooltip != null) {
                buffer.append(bgTooltip);
            } else {
                if (displayBgName) {
                    buffer.append(bgName).append(": ");
                }
                buffer.append(formatedBGRatio).append("%");
            }
        }
        return buffer.toString();
    }

    /**
     * @return the displayFGProgress
     */
    public boolean isDisplayText() {
        return displayText;
    }

    /**
     * @param displayFGProgress the displayFGProgress to set
     */
    public void setDisplayFGProgress(boolean displayFGProgress) {
        this.displayText = displayFGProgress;
    }

    /**
     * @return the bgTooltip
     */
    public String getBGTooltip() {
        return bgTooltip;
    }

    /**
     * @param bgTooltip the bgTooltip to set
     */
    public void setBGTooltip(String bgTooltip) {
        this.bgTooltip = bgTooltip;
    }

    /**
     * @return the fgTooltip
     */
    public String getFGTooltip() {
        return fgTooltip;
    }

    /**
     * @param fgTooltip the fgTooltip to set
     */
    public void setFGTooltip(String fgTooltip) {
        this.fgTooltip = fgTooltip;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
        revalidate();
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension size = super.getPreferredSize();
        if (size.width <= 1 || size.height <= 1) {
            if ((getText() == null) || getText().isEmpty()) {
                size = measureString("100%");
                if (getText() == null) {
                    updateDigit();
                    size.width = totalSize;
                }
            } else {
                int totalSize = 0;
                int[] fsize = new int[text.length()];
                for (int i = 0; i < text.length(); i++) {
                    String character = ObjectUtils.EMPTY_STRING + text.charAt(i);
                    fsize[i] = measureString(character).width + 1;
                    totalSize += fsize[i];
                }
                fsize = null;
                size = measureString(getText());
                size.width = totalSize;
            }
            if (size.width < 90) {
                size.width = 100;
            } else {
                size.width += 10;
            }
            size.width += borderMargin.left + borderMargin.right + 1;
            size.height += borderMargin.top + borderMargin.bottom + 1;
        }
        return size;
    }

    public boolean isTransparent() {
        return transparent;
    }

    public void setTransparent(boolean transparent) {
        this.transparent = transparent;
    }

}
