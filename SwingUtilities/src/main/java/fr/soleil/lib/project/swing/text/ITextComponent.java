/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ComponentListener;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeListener;

import javax.swing.SwingConstants;
import javax.swing.border.Border;

/**
 * An interface that publishes the common methods to all text <code>JComponent</code>s.
 * <p>
 * Typically, you can force a <code>JLabel</code> or a <code>JTextfield</code> to implement that interface with no code
 * impact.
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface ITextComponent extends SwingConstants {

    /**
     * Returns this {@link ITextComponent}'s text.
     * 
     * @return A {@link String}.
     */
    public String getText();

    /**
     * Sets this {@link ITextComponent}'s text.
     * 
     * @param text The text to set.
     */
    public void setText(String text);

    /**
     * Returns this {@link ITextComponent}'s font.
     * 
     * @return A {@link Font}.
     */
    public Font getFont();

    /**
     * Sets this {@link ITextComponent}'s font.
     * 
     * @param font The {@link Font} to set.
     */
    public void setFont(Font font);

    /**
     * Returns whether this {@link ITextComponent} currently has the focus.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean hasFocus();

    /**
     * Forces this {@link ITextComponent}'s to revalidate its preferred dimensions.
     */
    public void revalidate();

    /**
     * Forces this {@link ITextComponent} to repaint.
     */
    public void repaint();

    /**
     * Returns whether this {@link ITextComponent} is enabled.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isEnabled();

    /**
     * Sets whether this {@link ITextComponent} should be enabled.
     * 
     * @param enabled Whether this {@link ITextComponent} should be enabled.
     */
    public void setEnabled(boolean enabled);

    /**
     * Adds a {@link ComponentListener} to this {@link ITextComponent}.
     * 
     * @param listener The {@link ComponentListener} to add.
     */
    public void addComponentListener(ComponentListener listener);

    /**
     * Removes a {@link ComponentListener} from this {@link ITextComponent}.
     * 
     * @param listener The {@link ComponentListener} to remove.
     */
    public void removeComponentListener(ComponentListener listener);

    /**
     * Adds a {@link FocusListener} to this {@link ITextComponent}.
     * 
     * @param listener The {@link FocusListener} to add.
     */
    public void addFocusListener(FocusListener listener);

    /**
     * Removes a {@link FocusListener} from this {@link ITextComponent}.
     * 
     * @param listener The {@link FocusListener} to remove.
     */
    public void removeFocusListener(FocusListener listener);

    /**
     * Adds a {@link PropertyChangeListener} to this {@link ITextComponent}.
     * 
     * @param listener The {@link PropertyChangeListener} to add.
     */
    public void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Removes a {@link PropertyChangeListener} from this {@link ITextComponent}.
     * 
     * @param listener The {@link PropertyChangeListener} to remove.
     */
    public void removePropertyChangeListener(PropertyChangeListener listener);

    /**
     * Returns this {@link ITextComponent}'s border.
     * 
     * @return A {@link Border}.
     */
    public Border getBorder();

    /**
     * Sets this {@link ITextComponent}'s border.
     * 
     * @param border The {@link Border} to set.
     */
    public void setBorder(Border border);

    /**
     * Returns this {@link ITextComponent}'s width.
     * 
     * @return An <code>int</code>.
     */
    public int getWidth();

    /**
     * Returns this {@link ITextComponent}'s height.
     * 
     * @return An <code>int</code>.
     */
    public int getHeight();

    /**
     * Returns this {@link ITextComponent}'s size.
     * 
     * @return A {@link Dimension}.
     */
    public Dimension getSize();

    /**
     * Returns whether this {@link ITextComponent} is opaque.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isOpaque();

    /**
     * Sets whether this {@link ITextComponent} should be opaque.
     * 
     * @param opaque Whether this {@link ITextComponent} should be opaque.
     */
    public void setOpaque(boolean opaque);

    /**
     * Returns this {@link ITextComponent}'s background color.
     * 
     * @return A {@link Color}.
     */
    public Color getBackground();

    /**
     * Sets this {@link ITextComponent}'s background color.
     * 
     * @param background The {@link Color} to set
     */
    public void setBackground(Color background);

    /**
     * Returns this {@link ITextComponent}'s foreground color.
     * 
     * @return A {@link Color}.
     */
    public Color getForeground();

    /**
     * Sets this {@link ITextComponent}'s foreground color.
     * 
     * @param foreground The {@link Color} to set
     */
    public void setForeground(Color foreground);

}
