/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicSpinnerUI;

/**
 * A {@link BasicSpinnerUI} that draws its arrow buttons with Metal look&feel
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class MetalSpinnerUI extends BasicSpinnerUI {

    protected static final Color TOP_COLOR = new Color(221, 232, 243);
    protected static final Color CENTER_COLOR = Color.WHITE;
    protected static final Color BOTTOM_COLOR = new Color(184, 207, 229);

    protected static final Color DISABLED_BACKGROUND = new JPanel().getBackground();

    public MetalSpinnerUI() {
        super();
    }

    @Override
    protected Component createPreviousButton() {
        Component c = createArrowButton(SwingConstants.SOUTH);
        c.setName("Spinner.previousButton");
        installPreviousButtonListeners(c);
        return c;
    }

    @Override
    protected Component createNextButton() {
        Component c = createArrowButton(SwingConstants.NORTH);
        c.setName("Spinner.nextButton");
        installNextButtonListeners(c);
        return c;
    }

    private MetalArrowButton createArrowButton(int direction) {
        MetalArrowButton b = new MetalArrowButton(direction);
        Border buttonBorder = UIManager.getBorder("Spinner.arrowButtonBorder");
        if (buttonBorder instanceof UIResource) {
            // Wrap the border to avoid having the UIResource be replaced by
            // the ButtonUI. This is the opposite of using BorderUIResource.
            b.setBorder(new CompoundBorder(buttonBorder, null));
        } else {
            b.setBorder(buttonBorder);
        }
        b.setInheritsPopupMenu(true);
        return b;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class MetalArrowButton extends JButton implements SwingConstants {

        private static final long serialVersionUID = -7032011420609487168L;

        protected final int direction;
        protected final Color shadow;
        protected final Color darkShadow;
        protected final Color highlight;

        public MetalArrowButton(int direction, Color background, Color shadow, Color darkShadow, Color highlight) {
            super();
            this.direction = direction;
            setRequestFocusEnabled(false);
            setBackground(background);
            this.shadow = shadow;
            this.darkShadow = darkShadow;
            this.highlight = highlight;
        }

        @Override
        public Color getBackground() {
            return isEnabled() ? super.getBackground() : DISABLED_BACKGROUND;
        }

        /**
         * Creates a {@code BasicArrowButton} whose arrow
         * is drawn in the specified direction.
         *
         * @param direction the direction of the arrow; one of
         *            {@code SwingConstants.NORTH}, {@code SwingConstants.SOUTH},
         *            {@code SwingConstants.EAST} or {@code SwingConstants.WEST}
         */
        public MetalArrowButton(int direction) {
            this(direction, UIManager.getColor("control"), UIManager.getColor("controlShadow"),
                    UIManager.getColor("controlDkShadow"), UIManager.getColor("controlLtHighlight"));
        }

        @Override
        public void paint(Graphics g) {
            Color origColor;
            boolean isPressed, isEnabled;
            int w, h, size;

            w = getSize().width;
            h = getSize().height;
            origColor = g.getColor();
            isPressed = getModel().isPressed();
            isEnabled = isEnabled();

            int x1 = 1, y1 = 1, w1 = w - 2, h1 = h - 2;

            if (isEnabled && (g instanceof Graphics2D)) {
                Graphics2D g2 = (Graphics2D) g;
                int h2 = (int) (h1 * 0.3);
                GradientPaint paint = new GradientPaint(x1, y1, TOP_COLOR, x1, h2, CENTER_COLOR);
                g2.setPaint(paint);
                int h3 = h1 + 1 - h2;
                g2.fillRect(x1, y1, w1, h2);
                paint = new GradientPaint(x1, h2, CENTER_COLOR, x1, h1, BOTTOM_COLOR);
                g2.setPaint(paint);
                g2.fillRect(x1, h2, w1, h3);
            } else {
                g.setColor(getBackground());
                g.fillRect(x1, y1, w1, h1);
            }

            /// Draw the proper Border
            if (getBorder() != null && !(getBorder() instanceof UIResource)) {
                paintBorder(g);
            } else if (isPressed) {
                g.setColor(shadow);
                g.drawRect(0, 0, w - 1, h - 1);
            } else {
                // Using the background color set above
                g.drawLine(0, 0, 0, h - 1);
                g.drawLine(1, 0, w - 2, 0);

                g.setColor(highlight); // inner 3D border
                g.drawLine(1, 1, 1, h - 3);
                g.drawLine(2, 1, w - 3, 1);

                g.setColor(shadow); // inner 3D border
                g.drawLine(1, h - 2, w - 2, h - 2);
                g.drawLine(w - 2, 1, w - 2, h - 3);

                g.setColor(darkShadow); // black drop shadow __|
                g.drawLine(0, h - 1, w - 1, h - 1);
                g.drawLine(w - 1, h - 1, w - 1, 0);
            }

            // If there's no room to draw arrow, bail
            if (h < 5 || w < 5) {
                g.setColor(origColor);
                return;
            }

            if (isPressed) {
                g.translate(1, 1);
            }

            // Draw the arrow
            size = Math.min((h - 4) / 3, (w - 4) / 3);
            size = Math.max(size, 2);
            paintTriangle(g, (w - size) / 2, (h - size) / 2, size, direction, isEnabled);

            // Reset the Graphics back to it's original settings
            if (isPressed) {
                g.translate(-1, -1);
            }
            g.setColor(origColor);

        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(16, 16);
        }

        @Override
        public Dimension getMinimumSize() {
            return new Dimension(5, 5);
        }

        @Override
        public Dimension getMaximumSize() {
            return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
        }

        @Override
        public boolean isFocusTraversable() {
            return false;
        }

        /**
         * Paints a triangle.
         *
         * @param g the {@code Graphics} to draw to
         * @param x the x coordinate
         * @param y the y coordinate
         * @param size the size of the triangle to draw
         * @param direction the direction in which to draw the arrow;
         *            one of {@code SwingConstants.NORTH},
         *            {@code SwingConstants.SOUTH}, {@code SwingConstants.EAST} or
         *            {@code SwingConstants.WEST}
         * @param isEnabled whether or not the arrow is drawn enabled
         */
        public void paintTriangle(Graphics g, int x, int y, int size, int direction, boolean isEnabled) {
            Color oldColor = g.getColor();
            int mid, i, j;

            j = 0;
            size = Math.max(size, 2);
            mid = (size / 2) - 1;

            g.translate(x, y);
            if (isEnabled)
                g.setColor(darkShadow);
            else
                g.setColor(shadow);

            switch (direction) {
                case NORTH:
                    for (i = 0; i < size; i++) {
                        g.drawLine(mid - i, i, mid + i, i);
                    }
                    if (!isEnabled) {
                        g.setColor(highlight);
                        g.drawLine(mid - i + 2, i, mid + i, i);
                    }
                    break;
                case SOUTH:
                    if (!isEnabled) {
                        g.translate(1, 1);
                        g.setColor(highlight);
                        for (i = size - 1; i >= 0; i--) {
                            g.drawLine(mid - i, j, mid + i, j);
                            j++;
                        }
                        g.translate(-1, -1);
                        g.setColor(shadow);
                    }

                    j = 0;
                    for (i = size - 1; i >= 0; i--) {
                        g.drawLine(mid - i, j, mid + i, j);
                        j++;
                    }
                    break;
                case WEST:
                    for (i = 0; i < size; i++) {
                        g.drawLine(i, mid - i, i, mid + i);
                    }
                    if (!isEnabled) {
                        g.setColor(highlight);
                        g.drawLine(i, mid - i + 2, i, mid + i);
                    }
                    break;
                case EAST:
                    if (!isEnabled) {
                        g.translate(1, 1);
                        g.setColor(highlight);
                        for (i = size - 1; i >= 0; i--) {
                            g.drawLine(j, mid - i, j, mid + i);
                            j++;
                        }
                        g.translate(-1, -1);
                        g.setColor(shadow);
                    }

                    j = 0;
                    for (i = size - 1; i >= 0; i--) {
                        g.drawLine(j, mid - i, j, mid + i);
                        j++;
                    }
                    break;
            }
            g.translate(-x, -y);
            g.setColor(oldColor);
        }
    }
}
