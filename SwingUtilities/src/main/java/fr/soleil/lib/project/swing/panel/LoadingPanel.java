/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.panel;

import java.awt.CardLayout;
import java.awt.Component;

import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.SwingUtilitiesConstants;

/**
 * A {@link JPanel} that contains a {@link Component} and that can be put in a "loading" mode.
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 * @param <C> The type of {@link Component} this panel contains
 */
public class LoadingPanel<C extends Component> extends JPanel implements SwingUtilitiesConstants {

    private static final long serialVersionUID = 5256725528247886932L;

    public static final ImageIcon DEFAULT_LOADING_ICON = DEFAULT_ICONS
            .getIcon("fr.soleil.lib.project.swing.state.loading");
    private static final String DEFAULT_LOADING_TEXT = "Loading...";
    private static final String LAYOUT_COMPONENT = "component";
    private static final String LAYOUT_LOADING = "loading";

    private C component;
    private final Component emptyComponent = Box.createGlue();
    private final CardLayout layout;
    private final JLabel loadingLabel;
    private String lastLayout;
    // A lock that ensures not to display component while modifying it
    private final Object componentLock;

    /**
     * Default constructor
     */
    public LoadingPanel() {
        this(null);
    }

    /**
     * Constructs this {@link LoadingPanel} with a given {@link Component}
     * 
     * @param component This {@link LoadingPanel}'s {@link Component}
     */
    public LoadingPanel(C component) {
        super();
        componentLock = new Object();
        this.component = component;
        layout = new CardLayout();
        setLayout(layout);
        lastLayout = LAYOUT_COMPONENT;
        loadingLabel = new JLabel(DEFAULT_LOADING_TEXT, DEFAULT_LOADING_ICON, JLabel.CENTER);
        loadingLabel.setVerticalAlignment(JLabel.CENTER);
        add(loadingLabel, LAYOUT_LOADING);
        add(this.component, LAYOUT_COMPONENT);
        layout.show(this, lastLayout);
    }

    /**
     * Returns the {@link Component} this panel contains
     * 
     * @return A {@link Component}
     */
    public C getComponent() {
        return component;
    }

    /**
     * Sets the {@link Component} this panel should contain
     * 
     * @param component The {@link Component} this panel should contain
     */
    protected void setComponent(C component) {
        if (this.component != component) {
            synchronized (componentLock) {
                layout.show(this, LAYOUT_LOADING);
                if (this.component == null) {
                    remove(emptyComponent);
                } else {
                    remove(this.component);
                }
                this.component = component;
                if (this.component == null) {
                    add(emptyComponent, LAYOUT_COMPONENT);
                } else {
                    add(this.component, LAYOUT_COMPONENT);
                }
                revalidate();
                layout.show(this, lastLayout);
                repaint();
            }
        }
    }

    /**
     * Returns whether this {@link LoadingPanel} is in a loading mode
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isLoading() {
        return LAYOUT_LOADING.equals(lastLayout);
    }

    /**
     * Sets this {@link LoadingPanel} in a loading/loaded mode
     * 
     * @param loading A <code>boolean</code> value. <code>TRUE</code> to set this
     *            {@link LoadingPanel} in a loading mode.
     */
    public void setLoading(boolean loading) {
        synchronized (componentLock) {
            lastLayout = (loading ? LAYOUT_LOADING : LAYOUT_COMPONENT);
            layout.show(this, lastLayout);
            repaint();
        }
    }

    /**
     * Returns the text displayed in loading mode
     * 
     * @return A {@link String}
     */
    public String getLoadingText() {
        return loadingLabel.getText();
    }

    /**
     * Sets the text to display in loading mode
     * 
     * @param text The text to set. If <code>null</code>, a default text is used.
     */
    public void setLoadingText(String text) {
        if (text == null) {
            text = DEFAULT_LOADING_TEXT;
        }
        if (!ObjectUtils.sameObject(text, getLoadingText())) {
            loadingLabel.setText(text);
            loadingLabel.revalidate();
            if (isLoading()) {
                revalidate();
                repaint();
            }
        }
    }

    /**
     * Returns the image displayed in loading mode
     * 
     * @return An {@link Icon}
     */
    public Icon getLoadingIcon() {
        return loadingLabel.getIcon();
    }

    /**
     * Sets the image to display in loading mode
     * 
     * @param loadingIcon The image to set, represented as an {@link Icon}
     */
    public void setLoadingIcon(Icon loadingIcon) {
        if (!ObjectUtils.sameObject(loadingIcon, getLoadingIcon())) {
            loadingLabel.setIcon(loadingIcon);
            loadingLabel.revalidate();
            if (isLoading()) {
                revalidate();
                repaint();
            }
        }
    }

    public static void main(String[] args) {
        JFrame testFrame = new JFrame(LoadingPanel.class.getSimpleName() + " test");
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JLabel component = new JLabel("This is the loaded text!");
        final LoadingPanel<JLabel> panel = new LoadingPanel<JLabel>(component);
        panel.setLoading(true);
        testFrame.setContentPane(panel);
        testFrame.setSize(300, 100);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
        new Thread() {
            @Override
            public void run() {
                try {
                    sleep(3000);
                    panel.setLoading(false);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            };
        }.start();
    }
}
