package fr.soleil.lib.project.swing.border;

import java.awt.Color;

import javax.swing.border.LineBorder;

/**
 * A {@link LineBorder} for which the line color can be changed.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class SettableColorLineBorder extends LineBorder {

    private static final long serialVersionUID = -954792311453371930L;

    public SettableColorLineBorder(Color color, int thickness, boolean roundedCorners) {
        super(color == null ? Color.BLACK : color, thickness < 0 ? 0 : thickness, roundedCorners);
    }

    public SettableColorLineBorder(Color color, int thickness) {
        super(color == null ? Color.BLACK : color, thickness < 0 ? 0 : thickness);
    }

    public SettableColorLineBorder(Color color) {
        super(color == null ? Color.BLACK : color);
    }

    /**
     * Sets this {@link SettableColorLineBorder}'s line color.
     * 
     * @param color The line color to set.
     */
    public void setLineColor(Color color) {
        this.lineColor = color == null ? Color.BLACK : color;
    }

    /**
     * Sets this {@link SettableColorLineBorder}'s line thickness.
     * 
     * @param thickness The line thickness to set.
     */
    public void setThickness(int thickness) {
        this.thickness = thickness < 0 ? 0 : thickness;
    }

}
