/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import javax.swing.Icon;
import javax.swing.SwingConstants;

/**
 * An interface for an {@link ITextComponent} that may have an icon.
 * <p>
 * Typically, you can force a <code>JLabel</code> or a <code>JButton</code> to implement that interface with no code
 * impact.
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IIconTextComponent extends ITextComponent {

    public Icon getIcon();

    public void setIcon(Icon icon);

    /**
     * Returns the gap between text and icon.
     * 
     * @return An <code>int</code>.
     */
    public int getIconTextGap();

    /**
     * If both the icon and text properties are set, this property defines the space between them.
     * 
     * @param iconTextGap The gap between text and icon.
     */
    public void setIconTextGap(int iconTextGap);

    /**
     * Returns the alignment of the icon and text along the X axis.
     * 
     * @return An <code>int</code>, one of the following values:
     *         <ul>
     *         <li>{@link SwingConstants#RIGHT}</li>
     *         <li>{@link SwingConstants#LEFT}</li>
     *         <li>{@link SwingConstants#CENTER}</li>
     *         <li>{@link SwingConstants#LEADING}</li>
     *         <li>{@link SwingConstants#TRAILING}</li>
     *         </ul>
     */
    public int getHorizontalAlignment();

    /**
     * Sets the alignment of the icon and text along the X axis.
     * 
     * @param alignment The alignment of the icon and text along the X axis.
     *            Can be one of the following values:
     *            <ul>
     *            <li>{@link SwingConstants#RIGHT}</li>
     *            <li>{@link SwingConstants#LEFT}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#LEADING}</li>
     *            <li>{@link SwingConstants#TRAILING}</li>
     *            </ul>
     */
    public void setHorizontalAlignment(int alignment);

    /**
     * Returns the alignment of the icon and text along the Y axis.
     * 
     * @return An <code>int</code>, one of the following values:
     *         <ul>
     *         <li>{@link SwingConstants#TOP}</li>
     *         <li>{@link SwingConstants#CENTER}</li>
     *         <li>{@link SwingConstants#BOTTOM}</li>
     *         </ul>
     */
    public int getVerticalAlignment();

    /**
     * Sets the alignment of the icon and text along the Y axis.
     * 
     * @param alignment The alignment of the icon and text along the Y axis.
     *            Can be one of the following values:
     *            <ul>
     *            <li>{@link SwingConstants#TOP}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#BOTTOM}</li>
     *            </ul>
     */
    public void setVerticalAlignment(int alignment);

    /**
     * Returns the horizontal position of the text relative to the icon.
     * 
     * @return An <code>int</code>, one of the following values:
     *         <ul>
     *         <li>{@link SwingConstants#RIGHT}</li>
     *         <li>{@link SwingConstants#LEFT}</li>
     *         <li>{@link SwingConstants#CENTER}</li>
     *         <li>{@link SwingConstants#LEADING}</li>
     *         <li>{@link SwingConstants#TRAILING}</li>
     *         </ul>
     */
    public int getHorizontalTextPosition();

    /**
     * Sets the horizontal position of the text relative to the icon.
     * 
     * @param textPosition The horizontal position of the text relative to the icon.
     *            Can be one of the following values:
     *            <ul>
     *            <li>{@link SwingConstants#RIGHT}</li>
     *            <li>{@link SwingConstants#LEFT}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#LEADING}</li>
     *            <li>{@link SwingConstants#TRAILING}</li>
     *            </ul>
     */
    public void setHorizontalTextPosition(int textPosition);

    /**
     * Returns the vertical position of the text relative to the icon (or of the icon relative to text if text is bigger
     * than icon).
     * 
     * @return An <code>int</code>, one of the following values:
     *         <ul>
     *         <li>{@link SwingConstants#TOP}</li>
     *         <li>{@link SwingConstants#CENTER}</li>
     *         <li>{@link SwingConstants#BOTTOM}</li>
     *         </ul>
     */
    public int getVerticalTextPosition();

    /**
     * Sets the vertical position of the text relative to the icon (or of the icon relative to text if text is bigger
     * than icon).
     * 
     * @param textPosition The vertical position of the text relative to the icon (or of the icon relative to text if
     *            text is bigger than icon).
     *            Can be one of the following values:
     *            <ul>
     *            <li>{@link SwingConstants#TOP}</li>
     *            <li>{@link SwingConstants#CENTER}</li>
     *            <li>{@link SwingConstants#BOTTOM}</li>
     *            </ul>
     */
    public void setVerticalTextPosition(int textPosition);

}
