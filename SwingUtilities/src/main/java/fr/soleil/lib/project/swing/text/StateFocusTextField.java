/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Color;
import java.awt.Font;

import javax.swing.text.Document;

import fr.soleil.lib.project.resource.MessageManager;

/**
 * Basic implementation of {@link AStateFocusTextField}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class StateFocusTextField extends AStateFocusTextField {

    private static final long serialVersionUID = -7424735173113308349L;

    public StateFocusTextField(MessageManager messageManager) {
        super(messageManager);
    }

    public StateFocusTextField(Document doc, String text, int columns, MessageManager messageManager) {
        super(doc, text, columns, messageManager);
    }

    public StateFocusTextField(int columns, MessageManager messageManager) {
        super(columns, messageManager);
    }

    public StateFocusTextField(String text, int columns, MessageManager messageManager) {
        super(text, columns, messageManager);
    }

    public StateFocusTextField(String text, MessageManager messageManager) {
        super(text, messageManager);
    }

    @Override
    public Color getValidBackground() {
        return super.getValidBackground();
    }

    @Override
    public void setValidBackground(Color validBackground) {
        super.setValidBackground(validBackground);
    }

    @Override
    public Font getValidFont() {
        return super.getValidFont();
    }

    @Override
    public void setValidFont(Font numberFont) {
        super.setValidFont(numberFont);
    }

    @Override
    public synchronized void setValidValue(String t) {
        super.setValidValue(t);
    }

}
