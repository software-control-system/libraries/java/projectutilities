/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import javax.swing.JSplitPane;

/**
 * A class used for interactions with {@link JSplitPane}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class SplitPaneUtils {

    private SplitPaneUtils() {
        // hide constructor
    }

    /**
     * Returns whether a {@link JSplitPane} divider is obviously invalid.
     * 
     * @param dim The {@link JSplitPane}(s reference dimension (width or height).
     * @param dividerSize The divider size.
     * @param dividerLocation The divider location.
     * @param delta The previously calculated <code>dim - dividerSize</code>.
     * @return A <code>boolean</code>. <code>true</code> if divider should be considered as invalid.
     */
    protected static boolean isInvalidDivider(int dim, int dividerSize, int dividerLocation, double delta) {
        return (dim <= 0 || dividerSize < 0 || dividerSize >= dim || dividerLocation > delta);
    }

    /**
     * Returns the proportional divider location <i>(i.e. the one you can set with
     * {@link JSplitPane#setDividerLocation(double)})</i> of a {@link JSplitPane}.
     * 
     * @param splitPane The {@link JSplitPane}.
     * @return A <code>double</code>. <code>NaN</code> if <code>splitpane</code> is <code>null</code> or if the
     *         proportional location could'nt be calculated due to dimensions or divider location.
     */
    public static double getDividerLocation(JSplitPane splitPane) {
        double dividerLocation;
        if (splitPane == null) {
            dividerLocation = Double.NaN;
        } else {
            int divSize = splitPane.getDividerSize(), divLocation = splitPane.getDividerLocation();
            double delta;
            if (splitPane.getOrientation() == JSplitPane.HORIZONTAL_SPLIT) {
                // Components splits horizontally, i.e. left-right
                int width = splitPane.getWidth();
                delta = width - divSize;
                if (isInvalidDivider(width, divSize, divLocation, delta)) {
                    dividerLocation = Double.NaN;
                } else {
                    dividerLocation = divLocation / delta;
                }
            } else {
                // Components splits vertically, i.e. top-bottom
                int height = splitPane.getHeight();
                delta = height - divSize;
                if (isInvalidDivider(height, divSize, divLocation, delta)) {
                    dividerLocation = Double.NaN;
                } else {
                    dividerLocation = divLocation / delta;
                }
            }
        }
        return dividerLocation;
    }

}
