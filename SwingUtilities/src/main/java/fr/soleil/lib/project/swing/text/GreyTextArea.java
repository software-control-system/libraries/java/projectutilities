/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import java.awt.Color;

import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.text.Document;

/**
 * A JTextArea with a grey background when not editable.
 * 
 * @author MAINGUY
 * 
 */
public class GreyTextArea extends JTextArea {

    private static final long serialVersionUID = -8896892083979868126L;

    private static final Color INACTIVE_BACKGROUND = (Color) UIManager.get("TextField.inactiveBackground");
    private static final Color ACTIVE_BACKGROUND = (Color) UIManager.get("TextField.background");

    /**
     * 
     */
    public GreyTextArea() {
        super();
    }

    /**
     * @param text
     */
    public GreyTextArea(final String text) {
        super(text);
    }

    /**
     * @param doc
     */
    public GreyTextArea(final Document doc) {
        super(doc);
    }

    /**
     * @param rows
     * @param columns
     */
    public GreyTextArea(final int rows, final int columns) {
        super(rows, columns);
    }

    /**
     * @param text
     * @param rows
     * @param columns
     */
    public GreyTextArea(final String text, final int rows, final int columns) {
        super(text, rows, columns);
    }

    /**
     * @param doc
     * @param text
     * @param rows
     * @param columns
     */
    public GreyTextArea(final Document doc, final String text, final int rows, final int columns) {
        super(doc, text, rows, columns);
    }

    /* (non-Javadoc)
     * @see javax.swing.text.JTextComponent#setEditable(boolean)
     */
    @Override
    public void setEditable(final boolean editable) {
        if (editable) {
            setBackground(ACTIVE_BACKGROUND);
        } else {
            setBackground(INACTIVE_BACKGROUND);
        }
        super.setEditable(editable);
    }
}
