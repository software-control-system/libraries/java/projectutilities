/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.renderer;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.listener.TableRowHeightResetter;
import fr.soleil.lib.project.xmlhelpers.HtmlEscape;

/**
 * A {@link DefaultTableCellRenderer} able to render multiple lines text.
 * Combined with a {@link TableRowHeightResetter}, this allows to have a {@link JTable} that renders multiple lines in a
 * cell and dynamically adapts its row height to each cell.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class MultiLinesCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 8379506945699427110L;

    private static final int MAX_HEIGHT = 500;

    protected final JTextArea area;
    protected final JPanel centeringPanel;
    protected boolean alwaysUseTextArea;
    // boolean used to avoid changing height immediately during cell rendering
    protected boolean immediatelyComputeBestHeight;

    public MultiLinesCellRenderer() {
        this(false);
    }

    public MultiLinesCellRenderer(boolean alwaysUseTextArea) {
        super();
        area = new JTextArea();
        area.setLineWrap(false);
        area.setWrapStyleWord(false);
        area.setEditable(false);
        area.setOpaque(false);
        area.setBorder(new EmptyBorder(0, 0, 0, 0));
        centeringPanel = new JPanel(new GridBagLayout());
        GridBagConstraints areaConstraints = new GridBagConstraints();
        areaConstraints.fill = GridBagConstraints.BOTH;
        areaConstraints.gridx = 0;
        areaConstraints.gridy = 0;
        areaConstraints.weightx = 1;
        areaConstraints.weighty = 0;
        areaConstraints.anchor = GridBagConstraints.CENTER;
        centeringPanel.add(area, areaConstraints);
        this.alwaysUseTextArea = alwaysUseTextArea;
        immediatelyComputeBestHeight = true;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        JComponent comp;
        JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        String text = label.getText();
        if (alwaysUseTextArea || text.indexOf('\n') > -1) {
            comp = centeringPanel;
            area.setText(text);
            area.setFont(label.getFont());
            area.setForeground(label.getForeground());
            centeringPanel.setBackground(label.getBackground());
            centeringPanel.setBorder(label.getBorder());
            label.setText(ObjectUtils.EMPTY_STRING);
        } else {
            comp = label;
        }
        comp.setToolTipText(text == null || text.isEmpty() ? null
                : new StringBuilder("<html><body>").append(HtmlEscape.escape(text, true, true)).append("</body></html>")
                        .toString());
        if (immediatelyComputeBestHeight) {
            applyBestHeight(table, row, comp);
        }
        return comp;
    }

    protected void applyBestHeight(JTable table, int row, JComponent comp) {
        int compHeight = computeBestHeight(comp, table);
        int rowHeight = table.getRowHeight(row);
        if (compHeight > rowHeight) {
            table.setRowHeight(row, compHeight);
        }
    }

    /**
     * Computes best height for rendering component.
     * 
     * @param comp The component.
     * @param table The {@link JTable} to render.
     * @return An <code>int</code>: the best height.
     */
    protected int computeBestHeight(JComponent comp, JTable table) {
        // In order to avoid freezes, limit allowed height
        return Math.min(MAX_HEIGHT, comp.getPreferredSize().height + (table.getIntercellSpacing().height * 2));
    }

    /**
     * Returns whether this renderer will always use a {@link JTextArea} to render table cells.
     * 
     * @return Whether this renderer will always use a {@link JTextArea} to render table cells.
     *         <p>
     *         <code>false</code> by default, which means by default a {@link JTextArea} will be used only for multiple
     *         lines texts.
     *         </p>
     */
    public boolean isAlwaysUseTextArea() {
        return alwaysUseTextArea;
    }

    /**
     * Sets whether this renderer should always use a {@link JTextArea} to render table cells.
     * 
     * @param alwaysUseTextArea Whether this renderer should always use a {@link JTextArea} to render table cells.
     */
    public void setAlwaysUseTextArea(boolean alwaysUseTextArea) {
        this.alwaysUseTextArea = alwaysUseTextArea;
    }

}
