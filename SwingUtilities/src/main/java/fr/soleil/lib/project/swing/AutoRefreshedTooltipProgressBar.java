/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

import java.awt.Container;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JProgressBar;
import javax.swing.JToolTip;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.plaf.ProgressBarUI;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.ui.TubeProgressBarUI;

/**
 * A {@link JProgressBar} which is able to automatically refresh its tooltip content.
 * Such solution seems mandatory because default java implementation is not stable enough when one wants to
 * regularly update a tooltip text.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AutoRefreshedTooltipProgressBar extends JProgressBar {

    private static final long serialVersionUID = -5962863608271645215L;

    protected final JToolTip tooltip;
    protected Window tooltipWindow;
    protected final Timer timer;
    private volatile long entered;
    private final Runnable packRunnable;
    private final MouseAdapter adapter;

    public AutoRefreshedTooltipProgressBar(int min, int max) {
        super(min, max);
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                long entered = AutoRefreshedTooltipProgressBar.this.entered;
                if ((entered != 0) && (System.currentTimeMillis() > entered + 750) && (tooltipWindow != null)) {
                    tooltipWindow.setVisible(true);
                }
            }
        }, 0, 200);
        tooltip = createToolTip();
        packRunnable = new Runnable() {
            @Override
            public void run() {
                tooltipWindow.setSize(tooltip.getPreferredSize());
            }
        };

        adapter = new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent evt) {
                if (tooltipWindow == null) {
                    tooltipWindow = createTooltipWindow();
                }
                entered = System.currentTimeMillis();
                updateTooltipLocationAndVisibility();
            }

            @Override
            public void mouseExited(MouseEvent evt) {
                updateTooltipLocationAndVisibility();
            }

            @Override
            public void mouseMoved(MouseEvent evt) {
                if ((entered == 0) && (getToolTipText() != null)) {
                    entered = System.currentTimeMillis();
                }
                updateTooltipLocationAndVisibility();
            }
        };
        addMouseListener(adapter);
        addMouseMotionListener(adapter);
        addAncestorListener(new AncestorListener() {
            private volatile Container lastParent;

            @Override
            public void ancestorRemoved(AncestorEvent event) {
                if (getParent() == null) {
                    lastParent = null;
                    entered = 0;
                    if (tooltipWindow != null) {
                        tooltipWindow.setVisible(false);
                        lastParent = null;
                    }
                }
            }

            @Override
            public void ancestorMoved(AncestorEvent event) {
                // not managed
            }

            @Override
            public void ancestorAdded(AncestorEvent event) {
                Container parent = getParent();
                if (parent != lastParent) {
                    lastParent = parent;
                    entered = System.currentTimeMillis();
                }
            }
        });
    }

    protected JWindow createTooltipWindow() {
        JWindow tooltipWindow = new JWindow(
                WindowSwingUtils.getWindowForComponent(AutoRefreshedTooltipProgressBar.this));
        tooltipWindow.setFocusableWindowState(false);
        tooltipWindow.setType(Window.Type.POPUP);
        tooltipWindow.setContentPane(tooltip);
        tooltipWindow.setAlwaysOnTop(true);
        tooltipWindow.addMouseListener(adapter);
        tooltipWindow.addMouseMotionListener(adapter);
        return tooltipWindow;
    }

    protected void updateTooltipLocationAndVisibility() {
        if (tooltipWindow != null) {
            boolean contains = false;
            try {
                Point locationOnScreen = MouseInfo.getPointerInfo().getLocation();
                Point locationOnComponent = new Point(locationOnScreen);
                SwingUtilities.convertPointFromScreen(locationOnComponent, this);
                contains = contains(locationOnComponent);
                if (contains) {
                    tooltipWindow.setLocation(locationOnScreen.x, locationOnScreen.y + 20);
                }
            } catch (Exception e) {
                contains = false;
            }
            if (!contains) {
                entered = 0;
                if (tooltipWindow != null) {
                    tooltipWindow.setVisible(false);
                }
            }
        }
    }

    @Override
    public void updateUI() {
        ProgressBarUI ui = getUI();
        if (ui == null) {
            super.updateUI();
        } else {
            ProgressBarUI newUI = null;
            try {
                newUI = ui.getClass().newInstance();
            } catch (Exception e) {
                newUI = null;
            }
            if (newUI == null) {
                super.updateUI();
            } else {
                if (ui instanceof TubeProgressBarUI) {
                    TubeProgressBarUI oldTubeUI = (TubeProgressBarUI) ui;
                    TubeProgressBarUI newTubeUI = (TubeProgressBarUI) newUI;
                    newTubeUI.setCornerArcDiameter(oldTubeUI.getCornerArcDiameter());
                    newTubeUI.setProgressColor(oldTubeUI.getProgressColor());
                    newTubeUI.setUseTubeEffect(oldTubeUI.isUseTubeEffect());
                }
                setUI(newUI);
            }
        }
    }

    @Override
    public String getToolTipText() {
        return tooltip == null ? null : tooltip.getTipText();
    }

    @Override
    public void setToolTipText(String text) {
        try {
            if (text == null) {
                if (tooltipWindow != null) {
                    tooltipWindow.setVisible(false);
                }
                tooltip.setTipText(ObjectUtils.EMPTY_STRING);
            } else {
                tooltip.setTipText(text);
            }
            if (tooltipWindow != null) {
                if (SwingUtilities.isEventDispatchThread()) {
                    packRunnable.run();
                } else {
                    SwingUtilities.invokeLater(packRunnable);
                }
            }
        } catch (Exception e) {
            // Ignore
        }
    }

}