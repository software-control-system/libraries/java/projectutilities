/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.text;

import javax.swing.text.AbstractDocument;
import javax.swing.text.BoxView;
import javax.swing.text.ComponentView;
import javax.swing.text.Element;
import javax.swing.text.IconView;
import javax.swing.text.LabelView;
import javax.swing.text.ParagraphView;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;

/**
 * A {@link StyledEditorKit} used to center text vertically in styled text components, like
 * JTextPanes.
 * <p>
 * Code found on <a
 * href="http://www.siteduzero.com/forum-83-489803-p1-jlabel-retour-a-la-ligne.html"
 * >siteduzero.com</a>
 * </p>
 * 
 * @author StanislavL
 */
public class CenteredEditorKit extends StyledEditorKit {

    private static final long serialVersionUID = -3463326903542557142L;

    @Override
    public ViewFactory getViewFactory() {
        return new StyledViewFactory();
    }

    /**
     * A {@link ViewFactory} that will generate the right {@link View} to center text
     */
    protected static class StyledViewFactory implements ViewFactory {
        @Override
        public View create(Element elem) {
            View result = null;
            String kind = elem.getName();
            if (kind != null) {
                if (kind.equals(AbstractDocument.ContentElementName)) {
                    result = new LabelView(elem);
                } else if (kind.equals(AbstractDocument.ParagraphElementName)) {
                    result = new ParagraphView(elem);
                } else if (kind.equals(AbstractDocument.SectionElementName)) {
                    result = new CenteredBoxView(elem, View.Y_AXIS);
                } else if (kind.equals(StyleConstants.ComponentElementName)) {
                    result = new ComponentView(elem);
                } else if (kind.equals(StyleConstants.IconElementName)) {
                    result = new IconView(elem);
                }
            }
            if (result == null) {
                result = new LabelView(elem);
            }
            // default to text display
            return result;
        }
    } // end class StyledViewFactory

    /**
     * A {@link BoxView} that centers text
     */
    protected static class CenteredBoxView extends BoxView {
        public CenteredBoxView(Element elem, int axis) {
            super(elem, axis);
        }

        @Override
        protected void layoutMajorAxis(int targetSpan, int axis, int[] offsets, int[] spans) {
            super.layoutMajorAxis(targetSpan, axis, offsets, spans);
            int textBlockHeight = 0;
            int offset = 0;
            for (int i = 0; i < spans.length; i++) {
                textBlockHeight += spans[i];
            }
            offset = (targetSpan - textBlockHeight) / 2;
            // offset = (targetSpan - textBlockHeight);
            for (int i = 0; i < offsets.length; i++) {
                offsets[i] += offset;
            }
        }
    } // end class CenteredBoxView

} // end class CenteredEditorKit
