/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.renderer;

import java.awt.Color;
import java.awt.Font;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.soleil.lib.project.swing.ScrollMagnetism;
import fr.soleil.lib.project.swing.SwingUtilitiesConstants;

/**
 * A {@link DefaultListCellRenderer} used to represent {@link ScrollMagnetism} values.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ScrollMagnetismListCellRenderer extends LightSelectionBackgroundListCellRenderer
        implements SwingUtilitiesConstants {

    private static final long serialVersionUID = -1770395899063832465L;

    // constants to manage text and tooltip
    private static final String NO_TEXT = "fr.soleil.lib.project.swing.magnet.scroll.no.text";
    private static final String NO_TOOLTIP = "fr.soleil.lib.project.swing.magnet.scroll.no.tooltip";
    private static final String TOP_TEXT = "fr.soleil.lib.project.swing.magnet.scroll.top.text";
    private static final String TOP_TOOLTIP = "fr.soleil.lib.project.swing.magnet.scroll.top.tooltip";
    private static final String BOTTOM_TEXT = "fr.soleil.lib.project.swing.magnet.scroll.bottom.text";
    private static final String BOTTOM_TOOLTIP = "fr.soleil.lib.project.swing.magnet.scroll.bottom.tooltip";
    private static final String LEFT_TEXT = "fr.soleil.lib.project.swing.magnet.scroll.left.text";
    private static final String LEFT_TOOLTIP = "fr.soleil.lib.project.swing.magnet.scroll.left.tooltip";
    private static final String RIGHT_TEXT = "fr.soleil.lib.project.swing.magnet.scroll.right.text";
    private static final String RIGHT_TOOLTIP = "fr.soleil.lib.project.swing.magnet.scroll.right.tooltip";
    private static final String TOP_LEFT_TEXT = "fr.soleil.lib.project.swing.magnet.scroll.top-left.text";
    private static final String TOP_LEFT_TOOLTIP = "fr.soleil.lib.project.swing.magnet.scroll.top-left.tooltip";
    private static final String TOP_RIGHT_TEXT = "fr.soleil.lib.project.swing.magnet.scroll.top-right.text";
    private static final String TOP_RIGHT_TOOLTIP = "fr.soleil.lib.project.swing.magnet.scroll.top-right.tooltip";
    private static final String BOTTOM_LEFT_TEXT = "fr.soleil.lib.project.swing.magnet.scroll.bottom-left.text";
    private static final String BOTTOM_LEFT_TOOLTIP = "fr.soleil.lib.project.swing.magnet.scroll.bottom-left.tooltip";
    private static final String BOTTOM_RIGHT_TEXT = "fr.soleil.lib.project.swing.magnet.scroll.bottom-right.text";
    private static final String BOTTOM_RIGHT_TOOLTIP = "fr.soleil.lib.project.swing.magnet.scroll.bottom-right.tooltip";

    public ScrollMagnetismListCellRenderer(JComponent componentToRender) {
        super(componentToRender);
    }

    @Override
    protected void doApplyToComponent(JComponent comp, Font font, Color foreground, String tooltipText,
            JList<? extends Object> list, Object value, int index, boolean isSelected, boolean cellHasFocus,
            boolean derive) {
        super.doApplyToComponent(comp, font, foreground, tooltipText, list, value, index, isSelected, cellHasFocus,
                derive);
        if (comp instanceof JLabel) {
            JLabel label = (JLabel) comp;
            if (value instanceof ScrollMagnetism) {
                ScrollMagnetism magnetism = (ScrollMagnetism) value;
                String textKey, tooltipKey;
                switch (magnetism) {
                    case NONE:
                        textKey = NO_TEXT;
                        tooltipKey = NO_TOOLTIP;
                        break;
                    case TOP:
                        textKey = TOP_TEXT;
                        tooltipKey = TOP_TOOLTIP;
                        break;
                    case BOTTOM:
                        textKey = BOTTOM_TEXT;
                        tooltipKey = BOTTOM_TOOLTIP;
                        break;
                    case LEFT:
                        textKey = LEFT_TEXT;
                        tooltipKey = LEFT_TOOLTIP;
                        break;
                    case RIGHT:
                        textKey = RIGHT_TEXT;
                        tooltipKey = RIGHT_TOOLTIP;
                        break;
                    case TOP_LEFT:
                        textKey = TOP_LEFT_TEXT;
                        tooltipKey = TOP_LEFT_TOOLTIP;
                        break;
                    case TOP_RIGHT:
                        textKey = TOP_RIGHT_TEXT;
                        tooltipKey = TOP_RIGHT_TOOLTIP;
                        break;
                    case BOTTOM_LEFT:
                        textKey = BOTTOM_LEFT_TEXT;
                        tooltipKey = BOTTOM_LEFT_TOOLTIP;
                        break;
                    case BOTTOM_RIGHT:
                        textKey = BOTTOM_RIGHT_TEXT;
                        tooltipKey = BOTTOM_RIGHT_TOOLTIP;
                        break;
                    default:
                        textKey = null;
                        tooltipKey = null;
                        break;

                }
                if (textKey != null) {
                    label.setText(DEFAULT_MESSAGE_MANAGER.getMessage(textKey));
                }
                if (tooltipKey != null) {
                    label.setToolTipText(DEFAULT_MESSAGE_MANAGER.getMessage(tooltipKey));
                }
            }
        }
    }

}
