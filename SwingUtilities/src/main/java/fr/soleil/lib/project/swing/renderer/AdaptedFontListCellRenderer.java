/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.renderer;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;

/**
 * A {@link ListCellRenderer} that is able to adapt its rendering {@link Font} when the {@link Component} to render is
 * not opaque.
 * It will Typically be used with <code>JComboBox</code>. When the <code>JComboBox</code> is not opaque, the renderer
 * will try to change the {@link Font} to show which value is selected.
 * 
 * @param <E> The type of values this renderer can be used for
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface AdaptedFontListCellRenderer<E> extends ListCellRenderer<E> {

    /**
     * Returns whether the {@link Font} in {@link #getListCellRendererComponent(JList, Object, int, boolean, boolean)}
     * will be adapted if the {@link Component} to render is not opaque
     * 
     * @return A <code>boolean</code>
     */
    public boolean isAutoAdaptFont();

    /**
     * Sets whether the {@link Font} in {@link #getListCellRendererComponent(JList, Object, int, boolean, boolean)}
     * should be adapted if the {@link Component} to render is not opaque
     * 
     * @param autoAdaptFont Whether the {@link Font} in
     *            {@link #getListCellRendererComponent(JList, Object, int, boolean, boolean)} should be adapted if the
     *            {@link Component} to render is not opaque
     */
    public void setAutoAdaptFont(boolean autoAdaptFont);

    /**
     * Return a {@link JComponent} that has been configured to display the specified value. That component's
     * <code>paint</code> method is then called to "render" the cell. If it is necessary to compute the dimensions
     * of a list because the list cells do not have a fixed size, this method is called to generate a component on which
     * <code>getPreferredSize</code> can be invoked.
     * The resulting {@link JComponent} might have an adapted {@link Font}, especially when the rendered Component is
     * not opaque.
     *
     * @param list The JList we're painting.
     * @param value The value returned by list.getModel().getElementAt(index).
     * @param index The cells index.
     * @param isSelected True if the specified cell was selected.
     * @param cellHasFocus True if the specified cell has the focus.
     * @param derive Whether to derive Font to adapt it, if {@link #isAutoAdaptFont(boolean)} is <code>true</code>.
     * @return A {@link JComponent} whose paint() method will render the specified value.
     *
     * @see JList
     * @see ListSelectionModel
     * @see ListModel
     */
    public JComponent getListCellRendererComponent(JList<? extends E> list, E value, int index, boolean isSelected,
            boolean cellHasFocus, boolean derive);

}
