/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.panel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link JPanel} that displays an image as background
 * 
 * @author Rapha&euml;l GIRARDOT
 * 
 */
public class ImagePanel extends JPanel {

    private static final long serialVersionUID = 3448752699907052142L;

    protected BufferedImage backgroundImage;
    protected DrawType drawType;
    protected double imageAlpha;

    public ImagePanel() {
        super();
        init();
    }

    public ImagePanel(LayoutManager layout) {
        super(layout);
        init();
    }

    public ImagePanel(boolean isDoubleBuffered) {
        super(isDoubleBuffered);
        init();
    }

    public ImagePanel(LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
        init();
    }

    protected void init() {
        backgroundImage = null;
        drawType = DrawType.SCALED;
        imageAlpha = 0.5;
    }

    public BufferedImage getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(BufferedImage backgroundImage) {
        if (!ObjectUtils.sameObject(backgroundImage, this.backgroundImage)) {
            this.backgroundImage = backgroundImage;
            repaint();
        }
    }

    public void setBackgroundImage(ImageIcon icon) {
        BufferedImage bufferedImage;
        if (icon == null) {
            bufferedImage = null;
        } else {
            Image image = icon.getImage();
            bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
            bufferedImage.createGraphics().drawImage(image, 0, 0, null);
        }
        setBackgroundImage(bufferedImage);
    }

    public DrawType getDrawType() {
        return drawType;
    }

    public void setDrawType(DrawType drawType) {
        if (!ObjectUtils.sameObject(drawType, this.drawType)) {
            this.drawType = drawType;
            repaint();
        }
    }

    public double getImageAlpha() {
        return imageAlpha;
    }

    public void setImageAlpha(double imageAlpha) {
        this.imageAlpha = imageAlpha;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        BufferedImage image = adaptImage(backgroundImage);
        DrawType type = drawType;
        if ((g instanceof Graphics2D) && (image != null) && (type != null)) {
            Graphics2D g2d = (Graphics2D) g;
            Paint paint = g2d.getPaint();
            Paint previous = paint;
            int imgWidth = image.getWidth();
            int imgHeight = image.getHeight();
            int width = getWidth();
            int height = getHeight();
            paintBorder(g2d);
            if (isOpaque()) {
                g2d.setColor(getBackground());
                g2d.fillRect(0, 0, width, height);
            }
            int x = 0;
            int y = 0;
            switch (type) {
                case MOSAIC:
                    paint = new TexturePaint(image, new Rectangle(imgWidth, imgHeight));
                    break;
                case CENTERED:
                    x = (width - imgWidth) / 2;
                    y = (height - imgHeight) / 2;
                    paint = new TexturePaint(image, new Rectangle(x, y, imgWidth, imgHeight));
                    width = imgWidth;
                    height = imgHeight;
                    break;
                case SCALED:
                    paint = new TexturePaint(image, new Rectangle(width, height));
                    break;
            }
            g2d.setPaint(paint);
            g2d.fillRect(x, y, width, height);
            g2d.setPaint(previous);
            paintChildren(g2d);
        } else {
            super.paintComponent(g);
        }
    }

    protected BufferedImage adaptImage(BufferedImage image) {
        BufferedImage result;
        if (image == null) {
            result = null;
        } else if (isOpaque()) {
            result = getAdaptedImage(image, imageAlpha);
        } else {
            double alpha = imageAlpha;
            if ((alpha <= 0) || (alpha > 1)) {
                alpha = 1;
            }
            alpha /= 2;
            result = getAdaptedImage(image, alpha);
        }
        return result;
    }

    public static BufferedImage getAdaptedImage(BufferedImage image, double alpha) {
        BufferedImage result;
        if (image == null) {
            result = null;
        } else if ((alpha <= 0) || (alpha >= 1)) {
            result = image;
        } else {
            result = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
            for (int y = 0; y < image.getHeight(); y++) {
                for (int x = 0; x < image.getWidth(); x++) {
                    int argb = image.getRGB(x, y);
                    int a = getAdaptedValue((argb >> 24) & 0xFF, alpha);
                    int r = (argb >> 16) & 0xFF;
                    int g = (argb >> 8) & 0xFF;
                    int b = (argb >> 0) & 0xFF;
                    // increase transparency
                    argb = ((a & 0xFF) << 24) | ((r & 0xFF) << 16) | ((g & 0xFF) << 8) | ((b & 0xFF) << 0);
                    result.setRGB(x, y, argb);
                }
            }
        }
        return result;
    }

    protected static int getAdaptedValue(int colorComponent, double mult) {
        int result = (int) Math.round(colorComponent * mult);
        if (result > 255) {
            result = 255;
        }
        if (result < 0) {
            result = 0;
        }
        return result;
    }

    public static void main(String[] args) {
        JFrame testFrame = new JFrame("test");
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        ImagePanel panel = new ImagePanel();
        String url = "/fr/soleil/lib/project/swing/icons/page_white_save.png";
        DrawType type = DrawType.MOSAIC;
        Color bg = null;
        if ((args != null) && (args.length > 0)) {
            url = args[0];
            if (args.length > 1) {
                for (DrawType tmp : DrawType.values()) {
                    if (tmp.toString().equalsIgnoreCase(args[1])) {
                        type = tmp;
                        break;
                    }
                }
                if (args.length > 2) {
                    if ("WHITE".equalsIgnoreCase(args[2])) {
                        bg = Color.WHITE;
                    } else if ("LIGHT_GRAY".equalsIgnoreCase(args[2])) {
                        bg = Color.LIGHT_GRAY;
                    } else if ("GRAY".equalsIgnoreCase(args[2])) {
                        bg = Color.GRAY;
                    } else if ("DARK_GRAY".equalsIgnoreCase(args[2])) {
                        bg = Color.DARK_GRAY;
                    } else if ("BLACK".equalsIgnoreCase(args[2])) {
                        bg = Color.BLACK;
                    } else if ("RED".equalsIgnoreCase(args[2])) {
                        bg = Color.RED;
                    } else if ("PINK".equalsIgnoreCase(args[2])) {
                        bg = Color.PINK;
                    } else if ("ORANGE".equalsIgnoreCase(args[2])) {
                        bg = Color.ORANGE;
                    } else if ("YELLOW".equalsIgnoreCase(args[2])) {
                        bg = Color.YELLOW;
                    } else if ("GREEN".equalsIgnoreCase(args[2])) {
                        bg = Color.GREEN;
                    } else if ("MAGENTA".equalsIgnoreCase(args[2])) {
                        bg = Color.MAGENTA;
                    } else if ("CYAN".equalsIgnoreCase(args[2])) {
                        bg = Color.CYAN;
                    } else if ("BLUE".equalsIgnoreCase(args[2])) {
                        bg = Color.BLUE;
                    }
                }
            }
        }
        URL location;
        try {
            File tmp = new File(url);
            if (tmp.isFile()) {
                location = tmp.toURI().toURL();
            } else {
                location = ImagePanel.class.getResource(url);
            }
        } catch (Exception e) {
            location = ImagePanel.class.getResource(url);
        }
        panel.setBackground(bg);
        panel.setBackgroundImage(new ImageIcon(location));
        panel.setDrawType(type);
        testFrame.setContentPane(panel);
        testFrame.setSize(750, 550);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    public static enum DrawType {
        MOSAIC, CENTERED, SCALED
    }

}
