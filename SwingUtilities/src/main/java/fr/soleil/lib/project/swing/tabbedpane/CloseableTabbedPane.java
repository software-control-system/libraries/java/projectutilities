/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.tabbedpane;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.plaf.metal.MetalIconFactory;

import fr.soleil.lib.project.swing.listener.ButtonDecorationListener;

/**
 * A {@link JTabbedPane} that offers the possibility to close a tab through a X button.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class CloseableTabbedPane extends JTabbedPane {

    private static final long serialVersionUID = -3960262804684778468L;

    protected final Object lock;
    protected final ButtonDecorationListener buttonDecorationListener;

    public CloseableTabbedPane() {
        super();
        lock = new Object();
        buttonDecorationListener = generateButtonDecorationListener();
    }

    public CloseableTabbedPane(int tabPlacement) {
        super(tabPlacement);
        lock = new Object();
        buttonDecorationListener = generateButtonDecorationListener();
    }

    public CloseableTabbedPane(int tabPlacement, int tabLayoutPolicy) {
        super(tabPlacement, tabLayoutPolicy);
        lock = new Object();
        buttonDecorationListener = generateButtonDecorationListener();
    }

    /**
     * Creates a new {@link ButtonDecorationListener} that will be used with the close buttons.
     * 
     * @return A {@link ButtonDecorationListener}
     */
    protected ButtonDecorationListener generateButtonDecorationListener() {
        return new ButtonDecorationListener();
    }

    @Override
    public void addTab(String title, Component component) {
        addTab(title, null, component);
    }

    @Override
    public void addTab(String title, Icon icon, Component component, String tip) {
        synchronized (lock) {
            int count = this.getTabCount();
            super.addTab(title, icon, component, tip);
            CloseableTab tabComponent = buildTabForComponent(component, title, icon);
            if (tabComponent != null) {
                setTabComponentAt(count, tabComponent);
            }
        }
    }

    /**
     * Builds a {@link CloseableTab} for a given {@link Component}
     * 
     * @param component The {@link Component}
     * @param title The tab title
     * @param icon The tab icon
     * @return A {@link CloseableTab}
     */
    protected CloseableTab buildTabForComponent(Component component, String title, Icon icon) {
        return new CloseableTab(title, icon);
    }

    @Override
    public void addTab(String title, Icon icon, Component component) {
        addTab(title, icon, component, null);
    }

    @Override
    public void removeTabAt(int index) {
        synchronized (lock) {
            super.removeTabAt(index);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link JPanel} in charge of rendering a tab for a {@link Component}. It contains the close button.
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class CloseableTab extends JPanel implements ActionListener {

        private static final long serialVersionUID = 6328400761002812774L;

        protected String title;
        protected final JLabel label;
        protected final JButton closeButton;

        public CloseableTab(final String title, Icon icon) {
            super();
            this.title = title;
            setBorder(null);

            setOpaque(false);
            FlowLayout flowLayout = new FlowLayout(FlowLayout.LEADING, 5, 5);
            setLayout(flowLayout);
            setVisible(true);

            label = new JLabel(this.title);
            label.setIcon(icon);
            add(label);

            closeButton = new JButton(MetalIconFactory.getInternalFrameCloseIcon(16));
            closeButton.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
            closeButton.addMouseListener(buttonDecorationListener);
            closeButton.addActionListener(this);
            add(closeButton);
        }

        /**
         * Removes the tab associated with this {@link CloseableTab}
         */
        protected void removeTab() {
            CloseableTabbedPane tabbedPane = (CloseableTabbedPane) getParent().getParent();
            int index = tabbedPane.indexOfTabComponent(this);
            if (index > -1) {
                tabbedPane.remove(index);
            }
            closeButton.removeMouseListener(buttonDecorationListener);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if ((e != null) && (e.getSource() == closeButton)) {
                removeTab();
            }
        }

    }

}
