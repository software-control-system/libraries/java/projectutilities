/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.file;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.swing.SwingUtilitiesConstants;

/**
 * A {@link FileFilter} based on file extensions. This {@link FileFilter} accepts directories and
 * files that have a given extension
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ExtensionFileFilter extends FileFilter implements SwingUtilitiesConstants {

    private String extension;
    private String description;

    /**
     * Constructs a new {@link ExtensionFileFilter}
     * 
     * @param fileExtension The compatible file extension
     */
    public ExtensionFileFilter(String fileExtension) {
        this(fileExtension, null);
    }

    /**
     * Constructs a new {@link ExtensionFileFilter}, describing its corresponding files
     * 
     * @param fileExtension The compatible file extension
     * @param fileDescription The file description
     */
    public ExtensionFileFilter(String fileExtension, String fileDescription) {
        if (fileExtension == null) {
            extension = ObjectUtils.EMPTY_STRING;
        } else {
            extension = fileExtension;
        }
        if (fileDescription == null) {
            description = String.format(
                    DEFAULT_MESSAGE_MANAGER.getMessage("fr.soleil.lib.project.swing.file.chooser.files"), extension);
        } else {
            description = fileDescription;
        }
    }

    @Override
    public boolean accept(File f) {
        return ((f != null) && (f.isDirectory() || extension.equalsIgnoreCase(FileUtils.getExtension(f))));
    }

    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Returns the compatible file extension
     * 
     * @return A {@link String}
     */
    public String getExtension() {
        return extension;
    }

    @Override
    public boolean equals(final Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (getClass().equals(obj.getClass())) {
            ExtensionFileFilter selector = (ExtensionFileFilter) obj;
            equals = ObjectUtils.sameObject(getExtension(), selector.getExtension())
                    && ObjectUtils.sameObject(getDescription(), selector.getDescription());
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xF11E;
        int mult = 0xF117E13;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getExtension());
        code = code * mult + ObjectUtils.getHashCode(getDescription());
        return code;
    }

}
