/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.listener;

import javax.swing.JTree;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;

/**
 * A {@link TreeModelListener} that updates ui for a given tree in case of important changes in its
 * structure. This is useful to avoid some undesired UI bugs, like strange selection loss
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class TreeUiUpdater implements TreeModelListener {

    protected JTree tree;
    protected boolean keepFirstRowExpanded;
    protected boolean expandNewPaths;

    /**
     * Constructor
     * 
     * @param tree The tree for which to update ui when necessary
     * @param keepFirstRowExpanded Whether to keep first row (i.e. root node) expanded in tree
     * @param expandNewPaths Whether to expand newly added thread
     */
    public TreeUiUpdater(JTree tree, boolean keepFirstRowExpanded, boolean expandNewPaths) {
        this.tree = tree;
        this.keepFirstRowExpanded = keepFirstRowExpanded;
        this.expandNewPaths = expandNewPaths;
    }

    @Override
    public void treeNodesChanged(TreeModelEvent e) {
        if (tree != null) {
            tree.repaint();
        }
    }

    @Override
    public void treeNodesInserted(TreeModelEvent e) {
        if (expandNewPaths && (tree != null) && (e != null) && (e.getTreePath() != null)) {
            tree.expandPath(e.getTreePath());
        }
        updateUIWorkaround();
    }

    @Override
    public void treeNodesRemoved(TreeModelEvent e) {
        updateUIWorkaround();
    }

    @Override
    public void treeStructureChanged(TreeModelEvent e) {
        if ((tree != null) && (tree.getRowCount() > 0) && keepFirstRowExpanded) {
            tree.expandRow(0);
        }
        updateUIWorkaround();
    }

    /**
     * Hack to avoid some strange U.I. bugs (including some selection loss)
     */
    protected void updateUIWorkaround() {
        if (tree != null) {
            tree.updateUI();
            tree.repaint();
        }
    }

}
