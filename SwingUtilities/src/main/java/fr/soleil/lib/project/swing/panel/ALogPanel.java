/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.swing.LoggingDelegate;
import fr.soleil.lib.project.swing.SwingUtilitiesConstants;
import fr.soleil.lib.project.swing.file.ExtensionFileFilter;
import fr.soleil.lib.project.xmlhelpers.HtmlEscape;

/**
 * A {@link JPanel} that handles logging in a text area, using a handler
 *
 * @param <L> The type of logger known by this {@link ALogPanel}
 * @param <LVL> The type of logging level known by this {@link ALogPanel}
 * @param <H> The type oh handler known by this {@link ALogPanel}
 *
 * @author Rapha&euml;l GIRARDOT
 * @deprecated Use {@link ALogViewer} instead.
 */
@Deprecated
public abstract class ALogPanel<L, LVL, H> extends JPanel implements ActionListener, SwingUtilitiesConstants {

    private static final long serialVersionUID = -1627738777214459770L;

    // default maximum text lines
    public static final int DEFAULT_MAX_LINES = 300;
    // message separator
    public static final String TEXT_SEPARATOR = "\n*****************************************\n";
    public static final String HTML_SEPARATOR = "<div class=\"separator\">_________________________________________</div><br />";
    private static final String CLASS_SEPARATOR = "div.separator {color:#000000;font-size:12pt;font-style:normal;}";
    protected static final String LEVEL_SEPARATOR = ": ";

    // buttons margin
    private static final Insets NO_MARGIN = new Insets(0, 0, 0, 0);
    // insets in button panel
    private static final Insets DEFAULT_MARGIN = new Insets(0, 5, 0, 0);
    // save log icon
    private static final ImageIcon SAVE_ICON = DEFAULT_ICONS.getIcon("fr.soleil.lib.project.swing.log.save");
    // save log as icon
    private static final ImageIcon SAVE_AS_ICON = DEFAULT_ICONS.getIcon("fr.soleil.lib.project.swing.log.save.as");
    // clear icon
    private static final ImageIcon CLEAR_ICON = DEFAULT_ICONS.getIcon("fr.soleil.lib.project.swing.log.clear");
    // os way to end line in files
    private static final String CRLF = System.getProperty("line.separator");

    // String conversion
    protected static final String NEW_LINE = ObjectUtils.NEW_LINE;
    protected static final String HTML_NEW_LINE = "<br />";

    // text lock is used to organize thread access
    protected final StringBuilder messageBuilder;
    // formatter used to format messages with current date
    protected final DateFormat dateFormat;
    // maximum text lines
    private int maxLines;
    // whether to insert log at text area beginning
    private boolean logFirst;
    // whether to display date in logs
    private boolean publishDate;
    // Handler used to transmit logs to text area
    private final H handler;
    // JFileChooser used to save logs
    private final JFileChooser logSaver;
    // title to display when asking whether to overwrite
    private String overwriteTitle;
    // message to display when asking whether to overwrite
    private String overwriteMessage;
    // message to display in case of file writing error
    private String saveErrorMessage;
    // message to display in case of file writer closing error
    private String writerClosingErrorMessage;
    // logger used to report errors when trying to save logs
    protected final L logger;
    // listeners to warn once clear log button clicked
    private final Set<ActionListener> clearLogListeners;

    protected final JPopupMenu popupMenu;
    protected final JMenuItem clearMenuItem;
    protected final JScrollPane textScrollPane;
    protected final JLabel htmlTextArea;
    protected final JTextArea defaultTextArea;
    protected final JPanel buttonPanel;
    protected final JButton saveButton;
    protected final JButton saveAsButton;
    protected final JButton clearButton;

    private String separator;
    private final boolean html;
    protected final LoggingDelegate loggingDelegate;

    public ALogPanel() {
        this(false);
    }

    public ALogPanel(boolean html) {
        super(new BorderLayout(5, 5));
        this.html = html;
        separator = html ? HTML_SEPARATOR : TEXT_SEPARATOR;
        messageBuilder = new StringBuilder();
        dateFormat = new SimpleDateFormat("yyyy/MM/dd  HH:mm:ss - ");
        maxLines = DEFAULT_MAX_LINES;
        logFirst = false;
        publishDate = true;
        handler = generateHandler(new Publisher());
        logSaver = new JFileChooser(".");
        overwriteTitle = "Confirm overwrite";
        overwriteMessage = "File %s exits. Overwrite?";
        saveErrorMessage = "Failed to save file %s";
        writerClosingErrorMessage = "Failed to close writer for file %s";
        logger = generateLogger(getClass().getName() + "-" + System.currentTimeMillis() + "-" + (Math.random() * 500));
        listenToLogger(logger);
        clearLogListeners = new HashSet<ActionListener>();

        clearMenuItem = new JMenuItem();
        clearMenuItem.setText("Clear history");
        clearMenuItem.setToolTipText("Clear history");
        clearMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                clear();
            }
        });

        popupMenu = new JPopupMenu();
        popupMenu.setDoubleBuffered(true);
        popupMenu.add(clearMenuItem);

        final JComponent textArea;
        if (html) {
            htmlTextArea = new HtmlTextArea();
            textArea = htmlTextArea;
            ExtensionFileFilter filter = new ExtensionFileFilter("html", "html files");
            logSaver.addChoosableFileFilter(filter);
            logSaver.setFileFilter(filter);
            defaultTextArea = null;
        } else {
            defaultTextArea = new DefaultTextArea();
            textArea = defaultTextArea;
            ExtensionFileFilter filter = new ExtensionFileFilter("txt", "text files");
            logSaver.addChoosableFileFilter(filter);
            logSaver.setFileFilter(filter);
            htmlTextArea = null;
        }
        prepareTextArea(textArea);
        textScrollPane = new JScrollPane(textArea);
        loggingDelegate = createLoggingDelegate(textArea);
        textScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        textScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        logSaver.setFileSelectionMode(JFileChooser.FILES_ONLY);

        buttonPanel = new JPanel(new GridBagLayout());
        clearButton = new JButton("Clear", CLEAR_ICON);
        clearButton.setMargin(NO_MARGIN);
        clearButton.addActionListener(this);
        saveButton = new JButton("Save", SAVE_ICON);
        saveButton.setMargin(NO_MARGIN);
        saveButton.addActionListener(this);
        saveAsButton = new JButton("Save as", SAVE_AS_ICON);
        saveAsButton.setMargin(NO_MARGIN);
        saveAsButton.addActionListener(this);
        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.BOTH;
        glueConstraints.gridx = 0;
        glueConstraints.gridy = 0;
        glueConstraints.weightx = 1;
        glueConstraints.weighty = 1;
        buttonPanel.add(Box.createGlue(), glueConstraints);
        GridBagConstraints clearLogConstraints = new GridBagConstraints();
        clearLogConstraints.fill = GridBagConstraints.VERTICAL;
        clearLogConstraints.gridx = 1;
        clearLogConstraints.gridy = 0;
        clearLogConstraints.weightx = 0;
        clearLogConstraints.weighty = 1;
        clearLogConstraints.insets = DEFAULT_MARGIN;
        buttonPanel.add(clearButton, clearLogConstraints);
        GridBagConstraints saveLogConstraints = new GridBagConstraints();
        saveLogConstraints.fill = GridBagConstraints.VERTICAL;
        saveLogConstraints.gridx = 2;
        saveLogConstraints.gridy = 0;
        saveLogConstraints.weightx = 0;
        saveLogConstraints.weighty = 1;
        saveLogConstraints.insets = DEFAULT_MARGIN;
        buttonPanel.add(saveButton, saveLogConstraints);
        GridBagConstraints saveLogAsConstraints = new GridBagConstraints();
        saveLogAsConstraints.fill = GridBagConstraints.VERTICAL;
        saveLogAsConstraints.gridx = 3;
        saveLogAsConstraints.gridy = 0;
        saveLogAsConstraints.weightx = 0;
        saveLogAsConstraints.weighty = 1;
        saveLogAsConstraints.insets = DEFAULT_MARGIN;
        buttonPanel.add(saveAsButton, saveLogAsConstraints);

        add(textScrollPane, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);

        setBorder(new EmptyBorder(5, 5, 5, 5));
    }

    protected abstract L generateLogger(String id);

    /**
     * Assures this {@link ALogPanel} traces messages coming from given logger
     * 
     * @param logger The logger
     */
    public final void listenToLogger(L logger) {
        addHandlerToLogger(logger, handler);
    }

    protected abstract void addHandlerToLogger(L logger, H handler);

    protected abstract H generateHandler(Publisher publisher);

    protected abstract void logError(String message, Exception error);

    protected abstract LVL[] getLevels();

    protected abstract String getDivCSSClass(LVL lvl);

    protected abstract String getDiv(LVL lvl);

    protected LoggingDelegate createLoggingDelegate(JComponent textArea) {
        return new LoggingDelegate(textArea, false);
    }

    protected void prepareTextArea(JComponent textArea) {
        textArea.setOpaque(true);
        textArea.setBackground(Color.WHITE);
        textArea.setForeground(Color.BLACK);
        if (textArea instanceof JTextComponent) {
            ((JTextComponent) textArea).setEditable(false);
        }
        textArea.setComponentPopupMenu(popupMenu);
        textArea.setFont(getFont());
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);
        if (defaultTextArea != null) {
            defaultTextArea.setFont(font);
        }
        if (htmlTextArea != null) {
            htmlTextArea.setFont(font);
        }
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (textScrollPane != null) {
            textScrollPane.getViewport().setBackground(bg);
            textScrollPane.getHorizontalScrollBar().setBackground(bg);
            textScrollPane.getVerticalScrollBar().setBackground(bg);
        }
        if (buttonPanel != null) {
            buttonPanel.setBackground(bg);
        }
        if (saveButton != null) {
            saveButton.setBackground(bg);
        }
        if (saveAsButton != null) {
            saveAsButton.setBackground(bg);
        }
        if (clearButton != null) {
            clearButton.setBackground(bg);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e != null) {
            if (e.getSource() == saveButton) {
                save(true);
            } else if (e.getSource() == saveAsButton) {
                saveAs();
            } else if (e.getSource() == clearButton) {
                clear();
                synchronized (clearLogListeners) {
                    for (ActionListener listener : clearLogListeners) {
                        listener.actionPerformed(e);
                    }
                }
            }
        }
    }

    /**
     * Returns the maximum number of lines displayed in text area
     * 
     * @return An <code>int</code>
     */
    public int getMaxLines() {
        return maxLines;
    }

    /**
     * Sets the maximum number of lines to display in text area
     * 
     * @param maxLines
     *            The maximum number of lines to display in text area
     */
    public void setMaxLines(int maxLines) {
        if (maxLines > 0) {
            this.maxLines = maxLines;
        } else {
            this.maxLines = DEFAULT_MAX_LINES;
        }
    }

    /**
     * Returns whether logs are put at the beginning of the text area
     * 
     * @return A <code>boolean</code> value. <code>false</code> by default
     */
    public boolean isLogFirst() {
        return logFirst;
    }

    /**
     * Sets whether logs should be put at the beginning of the text area
     * 
     * @param logFirst
     *            Whether logs should be put at the beginning of the text area
     */
    public void setLogFirst(boolean logFirst) {
        this.logFirst = logFirst;
        applyLogPosition();
    }

    /**
     * Returns whether current date is inserted before every log
     * 
     * @return A <code>boolean</code> value. <code>true</code> by default
     */
    public boolean isPublishDate() {
        return publishDate;
    }

    /**
     * Sets whether current date should be inserted before every log
     * 
     * @param publishDate
     *            Whether current date should be inserted before every log
     */
    public void setPublishDate(boolean publishDate) {
        this.publishDate = publishDate;
    }

    /**
     * Returns whether this {@link ALogPanel} uses html text formatting
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isHtml() {
        return html;
    }

    /**
     * Returns the handler that can be associated to any logger to redirect logs to text area
     * 
     * @return A handler
     */
    public H getHandler() {
        return handler;
    }

    /**
     * Returns the title displayed in popup when asking whether to overwrite
     * file
     * 
     * @return A {@link String}
     */
    public String getOverwriteTitle() {
        return overwriteTitle;
    }

    /**
     * Sets the title to display in popup when asking whether to overwrite file
     * 
     * @param overwriteTitle
     *            The title to display in popup when asking whether to overwrite
     *            file
     */
    public void setOverwriteTitle(String overwriteTitle) {
        if (overwriteTitle != null) {
            this.overwriteTitle = overwriteTitle;
        }
    }

    /**
     * Returns the message displayed to ask whether to overwrite file
     * 
     * @return A {@link String}
     */
    public String getOverwriteMessage() {
        return overwriteMessage;
    }

    /**
     * Sets the message to display to ask whether to overwrite file
     * 
     * @param overwriteMessage
     *            The message to display to ask whether to overwrite file
     */
    public void setOverwriteMessage(String overwriteMessage) {
        if (overwriteMessage != null) {
            this.overwriteMessage = overwriteMessage;
        }
    }

    /**
     * Returns the message displayed when an error occurred while trying to save
     * logs
     * 
     * @return A {@link String}
     */
    public String getSaveErrorMessage() {
        return saveErrorMessage;
    }

    /**
     * Sets the message to display when an error occurred while trying to save
     * logs
     * 
     * @param saveErrorMessage
     *            The message to display when an error appeared while trying to
     *            save logs
     */
    public void setSaveErrorMessage(String saveErrorMessage) {
        if (saveErrorMessage != null) {
            this.saveErrorMessage = saveErrorMessage;
        }
    }

    /**
     * Returns the message displayed when an error occurred while trying to
     * close the file writer during logs saving
     * 
     * @return A {@link String}
     */
    public String getWriterClosingErrorMessage() {
        return writerClosingErrorMessage;
    }

    /**
     * Sets the message to display when an error occurred while trying to close
     * the file writer during logs saving
     * 
     * @param writerClosingErrorMessage
     *            The message to display when an error occurred while trying to
     *            close the file writer during logs saving
     */
    public void setWriterClosingErrorMessage(String writerClosingErrorMessage) {
        if (writerClosingErrorMessage != null) {
            this.writerClosingErrorMessage = writerClosingErrorMessage;
        }
    }

    /**
     * Returns the text of the clear log button
     * 
     * @return A {@link String}
     */
    public String getClearText() {
        return clearButton.getText();
    }

    /**
     * Sets the text of the clear log button
     * 
     * @param text
     *            The text to set
     */
    public void setClearText(String text) {
        if (text != null) {
            clearButton.setText(text);
        }
    }

    /**
     * Returns the icon displayed in clear log button
     * 
     * @return An {@link Icon}
     */
    public Icon getClearIcon() {
        return clearButton.getIcon();
    }

    /**
     * Sets the icon to display in clear log button
     * 
     * @param icon
     *            The {@link Icon} to set
     */
    public void setClearIcon(Icon icon) {
        clearButton.setIcon(icon);
    }

    /**
     * Returns the text of the save log button
     * 
     * @return A {@link String}
     */
    public String getSaveText() {
        return saveButton.getText();
    }

    /**
     * Sets the text of the save log button
     * 
     * @param text
     *            The text to set
     */
    public void setSaveText(String text) {
        if (text != null) {
            saveButton.setText(text);
        }
    }

    /**
     * Returns the icon displayed in save log button
     * 
     * @return An {@link Icon}
     */
    public Icon getSaveIcon() {
        return saveButton.getIcon();
    }

    /**
     * Sets the icon to display in save log button
     * 
     * @param icon
     *            The {@link Icon} to set
     */
    public void setSaveIcon(Icon icon) {
        saveButton.setIcon(icon);
    }

    /**
     * Returns the text of the save log as button
     * 
     * @return A {@link String}
     */
    public String getSaveAsText() {
        return saveAsButton.getText();
    }

    /**
     * Sets the text of the save log as button
     * 
     * @param text
     *            The text to set
     */
    public void setSaveAsText(String text) {
        if (text != null) {
            saveAsButton.setText(text);
        }
    }

    /**
     * Returns the icon displayed in save log as button
     * 
     * @return An {@link Icon}
     */
    public Icon getSaveAsIcon() {
        return saveAsButton.getIcon();
    }

    /**
     * Sets the icon to display in save log as button
     * 
     * @param icon
     *            The {@link Icon} to set
     */
    public void setSaveAsIcon(Icon icon) {
        saveAsButton.setIcon(icon);
    }

    /**
     * Clears logs
     */
    public void clear() {
        synchronized (messageBuilder) {
            messageBuilder.delete(0, messageBuilder.length());
            if (html) {
                htmlTextArea.setText(ObjectUtils.EMPTY_STRING);
            } else {
                defaultTextArea.setText(ObjectUtils.EMPTY_STRING);
            }
        }
    }

    /**
     * Ask user where to save logs and then save them
     */
    protected void saveAs() {
        if (logSaver.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File toSave = logSaver.getSelectedFile();
            FileFilter filter = logSaver.getFileFilter();
            if (filter instanceof ExtensionFileFilter) {
                ExtensionFileFilter extensionFilter = (ExtensionFileFilter) filter;
                if (!extensionFilter.getExtension().equalsIgnoreCase(FileUtils.getExtension(toSave))) {
                    logSaver.setSelectedFile(new File(toSave.getAbsoluteFile() + "." + extensionFilter.getExtension()));
                }
            }
            save(false);
        }
    }

    /**
     * Saves logs
     * 
     * @param overWrite Whether to always overwrite when file already exists
     */
    protected void save(boolean overWrite) {
        final File toSave = logSaver.getSelectedFile();
        if (toSave == null) {
            saveAs();
        } else {
            int choice = JOptionPane.YES_OPTION;
            if ((!overWrite) && toSave.exists()) {
                choice = JOptionPane.showConfirmDialog(this, getFormattedMessage(overwriteMessage, toSave),
                        overwriteTitle, JOptionPane.YES_NO_OPTION);
            }
            if (choice == JOptionPane.YES_OPTION) {
                // save log in a separated thread
                new Thread("Save log in " + toSave.getAbsolutePath()) {
                    @Override
                    public void run() {
                        String log;
                        synchronized (messageBuilder) {
                            log = createHtmlBuilder().toString();
                        }
                        log = log.replace(ObjectUtils.NEW_LINE, CRLF);
                        FileWriter writer = null;
                        try {
                            writer = new FileWriter(toSave);
                            writer.write(log);
                        } catch (IOException e) {
                            logError(getFormattedMessage(saveErrorMessage, toSave), e);
                        } finally {
                            if (writer != null) {
                                try {
                                    writer.close();
                                } catch (IOException e) {
                                    logError(getFormattedMessage(writerClosingErrorMessage, toSave), e);
                                }
                            }
                        }
                    }
                }.start();
            }
        }
    }

    /**
     * Sets the {@link FileFilter}s to use with {@link JFileChooser} when saving
     * log as
     * 
     * @param filters
     *            The {@link FileFilter}s to use
     */
    public void setFileFilters(FileFilter... filters) {
        logSaver.resetChoosableFileFilters();
        if (filters != null) {
            for (FileFilter filter : filters) {
                if (filter != null) {
                    logSaver.addChoosableFileFilter(filter);
                }
            }
        }
    }

    /**
     * Adds an {@link ActionListener} to clear log button
     * 
     * @param listener
     *            The {@link ActionListener} to add
     */
    public void addClearButtonListener(ActionListener listener) {
        if (listener != null) {
            synchronized (clearLogListeners) {
                clearLogListeners.add(listener);
            }
        }
    }

    /**
     * Removes an {@link ActionListener} from clear log button
     * 
     * @param listener
     *            The {@link ActionListener} to remove
     */
    public void removeClearButtonListener(ActionListener listener) {
        if (listener != null) {
            synchronized (clearLogListeners) {
                clearLogListeners.remove(listener);
            }
        }
    }

    /**
     * Formats a message, using a file name
     * 
     * @param message
     *            The message to format
     * @param f
     *            The file
     * @return The formatted message
     */
    protected static final String getFormattedMessage(String message, File f) {
        String result;
        if (message.indexOf("%s") > -1) {
            result = String.format(message, f.getName());
        } else {
            result = message;
        }
        return result;
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        if (separator == null) {
            if (html) {
                this.separator = HTML_SEPARATOR;
            } else {
                this.separator = TEXT_SEPARATOR;
            }
        } else {
            this.separator = separator;
        }
    }

    protected StringBuilder createHtmlBuilder() {
        StringBuilder htmlBuilder = new StringBuilder("<html><head><style>");
        htmlBuilder.append(CLASS_SEPARATOR);
        for (LVL lvl : getLevels()) {
            String css = getDivCSSClass(lvl);
            if (css != null) {
                htmlBuilder.append(css);
            }
        }
        htmlBuilder.append("</style></head><body>");
        htmlBuilder.append(messageBuilder).append("</body></html>");
        return htmlBuilder;
    }

    protected void applyLogPosition() {
        if (html) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (isLogFirst()) {
                        textScrollPane.getVerticalScrollBar().setValue(0);
                    } else {
                        textScrollPane.getVerticalScrollBar()
                                .setValue(textScrollPane.getVerticalScrollBar().getMaximum());
                    }
                }
            };
            if (SwingUtilities.isEventDispatchThread()) {
                runnable.run();
            } else {
                SwingUtilities.invokeLater(runnable);
            }
        } else {
            if (isLogFirst()) {
                defaultTextArea.setCaretPosition(0);
            } else {
                defaultTextArea.setCaretPosition(defaultTextArea.getDocument().getLength());
            }
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class Publisher {
        public void publish(String message, LVL level) {
            if (message != null) {
                boolean insertLogAtBegining = logFirst;
                int max = maxLines;
                synchronized (messageBuilder) {
                    String date = dateFormat.format(new Date().getTime());
                    if (html) {
                        date = HtmlEscape.escape(date, true, true);
                    }
                    if (insertLogAtBegining) {
                        messageBuilder.insert(0, separator);
                        if (html) {
                            messageBuilder.insert(0, "</div>");
                        }
                        messageBuilder.insert(0, message);
                        if (publishDate) {
                            messageBuilder.insert(0, date);
                        }
                        if (html) {
                            messageBuilder.insert(0, getDiv(level));
                        }
                    } else {
                        messageBuilder.append(separator);
                        if (html) {
                            messageBuilder.append(getDiv(level));
                        }
                        if (publishDate) {
                            messageBuilder.append(date);
                        }
                        messageBuilder.append(message);
                        if (html) {
                            messageBuilder.append("</div>");
                        }
                    }
                    // Limit text size
                    int length = messageBuilder.length();
                    int lineCount = 1;
                    int indexInBuffer = -1;
                    if (insertLogAtBegining) {
                        // delete last lines when to many
                        if (html) {
                            int currentIndex = 0;
                            while ((currentIndex > -1) & (lineCount <= max)) {
                                currentIndex = messageBuilder.indexOf("<div>", currentIndex);
                                if (currentIndex > -1) {
                                    lineCount++;
                                    currentIndex++;
                                }
                            }
                            if (lineCount > max) {
                                indexInBuffer = currentIndex - 1;
                            }
                        } else {
                            for (int i = 0; i < length; i++) {
                                if (messageBuilder.charAt(i) == '\n') {
                                    lineCount++;
                                    if (lineCount > max) {
                                        indexInBuffer = i;
                                        break;
                                    }
                                }
                            }
                        }
                        if (indexInBuffer > 0) {
                            messageBuilder.delete(indexInBuffer, length);
                        }
                    } else {
                        // delete first lines when to many
                        if (html) {
                            List<Integer> indexes = new ArrayList<Integer>(max);
                            int currentIndex = 0;
                            while (currentIndex > -1) {
                                currentIndex = messageBuilder.indexOf("<div>", currentIndex);
                                if (currentIndex > -1) {
                                    indexes.add(Integer.valueOf(currentIndex));
                                    currentIndex++;
                                }
                            }
                            if (indexes.size() > max) {
                                indexInBuffer = indexes.get(indexes.size() - max).intValue();
                            }
                            indexes.clear();
                        } else {
                            for (int i = length - 1; i >= 0; i--) {
                                if (messageBuilder.charAt(i) == '\n') {
                                    lineCount++;
                                    if (lineCount > max) {
                                        indexInBuffer = i;
                                        break;
                                    }
                                }
                            }
                        }
                        if (indexInBuffer > 0) {
                            messageBuilder.delete(0, indexInBuffer);
                        }
                    }
                    // Display filtered text
                    transmitText();
                } // end synchronized (messageBuilder)
            }
        }

        protected void transmitText() {
            final String text;
            if (html) {
                text = createHtmlBuilder().toString();
            } else {
                text = messageBuilder.toString();
            }
            if (html) {
                htmlTextArea.setText(text);
            } else {
                defaultTextArea.setText(text);
            }
            applyLogPosition();
        }
    }

    protected class HtmlTextArea extends JLabel {

        private static final long serialVersionUID = 5400048879411120285L;

        @Override
        public void setText(String text) {
            if (loggingDelegate != null) {
                loggingDelegate.prepareScroll();
            }
            super.setText(text);
        }

        @Override
        public void revalidate() {
            super.revalidate();
            if (loggingDelegate != null) {
                loggingDelegate.updateScroll();
            }
        }
    }

    protected class DefaultTextArea extends JTextArea {

        private static final long serialVersionUID = 3870633119984461088L;

        @Override
        public void setText(String text) {
            if (loggingDelegate != null) {
                loggingDelegate.prepareScroll();
            }
            super.setText(text);
        }

        @Override
        public void revalidate() {
            super.revalidate();
            if (loggingDelegate != null) {
                loggingDelegate.updateScroll();
            }
        }
    }

}
