/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing;

/**
 * An {@link Enum} used to represent the way the scroll can be magnetized when a component is in a scrollpane.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public enum ScrollMagnetism {
    /**
     * No magnetism: let default behaviour.
     */
    NONE,
    /**
     * When component is resized, try to magnetize scroll to the top.
     */
    TOP,
    /**
     * When component is resized, try to magnetize scroll to the bottom.
     */
    BOTTOM,
    /**
     * When component is resized, try to magnetize scroll to the left.
     */
    LEFT,
    /**
     * When component is resized, try to magnetize scroll to the right.
     */
    RIGHT,
    /**
     * When component is resized, try to magnetize scroll to the top left corner.
     */
    TOP_LEFT,
    /**
     * When component is resized, try to magnetize scroll to the top right corner.
     */
    TOP_RIGHT,
    /**
     * When component is resized, try to magnetize scroll to the bottom left corner.
     */
    BOTTOM_LEFT,
    /**
     * When component is resized, try to magnetize scroll to the bottom right corner.
     */
    BOTTOM_RIGHT
}
