/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.frame;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;

/**
 * A {@link JFrame} that can be easily switched to full screen
 * 
 * @author HARDION
 */
public class FullScreenFrame extends JFrame {

    private static final long serialVersionUID = 6639807330208502316L;

    protected GraphicsDevice graphicsDevice;
    protected boolean fullScreen;

    public FullScreenFrame() {
        this(null);

        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] devices = env.getScreenDevices();
        this.graphicsDevice = devices[0];
    }

    public FullScreenFrame(GraphicsDevice graphicsDevice) {
        super("Swap");
        fullScreen = false;
        setSize(400, 400);
        setLocationRelativeTo(null);
        this.graphicsDevice = graphicsDevice;

        Action swapDisplay = new AbstractAction() {
            private static final long serialVersionUID = 1226681067307901759L;

            @Override
            public void actionPerformed(ActionEvent arg0) {
                fullScreen = !fullScreen;
                changeScreen(fullScreen);

            }
        };

        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, InputEvent.ALT_MASK), "swap display");
        getRootPane().getActionMap().put("swap display", swapDisplay);
    }

    public void changeScreen(boolean full) {
        if (full) {
            // Full-screen mode
            dispose();
            if (isDefaultLookAndFeelDecorated()) {
                getRootPane().setWindowDecorationStyle(JRootPane.NONE);
            }
            setUndecorated(true);

            setResizable(false);
            if (getJMenuBar() != null)
                getJMenuBar().setVisible(false);
            graphicsDevice.setFullScreenWindow(this); // comment this line if you want only undecorated frame
            setVisible(true);
        } else {
            // Windowed mode
            dispose();
            if (isDefaultLookAndFeelDecorated()) {
                getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
            } else {
                setUndecorated(false);
            }
            setResizable(true);
            if (getJMenuBar() != null)
                getJMenuBar().setVisible(true);
            graphicsDevice.setFullScreenWindow(null); // comment this line if you want only undecorated frame
            repaint();
            setVisible(true);
        }
    }

    public static void main(String[] args) {
        new FullScreenFrame().setVisible(true);
    }

}
