/*
 * This file is part of SwingUtilities.
 * 
 * SwingUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * SwingUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with SwingUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.swing.border;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

/**
 * A {@link Border} that represents a text bubble.
 * <p>
 * Code inspired by <a href=
 * "http://stackoverflow.com/questions/15025092/border-with-rounded-corners-transparency">
 * this solution</a>.
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT, Andrew Thompson
 */
public class TextBubbleBorder extends ShapedBorder {

    private final int thickness;
    private final int radii;
    private final int pointerSize;
    private final Insets insets;
    private final int strokePad;
    private final int pointerPad;
    private final boolean left;

    /**
     * Creates a new {@link TextBubbleBorder}
     * 
     * @param color The border color
     */
    public TextBubbleBorder(Color color) {
        this(color, 4, 8, 7);
    }

    /**
     * Creates a new TextBubbleBorder
     * 
     * @param color The border color
     * @param thickness The border thickness
     * @param radii The border radii
     * @param pointerSize The size of the pointing part of the text bubble
     */
    public TextBubbleBorder(Color color, int thickness, int radii, int pointerSize) {
        this(color, thickness, radii, pointerSize, true);
    }

    /**
     * Creates a new TextBubbleBorder
     * 
     * @param color The border color
     * @param thickness The border thickness
     * @param radii The border radii
     * @param pointerSize The size of the pointing part of the text bubble
     * @param left Whether the pointing part points left
     */
    public TextBubbleBorder(Color color, int thickness, int radii, int pointerSize, boolean left) {
        super(color, new BasicStroke(thickness));
        this.thickness = thickness;
        this.radii = radii;
        this.pointerSize = pointerSize;
        this.pointerPad = 4;

        strokePad = thickness / 2;

        int pad = radii + strokePad;
        int bottomPad = pad + pointerSize + strokePad;
        insets = new Insets(pad, pad, bottomPad, pad);
        this.left = left;
    }

    @Override
    protected Area buildShapedArea(int x, int y, int width, int height) {
        int bottomLineY = height - thickness - pointerSize;
        RoundRectangle2D.Double bubble = new RoundRectangle2D.Double(0 + strokePad, 0 + strokePad, width - thickness,
                bottomLineY, radii, radii);

        Polygon pointer = new Polygon();

        if (left) {
            // left point
            pointer.addPoint(strokePad + radii + pointerPad, bottomLineY);
            // right point
            pointer.addPoint(strokePad + radii + pointerPad + pointerSize, bottomLineY);
            // bottom point
            pointer.addPoint(strokePad + radii + pointerPad + (pointerSize / 2), height - strokePad);
        } else {
            // left point
            pointer.addPoint(width - (strokePad + radii + pointerPad), bottomLineY);
            // right point
            pointer.addPoint(width - (strokePad + radii + pointerPad + pointerSize), bottomLineY);
            // bottom point
            pointer.addPoint(width - (strokePad + radii + pointerPad + (pointerSize / 2)), height - strokePad);
        }

        Area area = new Area(bubble);
        area.add(new Area(pointer));
        return area;
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return insets;
    }

    public static void main(String[] args) {
        JFrame testFrame = new JFrame(TextBubbleBorder.class.getSimpleName() + " test");
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JPanel mainPanel = new JPanel(new BorderLayout(5, 5));
        mainPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
        JLabel label = new JLabel("Hey!");
        label.setBackground(Color.WHITE);
        label.setOpaque(true);
        label.setBorder(new TextBubbleBorder(Color.DARK_GRAY, 2, 5, 10));
        mainPanel.add(label, BorderLayout.CENTER);
        testFrame.setContentPane(mainPanel);
        testFrame.pack();
        testFrame.setVisible(true);
    }
}