/*******************************************************************************
 * Copyright (c) 2008-2021 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.lib.project.swing.dialog;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import org.junit.Ignore;

@Ignore
public class JAnchoredDialogTest {

    public static void main(String[] args) {
        final JFrame testFrame = new JFrame(JAnchoredDialog.class.getName() + " test: parent frame");
        testFrame.setContentPane(new JLabel("click to show " + JAnchoredDialog.class.getSimpleName()));
        testFrame.setIconImage(new ImageIcon(JAnchoredDialogTest.class.getResource("ledBlue.gif")).getImage());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        final JAnchoredDialog dialog = new JAnchoredDialog(testFrame, JAnchoredDialog.class.getName() + " test");
        dialog.setContentPane(new JLabel(dialog.getTitle()));
        dialog.setIconImage(new ImageIcon(JAnchoredDialogTest.class.getResource("ledGreen.gif")).getImage());
        testFrame.getContentPane().addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (!dialog.isVisible()) {
                    dialog.pack();
                    dialog.setLocationRelativeTo(testFrame);
                    dialog.setVisible(true);
                }
            }
        });
        testFrame.setSize(300, 200);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }
}
