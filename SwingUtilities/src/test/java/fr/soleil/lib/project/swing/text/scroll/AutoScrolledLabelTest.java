package fr.soleil.lib.project.swing.text.scroll;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.junit.Ignore;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.FontUtils;
import fr.soleil.lib.project.file.FileUtils;

/**
 * A class used to test {@link AutoScrolledLabel}.
 * <p>
 * As described in {@link ScrollDelegate#DRAW_LINES}, you may want to run your JVM with
 * <b><code>-DdrawLines=true</code></b> argument in order to draw the text and icon lines
 * </p>
 * <p>
 * Program arguments are expected to be like <b><code>key=value</code></b>, with no space surrounding "=", and key is
 * case sensitive
 * whereas value isn't.
 * <ul>
 * <table border="1" style="border-collapse:collapse">
 * <tr>
 * <th colspan="3">Accepted keys and values<br/>
 * <sup><i>(order is not important)</i></sup></th>
 * </tr>
 * <tr>
 * <th>Key</th>
 * <th>Value</th>
 * <th>Description</th>
 * </tr>
 * <tr>
 * <td>hAlign</td>
 * <td>One of these:
 * <ul>
 * <li>LEFT</li>
 * <li>LEADING</li>
 * <li>RIGHT</li>
 * <li>TRAILING</li>
 * <li>CENTER</li>
 * </ul>
 * </td>
 * <td>Horizontal alignment in {@link AutoScrolledLabel}</td>
 * </tr>
 * <tr>
 * <td>vAlign</td>
 * <td>One of these:
 * <ul>
 * <li>TOP</li>
 * <li>BOTTOM</li>
 * <li>CENTER</li>
 * </ul>
 * </td>
 * <td>Vertical alignment in {@link AutoScrolledLabel}</td>
 * </tr>
 * <tr>
 * <td>hTP</td>
 * <td>One of these:
 * <ul>
 * <li>LEFT</li>
 * <li>LEADING</li>
 * <li>RIGHT</li>
 * <li>TRAILING</li>
 * <li>CENTER</li>
 * </ul>
 * </td>
 * <td>Horizontal text position in {@link AutoScrolledLabel}</td>
 * </tr>
 * <tr>
 * <td>vTP</td>
 * <td>One of these:
 * <ul>
 * <li>TOP</li>
 * <li>BOTTOM</li>
 * <li>CENTER</li>
 * </ul>
 * </td>
 * <td>Vertical text position in {@link AutoScrolledLabel}</td>
 * </tr>
 * <tr>
 * <td>fontSize</td>
 * <td>A strictly positive integer</td>
 * <td>Font size in {@link AutoScrolledLabel}</td>
 * </tr>
 * <tr>
 * <td>icon</td>
 * <td>Path to an image file</td>
 * <td>The icon to apply to {@link AutoScrolledLabel}</td>
 * </tr>
 * </table>
 * </ul>
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
@Ignore
public class AutoScrolledLabelTest {

    private static final String TOP = "TOP";
    private static final String BOTTOM = "BOTTOM";
    private static final String CENTER = "CENTER";
    private static final String LEFT = "LEFT";
    private static final String RIGHT = "RIGHT";
    private static final String LEADING = "LEADING";
    private static final String TRAILING = "TRAILING";
    private static final String H_ALIGN = "hAlign=";
    private static final String V_ALIGN = "vAlign=";
    private static final String H_TP = "hTP=";
    private static final String V_TP = "vTP=";
    private static final String FONT_SIZE = "fontSize=";
    private static final String ICON = "icon=";
    private static final String ANTIALIASING = "antialiasing=";
    private static final String[] READABLE_IMAGES = ImageIO.getReaderFileSuffixes();

    protected static int parseConstant(String value) {
        int result;
        if (value == null) {
            result = -1;
        } else {
            String toParse = value.toUpperCase();
            switch (toParse) {
                case TOP:
                    result = SwingConstants.TOP;
                    break;
                case BOTTOM:
                    result = SwingConstants.BOTTOM;
                    break;
                case CENTER:
                    result = SwingConstants.CENTER;
                    break;
                case LEFT:
                    result = SwingConstants.LEFT;
                    break;
                case RIGHT:
                    result = SwingConstants.RIGHT;
                    break;
                case LEADING:
                    result = SwingConstants.LEADING;
                    break;
                case TRAILING:
                    result = SwingConstants.TRAILING;
                    break;
                default:
                    result = -1;
                    break;
            }
        }
        return result;
    }

    protected static int parseHorizontalConstant(String value, int defaultValue) {
        int constant = parseConstant(value);
        int result;
        switch (constant) {
            case SwingConstants.LEFT:
            case SwingConstants.LEADING:
            case SwingConstants.RIGHT:
            case SwingConstants.TRAILING:
            case SwingConstants.CENTER:
                result = constant;
                break;
            default:
                result = defaultValue;
                break;
        }
        return result;
    }

    protected static int parseVerticalConstant(String value, int defaultValue) {
        int constant = parseConstant(value);
        int result;
        switch (constant) {
            case SwingConstants.TOP:
            case SwingConstants.BOTTOM:
            case SwingConstants.CENTER:
                result = constant;
                break;
            default:
                result = defaultValue;
                break;
        }
        return result;
    }

    protected static String constantToString(int value) {
        String result;
        switch (value) {
            case SwingConstants.TOP:
                result = TOP;
                break;
            case SwingConstants.BOTTOM:
                result = BOTTOM;
                break;
            case SwingConstants.CENTER:
                result = CENTER;
                break;
            case SwingConstants.LEFT:
                result = LEFT;
                break;
            case SwingConstants.RIGHT:
                result = RIGHT;
                break;
            case SwingConstants.LEADING:
                result = LEADING;
                break;
            case SwingConstants.TRAILING:
                result = TRAILING;
                break;
            default:
                result = ObjectUtils.EMPTY_STRING;
                break;
        }
        return result;
    }

    protected static TitledBorder createTitleBorder(String title, Color color) {
        TitledBorder border = new TitledBorder(new LineBorder(color, 1), title);
        border.setTitleColor(color);
        return border;
    }

    protected static void parseArguments(AutoScrolledLabel scrolledLabel, JLabel label, JTextArea argumentsArea,
            String... args) {
        ImageIcon icon = null;
        Boolean antialiasing = null;
        int fontSize = -1;
        int hAlign = -1, vAlign = -1, hTP = -1, vTP = -1;
        if (args != null) {
            for (String arg : args) {
                if (arg.startsWith(H_ALIGN)) {
                    hAlign = parseHorizontalConstant(arg.substring(H_ALIGN.length()).trim().toUpperCase(), hAlign);
                } else if (arg.startsWith(V_ALIGN)) {
                    vAlign = parseVerticalConstant(arg.substring(V_ALIGN.length()).trim().toUpperCase(), vAlign);
                } else if (arg.startsWith(H_TP)) {
                    hTP = parseHorizontalConstant(arg.substring(H_TP.length()).trim().toUpperCase(), hTP);
                } else if (arg.startsWith(V_TP)) {
                    vTP = parseVerticalConstant(arg.substring(V_TP.length()).trim().toUpperCase(), vTP);
                } else if (arg.startsWith(ICON)) {
                    String path = arg.substring(ICON.length()).trim();
                    File tmp = new File(path);
                    if (tmp.exists() && !tmp.isDirectory()) {
                        String extension = FileUtils.getExtension(path);
                        for (String img : READABLE_IMAGES) {
                            if (img.equalsIgnoreCase(extension)) {
                                icon = new ImageIcon(path);
                                break;
                            }
                        }
                    }
                } else if (arg.startsWith(FONT_SIZE)) {
                    int tmp = -1;
                    try {
                        tmp = Integer.parseInt(arg.substring(FONT_SIZE.length()).trim());
                    } catch (Exception e) {
                        tmp = -1;
                    }
                    if (tmp > 0) {
                        fontSize = tmp;
                    }
                } else if (arg.startsWith(ANTIALIASING)) {
                    antialiasing = Boolean.valueOf(arg.substring(ANTIALIASING.length()).trim());
                }
            }
        }
        if (fontSize < 1) {
            fontSize = scrolledLabel.getFont().getSize();
        }
        if (hAlign < 0) {
            hAlign = scrolledLabel.getHorizontalAlignment();
        }
        if (hTP < 0) {
            hTP = scrolledLabel.getHorizontalTextPosition();
        }
        if (vAlign < 0) {
            vAlign = scrolledLabel.getVerticalAlignment();
        }
        if (vTP < 0) {
            vTP = scrolledLabel.getVerticalTextPosition();
        }
        if (antialiasing == null) {
            antialiasing = Boolean.valueOf(scrolledLabel.isAntiAliasingEnabled());
        }
        if (fontSize != scrolledLabel.getFont().getSize()) {
            scrolledLabel.setFont(scrolledLabel.getFont().deriveFont((float) fontSize));
        }
        scrolledLabel.setHorizontalAlignment(hAlign);
        scrolledLabel.setHorizontalTextPosition(hTP);
        scrolledLabel.setVerticalAlignment(vAlign);
        scrolledLabel.setVerticalTextPosition(vTP);
        scrolledLabel.setAntiAliasingEnabled(antialiasing.booleanValue());
        scrolledLabel.setIcon(icon);
        label.setIcon(icon);
        label.setFont(scrolledLabel.getFont());
        label.setHorizontalAlignment(scrolledLabel.getHorizontalAlignment());
        label.setVerticalAlignment(scrolledLabel.getVerticalAlignment());
        label.setHorizontalTextPosition(scrolledLabel.getHorizontalTextPosition());
        label.setVerticalTextPosition(scrolledLabel.getVerticalTextPosition());
        StringBuilder builder = new StringBuilder();
        builder.append(H_ALIGN).append(constantToString(hAlign));
        builder.append('\n').append(V_ALIGN).append(constantToString(vAlign));
        builder.append('\n').append(H_TP).append(constantToString(hTP));
        builder.append('\n').append(V_TP).append(constantToString(vTP));
        builder.append('\n').append(FONT_SIZE).append(fontSize);
        String description;
        if (icon == null) {
            description = ObjectUtils.EMPTY_STRING;
        } else {
            description = icon.getDescription();
            if (description.startsWith(ICON)) {
                description = description.substring(ICON.length());
            }
        }
        builder.append('\n').append(ICON).append(description);
        builder.append('\n').append(ANTIALIASING).append(antialiasing);
        argumentsArea.setText(builder.toString());
        builder.delete(0, builder.length());
    }

    public static void main(String[] args) {
        JFrame testFrame = new JFrame(AutoScrolledLabelTest.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        String initialText = FontUtils.TEXTLAYOUT_DEFAULT_TEXT;
        AutoScrolledLabel scrolledLabel = new AutoScrolledLabel(initialText);
        scrolledLabel.setBorder(createTitleBorder("Auto scrolled text", Color.BLUE));

        JPanel mainPanel = new JPanel(new BorderLayout());
        JPanel labelPanel = new JPanel(new GridLayout(2, 1));
        JLabel label = new JLabel(initialText);
        label.setBorder(createTitleBorder("Fixed text", Color.GRAY));
        JTextField editor = new JTextField(10);
        editor.setBorder(createTitleBorder("Text edit ( [Enter] to apply )", Color.RED));
        editor.addActionListener((e) -> {
            String text = editor.getText();
            scrolledLabel.setText(text);
            label.setText(text);
        });
        JLabel modeLabel = new JLabel(String.valueOf(scrolledLabel.getScrollMode()));
        modeLabel.setBorder(createTitleBorder("Scroll mode", new Color(0, 150, 0)));
        modeLabel.setFont(editor.getFont());
        modeLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                ScrollMode scrollMode = null;
                switch (scrolledLabel.getScrollMode()) {
                    case LOOP:
                        scrollMode = ScrollMode.PENDULAR;
                        break;
                    case PENDULAR:
                        scrollMode = ScrollMode.REACH_END;
                        break;
                    case REACH_END:
                        scrollMode = ScrollMode.LOOP;
                        break;
                }
                ((JLabel) e.getSource()).setText(String.valueOf(scrollMode));
                scrolledLabel.setScrollMode(scrollMode);
            }
        });

        JPanel speedPanel = new JPanel(new BorderLayout());
        JButton speedUpButton = new JButton("Speed up");
        speedUpButton.addActionListener((e) -> {
            int stepTime = scrolledLabel.getScrollStepsTime();
            if (stepTime == 0) {
                stepTime = 100;
            } else if (stepTime > 1) {
                stepTime /= 2;
            }
            scrolledLabel.setScrollStepsTime(stepTime);
        });
        JButton slowDownButton = new JButton("Slow down");
        slowDownButton.addActionListener((e) -> {
            int stepTime = scrolledLabel.getScrollStepsTime();
            if (stepTime > 0) {
                stepTime *= 2;
            }
            if (stepTime >= 100) {
                stepTime = 0;
            }
            scrolledLabel.setScrollStepsTime(stepTime);
        });
        JTextArea argumentsArea = new JTextArea(" ") {

            private static final long serialVersionUID = -8909822262449964714L;

            @Override
            public Dimension getPreferredSize() {
                Dimension size = super.getPreferredSize();
                size.width = Math.max(size.width, 235);
                return size;
            }
        };
        argumentsArea.setOpaque(false);
        argumentsArea.setBorder(createTitleBorder("Arguments  ( [CTRL]-[Enter] to apply )", Color.ORANGE));
        argumentsArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER && e.isControlDown()) {
                    parseArguments(scrolledLabel, label, argumentsArea, argumentsArea.getText().split("\n"));
                }
            }
        });
        parseArguments(scrolledLabel, label, argumentsArea, args);
        labelPanel.add(scrolledLabel);
        labelPanel.add(label);
        speedPanel.add(speedUpButton, BorderLayout.NORTH);
        speedPanel.add(slowDownButton, BorderLayout.SOUTH);
        mainPanel.add(labelPanel, BorderLayout.CENTER);
        mainPanel.add(editor, BorderLayout.SOUTH);
        mainPanel.add(modeLabel, BorderLayout.NORTH);
        mainPanel.add(speedPanel, BorderLayout.EAST);
        mainPanel.add(argumentsArea, BorderLayout.WEST);
        testFrame.setContentPane(mainPanel);
        testFrame.pack();
        testFrame.setSize(Math.max(600, testFrame.getWidth()), testFrame.getHeight());
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
        SwingUtilities.invokeLater(() -> {
            scrolledLabel.setText(ObjectUtils.EMPTY_STRING);
            label.setText(ObjectUtils.EMPTY_STRING);
        });
    }

}
