package fr.soleil.lib.project.ij;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.math.NumberArrayUtils;
import ij.gui.Roi;

/**
 * Class dedicated in image histogram calculations.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class HistogramTool {

    /**
     * Returns whether given bounds are invalid.
     * 
     * @param min The minimum.
     * @param max The maximum.
     * @param autoBounds Whether autoBounds is activated.
     * @return A <code>boolean</code>.
     */
    protected static boolean areInvalidBounds(double min, double max, boolean autoBounds) {
        return (!autoBounds) && (Double.isNaN(min) || Double.isNaN(max) || Double.isInfinite(min)
                || Double.isInfinite(max) || (min > max));
    }

    /**
     * Returns whether given step should be considered as invalid.
     * 
     * @param step The step.
     * @param autoStep Whether autostep is activated.
     * @return A <code>boolean</code>.
     */
    protected static boolean isInvalidStep(double step, boolean autoStep) {
        return (!autoStep) && (Double.isNaN(step) || Double.isInfinite(step) || step < MathConst.PRECISION_LIMIT);
    }

    /**
     * Prepares the histogram by filling it with possible values and previously initialized value counter.
     * 
     * @param min The known minimum.
     * @param max The known maximum.
     * @param step The known step.
     * @param length The known number of possible values.
     * @param histogram The histogram.
     * @param count The value counter.
     * @param abscissaIndex The index at which to put the array of possible values.
     */
    protected static void initHistogram(double min, double max, double step, int length, double[][] histogram,
            double[] count, int abscissaIndex) {
        double[] values = NumberArrayUtils.buildArray(min, step, length);
        values[length - 1] = max;
        histogram[abscissaIndex] = values;
        histogram[1 - abscissaIndex] = count;
    }

    /**
     * Updates the histogram value counter for given value.
     * 
     * @param value The value.
     * @param min The known minimum.
     * @param max The known maximum.
     * @param step The known step.
     * @param length The known number of possible values.
     * @param count The value counter.
     */
    protected static void updateHistoCount(double value, double min, double max, double step, int length,
            double[] count) {
        int index;
        if (value == max) {
            index = length - 1;
        } else {
            index = NumberArrayUtils.getIndexInArray(value, min, step);
        }
        if ((index > -1) && (index < length)) {
            count[index]++;
        }
    }

    /**
     * Fills an histogram with the particular single value case..
     * 
     * @param histogram THe histogram to fill.
     * @param value The value.
     * @param dataCount The number of data.
     * @param abscissaIndex The index at which to put the array of possible values.
     */
    protected static void fillEqualityHistogram(double[][] histogram, double value, int dataCount, int abscissaIndex) {
        double[] count = new double[3];
        double[] values = new double[3];
        histogram[abscissaIndex] = values;
        histogram[1 - abscissaIndex] = count;
        count[1] = dataCount;
        values[0] = value - 1;
        values[1] = value;
        values[2] = value + 1;
    }

    /**
     * Calculates the histogram of an image inside a {@link Roi}.
     * 
     * @param roi The {@link Roi}. If <code>null</code>, the histogram will be calculated on the whole image.
     * @param imageData The image data. It is expected to be a flat value, i.e. a single dimension number array.
     * @param dimX The image X dimension.
     * @param dimY The image Y dimension.
     * @param min The desired minimum for generated histogram. Ignored if <code>autoBounds</code> is <code>true</code>.
     * @param max The desired maximum for generated histogram. Ignored if <code>autoBounds</code> is <code>true</code>.
     * @param autoBounds Whether to automatically compute histogram minimum and maximum.
     * @param step The desired step in possible values array. If invalid, the result might be <code>null</code>.
     *            <p>
     *            <code>step</code> will be considered as invalid if it is <code>NaN</code> or infinite or
     *            <code>&lt; {@link MathConst#PRECISION_LIMIT}</code>
     *            </p>
     *            <p>
     *            If <code>autoStep</code> is <code>true</code>, <code>step</code> input will be ignored and the step
     *            will be automatically calculated.
     *            </p>
     * @param autoStep Whether to automatically compute histogram step.
     * @param abscissaIndex The index, in the resulting array, at which to put the histogram abscissa, i.e. the possible
     *            values array. It is expected to be 0 or 1.
     * @return An array of 2 elements:
     *         <ul>
     *         <li>The possible values, as a <code>double[]</code>, placed at index <code>abscissaIndex</code></li>
     *         <li>The number of element matching the value, as a <code>double[]</code>, placed at the other index.</li>
     *         </ul>
     *         That array might be <code>null</code> if inputs are invalid.
     */
    public static double[][] getHistogram(Roi roi, Object imageData, int dimX, int dimY, double min, double max,
            boolean autoBounds, double step, boolean autoStep, int abscissaIndex) {
        double[][] result;
        if ((imageData == null) || (!imageData.getClass().isArray())
                || (!ObjectUtils.isNumberClass(imageData.getClass().getComponentType()))
                || areInvalidBounds(min, max, autoBounds) || isInvalidStep(step, autoStep)) {
            result = null;
        } else {
            int xIndex = Math.max(0, Math.min(abscissaIndex, 1));
            int[] roiBounds = RoiTool.extractUsableRoiBounds(roi, dimX, dimY, true);
            int minYroi, maxYroi, minXroi, maxXroi;
            if (roiBounds == null) {
                minYroi = 0;
                maxYroi = dimY;
                minXroi = 0;
                maxXroi = dimX;
            } else {
                minYroi = roiBounds[0];
                maxYroi = roiBounds[1];
                minXroi = roiBounds[2];
                maxXroi = roiBounds[3];
            }
            roiBounds = null;
            int maxValues = 65536;// 2^16
            int length = Math.min(maxXroi * maxYroi, maxValues);
            if (length < 1) {
                result = null;
            } else {
                if (imageData instanceof byte[]) {
                    // byte image data
                    byte[] array = (byte[]) imageData;
                    if (autoBounds) {
                        // if autoBounds, recover min and max inside the Roi
                        min = Double.POSITIVE_INFINITY;
                        max = Double.NEGATIVE_INFINITY;
                        for (int y = minYroi; y < maxYroi; y++) {
                            for (int x = minXroi; x < maxXroi; x++) {
                                byte value = array[y * dimX + x];
                                if (value < min) {
                                    min = value;
                                }
                                if (value > max) {
                                    max = value;
                                }
                            }
                        }
                    }
                    if (min > max) {
                        // invalid bounds
                        result = null;
                    } else {
                        if (!autoStep) {
                            // compute histogram length from according to given step
                            length = NumberArrayUtils.recoverLength(min, max, step);
                        }
                        if (length < 1) {
                            // invalid step
                            result = null;
                        } else {
                            // try to fill histogram
                            result = new double[2][];
                            double[] count;
                            if (min == max) {
                                // all values are the same
                                fillEqualityHistogram(result, min, length, xIndex);
                            } else {
                                if (autoStep) {
                                    // compute step if auto step
                                    step = NumberArrayUtils.recoverStep(min, max, length);
                                }
                                // classic case: prepare possible values, value counter, and then fill histogram
                                count = new double[length];
                                initHistogram(min, max, step, length, result, count, xIndex);
                                for (int y = minYroi; y < maxYroi; y++) {
                                    for (int x = minXroi; x < maxXroi; x++) {
                                        updateHistoCount(array[y * dimX + x], minXroi, maxValues, step, length, count);
                                    }
                                }
                            }
                        }
                    } // end if (min > max) ... else
                } else if (imageData instanceof short[]) {
                    // short image data
                    short[] array = (short[]) imageData;
                    if (autoBounds) {
                        // if autoBounds, recover min and max inside the Roi
                        min = Double.POSITIVE_INFINITY;
                        max = Double.NEGATIVE_INFINITY;
                        for (int y = minYroi; y < maxYroi; y++) {
                            for (int x = minXroi; x < maxXroi; x++) {
                                short value = array[y * dimX + x];
                                if (value < min) {
                                    min = value;
                                }
                                if (value > max) {
                                    max = value;
                                }
                            }
                        }
                    }
                    if (min > max) {
                        // invalid bounds
                        result = null;
                    } else {
                        if (!autoStep) {
                            // compute histogram length from according to given step
                            length = NumberArrayUtils.recoverLength(min, max, step);
                        }
                        if (length < 1) {
                            // invalid step
                            result = null;
                        } else {
                            // try to fill histogram
                            result = new double[2][];
                            double[] count;
                            if (min == max) {
                                // all values are the same
                                fillEqualityHistogram(result, min, length, xIndex);
                            } else {
                                if (autoStep) {
                                    // compute step if auto step
                                    step = NumberArrayUtils.recoverStep(min, max, length);
                                }
                                // classic case: prepare possible values, value counter, and then fill histogram
                                count = new double[length];
                                initHistogram(min, max, step, length, result, count, xIndex);
                                for (int y = minYroi; y < maxYroi; y++) {
                                    for (int x = minXroi; x < maxXroi; x++) {
                                        updateHistoCount(array[y * dimX + x], minXroi, maxValues, step, length, count);
                                    }
                                }
                            }
                        }
                    } // end if (min > max) ... else
                } else if (imageData instanceof int[]) {
                    // int image data
                    int[] array = (int[]) imageData;
                    if (autoBounds) {
                        // if autoBounds, recover min and max inside the Roi
                        min = Double.POSITIVE_INFINITY;
                        max = Double.NEGATIVE_INFINITY;
                        for (int y = minYroi; y < maxYroi; y++) {
                            for (int x = minXroi; x < maxXroi; x++) {
                                int value = array[y * dimX + x];
                                if (value < min) {
                                    min = value;
                                }
                                if (value > max) {
                                    max = value;
                                }
                            }
                        }
                    }
                    if (min > max) {
                        // invalid bounds
                        result = null;
                    } else {
                        if (!autoStep) {
                            // compute histogram length from according to given step
                            length = NumberArrayUtils.recoverLength(min, max, step);
                        }
                        if (length < 1) {
                            // invalid step
                            result = null;
                        } else {
                            // try to fill histogram
                            result = new double[2][];
                            double[] count;
                            if (min == max) {
                                // all values are the same
                                fillEqualityHistogram(result, min, length, xIndex);
                            } else {
                                if (autoStep) {
                                    // compute step if auto step
                                    step = NumberArrayUtils.recoverStep(min, max, length);
                                }
                                // classic case: prepare possible values, value counter, and then fill histogram
                                count = new double[length];
                                initHistogram(min, max, step, length, result, count, xIndex);
                                for (int y = minYroi; y < maxYroi; y++) {
                                    for (int x = minXroi; x < maxXroi; x++) {
                                        updateHistoCount(array[y * dimX + x], minXroi, maxValues, step, length, count);
                                    }
                                }
                            }
                        }
                    } // end if (min > max) ... else
                } else if (imageData instanceof long[]) {
                    // long image data
                    long[] array = (long[]) imageData;
                    if (autoBounds) {
                        // if autoBounds, recover min and max inside the Roi
                        min = Double.POSITIVE_INFINITY;
                        max = Double.NEGATIVE_INFINITY;
                        for (int y = minYroi; y < maxYroi; y++) {
                            for (int x = minXroi; x < maxXroi; x++) {
                                long value = array[y * dimX + x];
                                if (value < min) {
                                    min = value;
                                }
                                if (value > max) {
                                    max = value;
                                }
                            }
                        }
                    }
                    if (min > max) {
                        // invalid bounds
                        result = null;
                    } else {
                        if (!autoStep) {
                            // compute histogram length from according to given step
                            length = NumberArrayUtils.recoverLength(min, max, step);
                        }
                        if (length < 1) {
                            // invalid step
                            result = null;
                        } else {
                            // try to fill histogram
                            result = new double[2][];
                            double[] count;
                            if (min == max) {
                                // all values are the same
                                fillEqualityHistogram(result, min, length, xIndex);
                            } else {
                                if (autoStep) {
                                    // compute step if auto step
                                    step = NumberArrayUtils.recoverStep(min, max, length);
                                }
                                // classic case: prepare possible values, value counter, and then fill histogram
                                count = new double[length];
                                initHistogram(min, max, step, length, result, count, xIndex);
                                for (int y = minYroi; y < maxYroi; y++) {
                                    for (int x = minXroi; x < maxXroi; x++) {
                                        updateHistoCount(array[y * dimX + x], minXroi, maxValues, step, length, count);
                                    }
                                }
                            }
                        }
                    } // end if (min > max) ... else
                } else if (imageData instanceof float[]) {
                    // float image data
                    float[] array = (float[]) imageData;
                    int realLength = 0;
                    if (autoBounds) {
                        // if autoBounds, recover min and max inside the Roi
                        min = Double.POSITIVE_INFINITY;
                        max = Double.NEGATIVE_INFINITY;
                        for (int y = minYroi; y < maxYroi; y++) {
                            for (int x = minXroi; x < maxXroi; x++) {
                                float value = array[y * dimX + x];
                                if ((!Float.isNaN(value)) && (!Float.isInfinite(value))) {
                                    realLength++;
                                    if (value < min) {
                                        min = value;
                                    }
                                    if (value > max) {
                                        max = value;
                                    }
                                }
                            }
                        }
                    } else {
                        for (int y = minYroi; y < maxYroi; y++) {
                            for (int x = minXroi; x < maxXroi; x++) {
                                float value = array[y * dimX + x];
                                if ((!Float.isNaN(value)) && (!Float.isInfinite(value))) {
                                    realLength++;
                                }
                            }
                        }
                    }
                    if (autoStep) {
                        length = Math.min(length, realLength);
                    }
                    if ((min > max) || (length < 1)) {
                        result = null;
                    } else {
                        if (!autoStep) {
                            // compute histogram length from according to given step
                            length = NumberArrayUtils.recoverLength(min, max, step);
                        }
                        if (length < 1) {
                            // invalid step
                            result = null;
                        } else {
                            // try to fill histogram
                            result = new double[2][];
                            if (min == max) {
                                // all values are the same
                                fillEqualityHistogram(result, min, realLength, xIndex);
                            } else {
                                if (autoStep) {
                                    // compute step if auto step
                                    step = NumberArrayUtils.recoverStep(min, max, length);
                                }
                                if (Math.abs(step) < MathConst.PRECISION_LIMIT) {
                                    // all values are almost the same
                                    fillEqualityHistogram(result, min, realLength, xIndex);
                                } else {
                                    // classic case: prepare possible values, value counter, and then fill histogram
                                    double[] count = new double[length];
                                    initHistogram(min, max, step, length, result, count, xIndex);
                                    for (int y = minYroi; y < maxYroi; y++) {
                                        for (int x = minXroi; x < maxXroi; x++) {
                                            float value = array[y * dimX + x];
                                            if ((!Float.isNaN(value)) && (!Float.isInfinite(value))) {
                                                updateHistoCount(value, min, maxValues, step, length, count);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } // end if ((min > max) || (length < 1)) ... else
                } else if (imageData instanceof double[]) {
                    // double image data
                    double[] array = (double[]) imageData;
                    int realLength = 0;
                    if (autoBounds) {
                        // if autoBounds, recover min and max inside the Roi
                        min = Double.POSITIVE_INFINITY;
                        max = Double.NEGATIVE_INFINITY;
                        for (int y = minYroi; y < maxYroi; y++) {
                            for (int x = minXroi; x < maxXroi; x++) {
                                double value = array[y * dimX + x];
                                if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                                    realLength++;
                                    if (value < min) {
                                        min = value;
                                    }
                                    if (value > max) {
                                        max = value;
                                    }
                                }
                            }
                        }
                    } else {
                        for (int y = minYroi; y < maxYroi; y++) {
                            for (int x = minXroi; x < maxXroi; x++) {
                                double value = array[y * dimX + x];
                                if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                                    realLength++;
                                }
                            }
                        }
                    }
                    if (autoStep) {
                        length = Math.min(length, realLength);
                    }
                    if ((min > max) || (length < 1)) {
                        result = null;
                    } else {
                        if (!autoStep) {
                            // compute histogram length from according to given step
                            length = NumberArrayUtils.recoverLength(min, max, step);
                        }
                        if (length < 1) {
                            // invalid step
                            result = null;
                        } else {
                            // try to fill histogram
                            result = new double[2][];
                            if (min == max) {
                                // all values are the same
                                fillEqualityHistogram(result, min, realLength, xIndex);
                            } else {
                                if (autoStep) {
                                    // compute step if auto step
                                    step = NumberArrayUtils.recoverStep(min, max, length);
                                }
                                if (Math.abs(step) < MathConst.PRECISION_LIMIT) {
                                    // all values are almost the same
                                    fillEqualityHistogram(result, min, realLength, xIndex);
                                } else {
                                    // classic case: prepare possible values, value counter, and then fill histogram
                                    double[] count = new double[length];
                                    initHistogram(min, max, step, length, result, count, xIndex);
                                    for (int y = minYroi; y < maxYroi; y++) {
                                        for (int x = minXroi; x < maxXroi; x++) {
                                            double value = array[y * dimX + x];
                                            if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                                                updateHistoCount(value, min, maxValues, step, length, count);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } // end if ((min > max) || (length < 1)) ... else
                } else if (imageData instanceof Number[]) {
                    // Number image data
                    Number[] array = (Number[]) imageData;
                    int realLength = 0;
                    if (autoBounds) {
                        // if autoBounds, recover min and max inside the Roi
                        min = Double.POSITIVE_INFINITY;
                        max = Double.NEGATIVE_INFINITY;
                        for (int y = minYroi; y < maxYroi; y++) {
                            for (int x = minXroi; x < maxXroi; x++) {
                                Number tmp = array[y * dimX + x];
                                double value = tmp == null ? MathConst.NAN_FOR_NULL : tmp.doubleValue();
                                if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                                    realLength++;
                                    if (value < min) {
                                        min = value;
                                    }
                                    if (value > max) {
                                        max = value;
                                    }
                                }
                            }
                        }
                    } else {
                        for (int y = minYroi; y < maxYroi; y++) {
                            for (int x = minXroi; x < maxXroi; x++) {
                                Number tmp = array[y * dimX + x];
                                double value = tmp == null ? MathConst.NAN_FOR_NULL : tmp.doubleValue();
                                if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                                    realLength++;
                                }
                            }
                        }
                    }
                    if (autoStep) {
                        length = Math.min(length, realLength);
                    }
                    if ((min > max) || (length < 1)) {
                        result = null;
                    } else {
                        if (!autoStep) {
                            // compute histogram length from according to given step
                            length = NumberArrayUtils.recoverLength(min, max, step);
                        }
                        if (length < 1) {
                            // invalid step
                            result = null;
                        } else {
                            // try to fill histogram
                            result = new double[2][];
                            if (min == max) {
                                // all values are the same
                                fillEqualityHistogram(result, min, realLength, xIndex);
                            } else {
                                if (autoStep) {
                                    // compute step if auto step
                                    step = NumberArrayUtils.recoverStep(min, max, length);
                                }
                                if (Math.abs(step) < MathConst.PRECISION_LIMIT) {
                                    // all values are almost the same
                                    fillEqualityHistogram(result, min, realLength, xIndex);
                                } else {
                                    // classic case: prepare possible values, value counter, and then fill histogram
                                    double[] count = new double[length];
                                    initHistogram(min, max, step, length, result, count, xIndex);
                                    for (int y = minYroi; y < maxYroi; y++) {
                                        for (int x = minXroi; x < maxXroi; x++) {
                                            Number tmp = array[y * dimX + x];
                                            double value = tmp == null ? MathConst.NAN_FOR_NULL : tmp.doubleValue();
                                            if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                                                updateHistoCount(value, min, maxValues, step, length, count);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } // end if ((min > max) || (length < 1)) ... else
                } else {
                    // incompatible image data
                    result = null;
                } // end array type tests
            } // end if (length < 1) ... else
        } // end if ((imageData == null) || (!imageData.getClass().isArray()) ||
          // (!ObjectUtils.isNumberClass(imageData.getClass().getComponentType()))) ... else
        return result;
    }

}
