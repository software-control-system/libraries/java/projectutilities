package fr.soleil.lib.project.ij;

import java.awt.Rectangle;
import java.util.Arrays;
import java.util.Collection;

import ij.gui.Roi;

/**
 * Class dedicated in flat matrixes calculations.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class FlatMatrixTool {

    /**
     * Calculates masked zone inside an inner Roi, combined with some masks.
     * This algorithm supposes you already hided the exterior of the Roi surrounding rectangle
     * 
     * @param imageData The original value, represented as a single dimension array (example: <code>byte[]</code>,
     *            <code>float[]</code>, {@link Number}<code>[]</code>, etc...)
     * @param imageData The original value,
     * @param masks The masks
     * @param minYroi The previously calculated roi included minimum y inside the matrix
     * @param maxYroi The previously calculated roi excluded maximum y inside the matrix
     * @param minXroi The previously calculated roi included minimum x inside the matrix
     * @param maxXroi The previously calculated roi excluded maximum x inside the matrix
     * @param dimX The matrix width
     * @param result The pre-filled <code>double</code> matrix, that should be updated
     */
    protected static void showInterior(Roi roi, Object imageData, Collection<boolean[]> masks, int minYroi, int maxYroi,
            int minXroi, int maxXroi, int dimX, double[] result) {
        if (imageData instanceof byte[]) {
            byte[] array = (byte[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof byte[])
        else if (imageData instanceof short[]) {
            short[] array = (short[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof short[])
        else if (imageData instanceof int[]) {
            int[] array = (int[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof int[])
        else if (imageData instanceof long[]) {
            long[] array = (long[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof long[])
        else if (imageData instanceof float[]) {
            float[] array = (float[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof float[])
        else if (imageData instanceof double[]) {
            double[] array = (double[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof double[])
        else if (imageData instanceof Number[]) {
            Number[] array = (Number[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = IJMathTool.extractDouble(array[(y * dimX) + x]);
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof Number[])
    }

    /**
     * Calculates masked zone inside an inner Roi. This algorithm supposes you already hided the exterior of the Roi
     * surrounding rectangle
     * 
     * @param imageData The original value, represented as a single dimension array (example: <code>byte[]</code>,
     *            <code>float[]</code>, {@link Number}<code>[]</code>, etc...)
     * @param imageData The original value,
     * @param minYroi The previously calculated roi included minimum y inside the matrix
     * @param maxYroi The previously calculated roi excluded maximum y inside the matrix
     * @param minXroi The previously calculated roi included minimum x inside the matrix
     * @param maxXroi The previously calculated roi excluded maximum x inside the matrix
     * @param dimX The matrix width
     * @param result The pre-filled <code>double</code> matrix, that should be updated
     */
    protected static void showInterior(Roi roi, Object imageData, int minYroi, int maxYroi, int minXroi, int maxXroi,
            int dimX, double[] result) {
        if (imageData instanceof byte[]) {
            byte[] array = (byte[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof byte[])
        else if (imageData instanceof short[]) {
            short[] array = (short[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof short[])
        else if (imageData instanceof int[]) {
            int[] array = (int[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof int[])
        else if (imageData instanceof long[]) {
            long[] array = (long[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof long[])
        else if (imageData instanceof float[]) {
            float[] array = (float[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof float[])
        else if (imageData instanceof double[]) {
            double[] array = (double[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof double[])
        else if (imageData instanceof Number[]) {
            Number[] array = (Number[]) imageData;
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isInsideRoi(roi, y, x)) {
                        result[y * dimX + x] = IJMathTool.extractDouble(array[(y * dimX) + x]);
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = Double.NaN;
                }
            }
        } // end if (imageData instanceof Number[])
    }

    /**
     * Calculates masked zone outside an outer Roi, combined with 2 masks.
     * 
     * @param imageData The original value, represented as a single dimension array (example: <code>byte[]</code>,
     *            <code>float[]</code>, {@link Number}<code>[]</code>, etc...)
     * @param imageData The original value,
     * @param mainMask The 1st mask
     * @param secondaryMask The 2nd mask
     * @param minYroi The previously calculated roi included minimum y inside the matrix
     * @param maxYroi The previously calculated roi excluded maximum y inside the matrix
     * @param minXroi The previously calculated roi included minimum x inside the matrix
     * @param maxXroi The previously calculated roi excluded maximum x inside the matrix
     * @param dimX he matrix width
     * @param result The <code>double</code> matrix that should be updated with expected values
     */
    protected static void showExterior(Roi roi, Object imageData, Collection<boolean[]> masks, int minYroi, int maxYroi,
            int minXroi, int maxXroi, int dimX, int dimY, double[] result) {
        if (imageData instanceof byte[]) {
            byte[] array = (byte[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
        } // end if (imageData instanceof byte[])
        else if (imageData instanceof short[]) {
            short[] array = (short[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
        } // end if (imageData instanceof short[])
        else if (imageData instanceof int[]) {
            int[] array = (int[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
        } // end if (imageData instanceof int[])
        else if (imageData instanceof long[]) {
            long[] array = (long[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
        } // end if (imageData instanceof long[])
        else if (imageData instanceof float[]) {
            float[] array = (float[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
        } // end if (imageData instanceof float[])
        else if (imageData instanceof double[]) {
            double[] array = (double[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
        } // end if (imageData instanceof double[])
        else if (imageData instanceof Number[]) {
            Number[] array = (Number[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = IJMathTool.extractDouble(array[(y * dimX) + x]);
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = IJMathTool.extractDouble(array[(y * dimX) + x]);
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x) && MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = IJMathTool.extractDouble(array[(y * dimX) + x]);
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = IJMathTool.extractDouble(array[(y * dimX) + x]);
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    if (MaskTool.andMaskValues(masks, y, x, dimX)) {
                        result[y * dimX + x] = IJMathTool.extractDouble(array[(y * dimX) + x]);
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
            }
        } // end if (imageData instanceof Number[])
    }

    /**
     * Calculates masked zone outside an outer Roi.
     * 
     * @param imageData The original value, represented as a single dimension array (example: <code>byte[]</code>,
     *            <code>float[]</code>, {@link Number}<code>[]</code>, etc...)
     * @param imageData The original value,
     * @param minYroi The previously calculated roi included minimum y inside the matrix
     * @param maxYroi The previously calculated roi excluded maximum y inside the matrix
     * @param minXroi The previously calculated roi included minimum x inside the matrix
     * @param maxXroi The previously calculated roi excluded maximum x inside the matrix
     * @param dimX The matrix width
     * @param result The <code>double</code> matrix that should be updated with expected values
     */
    protected static void showExterior(Roi roi, Object imageData, int minYroi, int maxYroi, int minXroi, int maxXroi,
            int dimX, int dimY, double[] result) {
        if (imageData instanceof byte[]) {
            byte[] array = (byte[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
        } // end if (imageData instanceof byte[])
        else if (imageData instanceof short[]) {
            short[] array = (short[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
        } // end if (imageData instanceof short[])
        else if (imageData instanceof int[]) {
            int[] array = (int[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
        } // end if (imageData instanceof int[])
        else if (imageData instanceof long[]) {
            long[] array = (long[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
        } // end if (imageData instanceof long[])
        else if (imageData instanceof float[]) {
            float[] array = (float[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
        } // end if (imageData instanceof float[])
        else if (imageData instanceof double[]) {
            double[] array = (double[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x)) {
                        result[y * dimX + x] = array[(y * dimX) + x];
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = array[(y * dimX) + x];
                }
            }
        } // end if (imageData instanceof double[])
        else if (imageData instanceof Number[]) {
            Number[] array = (Number[]) imageData;
            // treat top rectangle
            for (int y = 0; y < minYroi; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = IJMathTool.extractDouble(array[(y * dimX) + x]);
                }
            }
            for (int y = minYroi; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < minXroi; x++) {
                    result[y * dimX + x] = IJMathTool.extractDouble(array[(y * dimX) + x]);
                }
                // treat roi rectangle
                for (int x = minXroi; x < maxXroi; x++) {
                    if (RoiTool.isOutsideRoi(roi, y, x)) {
                        result[y * dimX + x] = IJMathTool.extractDouble(array[(y * dimX) + x]);
                    } else {
                        result[y * dimX + x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = maxXroi; x < dimX; x++) {
                    result[y * dimX + x] = IJMathTool.extractDouble(array[(y * dimX) + x]);
                }
            }
            // treat bottom rectangle
            for (int y = maxYroi; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y * dimX + x] = IJMathTool.extractDouble(array[(y * dimX) + x]);
                }
            }
        } // end if (imageData instanceof Number[])
    }

    /**
     * Calculates and returns the <code>double[]</code> resulting from an
     * original value, masked by a {@link Roi} and some mask matrixes
     * 
     * @param roi The {@link Roi}
     * @param imageData The original value, represented as a single dimension array (example: <code>byte[]</code>,
     *            <code>float[]</code>, {@link Number}<code>[]</code>, etc...)
     * @param dimX The matrix width
     * @param dimY The matrix height
     * @param innerRoi A <code>boolean</code> whether the {@link Roi} is an inner {@link Roi} (i.e. exterior of this
     *            {@link Roi} should be considered as masked) or an outer {@link Roi} (i.e. interior of this {@link Roi}
     *            should be considered as masked). <code>TRUE</code> for an inner {@link Roi}
     * @param maskValues The masks to apply. Each mask should be a matrix at image dimensions, with <code>true</code>
     *            for visible and <code>false</code> for masked.
     * @return A <code>double[]</code>, with {@link Double#NaN} value in masked zones, and original value cast into
     *         <code>double</code> in the rest of the flat matrix.
     */
    public static double[] getDataAsFlatDoubleMatrix(Roi roi, Object imageData, int dimX, int dimY, boolean innerRoi,
            Collection<boolean[]> maskValues) {
        double[] result = null;
        if ((imageData != null) && (dimY > -1) && (dimX > -1)) {
            result = new double[dimY * dimX];
            Rectangle roiBounds = RoiTool.getRoiBounds(roi, dimX, dimY, innerRoi);
            int[] usableBounds = RoiTool.extractUsableRoiBounds(roiBounds, dimX, dimY);
            if ((roiBounds == null) || (usableBounds == null)) {
                Arrays.fill(result, Double.NaN);
            } else {
                // Roi Y minimum, included, inside the image
                int minYroi = usableBounds[0];
                // Roi Y maximum, excluded, inside the image
                int maxYroi = usableBounds[1];
                // Roi X minimum, included, inside the image
                int minXroi = usableBounds[2];
                // Roi X maximum, excluded, inside the image
                int maxXroi = usableBounds[3];
                if (innerRoi) {
                    // treat top rectangle
                    for (int y = 0; y < minYroi; y++) {
                        for (int x = 0; x < dimX; x++) {
                            result[y * dimX + x] = Double.NaN;
                        }
                    }
                    if ((maskValues == null) || (maskValues.isEmpty())) {
                        // 0 mask
                        showInterior(roi, imageData, minYroi, maxYroi, minXroi, maxXroi, dimX, result);
                    } else {
                        showInterior(roi, imageData, maskValues, minYroi, maxYroi, minXroi, maxXroi, dimX, result);
                    }
                    // treat bottom rectangle
                    for (int y = roiBounds.y + roiBounds.height; y < dimY; y++) {
                        for (int x = 0; x < dimX; x++) {
                            result[y * dimX + x] = Double.NaN;
                        }
                    }
                } // end if (innerRoi)
                else {
                    if ((maskValues == null) || (maskValues.isEmpty())) {
                        // 0 mask
                        showExterior(roi, imageData, minYroi, maxYroi, minXroi, maxXroi, dimX, dimY, result);
                    } else {
                        showExterior(roi, imageData, maskValues, minYroi, maxYroi, minXroi, maxXroi, dimX, dimY,
                                result);
                    }
                } // end if (innerRoi)...else
            } // end if (returnNaN)...else
        } // end if (imageData != null)
        return result;
    }

    /**
     * Extracts the sub-matrix defined by an inner rectangle {@link Roi}, and
     * returns its minimum, maximum, and mean value
     * 
     * @param innerRectangleRoi The inner rectangle {@link Roi}
     * @param imageData The image data
     * @param dimX The matrix width
     * @param dimY The matrix height
     * @return A <code>double[]</code>, never <code>null</code>: <code>{ minimum, maximum, mean value }</code>
     */
    public static double[] extractSubMatrixMinMaxAndMean(Roi innerRectangleRoi, Object imageData, int dimX, int dimY) {
        return extractSubMatrixMinMaxMeanAndStdDev(innerRectangleRoi, imageData, dimX, dimY, false);
    }

    /**
     * Extracts the sub-matrix defined by an inner rectangle {@link Roi}, and
     * returns its minimum, maximum, mean value and standard deviation
     * 
     * @param innerRectangleRoi The inner rectangle {@link Roi}
     * @param imageData The image data
     * @param dimX The matrix width
     * @param dimY The matrix height
     * @return A <code>double[]</code>, never <code>null</code>:
     *         <code>{minimum, maximum, mean value, standard deviation, sample standard deviation}</code>
     */
    public static double[] extractSubMatrixMinMaxMeanAndStdDev(Roi innerRectangleRoi, Object imageData, int dimX,
            int dimY) {
        return extractSubMatrixMinMaxMeanAndStdDev(innerRectangleRoi, imageData, dimX, dimY, true);
    }

    /**
     * Extracts the sub-matrix defined by an inner rectangle {@link Roi}, and
     * returns its minimum, maximum, mean value and if asked its standard deviation
     * 
     * @param innerRectangleRoi The inner rectangle {@link Roi}
     * @param imageData The image data
     * @param dimX The matrix width
     * @param dimY The matrix height
     * @param extractStandardDeviation Whether to extract standardDeviation
     * @return A <code>double[]</code>, never <code>null</code>:
     *         <ul>
     *         <li><code>{minimum, maximum, mean value, standard deviation, sample standard deviation}</code> if
     *         <code>extractStandardDeviation</code> is <code>true</code></li>
     *         <li><code>{minimum, maximum, mean value}</code> if <code>extractStandardDeviation</code> is
     *         <code>false</code></li>
     *         </ul>
     */
    protected static double[] extractSubMatrixMinMaxMeanAndStdDev(Roi innerRectangleRoi, Object imageData, int dimX,
            int dimY, boolean extractStandardDeviation) {
        double[] result;
        int[] usableBounds = RoiTool.extractUsableRoiBounds(RoiTool.getRoiBounds(innerRectangleRoi, dimX, dimY, true),
                dimX, dimY);
        if (usableBounds == null) {
            result = new double[extractStandardDeviation ? 5 : 3];
            Arrays.fill(result, Double.NaN);
        } else {
            // Roi Y minimum, included, inside the image
            int minYroi = usableBounds[0];
            // Roi Y maximum, excluded, inside the image
            int maxYroi = usableBounds[1];
            // Roi X minimum, included, inside the image
            int minXroi = usableBounds[2];
            // Roi X maximum, excluded, inside the image
            int maxXroi = usableBounds[3];
            int height = maxYroi - minYroi;
            int width = maxXroi - minXroi;
            int count = height * width;
            double min = Double.POSITIVE_INFINITY;
            double max = Double.NEGATIVE_INFINITY;
            double mean = 0;
            double squareSum = 0;
            boolean allNaN = true;
            double stdDev = Double.NaN, sampleStdDev = Double.NaN;
            if (imageData instanceof byte[]) {
                byte[] array = (byte[]) imageData;
                for (int y = 0; y < height; y++) {
                    int realY = y + minYroi;
                    for (int x = 0; x < width; x++) {
                        int realX = x + minXroi;
                        double value = array[(realY * dimX) + realX];
                        if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                            squareSum += value * value;
                            allNaN = false;
                            mean += value;
                            if (value < min) {
                                min = value;
                            }
                            if (value > max) {
                                max = value;
                            }
                        }
                    }
                }
            } // end if (imageData instanceof byte[])
            else if (imageData instanceof short[]) {
                short[] array = (short[]) imageData;
                for (int y = 0; y < height; y++) {
                    int realY = y + minYroi;
                    for (int x = 0; x < width; x++) {
                        int realX = x + minXroi;
                        double value = array[(realY * dimX) + realX];
                        if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                            squareSum += value * value;
                            allNaN = false;
                            mean += value;
                            if (value < min) {
                                min = value;
                            }
                            if (value > max) {
                                max = value;
                            }
                        }
                    }
                }
            } // end if (imageData instanceof short[])
            else if (imageData instanceof int[]) {
                int[] array = (int[]) imageData;
                for (int y = 0; y < height; y++) {
                    int realY = y + minYroi;
                    for (int x = 0; x < width; x++) {
                        int realX = x + minXroi;
                        double value = array[(realY * dimX) + realX];
                        if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                            squareSum += value * value;
                            allNaN = false;
                            mean += value;
                            if (value < min) {
                                min = value;
                            }
                            if (value > max) {
                                max = value;
                            }
                        }
                    }
                }
            } // end if (imageData instanceof int[])
            else if (imageData instanceof long[]) {
                long[] array = (long[]) imageData;
                for (int y = 0; y < height; y++) {
                    int realY = y + minYroi;
                    for (int x = 0; x < width; x++) {
                        int realX = x + minXroi;
                        double value = array[(realY * dimX) + realX];
                        if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                            squareSum += value * value;
                            allNaN = false;
                            mean += value;
                            if (value < min) {
                                min = value;
                            }
                            if (value > max) {
                                max = value;
                            }
                        }
                    }
                }
            } // end if (imageData instanceof long[])
            else if (imageData instanceof float[]) {
                float[] array = (float[]) imageData;
                for (int y = 0; y < height; y++) {
                    int realY = y + minYroi;
                    for (int x = 0; x < width; x++) {
                        int realX = x + minXroi;
                        double value = array[(realY * dimX) + realX];
                        if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                            squareSum += value * value;
                            allNaN = false;
                            mean += value;
                            if (value < min) {
                                min = value;
                            }
                            if (value > max) {
                                max = value;
                            }
                        }
                    }
                }
            } // end if (imageData instanceof float[])
            else if (imageData instanceof double[]) {
                double[] array = (double[]) imageData;
                for (int y = 0; y < height; y++) {
                    int realY = y + minYroi;
                    for (int x = 0; x < width; x++) {
                        int realX = x + minXroi;
                        double value = array[(realY * dimX) + realX];
                        if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                            squareSum += value * value;
                            allNaN = false;
                            mean += value;
                            if (value < min) {
                                min = value;
                            }
                            if (value > max) {
                                max = value;
                            }
                        }
                    }
                }
            } // end if (imageData instanceof double[])
            else if (imageData instanceof Number[]) {
                Number[] array = (Number[]) imageData;
                for (int y = 0; y < height; y++) {
                    int realY = y + minYroi;
                    for (int x = 0; x < width; x++) {
                        int realX = x + minXroi;
                        double value = IJMathTool.extractDouble(array[(realY * dimX) + realX]);
                        if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                            squareSum += value * value;
                            allNaN = false;
                            mean += value;
                            if (value < min) {
                                min = value;
                            }
                            if (value > max) {
                                max = value;
                            }
                        }
                    }
                }
            } // end if (imageData instanceof Number[])
            allNaN = allNaN || (count <= 0) || Double.isInfinite(min);
            if (allNaN) {
                result = new double[extractStandardDeviation ? 5 : 3];
                Arrays.fill(result, Double.NaN);
            } else {
                mean /= count;
                if (extractStandardDeviation) {
                    // compute standard deviation, knowing square sum
                    squareSum /= count;
                    stdDev = Math.sqrt(squareSum - (mean * mean));
                    if (count > 1) {
                        sampleStdDev = Math
                                .sqrt((((double) count) / (double) (count - 1)) * (squareSum - (mean * mean)));
                    } else {
                        sampleStdDev = Double.NaN;
                    }
                    result = new double[] { min, max, mean, stdDev, sampleStdDev };
                } else {
                    result = new double[] { min, max, mean };
                }
            }
        }
        return result;
    }

}
