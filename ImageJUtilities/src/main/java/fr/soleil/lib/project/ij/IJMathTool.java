/*******************************************************************************
 * Copyright (c) 2008-2021 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.lib.project.ij;

import java.awt.Rectangle;
import java.util.Collection;

import fr.soleil.lib.project.math.MathConst;
import ij.gui.Roi;

/**
 * Main class for calculations around image data and ImageJ.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class IJMathTool {

    /**
     * Extracts a <code>double</code> value from a {@link Number} value.
     * 
     * @param nb The {@link Number} value. Can be <code>null</code>.
     * @return A <code>double</code>.
     */
    public static double extractDouble(Number nb) {
        return nb == null ? MathConst.NAN_FOR_NULL : nb.doubleValue();
    }

    /**
     * @deprecated You should use {@link MatrixTool#getDataAsDoubleMatrix(Roi, Object, int, int, boolean, Collection)}
     *             instead.
     */
    @Deprecated
    public static double[][] getDataAsDoubleMatrix(Roi roi, Object imageData, int dimX, int dimY, boolean innerRoi,
            Collection<boolean[]> maskValues) {
        return MatrixTool.getDataAsDoubleMatrix(roi, imageData, dimX, dimY, innerRoi, maskValues);
    }

    /**
     * @deprecated You should use {@link MatrixTool#getDataAsDoubleMatrixNoMask(Roi, Object, int, int, boolean)}
     *             instead.
     */
    @Deprecated
    public static double[][] getDataAsDoubleMatrixNoMask(Roi roi, Object imageData, int dimX, int dimY,
            boolean innerRoi) {
        return MatrixTool.getDataAsDoubleMatrixNoMask(roi, imageData, dimX, dimY, innerRoi);
    }

    /**
     * @deprecated You should use {@link RoiTool#extractUsableRoiBounds(Roi, int, int, boolean)} instead.
     */
    @Deprecated
    public static int[] extractUsableRoiBounds(Roi roi, int dimX, int dimY, boolean innerRoi) {
        return RoiTool.extractUsableRoiBounds(roi, dimX, dimY, innerRoi);
    }

    /**
     * @deprecated You should use {@link RoiTool#getRoiBounds(Roi, int, int, boolean)} instead.
     */
    @Deprecated
    public static Rectangle getRoiBounds(Roi roi, int dimX, int dimY, boolean innerRoi) {
        return RoiTool.getRoiBounds(roi, dimX, dimY, innerRoi);
    }

    /**
     * @deprecated You should use {@link RoiTool#extractUsableRoiBounds(int, int, int, int, int, int)} instead.
     */
    @Deprecated
    public static int[] extractUsableRoiBounds(int roiX, int roiY, int roiWidth, int roiHeight, int dimX, int dimY) {
        return RoiTool.extractUsableRoiBounds(roiX, roiY, roiWidth, roiHeight, dimX, dimY);
    }

    /**
     * @deprecated You should use {@link RoiTool#extractUsableRoiBounds(Rectangle, int, int)} instead.
     */
    @Deprecated
    public static int[] extractUsableRoiBounds(Rectangle roiBounds, int dimX, int dimY) {
        return RoiTool.extractUsableRoiBounds(roiBounds, dimX, dimY);
    }

    /**
     * @deprecated You should use
     *             {@link FlatMatrixTool#getDataAsFlatDoubleMatrix(Roi, Object, int, int, boolean, Collection)} instead.
     */
    @Deprecated
    public static double[] getDataAsFlatDoubleMatrix(Roi roi, Object imageData, int dimX, int dimY, boolean innerRoi,
            Collection<boolean[]> maskValues) {
        return FlatMatrixTool.getDataAsFlatDoubleMatrix(roi, imageData, dimX, dimY, innerRoi, maskValues);
    }

    /**
     * @deprecated You hould use {@link FlatMatrixTool#extractSubMatrixMinMaxAndMean(Roi, Object, int, int)} instead.
     */
    @Deprecated
    public static double[] extractSubMatrixMinMaxAndMean(Roi innerRectangleRoi, Object imageData, int dimX, int dimY) {
        return FlatMatrixTool.extractSubMatrixMinMaxAndMean(innerRectangleRoi, imageData, dimX, dimY);
    }

    /**
     * @deprecated You should use {@link FlatMatrixTool#extractSubMatrixMinMaxMeanAndStdDev(Roi, Object, int, int)}
     *             instead.
     */
    @Deprecated
    public static double[] extractSubMatrixMinMaxMeanAndStdDev(Roi innerRectangleRoi, Object imageData, int dimX,
            int dimY) {
        return FlatMatrixTool.extractSubMatrixMinMaxAndMean(innerRectangleRoi, imageData, dimX, dimY);
    }

}
