package fr.soleil.lib.project.ij;

import java.util.Collection;
import java.util.List;

/**
 * Class dedicated in masks calculations.
 * 
 * @author Rapha&euml;l GIRARDOT
 *
 */
public class MaskTool {

    /**
     * Calculates a <code>boolean and</code> operation on a {@link List} of <code>boolean[]</code> at a given
     * coordinate
     * 
     * @param masks The <code>boolean[]</code> {@link List}
     * @param y The y coordinate
     * @param x The x coordinate
     * @param dimX The image height
     * @return A <code>boolean</code> value
     */
    public static boolean andMaskValues(Collection<boolean[]> masks, int y, int x, int dimX) {
        boolean and = true;
        if (masks != null) {
            for (boolean[] mask : masks) {
                and &= mask[y * dimX + x];
            }
        }
        return and;
    }

}
