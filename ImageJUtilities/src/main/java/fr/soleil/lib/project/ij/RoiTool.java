package fr.soleil.lib.project.ij;

import java.awt.Rectangle;

import ij.gui.Roi;

public class RoiTool {

    /**
     * Extracts {@link Roi} bounds
     * 
     * @param roi The {@link Roi}
     * @param dimX The matrix width
     * @param dimY The matrix height
     * @param innerRoi A <code>boolean</code> whether the {@link Roi} is an inner {@link Roi} (i.e. exterior of this
     *            {@link Roi} should be onsidered as masked) or an outer {@link Roi} (i.e. interior of this {@link Roi}
     *            should be considered as masked).
     * @return A {@link Rectangle}
     */
    public static Rectangle getRoiBounds(Roi roi, int dimX, int dimY, boolean innerRoi) {
        Rectangle roiBounds = new Rectangle(0, 0, dimX, dimY);
        if (roi != null) {
            roiBounds = roi.getBounds();
        }
        int roiX = Math.max(roiBounds.x, 0);
        if ((dimX > 0) && (roiX > dimX - 1)) {
            roiX = dimX - 1;
        }
        int roiY = Math.max(roiBounds.y, 0);
        if ((dimY > 0) && (roiY > dimY - 1)) {
            roiY = dimY - 1;
        }
        int roiWidth = Math.min(dimX - roiX, roiBounds.x + roiBounds.width - roiX);
        int roiHeight = Math.min(dimY - roiY, roiBounds.y + roiBounds.height - roiY);
        roiBounds.x = roiX;
        roiBounds.y = roiY;
        roiBounds.width = roiWidth;
        roiBounds.height = roiHeight;

        if (innerRoi) {
            if ((roiBounds.width <= 0) || (roiBounds.height <= 0)) {
                roiBounds = null;
            }
        } else {
            if ((dimX <= 0) || (dimY <= 0)) {
                roiBounds = null;
            }
        }
        return roiBounds;
    }

    /**
     * Calculates <code>{minYroi, maxYroi, minXroi, maxXroi}</code> from previously calculated {@link Roi} bounds where:
     * <ul>
     * <li><code>minYroi</code> is Roi Y minimum, included, inside the image</li>
     * <li><code>maxYroi</code> is Roi Y maximum, excluded, inside the image</li>
     * <li><code>minXroi</code> is Roi X minimum, included, inside the image</li>
     * <li><code>maxXroi</code> is Roi X maximum, excluded, inside the image</li>
     * </ul>
     * 
     * @param roiX The previously calculated roi X
     * @param roiY The previously calculated roi Y
     * @param roiWidth The previously calculated roi width
     * @param roiHeight The previously calculated roi height
     * @param dimX The matrix width
     * @param dimY The matrix height
     * @return An <code>int[]</code>. <code>null</code> if <code>dimX</code> or <code>dimY</code> or
     *         <code>roiWidth</code> or <code>roiHeight</code> is <code>&lt; 0</code>
     */
    public static int[] extractUsableRoiBounds(int roiX, int roiY, int roiWidth, int roiHeight, int dimX, int dimY) {
        int[] result;
        if ((dimX < 0) || (dimY < 0) || (roiWidth < 0) || (roiHeight < 0)) {
            result = null;
        } else {
            // Roi Y minimum, included, inside the image
            int minYroi = Math.min(Math.max(roiY, 0), dimY);
            // Roi Y maximum, excluded, inside the image
            int maxYroi = Math.min(roiY + roiHeight, dimY);
            // Roi X minimum, included, inside the image
            int minXroi = Math.min(Math.max(roiX, 0), dimX);
            // Roi X maximum, excluded, inside the image
            int maxXroi = Math.min(roiX + roiWidth, dimX);
            result = new int[] { minYroi, maxYroi, minXroi, maxXroi };
        }
        return result;
    }

    /**
     * Calculates <code>{minYroi, maxYroi, minXroi, maxXroi}</code> from previously calculated {@link Roi} bounds where:
     * <ul>
     * <li><code>minYroi</code> is Roi Y minimum, included, inside the image</li>
     * <li><code>maxYroi</code> is Roi Y maximum, excluded, inside the image</li>
     * <li><code>minXroi</code> is Roi X minimum, included, inside the image</li>
     * <li><code>maxXroi</code> is Roi X maximum, excluded, inside the image</li>
     * </ul>
     * 
     * @param roiBounds The {@link Roi} bounds, obtained through {@link #getRoiBounds(Roi, int, int, boolean)}
     * @param dimX The matrix width
     * @param dimY The matrix height
     * @return An <code>int[]</code>. <code>null</code> if <code>roiBounds</code> is <code>null</code>, or if
     *         <code>dimX</code> or <code>dimY</code> is <code>&lt; 0</code>
     */
    public static int[] extractUsableRoiBounds(Rectangle roiBounds, int dimX, int dimY) {
        int[] result;
        if (roiBounds == null) {
            result = null;
        } else {
            result = extractUsableRoiBounds(roiBounds.x, roiBounds.y, roiBounds.width, roiBounds.height, dimX, dimY);
        }
        return result;
    }

    /**
     * Calculates <code>{minYroi, maxYroi, minXroi, maxXroi}</code> from {@link Roi} where:
     * <ul>
     * <li><code>minYroi</code> is Roi Y minimum, included, inside the image</li>
     * <li><code>maxYroi</code> is Roi Y maximum, excluded, inside the image</li>
     * <li><code>minXroi</code> is Roi X minimum, included, inside the image</li>
     * <li><code>maxXroi</code> is Roi X maximum, excluded, inside the image</li>
     * </ul>
     * 
     * @param roi The {@link Roi}
     * @param dimX The image width
     * @param dimY The image height
     * @param innerRoi A <code>boolean</code> whether the {@link Roi} is an inner {@link Roi} (i.e. exterior of this
     *            {@link Roi} should be considered as masked) or an outer {@link Roi} (i.e. interior of this {@link Roi}
     *            should be considered as masked). <code>TRUE</code> for an inner {@link Roi}
     * @return An <code>int[]</code>. <code>null</code> if <code>dimX</code> or <code>dimY</code> is <code>&lt; 0</code>
     */
    public static int[] extractUsableRoiBounds(Roi roi, int dimX, int dimY, boolean innerRoi) {
        Rectangle roiBounds = getRoiBounds(roi, dimX, dimY, innerRoi);
        return extractUsableRoiBounds(roiBounds, dimX, dimY);
    }

    /**
     * Returns whether a given coordinate is inside of a {@link Roi}
     * 
     * @param roi The {@link Roi}
     * @param y The y coordinate
     * @param x The x coordinate
     * @return A <code>boolean</code> value
     */
    public static boolean isInsideRoi(Roi roi, int y, int x) {
        return ((roi == null) || roi.contains(x, y));
    }

    /**
     * Returns whether a given coordinate is outside of a given {@link Roi}
     * 
     * @param roi The {@link Roi}
     * @param y The y coordinate
     * @param x The x coordinate
     * @return A <code>boolean</code> value
     */
    public static boolean isOutsideRoi(Roi roi, int y, int x) {
        return ((roi == null) || (!roi.contains(x, y)));
    }

}
