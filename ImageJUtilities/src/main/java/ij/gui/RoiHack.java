package ij.gui;

import java.awt.Shape;

import fr.soleil.lib.project.ObjectUtils;

/**
 * Class created in {@link Roi} package to ensure being able to fully interact with {@link Roi}.
 * <p>
 * This class must stay in <code>ij.gui</code> package to ensure accessing protected or package friendly fields and
 * methods.
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class RoiHack {

    private RoiHack() {
        // hide constructor
    }

    /**
     * Changes some {@link Roi}'s dimensions.
     * 
     * @param roi The {@link Roi}.
     * @param width The width to set.
     * @param height The height to set.
     * @return A <code>boolean</code>: whether dimensions were successfully updated.
     */
    public static boolean resizeRoi(Roi roi, int width, int height) {
        boolean updated = false;
        if ((roi != null) && (roi.getType() == Roi.RECTANGLE) && (width > 0) && (height > 0)
                && ((width != roi.width) || (height != roi.height))) {
            roi.width = width;
            roi.height = height;
            roi.updateClipRect();
            roi.imp.draw(roi.clipX, roi.clipY, roi.clipWidth, roi.clipHeight);
            roi.oldWidth = roi.width;
            roi.oldHeight = roi.height;
            updated = true;
        }
        return updated;
    }

    /**
     * Changes the {@link Shape} of a {@link ShapeRoi}.
     * 
     * @param shapeRoi The {@link ShapeRoi}.
     * @param newShape The {@link Shape}.
     * @param keepType Whether to keep roi type as it was before xhanging shape.
     * @return A <code>boolean</code>: whether shape was successfully changed.
     */
    public static boolean reshapeRoi(ShapeRoi shapeRoi, Shape newShape, boolean keepType) {
        boolean updated = false;
        if ((shapeRoi != null) && (newShape != null)) {
            int type = shapeRoi.getType();
            shapeRoi.setShape(newShape);
            if (keepType) {
                shapeRoi.type = type;
            }
            updated = true;
        }
        return updated;
    }

    /**
     * Changes the coordinates of a {@link Line}.
     * 
     * @param roi The {@link Line}.
     * @param x1 The x1 coordinate.
     * @param y1 The y1 coordinate.
     * @param x2 The x2 coordinate.
     * @param y2 The y2 coordinate.
     * @return A <code>boolean</code>: whether coordinates were successfully updated.
     */
    public static boolean setCoordinates(Line roi, double x1, double y1, double x2, double y2) {
        boolean updated = false;
        if ((roi != null) && ((roi.x1d != x1) || (roi.y1d != y1) || (roi.x2d != x2) || (roi.y2d != y2))) {
            // based on Line(double ox1, double oy1, double ox2, double oy2)
            roi.x2d = x2;
            roi.y2d = y2;
            roi.x2 = (int) x2;
            roi.y2 = (int) y2;
            roi.x = (int) Math.min(roi.x1d, roi.x2d);
            roi.y = (int) Math.min(roi.y1d, roi.y2d);
            roi.x1R = roi.x1d - roi.x;
            roi.y1R = roi.y1d - roi.y;
            roi.x2R = roi.x2d - roi.x;
            roi.y2R = roi.y2d - roi.y;
            roi.width = (int) Math.abs(roi.x2R - roi.x1R);
            roi.height = (int) Math.abs(roi.y2R - roi.y1R);
            if (!(roi instanceof Arrow) && Line.lineWidth > 1) {
                roi.updateWideLine(Line.lineWidth);
            }
            roi.updateClipRect();
            roi.oldX = roi.x;
            roi.oldY = roi.y;
            roi.oldWidth = roi.width;
            roi.oldHeight = roi.height;
            updated = true;
        }
        return updated;
    }

    /**
     * Sets the location (x and y coordinates) of a {@link Line}.
     * 
     * @param roi The {@link Line}.
     * @param x The x coordinate.
     * @param y The y coordinate.
     * @return A <code>boolean</code>: whether location was successfully updated.
     */
    public static boolean setLocation(Line roi, int x, int y) {
        boolean updated = false;
        if ((roi.x != x) || (roi.y != y)) {
            // based on Line.move(int sx, int sy)
            double xNew = x + roi.startxd - roi.x;
            double yNew = y + roi.startyd - roi.y;
            roi.x += xNew - roi.startxd;
            roi.y += yNew - roi.startyd;
            roi.clipboard = null;
            roi.startxd = xNew;
            roi.startyd = yNew;
            roi.updateClipRect();
            roi.imp.draw(roi.clipX, roi.clipY, roi.clipWidth, roi.clipHeight);
            roi.oldX = roi.x;
            roi.oldY = roi.y;
            roi.oldWidth = roi.width;
            roi.oldHeight = roi.height;
            updated = true;
        }
        return updated;
    }

    /**
     * Changes the length of a {@link Line}, keeping its x coordinate as is.
     * 
     * @param roi The {@link Line}.
     * @param length The desired length.
     * @param keepBothCoordinates Whether to keep {@link Line}'s both x and y coordinates.
     *            <ul>
     *            <li>If <code>true</code>, this method will ensure the {@link Line} will be at the same x and y
     *            coordinates that before the length change (with rounding error margin).</li>
     *            <li>If <code>false</code>, this method will ensure the {@link Line} starts at the same left (x
     *            minimum) point, changing its length by following the same direction to the right.</li>
     *            </ul>
     *            In both cases, this method will ensure (x1,y1) point is the one on the left.
     * @return A <code>boolean</code>: whether length was successfully updated.
     */
    public static boolean setLength(Line roi, double length, boolean keepBothCoordinates) {
        boolean updated = false;
        if ((roi != null) && (!Double.isNaN(length)) && (length > 0) && (!Double.isInfinite(length))
                && (length != roi.getLength())) {
            // first step: update start points
            if (roi.x2d < roi.x1d) {
                double tmp = roi.x1d;
                roi.x1d = roi.x2d;
                roi.x2d = tmp;
                tmp = roi.y1d;
                roi.y1d = roi.y2d;
                roi.y2d = tmp;
                roi.x1 = (int) roi.x1d;
                roi.x2 = (int) roi.x2d;
                roi.y1 = (int) roi.y1d;
                roi.y2 = (int) roi.y2d;
            }
            // second step: update length
            double x1 = roi.x1d, y1 = roi.y1d;
            double x2o = roi.x2d, y2o = roi.y2d, x2, y2;
            if (y1 == y2o) {
                x2 = x1 + length;
                y2 = y1;
            } else if (x1 == x2o) {
                x2 = x1;
                y2 = y1 + length;
            } else {
                double d = (y2o - y1) / (x2o - x1);
                double a = ObjectUtils.square(d) + 1;
                double b = -2 * a * x1;
                double c = a * ObjectUtils.square(x1) - ObjectUtils.square(length);
                double delta = ObjectUtils.square(b) - 4 * a * c;
                if (delta < 0) {
                    // No real solution: keep previous length
                    x2 = x2o;
                    y2 = y2o;
                } else {
                    if (delta == 0) {
                        x2 = -b / (2 * a);
                    } else {
                        // 2 solutions: keep the one where x2 > x1
                        double s1 = (-b + Math.sqrt(delta)) / (2 * a);
                        double s2 = (-b - Math.sqrt(delta)) / (2 * a);
                        if (s1 > x1) {
                            x2 = s1;
                        } else {
                            x2 = s2;
                        }
                    }
                    y2 = d * (x2 - x1) + y1;
                }
            } // end if (y1 == y2o) ... else if (x1 == x2o) ... else
            if ((x2 != x2o) || (y2 != y2o)) {
                int x = roi.x, y = roi.y;
                setCoordinates(roi, x1, y1, x2, y2);
                // third step: ensure coordinates
                if (keepBothCoordinates) {
                    // max 5 attempts to keep coordinates
                    for (int i = 0; i < 5 && setLocation(roi, x, y); i++) {
                        // nothing to do: everything is done in setLocation method
                    }
                }
                updated = true;
            }
        } // end if ((roi != null) && (length > 0) && (!Double.isNaN(length)) && (!Double.isInfinite(length))
          // && (length != roi.getLength()))
        return updated;
    }

}
