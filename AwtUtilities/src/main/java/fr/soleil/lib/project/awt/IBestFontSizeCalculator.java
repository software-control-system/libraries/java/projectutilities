/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt;

import java.awt.Component;
import java.awt.Font;

/**
 * An interface for a {@link Component} able to compute its best {@link Font} size size according to current dimensions
 * and
 * {@link Font}.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IBestFontSizeCalculator {

    /**
     * Asks this {@link IBestFontSizeCalculator} to compute best {@link Font} size according to current dimensions and
     * {@link Font}.
     * 
     * @return An <code>int</code>.
     */
    public int computeBestFontSize();

}
