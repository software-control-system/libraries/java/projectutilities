/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt.layout;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import fr.soleil.lib.project.ObjectUtils;

/**
 * This class is the same as {@link java.awt.FlowLayout}, except that it is able to manage preferred
 * size and minimum size heights. Its code is mainly a copy of {@link java.awt.FlowLayout}, with
 * some better height management in {@link #minimumLayoutSize(Container)} and {@link #preferredLayoutSize(Container)}
 * methods
 */
public class VerticalFlowLayout implements LayoutManager, Serializable {

    private static final long serialVersionUID = -3968192971553669283L;

    /**
     * This value indicates that each row of components should be left-justified.
     */
    public static final int LEFT = 0;

    /**
     * This value indicates that each row of components should be centered.
     */
    public static final int CENTER = 1;

    /**
     * This value indicates that each row of components should be right-justified.
     */
    public static final int RIGHT = 2;

    /**
     * This value indicates that each row of components should be justified to the leading edge of
     * the container's orientation, for example, to the left in left-to-right orientations.
     * 
     * @see java.awt.Component#getComponentOrientation()
     * @see java.awt.ComponentOrientation
     */
    public static final int LEADING = 3;

    /**
     * This value indicates that each row of components should be justified to the trailing edge of
     * the container's orientation, for example, to the right in left-to-right orientations.
     * 
     * @see java.awt.Component#getComponentOrientation()
     * @see java.awt.ComponentOrientation
     */
    public static final int TRAILING = 4;

    /**
     * <code>align</code> is the property that determines how each row distributes empty space. It
     * can be one of the following values:
     * <ul>
     * <code>LEFT</code> <code>RIGHT</code> <code>CENTER</code> <code>LEADING</code> <code>TRAILING</code>
     * </ul>
     * 
     * @serial
     * @see #getAlignment()
     * @see #setAlignment(int)
     */
    int align; // This is for 1.1 serialization compatibility

    /**
     * <code>newAlign</code> is the property that determines how each row distributes empty space
     * for the Java 2 platform, v1.2 and greater. It can be one of the following three values:
     * <ul>
     * <code>LEFT</code> <code>RIGHT</code> <code>CENTER</code> <code>LEADING</code> <code>TRAILING</code>
     * </ul>
     * 
     * @serial
     * @see #getAlignment()
     * @see #setAlignment(int)
     */
    int newAlign; // This is the one we actually use

    /**
     * The flow layout manager allows a seperation of components with gaps. The horizontal gap will
     * specify the space between components and between the components and the borders of the <code>Container</code>.
     * 
     * @serial
     * @see #getHgap()
     * @see #setHgap(int)
     */
    int hgap;

    /**
     * The flow layout manager allows a seperation of components with gaps. The vertical gap will
     * specify the space between rows and between the the rows and the borders of the <code>Container</code>.
     * 
     * @serial
     * @see #getHgap()
     * @see #setHgap(int)
     */
    int vgap;

    /**
     * If true, components will be aligned on their baseline.
     */
    private boolean alignOnBaseline;

    /**
     * Constructs a new <code>VerticalFlowLayout</code> with a centered alignment and a default
     * 5-unit horizontal and vertical gap.
     */
    public VerticalFlowLayout() {
        this(CENTER, 5, 5);
    }

    /**
     * Constructs a new <code>VerticalFlowLayout</code> with the specified alignment and a default
     * 5-unit horizontal and vertical gap. The value of the alignment argument must be one of
     * <code>VerticalFlowLayout.LEFT</code>, <code>VerticalFlowLayout.RIGHT</code>,
     * <code>VerticalFlowLayout.CENTER</code>, <code>VerticalFlowLayout.LEADING</code>, or
     * <code>VerticalFlowLayout.TRAILING</code>.
     * 
     * @param align the alignment value
     */
    public VerticalFlowLayout(int align) {
        this(align, 5, 5);
    }

    /**
     * Creates a new flow layout manager with the indicated alignment and the indicated horizontal
     * and vertical gaps.
     * <p>
     * The value of the alignment argument must be one of <code>VerticalFlowLayout.LEFT</code>,
     * <code>VerticalFlowLayout.RIGHT</code>, <code>VerticalFlowLayout.CENTER</code>,
     * <code>VerticalFlowLayout.LEADING</code>, or <code>VerticalFlowLayout.TRAILING</code>.
     * 
     * @param align the alignment value
     * @param hgap the horizontal gap between components and between the components and the borders
     *            of the <code>Container</code>
     * @param vgap the vertical gap between components and between the components and the borders of
     *            the <code>Container</code>
     */
    public VerticalFlowLayout(int align, int hgap, int vgap) {
        this.hgap = hgap;
        this.vgap = vgap;
        setAlignment(align);
    }

    /**
     * Gets the alignment for this layout. Possible values are <code>VerticalFlowLayout.LEFT</code>,
     * <code>VerticalFlowLayout.RIGHT</code>, <code>VerticalFlowLayout.CENTER</code>,
     * <code>VerticalFlowLayout.LEADING</code>, or <code>VerticalFlowLayout.TRAILING</code>.
     * 
     * @return the alignment value for this layout
     * @see #setAlignment(int)
     */
    public int getAlignment() {
        return newAlign;
    }

    /**
     * Sets the alignment for this layout. Possible values are
     * <ul>
     * <li><code>VerticalFlowLayout.LEFT</code>
     * <li><code>VerticalFlowLayout.RIGHT</code>
     * <li><code>VerticalFlowLayout.CENTER</code>
     * <li><code>VerticalFlowLayout.LEADING</code>
     * <li><code>VerticalFlowLayout.TRAILING</code>
     * </ul>
     * 
     * @param align one of the alignment values shown above
     * @see #getAlignment()
     */
    public void setAlignment(int align) {
        this.newAlign = align;

        // this.align is used only for serialization compatibility,
        // so set it to a value compatible with the 1.1 version
        // of the class

        switch (align) {
            case LEADING:
                this.align = LEFT;
                break;
            case TRAILING:
                this.align = RIGHT;
                break;
            default:
                this.align = align;
                break;
        }
    }

    /**
     * Gets the horizontal gap between components and between the components and the borders of the
     * <code>Container</code>
     * 
     * @return the horizontal gap between components and between the components and the borders of
     *         the <code>Container</code>
     * @see #setHgap(int)
     */
    public int getHgap() {
        return hgap;
    }

    /**
     * Sets the horizontal gap between components and between the components and the borders of the
     * <code>Container</code>.
     * 
     * @param hgap the horizontal gap between components and between the components and the borders
     *            of the <code>Container</code>
     * @see #getHgap()
     */
    public void setHgap(int hgap) {
        this.hgap = hgap;
    }

    /**
     * Gets the vertical gap between components and between the components and the borders of the <code>Container</code>
     * .
     * 
     * @return the vertical gap between components and between the components and the borders of the
     *         <code>Container</code>
     * @see #setVgap(int)
     */
    public int getVgap() {
        return vgap;
    }

    /**
     * Sets the vertical gap between components and between the components and the borders of the <code>Container</code>
     * .
     * 
     * @param vgap the vertical gap between components and between the components and the borders of
     *            the <code>Container</code>
     * @see #getVgap()
     */
    public void setVgap(int vgap) {
        this.vgap = vgap;
    }

    /**
     * Sets whether or not components should be vertically aligned along their baseline. Components
     * that do not have a baseline will be centered. The default is false.
     * 
     * @param alignOnBaseline whether or not components should be vertically aligned on their
     *            baseline
     */
    public void setAlignOnBaseline(boolean alignOnBaseline) {
        this.alignOnBaseline = alignOnBaseline;
    }

    /**
     * Returns true if components are to be vertically aligned along their baseline. The default is
     * false.
     * 
     * @return true if components are to be vertically aligned along their baseline
     */
    public boolean getAlignOnBaseline() {
        return alignOnBaseline;
    }

    /**
     * Adds the specified component to the layout. Not used by this class.
     * 
     * @param name the name of the component
     * @param comp the component to be added
     */
    @Override
    public void addLayoutComponent(String name, Component comp) {
    }

    /**
     * Removes the specified component from the layout. Not used by this class.
     * 
     * @param comp the component to remove
     * @see java.awt.Container#removeAll()
     */
    @Override
    public void removeLayoutComponent(Component comp) {
    }

    /**
     * Returns the preferred dimensions for this layout given the <i>visible</i> components in the
     * specified target container.
     * 
     * @param target the container that needs to be laid out
     * @return the preferred dimensions to lay out the subcomponents of the specified container
     * @see Container
     * @see #minimumLayoutSize(Container)
     * @see java.awt.Container#getPreferredSize()
     */
    @Override
    public Dimension preferredLayoutSize(Container target) {
        return layoutSize(target, false);
    }

    /**
     * Returns the minimum dimensions needed to layout the <i>visible</i> components contained in
     * the specified target container.
     * 
     * @param target the container that needs to be laid out
     * @return the minimum dimensions to lay out the subcomponents of the specified container
     * @see #preferredLayoutSize(Container)
     * @see java.awt.Container
     * @see java.awt.Container#doLayout()
     */
    @Override
    public Dimension minimumLayoutSize(Container target) {
        return layoutSize(target, true);
    }

    protected Dimension layoutSize(Container target, boolean minimum) {
        Dimension dim = new Dimension(0, 0);
        final Container parent = target.getParent();
        int width = (parent == null ? Integer.MAX_VALUE : parent.getSize().width);
        synchronized (target.getTreeLock()) {
            int nmembers = target.getComponentCount();
            int maxAscent = 0;
            int maxDescent = 0;
            boolean useBaseline = getAlignOnBaseline();
            boolean firstVisibleComponent = true;
            int height = 0;
            Insets insets = target.getInsets();
            int horizontalGap = insets.left + insets.right + hgap * 2;
            int verticalGap = insets.top + insets.bottom + vgap * 2;
            for (int i = 0; i < nmembers; i++) {
                Component m = target.getComponent(i);
                if (m.isVisible()) {
                    Dimension d = minimum ? m.getMinimumSize() : m.getPreferredSize();
                    height = Math.max(height, m.getLocation().y + d.height);
                    dim.height = Math.max(dim.height, d.height);
                    dim.height = Math.max(height, dim.height);
                    if (firstVisibleComponent) {
                        firstVisibleComponent = false;
                    } else {
                        dim.width += hgap;
                    }
                    dim.width += d.width;
                    if (useBaseline) {
                        int baseline = m.getBaseline(d.width, d.height);
                        if (baseline >= 0) {
                            maxAscent = Math.max(maxAscent, baseline);
                            maxDescent = Math.max(maxDescent, d.height - baseline);
                        }
                    }
                }
            }
            if (useBaseline) {
                dim.height = Math.max(maxAscent + maxDescent, dim.height);
            }
            dim.width += horizontalGap;
            dim.width = Math.min(dim.width, width);
            dim.height += verticalGap;
        }
        return dim;
    }

    /**
     * Centers the elements in the specified row, if there is any slack.
     * 
     * @param target the component which needs to be moved
     * @param x the x coordinate
     * @param y the y coordinate
     * @param width the width dimensions
     * @param height the height dimensions
     * @param rowStart the beginning of the row
     * @param rowEnd the the ending of the row
     * @param useBaseline Whether or not to align on baseline.
     * @param ascent Ascent for the components. This is only valid if useBaseline is true.
     * @param descent Ascent for the components. This is only valid if useBaseline is true.
     * @return actual row height
     */
    private int moveComponents(Container target, int x, int y, int width, int height, int rowStart, int rowEnd,
            boolean ltr, boolean useBaseline, int[] ascent, int[] descent) {
        switch (newAlign) {
            case LEFT:
                x += ltr ? 0 : width;
                break;
            case CENTER:
                x += width / 2;
                break;
            case RIGHT:
                x += ltr ? width : 0;
                break;
            case LEADING:
                break;
            case TRAILING:
                x += width;
                break;
        }
        int maxAscent = 0;
        int nonbaselineHeight = 0;
        int baselineOffset = 0;
        if (useBaseline) {
            int maxDescent = 0;
            for (int i = rowStart; i < rowEnd; i++) {
                Component m = target.getComponent(i);
                if (m.isVisible()) {
                    if (ascent[i] >= 0) {
                        maxAscent = Math.max(maxAscent, ascent[i]);
                        maxDescent = Math.max(maxDescent, descent[i]);
                    } else {
                        nonbaselineHeight = Math.max(m.getHeight(), nonbaselineHeight);
                    }
                }
            }
            height = Math.max(maxAscent + maxDescent, nonbaselineHeight);
            baselineOffset = (height - maxAscent - maxDescent) / 2;
        }
        for (int i = rowStart; i < rowEnd; i++) {
            Component m = target.getComponent(i);
            if (m.isVisible()) {
                int cy;
                if (useBaseline && ascent[i] >= 0) {
                    cy = y + baselineOffset + maxAscent - ascent[i];
                } else {
                    cy = y + (height - m.getHeight()) / 2;
                }
                if (ltr) {
                    m.setLocation(x, cy);
                } else {
                    m.setLocation(target.getWidth() - x - m.getWidth(), cy);
                }
                x += m.getWidth() + hgap;
            }
        }
        return height;
    }

    /**
     * Lays out the container. This method lets each <i>visible</i> component take its preferred
     * size by reshaping the components in the target container in order to satisfy the alignment of
     * this <code>VerticalFlowLayout</code> object.
     * 
     * @param target the specified component being laid out
     * @see Container
     * @see java.awt.Container#doLayout()
     */
    @Override
    public void layoutContainer(Container target) {
        synchronized (target.getTreeLock()) {
            Insets insets = target.getInsets();
            int maxwidth = target.getWidth() - (insets.left + insets.right + hgap * 2);
            int nmembers = target.getComponentCount();
            int x = 0, y = insets.top + vgap;
            int rowh = 0, start = 0;

            boolean ltr = target.getComponentOrientation().isLeftToRight();

            boolean useBaseline = getAlignOnBaseline();
            int[] ascent = null;
            int[] descent = null;

            if (useBaseline) {
                ascent = new int[nmembers];
                descent = new int[nmembers];
            }

            for (int i = 0; i < nmembers; i++) {
                Component m = target.getComponent(i);
                if (m.isVisible()) {
                    Dimension d = m.getPreferredSize();
                    m.setSize(d.width, d.height);

                    if (useBaseline) {
                        int baseline = m.getBaseline(d.width, d.height);
                        if (baseline >= 0) {
                            ascent[i] = baseline;
                            descent[i] = d.height - baseline;
                        } else {
                            ascent[i] = -1;
                        }
                    }
                    if ((x == 0) || ((x + d.width) <= maxwidth)) {
                        if (x > 0) {
                            x += hgap;
                        }
                        x += d.width;
                        rowh = Math.max(rowh, d.height);
                    } else {
                        rowh = moveComponents(target, insets.left + hgap, y, maxwidth - x, rowh, start, i, ltr,
                                useBaseline, ascent, descent);
                        x = d.width;
                        y += vgap + rowh;
                        rowh = d.height;
                        start = i;
                    }
                }
            }
            moveComponents(target, insets.left + hgap, y, maxwidth - x, rowh, start, nmembers, ltr, useBaseline, ascent,
                    descent);
        }
    }

    //
    // the internal serial version which says which version was written
    // - 0 (default) for versions before the Java 2 platform, v1.2
    // - 1 for version >= Java 2 platform v1.2, which includes "newAlign" field
    //
    private static final int currentSerialVersion = 1;
    /**
     * This represent the <code>currentSerialVersion</code> which is bein used. It will be one of
     * two values : <code>0</code> versions before Java 2 platform v1.2.. <code>1</code> versions
     * after Java 2 platform v1.2..
     * 
     * @serial
     */
    private int serialVersionOnStream = currentSerialVersion;

    /**
     * Reads this object out of a serialization stream, handling objects written by older versions
     * of the class that didn't contain all of the fields we use now..
     */
    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        stream.defaultReadObject();

        if (serialVersionOnStream < 1) {
            // "newAlign" field wasn't present, so use the old "align" field.
            setAlignment(this.align);
        }
        serialVersionOnStream = currentSerialVersion;
    }

    /**
     * Returns a string representation of this <code>VerticalFlowLayout</code> object and its
     * values.
     * 
     * @return a string representation of this layout
     */
    @Override
    public String toString() {
        String str = ObjectUtils.EMPTY_STRING;
        switch (align) {
            case LEFT:
                str = ",align=left";
                break;
            case CENTER:
                str = ",align=center";
                break;
            case RIGHT:
                str = ",align=right";
                break;
            case LEADING:
                str = ",align=leading";
                break;
            case TRAILING:
                str = ",align=trailing";
                break;
        }
        return getClass().getName() + "[hgap=" + hgap + ",vgap=" + vgap + str + "]";
    }

}
