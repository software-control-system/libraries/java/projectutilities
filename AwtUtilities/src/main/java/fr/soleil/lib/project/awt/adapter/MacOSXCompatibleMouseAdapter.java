/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt.adapter;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A {@link MouseAdapter} that also is a {@link KeyListener}, and can guess on Mac OS X whether [Cmd] key was pressed
 * during an {@link InputEvent}.
 * 
 * @author Rapha&euml;l GIRARDOT
 * @see #isAdaptedControlDown(InputEvent)
 */
public class MacOSXCompatibleMouseAdapter extends MouseAdapter implements KeyListener {

    public static final boolean IS_MAC_OS_X;
    static {
        String os = System.getProperty("os.name");
        if (os == null) {
            IS_MAC_OS_X = false;
        } else {
            IS_MAC_OS_X = os.toLowerCase().contains("mac os x");
        }
    }

    protected final Map<Object, Boolean> cmdMap;

    public MacOSXCompatibleMouseAdapter() {
        super();
        cmdMap = new ConcurrentHashMap<>();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // nothing to do by default
    }

    /**
     * Returns whether a {@link KeyEvent} is about [Cmd] key
     * 
     * @param e The {@link KeyEvent}
     * @return A <code>boolean</code>
     */
    protected final boolean isMacCmd(KeyEvent e) {
        return IS_MAC_OS_X && (e != null) && (e.getSource() != null) && (e.getKeyCode() == KeyEvent.VK_WINDOWS);
    }

    @Override
    public final void keyPressed(KeyEvent e) {
        if (isMacCmd(e)) {
            cmdMap.put(e.getSource(), Boolean.TRUE);
        }
        keyPressedOnceChecked(e);
    }

    protected void keyPressedOnceChecked(KeyEvent e) {
        // Method to override in order to manage key pressed
    }

    @Override
    public final void keyReleased(KeyEvent e) {
        if (isMacCmd(e)) {
            cmdMap.remove(e.getSource());
        }
        keyReleasedOnceChecked(e);
    }

    protected void keyReleasedOnceChecked(KeyEvent e) {
        // Method to override in order to manage key released
    }

    /**
     * Returns whether [Ctrl] is pressed during an {@link InputEvent}, or (inclusive), on Mac OS X, whether [Cmd] key is
     * pressed.
     * <p>
     * This method will be effective only if this {@link MacOSXCompatibleMouseAdapter} was registered as a
     * {@link KeyListener}
     * of parent component.
     * </p>
     * 
     * @param e The {@link InputEvent}
     * @return A <code>boolean</code>
     */
    public boolean isAdaptedControlDown(InputEvent e) {
        boolean controlDown;
        if (e == null) {
            controlDown = false;
        } else if (e.isControlDown()) {
            controlDown = true;
        } else if (IS_MAC_OS_X) {
            Boolean cmdDown = cmdMap.get(e.getSource());
            controlDown = (cmdDown != null) && cmdDown.booleanValue();
        } else {
            controlDown = false;
        }
        return controlDown;
    }
}
