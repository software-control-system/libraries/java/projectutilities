/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt.listener;

import java.awt.Component;
import java.util.EventListener;

/**
 * An interface for something that can listen to many aspects of a {@link Component}.
 * <p>
 * Generally, when you create such a listener, you will call {@link #setupAndListenToComponent(Component)}.
 * When you don't need that listener anymore, you will call {@link #stopListeningToComponent(Component)}.
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 *
 */
public interface IComponentMultiListener extends EventListener {

    /**
     * This method does all necessary things to listen correctly to given {@link Component} and to do any necessary
     * setup action to that {@link Component}.
     * 
     * @param component The {@link Component}.
     */
    public void setupAndListenToComponent(Component component);

    /**
     * This method does all necessary things to stop listening to a {@link Component}.
     * 
     * @param component The {@link Component}.
     */
    public void stopListeningToComponent(Component component);

}
