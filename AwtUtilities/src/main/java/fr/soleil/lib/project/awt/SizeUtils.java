/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

public interface SizeUtils {

    /**
     * This methods returns an adapted preferred size for components that are not always able to compute their best
     * preferred size, by giving some more margin in width (useful for SCAN-830).
     * 
     * @param comp The component for which to adapt preferred size.
     * @param originalPreferredSize The preferred size of the component, returned by
     *            <code>super.getPreferredSize()</code>.
     * @return The adapted preferred size.
     */
    public static Dimension getAdaptedPreferredSize(final Component comp, final Dimension originalPreferredSize) {
        Dimension size = originalPreferredSize;
        if ((comp != null) && (size != null) && (!comp.isPreferredSizeSet())) {
            Font font = comp.getFont();
            size.width += font == null ? 10 : (int) Math.pow(10, Math.floor(Math.log10(Math.abs(font.getSize()))));
        }
        return size;
    }

}
