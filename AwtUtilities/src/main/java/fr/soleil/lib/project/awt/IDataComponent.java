/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt;

/**
 * An interface for a component that may receive some data and may update itself according to that data.
 * <p>
 * Typically, that interface might be used if you want your component to update only when it showing (in order to limit
 * resources consumption).
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 *
 */
public interface IDataComponent {

    /**
     * Returns whether the {@link IDataComponent} received some data and did not yet update according to it.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean dataChanged();

    /**
     * Asks this {@link IDataComponent} to update itself according to last received data.
     */
    public void updateFromData();

}
