/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt.layout;

import java.awt.GridLayout;

/**
 * An interface that proposes some useful static methods to interact with layouts.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface LayoutUtils {

    /**
     * Generates the best {@link GridLayout} according to given number of components.
     * 
     * @param count The number of components.
     * @param hgap The desired horizontal gap.
     * @param vgap The desired vertical gap.
     * @return A {@link GridLayout}. Never <code>null</code>.
     */
    public static GridLayout generateBestGridLayout(int count, int hgap, int vgap) {
        GridLayout layout;
        if (count > 0) {
            double cols = Math.ceil(Math.sqrt(count));
            int rows = (int) Math.ceil(count / cols);
            layout = new GridLayout(rows, (int) cols, hgap, vgap);
        } else {
            layout = new GridLayout(1, 1, hgap, vgap);
        }
        return layout;
    }

    /**
     * Generates the best {@link GridLayout} according to given number of components.
     * 
     * @param count The number of components.
     * @return A {@link GridLayout}. Never <code>null</code>.
     */
    public static GridLayout generateBestGridLayout(int count) {
        return generateBestGridLayout(count, 5, 5);
    }

}
