/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import fr.soleil.lib.project.file.FileUtils;

/**
 * A class that uses {@link Desktop} to provide some useful methods for project management
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DesktopUtils {

    /**
     * Creates a copy of a {@link File} available in project resources and uses {@link Desktop} to open this copy (the
     * copy will be deleted when the virtual machine terminates)
     * 
     * @param packageName The package in which the expected file should be found, separated by '/'
     * @param fileName The expected file name. If too short, the generated file name will start with "tmp" then the file
     *            name (to avoid {@link java.lang.IllegalArgumentException IllegalArgumentException})
     * @return A <code>boolean</code> value.
     *         <ul>
     *         <li><code>true</code> if the copy was successfully created and opened</li>
     *         <li>
     *         <code>false</code> in any of these cases:
     *         <ul>
     *         <li><code>packageName</code> is <code>null</code> or represents an empty string (includes strings
     *         containing only blank characters)</li>
     *         <li><code>fileName</code> is <code>null</code> or represents an empty string (includes strings containing
     *         only blank characters)</li>
     *         <li><code>{@link Desktop} class in not supported on this platform</code></li>
     *         </ul>
     *         </li>
     *         </ul>
     * @throws IOException If a file could not be created
     * @throws SecurityException
     *             If a security manager exists and its <code>{@link
     *             java.lang.SecurityManager#checkWrite(java.lang.String)}</code> method does not allow a file to be
     *             created, or its {@link java.lang.SecurityManager#checkRead(java.lang.String)} method denies read
     *             access to the file, or it denies the <code>AWTPermission("showWindowWithoutWarningBanner")</code>
     *             permission, or the calling thread is not allowed to create a
     *             subprocess
     * @throws UnsupportedOperationException if the current platform
     *             does not support the {@link Desktop.Action#OPEN} action
     * @see FileUtils#createCopyFromResource(String, String, boolean)
     * @see Desktop#open(File)
     */
    public static boolean openResource(final String packageName, final String fileName)
            throws SecurityException, IOException, IllegalArgumentException, UnsupportedOperationException {
        boolean resourceOpened;
        File tempFile = FileUtils.createCopyFromResource(packageName, fileName, true);
        if (tempFile == null) {
            resourceOpened = false;
        } else if (Desktop.isDesktopSupported()) {
            Desktop.getDesktop().open(tempFile);
            resourceOpened = true;
        } else {
            resourceOpened = false;
        }
        return resourceOpened;
    }

}
