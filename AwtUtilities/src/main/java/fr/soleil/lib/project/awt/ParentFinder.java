/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt;

import java.awt.Component;

/**
 * An interface for something able to find the parent for a {@link Component}
 * 
 * @author Rapha&euml;l GIRARDOT
 *
 */
public interface ParentFinder {

    /**
     * Returns the parent of a {@link Component}.
     * 
     * @param c The {@link Component}.
     * @return A {@link Component}.
     */
    public Component getParent(Component c);

}
