/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt.listener;

import java.awt.Component;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.util.Map;

import fr.soleil.lib.project.awt.IDataComponent;
import fr.soleil.lib.project.map.ConcurrentWeakHashMap;

/**
 * A class delegated to listening to {@link IDataComponent}, in order to update their content only when they really are
 * showing.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DataComponentDelegate implements IComponentMultiListener, ComponentListener, HierarchyListener {

    /**
     * The {@link Map} that stores whether the data of known {@link IDataComponent}s did change.
     */
    protected final Map<IDataComponent, Boolean> changedMap;

    /**
     * The {@link Map} that stores whether the data of known {@link IDataComponent}s did change.
     */
    protected final Map<IDataComponent, Boolean> autoUpdateMap;

    /**
     * Default constructor.
     * <p>
     * Does nothing else but constructing a new {@link DataComponentDelegate}.
     * </p>
     * <p>
     * To start the interaction with this {@link DataComponentDelegate}, you should call
     * {@link #setupAndListenToComponent(Component)} and {@link #setUpdateOnDataChanged(IDataComponent, boolean)}.
     * </p>
     */
    public DataComponentDelegate() {
        changedMap = new ConcurrentWeakHashMap<>();
        autoUpdateMap = new ConcurrentWeakHashMap<>();
    }

    @Override
    public void setupAndListenToComponent(Component component) {
        if (component instanceof IDataComponent) {
            IDataComponent dataComponent = (IDataComponent) component;
            component.addComponentListener(this);
            component.addHierarchyListener(this);
            changedMap.put(dataComponent, Boolean.valueOf(dataComponent.dataChanged()));
        }
    }

    /**
     * Sets whether this {@link DataComponentDelegate} should try to update an {@link IDataComponent}'s content every
     * time its data changed.
     * 
     * @param dataComponent The {@link IDataComponent}.
     * @param update Whether to automatically try to update the {@link IDataComponent}.
     * @see #setDataChanged(IDataComponent, boolean)
     */
    public void setUpdateOnDataChanged(IDataComponent dataComponent, boolean update) {
        if (dataComponent != null) {
            autoUpdateMap.put(dataComponent, Boolean.valueOf(update));
        }
    }

    /**
     * Notifies this {@link DataComponentDelegate} whether the data of an {@link IDataComponent} did change.
     * <p>
     * If the data did change and the {@link IDataComponent} is a showing {@link Component} and the update on data
     * change was configured for the {@link IDataComponent}, this method will call {@link #updateContent(Component)}.
     * </p>
     * 
     * @param dataComponent The {@link IDataComponent}.
     * @param changed Whether the data did change.
     * @see #setUpdateOnDataChanged(IDataComponent, boolean)
     * @see #updateContent(Component)
     */
    public void setDataChanged(IDataComponent dataComponent, boolean changed) {
        if (dataComponent != null) {
            changedMap.put(dataComponent, Boolean.valueOf(changed));
            if (changed && getBooleanValue(dataComponent, autoUpdateMap) && (dataComponent instanceof Component)) {
                updateContent((Component) dataComponent);
            }
        }
    }

    /**
     * Returns whether a {@link Component} is really showing, according to known visibility.
     * 
     * @param component The {@link Component}.
     * @param knownVisibility The known visibility (i.e. the know result of {@link Component#isVisible()}).
     * @return A <code>boolean</code>.
     */
    protected boolean isShowing(Component component, boolean knownVisibility) {
        return component.isShowing() && knownVisibility;
    }

    /**
     * Returns whether a {@link Component} is really showing.
     * 
     * @param component The {@link Component}.
     * @return A <code>boolean</code>.
     */
    protected boolean isShowing(Component component) {
        return isShowing(component, component.isVisible());
    }

    /**
     * Recovers a <code>boolean</code> value associated with an {@link IDataComponent} in a {@link Map}.
     * 
     * @param dataComponent The {@link IDataComponent}.
     * @param map The {@link Map}.
     * @return A <code>boolean</code>.
     */
    protected boolean getBooleanValue(IDataComponent dataComponent, Map<IDataComponent, Boolean> map) {
        Boolean val = map.get(dataComponent);
        return (val != null) && val.booleanValue();
    }

    /**
     * Returns whether some {@link IDataComponent}'s data is considered as changed.
     * <p>
     * Typically, this method can be used by {@link IDataComponent#dataChanged()}.
     * </p>
     * 
     * @param component The {@link IDataComponent}.
     * @return A <code>boolean</code>.
     */
    public boolean dataChanged(IDataComponent component) {
        return (component != null) && getBooleanValue(component, changedMap);
    }

    /**
     * Computes whether a {@link Component}'s content should be updated, according to known visibility.
     * <p>
     * This method will do something only if the {@link Component} is an {@link IDataComponent} and its data did change
     * and the {@link Component} is really showing according to {@link #isShowing(Component, boolean)} method.
     * </p>
     * 
     * @param component The {@link Component}.
     * @param knownVisibility The known visibility (i.e. the know result of {@link Component#isVisible()}).
     */
    protected void updateContent(Component component, boolean knownVisibility) {
        if (component instanceof IDataComponent) {
            IDataComponent dataComponent = (IDataComponent) component;
            boolean showing = isShowing(component, knownVisibility);
            if (showing) {
                boolean wasChanged = getBooleanValue(dataComponent, changedMap);
                if (wasChanged) {
                    setDataChanged(dataComponent, false);
                    dataComponent.updateFromData();
                }
            }
        }
    }

    /**
     * Computes whether a {@link Component}'s content should be updated, according to known visibility.
     * <p>
     * This method will do something only if the {@link Component} is an {@link IDataComponent} and its data did change
     * and the {@link Component} is really showing according to {@link #isShowing(Component)} method.
     * </p>
     * 
     * @param component The {@link Component}.
     */
    public void updateContent(Component component) {
        if (component != null) {
            updateContent(component, component.isVisible());
        }
    }

    @Override
    public void stopListeningToComponent(Component component) {
        if (component instanceof IDataComponent) {
            IDataComponent dataComponent = (IDataComponent) component;
            component.removeComponentListener(this);
            component.removeHierarchyListener(this);
            changedMap.remove(dataComponent);
        }
    }

    @Override
    public void hierarchyChanged(HierarchyEvent e) {
        if (e != null) {
            updateContent(e.getComponent());
        }
    }

    @Override
    public void componentResized(ComponentEvent e) {
        // not managed
    }

    @Override
    public void componentMoved(ComponentEvent e) {
        // not managed
    }

    @Override
    public void componentShown(ComponentEvent e) {
        if (e != null) {
            updateContent(e.getComponent(), true);
        }
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        if (e != null) {
            updateContent(e.getComponent(), false);
        }
    }

    @Override
    protected void finalize() {
        changedMap.clear();
        autoUpdateMap.clear();
    }

}
