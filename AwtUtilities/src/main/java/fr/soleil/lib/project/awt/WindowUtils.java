/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt;

import java.awt.Component;
import java.awt.Window;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * A class used to interact with {@link Window}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class WindowUtils implements ParentFinder {

    private static final WindowUtils INSTANCE = new WindowUtils();

    @Override
    public Component getParent(Component c) {
        return c == null ? null : c.getParent();
    }

    /**
     * Returns the parent {@link Window} of the given {@link Component}, using a custom {@link ParentFinder}.
     * 
     * @param component Child {@link Component}.
     * @param parentFinder The {@link ParentFinder} that is able to find {@link Component}'s parent.
     * @return The expected {@link Window}.
     */
    public static Window getWindowForComponent(Component component, ParentFinder parentFinder) {
        ParentFinder finder = parentFinder == null ? INSTANCE : parentFinder;
        Window window;
        if (component == null) {
            window = null;
        } else if (component instanceof Window) {
            window = (Window) component;
        } else {
            window = getWindowForComponent(finder.getParent(component));
        }
        return window;
    }

    /**
     * Returns the parent {@link Window} of the given {@link Component}.
     * 
     * @param component Child {@link Component}.
     * @return The expected {@link Window}.
     */
    public static Window getWindowForComponent(Component component) {
        return getWindowForComponent(component, INSTANCE);
    }

    /**
     * Closes a {@link Window}, as if it was closed through its exit cross
     * 
     * @param window The {@link Window} to close
     */
    public static void closeWindow(Window window) {
        WindowEvent event = new WindowEvent(window, WindowEvent.WINDOW_CLOSING);
        for (WindowListener listener : window.getWindowListeners()) {
            listener.windowClosing(event);
        }
        window.setVisible(false);
        event = new WindowEvent(window, WindowEvent.WINDOW_CLOSED);
        for (WindowListener listener : window.getWindowListeners()) {
            listener.windowClosed(event);
        }
    }

}
