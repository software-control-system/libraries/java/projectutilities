/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt;

import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import fr.soleil.lib.project.ObjectUtils;

/**
 * Common color utilities.
 * <p>
 * Also contains color rotation utilities.
 * </p>
 * <p>
 * Some of the code comes from no.geosoft.cc.color.ui.ColorUtil.
 * </p>
 * <p>
 * The getXXXGray methods algorithm come from <a
 * href="http://www.developpez.net/forums/d900539/java/interfaces-graphiques-java/conversion-couleur-niveau-gris"
 * >http://www.developpez.net/forums/d900539/java/interfaces-graphiques-java/conversion-couleur-niveau-gris</a>
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 * @author saintin
 * @author <a href="mailto:jacob.dreyer@geosoft.no">Jacob Dreyer</a>
 */
public class ColorUtils {
    // RED, GREEN, BLUE, MAGENTA, GRAY, LIGHT GREEN, LILA, ORANGE, SKY BLUE, PURPLE, KHAKI
    private static final Color[] DEFAULT_COLORS = { Color.RED, new Color(0, 210, 0), Color.BLUE, Color.MAGENTA,
            new Color(100, 100, 100), new Color(51, 255, 51), new Color(153, 153, 255), new Color(255, 165, 0),
            new Color(0, 220, 220), new Color(170, 100, 255), new Color(193, 160, 4) };
    private static final Color BRIGHTER = new Color(245, 245, 245);

    private static final ColorUtils COLOR_ROTATOR = new ColorUtils();

    private LinkedList<Color> colorList;
    private int typeColor;
    private Color[] customColors;

    public ColorUtils() {
        typeColor = 0;
        colorList = null;
        customColors = null;
    }

    /**
     * Generates a table of colors, all different
     * 
     * @param nbColors The wished number of colors (size of the table).
     * @return A <code>Color[]</code>, of size <code>nbColors</code>, containing colors which are
     *         all different, arranged as a rainbow.
     * @author SOLEIL
     */
    public static Color[] generateRainbowColors(int nbColors) {
        Color[] colors = null;
        if (nbColors > 0) {
            switch (nbColors) {
                case 1:
                    colors = new Color[] { Color.RED };
                    break;
                case 2:
                    colors = new Color[] { Color.RED, Color.BLUE };
                    break;
                case 3:
                    colors = new Color[] { Color.RED, Color.GREEN, Color.BLUE };
                    break;
                case 4:
                    colors = new Color[] { Color.RED, Color.GREEN, Color.BLUE, Color.MAGENTA };
                    break;
                case 5:
                    colors = new Color[] { Color.RED, Color.YELLOW, Color.GREEN, Color.BLUE, Color.MAGENTA };
                    break;
                case 6:
                    colors = new Color[] { Color.RED, Color.YELLOW, Color.GREEN, Color.CYAN, Color.BLUE,
                            Color.MAGENTA };
                    break;
                default:
                    colors = new Color[nbColors];
                    int[] rgb = buildColorMap(nbColors);
                    for (int i = 0; i < nbColors; i++) {
                        colors[i] = new Color(rgb[i]);
                    }
                    break;
            }
        }
        return colors;
    }

    /**
     * Builds an int array, representing a rainbow color map
     * 
     * @return a nb 32Bit [ARGB] array, null if fails
     * @see #generateRainbowColors(int)
     */
    protected static int[] buildColorMap(int nb) {
        int[] ret;
        if (nb > -1) {
            ret = new int[nb];
            double nb_1_6 = (double) nb / (double) 6;
            int i_nb_1_6 = (int) Math.rint(nb_1_6);
            int i_nb_2_6 = (int) Math.rint(2 * nb_1_6);
            int i_nb_3_6 = (int) Math.rint(3 * nb_1_6);
            int i_nb_4_6 = (int) Math.rint(4 * nb_1_6);
            int i_nb_5_6 = (int) Math.rint(5 * nb_1_6);
            // red -> yellow
            ret[0] = Color.RED.getRGB();
            for (int i = 1; i < i_nb_1_6; i++) {
                double frac = i / nb_1_6;
                double step = 255 * frac;
                ret[i] = ((255 & 0xFF) << 24) // alpha
                        | ((255 & 0xFF) << 16) // red
                        | ((((int) Math.rint(0 + step)) & 0xFF) << 8); // green
            }
            // yellow -> green
            ret[i_nb_1_6] = Color.YELLOW.getRGB();
            for (int i = i_nb_1_6 + 1; i < i_nb_2_6; i++) {
                double frac = i / nb_1_6;
                double step = 255 * frac;
                ret[i] = ((255 & 0xFF) << 24) // alpha
                        | ((((int) Math.rint(255 - step)) & 0xFF) << 16) // red
                        | ((255 & 0xFF) << 8); // green
            }
            // green -> cyan
            ret[i_nb_2_6] = Color.GREEN.getRGB();
            for (int i = i_nb_2_6 + 1; i < i_nb_3_6; i++) {
                double frac = i / nb_1_6;
                double step = 255 * frac;
                ret[i] = ((255 & 0xFF) << 24) // alpha
                        | ((255 & 0xFF) << 8) // green
                        | ((((int) Math.rint(0 + step)) & 0xFF) << 0); // blue
            }
            // cyan -> blue
            ret[i_nb_3_6] = Color.CYAN.getRGB();
            for (int i = i_nb_3_6 + 1; i < i_nb_4_6; i++) {
                double frac = i / nb_1_6;
                double step = 255 * frac;
                ret[i] = ((255 & 0xFF) << 24) // alpha
                        | ((((int) Math.rint(255 - step)) & 0xFF) << 8) // green
                        | ((255 & 0xFF) << 0); // blue
            }
            // blue -> magenta
            ret[i_nb_4_6] = Color.BLUE.getRGB();
            for (int i = i_nb_4_6 + 1; i < i_nb_5_6; i++) {
                double frac = i / nb_1_6;
                double step = 255 * frac;
                ret[i] = ((255 & 0xFF) << 24) // alpha
                        | ((((int) Math.rint(0 + step)) & 0xFF) << 16) // red
                        | ((255 & 0xFF) << 0); // blue
            }
            // magenta -> red
            ret[i_nb_5_6] = Color.MAGENTA.getRGB();
            for (int i = i_nb_5_6 + 1; i < nb; i++) {
                double frac = i / nb_1_6;
                double step = 255 * frac;
                ret[i] = ((255 & 0xFF) << 24) // alpha
                        | ((255 & 0xFF) << 16) // red
                        | ((((int) Math.rint(255 - step)) & 0xFF) << 0); // blue
            }
        } else {
            ret = null;
        }
        return ret;
    }

    /**
     * Generates a new {@link Color} array that can be used for {@link Color} rotation
     * 
     * @return A {@link Color} array
     */
    public static Color[] generateDefaultColors() {
        return DEFAULT_COLORS.clone();
    }

    /**
     * Returns a copy of the previously registered custom {@link Color}s
     * 
     * @return A {@link Color} array
     */
    public Color[] getCustomColors() {
        Color[] custom = customColors;
        return (custom == null ? null : custom.clone());
    }

    /**
     * Sets the custom {@link Color}s to use
     * 
     * @param customColors The custom {@link Color}s to use
     */
    public synchronized void setCustomColors(Color[] customColors) {
        if (!ObjectUtils.sameObject(customColors, this.customColors)) {
            this.customColors = customColors;
            colorList = null;
        }
    }

    /**
     * Returns next {@link Color} from color list, using index rotation when necessary.
     * 
     * @return An unused {@link Color}.
     */
    public synchronized Color getNextColor() {
        return getNextColorNoLock();
    }

    /**
     * Returns whether a gray is too near another one.
     * 
     * @param gray1 The 1st gray.
     * @param gray2 The 2nd gray.
     * @return A <code>boolean</code>.
     */
    protected static boolean isTooNear(double gray1, double gray2) {
        return Math.abs(gray1 - gray2) < 30;
    }

    /**
     * Returns the gray encoding of a {@link Color}, in order to compare it with another one
     * 
     * @param color The {@link Color}
     * @return A <code>double</code>
     * @see #isTooNear(double, double)
     */
    protected static double toGrayBrightnessDistanceTest(Color color) {
        return color == null ? Double.NaN : getRealGray(color.getRGB());
    }

    /**
     * Returns next {@link Color} from color list, using index rotation when necessary, and avoiding being too near a
     * given Color.
     * 
     * @param refColor The {@link Color} from which the resulting {@link Color} should not be too near.
     * 
     * @return An unused {@link Color}.
     */
    public synchronized Color getNextColorFarEnoughFrom(Color refColor) {
        Color next = getNextColorNoLock();
        if (refColor != null) {
            double refGray = toGrayBrightnessDistanceTest(refColor);
            while (isTooNear(refGray, toGrayBrightnessDistanceTest(next))) {
                next = getNextColorNoLock();
            }
        }
        return next;
    }

    private Color getNextColorNoLock() {
        Color result;
        if (colorList == null) {
            colorList = new LinkedList<>();
            typeColor = 0;
            initColor();
        } else if (colorList.isEmpty()) {
            typeColor = (typeColor + 1) % 3;
            initColor();
        }
        result = colorList.pollFirst();
        return result;
    }

    /**
     * Initializes {@link Color} list with basic colors, then darker and brighter colors, or with
     * custom colors if previously defined.
     */
    private void initColor() {
        Color[] custom = customColors;
        if ((custom == null) || (custom.length == 0)) {
            for (Color color : DEFAULT_COLORS) {
                if (typeColor == 0) {
                    colorList.add(color);
                } else if (typeColor == 1) {
                    colorList.add(darker(color, 0.4));
                } else {
                    colorList.add(blend(color, BRIGHTER));
                }
            }
        } else {
            for (Color color : custom) {
                colorList.add(color);
            }
        }

    }

    public static int getRed(int rgba) {
        return (rgba >> 16) & 0xFF;
    }

    public static int getGreen(int rgba) {
        return (rgba >> 8) & 0xFF;
    }

    public static int getBlue(int rgba) {
        return (rgba >> 0) & 0xFF;
    }

    public static int getAlpha(int rgba) {
        return (rgba >> 24) & 0xFF;
    }

    public static int toRGBA(int r, int g, int b, int a) {
        return ((a & 0xFF) << 24) | ((r & 0xFF) << 16) | ((g & 0xFF) << 8) | ((b & 0xFF) << 0);
    }

    protected static float toFloat(int color) {
        return (color / 255f);
    }

    protected static int toInt(float color) {
        return (int) (color * 255);
    }

    /**
     * Calculates a {@link Color}'s gray level, using a basic algorithm :
     * (<code>color.getRed() + color.getGreen() + color.getBlue()) / 3.0</code>)
     * 
     * @param color The {@link Color}
     * @return A <code>double</code>: <code>0 &le; gray &le; 255</code>
     */
    public static double getBasicGray(Color color) {
        double gray;
        if (color == null) {
            gray = Double.NaN;
        } else {
            gray = getBasicGray(color.getRGB());
        }
        return gray;
    }

    /**
     * Calculates a color's gray level, using a basic algorithm :
     * (<code>(red + green + blue) / 3.0</code>)
     * 
     * @param rgb The color rgb encoding
     * @return A <code>double</code>: <code>0 &le; gray &le; 255</code>
     */
    public static double getBasicGray(int rgb) {
        return (getRed(rgb) + getGreen(rgb) + getBlue(rgb)) / 3.0d;
    }

    /**
     * Calculates a {@link Color}'s gray level, using following algorithm :
     * <code>0.2125 * color.getRed() + 0.7154 * color.getGreen() + 0.0721 * color.getBlue()</code>
     * 
     * @param color The {@link Color}
     * @return A <code>double</code>: <code>0 &le; gray &le; 255</code>
     */
    public static double getRealGray(Color color) {
        double gray;
        if (color == null) {
            gray = Double.NaN;
        } else {
            gray = getRealGray(color.getRGB());
        }
        return gray;
    }

    /**
     * Calculates a color's gray level, using following algorithm :
     * <code>0.2125 * red + 0.7154 * green + 0.0721 * blue</code>
     * 
     * @param rgb The color rgb encoding
     * @return A <code>double</code>: <code>0 &le; gray &le; 255</code>
     */
    public static double getRealGray(int rgb) {
        return 0.2125d * getRed(rgb) + 0.7154d * getGreen(rgb) + 0.0721d * getBlue(rgb);
    }

    /**
     * Calculates a {@link Color}'s gray level, using following algorithm :
     * <code> 0.299 * color.getRed() + 0.587 * color.getGreen() + 0.114 * color.getBlue()</code>
     * 
     * @param color The {@link Color}
     * @return A <code>double</code>: <code>0 &le; gray &le; 255</code>
     */
    public static double getGammaGray(Color color) {
        double gray;
        if (color == null) {
            gray = Double.NaN;
        } else {
            gray = getGammaGray(color.getRGB());
        }
        return gray;
    }

    /**
     * Calculates a color's gray level, using following algorithm :
     * <code> 0.299 * red + 0.587 * green) + 0.114 * blue</code>
     * 
     * @param rgb The color rgb encoding
     * @return A <code>double</code>: <code>0 &le; gray &le; 255</code>
     */
    public static double getGammaGray(int rgb) {
        return 0.299d * getRed(rgb) + 0.587d * getGreen(rgb) + 0.114d * getBlue(rgb);
    }

    /**
     * Computes the best (black or white) foreground color for given background color.
     * 
     * @param bg The background color.
     * @return A {@link Color}.
     */
    public static Color getBestForeground(Color bg) {
        Color fg;
        if (bg == null) {
            fg = Color.BLACK;
        } else {
            fg = Math.round(getRealGray(bg)) < 128 ? Color.WHITE : Color.BLACK;
        }
        return fg;
    }

    /**
     * Blend two colors.
     * 
     * @param color1 First color to blend.
     * @param color2 Second color to blend.
     * @param ratio Blend ratio. 0.5 will give even blend, 1.0 will return color1, 0.0 will return
     *            color2 and so on.
     * @return Blended color.
     */
    public static Color blend(Color color1, Color color2, double ratio) {
        Color color;
        if ((color1 == null) || (color2 == null)) {
            color = null;
        } else {
            float r = (float) ratio;
            float ir = (float) 1.0 - r;

            float rgb1[] = new float[3];
            float rgb2[] = new float[3];

            color1.getColorComponents(rgb1);
            color2.getColorComponents(rgb2);

            color = new Color(rgb1[0] * r + rgb2[0] * ir, rgb1[1] * r + rgb2[1] * ir, rgb1[2] * r + rgb2[2] * ir);
        }
        return color;
    }

    /**
     * Blend two colors.
     * 
     * @param rgb1 First color to blend.
     * @param rgb2 Second color to blend.
     * @param ratio Blend ratio. 0.5 will give even blend, 1.0 will return rgb1, 0.0 will return
     *            rgb2 and so on.
     * @return Blended color's rgb.
     */
    public static int blend(int rgb1, int rgb2, double ratio) {
        float r = (float) ratio;
        float ir = (float) 1.0 - r;

        float r1 = toFloat(getRed(rgb1));
        float r2 = toFloat(getRed(rgb2));
        float g1 = toFloat(getGreen(rgb1));
        float g2 = toFloat(getGreen(rgb2));
        float b1 = toFloat(getBlue(rgb1));
        float b2 = toFloat(getBlue(rgb2));

        return toRGBA(toInt(r1 * r + r2 * ir), toInt(g1 * r + g2 * ir), toInt(b1 * r + b2 * ir), 255);
    }

    /**
     * Make an even blend between two colors.
     * 
     * @param c1 First color to blend.
     * @param c2 Second color to blend.
     * @return Blended color.
     */
    public static Color blend(Color color1, Color color2) {
        return color1 == null || color2 == null ? null : blend(color1, color2, 0.5);
    }

    /**
     * Make an even blend between two colors.
     * 
     * @param rgb1 First color to blend.
     * @param rgb2 Second color to blend.
     * @return Blended color's rgb.
     */
    public static int blend(int rgb1, int rgb2) {
        return blend(rgb1, rgb2, 0.5);
    }

    /**
     * Make a color darker.
     * 
     * @param color Color to make darker.
     * @param fraction Darkness fraction.
     * @return Darker color.
     */
    public static Color darker(Color color, double fraction) {
        return color == null ? null : new Color(darker(color.getRGB(), fraction), true);
    }

    /**
     * Make a color darker.
     * 
     * @param rgba Color to make darker.
     * @param fraction Darkness fraction.
     * @return Darker color's rgb.
     */
    public static int darker(int rgba, double fraction) {
        int red = (int) Math.round(getRed(rgba) * (1.0 - fraction));
        int green = (int) Math.round(getGreen(rgba) * (1.0 - fraction));
        int blue = (int) Math.round(getBlue(rgba) * (1.0 - fraction));

        if (red < 0) {
            red = 0;
        } else if (red > 255) {
            red = 255;
        }
        if (green < 0) {
            green = 0;
        } else if (green > 255) {
            green = 255;
        }
        if (blue < 0) {
            blue = 0;
        } else if (blue > 255) {
            blue = 255;
        }

        int alpha = getAlpha(rgba);

        return toRGBA(red, green, blue, alpha);
    }

    /**
     * Make a color brighter.
     * 
     * @param color Color to make brighter.
     * @param fraction Brightness fraction.
     * @return Brighter color.
     */
    public static Color brighter(Color color, double fraction) {
        return color == null ? null : new Color(brighter(color.getRGB(), fraction), true);
    }

    /**
     * Makes a color brighter.
     * 
     * @param rgba Color to make lighter.
     * @param fraction Brightness fraction.
     * @return Brighter color's rgb.
     */
    public static int brighter(int rgba, double fraction) {
        return darker(rgba, -fraction);
    }

    /**
     * Makes a color brighter.
     * 
     * @param color Color to make lighter.
     * @return Brighter color.
     */
    public static Color brighter(Color base) {
        return base == null ? null : new Color(brighter(base.getRGB()), true);
    }

    /**
     * Makes a color brighter.
     * 
     * @param rgba Color to make lighter.
     * @return Brighter color's rgb.
     */
    public static int brighter(int rgba) {
        int r = getRed(rgba);
        int g = getGreen(rgba);
        int b = getBlue(rgba);

        boolean isChange = false;
        if (r == 0) {
            r += 150;
            isChange = true;
        }
        if (g == 0) {
            g += 150;
            isChange = true;
        }
        if (b == 0) {
            b += 150;
            isChange = true;
        }

        if (!isChange) {
            r = 255 - (r % 255) + 150;
            if (r > 255) {
                r = 255;
            }
            g = 255 - (g % 255) + 150;
            if (r > 255) {
                g = 255;
            }
            b = 255 - (b % 255) + 150;
            if (b > 255) {
                b = 255;
            }
        }
        int a = getAlpha(rgba);
        return toRGBA(r, g, b, a);
    }

    /**
     * Return the hex name of a specified color.
     * 
     * @param color Color to get hex name of.
     * @return Hex name of color: "rrggbb".
     */
    public static String getHexName(Color color) {
        return color == null ? null : getHexName(color.getRGB());
    }

    /**
     * Return the hex name of a specified color.
     * 
     * @param rgb Color to get hex name of.
     * @return Hex name of color: "rrggbb".
     */
    public static String getHexName(int rgb) {
        int r = getRed(rgb);
        int g = getGreen(rgb);
        int b = getBlue(rgb);

        String rHex = Integer.toString(r, 16);
        String gHex = Integer.toString(g, 16);
        String bHex = Integer.toString(b, 16);

        return (rHex.length() == 2 ? ObjectUtils.EMPTY_STRING + rHex : "0" + rHex)
                + (gHex.length() == 2 ? ObjectUtils.EMPTY_STRING + gHex : "0" + gHex)
                + (bHex.length() == 2 ? ObjectUtils.EMPTY_STRING + bHex : "0" + bHex);
    }

    /**
     * Return the "distance" between two colors. The rgb entries are taken to be coordinates in a 3D
     * space [0.0-1.0], and this method returns the distance between the coordinates for the first
     * and second color.
     * 
     * @param r1, g1, b1 First color.
     * @param r2, g2, b2 Second color.
     * @return Distance between colors.
     */
    public static double colorDistance(double r1, double g1, double b1, double r2, double g2, double b2) {
        double a = r2 - r1;
        double b = g2 - g1;
        double c = b2 - b1;

        return Math.sqrt(a * a + b * b + c * c);
    }

    /**
     * Return the "distance" between two colors.
     * 
     * @param color1 First color [r,g,b].
     * @param color2 Second color [r,g,b].
     * @return Distance between colors.
     */
    public static double colorDistance(double[] color1, double[] color2) {
        return color1 == null || color2 == null || color1.length < 3 || color2.length < 3 ? Double.NaN
                : colorDistance(color1[0], color1[1], color1[2], color2[0], color2[1], color2[2]);
    }

    /**
     * Return the "distance" between two colors.
     * 
     * @param color1 First color.
     * @param color2 Second color.
     * @return Distance between colors.
     */
    public static double colorDistance(Color color1, Color color2) {
        double distance;
        if (color1 == null || color2 == null) {
            distance = Double.NaN;
        } else {
            float rgb1[] = new float[3];
            float rgb2[] = new float[3];

            color1.getColorComponents(rgb1);
            color2.getColorComponents(rgb2);

            distance = colorDistance(rgb1[0], rgb1[1], rgb1[2], rgb2[0], rgb2[1], rgb2[2]);
        }
        return distance;
    }

    /**
     * Return the "distance" between two colors.
     * 
     * @param rgb1 First color.
     * @param rgb2 Second color.
     * @return Distance between colors.
     */
    public static double colorDistance(int rgb1, int rgb2) {
        return colorDistance(toFloat(getRed(rgb1)), toFloat(getGreen(rgb1)), toFloat(getBlue(rgb1)),
                toFloat(getRed(rgb2)), toFloat(getGreen(rgb2)), toFloat(getBlue(rgb2)));
    }

    /**
     * Check if a color is more dark than light. Useful if an entity of this color is to be labeled:
     * Use white label on a "dark" color and black label on a "light" color.
     * 
     * @param r,g,b Color to check.
     * @return True if this is a "dark" color, false otherwise.
     */
    public static boolean isDark(double r, double g, double b) {
        // Measure distance to white and black respectively
        double dWhite = colorDistance(r, g, b, 1.0, 1.0, 1.0);
        double dBlack = colorDistance(r, g, b, 0.0, 0.0, 0.0);

        return dBlack < dWhite;
    }

    /**
     * Check if a color is more dark than light. Useful if an entity of this color is to be labeled:
     * Use white label on a "dark" color and black label on a "light" color.
     * 
     * @param color Color to check.
     * @return True if this is a "dark" color, false otherwise.
     */
    public static boolean isDark(Color color) {
        return color == null ? false : isDark(color.getRGB());
    }

    /**
     * Check if a color is more dark than light. Useful if an entity of this color is to be labeled:
     * Use white label on a "dark" color and black label on a "light" color.
     * 
     * @param rgb Color to check.
     * @return True if this is a "dark" color, false otherwise.
     */
    public static boolean isDark(int rgb) {
        float r = toFloat(getRed(rgb));
        float g = toFloat(getGreen(rgb));
        float b = toFloat(getBlue(rgb));

        return isDark(r, g, b);
    }

    /**
     * Returns the color at medium distance from 2 colors
     * 
     * @param c1 The first {@link Color}
     * @param c2 The second {@link Color}
     * @return A {@link Color}
     */
    public static Color getMediumColor(Color c1, Color c2) {
        return c1 == null || c2 == null ? null : new Color(getMediumColor(c1.getRGB(), c2.getRGB()));
    }

    /**
     * Returns the color at medium distance from 2 colors
     * 
     * @param rgb1 The first color
     * @param rgb2 The second color
     * @return A color in RGB encoding
     */
    public static int getMediumColor(int rgb1, int rgb2) {
        return toRGBA((getRed(rgb1) + 3 * getRed(rgb2)) / 4, (getGreen(rgb1) + 3 * getGreen(rgb2)) / 4,
                (getBlue(rgb1) + 3 * getBlue(rgb2)) / 4, 255);
    }

    private static void checkLabel(Label colorLabel, Color refColorToAvoid) {
        if ((colorLabel != null) && (refColorToAvoid != null)) {
            if (isTooNear(toGrayBrightnessDistanceTest(colorLabel.getBackground()),
                    toGrayBrightnessDistanceTest(refColorToAvoid))) {
                colorLabel.setText("skipped");
            } else {
                colorLabel.setText(ObjectUtils.EMPTY_STRING);
            }
        }
    }

    private static Label generateLabel(Color bg, Color refColorToAvoid) {
        Label colorLabel = new Label();
        if (bg != null) {
            colorLabel.setBackground(bg);
            Color fg = Math.round(ColorUtils.getRealGray(bg)) < 128 ? Color.WHITE : Color.BLACK;
            colorLabel.setForeground(fg);
            if (isTooNear(toGrayBrightnessDistanceTest(bg), toGrayBrightnessDistanceTest(refColorToAvoid))) {
                colorLabel.setText("skipped");
            }
        }
        return colorLabel;
    }

    public static void main(String[] args) {
        ColorUtils tool = new ColorUtils();
        final Panel colorPanel = new Panel(new GridBagLayout());
        Color bg = Color.WHITE;
        colorPanel.setBackground(bg);
        final Collection<Label> labels = new ArrayList<>();

        final GridBagConstraints buttonConstraints = new GridBagConstraints();
        buttonConstraints.fill = GridBagConstraints.HORIZONTAL;
        buttonConstraints.gridx = 0;
        buttonConstraints.gridy = 0;
        buttonConstraints.gridwidth = GridBagConstraints.REMAINDER;
        buttonConstraints.gridheight = 1;
        buttonConstraints.weightx = 1;
        buttonConstraints.weighty = 0;
        buttonConstraints.insets = new Insets(0, 0, 50, 0);

        final GridBagConstraints firstLineConstraints = new GridBagConstraints();
        firstLineConstraints.fill = GridBagConstraints.BOTH;
        firstLineConstraints.gridx = 0;
        firstLineConstraints.gridy = 1;
        firstLineConstraints.gridwidth = GridBagConstraints.REMAINDER;
        firstLineConstraints.gridheight = 1;
        firstLineConstraints.weightx = 1;
        firstLineConstraints.weighty = 0.33;
        firstLineConstraints.insets = new Insets(0, 50, 10, 50);

        final GridBagConstraints secondLineConstraints = new GridBagConstraints();
        secondLineConstraints.fill = GridBagConstraints.BOTH;
        secondLineConstraints.gridx = 0;
        secondLineConstraints.gridy = 2;
        secondLineConstraints.gridwidth = GridBagConstraints.REMAINDER;
        secondLineConstraints.gridheight = 1;
        secondLineConstraints.weightx = 1;
        secondLineConstraints.weighty = 0.33;
        secondLineConstraints.insets = new Insets(0, 50, 10, 50);

        final GridBagConstraints thirdLineConstraints = new GridBagConstraints();
        thirdLineConstraints.fill = GridBagConstraints.BOTH;
        thirdLineConstraints.gridx = 0;
        thirdLineConstraints.gridy = 3;
        thirdLineConstraints.gridwidth = GridBagConstraints.REMAINDER;
        thirdLineConstraints.gridheight = 1;
        thirdLineConstraints.weightx = 1;
        thirdLineConstraints.weighty = 0.33;
        thirdLineConstraints.insets = new Insets(0, 50, 50, 50);

        final Panel firstLinePanel = new Panel(new GridLayout(1, DEFAULT_COLORS.length));
        for (int i = 0; i < DEFAULT_COLORS.length; i++) {
            Label colorLabel = generateLabel(tool.getNextColor(), Color.WHITE);
            labels.add(colorLabel);
            firstLinePanel.add(colorLabel);
        }
        final Panel secondLinePanel = new Panel(new GridLayout(1, DEFAULT_COLORS.length));
        for (int i = 0; i < DEFAULT_COLORS.length; i++) {
            Label colorLabel = generateLabel(tool.getNextColor(), Color.WHITE);
            labels.add(colorLabel);
            secondLinePanel.add(colorLabel);
        }
        final Panel thirdLinePanel = new Panel(new GridLayout(1, DEFAULT_COLORS.length));
        for (int i = 0; i < DEFAULT_COLORS.length; i++) {
            Label colorLabel = generateLabel(tool.getNextColor(), Color.WHITE);
            labels.add(colorLabel);
            thirdLinePanel.add(colorLabel);
        }
        final Button switchButton = new Button("switch bright and dark");
        switchButton.addActionListener(new ActionListener() {
            @Override
            public synchronized void actionPerformed(ActionEvent e) {
                Component comp = colorPanel.getComponent(2);
                colorPanel.remove(secondLinePanel);
                colorPanel.remove(thirdLinePanel);
                if (comp == secondLinePanel) {
                    colorPanel.add(thirdLinePanel, secondLineConstraints);
                    colorPanel.add(secondLinePanel, thirdLineConstraints);
                } else {
                    colorPanel.add(secondLinePanel, secondLineConstraints);
                    colorPanel.add(thirdLinePanel, thirdLineConstraints);
                }
                colorPanel.invalidate();
                colorPanel.validate();
                colorPanel.repaint();
            }
        });
        colorPanel.add(switchButton, buttonConstraints);
        colorPanel.setBackground(Color.WHITE);
        colorPanel.add(firstLinePanel, firstLineConstraints);
        colorPanel.add(secondLinePanel, secondLineConstraints);
        colorPanel.add(thirdLinePanel, thirdLineConstraints);
        colorPanel.addMouseListener(new MouseAdapter() {
            int count = 0;

            @Override
            public void mousePressed(MouseEvent e) {
                count++;
                count = count % 3;
                Color bg;
                switch (count) {
                    case 0:
                        bg = Color.WHITE;
                        break;
                    case 1:
                        bg = Color.BLACK;
                        break;
                    default:
                        bg = switchButton.getBackground();
                        break;
                }
                for (Label label : labels) {
                    checkLabel(label, bg);
                }
                colorPanel.setBackground(bg);
                firstLinePanel.setBackground(bg);
                secondLinePanel.setBackground(bg);
                thirdLinePanel.setBackground(bg);
                colorPanel.repaint();
            }
        });
        firstLinePanel.setBackground(bg);
        secondLinePanel.setBackground(bg);
        thirdLinePanel.setBackground(bg);
        Frame testFrame = new Frame(tool.getClass().getSimpleName() + " test");
        testFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        testFrame.add(colorPanel);
        testFrame.setSize(800, 600);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }

}
