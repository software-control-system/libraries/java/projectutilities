/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import fr.soleil.lib.project.ObjectUtils;

public class FontUtils {

    private static FontRenderContext frc = null;

    /**
     * Default text used to compute {@link TextLayout} in case of <code>null</code> or empty text ({@link TextLayout}
     * constructors are compatible with neither of these 2 cases).
     */
    public static final String TEXTLAYOUT_DEFAULT_TEXT = " ";

    protected static void initFontRendererContext() {
        if (frc == null) {
            BufferedImage img = new BufferedImage(10, 10, BufferedImage.TYPE_INT_RGB);
            Graphics g = img.getGraphics();
            prepareGraphicsForText(g);
            if (g instanceof Graphics2D) {
                frc = ((Graphics2D) g).getFontRenderContext();
            }
            g.dispose();
        }
    }

    /**
     * Prepares some {@link Graphics} to display some text. This enables anti-aliasing for the concerned
     * {@link Graphics}
     * 
     * @param g The concerned {@link Graphics}
     */
    public static void prepareGraphicsForText(Graphics g) {
        if (g instanceof Graphics2D) {
            Graphics2D g2d = (Graphics2D) g;
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        }
    }

    /**
     * Retrieve the default render context.
     */
    public static FontRenderContext getDefaultRenderContext() {
        initFontRendererContext();
        return frc;
    }

    /**
     * Measure a string (using AA font , zoom and translation of Graphics2D are not supported)
     * 
     * @param s String to be measured
     * @param f Font used
     * @param frc The {@link FontRenderContext} that may be used to calculate string dimensions.
     * @return {@link Dimension} of the string (in pixel)
     */
    public static Dimension measureString(String s, Font f, FontRenderContext frc) {
        Rectangle2D bounds = f.getStringBounds(s, frc == null ? getDefaultRenderContext() : frc);
        int w = (int) (bounds.getWidth() + 0.5);
        int h = (int) (bounds.getHeight() + 0.5);
        return new Dimension(w, h);
    }

    /**
     * Measure a string (using AA font , zoom and translation of Graphics2D are not supported)
     * 
     * @param s String to be measured
     * @param f Font used
     * @return {@link Dimension} of the string (in pixel)
     */
    public static Dimension measureString(String s, Font f) {
        Rectangle2D bounds = f.getStringBounds(s, getDefaultRenderContext());
        int w = (int) (bounds.getWidth() + 0.5);
        int h = (int) (bounds.getHeight() + 0.5);
        return new Dimension(w, h);
    }

    /**
     * Returns the font height based on text bounds.
     * 
     * @param textLayout The text bounds, as a {@link Rectangle2D}.
     * @return An <code>int</code>.
     */
    public static final int getFontHeight(Rectangle2D textBounds) {
        int height;
        if (textBounds == null) {
            height = 0;
        } else {
            height = (int) Math.ceil(textBounds.getHeight());
        }
        return height;
    }

    /**
     * Returns the font height inside a {@link TextLayout}.
     * 
     * @param textLayout The {@link TextLayout}.
     * @return An <code>int</code>.
     */
    public static final int getFontHeight(TextLayout textLayout) {
        return textLayout == null ? 0 : getFontHeight(textLayout.getBounds());
    }

    /**
     * Returns the {@link Font} height inside a {@link Component}.
     * 
     * @param font The {@link Font}.
     * @param frc The {@link FontRenderContext}.
     * @param component The {@link Component}.
     * @param text The text.
     * @param textBounds The previously calculated text bounds. Can be <code>null</code>.
     * @return A <code>float</code>: the font height.
     */
    public static int getFontHeight(Font font, FontRenderContext frc, String text, Component component,
            Rectangle2D textBounds) {
        int currentFontHeight;
        if ((text == null) || text.isEmpty() || (frc == null)) {
            currentFontHeight = component.getFontMetrics(font).getHeight();
            if (currentFontHeight == 0) {
                currentFontHeight = font.getSize();
            }
            if (textBounds != null) {
                currentFontHeight = Math.min(currentFontHeight, getFontHeight(textBounds));
            }
            if (currentFontHeight == 0) {
                currentFontHeight = font.getSize();
            }
        } else {
            // It is described in Font javadoc to use TextLayout in order to obtain the right surrounding rectangle.
            currentFontHeight = getFontHeight(new TextLayout(text, font, frc));
        }
        return currentFontHeight;
    }

    /**
     * Computes the text width based on given text bounds.
     * 
     * @param bounds The text bounds as a {@link Rectangle2D}.
     * @return An <code>int</code>.
     */
    public static int getTextWidth(Rectangle2D bounds) {
        int width;
        if (bounds == null) {
            width = 0;
        } else {
            width = (int) Math.ceil(bounds.getWidth() + Math.abs(bounds.getX()));
        }
        return width;
    }

    /**
     * Returns the effective width of some text, based on known {@link Font}, {@link FontRenderContext} and potentially
     * previously calculated {@link TextLayout}.
     * 
     * @param f The {@link Font}.
     * @param frc The {@link FontRenderContext}.
     * @param text The text for which to calculate effective width. If <code>null</code> or empty,
     *            {@link #TEXTLAYOUT_DEFAULT_TEXT}
     *            will be used instead.
     * @param textLayout The previously calculated {@link TextLayout}
     * @return An <code>int</code>: the text width.
     */
    public static int getTextWidth(Font f, FontRenderContext frc, String text, TextLayout textLayout) {
        int textWidth;
        String realText = text == null ? ObjectUtils.EMPTY_STRING : text;
        if (realText.trim().isEmpty() && !realText.isEmpty()) {
            // Experience showed TextLayout is bad at calculating space width
            textWidth = getTextWidth(f.getStringBounds(realText, frc));
        } else {
            // TextLayout is the best way to measure string, as described in Font javadoc.
            TextLayout layout = textLayout;
            if (layout == null) {
                layout = new TextLayout(realText.isEmpty() ? TEXTLAYOUT_DEFAULT_TEXT : realText, f, frc);
            }
            textWidth = getTextWidth(layout.getBounds());
            if (textWidth <= 0) {
                textWidth = getTextWidth(f.getStringBounds(realText, frc));
            }
        }
        return textWidth;
    }

    /**
     * Returns the effective width of some text, based on known {@link Font} and {@link FontRenderContext}.
     * 
     * @param f The {@link Font}.
     * @param frc The {@link FontRenderContext}.
     * @param text The text for which to calculate effective width. If <code>null</code> or empty,
     *            {@link #TEXTLAYOUT_DEFAULT_TEXT}
     *            will be used instead.
     * @return An <code>int</code>: the text width.
     */
    public static int getTextWidth(Font f, FontRenderContext frc, String text) {
        return getTextWidth(f, frc, text, null);
    }

    /**
     * Computes the effective text descent based on known text bounds.
     * 
     * @param bounds The text bounds as a {@link Rectangle2D}.
     * @return An <code>int</code>: the text effective descent.
     */
    public static int getEffectiveDescent(Rectangle2D bounds) {
        // After some tests, comparing bounds y and height seems to be the best way to obtain the real descent.
        // At the opposite, textLayout.getDescent() gives the potential descent, but not the real (effective) one.
        return bounds == null ? 0 : (int) Math.round((Math.abs(bounds.getHeight()) - Math.abs(bounds.getY())));
    }

    /**
     * Computes the effective text descent based on known {@link TextLayout}.
     * 
     * @param textLayout The {@link TextLayout}.
     * @return An <code>int</code>: the text effective descent.
     */
    public static int getEffectiveDescent(TextLayout textLayout) {
        // After some tests, comparing bounds y and height seems to be the best way to obtain the real descent.
        // At the opposite, textLayout.getDescent() gives the potential descent, but not the real (effective) one.
        return textLayout == null ? 0 : getEffectiveDescent(textLayout.getBounds());
    }

    /**
     * Returns the line metrics for the given font.
     * 
     * @param s String to be measured
     * @param f Font object
     * @return {@link LineMetrics}
     */
    public static LineMetrics getLineMetrics(String s, Font f) {
        return f.getLineMetrics(s, getDefaultRenderContext());
    }

}
