/*
 * This file is part of AwtUtilities.
 * 
 * AwtUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * AwtUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with AwtUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.awt.adapter;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.Future;

import fr.soleil.lib.project.ICancelable;

/**
 * This class is used with fr.soleil.swing.dialog.ProgressDialog (SwingUtilities) to be able to cancel a {@link Future}
 * or {@link ICancelable} on dialog closing
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class Canceler extends WindowAdapter {

    private Object cancelable = null;

    public Canceler() {
        super();
    }

    /**
     * @return the cancelable
     */
    public Object getCancelable() {
        return cancelable;
    }

    /**
     * @param cancelable the {@link Future} to set
     */
    public void setCancelable(Future<?> cancelable) {
        this.cancelable = cancelable;
    }

    /**
     * @param cancelable the cancelable to set
     */
    public void setCancelable(ICancelable cancelable) {
        this.cancelable = cancelable;
    }

    public void cancel() {
        if (cancelable instanceof ICancelable) {
            ((ICancelable) cancelable).setCanceled(true);
        } else if (cancelable instanceof Future<?>) {
            ((Future<?>) cancelable).cancel(true);
        }
    }

    @Override
    public void windowClosing(WindowEvent e) {
        cancel();
    }
}
