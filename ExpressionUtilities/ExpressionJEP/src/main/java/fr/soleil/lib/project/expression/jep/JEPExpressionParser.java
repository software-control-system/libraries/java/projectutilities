/*
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.expression.jep;

import org.nfunk.jep.JEP;

import fr.soleil.lib.project.expression.IExpressionParser;
import fr.soleil.lib.project.expression.exception.ExpressionEvaluationException;

/**
 * JEP implementation of {@link IExpressionParser}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class JEPExpressionParser implements IExpressionParser {

    private JEP parser;

    public JEPExpressionParser() {
        clean();
    }

    protected JEP getParser() {
        return parser;
    }

    @Override
    public void addVariable(String name, Object value) {
        parser.addVariable(name, value);
    }

    @Override
    public Object getVariableValue(String name) {
        return parser.getVarValue(name);
    }

    @Override
    public void removeVariable(String name) {
        parser.removeVariable(name);
    }

    @Override
    public double parseExpression(String expression) throws ExpressionEvaluationException {
        try {
            parser.parseExpression(expression);
            return parser.getValue();
        } catch (Exception e) {
            throw new ExpressionEvaluationException(e.getMessage(), e);
        }
    }

    @Override
    public void clean() {
        JEP parser = new JEP();
        parser.addStandardConstants();
        parser.addStandardFunctions();
        this.parser = parser;
    }

}
