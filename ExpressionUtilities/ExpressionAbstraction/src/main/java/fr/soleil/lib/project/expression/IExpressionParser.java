/*
 * This file is part of ExpressionAbstraction.
 * 
 * ExpressionAbstraction is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionAbstraction is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionAbstraction. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.expression;

import fr.soleil.lib.project.expression.exception.ExpressionEvaluationException;

/**
 * An interface for something that is able to parse some expressions
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IExpressionParser {

    /**
     * Adds a variable to this {@link IExpressionParser}
     * 
     * @param name The variable name
     * @param value The variable value
     */
    public void addVariable(String name, Object value);

    /**
     * Returns the value of a variable known by this {@link IExpressionParser}
     * 
     * @param name The variable name
     * @return The expected value.
     */
    public Object getVariableValue(String name);

    /**
     * Removes a variable from this {@link IExpressionParser}
     * 
     * @param name The name of the variable to remove
     */
    public void removeVariable(String name);

    /**
     * Parses an expression and returns the evaluated result.
     * 
     * @param expression The expression to parse
     * @return The evaluated result, as a <code>double</code>
     * @throws ExpressionEvaluationException When the parsing failed
     */
    public double parseExpression(String expression) throws ExpressionEvaluationException;

    /**
     * Cleans this {@link IExpressionParser}, removing all known variables.
     */
    public void clean();
}
