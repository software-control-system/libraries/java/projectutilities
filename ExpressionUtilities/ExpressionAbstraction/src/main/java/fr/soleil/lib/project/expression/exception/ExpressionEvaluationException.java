/*
 * This file is part of ExpressionAbstraction.
 * 
 * ExpressionAbstraction is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionAbstraction is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionAbstraction. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.expression.exception;

/**
 * An {@link Exception} thrown when the parsing of an expression failed
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ExpressionEvaluationException extends Exception {

    private static final long serialVersionUID = 664809191821423789L;

    public ExpressionEvaluationException() {
        super();
    }

    public ExpressionEvaluationException(String message) {
        super(message);
    }

    public ExpressionEvaluationException(Throwable cause) {
        super(cause);
    }

    public ExpressionEvaluationException(String message, Throwable cause) {
        super(message, cause);
    }

}
