/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation;

import fr.soleil.lib.project.expression.jeval.evaluation.operator.Operator;

/**
 * Represents an operand being processed in the expression.
 */
public class ExpressionOperand {

    // The value of the operand.
    private String value = null;

    // The unary operator for the operand, if one exists.
    private Operator unaryOperator = null;

    /**
     * Create a new ExpressionOperand.
     * 
     * @param value
     *            The value for the new ExpressionOperand.
     * @param unaryOperator
     *            The unary operator for this object.
     */
    public ExpressionOperand(final String value, final Operator unaryOperator) {
        this.value = value;
        this.unaryOperator = unaryOperator;
    }

    /**
     * Returns the value of this object.
     * 
     * @return The value of this object.
     */
    public String getValue() {
        return value;
    }

    /**
     * Returns the unary operator for this object.
     * 
     * @return The unary operator for this object.
     */
    public Operator getUnaryOperator() {
        return unaryOperator;
    }
}