/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.operator;

import fr.soleil.lib.project.expression.jeval.evaluation.EvaluationException;

/**
 * An oerator than can specified in an expression.
 */
public interface Operator {

    /**
     * Evaluates two double operands.
     * 
     * @param leftOperand
     *            The left operand being evaluated.
     * @param rightOperand
     *            The right operand being evaluated.
     */
    public abstract double evaluate(double leftOperand, double rightOperand);

    /**
     * Evaluates two string operands.
     * 
     * @param leftOperand
     *            The left operand being evaluated.
     * @param rightOperand
     *            The right operand being evaluated.
     * 
     * @return String The value of the evaluated operands.
     * 
     * @exception EvaluateException
     *                Thrown when an error is found while evaluating the
     *                expression.
     */
    public abstract String evaluate(final String leftOperand, final String rightOperand) throws EvaluationException;

    /**
     * Evaluate one double operand.
     * 
     * @param operand
     *            The operand being evaluated.
     */
    public abstract double evaluate(final double operand);

    /**
     * Returns the character(s) that makes up the operator.
     * 
     * @return The operator symbol.
     */
    public abstract String getSymbol();

    /**
     * Returns the precedence given to this operator.
     * 
     * @return The precedecne given to this operator.
     */
    public abstract int getPrecedence();

    /**
     * Returns the length of the operator symbol.
     * 
     * @return The length of the operator symbol.
     */
    public abstract int getLength();

    /**
     * Returns an indicator of if the operator is unary or not.
     * 
     * @return An indicator of if the operator is unary or not.
     */
    public abstract boolean isUnary();
}