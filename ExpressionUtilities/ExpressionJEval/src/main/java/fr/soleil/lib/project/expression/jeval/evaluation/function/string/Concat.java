/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.function.string;

import java.util.ArrayList;

import fr.soleil.lib.project.expression.jeval.evaluation.EvaluationConstants;
import fr.soleil.lib.project.expression.jeval.evaluation.Evaluator;
import fr.soleil.lib.project.expression.jeval.evaluation.function.Function;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionConstants;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionException;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionHelper;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionResult;

/**
 * This class is a function that executes within Evaluator. The function
 * concatenates the second string to the end of the first. See the
 * String.concat(String) method in the JDK for a complete description of how
 * this function works.
 */
public class Concat implements Function {
    /**
     * Returns the name of the function - "concat".
     * 
     * @return The name of this function class.
     */
    @Override
    public String getName() {
        return "concat";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     * 
     * @param evaluator
     *            An instance of Evaluator.
     * @param arguments
     *            A string argument that will be converted into two string
     *            arguments. The first argument is the string in which the
     *            second argument string will be concatenated. The string
     *            argument(s) HAS to be enclosed in quotes. White space that is
     *            not enclosed within quotes will be trimmed. Quote characters
     *            in the first and last positions of any string argument (after
     *            being trimmed) will be removed also. The quote characters used
     *            must be the same as the quote characters used by the current
     *            instance of Evaluator. If there are multiple arguments, they
     *            must be separated by a comma (",").
     * 
     * @return Returns a strng that is made up the first string, followed by the
     *         second string.
     * 
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    @Override
    public FunctionResult execute(final Evaluator evaluator, final String arguments) throws FunctionException {
        String result = null;
        String exceptionMessage = "Two string arguments are required.";

        ArrayList<String> strings = FunctionHelper.getStrings(arguments,
                EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);

        if (strings.size() != 2) {
            throw new FunctionException(exceptionMessage);
        }

        try {
            String argumentOne = FunctionHelper.trimAndRemoveQuoteChars(strings.get(0), evaluator.getQuoteCharacter());
            String argumentTwo = FunctionHelper.trimAndRemoveQuoteChars(strings.get(1), evaluator.getQuoteCharacter());
            result = argumentOne.concat(argumentTwo);
        } catch (FunctionException fe) {
            throw new FunctionException(fe.getMessage(), fe);
        } catch (Exception e) {
            throw new FunctionException(exceptionMessage, e);
        }

        return new FunctionResult(result, FunctionConstants.FUNCTION_RESULT_TYPE_STRING);
    }
}