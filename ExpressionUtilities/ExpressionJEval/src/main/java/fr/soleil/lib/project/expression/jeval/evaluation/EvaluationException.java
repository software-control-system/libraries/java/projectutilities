/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation;

/**
 * This exception is thrown when an error occurs during the evaluation process.
 */
public class EvaluationException extends Exception {

    private static final long serialVersionUID = -3010333364122748053L;

    /**
     * This constructor takes a custom message as input.
     * 
     * @param message
     *            A custom message for the exception to display.
     */
    public EvaluationException(String message) {
        super(message);
    }

    /**
     * This constructor takes an exception as input.
     * 
     * @param exception
     *            An exception.
     */
    public EvaluationException(Exception exception) {
        super(exception);
    }

    /**
     * This constructor takes an exception as input.
     * 
     * @param message
     *            A custom message for the exception to display.
     * @param exception
     *            An exception.
     */
    public EvaluationException(String message, Exception exception) {
        super(message, exception);
    }
}