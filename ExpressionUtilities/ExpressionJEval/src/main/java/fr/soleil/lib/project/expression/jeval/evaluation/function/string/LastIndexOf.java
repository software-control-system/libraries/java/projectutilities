/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.function.string;

import java.util.ArrayList;

import fr.soleil.lib.project.expression.jeval.evaluation.EvaluationConstants;
import fr.soleil.lib.project.expression.jeval.evaluation.Evaluator;
import fr.soleil.lib.project.expression.jeval.evaluation.function.Function;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionConstants;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionException;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionHelper;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionResult;

/**
 * This class is a function that executes within Evaluator. The function returns
 * the index within the source string of the last occurrence of the substring,
 * starting at the specified index. See the String.lastIndexOf(String, int)
 * method in the JDK for a complete description of how this function works.
 */
public class LastIndexOf implements Function {
    /**
     * Returns the name of the function - "lastIndexOf".
     * 
     * @return The name of this function class.
     */
    @Override
    public String getName() {
        return "lastIndexOf";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     * 
     * @param evaluator
     *            An instance of Evaluator.
     * @param arguments
     *            A string argument that will be converted into two string
     *            arguments and one integer argument. The first argument is the
     *            source string, the second argument is the substring and the
     *            third argument is the index. The string argument(s) HAS to be
     *            enclosed in quotes. White space that is not enclosed within
     *            quotes will be trimmed. Quote characters in the first and last
     *            positions of any string argument (after being trimmed) will be
     *            removed also. The quote characters used must be the same as
     *            the quote characters used by the current instance of
     *            Evaluator. If there are multiple arguments, they must be
     *            separated by a comma (",").
     * 
     * @return Returns The index at where the substring is found. If the
     *         substring is not found, then -1 is returned.
     * 
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    @Override
    public FunctionResult execute(final Evaluator evaluator, final String arguments) throws FunctionException {
        Integer result = null;
        String exceptionMessage = "Two string arguments and one integer " + "argument are required.";

        ArrayList<Object> values = FunctionHelper.getTwoStringsAndOneInteger(arguments,
                EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);

        if (values.size() != 3) {
            throw new FunctionException(exceptionMessage);
        }

        try {
            String argumentOne = FunctionHelper.trimAndRemoveQuoteChars((String) values.get(0),
                    evaluator.getQuoteCharacter());
            String argumentTwo = FunctionHelper.trimAndRemoveQuoteChars((String) values.get(1),
                    evaluator.getQuoteCharacter());
            int index = ((Integer) values.get(2)).intValue();
            result = new Integer(argumentOne.lastIndexOf(argumentTwo, index));
        } catch (FunctionException fe) {
            throw new FunctionException(fe.getMessage(), fe);
        } catch (Exception e) {
            throw new FunctionException(exceptionMessage, e);
        }

        return new FunctionResult(result.toString(), FunctionConstants.FUNCTION_RESULT_TYPE_NUMERIC);
    }
}