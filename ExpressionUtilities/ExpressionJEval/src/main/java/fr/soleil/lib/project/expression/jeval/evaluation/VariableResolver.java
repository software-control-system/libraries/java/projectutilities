/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation;

import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionException;

/**
 * This interface can be implement with a custom resolver and set onto the
 * Evaluator class. It will then be used to resolve variables when they are
 * replaced in an expression as it gets evaluated. Varaibles resolved by the
 * resolved will override any varibles that exist in the variable map of an
 * Evaluator instance.
 */
public interface VariableResolver {

    /**
     * Returns a variable value for the specified variable name.
     *
     * @param variableName The name of the variable to return the variable value for.
     *
     * @return A variable value for the specified variable name. If the variable name can not be resolved, then
     *         <code>null</code> should be returned.
     * 
     * @throws FunctionException if a problem occurred
     */
    public Object resolveVariable(String variableName) throws FunctionException;
}