/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation;

/**
 * Contains constants used by classes in this package.
 */
public class EvaluationConstants {

    /**
     * The single quote character.
     */
    public static final char SINGLE_QUOTE = '\'';

    /**
     * The double quote character.
     */
    public static final char DOUBLE_QUOTE = '"';

    /**
     * The open brace character.
     */
    public static final char OPEN_BRACE = '{';

    /**
     * The closed brace character.
     */
    public static final char CLOSED_BRACE = '}';

    /**
     * The pound sign character.
     */
    public static final char POUND_SIGN = '#';

    /**
     * The open variable string.
     */
    public static final String OPEN_VARIABLE = String.valueOf(POUND_SIGN) + String.valueOf(OPEN_BRACE);

    /**
     * The closed brace string.
     */
    public static final String CLOSED_VARIABLE = String.valueOf(CLOSED_BRACE);

    /**
     * The true value for a Boolean string.
     */
    public static final String BOOLEAN_STRING_TRUE = "1.0";

    /**
     * The false value for a Boolean string.
     */
    public static final String BOOLEAN_STRING_FALSE = "0.0";

    /**
     * The comma character.
     */
    public static final char COMMA = ',';

    /**
     * The function argument separator.
     */
    public static final char FUNCTION_ARGUMENT_SEPARATOR = COMMA;
}
