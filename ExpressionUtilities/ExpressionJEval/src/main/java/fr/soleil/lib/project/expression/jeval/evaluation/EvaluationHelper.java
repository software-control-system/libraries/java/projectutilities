/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation;

public class EvaluationHelper {

    /**
     * Replaces all old string within the expression with new strings.
     * 
     * @param expression The string being processed.
     * @param oldString The string to replace.
     * @param newString The string to replace the old string with.
     * 
     * @return The new expression with all of the old strings replaced with new strings.
     */
    public static String replaceAll(final String expression, final String oldString, final String newString) {

        String replacedExpression = expression;

        if (replacedExpression != null) {
            int charCtr = 0;
            int oldStringIndex = replacedExpression.indexOf(oldString, charCtr);

            while (oldStringIndex > -1) {
                // Remove the old string from the expression.
                final StringBuilder buffer = new StringBuilder(replacedExpression.substring(0, oldStringIndex)
                        + replacedExpression.substring(oldStringIndex + oldString.length()));

                // Insert the new string into the expression.
                buffer.insert(oldStringIndex, newString);

                replacedExpression = buffer.toString();

                charCtr = oldStringIndex + newString.length();

                // Determine if we need to continue to search.
                if (charCtr < replacedExpression.length()) {
                    oldStringIndex = replacedExpression.indexOf(oldString, charCtr);
                } else {
                    oldStringIndex = -1;
                }
            }
        }

        return replacedExpression;
    }

    /**
     * Determines if a character is a space or white space.
     * 
     * @param character The character being evaluated.
     * 
     * @return True if the character is a space or white space and false if not.
     */
    public static boolean isSpace(final char character) {
        return (character == ' ' || character == '\t' || character == '\n' || character == '\r' || character == '\f');
    }
}
