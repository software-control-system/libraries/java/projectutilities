/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.operator;

import fr.soleil.lib.project.expression.jeval.evaluation.EvaluationConstants;

/**
 * The equal operator.
 */
public class EqualOperator extends AbstractOperator {

    /**
     * Default constructor.
     */
    public EqualOperator() {
        super("==", 3);
    }

    /**
     * Evaluates two double operands.
     * 
     * @param leftOperand
     *            The left operand being evaluated.
     * @param rightOperand
     *            The right operand being evaluated.
     */
    @Override
    public double evaluate(final double leftOperand, final double rightOperand) {
        if (leftOperand == rightOperand) {
            return 1;
        }

        return 0;
    }

    /**
     * Evaluates two string operands.
     * 
     * @param leftOperand
     *            The left operand being evaluated.
     * @param rightOperand
     *            The right operand being evaluated.
     */
    @Override
    public String evaluate(String leftOperand, String rightOperand) {
        if (leftOperand.compareTo(rightOperand) == 0) {
            return EvaluationConstants.BOOLEAN_STRING_TRUE;
        }

        return EvaluationConstants.BOOLEAN_STRING_FALSE;
    }
}