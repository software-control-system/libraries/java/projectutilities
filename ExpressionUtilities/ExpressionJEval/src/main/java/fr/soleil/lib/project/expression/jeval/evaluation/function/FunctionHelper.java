/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.function;

import java.util.ArrayList;

import fr.soleil.lib.project.expression.jeval.evaluation.ArgumentTokenizer;

/**
 * This class contains many methods that are helpful when writing functions.
 * Some of these methods were created to help with the creation of the math and
 * string functions packaged with Evaluator.
 */
public class FunctionHelper {

    /**
     * This method first removes any white space at the beginning and end of the
     * input string. It then removes the specified quote character from the the
     * first and last characters of the string if a quote character exists in
     * those positions. If quote characters are not in the first and last
     * positions after the white space is trimmed, then a FunctionException will
     * be thrown.
     * 
     * @param arguments
     *            The arguments to trim and revove quote characters from.
     * @param quoteCharacter
     *            The character to remove from the first and last position of
     *            the trimmed string.
     * 
     * @return The arguments with white space and quote characters removed.
     * 
     * @exception FunctionException
     *                Thrown if quote characters do not exist in the first and
     *                last positions after the white space is trimmed.
     */
    public static String trimAndRemoveQuoteChars(final String arguments, final char quoteCharacter)
            throws FunctionException {

        String trimmedArgument = arguments;

        trimmedArgument = trimmedArgument.trim();

        if (trimmedArgument.charAt(0) == quoteCharacter) {
            trimmedArgument = trimmedArgument.substring(1, trimmedArgument.length());
        } else {
            throw new FunctionException("Value does not start with a quote.");
        }

        if (trimmedArgument.charAt(trimmedArgument.length() - 1) == quoteCharacter) {
            trimmedArgument = trimmedArgument.substring(0, trimmedArgument.length() - 1);
        } else {
            throw new FunctionException("Value does not end with a quote.");
        }

        return trimmedArgument;
    }

    /**
     * This methods takes a string of input function arguments, evaluates each
     * argument and creates a Double value for each argument from the result of
     * the evaluations.
     * 
     * @param arguments
     *            The arguments to parse.
     * @param delimiter
     *            The delimiter to use while parsing.
     * 
     * @return An array list of Double values found in the input string.
     * 
     * @exception FunctionException
     *                Thrown if the string does not properly parse into Double
     *                values.
     */
    public static ArrayList<Double> getDoubles(final String arguments, final char delimiter) throws FunctionException {

        ArrayList<Double> returnValues = new ArrayList<Double>();

        try {

            final ArgumentTokenizer tokenizer = new ArgumentTokenizer(arguments, delimiter);

            while (tokenizer.hasMoreTokens()) {
                final String token = tokenizer.nextToken().trim();
                returnValues.add(new Double(token));
            }
        } catch (Exception e) {
            throw new FunctionException("Invalid values in string.", e);
        }

        return returnValues;
    }

    /**
     * This methods takes a string of input function arguments, evaluates each
     * argument and creates a String value for each argument from the result of
     * the evaluations.
     * 
     * @param arguments
     *            The arguments of values to parse.
     * @param delimiter
     *            The delimiter to use while parsing.
     * 
     * @return An array list of String values found in the input string.
     * 
     * @exception FunctionException
     *                Thrown if the stirng does not properly parse into String
     *                values.
     */
    public static ArrayList<String> getStrings(final String arguments, final char delimiter) throws FunctionException {

        final ArrayList<String> returnValues = new ArrayList<String>();

        try {
            ArgumentTokenizer tokenizer = new ArgumentTokenizer(arguments, delimiter);

            while (tokenizer.hasMoreTokens()) {
                final String token = tokenizer.nextToken();
                returnValues.add(token);
            }
        } catch (Exception e) {
            throw new FunctionException("Invalid values in string.", e);
        }

        return returnValues;
    }

    /**
     * This methods takes a string of input function arguments, evaluates each
     * argument and creates a one Integer and one String value for each argument
     * from the result of the evaluations.
     * 
     * @param arguments
     *            The arguments of values to parse.
     * @param delimiter
     *            The delimiter to use while parsing.
     * 
     * @return An array list of object values found in the input string.
     * 
     * @exception FunctionException
     *                Thrown if the stirng does not properly parse into the
     *                proper objects.
     */
    public static ArrayList<Object> getOneStringAndOneInteger(final String arguments, final char delimiter)
            throws FunctionException {

        ArrayList<Object> returnValues = new ArrayList<Object>();

        try {
            final ArgumentTokenizer tokenizer = new ArgumentTokenizer(arguments, delimiter);

            int tokenCtr = 0;
            while (tokenizer.hasMoreTokens()) {
                if (tokenCtr == 0) {
                    final String token = tokenizer.nextToken();
                    returnValues.add(token);
                } else if (tokenCtr == 1) {
                    final String token = tokenizer.nextToken().trim();
                    returnValues.add(new Integer(new Double(token).intValue()));
                } else {
                    throw new FunctionException("Invalid values in string.");
                }

                tokenCtr++;
            }
        } catch (Exception e) {
            throw new FunctionException("Invalid values in string.", e);
        }

        return returnValues;
    }

    /**
     * This methods takes a string of input function arguments, evaluates each
     * argument and creates a two Strings and one Integer value for each
     * argument from the result of the evaluations.
     * 
     * @param arguments
     *            The arguments of values to parse.
     * @param delimiter
     *            The delimiter to use while parsing.
     * 
     * @return An array list of object values found in the input string.
     * 
     * @exception FunctionException
     *                Thrown if the stirng does not properly parse into the
     *                proper objects.
     */
    public static ArrayList<Object> getTwoStringsAndOneInteger(final String arguments, final char delimiter)
            throws FunctionException {

        final ArrayList<Object> returnValues = new ArrayList<Object>();

        try {
            final ArgumentTokenizer tokenizer = new ArgumentTokenizer(arguments, delimiter);

            int tokenCtr = 0;
            while (tokenizer.hasMoreTokens()) {
                if (tokenCtr == 0 || tokenCtr == 1) {
                    final String token = tokenizer.nextToken();
                    returnValues.add(token);
                } else if (tokenCtr == 2) {
                    final String token = tokenizer.nextToken().trim();
                    returnValues.add(new Integer(new Double(token).intValue()));
                } else {
                    throw new FunctionException("Invalid values in string.");
                }

                tokenCtr++;
            }
        } catch (Exception e) {
            throw new FunctionException("Invalid values in string.", e);
        }

        return returnValues;
    }

    /**
     * This methods takes a string of input function arguments, evaluates each
     * argument and creates a one String and two Integers value for each
     * argument from the result of the evaluations.
     * 
     * @param arguments
     *            The arguments of values to parse.
     * @param delimiter
     *            The delimiter to use while parsing.
     * 
     * @return An array list of object values found in the input string.
     * 
     * @exception FunctionException
     *                Thrown if the stirng does not properly parse into the
     *                proper objects.
     */
    public static ArrayList<Object> getOneStringAndTwoIntegers(final String arguments, final char delimiter)
            throws FunctionException {

        final ArrayList<Object> returnValues = new ArrayList<Object>();

        try {
            final ArgumentTokenizer tokenizer = new ArgumentTokenizer(arguments, delimiter);

            int tokenCtr = 0;
            while (tokenizer.hasMoreTokens()) {
                if (tokenCtr == 0) {
                    final String token = tokenizer.nextToken().trim();
                    returnValues.add(token);
                } else if (tokenCtr == 1 || tokenCtr == 2) {
                    final String token = tokenizer.nextToken().trim();
                    returnValues.add(new Integer(new Double(token).intValue()));
                } else {
                    throw new FunctionException("Invalid values in string.");
                }

                tokenCtr++;
            }
        } catch (Exception e) {
            throw new FunctionException("Invalid values in string.", e);
        }

        return returnValues;
    }
}
