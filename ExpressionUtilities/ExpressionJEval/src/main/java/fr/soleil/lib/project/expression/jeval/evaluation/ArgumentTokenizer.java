/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation;

import java.util.Enumeration;

import fr.soleil.lib.project.ObjectUtils;

/**
 * This class allow for tokenizer methods to be called on a String of arguments.
 */
public class ArgumentTokenizer implements Enumeration<String> {

    /**
     * The default delimitor.
     */
    public final char defaultDelimiter = EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR;

    // The arguments to be tokenized. This is updated every time the nextToken
    // method is called.
    private String arguments = null;

    // The separator between the arguments.
    private char delimiter = defaultDelimiter;

    /**
     * Constructor that takes a String of arguments and a delimitoer.
     * 
     * @param arguments
     *            The String of srguments to be tokenized.
     * @param delimiter
     *            The argument tokenizer.
     */
    public ArgumentTokenizer(final String arguments, final char delimiter) {
        this.arguments = arguments;
        this.delimiter = delimiter;
    }

    /**
     * Indicates if there are more elements.
     * 
     * @return True if there are more elements and false if not.
     */
    @Override
    public boolean hasMoreElements() {
        return hasMoreTokens();
    }

    /**
     * Indicates if there are more tokens.
     * 
     * @return True if there are more tokens and false if not.
     */
    public boolean hasMoreTokens() {

        if (arguments.length() > 0) {
            return true;
        }

        return false;
    }

    /**
     * Returns the next element.
     * 
     * @return The next element.
     */
    @Override
    public String nextElement() {
        return nextToken();
    }

    /**
     * Returns the next token.
     * 
     * @return The next element.
     */
    public String nextToken() {
        int charCtr = 0;
        int size = arguments.length();
        int parenthesesCtr = 0;
        String returnArgument = null;

        // Loop until we hit the end of the arguments String.
        while (charCtr < size) {
            if (arguments.charAt(charCtr) == '(') {
                parenthesesCtr++;
            } else if (arguments.charAt(charCtr) == ')') {
                parenthesesCtr--;
            } else if (arguments.charAt(charCtr) == delimiter && parenthesesCtr == 0) {

                returnArgument = arguments.substring(0, charCtr);
                arguments = arguments.substring(charCtr + 1);
                break;
            }

            charCtr++;
        }

        if (returnArgument == null) {
            returnArgument = arguments;
            arguments = ObjectUtils.EMPTY_STRING;
        }

        return returnArgument;
    }
}
