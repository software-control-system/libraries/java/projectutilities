/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.operator;

import fr.soleil.lib.project.expression.jeval.evaluation.EvaluationException;

/**
 * This is the standard operator that is the parent to all operators found in
 * expressions.
 */
public abstract class AbstractOperator implements Operator {

    private final String symbol;
    private final int precedence;
    private final boolean unary;

    /**
     * A constructor that takes the operator symbol and precedence as input.
     * 
     * @param symbol
     *            The character(s) that makes up the operator.
     * @param precedence
     *            The precedence level given to this operator.
     */
    public AbstractOperator(final String symbol, final int precedence) {
        this(symbol, precedence, false);
    }

    /**
     * A constructor that takes the operator symbol, precedence, unary indicator
     * and unary precedence as input.
     * 
     * @param symbol The character(s) that makes up the operator.
     * @param precedence The precedence level given to this operator.
     * @param unary Indicates of the operator is a unary operator or not.
     */
    public AbstractOperator(String symbol, int precedence, boolean unary) {
        this.symbol = symbol;
        this.precedence = precedence;
        this.unary = unary;
    }

    /**
     * Evaluates two double operands.
     * 
     * @param leftOperand The left operand being evaluated.
     * @param rightOperand The right operand being evaluated.
     */
    @Override
    public double evaluate(final double leftOperand, final double rightOperand) {
        return 0;
    }

    /**
     * Evaluates two string operands.
     * 
     * @param leftOperand The left operand being evaluated.
     * @param rightOperand The right operand being evaluated.
     * 
     * @return String The value of the evaluated operands.
     * 
     * @exception EvaluateException
     *                Thrown when an error is found while evaluating the
     *                expression.
     */
    @Override
    public String evaluate(final String leftOperand, final String rightOperand) throws EvaluationException {
        throw new EvaluationException("Invalid operation for a string.");
    }

    /**
     * Evaluate one double operand.
     * 
     * @param operand The operand being evaluated.
     */
    @Override
    public double evaluate(double operand) {
        return 0;
    }

    /**
     * Returns the character(s) that makes up the operator.
     * 
     * @return The operator symbol.
     */
    @Override
    public String getSymbol() {
        return symbol;
    }

    /**
     * Returns the precedence given to this operator.
     * 
     * @return The precedecne given to this operator.
     */
    @Override
    public int getPrecedence() {
        return precedence;
    }

    /**
     * Returns the length of the operator symbol.
     * 
     * @return The length of the operator symbol.
     */
    @Override
    public int getLength() {
        return symbol.length();
    }

    /**
     * Returns an indicator of if the operator is unary or not.
     * 
     * @return An indicator of if the operator is unary or not.
     */
    @Override
    public boolean isUnary() {
        return unary;
    }

    /**
     * Determines if this operator is equal to another operator. Equality is determined by comparing the operator symbol
     * of both operators.
     * 
     * @param object The object to compare with this operator.
     * 
     * @return <code>true</code> if the object is equal and <code>false</code> if not.
     */
    @Override
    public boolean equals(final Object object) {
        boolean equals;
        if (object == null) {
            equals = false;
        } else if (object == this) {
            equals = true;
        } else if (object instanceof AbstractOperator) {
            equals = symbol.equals(((AbstractOperator) object).getSymbol());
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0x573;
        int mult = 0xB01;
        code = code * mult + symbol.hashCode();
        return code;
    }

    /**
     * Returns the String representation of this operator, which is the symbol.
     * 
     * @return The operator symbol.
     */
    @Override
    public String toString() {
        return getSymbol();
    }
}