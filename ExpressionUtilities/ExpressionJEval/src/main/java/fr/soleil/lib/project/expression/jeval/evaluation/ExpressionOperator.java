/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation;

import fr.soleil.lib.project.expression.jeval.evaluation.operator.Operator;

/**
 * Represents an operator being processed in the expression.
 */
public class ExpressionOperator {

    // The operator that this object represents.
    private Operator operator = null;

    // The unary operator for this object, if there is one.
    private Operator unaryOperator = null;

    /**
     * Creates a new ExpressionOperator.
     * 
     * @param operator
     *            The operator this object represents.
     * @param unaryOperator
     *            The unary operator for this object.
     */
    public ExpressionOperator(final Operator operator, final Operator unaryOperator) {
        this.operator = operator;
        this.unaryOperator = unaryOperator;
    }

    /**
     * Returns the operator for this object.
     * 
     * @return The operator for this object.
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * Returns the unary operator for this object.
     * 
     * @return The unary operator for this object.
     */
    public Operator getUnaryOperator() {
        return unaryOperator;
    }
}