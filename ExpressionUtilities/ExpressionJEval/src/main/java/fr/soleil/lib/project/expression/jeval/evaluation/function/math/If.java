/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.function.math;

import java.util.ArrayList;

import fr.soleil.lib.project.expression.jeval.evaluation.EvaluationConstants;
import fr.soleil.lib.project.expression.jeval.evaluation.Evaluator;
import fr.soleil.lib.project.expression.jeval.evaluation.function.Function;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionConstants;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionException;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionHelper;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionResult;

/**
 *
 * @author jcorcessin
 */
public class If implements Function {

    /**
     * Returns the name of the function - "if".
     *
     * @return The name of this function class.
     */
    @Override
    public String getName() {
        return "if";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     *
     * @param evaluator An instance of Evaluator.
     * @param arguments A string argument that will be converted to a double
     *            value and evaluated.
     *
     * @return The absolute value of the argument.
     *
     * @exception FunctionException Thrown if the argument(s) are not valid for
     *                this function.
     */
    @Override
    public FunctionResult execute(Evaluator evaluator, String arguments) throws FunctionException {
        Double result = null;

        ArrayList<String> strings = FunctionHelper.getStrings(arguments,
                EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);

        if (strings.size() != 3) {
            throw new FunctionException("Three numeric arguments are required.");
        }

        try {
            String argumentOne = strings.get(0);
            double argumentTwo = Double.valueOf(strings.get(1));
            double argumentThree = Double.valueOf(strings.get(2));
            String evaluation = evaluator.evaluate(argumentOne);
            if ("1.0".equals(evaluation)) {
                result = argumentTwo;
            } else if ("0.0".equals(evaluation)) {
                result = argumentThree;
            } else {
                throw new FunctionException("Bad function evalutation in first argument");
            }
        } catch (Exception e) {
            throw new FunctionException("One expression argument followed by two numeric arguments are required.", e);
        }

        return new FunctionResult(result.toString(), FunctionConstants.FUNCTION_RESULT_TYPE_NUMERIC);
    }
}
