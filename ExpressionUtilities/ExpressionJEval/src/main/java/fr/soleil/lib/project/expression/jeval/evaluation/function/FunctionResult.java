/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.function;

/**
 * This is a wrapper for the result value returned from a function that not only
 * contains the result, but the type. All custom functions must return a
 * FunctionResult.
 */
public class FunctionResult {

    // The value returned from a function call.
    private String result;

    // The type of the result. Can be a numberic or string. Boolean values come
    // back as numeric values.
    private int type;

    /**
     * Constructor.
     * 
     * @param result
     *            The result value.
     * @param type
     *            The result type.
     * 
     * @throws FunctionException
     *             Thrown if result type is invalid.
     */
    public FunctionResult(String result, int type) throws FunctionException {

        if (type < FunctionConstants.FUNCTION_RESULT_TYPE_NUMERIC
                || type > FunctionConstants.FUNCTION_RESULT_TYPE_STRING) {

            throw new FunctionException("Invalid function result type.");
        }

        this.result = result;
        this.type = type;
    }

    /**
     * Returns the result value.
     * 
     * @return The result value.
     */
    public String getResult() {
        return result;
    }

    /**
     * Returns the result type.
     * 
     * @return The result type.
     */
    public int getType() {
        return type;
    }
}
