/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation;

import fr.soleil.lib.project.expression.jeval.evaluation.operator.Operator;

/**
 * Represents the next operator in the expression to process.
 */
class NextOperator {

    // The operator this object represetns.
    private Operator operator = null;

    // The index of the operator in the expression.
    private int index = -1;

    /**
     * Create a new NextOperator from an operator and index.
     * 
     * @param operator
     *            The operator this object represents.
     * @param index
     *            The index of the operator in the expression.
     */
    public NextOperator(final Operator operator, final int index) {
        this.operator = operator;
        this.index = index;
    }

    /**
     * Returns the operator for this object.
     * 
     * @return The operator represented by this object.
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * Returns the index for this object.
     * 
     * @return The index of the operator in the expression.
     */
    public int getIndex() {
        return index;
    }
}