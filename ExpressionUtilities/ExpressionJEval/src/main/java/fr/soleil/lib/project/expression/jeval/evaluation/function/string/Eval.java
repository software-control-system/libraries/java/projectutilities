/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.function.string;

import fr.soleil.lib.project.expression.jeval.evaluation.EvaluationException;
import fr.soleil.lib.project.expression.jeval.evaluation.Evaluator;
import fr.soleil.lib.project.expression.jeval.evaluation.function.Function;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionConstants;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionException;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionResult;

/**
 * This class is a function that executes within Evaluator. The function returns
 * the result of a Evaluator compatible expression. See the
 * Evaluator.evaluate(String) method for a complete description of how this
 * function works.
 */
public class Eval implements Function {
    /**
     * Returns the name of the function - "eval".
     * 
     * @return The name of this function class.
     */
    @Override
    public String getName() {
        return "eval";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     * 
     * @param evaluator
     *            An instance of evaluator.
     * @param arguments
     *            A string expression that is compatible with Evaluator. *** THE
     *            STRING ARGUMENT SHOULD NOT BE ENCLOSED IN QUOTES OR THE
     *            EXPRESSION MAY NOT BE EVALUATED CORRECTLY.*** *** FUNCTION
     *            CALLS ARE VALID WITHIN THE EVAL FUNCTION. ***
     * 
     * @return The evaluated result fot the input expression.
     * 
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    @Override
    public FunctionResult execute(final Evaluator evaluator, final String arguments) throws FunctionException {
        String result = null;

        try {
            result = evaluator.evaluate(arguments, false, true);
        } catch (EvaluationException ee) {
            throw new FunctionException(ee.getMessage(), ee);
        }

        int resultType = FunctionConstants.FUNCTION_RESULT_TYPE_NUMERIC;
        try {
            Double.parseDouble(result);
        } catch (NumberFormatException nfe) {
            resultType = FunctionConstants.FUNCTION_RESULT_TYPE_STRING;
        }

        return new FunctionResult(result, resultType);
    }
}