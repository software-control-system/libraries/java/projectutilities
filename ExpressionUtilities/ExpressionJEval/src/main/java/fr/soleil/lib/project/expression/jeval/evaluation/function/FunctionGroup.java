/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.function;

import java.util.List;

import fr.soleil.lib.project.expression.jeval.evaluation.Evaluator;

/**
 * A groups of functions that can loaded at one time into an instance of
 * Evaluator.
 */
public interface FunctionGroup {
    /**
     * Returns the name of the function group.
     * 
     * @return The name of this function group class.
     */
    public String getName();

    /**
     * Returns a list of the functions that are loaded by this class.
     * 
     * @return A list of the functions loaded by this class.
     */
    public List<Function> getFunctions();

    /**
     * Loads the functions in this function group into an instance of Evaluator.
     * 
     * @param evaluator
     *            An instance of Evaluator to load the functions into.
     */
    public void load(Evaluator evaluator);

    /**
     * Unloads the functions in this function group from an instance of
     * Evaluator.
     * 
     * @param evaluator
     *            An instance of Evaluator to unload the functions from.
     */
    public void unload(Evaluator evaluator);
}