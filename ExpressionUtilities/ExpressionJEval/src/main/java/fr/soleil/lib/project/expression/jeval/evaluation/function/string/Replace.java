/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.function.string;

import java.util.ArrayList;

import fr.soleil.lib.project.expression.jeval.evaluation.EvaluationConstants;
import fr.soleil.lib.project.expression.jeval.evaluation.Evaluator;
import fr.soleil.lib.project.expression.jeval.evaluation.function.Function;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionConstants;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionException;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionHelper;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionResult;

/**
 * This class is a function that executes within Evaluator. The function returns
 * a new string with all of the occurances of the old character in the source
 * string replaced with the new character. See the String.replace(char, char)
 * method in the JDK for a complete description of how this function works.
 */
public class Replace implements Function {
    /**
     * Returns the name of the function - "replace".
     * 
     * @return The name of this function class.
     */
    @Override
    public String getName() {
        return "replace";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     * 
     * @param evaluator
     *            An instance of Evaluator.
     * @param arguments
     *            A string argument that will be converted into one string and
     *            two character arguments. The first argument is the source
     *            string to replace the charactes in. The second argument is the
     *            old character to replace in the source string. The third
     *            argument is the new character to replace the old character
     *            with in the source string. The string and character
     *            argument(s) HAS to be enclosed in quotes. White space that is
     *            not enclosed within quotes will be trimmed. Quote characters
     *            in the first and last positions of any string argument (after
     *            being trimmed) will be removed also. The quote characters used
     *            must be the same as the quote characters used by the current
     *            instance of Evaluator. If there are multiple arguments, they
     *            must be separated by a comma (",").
     * 
     * @return Returns a string with every occurence of the old character
     *         replaced with the new character.
     * 
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    @Override
    public FunctionResult execute(final Evaluator evaluator, final String arguments) throws FunctionException {
        String result = null;
        String exceptionMessage = "One string argument and two character " + "arguments are required.";

        ArrayList<String> values = FunctionHelper.getStrings(arguments,
                EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);

        if (values.size() != 3) {
            throw new FunctionException(exceptionMessage);
        }

        try {
            String argumentOne = FunctionHelper.trimAndRemoveQuoteChars(values.get(0), evaluator.getQuoteCharacter());

            String argumentTwo = FunctionHelper.trimAndRemoveQuoteChars(values.get(1), evaluator.getQuoteCharacter());

            String argumentThree = FunctionHelper.trimAndRemoveQuoteChars(values.get(2), evaluator.getQuoteCharacter());

            char oldCharacter = ' ';
            if (argumentTwo.length() == 1) {
                oldCharacter = argumentTwo.charAt(0);
            } else {
                throw new FunctionException(exceptionMessage);
            }

            char newCharacter = ' ';
            if (argumentThree.length() == 1) {
                newCharacter = argumentThree.charAt(0);
            } else {
                throw new FunctionException(exceptionMessage);
            }

            result = argumentOne.replace(oldCharacter, newCharacter);
        } catch (FunctionException fe) {
            throw new FunctionException(fe.getMessage(), fe);
        } catch (Exception e) {
            throw new FunctionException(exceptionMessage, e);
        }

        return new FunctionResult(result, FunctionConstants.FUNCTION_RESULT_TYPE_STRING);
    }
}