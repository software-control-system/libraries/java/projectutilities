/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.function.math;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.soleil.lib.project.expression.jeval.evaluation.Evaluator;
import fr.soleil.lib.project.expression.jeval.evaluation.function.Function;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionGroup;

/**
 * A groups of functions that can loaded at one time into an instance of
 * Evaluator. This group contains all of the functions located in the
 * fr.synchrotron.soleileval.function.math package.
 */
public class MathFunctions implements FunctionGroup {
    /**
     * Used to store instances of all of the functions loaded by this class.
     */
    private final List<Function> functions = new ArrayList<Function>();

    /**
     * Default contructor for this class. The functions loaded by this class are
     * instantiated in this constructor.
     */
    public MathFunctions() {
        functions.add(new Cos());
        functions.add(new Sin());
        functions.add(new Tan());

        functions.add(new Acos());
        functions.add(new Asin());
        functions.add(new Atan());
        functions.add(new Atan2());

        functions.add(new Cosh());
        functions.add(new Sinh());
        functions.add(new Tanh());

        functions.add(new Acosh());
        functions.add(new Asinh());
        functions.add(new Atanh());
        functions.add(new Ceil());

        functions.add(new Ln());
        functions.add(new Log());
        functions.add(new Exp());

        functions.add(new Floor());
        functions.add(new IEEEremainder());

        functions.add(new Sqrt());
        functions.add(new Mod());
        functions.add(new Abs());
        functions.add(new Max());
        functions.add(new Min());
        functions.add(new Pow());
        functions.add(new Random());
        functions.add(new Rint());
        functions.add(new Round());
        functions.add(new ToDegrees());
        functions.add(new ToRadians());
        functions.add(new Sum());
        functions.add(new If());

    }

    /**
     * Returns the name of the function group - "numberFunctions".
     * 
     * @return The name of this function group class.
     */
    @Override
    public String getName() {
        return "numberFunctions";
    }

    /**
     * Returns a list of the functions that are loaded by this class.
     * 
     * @return A list of the functions loaded by this class.
     */
    @Override
    public List<Function> getFunctions() {
        return functions;
    }

    /**
     * Loads the functions in this function group into an instance of Evaluator.
     * 
     * @param evaluator
     *            An instance of Evaluator to load the functions into.
     */
    @Override
    public void load(final Evaluator evaluator) {
        Iterator<Function> functionIterator = functions.iterator();

        while (functionIterator.hasNext()) {
            evaluator.putFunction(functionIterator.next());
        }
    }

    /**
     * Unloads the functions in this function group from an instance of
     * Evaluator.
     * 
     * @param evaluator
     *            An instance of Evaluator to unload the functions from.
     */
    @Override
    public void unload(final Evaluator evaluator) {
        Iterator<Function> functionIterator = functions.iterator();

        while (functionIterator.hasNext()) {
            evaluator.removeFunction(functionIterator.next().getName());
        }
    }
}
