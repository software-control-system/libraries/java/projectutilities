/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.function;

import fr.soleil.lib.project.expression.jeval.evaluation.Evaluator;

/**
 * A function that can be specified in an expression.
 */
public interface Function {

    /**
     * Returns the name of the function.
     * 
     * @return The name of this function class.
     */
    public String getName();

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     * 
     * @param evaluator
     *            An instance of Evaluator.
     * @param arguments
     *            The arguments that will be evaluated by the function. It is up
     *            to the function implementation to break the string into one or
     *            more arguments.
     * 
     * @return The value of the evaluated argument and its type.
     * 
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    public FunctionResult execute(Evaluator evaluator, String arguments) throws FunctionException;
}