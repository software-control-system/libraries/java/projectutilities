/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.expression.jeval;

import java.util.Map;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.expression.IExpressionParser;
import fr.soleil.lib.project.expression.exception.ExpressionEvaluationException;
import fr.soleil.lib.project.expression.jeval.evaluation.Evaluator;

public class JEvalExpressionParser implements IExpressionParser {
    protected static final String EULER = "Euler";
    protected static final String E = "E";
    protected static final String E_POWER = "E^";
    protected static final String E_POWER_LOWER = "e^";
    protected static final String X = "x";
    protected static final String EXP_TRANSFORMED = "e#{x}p";
    protected static final String EXP = "exp";
    protected static final String VARIABLE_START = "#{";
    protected static final String VARIABLE_END = "}";

    protected boolean variableChanged;
    protected String lastExpression;
    protected String lastAdaptedExpression;

    protected final Evaluator evaluator;

    public JEvalExpressionParser() {
        evaluator = new Evaluator('"', true, true, true, true);
        variableChanged = true;
    }

    @Override
    public void addVariable(String name, Object value) {
        boolean changed = evaluator.hasVariable(name);
        evaluator.putVariable(name, value);
        variableChanged = changed;
    }

    @Override
    public Object getVariableValue(String name) {
        Object value;
        try {
            value = evaluator.getVariableValue(name);
        } catch (Exception e) {
            value = null;
        }
        return value;
    }

    @Override
    public void removeVariable(String name) {
        boolean changed = evaluator.hasVariable(name);
        evaluator.removeVariable(name);
        variableChanged = changed;
    }

    protected String adaptExpression(String expression) {
        String adapted = expression;
        if (variableChanged || (!ObjectUtils.sameObject(expression, lastExpression))) {
            if (expression != null) {
                adapted = adapted.replace(EXP, E_POWER).replace(EULER, E).replace(E_POWER_LOWER, E_POWER);
                Map<String, Object> variables = evaluator.getVariables();
                if (variables != null) {
                    for (String variable : variables.keySet()) {
                        adapted = adapted.replace(variable, VARIABLE_START + variable + VARIABLE_END);
                        if (X.equals(variable)) {
                            adapted = adapted.replace(EXP_TRANSFORMED, EXP);
                        }
                    }
                }
            }
            variableChanged = false;
            lastExpression = expression;
            lastAdaptedExpression = adapted;
        } else {
            adapted = lastAdaptedExpression;
        }
        return adapted;
    }

    @Override
    public double parseExpression(String expression) throws ExpressionEvaluationException {
        try {
            return evaluator.getNumberResult(adaptExpression(expression));
        } catch (Exception e) {
            throw new ExpressionEvaluationException(e.getMessage(), e);
        }
    }

    @Override
    public void clean() {
        evaluator.clearVariables();
    }

}
