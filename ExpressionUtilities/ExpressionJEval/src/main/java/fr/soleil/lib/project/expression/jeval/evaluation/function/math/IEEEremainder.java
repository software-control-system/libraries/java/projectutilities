/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation.function.math;

import java.util.ArrayList;

import fr.soleil.lib.project.expression.jeval.evaluation.EvaluationConstants;
import fr.soleil.lib.project.expression.jeval.evaluation.Evaluator;
import fr.soleil.lib.project.expression.jeval.evaluation.function.Function;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionConstants;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionException;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionHelper;
import fr.soleil.lib.project.expression.jeval.evaluation.function.FunctionResult;

/**
 * This class is a function that executes within Evaluator. The function returns
 * the remainder operation on two arguments as prescribed by the IEEE 754
 * standard. See the Math.IEEERemainder(double, double) method in the JDK for a
 * complete description of how this function works.
 */
public class IEEEremainder implements Function {
    /**
     * Returns the name of the function - "IEEEremainder".
     * 
     * @return The name of this function class.
     */
    @Override
    public String getName() {
        return "IEEEremainder";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     * 
     * @param evaluator
     *            An instance of Evaluator.
     * @param arguments
     *            A string argument that will be converted into two double
     *            values and evaluated.
     * 
     * @return The the remainder operation on two arguments as prescribed by the
     *         IEEE 754 standard.
     * 
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    @Override
    public FunctionResult execute(final Evaluator evaluator, final String arguments) throws FunctionException {
        Double result = null;

        ArrayList<Double> numbers = FunctionHelper.getDoubles(arguments,
                EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);

        if (numbers.size() != 2) {
            throw new FunctionException("Two numeric arguments are required.");
        }

        try {
            double argumentOne = numbers.get(0).doubleValue();
            double argumentTwo = numbers.get(1).doubleValue();
            result = new Double(Math.IEEEremainder(argumentOne, argumentTwo));
        } catch (Exception e) {
            throw new FunctionException("Two numeric arguments are required.", e);
        }

        return new FunctionResult(result.toString(), FunctionConstants.FUNCTION_RESULT_TYPE_NUMERIC);
    }
}