/*
 * This file is part of ExpressionJEval.
 * 
 * ExpressionJEval is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ExpressionJEval is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ExpressionJEval. If not,
 * see
 * <https://www.gnu.org/licenses/>.
 * 
 * Original file, written by Robert Breidecker (rbreidec@gmail.com), was licensed under Apache License, Version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */
package fr.soleil.lib.project.expression.jeval.evaluation;

import fr.soleil.lib.project.expression.jeval.evaluation.function.Function;
import fr.soleil.lib.project.expression.jeval.evaluation.operator.Operator;

/**
 * This class represents a function that has been parsed.
 */
public class ParsedFunction {

    // The function that has been parsed.
    // FIXME Make all class instance methods final if possible.
    private final Function function;

    // The arguments to the function.
    private final String arguments;

    // The unary operator for this object, if there is one.
    private final Operator unaryOperator;

    /**
     * The constructor for this class.
     * 
     * @param function
     *            The function that has been parsed.
     * @param arguments
     *            The arguments to the function.
     * @param unaryOperator
     *            The unary operator for this object, if there is one.
     */
    public ParsedFunction(final Function function, final String arguments, final Operator unaryOperator) {

        this.function = function;
        this.arguments = arguments;
        this.unaryOperator = unaryOperator;
    }

    /**
     * Returns the function that has been parsed.
     * 
     * @return The function that has been parsed.
     */
    public Function getFunction() {
        return function;
    }

    /**
     * Returns the arguments to the function.
     * 
     * @return The arguments to the function.
     */
    public String getArguments() {
        return arguments;
    }

    /**
     * Returns the unary operator for this object, if there is one.
     * 
     * @return The unary operator for this object, if there is one.
     */
    public Operator getUnaryOperator() {
        return unaryOperator;
    }
}