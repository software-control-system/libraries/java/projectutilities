/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.panel;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import fr.soleil.lib.project.application.user.manager.AccountManager;
import fr.soleil.lib.project.application.user.ui.action.AccountCreationBrowseAction;
import fr.soleil.lib.project.application.user.ui.action.AccountCreationCancelAction;
import fr.soleil.lib.project.application.user.ui.action.AccountCreationValidateAction;

/**
 * This is the main panel of the account creation dialog
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AccountCreationPanel extends JPanel {

    private static final long serialVersionUID = 2795620061949057963L;

    private final JLabel nameLabel, pathLabel;
    private final JTextField nameField, pathField;
    private final JButton browseButton, okButton, cancelButton;
    private final JPanel actionPanel;

    public AccountCreationPanel(AccountManager accountManager) {
        super(new GridBagLayout());
        Insets leftInsets = new Insets(5, 5, 0, 0);
        Insets rightInsets = new Insets(5, 5, 0, 5);
        Insets bottomInsets = new Insets(15, 5, 5, 5);
        nameLabel = new JLabel(accountManager.getMessage("user.manager.Account.Name"), SwingConstants.RIGHT);
        nameField = new JTextField(14);
        pathLabel = new JLabel(accountManager.getMessage("user.manager.Account.Path"), SwingConstants.RIGHT);
        pathField = new JTextField(14);
        browseButton = new JButton(new AccountCreationBrowseAction(accountManager));
        browseButton.setMargin(new Insets(0, 0, 0, 0));

        actionPanel = new JPanel(new GridBagLayout());
        okButton = new JButton(new AccountCreationValidateAction(accountManager));
        okButton.setMargin(new Insets(0, 0, 0, 0));
        cancelButton = new JButton(new AccountCreationCancelAction(accountManager));
        cancelButton.setMargin(new Insets(0, 0, 0, 0));

        // Line 1
        GridBagConstraints nameLabelConstraints = new GridBagConstraints();
        nameLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        nameLabelConstraints.gridx = 0;
        nameLabelConstraints.gridy = 0;
        nameLabelConstraints.weightx = 0;
        nameLabelConstraints.weighty = 0;
        nameLabelConstraints.insets = leftInsets;
        this.add(nameLabel, nameLabelConstraints);
        GridBagConstraints nameFieldConstraints = new GridBagConstraints();
        nameFieldConstraints.fill = GridBagConstraints.HORIZONTAL;
        nameFieldConstraints.gridx = 1;
        nameFieldConstraints.gridy = 0;
        nameFieldConstraints.weightx = 1;
        nameFieldConstraints.weighty = 0;
        nameFieldConstraints.insets = leftInsets;
        this.add(nameField, nameFieldConstraints);

        // Line 2
        GridBagConstraints pathLabelConstraints = new GridBagConstraints();
        pathLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        pathLabelConstraints.gridx = 0;
        pathLabelConstraints.gridy = 1;
        pathLabelConstraints.weightx = 0;
        pathLabelConstraints.weighty = 0;
        pathLabelConstraints.insets = leftInsets;
        this.add(pathLabel, pathLabelConstraints);
        GridBagConstraints pathFieldConstraints = new GridBagConstraints();
        pathFieldConstraints.fill = GridBagConstraints.HORIZONTAL;
        pathFieldConstraints.gridx = 1;
        pathFieldConstraints.gridy = 1;
        pathFieldConstraints.weightx = 1;
        pathFieldConstraints.weighty = 0;
        pathFieldConstraints.insets = leftInsets;
        this.add(pathField, pathFieldConstraints);
        GridBagConstraints browseButtonConstraints = new GridBagConstraints();
        browseButtonConstraints.fill = GridBagConstraints.NONE;
        browseButtonConstraints.gridx = 2;
        browseButtonConstraints.gridy = 1;
        browseButtonConstraints.weightx = 0;
        browseButtonConstraints.weighty = 0;
        browseButtonConstraints.insets = rightInsets;
        this.add(browseButton, browseButtonConstraints);

        // Line 3
        GridBagConstraints actionPanelConstraints = new GridBagConstraints();
        actionPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        actionPanelConstraints.gridx = 0;
        actionPanelConstraints.gridy = 2;
        actionPanelConstraints.weightx = 1;
        actionPanelConstraints.weighty = 0;
        actionPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        this.add(actionPanel, actionPanelConstraints);
        GridBagConstraints okButtonConstraints = new GridBagConstraints();
        okButtonConstraints.fill = GridBagConstraints.NONE;
        okButtonConstraints.gridx = 0;
        okButtonConstraints.gridy = 0;
        okButtonConstraints.weightx = 1;
        okButtonConstraints.weighty = 0;
        okButtonConstraints.anchor = GridBagConstraints.WEST;
        okButtonConstraints.insets = bottomInsets;
        actionPanel.add(okButton, okButtonConstraints);
        GridBagConstraints cancelButtonConstraints = new GridBagConstraints();
        cancelButtonConstraints.fill = GridBagConstraints.NONE;
        cancelButtonConstraints.gridx = 1;
        cancelButtonConstraints.gridy = 0;
        cancelButtonConstraints.weightx = 1;
        cancelButtonConstraints.weighty = 0;
        cancelButtonConstraints.insets = bottomInsets;
        cancelButtonConstraints.anchor = GridBagConstraints.EAST;
        actionPanel.add(cancelButton, cancelButtonConstraints);
    }

    /**
     * Gets the Path entered for the new profile
     * 
     * @return The Path entered for the new profile
     */
    public String getPath() {
        return pathField.getText();
    }

    /**
     * Sets the text of the "path" TextField
     */
    public void setPath(String path) {
        pathField.setText(path);
    }

    /**
     * Gets the Name entered for the new profile
     * 
     * @return The Name entered for the new profile
     */
    @Override
    public String getName() {
        return nameField.getText();
    }

    public void setGlobalBackground(Color bg) {
        setBackground(bg);
        actionPanel.setBackground(bg);
        browseButton.setBackground(bg);
        cancelButton.setBackground(bg);
        okButton.setBackground(bg);
        nameLabel.setBackground(bg);
        pathLabel.setBackground(bg);
    }

}
