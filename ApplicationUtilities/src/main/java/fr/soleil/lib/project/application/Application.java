/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Locale;
import java.util.prefs.Preferences;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.Splash;
import fr.soleil.lib.project.swing.frame.FullScreenFrame;
import fr.soleil.lib.project.swing.panel.ImagePanel;

/**
 * Template for application
 * 
 * @author HARDION
 * @version $Revision: 1.2 $
 */
public abstract class Application extends FullScreenFrame {

    private static final long serialVersionUID = 6160764574607958515L;

    private static MessageManager messageManager;
    protected static final String STATE = "state";
    protected static final String H = "h";
    protected static final String W = "w";
    protected static final String Y = "y";
    protected static final String X = "x";
    protected static final int DEFAULT_WINDOW_X = 0;
    protected static final int DEFAULT_WINDOW_Y = 0;
    protected static final int DEFAULT_WINDOW_WIDTH = 1024;
    protected static final int DEFAULT_WINDOW_HEIGHT = 768;

    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenu editMenu;
    private JMenu helpMenu;
    private JMenuItem exitMenuItem;
    private JMenuItem aboutMenuItem;
    private JMenuItem saveMenuItem;
    private JDialog aboutDialog;
    private final Splash splash;

    protected final Preferences prefs;

    private JMenuItem preferenceMenuItem;

    private int defaultCloseOperation;

    /**
     * Creates a new {@link Application}.
     */
    public Application() {
        this(null, null, true);
    }

    /**
     * Creates a new {@link Application}, setting its splash icon.
     * 
     * @param imageIcon The splash icon.
     */
    public Application(ImageIcon imageIcon) {
        this(imageIcon, null, true);
    }

    /**
     * Creates a new {@link Application}, setting its splash icon.
     * 
     * @param imageIcon The splash icon.
     * @param initialize Whether {@link #initialize()} should be called in this constructor.
     * @param initialize Whether to immediately call {@link #initialize()} method.
     */
    protected Application(ImageIcon imageIcon, boolean initialize) {
        this(imageIcon, null, initialize);
    }

    /**
     * Creates a new {@link Application}, setting its splash and splash icon.
     * 
     * @param imageIcon The splash icon.
     * @param splash The splash.
     * @param initialize Whether to immediately call {@link #initialize()} method.
     */
    protected Application(ImageIcon imageIcon, Splash splash, boolean initialize) {
        super();
        messageManager = getMessageManager(getClass().getClassLoader());
        prefs = recoverPreferences();
        if (splash == null) {
            ImageIcon icon = imageIcon;
            if (icon == null) {
                icon = getSplashImage();
            }
            Splash tmp = new Splash(icon, getSplashForeground());
            setupSplash(tmp);
            this.splash = tmp;
        } else {
            this.splash = splash;
        }
        if (initialize) {
            initialize();
        }
    }

    protected void setupSplash(Splash splash) {
        splash.initProgress();
        splash.setMaxProgress(getMaxProgress());
        splash.setTitle(getApplicationTitle());
        splash.setCopyright(getCopyright());
        splash.progress(0);
        splash.setMessage(messageManager.getMessage("Application.progressMessage0"));
    }

    protected Preferences recoverPreferences() {
        return Preferences.userNodeForPackage(getClass());
    }

    public static MessageManager getMessageManager(ClassLoader classLoader) {
        return getMessageManager(classLoader, "fr.soleil.lib.project.application");
    }

    public static MessageManager getMessageManager(ClassLoader classLoader, String packageName) {
        if (messageManager == null) {
            messageManager = new MessageManager(packageName, classLoader);
        }
        if (!messageManager.isResourceBundleInitialised()) {
            messageManager.initResourceBundle(Locale.getDefault());
            if (!messageManager.isResourceBundleInitialised()) {
                messageManager.initResourceBundle(Locale.US);
            }
        }
        return messageManager;
    }

    protected Color getSplashForeground() {
        return null;
    }

    protected abstract ImageIcon getSplashImage();

    protected ImageIcon getAboutDialogBackgroundImage() {
        return getSplashImage();
    }

    protected abstract Image getDefaultIconImage();

    protected abstract String getApplicationTitle();

    protected abstract String getCopyright();

    protected int getMaxProgress() {
        return 5;
    }

    protected int getProgressAfterPreferencesLoading() {
        return 2;
    }

    protected int getSetVisibleProgress() {
        return 4;
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    protected final void initialize() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Image image = getDefaultIconImage();
        if (image != null) {
            this.setIconImage(image);
        }
        setSize(new Dimension(871, 674));
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                quit();
            }
        });
        initJMenuBar();
        setTitle(getApplicationTitle());
        loadPreferences();
        getSplash().progress(getProgressAfterPreferencesLoading());
        getSplash().setMessage(messageManager.getMessage("Application.progressMessage2"));
    }

    @Override
    public void setDefaultCloseOperation(int operation) {
        this.defaultCloseOperation = operation;
        super.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    /**
     * This method initializes splash
     * 
     * @return {@link Splash}
     */
    public Splash getSplash() {
        return splash;
    }

    /**
     * This method initializes preferenceMenuItem
     * 
     * @return JMenuItem
     */
    protected JMenuItem getPreferenceMenuItem() {
        if (preferenceMenuItem == null) {
            preferenceMenuItem = new JMenuItem();
        }
        return preferenceMenuItem;
    }

    protected void addMenu(JMenuBar menuBar, JMenu menu) {
        if (menu != null) {
            menuBar.add(menu);
        }
    }

    protected void initJMenuBar() {
        if (menuBar == null) {
            menuBar = new JMenuBar();
            setJMenuBar(menuBar);
            addMenu(menuBar, getFileMenu());
            addMenu(menuBar, getEditMenu());
            addMenu(menuBar, getHelpMenu());
        }
    }

    /**
     * This method initializes jMenu
     * 
     * @return JMenu
     */
    protected JMenu getFileMenu() {
        if (fileMenu == null) {
            fileMenu = new JMenu();
            fileMenu.setText("File");
            fileMenu.add(getSaveMenuItem());
            fileMenu.add(getExitMenuItem());
        }
        return fileMenu;
    }

    /**
     * This method initializes jMenu
     * 
     * @return JMenu
     */
    protected JMenu getEditMenu() {
        if (editMenu == null) {
            editMenu = new JMenu();
            editMenu.setText("Edit");
            editMenu.add(getPreferenceMenuItem());
        }
        return editMenu;
    }

    /**
     * This method initializes jMenu
     * 
     * @return JMenu
     */
    protected JMenu getHelpMenu() {
        if (helpMenu == null) {
            helpMenu = new JMenu();
            helpMenu.setText("Help");
            helpMenu.add(getAboutMenuItem());
        }
        return helpMenu;
    }

    /**
     * This method initializes jMenuItem
     * 
     * @return JMenuItem
     */
    protected JMenuItem getExitMenuItem() {
        if (exitMenuItem == null) {
            exitMenuItem = new JMenuItem();
            exitMenuItem.setText("Exit");
            exitMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    processWindowEvent(new WindowEvent(Application.this, WindowEvent.WINDOW_CLOSING));
                }
            });
        }
        return exitMenuItem;
    }

    /**
     * This method initializes jMenuItem
     * 
     * @return JMenuItem
     */
    protected JMenuItem getAboutMenuItem() {
        if (aboutMenuItem == null) {
            aboutMenuItem = new JMenuItem();
            aboutMenuItem.setText("About");
            aboutMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    openAboutDialog();
                }
            });
        }
        return aboutMenuItem;
    }

    protected void openAboutDialog() {
        JDialog aboutDialog = getAboutDialog();
        if (aboutDialog != null) {
            aboutDialog.setVisible(true);
        }
    }

    /**
     * This method initializes "Save" {@link JMenuItem}
     * 
     * @return {@link JMenuItem}
     */
    protected JMenuItem getSaveMenuItem() {
        if (saveMenuItem == null) {
            saveMenuItem = new JMenuItem();
            saveMenuItem.setText("Save");
            saveMenuItem.setAccelerator(
                    KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.Event.CTRL_MASK, true));
        }
        return saveMenuItem;
    }

    protected JDialog generateAboutDialog() {
        JDialog aboutDialog = new JDialog(this, "About", true);
        aboutDialog.setResizable(false);
        ImagePanel panel = new ImagePanel(new GridBagLayout());
        ImageIcon icon = getAboutDialogBackgroundImage();
        panel.setBackgroundImage(icon);
        panel.setImageAlpha(1);
        JLabel titleLabel = new JLabel(getApplicationTitle());
        titleLabel.setFont(new Font(Font.DIALOG, Font.BOLD, 20));
        GridBagConstraints titleLabelConstraints = new GridBagConstraints();
        titleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        titleLabelConstraints.gridx = 0;
        titleLabelConstraints.gridy = 1;
        titleLabelConstraints.weightx = 1;
        titleLabelConstraints.weighty = 0;
        titleLabelConstraints.anchor = GridBagConstraints.WEST;
        panel.add(titleLabel, titleLabelConstraints);
        String copyright = getCopyright();
        if ((copyright != null) && !copyright.trim().isEmpty()) {
            copyright = "\u00A9 " + copyright;
            JLabel copyrightLabel = new JLabel(copyright);
            copyrightLabel.setFont(new Font(Font.DIALOG, Font.BOLD, 12));
            GridBagConstraints copyrightLabelConstraints = new GridBagConstraints();
            copyrightLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
            copyrightLabelConstraints.gridx = 0;
            copyrightLabelConstraints.gridy = 2;
            copyrightLabelConstraints.weightx = 1;
            copyrightLabelConstraints.weighty = 0;
            copyrightLabelConstraints.anchor = GridBagConstraints.WEST;
            panel.add(copyrightLabel, copyrightLabelConstraints);
        }
        if (icon != null) {
            GridBagConstraints pushConstraints = new GridBagConstraints();
            pushConstraints.fill = GridBagConstraints.BOTH;
            pushConstraints.gridx = 0;
            pushConstraints.gridy = 0;
            pushConstraints.weightx = 1;
            pushConstraints.weighty = 1;
            panel.add(Box.createGlue(), pushConstraints);
            Dimension panelSize = panel.getPreferredSize();
            int width = icon.getIconWidth(), height = icon.getIconHeight();
            if ((panelSize.width < width) || (panelSize.height < height)) {
                panel.setPreferredSize(
                        new Dimension(Math.max(panelSize.width, width), Math.max(panelSize.height, height)));
            }
        }
        aboutDialog.setContentPane(panel);
        aboutDialog.pack();
        return aboutDialog;
    }

    public final JDialog getAboutDialog() {
        if (aboutDialog == null) {
            aboutDialog = generateAboutDialog();
        }
        return aboutDialog;
    }

    public void setAboutDialog(JDialog aboutDialog) {
        this.aboutDialog = aboutDialog;
    }

    @Override
    public void setVisible(boolean arg0) {
        if (arg0 == true) {
            getSplash().progress(getSetVisibleProgress());
            getSplash().setMessage(ObjectUtils.EMPTY_STRING);
            getSplash().setVisible(false);
        }
        super.setVisible(arg0);
    }

    protected void savePreferences() {
        prefs.putInt(STATE, getExtendedState());
        setExtendedState(Frame.NORMAL);
        Rectangle bounds = getBounds();
        prefs.putInt(X, (int) bounds.getX());
        prefs.putInt(Y, (int) bounds.getY());
        prefs.putInt(W, (int) bounds.getWidth());
        prefs.putInt(H, (int) bounds.getHeight());

    }

    protected void loadPreferences() {
        int x = prefs.getInt(X, DEFAULT_WINDOW_X);
        int y = prefs.getInt(Y, DEFAULT_WINDOW_Y);
        setLocation(x, y);
        int w = prefs.getInt(W, DEFAULT_WINDOW_WIDTH);
        int h = prefs.getInt(H, DEFAULT_WINDOW_HEIGHT);
        setSize(w, h);
        int state = prefs.getInt(STATE, Frame.NORMAL);
        setExtendedState(state);
    }

    /**
     * Quits application after confirmation asked.
     */
    public boolean quit() {
        boolean quit;
        int confirm = JOptionPane.showConfirmDialog(this, messageManager.getMessage("Application.exitMessage"),
                getApplicationTitle(), JOptionPane.YES_NO_OPTION);
        if (confirm == JOptionPane.YES_OPTION) {
            quit = true;
            doQuit();
        } else {
            quit = false;
        }
        return quit;
    }

    /**
     * Quits application, without asking for confirmation
     */
    protected void doQuit() {
        Application.this.savePreferences();
        switch (defaultCloseOperation) {
            case HIDE_ON_CLOSE:
                setVisible(false);
                break;
            case DISPOSE_ON_CLOSE:
                setVisible(false);
                dispose();
                break;
            case EXIT_ON_CLOSE:
                setVisible(false);
                dispose();
                System.exit(0);
                break;
            default:
                // nothing to do
                break;
        }
    }

}
