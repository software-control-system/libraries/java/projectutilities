/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.logging;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

/**
 * An {@link AppenderBase} that can be used in logback configuration files to redirect all logs to its associated
 * {@link LogViewer}.
 * <p>
 * To use its associated {@link LogViewer}, call {@link #getLogViewerInstance(String, boolean)}.
 * </p>
 * <p>
 * <table border=1 style="border-collapse: collapse;">
 * <tr>
 * <th>Logback configuration file example:</th>
 * </tr>
 * <tr>
 * <td>
 * 
 * <pre>
 * &lt;appender name="PANEL" class="fr.soleil.lib.project.application.logging.LogManager"&gt;
 * &lt;/appender&gt;
 * 
 * &lt;root level="TRACE"&gt;
 *   &lt;appender-ref ref="PANEL"/&gt;
 * &lt;/root&gt;
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LogManager extends AppenderBase<ILoggingEvent> {

    private static LogViewer logViewerInstance;
    private static LogAppender logAppender;
    private static final Collection<ILoggingEvent> ACCUMULATED_EVENTS = new LinkedHashSet<>();

    /**
     * Returns associated {@link LogViewer}, creating it with given application id if not yet created.
     * <p>
     * If that {@link LogViewer} was already created, you can call {@link #getLogViewerInstance()} instead.
     * </p>
     * 
     * @param applicationId The application id that can be used to create {@link LogViewer} if not yet created.
     * @param stackTraces Whether errors stack traces should be displayed in {@link LogViewer}.
     * @return A {@link LogViewer}, never <code>null</code>.
     * @see #getLogViewerInstance()
     */
    public static LogViewer getLogViewerInstance(String applicationId, boolean printErrorsStackTraces) {
        if ((logViewerInstance == null) && (applicationId != null) && (!applicationId.isEmpty())) {
            Collection<ILoggingEvent> events;
            synchronized (LogManager.class) {
                if (logViewerInstance == null) {
                    logViewerInstance = new LogViewer(applicationId);
                    logViewerInstance.setAutoScrollMagnetism(true);
                    logAppender = new LogAppender(logViewerInstance);
                    events = new ArrayList<>(ACCUMULATED_EVENTS);
                    ACCUMULATED_EVENTS.clear();
                } else {
                    events = null;
                }
            }
            if ((events != null) && !events.isEmpty()) {
                for (ILoggingEvent event : events) {
                    logAppender.doAppend(event);
                }
                events.clear();
            }
        }
        return logViewerInstance;
    }

    /**
     * Returns the associated {@link LogViewer}.
     * 
     * @return A {@link LogViewer}. <code>null</code> if not yet created.
     * @see #getLogViewerInstance(String, boolean)
     */
    public static LogViewer getLogViewerInstance() {
        return logViewerInstance;
    }

    /**
     * {@link LogManager} default constructor, useful for logback only.
     * <p>
     * Don't call it in your application. Call {@link #getLogViewerInstance(String, boolean)} instead.
     * </p>
     */
    public LogManager() {
        super();
    }

    @Override
    protected void append(ILoggingEvent eventObject) {
        if (logAppender == null) {
            synchronized (LogManager.class) {
                if (logAppender == null) {
                    ACCUMULATED_EVENTS.add(eventObject);
                }
            }
        }
        if (logAppender != null) {
            logAppender.doAppend(eventObject);
        }
    }
}
