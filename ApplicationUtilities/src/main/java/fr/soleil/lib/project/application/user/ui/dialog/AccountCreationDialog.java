/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.dialog;

import java.awt.Color;

import javax.swing.JDialog;

import fr.soleil.lib.project.application.user.Account;
import fr.soleil.lib.project.application.user.manager.AccountManager;
import fr.soleil.lib.project.application.user.ui.panel.AccountCreationPanel;

/**
 * A JDialog used to create new {@link Account}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AccountCreationDialog extends JDialog {

    private static final long serialVersionUID = -5854468865960302326L;

    private final AccountCreationPanel accountCreationPanel;

    /**
     * Constructor
     * 
     * @param manager
     *            the {@link AccountManager} to inform for new accounts
     */
    public AccountCreationDialog(AccountManager manager) {
        super(manager.getAccountSelectionDialog(), manager.getMessage("user.manager.Account.Title") + ": "
                + manager.getMessage("user.manager.Account.New"), true);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        accountCreationPanel = new AccountCreationPanel(manager);
        this.setContentPane(accountCreationPanel);
        pack();
    }

    public AccountCreationPanel getAccountCreationPanel() {
        return accountCreationPanel;
    }

    public void setGlobalBackground(Color bg) {
        setBackground(bg);
        accountCreationPanel.setGlobalBackground(bg);
    }

}
