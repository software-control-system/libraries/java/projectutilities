/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.logging;

import javax.swing.ImageIcon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.icons.Icons;
import fr.soleil.lib.project.swingx.log.ATableLogViewer;

/**
 * A log viewer that can be used by any application.
 * <p>
 * It uses a {@link Logger} to treat errors <i>(like the ones that can be encountered during logs saving in a file)</i>.
 * </p>
 * <p>
 * If you want this {@link LogViewer} to listen to a {@link Logger}, you should use a {@link LogAppender}.
 * </p>
 * 
 * @see #treatError(String, Throwable, boolean)
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LogViewer extends ATableLogViewer {

    private static final long serialVersionUID = 3134809938109328759L;

    protected final String applicationIdForErrorFeedback;

    /**
     * Creates a new {@link LogViewer}, indicating an id to recover the {@link Logger} to which to redirect errors
     * treatment.
     * 
     * @param applicationIdForErrorFeedback The id that will be used to recover the {@link Logger}.
     */
    public LogViewer(final String applicationIdForErrorFeedback) {
        super();
        this.applicationIdForErrorFeedback = applicationIdForErrorFeedback == null ? getClass().getName()
                : applicationIdForErrorFeedback;
    }

    /**
     * Creates a new {@link LogViewer}, indicating an id to recover the {@link Logger} to which to redirect errors
     * treatment.
     * 
     * @param applicationIdForErrorFeedback The id that will be used to recover the {@link Logger}.
     * @param messageManager The {@link MessageManager} that knows the messages to display.
     * @param icons The {@link Icons} that knows the {@link ImageIcon}s to display.
     */
    public LogViewer(final String applicationIdForErrorFeedback, MessageManager messageManager, Icons icons) {
        super(messageManager, icons);
        this.applicationIdForErrorFeedback = applicationIdForErrorFeedback == null ? getClass().getName()
                : applicationIdForErrorFeedback;
    }

    /**
     * Redirects an error to the {@link Logger} defined at construction.
     * 
     * @param customMessage The custom message to transmit to {@link Logger}.
     * @param t The error.
     * @param Whether the error is not very important and should be considered as a warning.
     */
    @Override
    protected void treatError(String customMessage, Throwable t, boolean warning) {
        Logger logger = LoggerFactory.getLogger(applicationIdForErrorFeedback);
        if (warning) {
            if (t == null) {
                logger.warn(customMessage);
            } else {
                logger.warn(customMessage, t);
            }
        } else if (t == null) {
            logger.error(customMessage);
        } else {
            logger.error(customMessage, t);
        }
    }

}
