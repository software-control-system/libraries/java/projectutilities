/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.performance;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A tool used to benchmark application performances.
 * 
 * @author rodriguez
 */
public class Benchmarker {

    private static final String BENCHMARK = ">>>>>>>>>>>>>>>>>> Benchmark  <<<<<<<<<<<<<<<<<<<<<<";
    private static final String BENCHMARK_END = ">>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<";
    private static final String TO_MUCH_STOP_FOR = "To much stop for: ";
    private static final String STOPPING_INEXISTENT_TIMER = "Stopping inexistent timer: ";
    private static final String LABEL_SEPARATOR = ": ";
    private static final String SPENT = " spent: ";
    private static final String SECONDS = " s ";
    private static final String NB_CALLS_TITLE = " nb calls: ";
    private static final String SPACE = " ";
    private static final String MAX_THREAD = " max thread: ";
    private static final String MAXIMUM_COST = " maximum cost: ";
    private static final String MILLISECONDS = " ms ";
    private static final String AVERAGE_COST = " average cost: ";
    private static final String MINIMUM_COST = " minimum cost: ";
    private static final String FREE_MEM_VAR = " free mem var: ";
    private static final String MEGABYTES = " MB\n";
    private static final String TIMERS_ARE_STILL_RUNNING = "Timers are still running!!!!\n";
    private static final String PLEASE_STOP_THE_FOLLOWING_TIMERS = "Please stop the following timers:\n";
    private static final String LIST_START = "   - ";
    private static final String FORMAT = "#.";

    private static final Map<String, Long> TIMERS = new TreeMap<String, Long>();
    private static final Map<String, Long> MAX_TIMER = new TreeMap<String, Long>();
    private static final Map<String, Long> MIN_TIMER = new TreeMap<String, Long>();
    private static final Map<String, Long> COUNTERS = new TreeMap<String, Long>();
    private static final Map<String, Long> STARTERS = new TreeMap<String, Long>();
    private static final Map<String, Long> NB_CALLS = new TreeMap<String, Long>();
    private static final Map<String, Long> NB_THREAD = new TreeMap<String, Long>();
    private static final Map<String, Long> MEM_FREE = new TreeMap<String, Long>();
    private static final Map<String, Long> MEM_COST = new TreeMap<String, Long>();

    private static Logger logger = LoggerFactory.getLogger(Benchmarker.class);

    /**
     * Returns the {@link Logger} on which {@link Benchmarker} will log information.
     * 
     * @return A {@link Logger}
     */
    public static Logger getLogger() {
        return logger;
    }

    /**
     * Sets the {@link Logger} on which {@link Benchmarker} should log information.
     * 
     * @param logger The {@link Logger} on which {@link Benchmarker} should log information.
     */
    public static void setLogger(Logger logger) {
        if (logger == null) {
            Benchmarker.logger = LoggerFactory.getLogger(Benchmarker.class);
        } else {
            Benchmarker.logger = logger;
        }
    }

    /**
     * Start a timer with the given label.
     * 
     * @param label
     */
    public static void start(String label) {
        Long currentTime = System.currentTimeMillis();
        synchronized (Benchmarker.class) {
            if (COUNTERS.containsKey(label)) {
                Long counter = COUNTERS.get(label);
                Long call = NB_CALLS.get(label);
                if (counter <= 0) {
                    COUNTERS.put(label, (long) 1);
                    STARTERS.put(label, currentTime);
                } else {
                    COUNTERS.put(label, ++counter);
                    Long thread = NB_THREAD.get(label);
                    if (thread < counter) {
                        NB_THREAD.put(label, counter);
                    }
                }
                NB_CALLS.put(label, ++call);
                long run = Runtime.getRuntime().freeMemory();
                long free = MEM_FREE.get(label);
                if (free != 0) {
                    long cost = MEM_COST.get(label) + (run - free);
                    MEM_COST.put(label, cost);
                }
                MEM_FREE.put(label, run);
            } else {
                COUNTERS.put(label, 1L);
                STARTERS.put(label, currentTime);
                TIMERS.put(label, 0L);
                MAX_TIMER.put(label, 0L);
                MIN_TIMER.put(label, System.currentTimeMillis());
                NB_CALLS.put(label, 1L);
                NB_THREAD.put(label, 1L);
                MEM_FREE.put(label, Runtime.getRuntime().freeMemory());
                MEM_COST.put(label, 0L);
            }
        }
    }

    /**
     * Stop the timer having the given label.
     * 
     * @param label
     */
    public static void stop(String label) {
        Long currentTime = System.currentTimeMillis();
        synchronized (Benchmarker.class) {
            if (COUNTERS.containsKey(label)) {
                Long counter = COUNTERS.get(label);

                if (counter > 1) {
                    counter--;
                    COUNTERS.put(label, counter);
                    long run = Runtime.getRuntime().freeMemory();
                    long free = MEM_FREE.get(label);
                    if (free != 0) {
                        long cost = MEM_COST.get(label) + (run - free);
                        MEM_COST.put(label, cost);
                    }
                    MEM_FREE.put(label, run);
                } else if (counter > 0) {
                    counter--;
                    Long starter = STARTERS.get(label);
                    Long time = TIMERS.get(label);
                    time += (currentTime - starter);
                    TIMERS.put(label, time);
                    STARTERS.put(label, 0L);
                    COUNTERS.put(label, counter);

                    Long max = MAX_TIMER.get(label);
                    if (currentTime - starter > max) {
                        MAX_TIMER.put(label, currentTime - starter);
                    }
                    Long min = MIN_TIMER.get(label);
                    if (currentTime - starter < min) {
                        MIN_TIMER.put(label, currentTime - starter);
                    }

                    long run = Runtime.getRuntime().freeMemory();
                    long free = MEM_FREE.get(label);
                    if (free != 0) {
                        long cost = MEM_COST.get(label) + (run - free);
                        MEM_COST.put(label, cost);
                    }
                    MEM_FREE.put(label, 0L);

                } else {
                    logger.info(BENCHMARK);
                    logger.info(TO_MUCH_STOP_FOR + label);
                    logger.info(BENCHMARK_END);
                }
            } else {
                logger.info(BENCHMARK);
                logger.info(STOPPING_INEXISTENT_TIMER + label);
                logger.info(BENCHMARK_END);
            }
        }
    }

    /**
     * Trace the calling sequence with the full stack trace.
     */
    public static void traceCall() {
        traceCall(-1);
    }

    /**
     * Trace the calling sequence with the 'depthOfCall' last stack trace.
     * 
     * @param depthOfCall depth of the stack trace to be considered
     */
    public static void traceCall(int depthOfCall) {
        Exception trace = new Exception();
        String label = buildStringFromThrowable(trace, depthOfCall);

        synchronized (Benchmarker.class) {
            if (COUNTERS.containsKey(label)) {
                Long call = NB_CALLS.get(label);
                NB_CALLS.put(label, ++call);
            } else {
                COUNTERS.put(label, 0L);
                STARTERS.put(label, 0L);
                TIMERS.put(label, 0L);
                NB_CALLS.put(label, 1L);
                NB_THREAD.put(label, 0L);
                MEM_COST.put(label, 0L);
                MEM_FREE.put(label, 0L);
                MAX_TIMER.put(label, 0L);
                MIN_TIMER.put(label, 0L);
            }
        }
    }

    /**
     * Return the map containing all started timers
     */
    public static Map<String, Long> getTimers() {
        return Collections.unmodifiableMap(TIMERS);
    }

    /**
     * Return a String representation of all the static
     */
    public static String print() {
        StringBuilder result = new StringBuilder();
        synchronized (Benchmarker.class) {
            int max = 0;
            for (String label : TIMERS.keySet()) {
                String lines[] = label.split(ObjectUtils.NEW_LINE);
                for (String line : lines) {
                    if (line.length() + 2 > max) {
                        max = line.length() + 2;
                    }
                }
            }

            for (Entry<String, Long> entry : TIMERS.entrySet()) {
                String label = entry.getKey();
                long time = entry.getValue().longValue();
                result.append(print(label + LABEL_SEPARATOR, max));
                result.append(print(SPENT + roundNumber(time / 1000f, 3) + SECONDS, 20));
                result.append(print(NB_CALLS_TITLE + NB_CALLS.get(label), 20) + SPACE);
                result.append(print(MAX_THREAD + NB_THREAD.get(label), 20) + SPACE);
                result.append(print(MAXIMUM_COST + MAX_TIMER.get(label) + MILLISECONDS, 28));
                result.append(print(
                        AVERAGE_COST + roundNumber(time / NB_CALLS.get(label).floatValue(), 3) + MILLISECONDS, 30));
                result.append(print(MINIMUM_COST + MIN_TIMER.get(label) + MILLISECONDS, 28));
                result.append(print(FREE_MEM_VAR + roundNumber(Benchmarker.MEM_COST.get(label) / 1000000f, 3), 25)
                        + MEGABYTES);
            }
        }
        return result.toString();
    }

    public static String getTimeSpent(String label) {
        String result = null;
        result = roundNumber(Benchmarker.getTimers().get(label) / (float) 1000, 3) + SECONDS;
        return result;
    }

    /**
     * Reset all informations
     */
    public static void reset() {
        synchronized (Benchmarker.class) {
            boolean reset = true;
            List<String> running = new ArrayList<String>();
            for (Entry<String, Long> counter : COUNTERS.entrySet()) {
                if (counter.getValue() > 0) {
                    running.add(counter.getKey());
                    reset = false;
                }
            }
            if (reset) {
                TIMERS.clear();
                COUNTERS.clear();
                STARTERS.clear();
                NB_CALLS.clear();
                NB_THREAD.clear();
                MEM_FREE.clear();
                MEM_COST.clear();
                MAX_TIMER.clear();
                MIN_TIMER.clear();
            } else {
                StringBuilder log = new StringBuilder();
                log.append(BENCHMARK).append(ObjectUtils.NEW_LINE);
                log.append(TIMERS_ARE_STILL_RUNNING);
                log.append(PLEASE_STOP_THE_FOLLOWING_TIMERS);
                for (String timer : running) {
                    log.append(LIST_START + timer + ObjectUtils.NEW_LINE);
                }
                log.append(BENCHMARK_END);
                logger.info(log.toString());
            }
        }
    }

    // ------------------------------------------------------------------------
    // private methods
    // ------------------------------------------------------------------------
    private static String print(String value, int length) {
        String result = value;

        if (value.length() < length) {
            for (int i = 0; i < length - value.length(); i++) {
                result += ".";
            }
        }
        return result;
    }

    private static String buildStringFromThrowable(Throwable e, int depth) {
        StringBuilder sb = new StringBuilder();
        int i = -1;
        int depthCall = depth > e.getStackTrace().length ? e.getStackTrace().length : depth;
        for (StackTraceElement element : e.getStackTrace()) {
            if (i > -1 && (i < depthCall || depth < 0)) {
                sb.append(element.toString());
            } else if (i == depth && depth >= 0) {
                break;
            }
            if (i < depthCall - 1 && i > -1) {
                sb.append(ObjectUtils.NEW_LINE);
            }
            i++;
        }
        return sb.toString();
    }

    private static String roundNumber(Number num, int nbDigits) {
        String format = FORMAT;
        char[] digits = new char[nbDigits];
        Arrays.fill(digits, '#');
        format += String.copyValueOf(digits);
        DecimalFormat df = new DecimalFormat(format);
        return df.format(num);
    }

}
