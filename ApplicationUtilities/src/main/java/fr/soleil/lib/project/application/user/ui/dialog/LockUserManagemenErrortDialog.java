/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.dialog;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import fr.soleil.lib.project.application.user.manager.AccountManager;

public class LockUserManagemenErrortDialog extends AbstractUserManagementErrorDialog {

    private static final long serialVersionUID = -7371655113259194648L;

    public final static int STYLE_OK = 0;
    public final static int STYLE_CANCEL = 1;
    public final static int STYLE_REMOVE = 2;
    public final static int STYLE_OK_CANCEL = 3;
    public final static int STYLE_OK_REMOVE = 4;
    public final static int STYLE_CANCEL_REMOVE = 5;
    public final static int STYLE_OK_CANCEL_REMOVE = 6;
    public final static int STATE_OK = 0;
    public final static int STATE_CANCEL = 1;
    public final static int STATE_REMOVE = 2;
    private final AccountManager manager;
    private boolean canceled;
    private JButton okButton;
    private JButton cancelButton;
    private JButton removeLockButton;
    private final int style;
    private int state;
    private DefaultUserManagementErrorDialog errorDialog;

    public LockUserManagemenErrortDialog(String title, String message, AccountManager manager, int style) {
        super(title, message);
        this.manager = manager;
        this.style = style;
        initializeWithManager();
    }

    public LockUserManagemenErrortDialog(Frame frame, String title, String message, AccountManager manager, int style) {
        super(frame, title, message);
        this.manager = manager;
        this.style = style;
        initializeWithManager();
    }

    public LockUserManagemenErrortDialog(JDialog dialog, String title, String message, AccountManager manager,
            int style) {
        super(dialog, title, message);
        this.manager = manager;
        this.style = style;
        initializeWithManager();
    }

    @Override
    protected void initialize() {
        canceled = false;
        super.initialize();
        initializeWithManager();
    }

    protected void initializeWithManager() {
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                switch (style) {
                    case STYLE_REMOVE:
                        removeLock();
                        break;
                    case STYLE_OK:
                    case STYLE_OK_REMOVE:
                        ok();
                        break;
                    default:
                        cancel();
                        break;
                }
            }

        });
        if (manager != null) {
            removeLockButton = new JButton(manager.getMessage("user.manager.Account.UnlockGo"));
            removeLockButton.setIcon(manager.getIcon("user.manager.Account.Unlock"));
            removeLockButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    removeLock();
                }

            });
            okButton = new JButton(manager.getMessage("user.manager.Account.Ok"));
            okButton.setIcon(manager.getIcon("user.manager.Account.Validate"));
            okButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    ok();
                }

            });
            cancelButton = new JButton(manager.getMessage("user.manager.Account.Cancel"));
            cancelButton.setIcon(manager.getIcon("user.manager.Account.Cancel"));
            cancelButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    cancel();
                }

            });
            Insets gap = new Insets(5, 5, 5, 5);
            int columnIndex = 0;
            int lineIndex = mainPanel.getComponentCount();
            double weight = 1.0d / 3.0d;
            if (style == STYLE_OK || style == STYLE_OK_CANCEL || style == STYLE_OK_REMOVE
                    || style == STYLE_OK_CANCEL_REMOVE) {
                GridBagConstraints okConstraints = new GridBagConstraints();
                okConstraints.fill = GridBagConstraints.HORIZONTAL;
                okConstraints.gridx = columnIndex++;
                okConstraints.gridy = lineIndex;
                okConstraints.weightx = weight;
                okConstraints.weighty = 0;
                okConstraints.insets = gap;
                okConstraints.anchor = GridBagConstraints.CENTER;
                mainPanel.add(okButton, okConstraints);
            }
            if (style == STYLE_CANCEL || style == STYLE_OK_CANCEL || style == STYLE_CANCEL_REMOVE
                    || style == STYLE_OK_CANCEL_REMOVE) {
                GridBagConstraints cancelConstraints = new GridBagConstraints();
                cancelConstraints.fill = GridBagConstraints.HORIZONTAL;
                cancelConstraints.gridx = columnIndex++;
                cancelConstraints.gridy = lineIndex;
                cancelConstraints.weightx = weight;
                cancelConstraints.weighty = 0;
                cancelConstraints.insets = gap;
                cancelConstraints.anchor = GridBagConstraints.CENTER;
                mainPanel.add(cancelButton, cancelConstraints);
            }
            if (style == STYLE_REMOVE || style == STYLE_OK_REMOVE || style == STYLE_CANCEL_REMOVE
                    || style == STYLE_OK_CANCEL_REMOVE) {
                GridBagConstraints removeLockConstraints = new GridBagConstraints();
                removeLockConstraints.fill = GridBagConstraints.HORIZONTAL;
                removeLockConstraints.gridx = columnIndex++;
                removeLockConstraints.gridy = lineIndex;
                removeLockConstraints.weightx = weight;
                removeLockConstraints.weighty = 0;
                removeLockConstraints.insets = gap;
                removeLockConstraints.anchor = GridBagConstraints.CENTER;
                mainPanel.add(removeLockButton, removeLockConstraints);
            }
        }
        pack();
    }

    protected void ok() {
        setState(STATE_OK);
        setVisible(false);
    }

    protected void cancel() {
        setState(STATE_CANCEL);
        setVisible(false);
    }

    protected void removeLock() {
        boolean cancel = true;
        if (manager != null) {
            int ok = JOptionPane.showConfirmDialog(this, manager.getMessage("user.manager.Account.Unlock.Confirm"),
                    manager.getMessage("user.manager.Account.Unlock"), JOptionPane.YES_NO_OPTION);
            if (ok == JOptionPane.YES_OPTION) {
                if (manager.clearSelectedLock()) {
                    cancel = false;
                    setState(STATE_REMOVE);
                    setVisible(false);
                } else {
                    errorDialog = new DefaultUserManagementErrorDialog(this,
                            manager.getMessage("user.manager.Account.Unlock"),
                            manager.getMessage("user.manager.Account.Unlock.Error"));
                    errorDialog.setLocationRelativeTo(this);
                    errorDialog.setVisible(true);
                    cancel = true;
                }
            } else {
                cancel = true;
            }
        }
        if (cancel) {
            cancel();
        }
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

}
