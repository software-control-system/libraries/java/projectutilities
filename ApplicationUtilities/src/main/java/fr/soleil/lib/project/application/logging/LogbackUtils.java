/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.logging;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import fr.soleil.lib.project.log.Level;

/**
 * Utility class for interactions with logback.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LogbackUtils {

    private static LogManager logManager;
    private static LogViewer logViewer;

    /**
     * Returns a {@link LogViewer} that is configured to receive all log events. It will be created with given
     * application id if not yet created.
     * <p>
     * <big><b>
     * Warning! Should only be used when {@link LogManager} is not registered in logback configuration file
     * (<code>logback.xml</code>)!</b></big>
     * </p>
     * <p>
     * Otherwise, you should use {@link LogManager#getLogViewerInstance(String, boolean)}.
     * </p>
     * 
     * @param applicationId The application id that can be used to create {@link LogViewer} if not yet created.
     * @param stackTraces Whether errors stack traces should be displayed in {@link LogViewer}.
     * @return A {@link LogViewer}.
     * @see LogManager#getLogViewerInstance(String, boolean)
     */
    public static LogViewer getConfiguredLogViewer(String applicationId, boolean stackTraces) {
        if (logViewer == null) {
            logViewer = LogManager.getLogViewerInstance(applicationId, stackTraces);
        }
        if (logManager == null) {
            logManager = new LogManager();
            logManager.start();
            addRootAppender(logManager);
        }
        return logViewer;
    }

    /**
     * Adds an appender to all loggers, as explained
     * <a href="https://stackoverflow.com/questions/47299109/programmatically-add-appender-in-logback">here</a>.
     * 
     * @param appender The appender to add.
     */
    public static void addRootAppender(Appender<ILoggingEvent> appender) {
        addRootAppender(appender, true);
    }

    /**
     * Adds an appender to all loggers, as explained
     * <a href="https://stackoverflow.com/questions/47299109/programmatically-add-appender-in-logback">here</a>.
     * 
     * @param appender The appender to add.
     * @param autoConfigureContext Whether to automatically configure appender's logger context if it is
     *            <code>null</code>.
     */
    public static void addRootAppender(Appender<ILoggingEvent> appender, boolean autoConfigureContext) {
        if (appender != null) {
            Logger logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
            if (autoConfigureContext && (appender.getContext() == null)) {
                appender.setContext(logbackLogger.getLoggerContext());
            }
            logbackLogger.addAppender(appender);
        }
    }

    /**
     * Removes an appender from all loggers.
     * <p>
     * This is the opposite action of {@link #addRootAppender(Appender)}.
     * </p>
     * 
     * @param appender The appender to remove.
     */
    public static void removeRootAppender(Appender<ILoggingEvent> appender) {
        if (appender != null) {
            Logger logbackLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
            logbackLogger.detachAppender(appender);
        }
    }

    /**
     * Finds the best matching {@link ch.qos.logback.classic.Level} for a logging {@link Level}.
     * 
     * @param level The logging {@link Level}.
     * @return A {@link ch.qos.logback.classic.Level}.
     */
    public ch.qos.logback.classic.Level toLogbackLevel(Level level) {
        ch.qos.logback.classic.Level logbackLevel;
        if (level == null) {
            logbackLevel = null;
        } else {
            switch (level) {
                case ALL:
                    logbackLevel = ch.qos.logback.classic.Level.ALL;
                    break;
                case TRACE:
                    logbackLevel = ch.qos.logback.classic.Level.TRACE;
                    break;
                case DEBUG:
                    logbackLevel = ch.qos.logback.classic.Level.DEBUG;
                    break;
                case INFO:
                    logbackLevel = ch.qos.logback.classic.Level.INFO;
                    break;
                case WARNING:
                    logbackLevel = ch.qos.logback.classic.Level.WARN;
                    break;
                case ERROR:
                    logbackLevel = ch.qos.logback.classic.Level.ERROR;
                    break;
                default:
                    logbackLevel = ch.qos.logback.classic.Level.OFF;
                    break;
            }
        }
        return logbackLevel;
    }

    /**
     * Finds the best matching logging {@link Level} for a {@link ch.qos.logback.classic.Level}.
     * 
     * @param logbackLevel The {@link ch.qos.logback.classic.Level}.
     * @return A {@link Level}.
     */
    public static final Level toLoggingLevel(ch.qos.logback.classic.Level logbackLevel) {
        Level level;
        if (logbackLevel == null) {
            level = null;
        } else {
            switch (logbackLevel.levelInt) {
                case ch.qos.logback.classic.Level.ALL_INT:
                    level = Level.ALL;
                    break;
                case ch.qos.logback.classic.Level.TRACE_INT:
                    level = Level.TRACE;
                    break;
                case ch.qos.logback.classic.Level.DEBUG_INT:
                    level = Level.DEBUG;
                    break;
                case ch.qos.logback.classic.Level.INFO_INT:
                    level = Level.INFO;
                    break;
                case ch.qos.logback.classic.Level.WARN_INT:
                    level = Level.WARNING;
                    break;
                case ch.qos.logback.classic.Level.ERROR_INT:
                    level = Level.ERROR;
                    break;
                case ch.qos.logback.classic.Level.OFF_INT:
                default:
                    level = null;
                    break;
            }
        }
        return level;
    }

}
