/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.panel;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import fr.soleil.lib.project.application.user.manager.AccountManager;

/**
 * This is the main panel of the account selection dialog
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AccountSelectionPanel extends JPanel {

    private static final long serialVersionUID = -3745861693991234075L;

    private static final Font SESSION_FONT = new Font("Arial", Font.PLAIN, 12);
    private final JCheckBox multiSession;
    private final AccountSelectionListPanel selectionListPanel;
    private final AccountSelectionActionPanel selectionActionPanel;
    private final JLabel titleLabel;

    public AccountSelectionPanel(AccountManager manager) {
        super(new GridBagLayout());
        titleLabel = new JLabel(manager.getMessage("user.manager.Account.Choose"), SwingConstants.CENTER);
        multiSession = new JCheckBox(manager.getMessage("user.manager.Account.Session.Multi.Enable"));
        multiSession.setSelected(false);
        multiSession.setFont(SESSION_FONT);
        multiSession.setForeground(Color.RED);
        multiSession.setOpaque(false);
        multiSession.setSelected(false);
        selectionListPanel = new AccountSelectionListPanel(manager);
        selectionActionPanel = new AccountSelectionActionPanel(manager);
        GridBagConstraints titleLabelConstraints = new GridBagConstraints();
        titleLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        titleLabelConstraints.gridx = 0;
        titleLabelConstraints.gridy = 0;
        titleLabelConstraints.weightx = 1;
        titleLabelConstraints.weighty = 0;
        add(titleLabel, titleLabelConstraints);
        GridBagConstraints selectionListPanelConstraints = new GridBagConstraints();
        selectionListPanelConstraints.fill = GridBagConstraints.BOTH;
        selectionListPanelConstraints.gridx = 0;
        selectionListPanelConstraints.gridy = 1;
        selectionListPanelConstraints.weightx = 1;
        selectionListPanelConstraints.weighty = 1;
        add(selectionListPanel, selectionListPanelConstraints);
        if (manager.isManageMultiSession()) {
            GridBagConstraints multiSessionConstraints = new GridBagConstraints();
            multiSessionConstraints.fill = GridBagConstraints.BOTH;
            multiSessionConstraints.gridx = 0;
            multiSessionConstraints.gridy = 2;
            multiSessionConstraints.weightx = 1;
            multiSessionConstraints.weighty = 1;
            add(multiSession, multiSessionConstraints);
        }
        GridBagConstraints selectionActionPanelConstraints = new GridBagConstraints();
        selectionActionPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        selectionActionPanelConstraints.gridx = 0;
        selectionActionPanelConstraints.gridy = 3;
        selectionActionPanelConstraints.weightx = 1;
        selectionActionPanelConstraints.weighty = 0;
        add(selectionActionPanel, selectionActionPanelConstraints);

        setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
    }

    public boolean isMultiSession() {
        return multiSession.isSelected();
    }

    public AccountSelectionListPanel getSelectionListPanel() {
        return selectionListPanel;
    }

    public AccountSelectionActionPanel getSelectionActionPanel() {
        return selectionActionPanel;
    }

    public void setGlobalBackground(Color bg) {
        setBackground(bg);
        multiSession.setBackground(bg);
        titleLabel.setBackground(bg);
        selectionActionPanel.setGlobalBackground(bg);
        selectionListPanel.setGlobalBackground(bg);
    }

}
