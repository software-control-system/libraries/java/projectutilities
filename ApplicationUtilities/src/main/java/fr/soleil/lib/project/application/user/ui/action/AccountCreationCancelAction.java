/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import fr.soleil.lib.project.application.user.manager.AccountManager;

/**
 * Action that cancels an account creation
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AccountCreationCancelAction extends AbstractAction {

    private static final long serialVersionUID = -3217649533121180201L;

    private final AccountManager manager;

    /**
     * Constructor
     * 
     * @param manager
     *            the associated {@link AccountManager}
     */
    public AccountCreationCancelAction(AccountManager manager) {
        super();
        this.manager = manager;
        if (manager != null) {
            this.putValue(Action.NAME, manager.getMessage("user.manager.Account.Cancel"));
            this.putValue(Action.SMALL_ICON, manager.getIcon("user.manager.Account.Cancel"));
        }
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (manager != null) {
            manager.getAccountCreationDialog().setVisible(false);
        }
    }

}
