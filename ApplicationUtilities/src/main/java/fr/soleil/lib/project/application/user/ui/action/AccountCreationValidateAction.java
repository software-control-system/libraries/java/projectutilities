/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.action;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

import fr.soleil.lib.project.application.user.Account;
import fr.soleil.lib.project.application.user.manager.AccountManager;

/**
 * Action that validates a newly created account
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AccountCreationValidateAction extends AbstractAction {

    private static final long serialVersionUID = 6317006222329703489L;

    private final AccountManager manager;

    /**
     * Constructor
     * 
     * @param manager The associated {@link AccountManager}
     */
    public AccountCreationValidateAction(AccountManager manager) {
        super();
        this.manager = manager;
        if (manager != null) {
            this.putValue(Action.NAME, manager.getMessage("user.manager.Account.Ok"));
            this.putValue(SMALL_ICON, manager.getIcon("user.manager.Account.New.Validate"));
        }
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (manager != null) {
            String name = manager.getAccountCreationDialog().getAccountCreationPanel().getName();
            String path = manager.getAccountCreationDialog().getAccountCreationPanel().getPath();
            if (validate(name, path)) {
                int accountId = manager.addAccount(name, path);
                if (accountId > -1) {
                    Account account = manager.getAccount(accountId);
                    if (account != null) {
                        manager.getAccountSelectionDialog().getSelectionPanel().getSelectionListPanel()
                                .selectAccount(account);
                    }
                }
                if (accountId == AccountManager.NAME_ALREADY_EXISTS) {
                    JOptionPane.showMessageDialog(manager.getAccountCreationDialog(),
                            String.format(manager.getMessage("user.manager.Account.Name.Used"), name),
                            manager.getMessage("user.manager.Account.Error.Title"), JOptionPane.ERROR_MESSAGE);
                } else if (accountId == AccountManager.PATH_ALREADY_EXISTS) {
                    JOptionPane.showMessageDialog(manager.getAccountCreationDialog(),
                            String.format(manager.getMessage("user.manager.Account.Path.Used"), path),
                            manager.getMessage("user.manager.Account.Error.Title"), JOptionPane.ERROR_MESSAGE);
                } else {
                    manager.getAccountCreationDialog().setVisible(false);
                }
            } else {
                JOptionPane.showMessageDialog(manager.getAccountCreationDialog(),
                        manager.getMessage("user.manager.Account.Error.New"),
                        manager.getMessage("user.manager.Account.Error.Title"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private boolean validate(String name, String path) {
        boolean validated;
        if ((name != null) && (!name.trim().isEmpty()) && (path != null) && (!path.trim().isEmpty())) {
            File pathFile = new File(path.trim());
            if (pathFile.exists()) {
                validated = pathFile.isDirectory();
            } else {
                try {
                    validated = pathFile.mkdirs();
                } catch (Exception e) {
                    validated = false;
                }
            }
        } else {
            validated = false;
        }
        return validated;
    }

}
