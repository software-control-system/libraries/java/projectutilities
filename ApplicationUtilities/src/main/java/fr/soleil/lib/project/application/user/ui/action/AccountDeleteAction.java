/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

import fr.soleil.lib.project.application.user.manager.AccountManager;

/**
 * Action that deletes an account
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AccountDeleteAction extends AbstractAction {

    private static final long serialVersionUID = 3803828365478566267L;

    private final AccountManager manager;

    /**
     * Constructor
     * 
     * @param manager
     *            the associated {@link AccountManager}
     */
    public AccountDeleteAction(AccountManager manager) {
        super();
        this.manager = manager;
        if (manager != null) {
            this.putValue(Action.NAME, manager.getMessage("user.manager.Account.Delete"));
            this.putValue(SMALL_ICON, manager.getIcon("user.manager.Account.Delete"));
        }
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (manager != null) {
            if (validateChoice()) {
                manager.deleteSelectedAccount();
            } else {
                JOptionPane.showMessageDialog(manager.getAccountSelectionDialog(),
                        manager.getMessage("user.manager.Account.Error.NoAccount"),
                        manager.getMessage("user.manager.Account.Error.Title"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private boolean validateChoice() {
        if (manager == null) {
            return false;
        } else {
            return (manager.getAccountSelectionDialog().getSelectionPanel().getSelectionListPanel()
                    .getSelectedAccount() != -1);
        }
    }

}
