/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.dialog;

import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import fr.soleil.lib.project.application.user.manager.AccountManager;
import fr.soleil.lib.project.application.user.ui.panel.AccountSelectionPanel;

/**
 * A {@link JFrame} used to select an account
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AccountSelectionFrame extends JFrame {

    private static final long serialVersionUID = -5381781365172587800L;

    private final AccountSelectionPanel selectionPanel;

    public AccountSelectionFrame(AccountManager manager) {
        super();
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        if (manager != null) {
            setTitle(manager.getMessage("user.manager.Account.Title"));
            if (manager.getApplicationIcon() != null) {
                setIconImage(manager.getApplicationIcon().getImage());
            }
        }
        selectionPanel = new AccountSelectionPanel(manager);
        this.setContentPane(selectionPanel);
        pack();
        this.setSize(getSize().width, getSize().height + 30);
        final AccountManager accountManager = manager;
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                if (accountManager != null) {
                    accountManager.cancelAccountSelection();
                }
            }

        });
    }

    public AccountSelectionPanel getSelectionPanel() {
        return selectionPanel;
    }

    public void setGlobalBackground(Color bg) {
        setBackground(bg);
        selectionPanel.setGlobalBackground(bg);
    }

}
