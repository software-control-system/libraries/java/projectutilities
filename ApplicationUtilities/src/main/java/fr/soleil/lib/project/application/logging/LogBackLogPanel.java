/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.logging;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.ThrowableProxy;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.AppenderBase;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.panel.ALogPanel;
import fr.soleil.lib.project.xmlhelpers.HtmlEscape;

/**
 * An {@link ALogPanel} adapted to logback logging system.
 * 
 * @author Rapha&euml;l GIRARDOT
 * @deprecated use {@link LogViewer} instead.
 */
@Deprecated
public class LogBackLogPanel extends ALogPanel<Logger, Level, Appender<ILoggingEvent>> {

    private static final long serialVersionUID = -874712281578404521L;

    private static final Level[] LEVELS = { Level.OFF, Level.ERROR, Level.WARN, Level.INFO, Level.DEBUG, Level.TRACE,
            Level.ALL };

    // div classes
    private static final String DIV_ERROR = "<div class=\"error\">";
    private static final String DIV_WARNING = "<div class=\"warning\">";
    private static final String DIV_INFO = "<div class=\"info\">";
    private static final String DIV_DEBUG = "<div class=\"debug\">";
    private static final String DIV_TRACE = "<div class=\"trace\">";
    private static final String DIV_DEFAULT = "<div>";
    // class descriptor
    private static final String CLASS_ERROR = "div.error {color:#FF0000;font-size:14pt;font-weight:bold;}";
    private static final String CLASS_WARNING = "div.warning {color:#FF8000;font-size:13pt;font-style:italic;}";
    private static final String CLASS_INFO = "div.info {color:#000000;font-size:12pt;font-style:normal;}";
    private static final String CLASS_DEBUG = "div.debug {color:#0000FF;font-size:12pt;font-style:normal;}";
    private static final String CLASS_TRACE = "div.trace {color:#888888;font-size:10pt;font-style:normal;}";
    // Log levels (values extracted from corresponding Levels javadoc)
    public static final int ERROR = Level.ERROR_INT;
    public static final int WARNING = Level.WARN_INT;
    public static final int INFO = Level.INFO_INT;
    public static final int DEBUG = Level.DEBUG_INT;
    public static final int TRACE = Level.TRACE_INT;

    protected static final String WARNING_TEXT = "WARNING";

    protected Level level;
    protected boolean traceErrors;

    public LogBackLogPanel() {
        super();
        level = Level.ALL;
        traceErrors = false;
    }

    public LogBackLogPanel(boolean html) {
        super(html);
        level = Level.ALL;
        traceErrors = false;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    /**
     * Returns whether this {@link LogBackLogPanel} will trace error's stack trace in log area.
     * 
     * @return A <code>boolean</code>. <code>false</code> by default.
     */
    public boolean isTraceErrors() {
        return traceErrors;
    }

    /**
     * Sets whether this {@link LogBackLogPanel} should trace error's stack trace in log area.
     * 
     * @param traceErrors Whether this {@link LogBackLogPanel} should trace error's stack trace in log area.
     */
    public void setTraceErrors(boolean traceErrors) {
        this.traceErrors = traceErrors;
    }

    @Override
    protected void addHandlerToLogger(Logger logger, Appender<ILoggingEvent> handler) {
        if ((logger != null) && (handler != null)) {
            if (handler.getContext() == null) {
                handler.setContext(logger.getLoggerContext());
            }
            logger.addAppender(handler);
        }
    }

    @Override
    protected Appender<ILoggingEvent> generateHandler(Publisher publisher) {
        return new LogTracer(publisher);
    }

    @Override
    protected Logger generateLogger(String id) {
        org.slf4j.Logger logger = null;
        if (id != null) {
            logger = LoggerFactory.getLogger(id);
        }
        Logger result;
        if (logger instanceof Logger) {
            result = (Logger) logger;
        } else {
            result = null;
        }
        return result;
    }

    @Override
    protected void logError(String message, Exception error) {
        if (logger != null) {
            logger.error(message, error);
        }
    }

    @Override
    protected Level[] getLevels() {
        return LEVELS;
    }

    @Override
    protected String getDivCSSClass(Level lvl) {
        int level = (lvl == null ? -1 : lvl.toInt());
        String cssClass;
        switch (level) {
            case ERROR:
                cssClass = CLASS_ERROR;
                break;
            case WARNING:
                cssClass = CLASS_WARNING;
                break;
            case INFO:
                cssClass = CLASS_INFO;
                break;
            case DEBUG:
                cssClass = CLASS_DEBUG;
                break;
            case TRACE:
                cssClass = CLASS_TRACE;
                break;
            default:
                cssClass = null;
                break;
        }
        return cssClass;
    }

    @Override
    protected String getDiv(Level lvl) {
        int level = (lvl == null ? -1 : lvl.toInt());
        String cssClass;
        switch (level) {
            case ERROR:
                cssClass = DIV_ERROR;
                break;
            case WARNING:
                cssClass = DIV_WARNING;
                break;
            case INFO:
                cssClass = DIV_INFO;
                break;
            case DEBUG:
                cssClass = DIV_DEBUG;
                break;
            case TRACE:
                cssClass = DIV_TRACE;
                break;
            default:
                cssClass = DIV_DEFAULT;
                break;
        }
        return cssClass;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class LogTracer extends AppenderBase<ILoggingEvent> {
        protected final Publisher publisher;
        protected final PatternLayout patternLayout;

        public LogTracer(Publisher publisher) {
            super();
            this.publisher = publisher;
            this.patternLayout = new PatternLayout();
            setContext(new LoggerContext());
        }

        @Override
        public synchronized void doAppend(ILoggingEvent event) {
            if (event != null) {
                Level level = event.getLevel();
                if ((level != null) && (level.toInt() >= getLevel().toInt())) {
                    StringBuilder sb = new StringBuilder();
                    if (level.toInt() == WARNING) {
                        sb.append(WARNING_TEXT);
                    } else {
                        sb.append(level);
                    }
                    sb.append(LEVEL_SEPARATOR);
                    String message = event.getMessage();
                    if (message != null) {
                        if (isHtml()) {
                            sb.append(HtmlEscape.escape(message, true, true));
                        } else {
                            sb.append(message);
                        }
                    }
                    if (traceErrors || (message == null)) {
                        IThrowableProxy proxy = event.getThrowableProxy();
                        if (proxy instanceof ThrowableProxy) {
                            Throwable t = ((ThrowableProxy) proxy).getThrowable();
                            if (t != null) {
                                try {
                                    // try to trace a minimum message
                                    String errorMessage = t.getMessage();
                                    if ((message == null) && (errorMessage != null)) {
                                        if (isHtml()) {
                                            sb.append(HtmlEscape.escape(errorMessage, true, true));
                                        } else {
                                            sb.append(errorMessage);
                                        }
                                    }
                                    if (traceErrors) {
                                        errorMessage = ObjectUtils.printStackTrace(t);
                                        if (isHtml()) {
                                            sb.append(HTML_NEW_LINE)
                                                    .append(errorMessage.replace(NEW_LINE, HTML_NEW_LINE));
                                        } else {
                                            sb.append(NEW_LINE).append(errorMessage);
                                        }
                                    }
                                } catch (Exception ex) {
                                    // nothing to do: ignore exception
                                }
                            }
                        }
                    } // end if (traceErrors || (message == null))
                    publisher.publish(sb.toString(), level);
                } // end if ((level != null) && (level.toInt() > getLevel().toInt()))
            } // if (event != null)
        }

        @Override
        protected void append(ILoggingEvent eventObject) {
            // not managed
        }

    }

}
