/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.comparator;

import java.text.Collator;
import java.util.Comparator;

import fr.soleil.lib.project.application.user.Account;

/**
 * A {@link Comparator} that compares {@link Account}s according to their name, then their id.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AccountNameComparator implements Comparator<Account> {

    private final Collator collator;

    public AccountNameComparator() {
        collator = Collator.getInstance();
    }

    @Override
    public int compare(Account a1, Account a2) {
        int result;
        if (a1 == null) {
            if (a2 == null) {
                result = 0;
            } else {
                result = -1;
            }
        } else if (a2 == null) {
            result = 1;
        } else {
            String n1 = a1.getName(), n2 = a2.getName();
            if (n1 == null) {
                if (n2 == null) {
                    result = 0;
                } else {
                    result = -1;
                }
            } else if (n2 == null) {
                result = 1;
            } else {
                result = collator.compare(n1, n2);
                if (result == 0) {
                    result = Integer.compare(a1.getId(), a2.getId());
                }
            }
        }
        return result;
    }

}
