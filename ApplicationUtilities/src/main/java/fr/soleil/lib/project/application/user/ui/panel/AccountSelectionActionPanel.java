/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.panel;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;

import fr.soleil.lib.project.application.user.manager.AccountManager;
import fr.soleil.lib.project.application.user.ui.action.AccountCancelAction;
import fr.soleil.lib.project.application.user.ui.action.AccountCreationAction;
import fr.soleil.lib.project.application.user.ui.action.AccountDeleteAction;
import fr.soleil.lib.project.application.user.ui.action.AccountReloadAction;
import fr.soleil.lib.project.application.user.ui.action.AccountValidateAction;

/**
 * This is the panel that contains all the buttons in account selection dialog
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AccountSelectionActionPanel extends JPanel {

    private static final long serialVersionUID = -7166681142185182964L;

    private final JButton okButton;
    private final JButton cancelButton;
    private final JButton newButton;
    private final JButton deleteButton;
    private final JButton reloadButton;

    public AccountSelectionActionPanel(AccountManager manager) {
        super(new GridBagLayout());
        Insets noMargin = new Insets(0, 0, 0, 0);
        Insets firstLinegap = new Insets(0, 0, 0, 10);
        Insets secondLinegap = new Insets(5, 0, 0, 10);
        okButton = new JButton(new AccountValidateAction(manager));
        okButton.setMargin(noMargin);
        newButton = new JButton();
        newButton.setAction(new AccountCreationAction(manager));
        newButton.setMargin(noMargin);
        deleteButton = new JButton(new AccountDeleteAction(manager));
        deleteButton.setMargin(noMargin);
        reloadButton = new JButton(new AccountReloadAction(manager));
        reloadButton.setMargin(noMargin);
        cancelButton = new JButton();
        cancelButton.setAction(new AccountCancelAction(manager));
        cancelButton.setMargin(noMargin);

        double weight = 1.0d / 3.0d;

        // Line 1
        GridBagConstraints okConstraints = new GridBagConstraints();
        okConstraints.fill = GridBagConstraints.HORIZONTAL;
        okConstraints.gridx = 0;
        okConstraints.gridy = 0;
        okConstraints.weightx = weight;
        okConstraints.weighty = 0;
        okConstraints.insets = firstLinegap;
        this.add(okButton, okConstraints);
        GridBagConstraints cancelConstraints = new GridBagConstraints();
        cancelConstraints.fill = GridBagConstraints.HORIZONTAL;
        cancelConstraints.gridx = 1;
        cancelConstraints.gridy = 0;
        cancelConstraints.weightx = weight;
        cancelConstraints.weighty = 0;
        cancelConstraints.insets = firstLinegap;
        this.add(cancelButton, cancelConstraints);
        GridBagConstraints reloadConstraints = new GridBagConstraints();
        reloadConstraints.fill = GridBagConstraints.HORIZONTAL;
        reloadConstraints.gridx = 3;
        reloadConstraints.gridy = 0;
        reloadConstraints.weightx = weight;
        reloadConstraints.weighty = 0;
        reloadConstraints.insets = noMargin;
        this.add(reloadButton, reloadConstraints);

        // Line 2
        GridBagConstraints newConstraints = new GridBagConstraints();
        newConstraints.fill = GridBagConstraints.HORIZONTAL;
        newConstraints.gridx = 0;
        newConstraints.gridy = 1;
        newConstraints.weightx = weight;
        newConstraints.weighty = 0;
        newConstraints.insets = secondLinegap;
        this.add(newButton, newConstraints);
        GridBagConstraints deleteConstraints = new GridBagConstraints();
        deleteConstraints.fill = GridBagConstraints.HORIZONTAL;
        deleteConstraints.gridx = 1;
        deleteConstraints.gridy = 1;
        deleteConstraints.weightx = weight;
        deleteConstraints.weighty = 0;
        deleteConstraints.insets = secondLinegap;
        this.add(deleteButton, deleteConstraints);
    }

    public JButton getOkButton() {
        return okButton;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public void setGlobalBackground(Color bg) {
        setBackground(bg);
        okButton.setBackground(bg);
        cancelButton.setBackground(bg);
        newButton.setBackground(bg);
        deleteButton.setBackground(bg);
        reloadButton.setBackground(bg);
    }

}
