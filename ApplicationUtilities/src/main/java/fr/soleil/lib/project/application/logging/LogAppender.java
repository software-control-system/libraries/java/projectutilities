/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.logging;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.ThrowableProxy;
import ch.qos.logback.core.AppenderBase;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.lib.project.log.Level;
import fr.soleil.lib.project.log.LogData;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * An {@link AppenderBase} that will redirect log messages to a {@link LogViewer}.
 * 
 * <table border=1 style="border-collapse: collapse;">
 * <tr>
 * <th style="text-align: left;">Use case example:</th>
 * </tr>
 * <tr>
 * <td>
 * 
 * <pre>
 * LogViewer viewer = new LogViewer(applicationId);
 * LogAppender appender = new LogAppender(viewer);
 * LogAppender.registerAppender(logger, appender);
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * 
 * @author Rapha&euml;l GIRARDOT
 * @see #registerAppender(Logger, LogAppender)
 * @see #registerAppender(Class, LogAppender)
 * @see #registerAppender(String, LogAppender)
 */
public class LogAppender extends AppenderBase<ILoggingEvent> implements IDateConstants {

    protected final WeakReference<LogViewer> logViewerRef;
    protected final boolean printErrorTrace;
    protected final String DELIM = "{}", REPLACEMENT = "%s";

    /**
     * Constructs a new {@link LogAppender}, indicating to which {@link LogViewer} to redirect log messages.
     * 
     * @param viewer The {@link LogViewer}.
     */
    public LogAppender(LogViewer viewer) {
        this(viewer, true);
    }

    /**
     * Constructs a new {@link LogAppender}, indicating to which {@link LogViewer} to redirect log messages.
     * 
     * @param viewer The {@link LogViewer}.
     * @param printErrorTrace Whether to transmit errors stack traces.
     */
    public LogAppender(LogViewer viewer, boolean printErrorTrace) {
        super();
        this.logViewerRef = viewer == null ? null : new WeakReference<>(viewer);
        this.printErrorTrace = printErrorTrace;
    }

    @Override
    public synchronized void doAppend(ILoggingEvent event) {
        if (event != null) {
            LogViewer viewer = ObjectUtils.recoverObject(logViewerRef);
            if (viewer != null) {
                Level level = parseLevel(event.getLevel());
                String message = event.getMessage();
                Throwable t;
                IThrowableProxy proxy = event.getThrowableProxy();
                if (proxy instanceof ThrowableProxy) {
                    t = ((ThrowableProxy) proxy).getThrowable();
                } else {
                    t = null;
                }
                Object[] array = event.getArgumentArray();
                if ((array != null) && (array.length > 0)) {
                    if (t != null) {
                        if (array[array.length - 1] == t) {
                            array = Arrays.copyOf(array, array.length - 1);
                        } else if ((array.length > 1) && (array[0] == t)) {
                            array = Arrays.copyOfRange(array, 1, array.length);
                        }
                    }
                    if (array.length > 0) {
                        StringBuilder builder = new StringBuilder(message.length() + 50);
                        int index = message.indexOf(DELIM);
                        int argIndex = 0;
                        int strIndex = 0;
                        while ((index > -1) && (index < message.length())) {
                            if (argIndex < array.length) {
                                while (strIndex < index) {
                                    builder.append(message.charAt(strIndex++));
                                }
                                if ((index > 0) && message.charAt(index - 1) == '\\') {
                                    builder.deleteCharAt(builder.length() - 1);
                                    builder.append(DELIM);
                                } else {
                                    builder.append(ArrayUtils.toString(array[argIndex++]));
                                }
                                index = message.indexOf(DELIM, index + 1);
                                strIndex += DELIM.length();
                            } else {
                                while (strIndex < message.length()) {
                                    builder.append(message.charAt(strIndex++));
                                }
                            }
                        }
                        while (strIndex < message.length()) {
                            builder.append(message.charAt(strIndex++));
                        }
                        message = builder.toString();
                        builder.delete(0, builder.length());
                    }
                }
                String timestamp = new SimpleDateFormat(US_DATE_FORMAT).format(new Date(event.getTimeStamp()));
                if ((printErrorTrace) || (message == null)) {
                    if (t != null) {
                        message = new StringBuilder(message).append('\n').append(ObjectUtils.printStackTrace(t))
                                .toString();
                    }
                }
                viewer.addLogs(new LogData(timestamp, level, message));
            }
        }
    }

    @Override
    protected void append(ILoggingEvent eventObject) {
        // not managed
    }

    /**
     * Parses a {@link ch.qos.logback.classic.Level} and returns best matching {@link Level}.
     * 
     * @param logBackLevel The {@link ch.qos.logback.classic.Level} to parse.
     * @return A {@link Level}. <code>null</code> if <code>logBackLevel</code> is <code>null</code>.
     */
    protected static Level parseLevel(ch.qos.logback.classic.Level logBackLevel) {
        Level level;
        if (logBackLevel == null) {
            level = null;
        } else {
            switch (logBackLevel.levelInt) {
                case ch.qos.logback.classic.Level.ALL_INT:
                    level = Level.ALL;
                    break;
                case ch.qos.logback.classic.Level.ERROR_INT:
                    level = Level.ERROR;
                    break;
                case ch.qos.logback.classic.Level.WARN_INT:
                    level = Level.WARNING;
                    break;
                case ch.qos.logback.classic.Level.INFO_INT:
                    level = Level.INFO;
                    break;
                case ch.qos.logback.classic.Level.DEBUG_INT:
                    level = Level.DEBUG;
                    break;
                default:
                    level = Level.TRACE;
                    break;
            }
        }
        return level;
    }

    /**
     * Registers, if possible, an {@link LogAppender} to a {@link Logger}.
     * 
     * @param logger The {@link Logger}.
     * @param appender The {@link LogAppender}.
     */
    public static void registerAppender(Logger logger, LogAppender appender) {
        if ((appender != null) && (logger instanceof ch.qos.logback.classic.Logger)) {
            ch.qos.logback.classic.Logger logbackLogger = (ch.qos.logback.classic.Logger) logger;
            if (appender.getContext() == null) {
                appender.setContext(logbackLogger.getLoggerContext());
            }
            logbackLogger.addAppender(appender);
        }
    }

    /**
     * Registers, if possible, an {@link LogAppender} to a {@link Logger}.
     * 
     * @param id The id that will help recovering the {@link Logger}.
     * @param appender The {@link LogAppender}.
     */
    public static void registerAppender(String id, LogAppender appender) {
        registerAppender(LoggerFactory.getLogger(id), appender);
    }

    /**
     * Registers, if possible, an {@link LogAppender} to a {@link Logger}.
     * 
     * @param clazz The class The {@link Class} to which the {@link Logger} is associated.
     * @param appender The {@link LogAppender}.
     */
    public static void registerAppender(Class<?> clazz, LogAppender appender) {
        registerAppender(LoggerFactory.getLogger(clazz), appender);
    }

}
