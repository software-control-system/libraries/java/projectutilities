/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.manager;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.Application;
import fr.soleil.lib.project.application.user.Account;
import fr.soleil.lib.project.application.user.session.Session;
import fr.soleil.lib.project.application.user.ui.dialog.AccountCreationDialog;
import fr.soleil.lib.project.application.user.ui.dialog.AccountSelectionFrame;
import fr.soleil.lib.project.application.user.ui.dialog.DefaultUserManagementErrorDialog;
import fr.soleil.lib.project.application.user.ui.dialog.LockUserManagemenErrortDialog;
import fr.soleil.lib.project.application.user.ui.event.AccountSelectionEvent;
import fr.soleil.lib.project.application.user.ui.listener.IAccountSelectionListener;
import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.lib.project.swing.icons.Icons;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;

/**
 * The real Account Manager. This is the class that should be called by any
 * program that wants to use account management.
 * 
 * @author SOLEIL
 */
public class AccountManager implements IAccountSelectionListener {

    private static final String INDENT = "    ";

    /**
     * Represents the fact that no account is selected
     */
    public static final int NO_ACCOUNT_SELECTED = -1;

    /**
     * Represents the fact that the accounts selection was canceled
     */
    public static final int ACCOUNT_SELECTION_CANCELED = -2;

    /**
     * Represents the fact that the accounts map file could not be created
     */
    public static final int CREATION_FAILED = -3;

    /**
     * Represents the fact that the accounts map file could not be written, but
     * not because of a lock.
     */
    public static final int WRITE_FAILED = -4;

    /**
     * Represents the fact that the accounts map file could not be read
     */
    public static final int READ_FAILED = -5;

    /**
     * Represents the fact that the accounts map file is locked for writing
     */
    public static final int USED_FAILED = -6;

    /**
     * Represents the fact that the temporary lock file could not be written
     */
    public static final int LOCK_FAILED = -7;

    /**
     * Represents the fact that the temporary lock file could not be removed
     */
    public static final int REMOVE_LOCK_FAILED = -8;

    /**
     * Represents the fact that an unexpected exception happened
     */
    public static final int UNKNOWN = -9;

    /**
     * Represents the fact that the temporary lock file in account folder could not be written
     */
    public static final int LOCK_FOLDER_FAILED = -10;

    /**
     * Represents the fact that another account already uses the same name as proposed new account
     */
    public static final int NAME_ALREADY_EXISTS = -11;

    /**
     * Represents the fact that another account already uses the same path as proposed new account
     */
    public static final int PATH_ALREADY_EXISTS = -12;

    /**
     * Represents the fact that application failed to update account last access date and write it in account list file.
     */
    public static final int ACCOUNT_ACCES_DATE_UPDATE_FAILED = -13;

    public static final Color ACCOUNT_COLOR = new Color(192, 192, 255);

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountManager.class);

    private static final long TIME_TO_WAIT = 10000;
    private static final long MAX_TIME_TO_WAIT = 50000;

    private static final String LOCK_PROFILE_LIST_FILE_NAME = ".lockProfiles";
    private static final String LOCK_FILE_NAME = ".lock";
    // ".profile" due to history
    private static final String ACCOUNT_FILE_EXTENSION = ".profile";

    private final String applicationKey;
    private final ImageIcon applicationIcon;
    private final String accountFileName;
    private final String mainNodeTag;
    private final String accountFilePath;
    private final String lockProfileListFilePath;
    private final Map<Integer, Account> accounts;
    private final Map<Integer, Session> sessions;
    private int selectedAccount;
    private final AccountSelectionFrame accountSelectionFrame;
    private final AccountCreationDialog accountCreationDialog;
    private final Icons icons;
    private final boolean manageMultiSession;
    private DefaultUserManagementErrorDialog errorDialog;
    private LockUserManagemenErrortDialog lockDialog;
    private AccountThread accountThread;
    private final ClassLoader classLoader;
    private final boolean autoDeleteEmptyProfilesFile;

    /**
     * Constructor - sets every necessary information for this manager to be
     * able to load the account list
     * 
     * @param mainFolderPath The path of the application start folder
     * @param applicationKey The main name of the application (with no space)
     * @param applicationIcon The {@link ImageIcon} to represent the application. Can be <code>null</code>
     * @param manageMultiSession A boolean to know whether multi session can be used or not. <code>true</code> to allow
     *            multi session management.
     */
    public AccountManager(String mainFolderPath, String applicationKey, ImageIcon applicationIcon,
            boolean manageMultiSession) {
        this(mainFolderPath, applicationKey, applicationIcon, manageMultiSession, true);
    }

    /**
     * Constructor - sets every necessary information for this manager to be
     * able to load the account list
     * 
     * @param mainFolderPath The path of the application start folder
     * @param applicationKey The main name of the application (with no space)
     * @param applicationIcon The {@link ImageIcon} to represent the application. Can be <code>null</code>
     * @param manageMultiSession A boolean to know whether multi session can be used or not. <code>true</code> to allow
     *            multi session management.
     * @param autoDeleteEmptyProfilesFile Whether to automatically delete the ".profile" file when it is detected as
     *            empty.
     */
    public AccountManager(String mainFolderPath, String applicationKey, ImageIcon applicationIcon,
            boolean manageMultiSession, boolean autoDeleteEmptyProfilesFile) {
        super();
        classLoader = getClass().getClassLoader();
        this.accounts = new HashMap<Integer, Account>();
        this.sessions = new HashMap<Integer, Session>();
        this.selectedAccount = NO_ACCOUNT_SELECTED;
        this.manageMultiSession = manageMultiSession;
        this.autoDeleteEmptyProfilesFile = autoDeleteEmptyProfilesFile;
        this.icons = new Icons("fr.soleil.lib.project.application.user.icons");
        if (applicationKey == null) {
            applicationKey = getMessage("user.manager.Account.Application.Default");
        }
        this.applicationKey = applicationKey;
        this.applicationIcon = applicationIcon;
        this.accountFileName = applicationKey.toLowerCase() + ACCOUNT_FILE_EXTENSION;
        this.mainNodeTag = applicationKey + "Accounts";
        this.accountFilePath = mainFolderPath + File.separator + accountFileName;
        this.lockProfileListFilePath = mainFolderPath + File.separator + LOCK_PROFILE_LIST_FILE_NAME;
        this.accountThread = new AccountThread();
        this.accountSelectionFrame = new AccountSelectionFrame(this);
        this.accountCreationDialog = new AccountCreationDialog(this);
        setUIColor(ACCOUNT_COLOR);
    }

    /**
     * Returns whether this {@link AccountManager} will automatically delete the ".profile" file when it is empty.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isAutoDeleteEmptyProfilesFile() {
        return autoDeleteEmptyProfilesFile;
    }

    /**
     * This is the method that displays a dialog to select and launch an account.
     * Once over, you should check selected account.
     * 
     * @see #getSelectedAccount()
     * @see #getSelectedAccountName()
     * @see #getSelectedAccountWorkingPath()
     */
    public void launchAccountSelector() {
        loadAccounts();
        accountSelectionFrame.setLocationRelativeTo(null);
        accountSelectionFrame.setVisible(true);
        if ((accountThread == null) || (!accountThread.isAlive())) {
            accountThread = new AccountThread();
            accountThread.start();
            try {
                accountThread.join();
            } catch (InterruptedException e) {
                selectedAccount = ACCOUNT_SELECTION_CANCELED;
            }
        }
    }

    public int loadAccounts() {
        File accountFile = new File(accountFilePath);
        File lockFile = new File(lockProfileListFilePath);

        if (isAutoDeleteEmptyProfilesFile() && accountFile.exists() && (accountFile.length() == 0)) {
            // TANGOARCH-641: automatically delete empty profiles file
            accountFile.delete();
        }

        int result = 0;
        // if the account configuration file does not exist, create it
        if (!accountFile.exists()) {
            try {
                long time = System.currentTimeMillis();
                while ((!lockFile.createNewFile()) && (System.currentTimeMillis() < time + MAX_TIME_TO_WAIT)) {
                    result = CREATION_FAILED;
                    Thread.sleep(TIME_TO_WAIT);
                }
                if (System.currentTimeMillis() >= time + MAX_TIME_TO_WAIT) {
                    clearAccountListLock();
                    lockFile.createNewFile();
                    result = 0;
                }
                boolean accountFileCreated = accountFile.createNewFile();
                if (!accountFileCreated) {
                    showFailedMessage(CREATION_FAILED, null);
                    result = CREATION_FAILED;
                }
                XMLLine emptyLine = new XMLLine(mainNodeTag, XMLLine.EMPTY_TAG_CATEGORY);
                XMLUtils.save(emptyLine.toString(), accountFilePath);
                try {
                    lockFile.delete();
                } catch (SecurityException security) {
                    showFailedMessage(REMOVE_LOCK_FAILED, security);
                    result = REMOVE_LOCK_FAILED;
                }
            } catch (SecurityException security) {
                showFailedMessage(CREATION_FAILED, security);
                result = CREATION_FAILED;
            } catch (IOException io) {
                showFailedMessage(WRITE_FAILED, io);
                result = WRITE_FAILED;
            } catch (Exception e) {
                showFailedMessage(UNKNOWN, e);
                result = UNKNOWN;
            }
        }

        if (result == 0) {
            // Loading start
            try {
                Node rootNode = XMLUtils.getRootNode(accountFile);
                if (rootNode.hasChildNodes()) {
                    NodeList accountNodes = rootNode.getChildNodes();
                    accounts.clear();

                    SimpleDateFormat dateFormat = new SimpleDateFormat(IDateConstants.US_DATE_FORMAT);
                    for (int i = 0; i < accountNodes.getLength(); i++) {
                        // as many loops as there are account sub-blocks in the saved file
                        Node currentAccountNode = accountNodes.item(i);
                        if (!XMLUtils.isAFakeNode(currentAccountNode)) {
                            Map<String, String> accountAttrs = XMLUtils.loadAttributes(currentAccountNode);
                            String idAsString = accountAttrs.get(Account.ID_TAG);
                            String name = accountAttrs.get(Account.NAME_TAG);
                            String path = accountAttrs.get(Account.PATH_TAG);
                            String dateAsString = accountAttrs.get(Account.LAST_ACCESS_DATE_TAG);
                            long date;
                            try {
                                date = dateFormat.parse(dateAsString).getTime();
                            } catch (Exception e) {
                                // date not found or not correctly written: use default date
                                date = Account.getDefaultLastAccessDate();
                            }
                            int id = Integer.parseInt(idAsString);
                            accounts.put(id, new Account(id, name, path, date));
                        }
                        currentAccountNode = null;
                    }
                    result = 0;
                }
            } catch (Exception e) {
                showFailedMessage(UNKNOWN, e);
                result = UNKNOWN;
            } finally {
                // Loading successful
                refreshUIAndClearListLock();
            }
        }

        return result;

    }

    public int addAccount(String name, String path) {
        File lockFile = new File(lockProfileListFilePath);
        int result = -1;
        try {
            File directory = new File(path);
            // be sure to be up to date
            int reload = this.loadAccounts();
            long time = System.currentTimeMillis();
            while ((!lockFile.createNewFile()) && (System.currentTimeMillis() < time + MAX_TIME_TO_WAIT)) {
                showFailedMessage(USED_FAILED, null);
                Thread.sleep(TIME_TO_WAIT);
            }
            if (System.currentTimeMillis() >= time + MAX_TIME_TO_WAIT) {
                clearAccountListLock();
                lockFile.createNewFile();
            }
            switch (reload) {
                case 0:
                    for (Account account : accounts.values()) {
                        if (account != null) {
                            if (ObjectUtils.sameObject(name, account.getName())) {
                                result = NAME_ALREADY_EXISTS;
                                break;
                            } else if (directory.equals(new File(account.getPath()))) {
                                result = PATH_ALREADY_EXISTS;
                                break;
                            }
                        }
                    }
                    if (result == -1) {
                        // no error on reload : add account, save and remove lock
                        int id = this.getNewId();
                        accounts.put(Integer.valueOf(id), new Account(id, name, path, System.currentTimeMillis()));
                        saveAccounts();
                        try {
                            lockFile.delete();
                        } catch (SecurityException security) {
                            showFailedMessage(REMOVE_LOCK_FAILED, security);
                            result = REMOVE_LOCK_FAILED;
                            break;
                        }
                        result = id;
                    }
                    break;

                case USED_FAILED:
                    // file locked : try again later
                    showFailedMessage(USED_FAILED, null);
                    result = USED_FAILED;
                    break;

                default:
                    // any other exception: this was not expected, exit
                    showFailedMessage(UNKNOWN, null);
                    result = UNKNOWN;
                    break;

            }
        } catch (SecurityException security) {
            showFailedMessage(USED_FAILED, security);
            result = USED_FAILED;
        } catch (IOException io) {
            showFailedMessage(WRITE_FAILED, io);
            result = WRITE_FAILED;
        } catch (Exception e) {
            showFailedMessage(UNKNOWN, e);
            result = UNKNOWN;
        } finally {
            if ((result != NAME_ALREADY_EXISTS) && (result != PATH_ALREADY_EXISTS)) {
                refreshUIAndClearListLock();
            }
        }
        return result;
    }

    public void deleteSelectedAccount() {
        if (getSelectedAccount() > -1) {
            deleteAccount(getSelectedAccount());
        }
    }

    public int deleteAccount(int accountId) {

        int result = -1;
        File lockFile = new File(lockProfileListFilePath);
        try {
            long time = System.currentTimeMillis();
            while ((!lockFile.createNewFile()) && (System.currentTimeMillis() < time + MAX_TIME_TO_WAIT)) {
                showFailedMessage(USED_FAILED, null);
                Thread.sleep(TIME_TO_WAIT);
            }
            if (System.currentTimeMillis() >= time + MAX_TIME_TO_WAIT) {
                clearAccountListLock();
                lockFile.createNewFile();
            }
            // be sure to be up to date
            int reload = this.loadAccounts();
            switch (reload) {

                // no error on reload : remove account, save and remove lock
                case 0:
                    this.accounts.remove(Integer.valueOf(accountId));
                    this.saveAccounts();
                    try {
                        lockFile.delete();
                    } catch (SecurityException security) {
                        showFailedMessage(REMOVE_LOCK_FAILED, security);
                        result = REMOVE_LOCK_FAILED;
                    }
                    result = accountId;
                    sessions.remove(Integer.valueOf(accountId));
                    break;

                // file locked : try again later
                case USED_FAILED:
                    showFailedMessage(USED_FAILED, null);
                    result = USED_FAILED;
                    break;

                // any other exception : this was not expected
                default:
                    showFailedMessage(UNKNOWN, null);
                    result = UNKNOWN;

            }
        } catch (SecurityException security) {
            result = USED_FAILED;
            showFailedMessage(USED_FAILED, security);
        } catch (IOException io) {
            showFailedMessage(WRITE_FAILED, io);
            result = WRITE_FAILED;
        } catch (Exception e) {
            showFailedMessage(UNKNOWN, e);
            result = UNKNOWN;
        } finally {
            refreshUIAndClearListLock();
        }
        return result;
    }

    private Account[] getAccounts() {
        List<Integer> accountList = new ArrayList<Integer>();
        accountList.addAll(accounts.keySet());
        Collections.sort(accountList);
        Account[] accountArray = new Account[accountList.size()];
        for (int i = 0; i < accountArray.length; i++) {
            accountArray[i] = accounts.get(accountList.get(i));
        }
        return accountArray;
    }

    public Account getAccount(int id) {
        return accounts.get(Integer.valueOf(id));
    }

    private int getNewId() {
        int id = 0;
        Account[] accountArray = this.getAccounts();
        if (accountArray.length > 0) {
            id = accountArray[accountArray.length - 1].getId() + 1;
        }
        return id;
    }

    /**
     * Returns the id of the selected account.
     * 
     * @return An int. {@link #ACCOUNT_SELECTION_CANCELED} if account selection was canceled,
     *         {@link #NO_ACCOUNT_SELECTED} if no account was selected, or the selected account id if an account was
     *         selected
     */
    public int getSelectedAccount() {
        return this.selectedAccount;
    }

    public void setSelectedAccount(int id) {
        this.selectedAccount = id;
    }

    public void launch() {
        if (selectedAccount > -1) {
            launchAccount();
        } else {
            accountThread.cancel();
        }
    }

    public String getSelectedAccountWorkingPath() {
        String path = null;
        if (getSelectedAccount() >= 0) {
            Session session = sessions.get(Integer.valueOf(this.getSelectedAccount()));
            if (session != null) {
                path = session.getDirectory();
            }
        }
        return path;
    }

    /**
     * Use this method to get original path. You only need this method in case of multi session management, to load
     * information from the original path.
     * 
     * @return a {@link String}
     */
    public String getSelectedAccountOriginalPath() {
        String path = null;
        if (getSelectedAccount() >= 0) {
            Account account = accounts.get(Integer.valueOf(this.getSelectedAccount()));
            if (account != null) {
                path = account.getPath();
            }
        }
        return path;
    }

    public String getSelectedAccountName() {
        String name;
        if (this.getSelectedAccount() < 0) {
            name = null;
        } else {
            name = accounts.get(Integer.valueOf(this.getSelectedAccount())).getName();
        }
        return name;
    }

    private void showFailedMessage(int failureType, Throwable error) {
        String title = getMessage("user.manager.Account.Error.Title");
        String message = ObjectUtils.EMPTY_STRING;
        switch (failureType) {
            case CREATION_FAILED:
                message = getMessage("user.manager.Account.Error.File.Creation");
                break;
            case WRITE_FAILED:
                message = getMessage("user.manager.Account.Error.File.Write");
                break;
            case READ_FAILED:
                message = getMessage("user.manager.Account.Error.File.Read");
                break;
            case USED_FAILED:
                message = getMessage("user.manager.Account.Error.File.Used");
                break;
            case LOCK_FAILED:
                message = getMessage("user.manager.Account.Error.File.Lock");
                break;
            case LOCK_FOLDER_FAILED:
                message = getMessage("user.manager.Account.Error.File.Lock.Definitive");
                break;
            case REMOVE_LOCK_FAILED:
                message = getMessage("user.manager.Account.Error.File.Lock.Remove");
                break;
            case ACCOUNT_ACCES_DATE_UPDATE_FAILED:
                message = getMessage("user.manager.Account.Error.AccessDate.Write");
                break;
            default:
                message = getMessage("user.manager.Account.Error.Default");
                break;
        }
        if (error == null) {
            LOGGER.error(message);
        } else {
            LOGGER.error(message, error);
        }
        errorDialog = new DefaultUserManagementErrorDialog(accountSelectionFrame, title, message);
        errorDialog.setLocationRelativeTo(accountSelectionFrame);
        errorDialog.setVisible(true);
    }

    private void saveAccounts() throws Exception {
        Account[] accountArray = getAccounts();
        XMLLine openingLine = new XMLLine(mainNodeTag, XMLLine.OPENING_TAG_CATEGORY);
        StringBuilder content = openingLine.toAppendable(new StringBuilder());
        for (int i = 0; i < accountArray.length; i++) {
            content.append(XMLUtils.CRLF).append(INDENT);
            accountArray[i].toXMLStringBuffer(content);
        }
        XMLLine closingLine = new XMLLine(mainNodeTag, XMLLine.CLOSING_TAG_CATEGORY);
        content.append(XMLUtils.CRLF);
        closingLine.toAppendable(content);
        XMLUtils.save(content.toString(), accountFilePath);
    }

    public AccountSelectionFrame getAccountSelectionDialog() {
        return accountSelectionFrame;
    }

    public AccountCreationDialog getAccountCreationDialog() {
        return accountCreationDialog;
    }

    public String getMessage(String key) {
        String value = Application.getMessageManager(classLoader).getMessage(key);
        if (value != null) {
            value = value.replace("%application", applicationKey);
        }
        return value;
    }

    public Icon getIcon(String key) {
        return icons.getIcon(key);
    }

    private void refreshUIAndClearListLock() {
        clearAccountListLock();
        accountSelectionFrame.getSelectionPanel().getSelectionListPanel().setAcountList(getAccounts(),
                getSelectedAccount());
        if (getAccounts() != null && getAccounts().length > 0) {
            accountSelectionFrame.getSelectionPanel().getSelectionActionPanel().getOkButton().setEnabled(true);
            accountSelectionFrame.getSelectionPanel().getSelectionActionPanel().getDeleteButton().setEnabled(true);
        } else {
            accountSelectionFrame.getSelectionPanel().getSelectionActionPanel().getOkButton().setEnabled(false);
            accountSelectionFrame.getSelectionPanel().getSelectionActionPanel().getDeleteButton().setEnabled(false);
        }
        accountSelectionFrame.repaint();
    }

    public boolean isManageMultiSession() {
        return manageMultiSession;
    }

    public ImageIcon getApplicationIcon() {
        return applicationIcon;
    }

    public void launchAccount() {
        boolean validated = false;
        String path = null;
        int selected = getSelectedAccount();
        Integer id = Integer.valueOf(selected);
        if (selected > -1) {
            boolean created;
            boolean withError = false;
            Account account = accounts.get(id);
            loadAccounts();
            Account tmp = accounts.get(id);
            if (tmp == null) {
                accounts.put(id, tmp);
            } else {
                account = tmp;
            }
            account.setLastAccessDate(System.currentTimeMillis());
            try {
                saveAccounts();
            } catch (Exception e) {
                showFailedMessage(ACCOUNT_ACCES_DATE_UPDATE_FAILED, e);
            }
            path = account.getPath();
            File lockFile = new File(path + File.separator + LOCK_FILE_NAME);
            IOException io = null;
            try {
                File directory = new File(path);
                if (!directory.exists()) {
                    withError = !directory.mkdirs();
                }
                if (withError) {
                    created = false;
                } else {
                    created = lockFile.createNewFile();
                    withError = false;
                }
            } catch (IOException e) {
                io = e;
                created = false;
                withError = true;
            }
            if (withError) {
                showFailedMessage(LOCK_FOLDER_FAILED, io);
            } else {
                if (!created) {
                    if (manageMultiSession && accountSelectionFrame.getSelectionPanel().isMultiSession()) {
                        File directory;
                        for (int i = 1; (i < Integer.MAX_VALUE) && (!created); i++) {
                            String sessionPath = path + File.separatorChar + "session" + i;
                            directory = new File(sessionPath);
                            directory.mkdirs();// does not create the directory if it already exists
                            lockFile = new File(sessionPath + File.separatorChar + ".lock");
                            try {
                                if (lockFile.createNewFile()) {
                                    created = true;
                                    path = sessionPath;
                                    lockDialog = new LockUserManagemenErrortDialog(accountSelectionFrame,
                                            getMessage("user.manager.Account.Session.Warning.Title"),
                                            getMessage("user.manager.Account.Session.Locked.Warning"), this,
                                            LockUserManagemenErrortDialog.STYLE_OK_CANCEL_REMOVE);
                                    lockDialog.setLocationRelativeTo(accountSelectionFrame);
                                    lockDialog.setVisible(true);
                                    switch (lockDialog.getState()) {
                                        case LockUserManagemenErrortDialog.STATE_OK:
                                            created = true;
                                            path = sessionPath;
                                            break;
                                        case LockUserManagemenErrortDialog.STATE_REMOVE:
                                            created = true;
                                            clearLock(sessionPath);
                                            lockFile = new File(account.getPath() + File.separator + LOCK_FILE_NAME);
                                            lockFile.createNewFile();
                                            break;
                                        default:
                                            created = false;
                                            clearLock(sessionPath);
                                            break;
                                    }
                                    break;
                                } else {
                                    lockFile = new File(lockFile.getParent());
                                }
                            } catch (IOException e) {
                                // continue
                                LOGGER.error("Failed to create " + lockFile.getPath(), e);
                                lockFile = new File(lockFile.getParent());
                            }
                            sessionPath = null;
                        }
                        if (!created) {
                            path = null;
                        }
                    } else {
                        lockDialog = new LockUserManagemenErrortDialog(accountSelectionFrame,
                                getMessage("user.manager.Account.Session.Error.Title"),
                                getMessage("user.manager.Account.Session.Locked"), this,
                                LockUserManagemenErrortDialog.STYLE_CANCEL_REMOVE);
                        lockDialog.setLocationRelativeTo(accountSelectionFrame);
                        lockDialog.setVisible(true);
                        if (lockDialog.getState() == LockUserManagemenErrortDialog.STATE_REMOVE) {
                            if (clearSelectedLock()) {
                                created = true;
                                lockFile = new File(account.getPath() + File.separator + LOCK_FILE_NAME);
                                try {
                                    lockFile.createNewFile();
                                } catch (IOException e) {
                                    LOGGER.error("Failed to create " + lockFile.getPath(), e);
                                }
                            } else {
                                path = null;
                                created = false;
                            }
                        } else {
                            path = null;
                            created = false;
                        }
                    }
                }
                validated = created;
            }
            if (validated) {
                accountThread.cancel();
                sessions.put(id, new Session(path));
                clearAccountListLock();
                accountSelectionFrame.setVisible(false);
            } else {
                sessions.remove(id);
            }
        }
    }

    private void clearAccountListLock() {
        new File(lockProfileListFilePath).delete();
    }

    public void cancelAccountSelection() {
        setSelectedAccount(ACCOUNT_SELECTION_CANCELED);
        launch();
        clearAccountListLock();
        accountSelectionFrame.setVisible(false);
        accountThread.cancel();
    }

    public boolean clearSelectedLock() {
        boolean cleared;
        if (getSelectedAccount() > -1) {
            cleared = clearLock(accounts.get(Integer.valueOf(this.getSelectedAccount())).getPath());
        } else {
            cleared = false;
        }
        return cleared;
    }

    /**
     * Removes the lock file. This method should be called on application end.
     * 
     * @param directoryPath Path of the directory that contains the lock file.
     */
    public boolean clearLock(String directoryPath) {
        boolean cleared = true;
        if (directoryPath != null) {
            File lock = new File(directoryPath + File.separator + LOCK_FILE_NAME);
            if (lock.exists()) {
                try {
                    lock.delete();
                } catch (SecurityException e) {
                    LOGGER.error("Failed to clear " + lock.getPath(), e);
                    cleared = false;
                }
            }
        }
        return cleared;
    }

    /**
     * Sets the {@link Color} of the account selection/creation dialog.
     * 
     * @param color The {@link Color} to set
     */
    public void setUIColor(Color color) {
        accountCreationDialog.setGlobalBackground(color);
        accountSelectionFrame.setGlobalBackground(color);
    }

    public void updateLabelPath() {
        getAccountSelectionDialog().getSelectionPanel().getSelectionListPanel().updateLabelPath();
    }

    @Override
    public void selectionChanged(AccountSelectionEvent event) {
        // XXX Don't cancel account selection when no selected account:
        // it happens on new accounts and we don't want to start application.
        if (event != null) {
            if (event.isSelectionCanceled()) {
                setSelectedAccount(ACCOUNT_SELECTION_CANCELED);
                launch();
            } else if (event.getSelectedAccount() != null) {
                setSelectedAccount(event.getSelectedAccount().getId());
            }
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class AccountThread extends Thread {
        protected volatile boolean canceled;

        public AccountThread() {
            super("Account selection waiting Thread");
            canceled = false;
        }

        @Override
        public void run() {
            while (!canceled) {
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    canceled = true;
                }
            }
        }

        public void cancel() {
            canceled = true;
        }

    }

}
