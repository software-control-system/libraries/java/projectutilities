/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user;

import java.text.SimpleDateFormat;
import java.util.Date;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.Application;
import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * This class determines every necessary information for a user account
 * 
 * @author SOLEIL
 */
public class Account implements IDateConstants {

    public final static String PROFILE_TAG = "PROFILE";
    public final static String ID_TAG = "ID";
    public final static String NAME_TAG = "NAME";
    public final static String PATH_TAG = "PATH";
    public final static String LAST_ACCESS_DATE_TAG = "LAST_ACCESS_DATE";

    // This value represents January 2004 the 1st at 00:00:00.000
    private static long defaultLastAccessDate = 1072911600000L;
    private final int id;
    private final String name;
    private final String path;
    private final SimpleDateFormat dateFormat;
    private long lastAccessDate;

    /**
     * Constructor of a profile
     * 
     * @param id id of the profile
     * @param name
     * @param path
     */
    public Account(int id, String name, String path, long lastAccessDate) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.dateFormat = new SimpleDateFormat(US_DATE_FORMAT);
        this.lastAccessDate = lastAccessDate;
    }

    /**
     * Constructor of a dummy profile. Only useful to make the "new Profile" node in profile selection tree
     */
    public Account() {
        this(-1, Application.getMessageManager(Account.class.getClassLoader()).getMessage("user.manager.Account.New"),
                ObjectUtils.EMPTY_STRING, defaultLastAccessDate);
    }

    /**
     * @return The id of the profile as an <code>int</code>
     */
    public int getId() {
        return this.id;
    }

    /**
     * @return The name of the profile as <code>String</code>
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return The path of the profile as <code>String</code>
     */
    public String getPath() {
        return this.path;
    }

    @Override
    public String toString() {
        return this.getName();
    }

    @Override
    public int hashCode() {
        int code = 0xACC011;
        int mult = 0x27;
        code = code * mult + id;
        return code;
    }

    public long getLastAccessDate() {
        return lastAccessDate;
    }

    public void setLastAccessDate(long lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

    @Override
    public boolean equals(Object o) {
        boolean equals;
        if (o == null) {
            equals = false;
        } else if (o instanceof Account) {
            equals = (this.getId() == ((Account) o).getId());
        } else {
            equals = false;
        }
        return equals;
    }

    /**
     * Returns an xml line
     * 
     * @return An xml line. Used to save this profile.
     */
    public String toXMLString() {
        return toXMLStringBuffer(null).toString();
    }

    public <T extends Appendable> T toXMLStringBuffer(T buffer) {
        XMLLine line = new XMLLine(PROFILE_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        line.setAttribute(ID_TAG, Integer.toString(getId()));
        line.setAttribute(NAME_TAG, getName());
        line.setAttribute(PATH_TAG, getPath());
        line.setAttribute(LAST_ACCESS_DATE_TAG, dateFormat.format(new Date(getLastAccessDate())));
        return line.toAppendable(buffer);
    }

    public static long getDefaultLastAccessDate() {
        return defaultLastAccessDate;
    }

    public static void setDefaultLastAccessDate(long defaultLastAccessDate) {
        Account.defaultLastAccessDate = defaultLastAccessDate;
    }

}
