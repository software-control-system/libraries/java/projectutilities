/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.dialog;

import java.awt.Color;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A JDialog used to display some errors or warnings
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AbstractUserManagementErrorDialog extends JDialog {

    private static final long serialVersionUID = -3688256974290554828L;

    protected final static Color ERROR_COLOR = new Color(255, 255, 150);
    protected String message;
    protected JPanel mainPanel;

    /**
     * Constructor
     * 
     * @param title the dialog title
     * @param message the error/warning message
     */
    public AbstractUserManagementErrorDialog(String title, String message) {
        super((Frame) null, title, true);
        this.message = message;
        initialize();
    }

    /**
     * Constructor
     * 
     * @param frame the parent frame
     * @param title the dialog title
     * @param message the error/warning message
     */
    public AbstractUserManagementErrorDialog(Frame frame, String title, String message) {
        super(frame, title, true);
        this.message = message;
        initialize();
    }

    /**
     * Constructor
     * 
     * @param dialog the parent JDialog
     * @param title the dialog title
     * @param message the error/warning message
     */
    public AbstractUserManagementErrorDialog(JDialog dialog, String title, String message) {
        super(dialog, title, true);
        this.message = message;
        initialize();
    }

    protected void initialize() {
        String[] messages = message.split(ObjectUtils.NEW_LINE);
        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBackground(ERROR_COLOR);

        JLabel[] fields = new JLabel[messages.length];
        this.setContentPane(mainPanel);
        for (int labelIndex = 0; labelIndex < messages.length; labelIndex++) {
            JLabel label = new JLabel(messages[labelIndex], JLabel.CENTER);
            label.setBackground(ERROR_COLOR);
            fields[labelIndex] = label;
            GridBagConstraints labelConstraints = new GridBagConstraints();
            labelConstraints.fill = GridBagConstraints.HORIZONTAL;
            labelConstraints.gridx = 0;
            labelConstraints.gridy = labelIndex;
            labelConstraints.weightx = 1;
            labelConstraints.weighty = 0;
            labelConstraints.gridwidth = GridBagConstraints.REMAINDER;
            mainPanel.add(label, labelConstraints);
        }
    }

}
