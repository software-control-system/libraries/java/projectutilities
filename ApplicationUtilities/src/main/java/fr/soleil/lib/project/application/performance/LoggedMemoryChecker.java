/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.performance;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.swing.AMemoryChecker;

/**
 * An {@link AMemoryChecker} that logs errors if any.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LoggedMemoryChecker extends AMemoryChecker {

    private static final long serialVersionUID = -193616911651208853L;

    protected static final String DEFAULT_ERROR_MESSAGE = DEFAULT_MESSAGE_MANAGER
            .getMessage("fr.soleil.lib.project.swing.memory.check.error");

    private final Logger logger;

    public LoggedMemoryChecker() {
        this(null);
    }

    public LoggedMemoryChecker(String loggerId) {
        super();
        logger = LoggerFactory.getLogger(loggerId == null ? LoggedMemoryChecker.class.getSimpleName() : loggerId);
    }

    @Override
    protected void treatError(Throwable t) {
        logger.error(DEFAULT_ERROR_MESSAGE, t);
    }

    public static void main(String[] args) {
        LoggedMemoryChecker checker = new LoggedMemoryChecker();
        checker.setGCButtonVisible(true);
        JFrame testFrame = new JFrame(checker.getClass().getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setContentPane(checker);
        testFrame.pack();
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
        checker.start();
    }

}
