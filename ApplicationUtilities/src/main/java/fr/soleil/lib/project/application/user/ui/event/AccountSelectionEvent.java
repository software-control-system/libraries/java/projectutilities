/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.event;

import java.util.EventObject;

import fr.soleil.lib.project.application.user.Account;
import fr.soleil.lib.project.application.user.ui.listener.IAccountSelectionListener;
import fr.soleil.lib.project.application.user.ui.panel.AccountSelectionListPanel;

/**
 * An {@link EventObject} used to notify {@link IAccountSelectionListener}s for some changes in account selection.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AccountSelectionEvent extends EventObject {

    private static final long serialVersionUID = -5170653561500302721L;

    protected final Account selectedAccount;
    protected final boolean selectionCanceled;

    public AccountSelectionEvent(AccountSelectionListPanel panel, Account selectedAccount, boolean selectionCanceled) {
        super(panel);
        this.selectedAccount = selectedAccount;
        this.selectionCanceled = selectionCanceled;
    }

    @Override
    public AccountSelectionListPanel getSource() {
        return (AccountSelectionListPanel) super.getSource();
    }

    /**
     * Returns the selected {@link Account}
     * 
     * @return An {@link Account}
     */
    public Account getSelectedAccount() {
        return selectedAccount;
    }

    /**
     * Returns whether account selection was canceled
     * 
     * @return A <code>boolean</code>
     */
    public boolean isSelectionCanceled() {
        return selectionCanceled;
    }
}
