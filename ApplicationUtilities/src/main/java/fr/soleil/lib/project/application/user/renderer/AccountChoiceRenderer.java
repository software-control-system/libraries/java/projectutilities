/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.renderer;

import java.awt.Component;
import java.lang.ref.WeakReference;
import java.util.Comparator;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.user.Account;
import fr.soleil.lib.project.application.user.comparator.AccountDateComparator;
import fr.soleil.lib.project.application.user.comparator.AccountIdComparator;
import fr.soleil.lib.project.application.user.comparator.AccountNameComparator;
import fr.soleil.lib.project.application.user.manager.AccountManager;

/**
 * A renderer that can be used with {@link JComboBox}es containing {@link Account} {@link Comparator}s.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AccountChoiceRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = 8373918434745509506L;

    private final WeakReference<AccountManager> managerRef;

    public AccountChoiceRenderer(AccountManager manager) {
        super();
        managerRef = (manager == null ? null : new WeakReference<AccountManager>(manager));
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends Object> list, Object value, int index,
            boolean isSelected, boolean cellHasFocus) {
        JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        AccountManager manager = ObjectUtils.recoverObject(managerRef);
        if (manager != null) {
            if (value instanceof AccountDateComparator) {
                label.setText(manager.getMessage("user.manager.Account.Sort.Date"));
            } else if (value instanceof AccountNameComparator) {
                label.setText(manager.getMessage("user.manager.Account.Sort.Name"));
            } else if (value instanceof AccountIdComparator) {
                label.setText(manager.getMessage("user.manager.Account.Sort.Id"));
            }
        }
        return label;
    }

}
