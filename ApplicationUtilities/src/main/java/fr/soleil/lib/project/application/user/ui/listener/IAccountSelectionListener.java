/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.listener;

import java.util.EventListener;

import fr.soleil.lib.project.application.user.ui.event.AccountSelectionEvent;

/**
 * An {@link EventListener} that listens for changes in account selection
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface IAccountSelectionListener extends EventListener {

    public void selectionChanged(AccountSelectionEvent event);

}
