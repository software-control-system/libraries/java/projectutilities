/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.panel;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.user.Account;
import fr.soleil.lib.project.application.user.comparator.AccountDateComparator;
import fr.soleil.lib.project.application.user.comparator.AccountIdComparator;
import fr.soleil.lib.project.application.user.comparator.AccountNameComparator;
import fr.soleil.lib.project.application.user.manager.AccountManager;
import fr.soleil.lib.project.application.user.renderer.AccountChoiceRenderer;
import fr.soleil.lib.project.application.user.ui.event.AccountSelectionEvent;
import fr.soleil.lib.project.application.user.ui.listener.IAccountSelectionListener;

/**
 * The JPanel that contains the Combobox that lists the known accounts + the
 * label that displays the account path
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AccountSelectionListPanel extends JPanel implements ItemListener {

    private static final long serialVersionUID = 7120874464106396180L;

    private static final String FONT_NAME = "Arial";

    private static final Font PATH_FONT = new Font(FONT_NAME, Font.ITALIC, 11);
    private static final Font ACCOUNT_SORT_TITLE_FONT = new Font(FONT_NAME, Font.PLAIN, 11);
    private static final Font ACCOUNT_SORT_CHOICE_FONT = new Font(FONT_NAME, Font.ITALIC, 10);

    private String path;
    private final String errorPath;
    private final JLabel chooseIconLabel, pathLabel;
    private final JCheckBox showPathCheckBox;
    private final JPanel pathPanel;
    private final JComboBox<Account> chooseCombo;
    private final JLabel accountSortChoiceLabel;
    private final JComboBox<Comparator<Account>> accountSortChoiceCombo;
    private final Collection<IAccountSelectionListener> listeners;

    public AccountSelectionListPanel(AccountManager manager) {
        super(new GridBagLayout());
        listeners = Collections.newSetFromMap(new ConcurrentHashMap<IAccountSelectionListener, Boolean>());
        addAccountSelectionListener(manager);
        errorPath = manager.getMessage("user.manager.Account.Path.Undefined");
        chooseIconLabel = new JLabel(manager.getIcon("user.manager.Account.Choose"));
        chooseCombo = new JComboBox<>();
        chooseCombo.addItemListener(this);

        accountSortChoiceLabel = new JLabel(manager.getMessage("user.manager.Account.Sort"));
        accountSortChoiceLabel.setFont(ACCOUNT_SORT_TITLE_FONT);
        accountSortChoiceCombo = new JComboBox<>();
        accountSortChoiceCombo.setRenderer(new AccountChoiceRenderer(manager));
        accountSortChoiceCombo.setFont(ACCOUNT_SORT_CHOICE_FONT);
        accountSortChoiceCombo.addItem(new AccountDateComparator());
        accountSortChoiceCombo.addItem(new AccountNameComparator());
        accountSortChoiceCombo.addItem(new AccountIdComparator());
        accountSortChoiceCombo.addItemListener(this);

        pathPanel = new JPanel(new GridBagLayout());
        showPathCheckBox = new JCheckBox(manager.getMessage("user.manager.Account.Path.Show"));
        showPathCheckBox.setFont(PATH_FONT);
        showPathCheckBox.addActionListener(new VisiblePathAdapter());
        pathLabel = new JLabel();
        GridBagConstraints showPathCheckBoxConstraints = new GridBagConstraints();
        showPathCheckBoxConstraints.fill = GridBagConstraints.HORIZONTAL;
        showPathCheckBoxConstraints.gridx = 0;
        showPathCheckBoxConstraints.gridy = 0;
        showPathCheckBoxConstraints.weightx = 1;
        showPathCheckBoxConstraints.weighty = 0;
        pathPanel.add(showPathCheckBox, showPathCheckBoxConstraints);
        GridBagConstraints pathLabelConstraints = new GridBagConstraints();
        pathLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        pathLabelConstraints.gridx = 0;
        pathLabelConstraints.gridy = 1;
        pathLabelConstraints.weightx = 1;
        pathLabelConstraints.weighty = 0;
        pathLabelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        pathPanel.add(pathLabel, pathLabelConstraints);

        GridBagConstraints chooseIconLabelConstraints = new GridBagConstraints();
        chooseIconLabelConstraints.fill = GridBagConstraints.NONE;
        chooseIconLabelConstraints.gridx = 0;
        chooseIconLabelConstraints.gridy = 0;
        chooseIconLabelConstraints.weightx = 0;
        chooseIconLabelConstraints.weighty = 0;
        add(chooseIconLabel, chooseIconLabelConstraints);
        GridBagConstraints chooseComboConstraints = new GridBagConstraints();
        chooseComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        chooseComboConstraints.gridx = 1;
        chooseComboConstraints.gridy = 0;
        chooseComboConstraints.weightx = 1;
        chooseComboConstraints.weighty = 0;
        add(chooseCombo, chooseComboConstraints);
        GridBagConstraints accountSortChoiceLabelConstraints = new GridBagConstraints();
        accountSortChoiceLabelConstraints.fill = GridBagConstraints.NONE;
        accountSortChoiceLabelConstraints.gridx = 2;
        accountSortChoiceLabelConstraints.gridy = 0;
        accountSortChoiceLabelConstraints.weightx = 0;
        accountSortChoiceLabelConstraints.weighty = 0;
        accountSortChoiceLabelConstraints.insets = new Insets(0, 10, 0, 0);
        add(accountSortChoiceLabel, accountSortChoiceLabelConstraints);
        GridBagConstraints accountSortChoiceComboConstraints = new GridBagConstraints();
        accountSortChoiceComboConstraints.fill = GridBagConstraints.NONE;
        accountSortChoiceComboConstraints.gridx = 3;
        accountSortChoiceComboConstraints.gridy = 0;
        accountSortChoiceComboConstraints.weightx = 0;
        accountSortChoiceComboConstraints.weighty = 0;
        add(accountSortChoiceCombo, accountSortChoiceComboConstraints);

        GridBagConstraints pathPanelConstraints = new GridBagConstraints();
        pathPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        pathPanelConstraints.gridx = 0;
        pathPanelConstraints.gridy = 1;
        pathPanelConstraints.weightx = 1;
        pathPanelConstraints.weighty = 0;
        pathPanelConstraints.insets = new Insets(5, 0, 5, 5);
        pathPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        add(pathPanel, pathPanelConstraints);
    }

    public void addAccountSelectionListener(IAccountSelectionListener listener) {
        if (listener != null) {
            listeners.add(listener);
        }
    }

    public void removeAccountSelectionListener(IAccountSelectionListener listener) {
        if (listener != null) {
            listeners.remove(listener);
        }
    }

    public void warnForSelectedAccount() {
        warnListeners(new AccountSelectionEvent(this, (Account) chooseCombo.getSelectedItem(), false));
    }

    public void warnForSelectionCanceled() {
        warnListeners(new AccountSelectionEvent(this, null, true));
    }

    protected void warnListeners(AccountSelectionEvent event) {
        for (IAccountSelectionListener listener : listeners) {
            listener.selectionChanged(event);
        }
    }

    public void selectAccount(Account account) {
        if (account != null) {
            int index = -1;
            for (int i = 0; i < chooseCombo.getItemCount(); i++) {
                if (account.equals(chooseCombo.getItemAt(i))) {
                    index = i;
                    break;
                }
            }
            if (index > -1) {
                chooseCombo.setSelectedIndex(index);
            }
        }
    }

    /**
     * reloads account list
     */
    @SuppressWarnings("unchecked")
    public void setAcountList(Account[] accounts, int selectedId) {
        chooseCombo.removeItemListener(this);
        chooseCombo.removeAllItems();
        Arrays.sort(accounts, (Comparator<Account>) accountSortChoiceCombo.getSelectedItem());
        for (int i = 0; i < accounts.length; i++) {
            chooseCombo.addItem(accounts[i]);
            if (selectedId == accounts[i].getId()) {
                chooseCombo.setSelectedItem(accounts[i]);
            }
        }
        updateLabelPath();
        chooseCombo.addItemListener(this);
        warnForSelectedAccount();
    }

    public void updateLabelPath() {
        boolean selected = showPathCheckBox.isSelected();
        if (selected) {
            String path = pathString();
            pathLabel.setText(path);
            pathLabel.setToolTipText(path);
        } else {
            pathLabel.setText(ObjectUtils.EMPTY_STRING);
            pathLabel.setToolTipText(null);
        }
        revalidate();
    }

    /**
     * A convenient method to prepare the path label
     * 
     * @return The String to be displayed in path label
     */
    public String pathString() {
        path = errorPath;
        if (chooseCombo != null) {
            Account selected = (Account) chooseCombo.getSelectedItem();
            if (selected != null) {
                path = selected.getPath();
                if (path == null) {
                    path = errorPath;
                }
            }
        }
        return path;
    }

    /**
     * Returns the selected Account
     * 
     * @return The selected Account
     */
    public int getSelectedAccount() {
        int selected;
        if (chooseCombo.getSelectedItem() == null) {
            selected = -1;
        } else {
            return ((Account) chooseCombo.getSelectedItem()).getId();
        }
        return selected;
    }

    private class VisiblePathAdapter implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            updateLabelPath();
        }
    }

    public void setGlobalBackground(Color bg) {
        setBackground(bg);
        chooseCombo.setBackground(bg);
        chooseIconLabel.setBackground(bg);
        accountSortChoiceCombo.setBackground(bg);
        accountSortChoiceLabel.setBackground(bg);
        pathPanel.setBackground(bg);
        pathLabel.setBackground(bg);
        showPathCheckBox.setBackground(bg);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if ((e != null) && (e.getStateChange() == ItemEvent.SELECTED)) {
            if (e.getSource() == chooseCombo) {
                updateLabelPath();
                warnForSelectedAccount();
            } else if (e.getSource() == accountSortChoiceCombo) {
                Account[] accounts = new Account[chooseCombo.getItemCount()];
                for (int i = 0; i < accounts.length; i++) {
                    accounts[i] = chooseCombo.getItemAt(i);
                }
                // sort accounts again and change selected account to the 1st one according selected sort algorithm
                setAcountList(accounts, -1);
            }
        }
    }

}
