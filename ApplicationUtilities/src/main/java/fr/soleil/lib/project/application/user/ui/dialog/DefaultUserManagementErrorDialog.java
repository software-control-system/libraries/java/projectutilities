/*
 * This file is part of ApplicationUtilities.
 * 
 * ApplicationUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser
 * General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * ApplicationUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with ApplicationUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.application.user.ui.dialog;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;

/**
 * A JDialog used to display some errors or warnings
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DefaultUserManagementErrorDialog extends AbstractUserManagementErrorDialog {

    private static final long serialVersionUID = -3049101184418729387L;

    /**
     * Constructor
     * 
     * @param title the dialog title
     * @param message the error/warning message
     */
    public DefaultUserManagementErrorDialog(String title, String message) {
        super(title, message);
    }

    /**
     * Constructor
     * 
     * @param frame the parent frame
     * @param title the dialog title
     * @param message the error/warning message
     */
    public DefaultUserManagementErrorDialog(Frame frame, String title, String message) {
        super(frame, title, message);
    }

    /**
     * Constructor
     * 
     * @param dialog the parent JDialog
     * @param title the dialog title
     * @param message the error/warning message
     */
    public DefaultUserManagementErrorDialog(JDialog dialog, String title, String message) {
        super(dialog, title, message);
    }

    @Override
    protected void initialize() {
        super.initialize();
        JButton ok = new JButton("ok");
        ok.setOpaque(false);
        ok.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultUserManagementErrorDialog.this.setVisible(false);
                DefaultUserManagementErrorDialog.this.dispose();
            }

        });
        GridBagConstraints okConstraints = new GridBagConstraints();
        okConstraints.fill = GridBagConstraints.HORIZONTAL;
        okConstraints.gridx = 0;
        okConstraints.gridy = mainPanel.getComponentCount();
        okConstraints.weightx = 1;
        okConstraints.weighty = 0;
        okConstraints.insets = new Insets(5, 5, 0, 5);
        mainPanel.add(ok, okConstraints);
        pack();
    }

}
