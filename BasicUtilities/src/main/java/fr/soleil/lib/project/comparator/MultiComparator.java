/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.comparator;

import java.util.Comparator;

/**
 * A {@link Comparator} based on other ones.
 * Useful to sort according multiple criteria (sort on 1st criteria, then 2nd one, then 3rd one, etc...)
 * 
 * @author Rapha&euml;l GIRARDOT
 *
 * @param <T> The type of objects that may be compared by this comparator.
 */
public class MultiComparator<T> implements Comparator<T> {

    protected final Comparator<T>[] comparators;

    /**
     * Constructs a new {@link MultiComparator} with given {@link Comparator}s.
     * 
     * @param comparators The {@link Comparator}s. Compare order will be done in {@link Comparator} index order.
     */
    public MultiComparator(@SuppressWarnings("unchecked") Comparator<T>... comparators) {
        this.comparators = comparators;
    }

    @Override
    public int compare(T o1, T o2) {
        int comp = 0, index = 0;
        if (comparators != null) {
            while ((comp == 0) && (index < comparators.length)) {
                comp = comparators[index++].compare(o1, o2);
            }
        }
        return comp;
    }

}
