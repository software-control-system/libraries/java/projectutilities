/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.math.exception;

import fr.soleil.lib.project.math.ArraySet;

/**
 * An {@link Exception} that can be thrown when there is a problem with an {@link ArraySet} (example: initialization
 * failure)
 * 
 * @author huriez, Rapha&euml;l GIRARDOT
 * 
 */
public class ArraySetException extends Exception {

    private static final long serialVersionUID = -7156942225756476487L;

    public final static String INITIALIZATION_MESSAGE = "The ArraySet cannot be instanciated";
    public final static String DIMENSION_MESSAGE = "This ArraySet is too big to be stored in a single dimension array";

    public ArraySetException() {
        this(INITIALIZATION_MESSAGE);
    }

    public ArraySetException(String message) {
        super(message);
    }

    public ArraySetException(Throwable cause) {
        this(INITIALIZATION_MESSAGE + ": " + cause, cause);
    }

    public ArraySetException(String message, Throwable cause) {
        super(message, cause);
    }

}
