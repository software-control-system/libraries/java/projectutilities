/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.math;

import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A class that gives some usefull methods to interact with single/multi-dimensionalal arrays
 * 
 * @author Rapha&euml;l GIRARDOT, rodriguez
 */
public class ArrayUtils {

    public static final int[] EMPTY_SHAPE = new int[0];

    /**
     * Returns whether an {@link Object} is an array
     * 
     * @param array The {@link Object} to test
     * @return A <code>boolean</code>
     */
    public static boolean isArray(Object array) {
        return ((array != null) && array.getClass().isArray());
    }

    /**
     * Converts a mono-dimensional array into a multi-dimensional array of the same data type
     * (recovered by using {@link #recoverDataType(Object)}).
     * 
     * @param singleDimArray The mono-dimensional array
     * @param multiDimArrayShape The shape of the multi-dimensional array. Example: if you want a {@link String}
     *            <code>[dimZ][dimY][dimX]</code>, <code>multiDimArrayShape</code> should be
     *            <code>{dimZ, dimY, dimX}</code>
     * @return The expected multi-dimensional array. It will return <code>null</code> in any of
     *         these cases:
     *         <ul>
     *         <li><code>singleDimArray</code> is <code>null</code></li>
     *         <li><code>singleDimArray</code> is not a mono-dimensional array</li>
     *         <li><code>multiDimArrayShape</code> is <code>null</code></li>
     *         <li><code>multiDimArrayShape</code> is of length 0</li>
     *         <li><code>multiDimArrayShape</code> contains a negative value</li>
     *         <li><code>multiDimArrayShape</code> is too big to initialize such an array</li>
     *         <li><code>singleDimArray</code> length is not compatible with <code>multiDimArrayShape</code></li>
     *         </ul>
     * @see Array#newInstance(Class, int...)
     * @see #recoverDataType(Object)
     */
    public static Object convertArrayDimensionFrom1ToN(Object singleDimArray, int... multiDimArrayShape) {
        Object result = null;
        if (isArray(singleDimArray) && (multiDimArrayShape != null) && (multiDimArrayShape.length > 0)
                && (!singleDimArray.getClass().getComponentType().isArray())) {
            if (multiDimArrayShape.length == 1) {
                result = singleDimArray;
            } else {
                Class<?> dataType = recoverDataType(singleDimArray);
                if (Array.newInstance(dataType, 0).getClass().equals(singleDimArray.getClass())) {
                    int arrayLength = Array.getLength(singleDimArray);
                    int expectedLength = 1;
                    for (int currentLength : multiDimArrayShape) {
                        expectedLength *= currentLength;
                    }
                    if (arrayLength == expectedLength) {
                        try {
                            result = Array.newInstance(dataType, multiDimArrayShape);
                        } catch (Exception e) {
                            result = null;
                        }
                        if (result != null) {
                            // java initializes int values with 0 by default
                            int[] startPositions = new int[multiDimArrayShape.length];
                            reshapeArray(0, startPositions, multiDimArrayShape, singleDimArray, result, true);
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Converts a multi-dimensional array into a mono-dimensional array of the same data type
     * (recovered by using {@link #recoverDataType(Object)}).
     * 
     * @param multiDimArray The multi-dimensional array
     * @return The expected multi-dimensional array. It will return <code>null</code> in any of
     *         these cases:
     *         <ul>
     *         <li><code>multiDimArray</code> is <code>null</code></li>
     *         <li><code>multiDimArray</code> is not an array</li>
     *         <li><code>multiDimArray</code> is too big to be converted into a mono-dimensional array</li>
     *         </ul>
     * @see Array#newInstance(Class, int...)
     * @see #recoverDataType(Object)
     */
    public static Object convertArrayDimensionFromNTo1(Object multiDimArray) {
        Object result = null;
        if (isArray(multiDimArray)) {
            int[] multiDimArrayShape = recoverShape(multiDimArray);
            if ((multiDimArrayShape != null) && (multiDimArrayShape.length > 0)) {
                if (multiDimArrayShape.length == 1) {
                    result = multiDimArray;
                } else {
                    Class<?> dataType = recoverDataType(multiDimArray);
                    // java initializes int values with 0 by default
                    int[] startPositions = new int[multiDimArrayShape.length];
                    // compare with the smallest multi-dimensional array of the same expected type
                    if (Array.newInstance(dataType, startPositions).getClass().equals(multiDimArray.getClass())) {
                        int expectedLength = 1;
                        for (int currentLength : multiDimArrayShape) {
                            expectedLength *= currentLength;
                        }
                        result = Array.newInstance(dataType, expectedLength);
                        try {
                            reshapeArray(0, startPositions, multiDimArrayShape, result, multiDimArray, false);
                        } catch (Exception e) {
                            result = null;
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * This methods recovers the type of data present in a N dimension array.
     * 
     * @param array The array
     * @return The {@link Class} that represents the data type in the given array. (Example: if <code>array</code> is a
     *         <code>boolean[][]</code>, the result will be {@link Boolean#TYPE}). This method returns <code>null</code>
     *         if <code>array</code> is <code>null</code>.
     */
    public static Class<?> recoverDataType(Object array) {
        Class<?> result = null;
        if (array != null) {
            result = recoverDeepComponentType(array.getClass());
        }
        return result;
    }

    /**
     * This methods recovers the type of data present in a {@link Class} that represents N dimension
     * arrays.
     * 
     * @param arrayClass The {@link Class}
     * @return The {@link Class} that represents the data type in the given array. (Example: if <code>arrayClass</code>
     *         is <code>boolean[][]</code>, the result will be {@link Boolean#TYPE}). This method returns
     *         <code>null</code> if <code>arrayClass</code> is <code>null</code>.
     */
    public static Class<?> recoverDeepComponentType(Class<?> arrayClass) {
        Class<?> result = arrayClass;
        if (arrayClass != null) {
            while (result.isArray()) {
                result = result.getComponentType();
            }
        }
        return result;
    }

    /**
     * Recovers an array shape
     * 
     * @param array
     * @return An <code>int[]</code>. Returns <code>null</code> if <code>array</code> is <code>null</code>
     */
    public static int[] recoverShape(Object array) {
        int[] shape = null;
        if (array != null) {
            Class<?> type = recoverDataType(array);
            ArrayList<Integer> list = new ArrayList<Integer>();
            Object tempArray = array;
            boolean isArray = array.getClass().isArray();
            while (isArray) {
                int length = Array.getLength(tempArray);
                if (length > 0) {
                    list.add(length);
                    tempArray = Array.get(tempArray, 0);
                    isArray = isArray(tempArray) && type.equals(recoverDataType(tempArray));
                } else {
                    Class<?> arrayClass = tempArray.getClass();
                    while (arrayClass.isArray()) {
                        list.add(0);
                        arrayClass = arrayClass.getComponentType();
                    }
                    isArray = false;
                }
            }
            shape = new int[list.size()];
            for (int i = 0; i < shape.length; ++i) {
                shape[i] = list.get(i);
            }
            list.clear();
        }
        return shape;
    }

    /**
     * Recovers the rank of the arrays represented by a {@link Class}. For example, if the given {@link Class} is
     * {@link Class}<code>&lt;int[][]&gt;</code>, the result is 2. If it is {@link Class} <code>&lt;Object[]&gt;</code>,
     * the result is 1, etc... .
     * 
     * @param arrayClass The {@link Class} to test
     * @return An <code>int</code>. If <code>arrayClass</code> is <code>null</code>, the result is -1. If
     *         <code>arrayClass</code> does not represent an array, the result is 0. otherwise, the result is the
     *         expected rank.
     */
    public static int recoverArrayRank(Class<?> arrayClass) {
        int rank;
        if (arrayClass == null) {
            rank = -1;
        } else {
            rank = 0;
            while (arrayClass.isArray()) {
                rank++;
                arrayClass = arrayClass.getComponentType();
            }
        }
        return rank;
    }

    /**
     * Recursive method reshaping a filled mono-dimensional array into a multi-dimensional array, or
     * a filled multi-dimensional array into a mono-dimensional array.
     * 
     * @param dimIndex reshape all dimensions greater than this one
     * @param startPositions starting position in multi-dimensional array
     * @param dimensions dimensions' size of the multi-dimensional array
     * @param singleDimArray The mono-dimensional array
     * @param multiDimArray The multi-dimensional array
     * @param from1ToN A boolean to know the type of reshaping. If <code>TRUE</code>, then this
     *            method will reshape a filled mono-dimensional array into a multi-dimensional
     *            array. Otherwise, this method will reshape a filled multi-dimensional array into a
     *            mono-dimensional array.
     */
    private static void reshapeArray(int dimIndex, int[] startPositions, int[] dimensions, Object singleDimArray,
            Object multiDimArray, boolean from1ToN) {
        int lStartRaw;
        int lLinearStart;
        if (dimIndex == dimensions.length - 1) {
            lLinearStart = 0;
            for (int k = 1; k < dimensions.length; k++) {
                lStartRaw = 1;
                for (int j = k; j < dimensions.length; j++) {
                    lStartRaw *= dimensions[j];
                }
                lStartRaw *= startPositions[k - 1];
                lLinearStart += lStartRaw;
            }
            if ((singleDimArray != null) && (multiDimArray != null)) {
                if (from1ToN) {
                    System.arraycopy(singleDimArray, lLinearStart, multiDimArray, 0, dimensions[dimensions.length - 1]);
                } else {
                    System.arraycopy(multiDimArray, 0, singleDimArray, lLinearStart, dimensions[dimensions.length - 1]);
                }
            }
        } else {
            Object[] outputAsArray = (Object[]) multiDimArray;
            for (int i = 0; i < dimensions[dimIndex]; i++) {
                Object o = outputAsArray[i];
                reshapeArray(dimIndex + 1, startPositions, dimensions, singleDimArray, o, from1ToN);
                if (startPositions[dimIndex] < dimensions[dimIndex] - 1) {
                    startPositions[dimIndex] = i + 1;
                } else {
                    startPositions[dimIndex] = 0;
                }
            }
        }
    }

    /**
     * Tests whether two arrays (multi or mono dimensional) are equals
     * 
     * @param array1 One array to be tested for equality
     * @param array2 The other array to be tested for equality
     * @return <code>TRUE</code> if the two arrays are equal, <code>FALSE</code> otherwise
     */
    public static boolean equals(Object array1, Object array2) {
        boolean equals;
        if (array1 == null) {
            equals = (array2 == null);
        } else if (array2 != null) {
            Class<?> type1 = recoverDataType(array1);
            Class<?> type2 = recoverDataType(array2);
            int[] shape1 = recoverShape(array1);
            int[] shape2 = recoverShape(array2);
            if ((shape1 != null) && (shape2 != null) && (shape1.length > 0)) {
                if ((type1.equals(type2)) && Arrays.equals(shape1, shape2)) {
                    int[] startPositions = new int[shape1.length];
                    equals = areArraysEqual(0, startPositions, shape1, array1, array2);
                } else {
                    equals = false;
                }
            } else {
                equals = array1.equals(array2);
            }
        } else {
            equals = false;
        }
        return equals;
    }

    public static int hashCode(Object array) {
        int code = 0xA77;
        int mult = 0xA1;
        if (array == null) {
            code = 0;
        } else if (array.getClass().isArray()) {
            if (array.getClass().getComponentType().isPrimitive()) {
                if (array instanceof boolean[]) {
                    boolean[] foundArray = (boolean[]) array;
                    for (boolean value : foundArray) {
                        code = code * mult + ObjectUtils.generateHashCode(value);
                    }
                } else if (array instanceof char[]) {
                    char[] foundArray = (char[]) array;
                    for (char value : foundArray) {
                        code = code * mult + value;
                    }
                } else if (array instanceof byte[]) {
                    byte[] foundArray = (byte[]) array;
                    for (byte value : foundArray) {
                        code = code * mult + value;
                    }
                } else if (array instanceof short[]) {
                    short[] foundArray = (short[]) array;
                    for (short value : foundArray) {
                        code = code * mult + value;
                    }
                } else if (array instanceof int[]) {
                    int[] foundArray = (int[]) array;
                    for (int value : foundArray) {
                        code = code * mult + value;
                    }
                } else if (array instanceof long[]) {
                    long[] foundArray = (long[]) array;
                    for (long value : foundArray) {
                        code = code * mult + (int) value;
                    }
                } else if (array instanceof float[]) {
                    float[] foundArray = (float[]) array;
                    for (float value : foundArray) {
                        code = code * mult + (int) value;
                    }
                } else if (array instanceof double[]) {
                    double[] foundArray = (double[]) array;
                    for (double value : foundArray) {
                        code = code * mult + (int) value;
                    }
                }
            } else {
                Object[] foundArray = (Object[]) array;
                for (Object value : foundArray) {
                    code = code * mult + hashCode(value);
                }
            }
        } else {
            code = array.hashCode();
        }
        return code;
    }

    /**
     * Calculates the index in the flat array, corresponding to a position in the equivalent
     * multi-dimensional array
     * 
     * @param position The position in the multi-dimensional array
     * @param shape The shape of the multi-dimensional array
     * @return An int, that represents the expected index
     */
    public static int getFlatIndex(int[] position, int[] shape) {
        int result;
        if ((position != null) && (shape != null) && (position.length == shape.length)) {
            result = 0;
            for (int index = 0; index < position.length; index++) {
                result += indexN(index, position, shape);
            }
        } else {
            result = -1;
        }
        return result;
    }

    private static int indexN(int index, int[] position, int[] shape) {
        int result = position[index];
        for (int i = index + 1; i < shape.length; i++) {
            result *= shape[i];
        }
        return result;
    }

    /**
     * Recursive method testing two multi-dimensional arrays for equality.
     * 
     * @param dimIndex index in multi-dimensional array's shape
     * @param startPositions starting position in multi-dimensional array
     * @param dimensions dimensions' size of the multi-dimensional array
     * @param inputArray1 The 1st array
     * @param inputArray2 The 2nd array
     * @return <code>TRUE</code> if the two arrays are equal, <code>FALSE</code> otherwise
     */
    private static boolean areArraysEqual(int dimIndex, int[] startPositions, int[] dimensions, Object inputArray1,
            Object inputArray2) {
        boolean equals;
        if (inputArray1 == null) {
            equals = (inputArray2 == null);
        } else if (inputArray2 == null) {
            equals = false;
        } else if (inputArray1.getClass().equals(inputArray2.getClass())) {
            if (dimIndex == dimensions.length - 1) {
                if (inputArray1 instanceof boolean[]) {
                    boolean[] array1 = (boolean[]) inputArray1;
                    boolean[] array2 = (boolean[]) inputArray2;
                    equals = Arrays.equals(array1, array2);
                } else if (inputArray1 instanceof char[]) {
                    char[] array1 = (char[]) inputArray1;
                    char[] array2 = (char[]) inputArray2;
                    equals = Arrays.equals(array1, array2);
                } else if (inputArray1 instanceof byte[]) {
                    byte[] array1 = (byte[]) inputArray1;
                    byte[] array2 = (byte[]) inputArray2;
                    equals = Arrays.equals(array1, array2);
                } else if (inputArray1 instanceof short[]) {
                    short[] array1 = (short[]) inputArray1;
                    short[] array2 = (short[]) inputArray2;
                    equals = Arrays.equals(array1, array2);
                } else if (inputArray1 instanceof int[]) {
                    int[] array1 = (int[]) inputArray1;
                    int[] array2 = (int[]) inputArray2;
                    equals = Arrays.equals(array1, array2);
                } else if (inputArray1 instanceof long[]) {
                    long[] array1 = (long[]) inputArray1;
                    long[] array2 = (long[]) inputArray2;
                    equals = Arrays.equals(array1, array2);
                } else if (inputArray1 instanceof float[]) {
                    float[] array1 = (float[]) inputArray1;
                    float[] array2 = (float[]) inputArray2;
                    equals = Arrays.equals(array1, array2);
                } else if (inputArray1 instanceof double[]) {
                    double[] array1 = (double[]) inputArray1;
                    double[] array2 = (double[]) inputArray2;
                    equals = Arrays.equals(array1, array2);
                } else {
                    Object[] array1 = (Object[]) inputArray1;
                    Object[] array2 = (Object[]) inputArray2;
                    if (array1.length == array2.length) {
                        equals = true;
                        for (int i = 0; i < array1.length; i++) {
                            if (!ObjectUtils.sameObject(array1[i], array2[i])) {
                                equals = false;
                                break;
                            }
                        }
                    } else {
                        equals = false;
                    }
                }
            } else {
                equals = true;
                Object[] outputAsArray1 = (Object[]) inputArray1;
                Object[] outputAsArray2 = (Object[]) inputArray2;
                for (int i = 0; (i < dimensions[dimIndex]) && equals; i++) {
                    Object o1 = outputAsArray1[i];
                    Object o2 = outputAsArray2[i];
                    equals = areArraysEqual(dimIndex + 1, startPositions, dimensions, o1, o2);
                    if (startPositions[dimIndex] < dimensions[dimIndex] - 1) {
                        startPositions[dimIndex] = i + 1;
                    } else {
                        startPositions[dimIndex] = 0;
                    }
                }
            }
        } else {
            equals = false;
        }
        return equals;
    }

    /**
     * Returns the copy of an array (whatever its dimension)
     * 
     * @param array The array to copy
     * @return The copy of the given array. If the given <code>array</code> is a simple is not an
     *         array, the result is this {@link Object}.
     */
    public static Object getCopy(Object array) {
        Object result = array;
        if (isArray(array)) {
            // Define a convenient array (shape and type)
            result = Array.newInstance(recoverDataType(array), recoverShape(array));
            copyArray(array, result);
        }
        return result;
    }

    /**
     * Copies an array into another one. The 2 arrays should be of the same type and dimension
     * 
     * @param source The array to copy
     * @param target The array that should receive the copy
     * @see #recoverDataType(Object)
     * @see #recoverShape(Object)
     */
    public static void copyArray(Object source, Object target) {
        if ((source != null) && (target != null) && recoverDataType(source).equals(recoverDataType(target))) {
            int[] shape = recoverShape(source);
            if (equals(shape, recoverShape(target)) && (shape.length > 0)) {
                int length = Array.getLength(source);
                if (shape.length > 1) {
                    for (int i = 0; i < length; i++) {
                        copyArray(Array.get(source, i), Array.get(target, i));
                    }
                } else {
                    System.arraycopy(source, 0, target, 0, length);
                }
            }
        }
    }

    /**
     * Converts an array of unsigned values into an array of same dimensions with values in bigger
     * encoding, with negative values transformed into their positive equivalent if unsigned values
     * were supported by Java
     * 
     * @param array The array to convert
     * @param unsignedLongSupported Whether unsigned long values should be supported.
     * @return An array of bigger encoded value, if possible. This will only be done for arrays of
     *         one of the following types
     *         <ul>
     *         <li><code>byte</code></li>
     *         <li>{@link Byte}</li>
     *         <li><code>short</code></li>
     *         <li>{@link Short}</li>
     *         <li><code>int</code></li>
     *         <li>{@link Integer}</li>
     *         <li><code>long</code> if <code>unsignedLongSupported</code> is <code>true</code>.</li>
     *         <li>{@link Long} if <code>unsignedLongSupported</code> is <code>true</code>.</li>
     *         </ul>
     *         Otherwise, the original array is returned.
     */
    public static Object convertUnsignedArray(Object array, boolean unsignedLongSupported) {
        Object result = null;
        int[] shape = recoverShape(array);
        Class<?> sourceType = recoverDataType(array);
        if ((shape != null) && (shape.length > 0)) {
            if (Byte.TYPE.equals(sourceType)) {
                result = Array.newInstance(Short.TYPE, shape);
            } else if (Short.TYPE.equals(sourceType)) {
                result = Array.newInstance(Integer.TYPE, shape);
            } else if (Integer.TYPE.equals(sourceType)) {
                result = Array.newInstance(Long.TYPE, shape);
            } else if (Byte.class.equals(sourceType)) {
                result = Array.newInstance(Short.class, shape);
            } else if (Short.class.equals(sourceType)) {
                result = Array.newInstance(Integer.class, shape);
            } else if (Integer.class.equals(sourceType)) {
                result = Array.newInstance(Long.class, shape);
            } else if (unsignedLongSupported && (Long.class.equals(sourceType) || Long.TYPE.equals(sourceType))) {
                result = Array.newInstance(BigInteger.class, shape);
            }
        }
        if (result == null) {
            result = array;
        } else {
            convertUnsignedToCopy(array, result, shape, sourceType);
        }
        return result;
    }

    /**
     * Converts an array of unsigned values into an array of same dimensions with values in bigger
     * encoding, with negative values transformed into their positive equivalent if unsigned values
     * were supported by Java
     * 
     * @param array The array to convert
     * @return An array of bigger encoded value, if possible. This will only be done for arrays of
     *         one of the following types
     *         <ul>
     *         <li><code>byte</code></li>
     *         <li>{@link Byte}</li>
     *         <li><code>short</code></li>
     *         <li>{@link Short}</li>
     *         <li><code>int</code></li>
     *         <li>{@link Integer}</li>
     *         <li><code>long</code> if {@link UnsignedConverter#isUnsignedLongSupported()} returns
     *         <code>true</code>.</li>
     *         <li>{@link Long} if {@link UnsignedConverter#isUnsignedLongSupported()} returns <code>true</code>.</li>
     *         </ul>
     *         Otherwise, the original array is returned.
     */
    public static Object convertUnsignedArray(Object array) {
        return convertUnsignedArray(array, UnsignedConverter.isUnsignedLongSupported());
    }

    /**
     * Does the revert operation of {@link #convertUnsignedArray(Object)}
     * 
     * @param array The array to convert
     * @return An array of bigger encoded value, if possible. This will only be done for arrays of
     *         one of the following types
     *         <ul>
     *         <li><code>byte</code></li>
     *         <li>{@link Byte}</li>
     *         <li><code>short</code></li>
     *         <li>{@link Short}</li>
     *         <li><code>int</code></li>
     *         <li>{@link Integer}</li>
     *         </ul>
     *         Otherwise, the original array is returned.
     */
    public static Object undoUnsignedArrayConversion(Object array) {
        Object result = null;
        int[] shape = recoverShape(array);
        Class<?> sourceType = recoverDataType(array);
        if ((shape != null) && (shape.length > 0)) {
            if (Short.TYPE.equals(sourceType)) {
                result = Array.newInstance(Byte.TYPE, shape);
            } else if (Integer.TYPE.equals(sourceType)) {
                result = Array.newInstance(Short.TYPE, shape);
            } else if (Long.TYPE.equals(sourceType)) {
                result = Array.newInstance(Integer.TYPE, shape);
            } else if (Short.class.equals(sourceType)) {
                result = Array.newInstance(Byte.class, shape);
            } else if (Integer.class.equals(sourceType)) {
                result = Array.newInstance(Short.class, shape);
            } else if (Long.class.equals(sourceType)) {
                result = Array.newInstance(Integer.class, shape);
            } else if (BigInteger.class.equals(sourceType)) {
                result = Array.newInstance(Long.TYPE, shape);
            }
        }
        if (result == null) {
            result = array;
        } else {
            convertSignedToCopy(array, result, shape, sourceType);
        }
        return result;
    }

    /**
     * Does the unsigned conversion, by copying unsigned values conversion from a source array to a
     * destination array
     * 
     * @param source The source arrays that contains the unsigned values
     * @param dest The destination array that should receive the converted values
     * @param shape The array shape
     * @param sourceType The source array data type
     */
    protected static void convertUnsignedToCopy(Object source, Object dest, int[] shape, Class<?> sourceType) {
        int length = Array.getLength(source);
        if (shape.length > 1) {
            int[] newShape = Arrays.copyOfRange(shape, 1, shape.length);
            for (int i = 0; i < length; i++) {
                convertUnsignedToCopy(Array.get(source, i), Array.get(dest, i), newShape, sourceType);
            }
        } else {
            if (Byte.TYPE.equals(sourceType)) {
                byte[] src = (byte[]) source;
                short[] dst = (short[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = UnsignedConverter.convertUnsignedByte(src[i]);
                }
            } else if (Short.TYPE.equals(sourceType)) {
                short[] src = (short[]) source;
                int[] dst = (int[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = UnsignedConverter.convertUnsignedShort(src[i]);
                }
            } else if (Integer.TYPE.equals(sourceType)) {
                int[] src = (int[]) source;
                long[] dst = (long[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = UnsignedConverter.convertUnsignedInt(src[i]);
                }
            } else if (Long.TYPE.equals(sourceType)) {
                long[] src = (long[]) source;
                BigInteger[] dst = (BigInteger[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = UnsignedConverter.convertUnsignedLong(src[i]);
                }
            } else if (Byte.class.equals(sourceType)) {
                Byte[] src = (Byte[]) source;
                Short[] dst = (Short[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = (src[i] == null ? null : UnsignedConverter.convertUnsignedByte(src[i]));
                }
            } else if (Short.class.equals(sourceType)) {
                Short[] src = (Short[]) source;
                Integer[] dst = (Integer[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = (src[i] == null ? null : UnsignedConverter.convertUnsignedShort(src[i]));
                }
            } else if (Integer.class.equals(sourceType)) {
                Integer[] src = (Integer[]) source;
                Long[] dst = (Long[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = (src[i] == null ? null : UnsignedConverter.convertUnsignedInt(src[i]));
                }
            } else if (Long.class.equals(sourceType)) {
                Long[] src = (Long[]) source;
                BigInteger[] dst = (BigInteger[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = (src[i] == null ? null : UnsignedConverter.convertUnsignedLong(src[i]));
                }
            }
        }
    }

    /**
     * Reverts the unsigned conversion, by copying unsigned values conversion reverted from a source array to a
     * destination array. This method does the revert operation of
     * {@link #convertUnsignedToCopy(Object, Object, int[], Class)}.
     * 
     * @param source The source arrays that contains the previously converted unsigned values
     * @param dest The destination array that should receive the converted values
     * @param shape The array shape
     * @param sourceType The source array data type
     */
    protected static void convertSignedToCopy(Object source, Object dest, int[] shape, Class<?> sourceType) {
        int length = Array.getLength(source);
        if (shape.length > 1) {
            int[] newShape = Arrays.copyOfRange(shape, 1, shape.length);
            for (int i = 0; i < length; i++) {
                convertSignedToCopy(Array.get(source, i), Array.get(dest, i), newShape, sourceType);
            }
        } else {
            if (BigInteger.class.equals(sourceType)) {
                BigInteger[] src = (BigInteger[]) source;
                if (dest instanceof long[]) {
                    long[] dst = (long[]) dest;
                    for (int i = 0; i < length; i++) {
                        dst[i] = src[i] == null ? 0L : src[i].longValue();
                    }
                } else if (dest instanceof Long[]) {
                    Long[] dst = (Long[]) dest;
                    for (int i = 0; i < length; i++) {
                        dst[i] = src[i] == null ? null : Long.valueOf(src[i].longValue());
                    }
                }
            } else if (Long.TYPE.equals(sourceType)) {
                long[] src = (long[]) source;
                int[] dst = (int[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = (int) src[i];
                }
            } else if (Integer.TYPE.equals(sourceType)) {
                int[] src = (int[]) source;
                short[] dst = (short[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = (short) src[i];
                }
            } else if (Short.TYPE.equals(sourceType)) {
                short[] src = (short[]) source;
                byte[] dst = (byte[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = (byte) src[i];
                }
            } else if (Long.class.equals(sourceType)) {
                Long[] src = (Long[]) source;
                Integer[] dst = (Integer[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = (src[i] == null ? null : Integer.valueOf(src[i].intValue()));
                }
            } else if (Integer.class.equals(sourceType)) {
                Integer[] src = (Integer[]) source;
                Short[] dst = (Short[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = (src[i] == null ? null : Short.valueOf(src[i].shortValue()));
                }
            } else if (Short.class.equals(sourceType)) {
                Short[] src = (Short[]) source;
                Byte[] dst = (Byte[]) dest;
                for (int i = 0; i < length; i++) {
                    dst[i] = (src[i] == null ? null : Byte.valueOf(src[i].byteValue()));
                }
            }
        }
    }

    /**
     * Increments a stack position array, knowing the stack shape
     * 
     * @param shape The array that represents the stack shape
     * @param positions The array that represents the current position in stack
     * @return A <code>boolean</code> value. <code>true</code> if <code>positions</code> was correctly incremented,
     *         <code>false</code> otherwise.
     *         <p>
     *         <code>false</code> will always be returned in any of these cases:
     *         <ul>
     *         <li><code>shape</code> is <code>null</code></li>
     *         <li><code>positions</code> is <code>null</code></li>
     *         <li><code>shape</code> and <code>positions</code> do not have the same length</li>
     *         <li><code>positions</code> is already at, or above, the maximum position allowed by
     *         <code>shape</code></li>
     *         </ul>
     *         </p>
     */
    public static boolean incrementPosition(int[] shape, int[] positions) {
        return incrementPosition(shape, positions, shape == null ? -1 : shape.length - 1);
    }

    /**
     * Increments a stack position array, at a particular dimension, knowing the stack shape
     * 
     * @param shape The array that represents the stack shape
     * @param positions The array that represents the current position in stack
     * @param index the dimension index
     * @return A <code>boolean</code> value. <code>true</code> if <code>positions</code> was correctly incremented,
     *         <code>false</code> otherwise.
     *         <p>
     *         <code>false</code> will always be returned in any of these cases:
     *         <ul>
     *         <li><code>shape</code> is <code>null</code></li>
     *         <li><code>positions</code> is <code>null</code></li>
     *         <li><code>shape</code> and <code>positions</code> do not have the same length</li>
     *         <li><code>positions</code> is already at, or above, the maximum position allowed by
     *         <code>shape</code></li>
     *         <li><code>index</code> is out of <code>positions</code> range</li>
     *         </ul>
     *         </p>
     */
    public static boolean incrementPosition(int[] shape, int[] positions, int index) {
        boolean stillInShape;
        if ((shape == null) || (positions == null) || (positions.length != shape.length) || (index < 0)
                || (index >= positions.length)) {
            stillInShape = false;
        } else {
            stillInShape = doIncrementPosition(shape, positions, index);
        }
        return stillInShape;
    }

    /**
     * Increments a stack position array, at a particular dimension, knowing the stack shape by step
     * 
     * @param shape The array that represents the stack shape
     * @param positions The array that represents the current position in stack
     * @param step
     * @param lastPosition the last position
     * @return A <code>boolean</code> value. <code>true</code> if <code>positions</code> was correctly incremented,
     *         <code>false</code> otherwise.
     *         <p>
     *         <code>false</code> will always be returned in any of these cases:
     *         <ul>
     *         <li><code>shape</code> is <code>null</code></li>
     *         <li><code>positions</code> is <code>null</code></li>
     *         <li><code>shape</code> and <code>positions</code> do not have the same length</li>
     *         <li><code>positions</code> is already at, or above, the maximum position allowed by
     *         <code>shape</code></li>
     *         </ul>
     *         </p>
     */
    public static boolean incrementStepPosition(int[] shape, int[] positions, int step, int[] lastPosition) {
        boolean incrementOk = true;
        boolean isGreater = true;
        boolean isMaxPosition = false;
        int indexPosition = 0;
        int[] savePositions = positions.clone();
        while (incrementOk && (step > indexPosition) && isGreater && !isMaxPosition) {
            if (!ArrayUtils.isGreater(lastPosition, savePositions)) {
                isGreater = false;
            } else if (ArrayUtils.isMaxPosition(shape, savePositions)) {
                isMaxPosition = true;
            } else {
                incrementOk = ArrayUtils.incrementPosition(shape, savePositions);
                indexPosition++;
            }
        }
        // check number increment...
        if (indexPosition == step && incrementOk) {
            ArrayUtils.copyArray(savePositions, positions);
        } else {
            incrementOk = false;
        }

        return incrementOk;
    }

    /**
     * Returns whether 2 positions are compatible which each other. This will happen if they are not <code>null</code>
     * and of same length.
     * 
     * @param position1 The 1st position
     * @param position2 The 2nd position
     * @return A <code>boolean</code>
     */
    public static boolean areCompatible(int[] position1, int[] position2) {
        return ((position1 != null) && (position2 != null) && (position1.length == position2.length));
    }

    /**
     * Compares 2 positions, and returns whether the 1st one is greater than the 2nd one.
     * 
     * @param position1 The 1st position
     * @param position2 The 2nd position
     * @return A <code>boolean</code>
     */
    public static boolean isGreater(int[] position1, int[] position2) {
        boolean greater = false;
        if (areCompatible(position1, position2)) {
            for (int i = 0; i < position1.length && !greater; i++) {
                int p1 = position1[i], p2 = position2[i];
                if (p1 > p2) {
                    greater = true;
                } else if (p1 < p2) {
                    break;
                }
            }
        }
        return greater;
    }

    public static boolean isMaxPosition(int[] shape, int[] positions) {
        boolean maxPosition;
        if ((shape == null) || (positions == null) || (positions.length != shape.length)) {
            maxPosition = false;
        } else {
            maxPosition = true;
            for (int i = 0; (i < positions.length) && maxPosition; i++) {
                maxPosition = (positions[i] == shape[i] - 1);
            }
        }
        return maxPosition;
    }

    public static boolean isPosition(int[] shape, int[] positions) {
        boolean ok;
        if ((shape == null) || (positions == null) || (positions.length != shape.length)) {
            ok = false;
        } else {
            ok = true;
            for (int i = 0; (i < positions.length) && ok; i++) {
                int pos = positions[i];
                ok = (pos > -1) && (pos < shape[i]);
            }
        }
        return ok;
    }

    private static boolean doIncrementPosition(int[] shape, int[] positions, int index) {
        boolean stillInShape = true;
        positions[index]++;
        if (positions[index] >= shape[index]) {
            if (index > 0) {
                positions[index] = 0;
                stillInShape = doIncrementPosition(shape, positions, --index);
            } else {
                stillInShape = false;
            }
        }
        return stillInShape;
    }

    private static int getAdaptedLength(Object array) {
        int length;
        if (isArray(array)) {
            length = Array.getLength(array);
        } else {
            length = -1;
        }
        return length;
    }

    /**
     * Returns whether an array is consistent. That will help you to distinguish effective multi-dimension arrays from
     * arrays of arrays
     * 
     * @param array The array to test
     * @return A <code>boolean</code> value.
     *         <ul>
     *         <li><code>true</code> in any of these cases:
     *         <ul>
     *         <li><code>array</code> is <code>null</code></li>
     *         <li><code>array</code> is not an array, but a scalar</li>
     *         <li><code>array</code> is a single dimension array</li>
     *         <li><code>array</code> is a multi dimension array, for which each sub-array at a given dimension have the
     *         same length.<br />
     *         For example:
     *         <ul>
     *         <li><code>{ {1,2,3} , {4,5,6} }</code> is consistent</li>
     *         <li><code>{ {1,2,3} , {4,5,6,7} }</code> is NOT consistent</li>
     *         <li><code>{ {1,2,3}, null , {4,5,6} }</code> is NOT consistent</li>
     *         <li><code>{ <br />
     *          { {1,2,3}, {4,5,6} , {7,8,9}  }  <br />
     *          { {10,11,12}, {13,14,15} , {16,17,18}  }  <br />
     *          } </code> is consistent</li>
     *         <li><code>{ <br />
     *          { {1,2}, {3,4,5,6} , {7,8,9}  }  <br />
     *          { {10,11}, {12,13,14,15} , {16,17,18}  }  <br />
     *          } </code> is NOT consistent</li>
     *         </ul>
     *         </li>
     *         </ul>
     *         </li>
     *         <li><code>false</code> otherwise</li>
     *         </ul>
     */
    public static boolean isConsistent(Object array) {
        boolean consistent = true;
        if (isArray(array) && (!array.getClass().getComponentType().isPrimitive())) {
            int length = Array.getLength(array);
            int subLength = Integer.MIN_VALUE;
            for (int i = 0; i < length; i++) {
                Object subArray = Array.get(array, i);
                if (isConsistent(subArray)) {
                    int tmpLength = getAdaptedLength(subArray);
                    if (subLength == Integer.MIN_VALUE) {
                        subLength = tmpLength;
                    } else if (tmpLength != subLength) {
                        consistent = false;
                        break;
                    }
                } else {
                    consistent = false;
                    break;
                }
            }
        }
        return consistent;
    }

    /**
     * Returns the {@link String} representation of an array
     * 
     * @param array The array
     * @return A {@link String}, never <code>null</code>.
     */
    public static String toString(Object array) {
        String toString;
        if (isArray(array)) {
            int rank = recoverArrayRank(array.getClass());
            if (rank == 1) {
                if (array instanceof boolean[]) {
                    toString = Arrays.toString((boolean[]) array);
                } else if (array instanceof char[]) {
                    toString = Arrays.toString((char[]) array);
                } else if (array instanceof byte[]) {
                    toString = Arrays.toString((byte[]) array);
                } else if (array instanceof short[]) {
                    toString = Arrays.toString((short[]) array);
                } else if (array instanceof int[]) {
                    toString = Arrays.toString((int[]) array);
                } else if (array instanceof long[]) {
                    toString = Arrays.toString((long[]) array);
                } else if (array instanceof float[]) {
                    toString = Arrays.toString((float[]) array);
                } else if (array instanceof double[]) {
                    toString = Arrays.toString((double[]) array);
                } else {
                    toString = Arrays.toString((Object[]) array);
                }
            } else {
                toString = Arrays.deepToString((Object[]) array);
            }
        } else {
            toString = String.valueOf(array);
        }
        return toString;
    }

    /**
     * Rotates a matrix of + or - 90 degrees
     * 
     * @param data The matrix to rotate
     * @param positiveAngle Whether to use positive angle rotation
     * @return The rotated matrix. Can be <code>null</code>.
     */
    public static <T> T[] matrixRotation(T[] data, boolean positiveAngle) {
        T[] result;
        Class<?> targetType = ArrayUtils.recoverDataType(data);
        int[] shape = ArrayUtils.recoverShape(data);
        if (data == null) {
            result = null;
        } else if ((targetType != null) && (shape != null) && (shape.length == 2)) {
            int[] resultShape = new int[] { shape[1], shape[0] };
            @SuppressWarnings("unchecked")
            T[] tmp = (T[]) Array.newInstance(targetType, resultShape);
            result = tmp;
            if (targetType.isPrimitive()) {
                if (Boolean.TYPE.equals(targetType)) {
                    rotate((boolean[][]) data, (boolean[][]) result, positiveAngle);
                } else if (Character.TYPE.equals(targetType)) {
                    rotate((char[][]) data, (char[][]) result, positiveAngle);
                } else if (Byte.TYPE.equals(targetType)) {
                    rotate((byte[][]) data, (byte[][]) result, positiveAngle);
                } else if (Short.TYPE.equals(targetType)) {
                    rotate((short[][]) data, (short[][]) result, positiveAngle);
                } else if (Integer.TYPE.equals(targetType)) {
                    rotate((int[][]) data, (int[][]) result, positiveAngle);
                } else if (Long.TYPE.equals(targetType)) {
                    rotate((long[][]) data, (long[][]) result, positiveAngle);
                } else if (Float.TYPE.equals(targetType)) {
                    rotate((float[][]) data, (float[][]) result, positiveAngle);
                } else {
                    rotate((double[][]) data, (double[][]) result, positiveAngle);
                }
            } else {
                rotate((Object[][]) data, (Object[][]) result, positiveAngle);
            }
        } else {
            result = null;
        }
        return result;
    }

    /**
     * Rotates the flat representation of a matrix, of + or - 90 degrees.
     * 
     * @param data The flat matrix to rotate
     * @param dimY The matrix height
     * @param dimX The matrix width
     * @param positiveAngle Whether to use positive angle rotation
     * @return The rotated flat matrix. Can be <code>null</code>.
     */
    public static <M> M flatMatrixRotation(M data, int dimY, int dimX, boolean positiveAngle) {
        M result;
        Class<?> targetType = ArrayUtils.recoverDataType(data);
        if ((!isArray(data)) || (dimY < 0) || (dimX < 0) || (targetType == null)) {
            result = null;
        } else {
            int length = Array.getLength(data);
            if (length == dimY * dimX) {
                @SuppressWarnings("unchecked")
                M newArray = (M) Array.newInstance(targetType, length);
                result = newArray;
                if (targetType.isPrimitive()) {
                    if (Boolean.TYPE.equals(targetType)) {
                        rotate((boolean[]) data, (boolean[]) result, dimY, dimX, positiveAngle);
                    } else if (Character.TYPE.equals(targetType)) {
                        rotate((char[]) data, (char[]) result, dimY, dimX, positiveAngle);
                    } else if (Byte.TYPE.equals(targetType)) {
                        rotate((byte[]) data, (byte[]) result, dimY, dimX, positiveAngle);
                    } else if (Short.TYPE.equals(targetType)) {
                        rotate((short[]) data, (short[]) result, dimY, dimX, positiveAngle);
                    } else if (Integer.TYPE.equals(targetType)) {
                        rotate((int[]) data, (int[]) result, dimY, dimX, positiveAngle);
                    } else if (Long.TYPE.equals(targetType)) {
                        rotate((long[]) data, (long[]) result, dimY, dimX, positiveAngle);
                    } else if (Float.TYPE.equals(targetType)) {
                        rotate((float[]) data, (float[]) result, dimY, dimX, positiveAngle);
                    } else {
                        rotate((double[]) data, (double[]) result, dimY, dimX, positiveAngle);
                    }
                } else {
                    rotate((Object[]) data, (Object[]) result, dimY, dimX, positiveAngle);
                }
            } else {
                result = null;
            }
        }
        return result;
    }

    /**
     * Applies an horizontal axis or vertical axis symmetry to a matrix
     * 
     * @param data The matrix to which to apply symmetry
     * @param horizontalAxis Whether to use a horizontal axis symmetry
     * @return The resulting matrix. Can be <code>null</code>.
     */
    public static <T> T[] matrixSymmetry(T[] data, boolean horizontalAxis) {
        T[] result;
        Class<?> targetType = ArrayUtils.recoverDataType(data);
        int[] shape = ArrayUtils.recoverShape(data);
        if (data == null) {
            result = null;
        } else if ((targetType != null) && (shape != null) && (shape.length == 2)) {
            @SuppressWarnings("unchecked")
            T[] tmp = (T[]) Array.newInstance(targetType, shape);
            result = tmp;
            if (horizontalAxis) {
                revert(data, result);
            } else if (targetType.isPrimitive()) {
                if (Boolean.TYPE.equals(targetType)) {
                    for (int i = 0; i < data.length; i++) {
                        revert((boolean[]) data[i], (boolean[]) result[i]);
                    }
                } else if (Character.TYPE.equals(targetType)) {
                    for (int i = 0; i < data.length; i++) {
                        revert((char[]) data[i], (char[]) result[i]);
                    }
                } else if (Byte.TYPE.equals(targetType)) {
                    for (int i = 0; i < data.length; i++) {
                        revert((byte[]) data[i], (byte[]) result[i]);
                    }
                } else if (Short.TYPE.equals(targetType)) {
                    for (int i = 0; i < data.length; i++) {
                        revert((short[]) data[i], (short[]) result[i]);
                    }
                } else if (Integer.TYPE.equals(targetType)) {
                    for (int i = 0; i < data.length; i++) {
                        revert((int[]) data[i], (int[]) result[i]);
                    }
                } else if (Long.TYPE.equals(targetType)) {
                    for (int i = 0; i < data.length; i++) {
                        revert((long[]) data[i], (long[]) result[i]);
                    }
                } else if (Float.TYPE.equals(targetType)) {
                    for (int i = 0; i < data.length; i++) {
                        revert((float[]) data[i], (float[]) result[i]);
                    }
                } else {
                    for (int i = 0; i < data.length; i++) {
                        revert((double[]) data[i], (double[]) result[i]);
                    }
                }
            } else {
                for (int i = 0; i < data.length; i++) {
                    revert((Object[]) data[i], (Object[]) result[i]);
                }
            }
        } else {
            result = null;
        }
        return result;
    }

    /**
     * Applies an horizontal axis or vertical axis symmetry to a flat matrix
     * 
     * @param data The flat matrix to which to apply symmetry
     * @param dimY The matrix height
     * @param dimX The matrix width
     * @param horizontalAxis Whether to use a horizontal axis symmetry
     * @return The resulting flat matrix. Can be <code>null</code>.
     */
    public static <M> M flatMatrixSymmetry(M data, int dimY, int dimX, boolean horizontalAxis) {
        M result;
        Class<?> targetType = ArrayUtils.recoverDataType(data);
        if ((!isArray(data)) || (dimY < 0) || (dimX < 0) || (targetType == null)) {
            result = null;
        } else {
            int length = Array.getLength(data);
            if (length == dimY * dimX) {
                @SuppressWarnings("unchecked")
                M newArray = (M) Array.newInstance(targetType, length);
                result = newArray;
                if (horizontalAxis) {
                    for (int y = 0; y < dimY; y++) {
                        System.arraycopy(data, y * dimX, result, (dimY - (y + 1)) * dimX, dimX);
                    }
                } else if (targetType.isPrimitive()) {
                    if (Boolean.TYPE.equals(targetType)) {
                        boolean[] src = (boolean[]) data;
                        boolean[] dest = (boolean[]) result;
                        for (int y = 0; y < dimY; y++) {
                            for (int x = 0; x < dimX; x++) {
                                dest[y * dimX + x] = src[y * dimX + dimX - (x + 1)];
                            }
                        }
                    } else if (Character.TYPE.equals(targetType)) {
                        char[] src = (char[]) data;
                        char[] dest = (char[]) result;
                        for (int y = 0; y < dimY; y++) {
                            for (int x = 0; x < dimX; x++) {
                                dest[y * dimX + x] = src[y * dimX + dimX - (x + 1)];
                            }
                        }
                    } else if (Byte.TYPE.equals(targetType)) {
                        byte[] src = (byte[]) data;
                        byte[] dest = (byte[]) result;
                        for (int y = 0; y < dimY; y++) {
                            for (int x = 0; x < dimX; x++) {
                                dest[y * dimX + x] = src[y * dimX + dimX - (x + 1)];
                            }
                        }
                    } else if (Short.TYPE.equals(targetType)) {
                        short[] src = (short[]) data;
                        short[] dest = (short[]) result;
                        for (int y = 0; y < dimY; y++) {
                            for (int x = 0; x < dimX; x++) {
                                dest[y * dimX + x] = src[y * dimX + dimX - (x + 1)];
                            }
                        }
                    } else if (Integer.TYPE.equals(targetType)) {
                        int[] src = (int[]) data;
                        int[] dest = (int[]) result;
                        for (int y = 0; y < dimY; y++) {
                            for (int x = 0; x < dimX; x++) {
                                dest[y * dimX + x] = src[y * dimX + dimX - (x + 1)];
                            }
                        }
                    } else if (Long.TYPE.equals(targetType)) {
                        long[] src = (long[]) data;
                        long[] dest = (long[]) result;
                        for (int y = 0; y < dimY; y++) {
                            for (int x = 0; x < dimX; x++) {
                                dest[y * dimX + x] = src[y * dimX + dimX - (x + 1)];
                            }
                        }
                    } else if (Float.TYPE.equals(targetType)) {
                        float[] src = (float[]) data;
                        float[] dest = (float[]) result;
                        for (int y = 0; y < dimY; y++) {
                            for (int x = 0; x < dimX; x++) {
                                dest[y * dimX + x] = src[y * dimX + dimX - (x + 1)];
                            }
                        }
                    } else {
                        double[] src = (double[]) data;
                        double[] dest = (double[]) result;
                        for (int y = 0; y < dimY; y++) {
                            for (int x = 0; x < dimX; x++) {
                                dest[y * dimX + x] = src[y * dimX + dimX - (x + 1)];
                            }
                        }
                    }
                } else {
                    Object[] src = (Object[]) data;
                    Object[] dest = (Object[]) result;
                    for (int y = 0; y < dimY; y++) {
                        for (int x = 0; x < dimX; x++) {
                            dest[y * dimX + x] = src[y * dimX + dimX - (x + 1)];
                        }
                    }

                }
            } else {
                result = null;
            }
        }
        return result;
    }

    private static void rotate(Object[][] data, Object[][] result, boolean positive) {
        if (positive) {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[result.length - (x + 1)][y] = data[y][x];
                }
            }
        } else {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[x][result[x].length - (y + 1)] = data[y][x];
                }
            }
        }
    }

    private static void rotate(char[][] data, char[][] result, boolean positive) {
        if (positive) {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[result.length - (x + 1)][y] = data[y][x];
                }
            }
        } else {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[x][result[x].length - (y + 1)] = data[y][x];
                }
            }
        }
    }

    private static void rotate(boolean[][] data, boolean[][] result, boolean positive) {
        if (positive) {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[result.length - (x + 1)][y] = data[y][x];
                }
            }
        } else {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[x][result[x].length - (y + 1)] = data[y][x];
                }
            }
        }
    }

    private static void rotate(byte[][] data, byte[][] result, boolean positive) {
        if (positive) {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[result.length - (x + 1)][y] = data[y][x];
                }
            }
        } else {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[x][result[x].length - (y + 1)] = data[y][x];
                }
            }
        }
    }

    private static void rotate(short[][] data, short[][] result, boolean positive) {
        if (positive) {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[result.length - (x + 1)][y] = data[y][x];
                }
            }
        } else {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[x][result[x].length - (y + 1)] = data[y][x];
                }
            }
        }
    }

    private static void rotate(int[][] data, int[][] result, boolean positive) {
        if (positive) {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[result.length - (x + 1)][y] = data[y][x];
                }
            }
        } else {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[x][result[x].length - (y + 1)] = data[y][x];
                }
            }
        }
    }

    private static void rotate(long[][] data, long[][] result, boolean positive) {
        if (positive) {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[result.length - (x + 1)][y] = data[y][x];
                }
            }
        } else {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[x][result[x].length - (y + 1)] = data[y][x];
                }
            }
        }
    }

    private static void rotate(float[][] data, float[][] result, boolean positive) {
        if (positive) {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[result.length - (x + 1)][y] = data[y][x];
                }
            }
        } else {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[x][result[x].length - (y + 1)] = data[y][x];
                }
            }
        }
    }

    private static void rotate(double[][] data, double[][] result, boolean positive) {
        if (positive) {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[result.length - (x + 1)][y] = data[y][x];
                }
            }
        } else {
            for (int y = 0; y < data.length; y++) {
                for (int x = 0; x < data[y].length; x++) {
                    result[x][result[x].length - (y + 1)] = data[y][x];
                }
            }
        }
    }

    private static void rotate(Object[] data, Object[] result, int dimY, int dimX, boolean positive) {
        if (positive) {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[(dimX - (x + 1)) * dimY + y] = data[y * dimX + x];
                }
            }
        } else {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[x * dimY + dimY - (y + 1)] = data[y * dimX + x];
                }
            }
        }
    }

    private static void rotate(char[] data, char[] result, int dimY, int dimX, boolean positive) {
        if (positive) {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[(dimX - (x + 1)) * dimY + y] = data[y * dimX + x];
                }
            }
        } else {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[x * dimY + dimY - (y + 1)] = data[y * dimX + x];
                }
            }
        }
    }

    private static void rotate(boolean[] data, boolean[] result, int dimY, int dimX, boolean positive) {
        if (positive) {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[(dimX - (x + 1)) * dimY + y] = data[y * dimX + x];
                }
            }
        } else {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[x * dimY + dimY - (y + 1)] = data[y * dimX + x];
                }
            }
        }
    }

    private static void rotate(byte[] data, byte[] result, int dimY, int dimX, boolean positive) {
        if (positive) {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[(dimX - (x + 1)) * dimY + y] = data[y * dimX + x];
                }
            }
        } else {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[x * dimY + dimY - (y + 1)] = data[y * dimX + x];
                }
            }
        }
    }

    private static void rotate(short[] data, short[] result, int dimY, int dimX, boolean positive) {
        if (positive) {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[(dimX - (x + 1)) * dimY + y] = data[y * dimX + x];
                }
            }
        } else {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[x * dimY + dimY - (y + 1)] = data[y * dimX + x];
                }
            }
        }
    }

    private static void rotate(int[] data, int[] result, int dimY, int dimX, boolean positive) {
        if (positive) {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[(dimX - (x + 1)) * dimY + y] = data[y * dimX + x];
                }
            }
        } else {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[x * dimY + dimY - (y + 1)] = data[y * dimX + x];
                }
            }
        }
    }

    private static void rotate(long[] data, long[] result, int dimY, int dimX, boolean positive) {
        if (positive) {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[(dimX - (x + 1)) * dimY + y] = data[y * dimX + x];
                }
            }
        } else {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[x * dimY + dimY - (y + 1)] = data[y * dimX + x];
                }
            }
        }
    }

    private static void rotate(float[] data, float[] result, int dimY, int dimX, boolean positive) {
        if (positive) {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[(dimX - (x + 1)) * dimY + y] = data[y * dimX + x];
                }
            }
        } else {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[x * dimY + dimY - (y + 1)] = data[y * dimX + x];
                }
            }
        }
    }

    private static void rotate(double[] data, double[] result, int dimY, int dimX, boolean positive) {
        if (positive) {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[(dimX - (x + 1)) * dimY + y] = data[y * dimX + x];
                }
            }
        } else {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[x * dimY + dimY - (y + 1)] = data[y * dimX + x];
                }
            }
        }
    }

    protected static void revert(Object[] source, Object[] dest) {
        for (int i = 0; i < source.length; i++) {
            dest[source.length - (i + 1)] = source[i];
        }
    }

    protected static void revert(char[] source, char[] dest) {
        for (int i = 0; i < source.length; i++) {
            dest[source.length - (i + 1)] = source[i];
        }
    }

    protected static void revert(boolean[] source, boolean[] dest) {
        for (int i = 0; i < source.length; i++) {
            dest[source.length - (i + 1)] = source[i];
        }
    }

    protected static void revert(byte[] source, byte[] dest) {
        for (int i = 0; i < source.length; i++) {
            dest[source.length - (i + 1)] = source[i];
        }
    }

    protected static void revert(short[] source, short[] dest) {
        for (int i = 0; i < source.length; i++) {
            dest[source.length - (i + 1)] = source[i];
        }
    }

    protected static void revert(int[] source, int[] dest) {
        for (int i = 0; i < source.length; i++) {
            dest[source.length - (i + 1)] = source[i];
        }
    }

    protected static void revert(long[] source, long[] dest) {
        for (int i = 0; i < source.length; i++) {
            dest[source.length - (i + 1)] = source[i];
        }
    }

    protected static void revert(float[] source, float[] dest) {
        for (int i = 0; i < source.length; i++) {
            dest[source.length - (i + 1)] = source[i];
        }
    }

    protected static void revert(double[] source, double[] dest) {
        for (int i = 0; i < source.length; i++) {
            dest[source.length - (i + 1)] = source[i];
        }
    }

    /**
     * One trivial case of reverting a toString done on a 1D array, of doubles in this case.
     * 
     * @param arrayAsString
     * @return the parsed string, or double[0] if arrayAsString is null
     */
    public static double[] doubleArrayFromString(String arrayAsString) {
        if (arrayAsString == null) {
            return new double[0];
        }
        String[] strings = arrayAsString.replace("[", ObjectUtils.EMPTY_STRING).replace("]", ObjectUtils.EMPTY_STRING)
                .split(",");
        double[] result = new double[strings.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = Double.parseDouble(strings[i]);
        }
        return result;
    }

    /**
     * Creates a new array of expected dimensions, just like {@link Array#newInstance(Class, int...)}, trying to do it a
     * quicker way.
     * 
     * @param type The Class object representing the component type of the new array
     * @param dims The expected dimensions of the new array.
     * @return The new array.
     */
    public static Object newInstance(Class<?> type, int... dims) {
        Object array;
        if (dims == null) {
            array = null;
        } else if (dims.length == 1) {
            array = newArrayInstance(type, dims[0]);
        } else if (dims.length == 2) {
            array = newArrayInstance(type, dims[0], dims[1]);
        } else {
            // We can't guess the expected array
            array = Array.newInstance(type, dims);
        }
        return array;
    }

    private static Object newArrayInstance(Class<?> type, int length) {
        Object array;
        if (type == null) {
            array = null;
        } else if (type == Boolean.TYPE) {
            array = new boolean[length];
        } else if (type == Character.TYPE) {
            array = new char[length];
        } else if (type == Byte.TYPE) {
            array = new byte[length];
        } else if (type == Short.TYPE) {
            array = new short[length];
        } else if (type == Integer.TYPE) {
            array = new int[length];
        } else if (type == Long.TYPE) {
            array = new long[length];
        } else if (type == Float.TYPE) {
            array = new float[length];
        } else if (type == Double.TYPE) {
            array = new double[length];
        } else if (type == String.class) {
            array = new String[length];
        } else {
            // We can't guess the expected array
            array = Array.newInstance(type, length);
        }
        return array;
    }

    private static Object[] newArrayInstance(Class<?> type, int dim1, int dim2) {
        Object[] array;
        if (type == null) {
            array = null;
        } else if (type == Boolean.TYPE) {
            array = new boolean[dim1][dim2];
        } else if (type == Character.TYPE) {
            array = new char[dim1][dim2];
        } else if (type == Byte.TYPE) {
            array = new byte[dim1][dim2];
        } else if (type == Short.TYPE) {
            array = new short[dim1][dim2];
        } else if (type == Integer.TYPE) {
            array = new int[dim1][dim2];
        } else if (type == Long.TYPE) {
            array = new long[dim1][dim2];
        } else if (type == Float.TYPE) {
            array = new float[dim1][dim2];
        } else if (type == Double.TYPE) {
            array = new double[dim1][dim2];
        } else if (type == String.class) {
            array = new String[dim1][dim2];
        } else {
            // We can't guess the expected array
            array = (Object[]) Array.newInstance(type, dim1, dim2);
        }
        return array;
    }

    /**
     * Returns an array of expected maximum length, that is either a copy of given array or the given array itself if
     * its length is short enough.
     * 
     * @param array The array.
     * @param maxLength The maximum length.
     * @param keepLastElements Whether, in case of array reduction, to keep last elements and remove first ones, or to
     *            keep first elements and remove last ones.
     *            <p>
     *            <code>true</code> to keep last elements, <code>false</code> top keep first ones.
     *            </p>
     * @return An array.
     */
    public static <T> T getLimitedArray(T array, int maxLength, boolean keepLastElements) {
        Object newArray = array;
        if ((newArray != null) && newArray.getClass().isArray() && (maxLength > -1)) {
            if (newArray instanceof Object[]) {
                Object[] tmpArray = (Object[]) newArray;
                if (tmpArray.length > maxLength) {
                    if (keepLastElements) {
                        newArray = Arrays.copyOfRange(tmpArray, tmpArray.length - maxLength, tmpArray.length);
                    } else {
                        newArray = Arrays.copyOf(tmpArray, maxLength);
                    }
                }
            } else if (newArray instanceof boolean[]) {
                boolean[] tmpArray = (boolean[]) newArray;
                if (tmpArray.length > maxLength) {
                    if (keepLastElements) {
                        newArray = Arrays.copyOfRange(tmpArray, tmpArray.length - maxLength, tmpArray.length);
                    } else {
                        newArray = Arrays.copyOf(tmpArray, maxLength);
                    }
                }
            } else if (newArray instanceof char[]) {
                char[] tmpArray = (char[]) newArray;
                if (tmpArray.length > maxLength) {
                    if (keepLastElements) {
                        newArray = Arrays.copyOfRange(tmpArray, tmpArray.length - maxLength, tmpArray.length);
                    } else {
                        newArray = Arrays.copyOf(tmpArray, maxLength);
                    }
                }
            } else if (newArray instanceof byte[]) {
                byte[] tmpArray = (byte[]) newArray;
                if (tmpArray.length > maxLength) {
                    if (keepLastElements) {
                        newArray = Arrays.copyOfRange(tmpArray, tmpArray.length - maxLength, tmpArray.length);
                    } else {
                        newArray = Arrays.copyOf(tmpArray, maxLength);
                    }
                }
            } else if (newArray instanceof short[]) {
                short[] tmpArray = (short[]) newArray;
                if (tmpArray.length > maxLength) {
                    if (keepLastElements) {
                        newArray = Arrays.copyOfRange(tmpArray, tmpArray.length - maxLength, tmpArray.length);
                    } else {
                        newArray = Arrays.copyOf(tmpArray, maxLength);
                    }
                }
            } else if (newArray instanceof int[]) {
                int[] tmpArray = (int[]) newArray;
                if (tmpArray.length > maxLength) {
                    if (keepLastElements) {
                        newArray = Arrays.copyOfRange(tmpArray, tmpArray.length - maxLength, tmpArray.length);
                    } else {
                        newArray = Arrays.copyOf(tmpArray, maxLength);
                    }
                }
            } else if (newArray instanceof long[]) {
                long[] tmpArray = (long[]) newArray;
                if (tmpArray.length > maxLength) {
                    if (keepLastElements) {
                        newArray = Arrays.copyOfRange(tmpArray, tmpArray.length - maxLength, tmpArray.length);
                    } else {
                        newArray = Arrays.copyOf(tmpArray, maxLength);
                    }
                }
            } else if (newArray instanceof float[]) {
                float[] tmpArray = (float[]) newArray;
                if (tmpArray.length > maxLength) {
                    if (keepLastElements) {
                        newArray = Arrays.copyOfRange(tmpArray, tmpArray.length - maxLength, tmpArray.length);
                    } else {
                        newArray = Arrays.copyOf(tmpArray, maxLength);
                    }
                }
            } else if (newArray instanceof double[]) {
                double[] tmpArray = (double[]) newArray;
                if (tmpArray.length > maxLength) {
                    if (keepLastElements) {
                        newArray = Arrays.copyOfRange(tmpArray, tmpArray.length - maxLength, tmpArray.length);
                    } else {
                        newArray = Arrays.copyOf(tmpArray, maxLength);
                    }
                }
            }
        }
        @SuppressWarnings("unchecked")
        T result = (T) newArray;
        return result;
    }

    /**
     * Adds some data at the end of given array.
     * 
     * @param array The array.
     * @param data The data to add.
     * @return The array or a copy of it with the right length and the added data at the end of it.
     */
    public static <T> T[] addDataToArray(T[] array, @SuppressWarnings("unchecked") T... data) {
        T[] result;
        if (array == null) {
            if (data == null) {
                result = null;
            } else {
                result = data.clone();
            }
        } else if ((data != null) && (data.length > 0)) {
            int startIndex = array.length;
            result = Arrays.copyOf(array, array.length + data.length);
            System.arraycopy(data, 0, result, startIndex, data.length);
        } else {
            result = array;
        }
        return result;
    }

}
