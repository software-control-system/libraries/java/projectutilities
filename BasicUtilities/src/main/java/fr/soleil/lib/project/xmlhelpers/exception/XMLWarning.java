/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.xmlhelpers.exception;

public class XMLWarning extends Exception {

    private static final long serialVersionUID = 4568847931548670829L;

    public XMLWarning() {
        super();
    }

    public XMLWarning(String message) {
        super(message);
    }

    public XMLWarning(Throwable cause) {
        super(cause);
    }

    public XMLWarning(String message, Throwable cause) {
        super(message, cause);
    }

}
