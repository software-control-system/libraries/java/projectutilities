/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.data;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class FlatDataUtils {

    private FlatDataUtils() {
    }

    public static <F extends AFlatData<?>> F buildNewFlatData(F data, Object newValue, int... newShape) {
        F result;
        if (data == null) {
            result = null;
        } else {
            Object value = data.getValue();
            if ((value == null) || (newValue == null) || (!value.getClass().equals(newValue.getClass()))) {
                result = null;
            } else {
                Class<?> valueClass = value.getClass();
                try {
                    @SuppressWarnings("unchecked")
                    Constructor<? extends F> constructor = (Constructor<? extends F>) data.getClass()
                            .getConstructor(int[].class, valueClass);
                    try {
                        result = constructor.newInstance(newShape, newValue);
                    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                            | InvocationTargetException e) {
                        e.printStackTrace();
                        result = null;
                    }
                } catch (NoSuchMethodException | SecurityException e) {
                    try {
                        @SuppressWarnings("unchecked")
                        Constructor<? extends F> constructor = (Constructor<? extends F>) data.getClass()
                                .getConstructor(int[].class, Object.class);
                        try {
                            result = constructor.newInstance(newShape, newValue);
                        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                                | InvocationTargetException e2) {
                            e.printStackTrace();
                            result = null;
                        }
                    } catch (NoSuchMethodException | SecurityException e1) {
                        e1.printStackTrace();
                        result = null;
                    }
                }
            }
        }
        return result;
    }
}
