/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.progress;

/**
 * An interface for something that manages some progression
 */
public interface IProgressable {

    /**
     * Registers an {@link IProgressionListener} for this {@link IProgressable}
     * 
     * @param listener The {@link IProgressionListener} to register
     */
    public void addProgressionListener(final IProgressionListener listener);

    /**
     * Unregisters an {@link IProgressionListener} for this {@link IProgressable}
     * 
     * @param listener The {@link IProgressionListener} to unregister
     */
    public void removeProgressionListener(final IProgressionListener listener);

}
