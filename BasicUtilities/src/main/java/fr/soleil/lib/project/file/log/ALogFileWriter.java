/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.file.log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.log.LogData;
import fr.soleil.lib.project.resource.MessageManager;

/**
 * Abstract class for {@link ILogFileWriter} that gives some common methods to manage files.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class ALogFileWriter implements ILogFileWriter {

    protected final MessageManager messageManager;

    public ALogFileWriter(MessageManager messageManager) {
        this.messageManager = messageManager == null ? MessageManager.DEFAULT_MESSAGE_MANAGER : messageManager;
    }

    /**
     * Writes some {@link LogData} in a file through a {@link FileWriter}.
     * 
     * @param writer The {@link FileWriter}. Must not be <code>null</code>.
     * @param logs The data to write.
     * @throws IOException If a problem occurred.
     */
    protected abstract void writeLogs(FileWriter writer, LogData... logs) throws IOException;

    @Override
    public void writeLogs(File logFile, LogData... logs) throws IOException {
        if (logFile == null) {
            throw new IOException(messageManager.getMessage("fr.soleil.lib.project.log.file.null.error"));
        } else if (logFile.isDirectory()) {
            throw new IOException(messageManager.getMessage("fr.soleil.lib.project.log.file.directory.error"));
        } else {
            String extension = FileUtils.getExtension(logFile);
            boolean valid = false;
            String[] extensions = getManagedFileExtensions();
            for (String ext : extensions) {
                if (ext.equalsIgnoreCase(extension)) {
                    valid = true;
                    break;
                }
            }
            if (valid) {
                try (FileWriter writer = new FileWriter(logFile);) {
                    writeLogs(writer, logs);
                }
            } else {
                throw new IOException(
                        String.format(messageManager.getMessage("fr.soleil.lib.project.log.file.extension.error"),
                                logFile.getAbsolutePath(),
                                extensions.length == 1 ? extensions[0] : Arrays.toString(extensions)));
            }
        }
    }

}
