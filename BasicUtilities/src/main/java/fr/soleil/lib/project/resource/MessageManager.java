/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.resource;

import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A class that recovers application messages. Useful for possible translations.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class MessageManager {

    public static final MessageManager DEFAULT_MESSAGE_MANAGER = new MessageManager("fr.soleil.lib.project");

    private final String resourcePackage;
    private Map<String, String> resources;
    private Map<String, String> logResources;
    private Map<String, String> appProperties;
    private boolean resourceInitialized;
    private boolean logInitialized;
    private boolean appInitialized;
    private final ClassLoader classLoader;

    public MessageManager(String resourcePackage) {
        this(resourcePackage, null);
    }

    public MessageManager(String resourcePackage, ClassLoader classLoader) {
        this.resourcePackage = resourcePackage;
        resources = null;
        logResources = null;
        appProperties = null;
        resourceInitialized = false;
        logInitialized = false;
        appInitialized = false;
        this.classLoader = classLoader;
    }

    private Utf8PropertyResourceBundle createResourceBundle(String baseName, Locale locale) {
        Utf8PropertyResourceBundle result;
        try {
            result = new Utf8PropertyResourceBundle(baseName, locale, classLoader);
            if (!result.getKeys().hasMoreElements()) {
                result = null;
            }
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    private Utf8PropertyResourceBundle createResourceBundle(String baseName) {
        Utf8PropertyResourceBundle bundle;
        try {
            bundle = createResourceBundle(baseName, Locale.getDefault());
            if (bundle == null) {
                bundle = createResourceBundle(baseName, Locale.US);
            }
        } catch (Exception e) {
            try {
                bundle = createResourceBundle(baseName, Locale.US);
            } catch (Exception e2) {
                bundle = null;
            }
        }
        return bundle;
    }

    /**
     * Initializes the different {@link ResourceBundle}s used by this class.
     * 
     * @param currentLocale The {@link Locale} to use.
     * @throws Exception.
     */
    public void initResourceBundle(Locale currentLocale) {
        resources = getMap(createResourceBundle(resourcePackage + ".messages.resources", currentLocale));
        resourceInitialized = (resources != null);
        logResources = getMap(createResourceBundle(resourcePackage + ".messages.logs", currentLocale));
        logInitialized = (logResources != null);
        appProperties = getMap(createResourceBundle(resourcePackage + ".application", currentLocale));
        appInitialized = (appProperties != null);
    }

    /**
     * Return the resources package this {@link MessageManager} was built with.
     * 
     * @return A {@link String}.
     */
    public String getResourcePackage() {
        return resourcePackage;
    }

    /**
     * Returns the message identified by a particular key.
     * 
     * @param key The key.
     * @return A {@link String}.
     */
    public String getMessage(String key) {
        checkResourceBundles();
        return getMessage(resources, key);
    }

    /**
     * Returns the log message identified by a particular key.
     * 
     * @param key The key.
     * @return A {@link String}.
     */
    public String getLogMessage(String key) {
        checkResourceBundles();
        return getMessage(logResources, key);
    }

    /**
     * Returns the application information identified by a particular key.
     * 
     * @param key The key.
     * @return A {@link String}.
     */
    public String getAppMessage(String key) {
        checkResourceBundles();
        return getMessage(appProperties, key);
    }

    /**
     * Pushes the messages of a {@link MessageManager} in this {@link MessageManager}.
     * 
     * @param messageManager The {@link MessageManager} from which to extract messages.
     * @return this {@link MessageManager}. Useful to chain some methods.
     */
    public MessageManager pushProperties(MessageManager messageManager) {
        if (messageManager != null) {
            checkResourceBundles();
            messageManager.checkResourceBundles();
            pushProperties(appProperties, messageManager.appProperties);
            pushProperties(resources, messageManager.resources);
            pushProperties(logResources, messageManager.logResources);
        }
        return this;
    }

    /**
     * Pushes the messages of a {@link MessageManager}, that are not already configured in this {@link MessageManager},
     * in this {@link MessageManager}.
     * 
     * @param messageManager The {@link MessageManager} from which to extract messages.
     */
    public void pushPropertiesIfAbsent(MessageManager messageManager) {
        if (messageManager != null) {
            checkResourceBundles();
            messageManager.checkResourceBundles();
            pushPropertiesIfAbsent(appProperties, messageManager.appProperties);
            pushPropertiesIfAbsent(resources, messageManager.resources);
            pushPropertiesIfAbsent(logResources, messageManager.logResources);
        }
    }

    /**
     * Checks {@link ResourceBundle}s initializations.
     */
    private void checkResourceBundles() {
        try {
            if (!resourceInitialized) {
                resources = getMap(createResourceBundle(resourcePackage + ".messages.resources"));
                resourceInitialized = true;
            }
            if (!logInitialized) {
                logResources = getMap(createResourceBundle(resourcePackage + ".messages.logs"));
                logInitialized = true;
            }
            if (!appInitialized) {
                appProperties = getMap(createResourceBundle(resourcePackage + ".application"));
                appInitialized = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isResourceBundleInitialised() {
        return (resourceInitialized || logInitialized || appInitialized);
    }

    private String getMessage(Map<String, String> resourceBundle, String key) {
        String ret;
        if (resourceBundle == null) {
            ret = key;
        } else {
            try {
                ret = resourceBundle.get(key);
                if (ret == null) {
                    ret = key;
                }
            } catch (Exception e) {
                ret = key;
            }
        }
        return ret;
    }

    private Map<String, String> getMap(ResourceBundle bundle) {
        Map<String, String> result;
        if (bundle == null) {
            result = null;
        } else {
            result = new ConcurrentHashMap<String, String>();
            pushProperties(result, bundle);
        }
        return result;
    }

    private void pushProperties(Map<String, String> dest, Map<String, String> source) {
        if ((dest != null) && (source != null)) {
            for (Entry<String, String> entry : source.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if ((key != null) && (value != null)) {
                    dest.put(key, value);
                }
            }
        }
    }

    private void pushPropertiesIfAbsent(Map<String, String> dest, Map<String, String> source) {
        if ((dest != null) && (source != null)) {
            for (Entry<String, String> entry : source.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if ((key != null) && (value != null) && (dest.get(key) == null)) {
                    dest.put(key, value);
                }
            }
        }
    }

    private void pushProperties(Map<String, String> dest, ResourceBundle source) {
        if ((dest != null) && (source != null)) {
            Enumeration<String> keys = source.getKeys();
            if (keys != null) {
                while (keys.hasMoreElements()) {
                    String key = keys.nextElement();
                    try {
                        dest.put(key, source.getString(key));
                    } catch (Exception e) {
                        continue;
                    }
                }
            }
        }
    }

}
