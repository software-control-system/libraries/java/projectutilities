/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Wrapper around AtomicLong to make working with double values easier.
 * 
 * @author pilif, Rapha&euml;l GIRARDOT
 */
public class AtomicDouble extends Number {

    private static final long serialVersionUID = -7247121423958498930L;

    private final AtomicLong value;

    /** Create an instance with an initial value of 0. */
    public AtomicDouble() {
        this(0.0);
    }

    /** Create an instance with an initial value of {@code init}. */
    public AtomicDouble(double init) {
        super();
        value = new AtomicLong(Double.doubleToRawLongBits(init));
    }

    /** Return the current value. */
    public double get() {
        return Double.longBitsToDouble(value.get());
    }

    /**
     * Set the current value to {@code amount}.
     */
    public void set(double amount) {
        value.set(Double.doubleToRawLongBits(amount));
    }

    /**
     * Eventually sets to the given value.
     *
     * @param newValue the new value
     */
    public final void lazySet(double newValue) {
        value.lazySet(Double.doubleToRawLongBits(newValue));
    }

    /**
     * Set the value to {@code amount} and return the previous value.
     * 
     * @param amount The value to set
     * @return The previous value
     */
    public double getAndSet(double amount) {
        long v = value.getAndSet(Double.doubleToRawLongBits(amount));
        return Double.longBitsToDouble(v);
    }

    /**
     * Set the value to {@code amount} if the current value is {@code expect}. Return true if the
     * value was updated.
     * 
     * @param expect The expected current value
     * @param The value to set
     * @return Whether the value was set
     */
    public boolean compareAndSet(double expect, double update) {
        long e = Double.doubleToRawLongBits(expect);
        long u = Double.doubleToRawLongBits(update);
        return value.compareAndSet(e, u);
    }

    /**
     * Atomically sets the value to the given updated value
     * if the current value {@code ==} the expected value.
     *
     * <p>
     * May fail spuriously and does not provide ordering guarantees, so is only rarely an appropriate alternative to
     * {@code compareAndSet}.
     *
     * @param expect the expected value
     * @param update the new value
     * @return true if successful.
     */
    public final boolean weakCompareAndSet(double expect, double update) {
        return value.weakCompareAndSet(Double.doubleToRawLongBits(expect), Double.doubleToRawLongBits(update));
    }

    /**
     * Atomically increments by one the current value.
     *
     * @return the previous value
     */
    public final double getAndIncrement() {
        double current, next;
        do {
            current = get();
            next = current + 1;
        } while (!compareAndSet(current, next));
        return current;
    }

    /**
     * Atomically decrements by one the current value.
     *
     * @return the previous value
     */
    public final double getAndDecrement() {
        double current, next;
        do {
            current = get();
            next = current - 1;
        } while (!compareAndSet(current, next));
        return current;
    }

    /**
     * Add {@code amount} to the value and return the previous value.
     * 
     * @param amount The amount to add
     * @return The previous value
     */
    public double getAndAdd(double amount) {
        double current, next;
        long currentLong, nextLong;
        do {
            currentLong = value.get();
            current = Double.longBitsToDouble(currentLong);
            next = current + amount;
            nextLong = Double.doubleToRawLongBits(next);
        } while (!value.compareAndSet(currentLong, nextLong));
        return current;
    }

    /**
     * Atomically increments by one the current value.
     *
     * @return the updated value
     */
    public final double incrementAndGet() {
        double current, next;
        long currentLong, nextLong;
        do {
            currentLong = value.get();
            current = Double.longBitsToDouble(currentLong);
            next = current + 1;
            nextLong = Double.doubleToRawLongBits(next);
        } while (!value.compareAndSet(currentLong, nextLong));
        return next;
    }

    /**
     * Atomically decrements by one the current value.
     *
     * @return The updated value
     */
    public final double decrementAndGet() {
        double current, next;
        long currentLong, nextLong;
        do {
            currentLong = value.get();
            current = Double.longBitsToDouble(currentLong);
            next = current - 1;
            nextLong = Double.doubleToRawLongBits(next);
        } while (!value.compareAndSet(currentLong, nextLong));
        return next;
    }

    /**
     * Add {@code amount} to the value and return the new value.
     * 
     * @param amount The amount to add
     * @return The updated value
     */
    public double addAndGet(double amount) {
        double current, next;
        long currentLong, nextLong;
        do {
            currentLong = value.get();
            current = Double.longBitsToDouble(currentLong);
            next = current + amount;
            nextLong = Double.doubleToRawLongBits(next);
        } while (!value.compareAndSet(currentLong, nextLong));
        return next;
    }

    @Override
    public int intValue() {
        return (int) get();
    }

    @Override
    public long longValue() {
        return (long) get();
    }

    @Override
    public float floatValue() {
        return (float) get();
    }

    @Override
    public double doubleValue() {
        return get();
    }

    @Override
    public String toString() {
        return Double.toString(get());
    }
}
