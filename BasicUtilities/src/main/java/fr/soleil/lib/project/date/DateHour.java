/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

/**
 * Date and hour management.
 * <p>
 * Java classes that manage date and hour are not always handful, and do not allow to measure the gap between 2 dates.
 * </p>
 * <p>
 * In {@link DateHour} construction, given date is automatically corrected.
 * </p>
 * <p>
 * For example, if someone constructs the date <code>32/05/2002</code>, it will be converted to <code>01/06/2002</code>
 * </p>
 * In order to test date validity, the {@link #wasValid()} method indicates whether the given date is correct or not.
 * </p>
 * This class contains following elements:
 * <ul>
 * <li>Some useful constants to format an hour: {@link #FORMAT_DATE_TEXT}, {@link #FORMAT_DATE_NORMAL},
 * {@link #FORMAT_HEURE_NORMAL}, {@link #FORMAT_DATE_HEURE_NORMAL}, {@link #FORMAT_DATE_HEURE_MILLI_NORMAL},
 * {@link #FORMAT_DATE_SQL}, {@link #FORMAT_DATE_DB2} et {@link #FORMAT_DATE_HEURE_XML}.</li>
 * <li>Some useful constants for each day of week (1 corresponds t monday and not to sunday like some other systems):
 * {@link #MONDAY}, {@link #TUESDAY}, {@link #WEDNESDAY}, {@link #THIRSDAY}, {@link #FRIDAY}, {@link #SATURDAY} and
 * {@link #SUNDAY}.</li>
 * <li>Some constants for each month (from 1 to 12) : {@link #JANUARY}, {@link #FEBRUARY}, {@link #MARCH},
 * {@link #APRIL}, {@link #MAY}, {@link #JUNE}, {@link #JULY}, {@link #AUGUST}, {@link #SEPTEMBER}, {@link #OCTOBER},
 * {@link #NOVEMBER} and {@link #DECEMBER}.</li>
 * <li>Different constructors to create dates from many formats.</li>
 * <li>A {@link #wasValid()} method to know whether the date given in constructor is correct.</li>
 * <li>Some conversion methods to convert {@link DateHour} into {@link Date} ({@link #toDate()} ), into
 * <code>long</code> ( {@link #toTimeInMilliSeconds()} ), into {@link String} ( {@link #toString()} and
 * {@link #toString(java.lang.String)}) and to make acopy of the {@link DateHour} object ( {@link #clone()}).</li>
 * <li>2 methods to know whether a year is leap ( {@link #isLeap()} et {@link #isLeap(int)}).</li>
 * <li>Some methods to read date and hour data ( {@link #getYear()}, {@link #getMonth()}, {@link #getDay()},
 * {@link #getDayOfWeek()}, {@link #getDayOfYear()}, {@link #getHour()}, {@link #getMinute()}, {@link #getSecond()} and
 * {@link #getMillisecond()}).</li>
 * <li>Some methods to modify date and hour data ( {@link #addYear(int)}, {@link #addMonth(int)}, {@link #addDay(int)},
 * {@link #addHour(int)}, {@link #addMinute(int)}, {@link #addSecond(int)} et {@link #addMillisecond(int)}).</li>
 * <li>Some methods to compare 2 dates ( {@link #compareTo(java.lang.Object)} et {@link #equals(java.lang.Object)}
 * ).</li>
 * <li>Some methods to calculate the gap between 2 dates ( {@link #yearsBetween(DateHour)},
 * {@link #monthsBetween(DateHour)}, {@link #daysBetween(DateHour)}, {@link #hoursBetween(DateHour)},
 * {@link #minutesBetween(DateHour)}, {@link #secondsBetween(DateHour)} and
 * {@link #millisecondsBetween(DateHour)}).</li>
 * </ul>
 */
public class DateHour implements java.io.Serializable, Cloneable, Comparable<DateHour> {

    private static final long serialVersionUID = 5844656626657047199L;

    // ---------- Constants : dates and hours formats ----------
    /**
     * Format text : <code>"DayOfWeekInLetters DayOfMonth MonthInLetters Year"</code>.
     */
    public static final String FORMAT_DATE_TEXT = "EEEE dd MMMM yyyy";

    /**
     * Format text : <code>"dd/mm/yyyy"</code>.
     */
    public static final String FORMAT_DATE_NORMAL = "dd/MM/yyyy";

    /**
     * Format text : <code>"hh:mm:ss"</code>.
     */
    public static final String FORMAT_HEURE_NORMAL = "HH:mm:ss";

    /**
     * Format text : <code>"dd/mm/yyyy hh:mm:ss"</code>.
     */
    public static final String FORMAT_DATE_HEURE_NORMAL = "dd/MM/yyyy HH:mm:ss";

    /**
     * Format text : <code>"dd/mm/yyyy hh:mm:ss:mmm"</code>.
     */
    public static final String FORMAT_DATE_HEURE_MILLI_NORMAL = "dd/MM/yyyy HH:mm:ss:SSS";

    /**
     * Format text : <code>"yyyy-mm-dd"</code>.
     */
    public static final String FORMAT_DATE_SQL = "yyyy-MM-dd";

    public static final String FORMAT_DATE_HEURE_SQL = "yyyy-MM-dd HH:mm:ss";

    public static final String FORMAT_DATE_HEURE_MANAGER = "dd-MM-yyyy HH:mm:ss.SS";

    /**
     * Format text : <code>"yyyymmdd"</code>.
     */
    public static final String FORMAT_DATE_DB2 = "yyyyMMdd";

    public static final String FORMAT_DATE_MYSQL = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_DATE_ORACLE = "dd-MM-yyyy HH:mm:ss";

    /**
     * Format text : <code>"yyyy-mm-ddThh:mm:ss"</code>.
     */
    public static final String FORMAT_DATE_HEURE_XML = "yyyy-MM-dd'T'HH:mm:ss";

    // ---------- Constantes : jours de la semaine ----------
    /**
     * Constant for day of week: monday = 1.
     */
    public static final int MONDAY = 1;

    /**
     * Constant for day of week: tuesday = 2.
     */
    public static final int TUESDAY = 2;

    /**
     * Constant for day of week: wednesday = 3.
     */
    public static final int WEDNESDAY = 3;

    /**
     * Constant for day of week: thirsday = 4.
     */
    public static final int THIRSDAY = 4;

    /**
     * Constant for day of week: friday = 5.
     */
    public static final int FRIDAY = 5;

    /**
     * Constant for day of week: saturday = 6.
     */
    public static final int SATURDAY = 6;

    /**
     * Constant for day of week: sunday = 7.
     */
    public static final int SUNDAY = 7;

    /**
     * Month constant: january = 1.
     */
    public static final int JANUARY = 1;

    /**
     * Month constant: february = 2.
     */
    public static final int FEBRUARY = 2;

    /**
     * Month constant: march = 3.
     */
    public static final int MARCH = 3;

    /**
     * Month constant: april = 4.
     */
    public static final int APRIL = 4;

    /**
     * Month constant: may = 5.
     */
    public static final int MAY = 5;

    /**
     * Month constant: june = 6.
     */
    public static final int JUNE = 6;

    /**
     * Month constant: july = 7.
     */
    public static final int JULY = 7;

    /**
     * Month constant: august = 8.
     */
    public static final int AUGUST = 8;

    /**
     * Month constant: september = 9.
     */
    public static final int SEPTEMBER = 9;

    /**
     * Month constant: october = 10.
     */
    public static final int OCTOBER = 10;

    /**
     * Month constant: november = 11.
     */
    public static final int NOVEMBER = 11;

    /**
     * Month constant: december = 12.
     */
    public static final int DECEMBER = 12;

    // ---------- Class attributes ----------
    /**
     * Number of days per month (february is counted as 28 days).
     */
    private static final int[] DAYS_PER_MONTH = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    // ---------- Object attributes ----------
    /**
     * Indicates year.
     */
    private int year;

    /**
     * Indicates month.
     */
    private int month;

    /**
     * Indicates day.
     */
    private int day;

    /**
     * Indicates hour.
     */
    private int hour;

    /**
     * Indicates minutes.
     */
    private int minute;

    /**
     * Indicates seconds.
     */
    private int second;

    /**
     * Indicates milliseconds.
     */
    private int milli;

    /**
     * Indicates whether date was valid on object construction (it is automatically corrected by the constructor)
     */
    private boolean dateWasValide;

    // ---------- Constructeurs ----------
    /**
     * Constructs a new {@link DateHour} object at current date and time.
     */
    public DateHour() {
        this(new Date());
    }

    /**
     * Constructs a new {@link DateHour} object from a {@link Date}.
     * 
     * @param date The {@link Date} to use.
     */
    public DateHour(Date date) {
        dateToNumber(date, this);
        dateWasValide = true;
    }

    /**
     * Constructs a new {@link DateHour} object from {@link String} date and a format. <code>format</code> must be one
     * of the constants <code>FORMAT_XXX</code> of this class, or a custom format.
     * 
     * @param date {@link String} representing date and hour.
     * @param format Date format.
     * @throws ParseException If the {@link String} can't be parsed.
     * @see #FORMAT_DATE_TEXT
     * @see #FORMAT_DATE_NORMAL
     * @see #FORMAT_HEURE_NORMAL
     * @see #FORMAT_DATE_HEURE_NORMAL
     * @see #FORMAT_DATE_HEURE_MILLI_NORMAL
     * @see #FORMAT_DATE_SQL
     * @see #FORMAT_DATE_DB2
     * @see #FORMAT_DATE_HEURE_XML
     * @see SimpleDateFormat
     */
    public DateHour(String date, String format) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Date dateObj = dateFormat.parse(date);

        // Date validity test
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(date);
            dateWasValide = true;
        } catch (ParseException pEx) {
            dateWasValide = false;
        }

        // Attributes initialization
        dateToNumber(dateObj, this);
    }

    /**
     * Constructs a new {@link DateHour} object from given date (hour is initialized to 0). <br />
     * If given date is incorrect, it is automatically corrected.<br />
     * To known whether it was correct, call {@link #wasValid()}.
     * 
     * @param year Indicates year.
     * @param month Indicates month.
     * @param day Indicates day.
     */
    public DateHour(int year, int month, int day) {
        this(year, month, day, 0, 0, 0, 0);
    }

    /**
     * Constructs a new {@link DateHour} object from given date and hour (milliseconds are initialized to 0). <br />
     * If given date is incorrect, it is automatically corrected.<br />
     * To known whether it was correct, call {@link #wasValid()}.
     * 
     * @param year Indicates year.
     * @param month Indicates month.
     * @param day Indicates day.
     * @param hour Indicates hour.
     * @param minute Indicates minutes.
     * @param second Indicates seconds.
     */
    public DateHour(int year, int month, int day, int hour, int minute, int second) {
        this(year, month, day, hour, minute, second, 0);
    }

    /**
     * Constructs a new {@link DateHour} object from given date, hour and milliseconds. <br />
     * If given date is incorrect, it is automatically corrected.<br />
     * To known whether it was correct, call {@link #wasValid()}.
     * 
     * @param year Indicates year.
     * @param month Indicates month.
     * @param day Indicates day.
     * @param hour Indicates hour.
     * @param minute Indicates minutes.
     * @param second Indicates seconds.
     * @param millisecond Indicates milliseconds.
     */
    public DateHour(int year, int month, int day, int hour, int minute, int second, int millisecond) {
        GregorianCalendar calend = new GregorianCalendar(year, month - 1, day, hour, minute, second);
        Date date = new Date(calend.getTime().getTime() + millisecond);

        dateToNumber(date, this);

        dateWasValide = (this.year == year) && (this.month == month) && (this.day == day) && (this.hour == hour)
                && (this.minute == minute) && (this.second == second) && (this.milli == millisecond);
    }

    // ---------- Static methods ----------

    /**
     * Splits a {@link Date} into years, months, days, hours, minutes, seconds and milliseconds, and applies it to a
     * {@link DateHour}
     * 
     * @param date The {@link Date} to split.
     * @param obj The {@link DateHour} to which to apply the decomposition
     */
    private static void dateToNumber(Date date, DateHour obj) {
        long timeMilli = date.getTime();

        GregorianCalendar calend = new GregorianCalendar();
        calend.setTime(date);

        obj.year = calend.get(Calendar.YEAR);
        obj.month = calend.get(Calendar.MONTH) + 1;
        obj.day = calend.get(Calendar.DAY_OF_MONTH);
        obj.hour = calend.get(Calendar.HOUR_OF_DAY);
        obj.minute = calend.get(Calendar.MINUTE);
        obj.second = calend.get(Calendar.SECOND);

        calend = new GregorianCalendar(obj.year, obj.month - 1, obj.day, obj.hour, obj.minute, obj.second);

        obj.milli = (int) (timeMilli - calend.getTime().getTime());
    }

    /**
     * Converts an incorrect date into a calendar one.
     * 
     * @param dateHour The date to correct.
     */
    private static void corrigeDateHour(DateHour dateHour) {
        GregorianCalendar calend = new GregorianCalendar(dateHour.year, dateHour.month - 1, dateHour.day, dateHour.hour,
                dateHour.minute, dateHour.second);
        Date date = new Date(calend.getTime().getTime() + dateHour.milli);

        dateToNumber(date, dateHour);
    }

    /**
     * Indicates whether given year is leap.
     * 
     * @param year Year for which you want to known whether it is leap.
     * @return <code>true</code>, if year is leap. <code>false</code> otherwise.
     */
    public static boolean isLeap(int year) {
        if ((year % 4) == 0) {
            if (((year % 100) == 0) && ((year % 400) != 0)) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    // ---------- Object methods ----------
    /**
     * Indicates whether the date given to constructor was correct. <br />
     * <B>Note:</B> Whathever this method's result, the inner date is correct as it was corrected by constructor.
     * 
     * @return <code>true</code> if the date given to constructor was correct.
     */
    public boolean wasValid() {
        return dateWasValide;
    }

    /**
     * Indicates whether the date corresponds to a leap year.
     * 
     * @return <code>true</code>, if the year is leap.
     */
    public boolean isLeap() {
        return isLeap(year);
    }

    /**
     * Conversion into {@link Date}.
     * 
     * @return A {@link Date} with the same date.
     */
    public Date toDate() {
        return new Date(toTimeInMilliSeconds());
    }

    /**
     * Conversion into time in milliseconds.
     * 
     * @return A <code>long</code> that represents the date in milliseconds.
     */
    public long toTimeInMilliSeconds() {
        GregorianCalendar calend = new GregorianCalendar(year, month - 1, day, hour, minute, second);
        return (calend.getTime().getTime() + milli);
    }

    /**
     * Returns a {@link String} representing date and hour.
     * The used format is {@link #FORMAT_DATE_HEURE_MILLI_NORMAL}.
     * 
     * @return A {@link String} representing date and hour.
     */
    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE_HEURE_MILLI_NORMAL);
        return dateFormat.format(toDate());
    }

    /**
     * Returns a {@link String} representing date and hour, using specific format. <br />
     * <code>format</code> is one of the constants <code>FORMAT_XXX</code> of this class, or a custom one).
     * 
     * @param format Date format.
     * @return A {@link String} representing date and hour.
     * @see #FORMAT_DATE_TEXT
     * @see #FORMAT_DATE_NORMAL
     * @see #FORMAT_HEURE_NORMAL
     * @see #FORMAT_DATE_HEURE_NORMAL
     * @see #FORMAT_DATE_HEURE_MILLI_NORMAL
     * @see #FORMAT_DATE_SQL
     * @see #FORMAT_DATE_DB2
     * @see #FORMAT_DATE_HEURE_XML
     * @see SimpleDateFormat
     */
    public String toString(String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(toDate());
    }

    /**
     * Returns the day of week. <br />
     * 1 for monday, 2 for tuesday, ..., 7 for sunday.<br />
     * It is possible to use these constants:
     * <ul>
     * <li>{@link #MONDAY}</li>
     * <li>{@link #TUESDAY}</li>
     * <li>{@link #WEDNESDAY}</li>
     * <li>{@link #THIRSDAY}</li>
     * <li>{@link #FRIDAY}</li>
     * <li>{@link #SATURDAY}</li>
     * <li>{@link #SUNDAY}</li>
     * </ul>
     * 
     * @return The day of week.
     */
    public int getDayOfWeek() {
        GregorianCalendar calend = new GregorianCalendar(year, month - 1, day, hour, minute, second);
        int dayInWeek = calend.get(Calendar.DAY_OF_WEEK) - 1;
        if (dayInWeek == 0) {
            dayInWeek = 7;
        }

        return dayInWeek;
    }

    /**
     * Returns the day of year.
     * 
     * @return The day of year.
     */
    public int getDayOfYear() {
        GregorianCalendar calend = new GregorianCalendar(year, month - 1, day, hour, minute, second);
        return calend.get(Calendar.DAY_OF_YEAR);
    }

    /**
     * Returns the number of years between this {@link DateHour} and another one
     * The result is:
     * <ul>
     * <li>0 if a == b</li>
     * <li>negative if a &lt; b</li>
     * <li>positive if a &gt; b</li>
     * </ul>
     * <br />
     * Note: This method does not take care of hours.
     * 
     * @param dateHour The {@link DateHour} to compare to.
     * @return The number of years between the 2 dates (sign).
     */
    public int yearsBetween(DateHour dateHour) {
        return monthsBetween(dateHour) / 12;
    }

    /**
     * Returns the number of months between this {@link DateHour} and another one
     * The result is:
     * <ul>
     * <li>0 if a == b</li>
     * <li>negative if a &lt; b</li>
     * <li>positive if a &gt; b</li>
     * </ul>
     * <br />
     * Note: This method does not take care of hours.
     * 
     * @param dateHour The {@link DateHour} to compare to.
     * @return The number of months between the 2 dates (sign).
     */
    public int monthsBetween(DateHour dateHour) {
        int nbMonths = 0;

        boolean positive;
        DateHour big;
        DateHour small;

        // Sign recovery.
        positive = (millisecondsBetween(dateHour) >= 0);

        // Recovering date order.
        if (positive) {
            big = this.clone();
            small = dateHour.clone();
        } else {
            big = dateHour.clone();
            small = this.clone();
        }

        // Test days to avoid counting an incomplete month
        int nbDayMax = DAYS_PER_MONTH[big.month - 1];

        if ((big.month == 2) && big.isLeap()) {
            // Leap years special case
            nbDayMax = 29;
        }

        if ((small.day > big.day) && (big.day < nbDayMax)) {
            // The month is incomplete, so we go to next one
            if (small.month < 12) {
                small.month++;
            } else {
                // End of year: got to next one
                small.month = 1;
                small.year++;
            }
        }

        // Test months to avoid counting an incomplete years
        if (small.month > big.month) {
            // Year is incomplete,
            // So we count the months until the end of the year
            // and we go to next one.
            nbMonths = 13 - small.month;

            small.month = 1;
            small.year++;
        }

        // Counting gap between months
        nbMonths += big.month - small.month;

        // Ending by the complete years count
        nbMonths += (big.year - small.year) * 12;

        return positive ? nbMonths : -nbMonths;
    }

    /**
     * Returns the number of days between this {@link DateHour} and another one
     * The result is:
     * <ul>
     * <li>0 if a == b</li>
     * <li>negative if a &lt; b</li>
     * <li>positive if a &gt; b</li>
     * </ul>
     * 
     * @param dateHour The {@link DateHour} to compare to.
     * @return The number of days between the 2 dates (sign).
     */
    public int daysBetween(DateHour dateHour) {
        // IMPORTANT:
        // Hours can't be divided 24 to obtain hours count because of time change (summer / winters)
        //
        // The used methods consists in changing time zone (without changing time)

        GregorianCalendar calend1 = new GregorianCalendar(this.year, this.month - 1, this.day, this.hour, this.minute,
                this.second);
        GregorianCalendar calend2 = new GregorianCalendar(dateHour.year, dateHour.month - 1, dateHour.day,
                dateHour.hour, dateHour.minute, dateHour.second);
        TimeZone zonePST = new SimpleTimeZone(0, "PST");

        calend1.setTimeZone(zonePST);
        calend2.setTimeZone(zonePST);

        long milliThis = calend1.getTime().getTime() + this.milli;
        long milliOther = calend2.getTime().getTime() + dateHour.milli;

        return (int) ((milliThis - milliOther) / 86400000L);
    }

    /**
     * Returns the number of hours between this {@link DateHour} and another one
     * The result is:
     * <ul>
     * <li>0 if a == b</li>
     * <li>negative if a &lt; b</li>
     * <li>positive if a &gt; b</li>
     * </ul>
     * 
     * @param dateHour The {@link DateHour} to compare to.
     * @return The number of hours between the 2 dates (sign).
     */
    public long hoursBetween(DateHour dateHour) {
        return minutesBetween(dateHour) / 60;
    }

    /**
     * Returns the number of minutes between this {@link DateHour} and another one
     * The result is:
     * <ul>
     * <li>0 if a == b</li>
     * <li>negative if a &lt; b</li>
     * <li>positive if a &gt; b</li>
     * </ul>
     * 
     * @param dateHour The {@link DateHour} to compare to.
     * @return The number of minutes between the 2 dates (sign).
     */
    public long minutesBetween(DateHour dateHour) {
        return secondsBetween(dateHour) / 60;
    }

    /**
     * Returns the number of seconds between this {@link DateHour} and another one
     * The result is:
     * <ul>
     * <li>0 if a == b</li>
     * <li>negative if a &lt; b</li>
     * <li>positive if a &gt; b</li>
     * </ul>
     * 
     * @param dateHour The {@link DateHour} to compare to.
     * @return The number of seconds between the 2 dates (sign).
     */
    public long secondsBetween(DateHour dateHour) {
        return millisecondsBetween(dateHour) / 1000;
    }

    /**
     * Returns the number of milliseconds between this {@link DateHour} and another one
     * The result is:
     * <ul>
     * <li>0 if a == b</li>
     * <li>negative if a &lt; b</li>
     * <li>positive if a &gt; b</li>
     * </ul>
     * 
     * @param dateHour The {@link DateHour} to compare to.
     * @return The number of milliseconds between the 2 dates (sign).
     */
    public long millisecondsBetween(DateHour dateHour) {
        long milliThis = toTimeInMilliSeconds();
        long milliAutre = toTimeInMilliSeconds();

        return milliThis - milliAutre;
    }

    /**
     * Returns the year.
     * 
     * @return The year.
     */
    public int getYear() {
        return year;
    }

    /**
     * Returns the month.
     * 
     * @return The month.
     */
    public int getMonth() {
        return month;
    }

    /**
     * Returns the day.
     * 
     * @return The day.
     */
    public int getDay() {
        return day;
    }

    /**
     * Returns the hour.
     * 
     * @return The hour.
     */
    public int getHour() {
        return hour;
    }

    /**
     * Returns the minutes.
     * 
     * @return The minutes.
     */
    public int getMinute() {
        return minute;
    }

    /**
     * Returns the seconds.
     * 
     * @return The seconds.
     */
    public int getSecond() {
        return second;
    }

    /**
     * Returns the milliseconds.
     * 
     * @return The milliseconds.
     */
    public int getMillisecond() {
        return milli;
    }

    /**
     * Adds <code>year</code> years.
     * 
     * @param year The number of years to add.
     */
    public void addYear(int year) {
        this.year += year;
        corrigeDateHour(this);
    }

    /**
     * Adds <code>month</code> months.
     * 
     * @param month The number of months to add.
     */
    public void addMonth(int month) {
        this.month += month;
        corrigeDateHour(this);
    }

    /**
     * Adds <code>day</code> days.
     * 
     * @param day The number of days to add.
     */
    public void addDay(int day) {
        this.day += day;
        corrigeDateHour(this);
    }

    /**
     * Adds <code>hour</code> hours.
     * 
     * @param hour The number of hours to add.
     */
    public void addHour(int hour) {
        this.hour += hour;
        corrigeDateHour(this);
    }

    /**
     * Adds <code>minute</code> minutes.
     * 
     * @param minute The number of minutes to add.
     */
    public void addMinute(int minute) {
        this.minute += minute;
        corrigeDateHour(this);
    }

    /**
     * Adds <code>second</code> seconds.
     * 
     * @param second The number of seconds to add.
     */
    public void addSecond(int second) {
        this.second += second;
        corrigeDateHour(this);
    }

    /**
     * Adds <code>millisecond</code> milliseconds.
     * 
     * @param millisecond The number of milliseconds to add.
     */
    public void addMillisecond(int millisecond) {
        this.milli += millisecond;
        corrigeDateHour(this);
    }

    // ---------- Comparable interface methods ----------
    /**
     * Compares 2 {@link DateHour} objects. The returned result is:
     * <ul>
     * <li>0 if a == b</li>
     * <li>-1 if a &lt; b</li>
     * <li>+1 if a &gt; b</li>
     * </ul>
     * 
     * @param dateHour {@link DateHour} to compare.
     * @return The {@link DateHour}'s order.
     */
    @Override
    public int compareTo(DateHour dateHour) {
        int result;
        if (dateHour == null) {
            result = 1;
        } else {
            long ecart = millisecondsBetween(dateHour);
            if (ecart < 0) {
                result = -1;
            } else if (ecart > 0) {
                result = 1;
            } else {
                result = 0;
            }
        }
        return result;
    }

    // ---------- Object class methods ----------

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (getClass().equals(obj.getClass())) {
            DateHour dateHour = (DateHour) obj;
            equals = (millisecondsBetween(dateHour) == 0);
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xDA7E;
        int mult = 0x36005;
        code = code * mult + getClass().hashCode();
        code = code * mult + (int) toTimeInMilliSeconds();
        return code;
    }

    @Override
    public DateHour clone() {
        try {
            return (DateHour) super.clone();
        } catch (CloneNotSupportedException e) {
            // Can not happen as DateHour implements Cloneable.
            return null;
        }
    }
}
