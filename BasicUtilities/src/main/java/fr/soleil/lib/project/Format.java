/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project;

import java.util.IllegalFormatException;
import java.util.Locale;
import java.util.UnknownFormatConversionException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A class used to format number values
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class Format {
    private static final int DEFAULT_WIDTH = 3;

    /**
     * Returns whether a format coding character is found in a format {@link String} pattern, and whether the String
     * between the <code>'%'</code> and this character represents a number format.
     * 
     * @param format The format. Can not be <code>null</code>.
     * @param percentIndex The index of the <code>'%'</code> in <code>format</code>. Must be in <code>format</code>
     *            range.
     * @param toCheck The character to search for.
     * @return A <code>boolean</code>.
     */
    protected static boolean isFormatFound(String format, int percentIndex, char toCheck) {
        boolean isOk;
        int index = format.indexOf(toCheck, percentIndex);
        if (index > -1) {
            if (index == percentIndex + 1) {
                isOk = true;
            } else {
                try {
                    Double.parseDouble(format.substring(percentIndex + 1, index));
                    isOk = true;
                } catch (Exception e) {
                    isOk = false;
                }
            }
        } else {
            isOk = false;
        }
        return isOk;
    }

    /**
     * Does a very basic check on a format pattern to know whether it is correctly written.
     * 
     * @param format The format to test.
     * @param allowStringFormat Whether to allow string format (like <code>"%s"</code>)
     * @return A <code>boolean</code>.
     */
    protected static boolean isNumberFormatOKNoNumberTest(String format, boolean allowStringFormat) {
        boolean isOk;
        if (format == null) {
            isOk = false;
        } else {
            int index = format.indexOf('%');
            if ((index > -1) && (index < format.length() - 1)) {
                if (format.indexOf('s', index) == index + 1) {
                    isOk = allowStringFormat;
                } else {
                    isOk = isFormatFound(format, index, 'd') || isFormatFound(format, index, 'D')
                            || isFormatFound(format, index, 'f') || isFormatFound(format, index, 'F')
                            || isFormatFound(format, index, 'e') || isFormatFound(format, index, 'E')
                            || isFormatFound(format, index, 'x') || isFormatFound(format, index, 'X');
                }
            } else {
                isOk = false;
            }
        }
        return isOk;
    }

    /**
     * Checks whether a format pattern is correctly written.
     * 
     * @param format The format to test.
     * @param allowStringFormat Whether to allow string format (like <code>"%s"</code>)
     * @return <code>true</code> if it's correctly written.
     */
    public static boolean isNumberFormatOK(String format, boolean allowStringFormat) {
        boolean isOk;
        if (isNumberFormatOKNoNumberTest(format, false)) {
            isOk = true;
            int testValue = 1;
            try {
                // Test with an integer
                if (String.format(Locale.US, format, Integer.valueOf(testValue)).trim().isEmpty()) {
                    isOk = false;
                }
            } catch (IllegalFormatException e) {
                try {
                    // Test with a double
                    if (String.format(Locale.US, format, Double.valueOf(testValue)).trim().isEmpty()) {
                        isOk = false;
                    }
                } catch (Exception exc) {
                    isOk = false;
                }
            } catch (Exception e) {
                // here it depends on the real value
                Logger.getLogger(Format.class.getName()).log(Level.WARNING, "Unexpected exception to check format", e);
                isOk = false;
            }
        } else {
            isOk = false;
        }
        return isOk;
    }

    /**
     * Check if the pattern format is correctly written.
     * 
     * @param format The format to test.
     * @return <code>true</code> if it's correctly written.
     */
    public static boolean isNumberFormatOK(String format) {
        return isNumberFormatOK(format, false);
    }

    /**
     * Returns whether a character represents a digit or a point.
     * 
     * @param c The character to check.
     * @return A <code>boolean</code>.
     */
    protected static boolean isCoding(char c) {
        return Character.isDigit(c) || (c == '.') || (c == '+') || (c == '-');
    }

    /**
     * Returns the index of the character coding the format type in a format, starting the search at given index.
     * 
     * @param format The format.
     * @param start The index at which to start the search.
     * @return An <code>int</code>. <code>-1</code> when format type is not found.
     */
    protected static int getFormatTypeIndex(String format, int start) {
        int index = start;
        while ((index < format.length()) && isCoding(format.charAt(index))) {
            index++;
        }
        if (index >= format.length()) {
            index = -1;
        }
        return index;
    }

    /**
     * Method which returns the width part of a format. If the format is not correct, <code>0</code> will be returned.
     * 
     * @param format The format.
     * @return width of the given format.
     */
    public static int getWidth(String format) {
        int result = 0;
        if (Format.isNumberFormatOK(format, false)) {
            String formatToUse = format;
            int index = formatToUse.indexOf('%');
            if (index != 0) {
                formatToUse = formatToUse.substring(index);
            }
            // if there is an argument index we remove it
            if (formatToUse.contains("$")) {
                int begin = format.lastIndexOf('$') + 1;
                int end = format.length();
                formatToUse = format.substring(begin, end);
            } else {
                formatToUse = format.replace("%", ObjectUtils.EMPTY_STRING);
            }
            index = getFormatTypeIndex(formatToUse, 0);
            if (index > -1) {
                formatToUse = formatToUse.substring(0, index);
            }
            // we remove the precision part if there is one
            int end = format.indexOf('.');
            if (end > -1) {
                formatToUse = format.substring(0, end);
            }
            // we remove flags => flags are not digit
            // we replace character by ""
            formatToUse = formatToUse.replaceAll("[^0-9]", ObjectUtils.EMPTY_STRING);
            if (formatToUse.isEmpty()) {
                result = DEFAULT_WIDTH;
            } else {
                result = Integer.parseInt(formatToUse);
            }
        }
        return result;
    }

    /**
     * Returns whether given format has a float part.
     * 
     * @param format The format to test.
     * @return <code>true</code> if the given format has a float part.
     */
    public static boolean hasFloatPart(String format) {
        return format == null ? false : format.contains(".");
    }

    /**
     * Generates default {@link IllegalFormatException} for a bad number format
     * 
     * @param format The concerned format
     * @return An {@link IllegalFormatException}
     */
    protected static IllegalFormatException generateBadFormatException(String format) {
        return new BadNumberFormatException("'" + format + "' is not a valid number format");
    }

    /**
     * Returns a formatted string representation of a {@link Number} array, using the specified {@link Locale} and
     * format {@link String}. Mainly used to avoid some classic bugs with default Java methods. For example, you won't
     * get any exception with "%d" format for a floating value, whereas the
     * {@link String#format(Locale, String, Object...)} throws an {@link IllegalFormatException}.
     * 
     * @param l The {@link Locale} to use.
     * @param format The format {@link String}.
     * @param toFormat The {@link Number} array to format.
     * @return A formatted string representation of the specified {@link Number}, using the specified {@link Locale}
     *         and format {@link String}.
     *         Returns <code>null</code> if the {@link Number} <code>toFormat</code> is <code>null</code>.
     * @throws IllegalFormatException If a problem occurred during {@link String} formating.
     * @see String#format(Locale, String, Object...)
     */
    public static String format(Locale l, String format, Number... toFormat) throws IllegalFormatException {
        String result;
        if (toFormat == null) {
            result = null;
        } else if (isNumberFormatOKNoNumberTest(format, true)) {
            result = String.format(l, format, getAdaptedArray(format, toFormat));
        } else if ((format == null) || format.trim().isEmpty()) {
            if (toFormat.length == 1) {
                result = String.valueOf(toFormat[0]);
            } else {
                result = ObjectUtils.EMPTY_STRING;
            }
        } else {
            throw generateBadFormatException(format);
        }
        if (result != null) {
            result = result.trim();
        }
        return result;
    }

    /**
     * Calls {@link #format(Locale, String, Number...)} with {@link Locale#US}.
     * 
     * @param format The format {@link String}.
     * @param toFormat The {@link Number} to format.
     * @return A formatted string representation of the specified {@link Number}, using {@link Locale#US} and the
     *         specified format {@link String}.
     *         Returns <code>null</code> if the {@link Number} <code>toFormat</code> is <code>null</code>.
     * @throws IllegalFormatException If a problem occurred during {@link String} formating.
     * @see {@link #format(Locale, String, Number)}
     */
    public static String format(String format, Number toFormat) throws IllegalFormatException {
        return format(Locale.US, format, toFormat);
    }

    /**
     * Calls {@link #format(Locale, String, Number...)} with default {@link Locale}.
     * 
     * @param format The format {@link String}.
     * @param toFormat The {@link Number} array to format.
     * @return A formatted string representation of the specified {@link Number}, using {@link Locale#US} and the
     *         specified format {@link String}.
     *         Returns <code>null</code> if the {@link Number} <code>toFormat</code> is <code>null</code>.
     * @throws IllegalFormatException If a problem occurred during {@link String} formating.
     * @see {@link #format(Locale, String, Number)}
     */
    public static String format(String format, Number... toFormat) throws IllegalFormatException {
        return format(Locale.US, format, toFormat);
    }

    /**
     * Returns a formatted string representation of a <code>double</code>, using the specified format.
     * 
     * @param value The value to format.
     * @param format The format to use.
     * @param throwException Whether to throw an exception in case of format error
     * @param noFormatChecking Whether to avoid checking format validity before trying to format
     * @throws IllegalFormatException If there is a format error
     * @return A {@link String}.
     */
    protected static String formatValue(double value, String format, boolean throwException, boolean noFormatChecking)
            throws IllegalFormatException {
        String result;
        if (noFormatChecking || isNumberFormatOKNoNumberTest(format, true)) {
            try {
                result = String.format(Locale.US, format, value);
            } catch (IllegalFormatException ife) {
                try {
                    result = String.format(Locale.US, format, Long.valueOf((long) value));
                } catch (Exception e) {
                    if (throwException) {
                        throw ife;
                    } else {
                        result = Double.toString(value);
                    }
                }
            } catch (Exception e) {
                if (throwException) {
                    if (e instanceof IllegalFormatException) {
                        throw (IllegalFormatException) e;
                    } else {
                        throw new BadNumberFormatException(
                                "Failed to format " + value + " with format '" + format + "'", e);
                    }
                } else {
                    result = Double.toString(value);
                }
            }
        } else {
            if (throwException) {
                if ((format == null) || format.trim().isEmpty()) {
                    result = Double.toString(value);
                } else {
                    throw generateBadFormatException(format);
                }
            } else {
                result = Double.toString(value);
            }
        }
        return result;
    }

    /**
     * Returns a formatted string representation of a {@link Number}, using the specified format.
     * 
     * @param value The value to format.
     * @param format The format to use.
     * @return A {@link String}.
     * @deprecated Use {@link #format(String, Number)} instead.
     */
    @Deprecated
    public static String formatValue(Number value, String format) {
        return format(format, value);
    }

    /**
     * Returns a formatted string representation of a <code>double</code>, using the specified format.
     * 
     * @param value The value to format.
     * @param format The format to use.
     * @param noFormatChecking Whether to avoid checking format validity before trying to format
     * @return A {@link String}.
     */
    public static String formatValue(double value, String format, boolean noFormatChecking) {
        String result;
        try {
            result = formatValue(value, format, false, noFormatChecking);
        } catch (Exception e) {
            // Should not happen, as we ask for no exception
            result = Double.toString(value);
        }
        return result;
    }

    /**
     * Returns a formatted string representation of a <code>double</code>, using the specified format.
     * 
     * @param value The value to format.
     * @param format The format to use.
     * @return A {@link String}.
     */
    public static String formatValue(double value, String format) {
        return formatValue(value, format, false);
    }

    /**
     * Returns a formatted string representation of a <code>double</code>, using the specified format.
     * 
     * @param value The value to format.
     * @param format The format to use.
     * @param noFormatChecking Whether to avoid checking format validity before trying to format
     * @throws IllegalFormatException If value could not be correctly formatted
     * @return A {@link String}.
     */
    public static String formatValueWithException(double value, String format, boolean noFormatChecking)
            throws IllegalFormatException {
        return formatValue(value, format, true, noFormatChecking);
    }

    /**
     * Returns a formatted string representation of a <code>double</code>, using the specified format.
     * 
     * @param value The value to format.
     * @param format The format to use.
     * @throws IllegalFormatException If value could not be correctly formatted
     * @return A {@link String}.
     */
    public static String formatValueWithException(double value, String format) throws IllegalFormatException {
        return formatValueWithException(value, format, false);
    }

    /**
     * This methods transforms a classic format to a text decimal pattern.<br />
     * Example: <code>"%5.2f"</code> is transformed as pattern <code>"000.00"</code>.
     * 
     * @param format The format to transform.
     * @return The corresponding text decimal pattern, or <code>null</code> if a problem occurred during format
     *         conversion.
     */
    public static String classicFormatToTextDecimalPattern(String format) {
        String result = null;
        if (format != null) {
            try {
                String temp = format.toLowerCase().replace("%", ObjectUtils.EMPTY_STRING)
                        .replace("e", ObjectUtils.EMPTY_STRING).replace("f", ObjectUtils.EMPTY_STRING)
                        .replace("d", ObjectUtils.EMPTY_STRING).trim();
                int totalPart = 0;
                int integerPart = 0;
                int decimalPart = 0;
                int index = temp.indexOf(".");
                boolean formatOk = true;
                if (index < 0) {
                    totalPart = Integer.parseInt(temp);
                } else {
                    decimalPart = Integer.parseInt(temp.substring(index + 1));
                    if (index == 0) {
                        totalPart = decimalPart;
                    } else {
                        totalPart = Integer.parseInt(temp.substring(0, index));
                    }
                    if (decimalPart < 0 || totalPart < 0) {
                        formatOk = false;
                    }
                }
                if (formatOk) {
                    integerPart = totalPart - decimalPart;
                    if (integerPart < 1) {
                        integerPart = 1;
                    }
                    StringBuilder buffer = new StringBuilder();
                    while (integerPart > 0) {
                        buffer.append("0");
                        integerPart--;
                    }
                    if (decimalPart > 0) {
                        buffer.append(".");
                        while (decimalPart > 0) {
                            buffer.append("0");
                            decimalPart--;
                        }
                    }
                    if (buffer.length() > 0) {
                        result = buffer.toString();
                    }
                }
            } catch (Exception e) {
                // a problem occurred during conversion: the string is not well formatted
                // --> return null
                result = null;
            }
        }
        return result;
    }

    /**
     * Returns whether a {@link Number} value represents a {@link Float} or a {@link Double}.
     * 
     * @param value The value to check.
     * @return A <code>boolean</code>.
     */
    protected static boolean isFloat(Number value) {
        return (value instanceof Double) || (value instanceof Float);
    }

    /**
     * Returns a {@link Number} best adapted to given format, given a desired {@link Number} value.
     * 
     * @param formatType The character representing the format type (examples: <code>'e'</code>, <code>'f'</code>,
     *            <code>'x'</code>, <code>'s'</code>).
     * @param value The {@link Number} to adapt.
     * @return A {@link Number}.
     */
    protected static Number getAdaptedNumber(char formatType, Number value) {
        Number result = value;
        if (value != null) {
            // The following conversion allows to use float formats (examples "%6.2f", "%1.5e") with Integers and Longs,
            // and integer formats (example: "%5d", "%x") with Floats and Doubles
            switch (formatType) {
                case 'd':
                case 'D':
                case 'x':
                case 'X':
                    if (isFloat(value)) {
                        result = Long.valueOf(value.longValue());
                    }
                    break;
                case 'f':
                case 'F':
                case 'e':
                case 'E':
                    if (!isFloat(value)) {
                        result = Double.valueOf(value.doubleValue());
                    }
                    break;
            }
        }
        return result;
    }

    /**
     * Returns an adaptation of a {@link Number} array to given format, in order to avoid undesired
     * {@link IllegalFormatException}s.
     * 
     * @param format The format.
     * @param toFormat The array to adapt.
     * @return An {@link Object} array, containing the adapted {@link Number}s.
     */
    protected static Object[] getAdaptedArray(String format, Number... toFormat) {
        Number[] result = toFormat;
        if ((format != null) && (!format.isEmpty()) && (toFormat != null) && (toFormat.length > 0)) {
            boolean cloned = false;
            int numberIndex = 0, formatIndex = 0;
            while ((numberIndex < toFormat.length) && (formatIndex > -1)) {
                formatIndex = format.indexOf('%', formatIndex);
                if (formatIndex > -1) {
                    formatIndex = getFormatTypeIndex(format, formatIndex + 1);
                    if (formatIndex > -1) {
                        Number value = result[numberIndex];
                        Number adapted = getAdaptedNumber(format.charAt(formatIndex), value);
                        if (adapted != value) {
                            if (!cloned) {
                                cloned = true;
                                result = result.clone();
                            }
                            result[numberIndex] = adapted;
                        }
                        numberIndex++;
                    }
                }
            }
        }
        return result;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static class BadNumberFormatException extends UnknownFormatConversionException {

        private static final long serialVersionUID = 792381147058731349L;

        private String message;
        private Throwable cause;

        public BadNumberFormatException(String message) {
            this(message, null);
        }

        public BadNumberFormatException(String message, Throwable cause) {
            super(ObjectUtils.EMPTY_STRING);
            this.message = message;
            this.cause = cause;
        }

        @Override
        public synchronized Throwable getCause() {
            return (cause == this ? null : cause);
        }

        @Override
        public String getMessage() {
            return message;
        }
    }
}
