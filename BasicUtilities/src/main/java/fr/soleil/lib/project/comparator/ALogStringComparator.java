/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.comparator;

import java.text.Collator;

import fr.soleil.lib.project.log.LogData;

/**
 * An {@link ALogComparator} that compares some {@link String}s extracted from {@link LogData} objects.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class ALogStringComparator extends ALogComparator<String> {

    protected final Collator collator;

    public ALogStringComparator(boolean descending) {
        super(descending);
        collator = Collator.getInstance();
    }

    @Override
    protected int compareData(String s1, String s2) {
        return collator.compare(s1, s2);
    }

}
