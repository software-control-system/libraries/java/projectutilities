/*
 *  This file is part of BasicUtilities.

BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.comparator;

import fr.soleil.lib.project.log.LogData;

/**
 * An {@link ILogComparator} that compares some data extracted from {@link LogData} objects and has the possibility to
 * revert comparison (in order to choose between ascending and
 * descending order).
 * 
 * @author Rapha&euml;l GIRARDOT
 *
 * @param <T> The type of data extracted by this {@link ALogComparator}.
 */
public abstract class ALogComparator<T> implements ILogComparator {

    protected final boolean descending;

    /**
     * Constructs a new {@link ALogComparator}.
     * 
     * @param descending Whether to use descending order (i.e. revert comparison).
     */
    public ALogComparator(boolean descending) {
        super();
        this.descending = descending;
    }

    protected abstract T extractData(LogData log);

    protected abstract int compareData(T d1, T d2);

    @Override
    public final int compare(LogData o1, LogData o2) {
        int comp;
        if (o1 == null) {
            if (o2 == null) {
                comp = 0;
            } else {
                comp = -1;
            }
        } else if (o2 == null) {
            comp = 1;
        } else {
            T d1 = extractData(o1), d2 = extractData(o2);
            if (d1 == null) {
                if (d2 == null) {
                    comp = 0;
                } else {
                    comp = -1;
                }
            } else if (d2 == null) {
                comp = 1;
            } else {
                comp = compareData(d1, d2);
                if (descending && (comp != 0)) {
                    comp *= -1;
                }
            }
        }
        return comp;
    }
}
