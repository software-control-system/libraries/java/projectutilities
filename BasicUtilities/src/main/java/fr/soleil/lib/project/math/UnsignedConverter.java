/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.math;

import java.math.BigInteger;

/**
 * Class used for unsigned numbers conversion
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class UnsignedConverter {

    private static boolean unsignedLongSupported;
    static {
        try {
            unsignedLongSupported = Boolean.parseBoolean(System.getProperty("UnsignedLongSupport", "false"));
        } catch (Exception e) {
            unsignedLongSupported = false;
        }
    }

    /**
     * Returns whether unsigned <code>long</code> or unsigned {@link Long} values are supported.
     * 
     * @return A <code>boolean</code>.
     *         <code>false</code> by default.
     */
    public static boolean isUnsignedLongSupported() {
        return unsignedLongSupported;
    }

    /**
     * Sets whether unsigned <code>long</code> or unsigned {@link Long} values should be supported.
     * 
     * @param unsignedLongSupported Whether unsigned <code>long</code> or unsigned {@link Long} values should be
     *            supported.
     */
    public static void setUnsignedLongSupported(boolean unsignedLongSupported) {
        UnsignedConverter.unsignedLongSupported = unsignedLongSupported;
    }

    /**
     * Converts any unsigned values (arrays allowed).
     * 
     * @param unsigned The value to convert.
     * @param allowUnsignedLong Whether unsigned <code>long</code> or unsigned {@link Long} values should be supported.
     * @return The converted value.
     */
    public static Object convertUnsigned(Object unsigned, boolean allowUnsignedLong) {
        Object result = unsigned;
        if (result != null) {
            if (result.getClass().isArray()) {
                result = ArrayUtils.convertUnsignedArray(result, allowUnsignedLong);
            } else if (result instanceof Byte) {
                result = convertUnsignedByte((Byte) result);
            } else if (result instanceof Short) {
                result = convertUnsignedShort((Short) result);
            } else if (result instanceof Integer) {
                result = convertUnsignedInt((Integer) result);
            } else if (allowUnsignedLong && (result instanceof Long)) {
                result = convertUnsignedLong((Long) result);
            }
        }
        return result;
    }

    /**
     * Converts any unsigned values (arrays allowed).
     * If you want the support of unsigned {@link Long} values, you should ensure
     * {@link #setUnsignedLongSupported(boolean)}.
     * 
     * @param unsigned The value to convert.
     * @return The converted value.
     * @see #isUnsignedLongSupported()
     * @see #setUnsignedLongSupported(boolean)
     */
    public static Object convertUnsigned(Object unsigned) {
        return convertUnsigned(unsigned, isUnsignedLongSupported());
    }

    /**
     * Does the revert operation of {@link #convertUnsigned(Object)}.
     * 
     * @param signed The value to convert.
     * @return The converted value.
     */
    public static Object undoUnsignedConversion(Object signed) {
        Object result = signed;
        if (result != null) {
            if (result.getClass().isArray()) {
                result = ArrayUtils.undoUnsignedArrayConversion(result);
            } else if (result instanceof Short) {
                result = Byte.valueOf(((Short) result).byteValue());
            } else if (result instanceof Integer) {
                result = Short.valueOf(((Integer) result).shortValue());
            } else if (result instanceof Long) {
                result = Integer.valueOf(((Long) result).intValue());
            } else if (result instanceof BigInteger) {
                result = Long.valueOf(((BigInteger) result).longValue());
            }
        }
        return result;
    }

    /**
     * Converts a byte to its unsigned value, represented as as short.
     * 
     * @param unsigned The byte to convert.
     * @return a short.
     */
    public static short convertUnsignedByte(byte unsigned) {
        return (short) (unsigned & (short) 0xFF);
    }

    /**
     * Converts a short to its unsigned value, represented as as int.
     * 
     * @param unsigned The short to convert.
     * @return an int.
     */
    public static int convertUnsignedShort(short unsigned) {
        return unsigned & 0xFFFF;
    }

    /**
     * Converts an int to its unsigned value, represented as as long.
     * 
     * @param unsigned The int to convert.
     * @return a long.
     */
    public static long convertUnsignedInt(int unsigned) {
        return unsigned & 0xFFFFFFFFL;
    }

    /**
     * Converts a long to its unsigned value, represented as as BigInteger.
     * 
     * @param unsigned The long to convert.
     * @return a BigInteger.
     */
    public static BigInteger convertUnsignedLong(long unsigned) {
        if (unsigned >= 0L) {
            return BigInteger.valueOf(unsigned);
        } else {
            int upper = (int) (unsigned >>> 32);
            int lower = (int) unsigned;
            return (BigInteger.valueOf(convertUnsignedInt(upper))).shiftLeft(32)
                    .add(BigInteger.valueOf(convertUnsignedInt(lower)));
        }
    }

}
