/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.comparator;

import java.text.Collator;
import java.util.Comparator;

import fr.soleil.lib.project.IndexedName;

/**
 * A {@link Comparator} used to compare {@link IndexedName}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class IndexedNameComparator implements Comparator<IndexedName> {

    private static final IndexedNameComparator INSTANCE = new IndexedNameComparator();

    public static IndexedNameComparator getInstance() {
        return INSTANCE;
    }

    public IndexedNameComparator() {
        super();
    }

    @Override
    public int compare(IndexedName o1, IndexedName o2) {
        int comp;
        if (o1 == null) {
            if (o2 == null) {
                comp = 0;
            } else {
                comp = -1;
            }
        } else if (o2 == null) {
            comp = 1;
        } else {
            comp = Collator.getInstance().compare(o1.getName(), o2.getName());
            if ((comp == 0) && o1.isUseIndex() && o2.isUseIndex()) {
                comp = Integer.compare(o1.getIndex(), o2.getIndex());
            }
        }
        return comp;
    }

}
