/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.data;

import fr.soleil.lib.project.math.ArrayUtils;

/**
 * A class that stores some data flat representation
 * 
 * @param <T> The type of data stored in this class
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class AFlatData<T> {
    private int[] dataShape;
    private final T value;

    public AFlatData(int[] dataShape, T value) {
        this.dataShape = dataShape;
        this.value = value;
    }

    /**
     * Returns the orginal data shape
     * 
     * @return An <code>int[]</code>
     */
    public int[] getRealShape() {
        return dataShape;
    }

    /**
     * Returns the data flat value
     * 
     * @return A single dimensional array (example: A <code>double[]</code>)
     */
    public T getValue() {
        return value;
    }

    /**
     * Transforms this {@link FlatData}'s shape into a compatible one.
     * <p>
     * Compatible means the associated flat value can be transformed into such a shape
     * </p>
     * 
     * @param newShape The new shape to set. Will take effect only if new shape is compatible
     */
    public synchronized void updateShape(int[] newShape) {
        boolean canSet = false;
        if ((dataShape != null) && (newShape != null) && (newShape.length > 0)
                && (!ArrayUtils.equals(newShape, dataShape))) {
            int mult1 = 1, mult2 = 1;
            for (int dim : dataShape) {
                mult1 *= dim;
            }
            for (int dim : newShape) {
                mult2 *= dim;
            }
            canSet = (mult1 == mult2);
        }
        if (canSet) {
            dataShape = newShape;
        }
    }

}
