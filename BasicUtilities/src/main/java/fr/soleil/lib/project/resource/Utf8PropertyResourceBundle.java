/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.resource;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Locale;
import java.util.PropertyResourceBundle;

/**
 * A {@link PropertyResourceBundle} specialized in UTF-8 encoded resources
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class Utf8PropertyResourceBundle extends PropertyResourceBundle {

    private static final String UTF8 = "UTF8";
    private static final String PROPERTIES = "properties";

    public Utf8PropertyResourceBundle(Reader reader) throws IOException {
        super(reader);
    }

    public Utf8PropertyResourceBundle(InputStream stream) throws IOException {
        this(new InputStreamReader(stream, UTF8));
    }

    public Utf8PropertyResourceBundle(String baseName, Locale locale, ClassLoader classLoader) throws IOException {
        this(toReader(baseName, locale, classLoader));
    }

    public Utf8PropertyResourceBundle(String baseName, Locale locale) throws IOException {
        this(baseName, locale, null);
    }

    public static String toBundleName(String baseName, Locale locale) {
        String bundleName;
        if ((locale == null) || (locale == Locale.ROOT)) {
            bundleName = baseName;
        } else {
            String language = locale.getLanguage();
            String country = locale.getCountry();
            String variant = locale.getVariant();

            if (language.isEmpty() && country.isEmpty() && variant.isEmpty()) {
                bundleName = baseName;
            } else {
                StringBuilder sb = new StringBuilder(baseName);
                sb.append('_');
                if (!variant.isEmpty()) {
                    sb.append(language).append('_').append(country).append('_').append(variant);
                } else if (!country.isEmpty()) {
                    sb.append(language).append('_').append(country);
                } else {
                    sb.append(language);
                }
                bundleName = sb.toString();
            }

        }
        return bundleName;
    }

    public static Reader toReader(String baseName, Locale locale) {
        return toReader(baseName, locale, null);
    }

    public static Reader toReader(String baseName, Locale locale, ClassLoader classLoader) {
        Reader result = null;
        ClassLoader loader = classLoader;
        if (loader == null) {
            loader = ClassLoader.getSystemClassLoader();
        }
        String resourceName = toResourceName(toBundleName(baseName, locale), PROPERTIES, false);
        InputStream is = loader.getResourceAsStream(resourceName);
        if (is == null) {
            // System.err.println("class loader failed to find resource " + resourceName);
            resourceName = toResourceName(baseName, PROPERTIES, false);
            is = loader.getResourceAsStream(resourceName);
            if (is == null) {
                // System.err.println("class loader failed to find resource " + resourceName);
                resourceName = toResourceName(toBundleName(baseName, locale), PROPERTIES, true);
                is = loader.getResourceAsStream(resourceName);
                if (is == null) {
                    // System.err.println("class loader failed to find resource " + resourceName);
                    resourceName = toResourceName(baseName, PROPERTIES, true);
                    is = loader.getResourceAsStream(resourceName);
                    // if (is == null) {
                    // System.err.println("class loader failed to find resource " + resourceName);
                    // }
                }
            }
        }
        if (is != null) {
            // System.out.println("class loader successfully found resource " + resourceName);
            try {
                result = new InputStreamReader(is, UTF8);
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            }
        }
        return result;
    }

    public static final String toResourceName(String bundleName, String suffix) {
        return toResourceName(bundleName, suffix, false);
    }

    public static final String toResourceName(String bundleName, String suffix, boolean startWithSlash) {
        StringBuilder sb = new StringBuilder(bundleName.length() + 1 + suffix.length());
        sb.append(bundleName.replace('.', '/')).append('.').append(suffix);
        if ((startWithSlash) && (sb.charAt(0) != '/')) {
            sb.insert(0, '/');
        }
        return sb.toString();
    }

}
