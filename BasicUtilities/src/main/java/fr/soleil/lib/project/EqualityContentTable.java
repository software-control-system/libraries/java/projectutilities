/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project;

import java.util.Hashtable;
import java.util.Map;

/**
 * An {@link Hashtable} that can compare for equality, ignoring elements order
 * 
 * @param <K>
 *            The key type
 * @param <V>
 *            The value type
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class EqualityContentTable<K, V> extends Hashtable<K, V> {

    private static final long serialVersionUID = -4880594274639911237L;

    public EqualityContentTable() {
        super();
    }

    public EqualityContentTable(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public EqualityContentTable(int initialCapacity) {
        super(initialCapacity);
    }

    public EqualityContentTable(Map<? extends K, ? extends V> t) {
        super(t);
    }

    // equals redefined, but this algorithm is compatible with existing hashCode
    @Override
    public synchronized boolean equals(Object obj) {
        boolean equals;
        if (obj == this) {
            equals = true;
        } else if (obj == null) {
            equals = false;
        } else if (getClass().equals(obj.getClass())) {
            EqualityContentTable<?, ?> table = (EqualityContentTable<?, ?>) obj;
            equals = ObjectUtils.sameMapContent(this, table);
        } else {
            equals = false;
        }
        return equals;
    }
}
