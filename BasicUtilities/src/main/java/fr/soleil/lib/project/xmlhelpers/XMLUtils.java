/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.xmlhelpers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;

/**
 * A class used to interact with XML {@link File}s.
 * 
 * @author CLAISSE, Rapha&euml;l GIRARDOT
 */
public class XMLUtils {

    public static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
    public static final String CRLF = System.getProperty("line.separator");

    /**
     * Returns the root node of an XML {@link File}.
     * 
     * @param file The XML {@link File}.
     * @return a {@link Node}.
     * @throws XMLWarning If a problem occurred.
     */
    public static Node getRootNode(File file) throws XMLWarning {
        try {
            return getNewDocumentBuilder().parse(file).getDocumentElement();
        } catch (Exception e) {
            throw exceptionToXMLWarning(e);
        }
    }

    /**
     * Returns the root node of an XML {@link File} content.
     * 
     * @param content The XML {@link File} content.
     * @return a {@link Node}.
     * @throws XMLWarning If a problem occurred.
     */
    public static Node getRootNodeFromFileContent(String content) throws XMLWarning {
        try {
            return getNewDocumentBuilder().parse(new InputSource(new StringReader(content))).getDocumentElement();
        } catch (Exception e) {
            throw exceptionToXMLWarning(e);
        }
    }

    // FactoryConfigurationError is thrown by
    // DocumentBuilderFactory.newInstance()
    private static DocumentBuilder getNewDocumentBuilder()
            throws FactoryConfigurationError, ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        // should we add dtd validating?
        factory.setValidating(false);
        factory.setIgnoringComments(true);

        return factory.newDocumentBuilder();
    }

    private static XMLWarning exceptionToXMLWarning(Exception e) {
        XMLWarning warning = new XMLWarning(e.getMessage());
        warning.setStackTrace(e.getStackTrace());
        return warning;
    }

    /**
     * Tests whether a node is a fake node
     * 
     * @param nodeToTest The node to test
     * @return a boolean
     */
    public static boolean isAFakeNode(Node nodeToTest) {
        return (nodeToTest.getNodeType() != Node.ELEMENT_NODE);
    }

    /**
     * Checks whether a {@link Node} has some children
     * 
     * @param nodeToTest The {@link Node} to test
     * @return A boolean value. <code>true</code> If the {@link Node} has some children. <code>TRUE</code> otherwise
     */
    public static boolean hasRealChildNodes(Node nodeToTest) {
        boolean hasChildren = false;
        NodeList potentialChilds = nodeToTest.getChildNodes();
        for (int i = 0; (i < potentialChilds.getLength()) && (!hasChildren); i++) {
            Node nextNode = potentialChilds.item(i);
            if (!isAFakeNode(nextNode)) {
                hasChildren = true;
            }
        }
        return hasChildren;
    }

    /**
     * Saves resources in an xml file.
     * 
     * @param string The {@link String} to write in the file.
     * @param favoritesResourceLocation The path to the XML file.
     * @param withHeader <code>true</code> if the xml header (equals to XML_HEADER property inside {@link XMLUtils})
     *            must be added at the beginning of the file.
     * @throws IOException If and I/O error occurred.
     */
    public static void save(String string, String favoritesResourceLocation, boolean withHeader) throws IOException {
        if ((string != null) && (!string.trim().isEmpty())) {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(favoritesResourceLocation));) {
                if (withHeader) {
                    write(bw, XMLUtils.XML_HEADER, true);
                }
                write(bw, string, false);
            }
        }
    }

    /**
     * Saves resources in an xml file. Automatically add the header to the beginning of the file.
     * 
     * @param string The {@link String} to write in the file
     * @param favoritesResourceLocation The path to the XML file
     * @throws IOException If and I/O error occurred
     */
    public static void save(String string, String favoritesResourceLocation) throws IOException {
        save(string, favoritesResourceLocation, true);
    }

    /**
     * Adds {@link Node}'s attributes in {@link HashMap}
     * 
     * @param domNode The {@link Node} from which to get the attributes
     * @return An {@link HashMap}
     * @throws DOMException If a problem occurred during node data extraction
     * @see Node#getNodeValue()
     */
    public static Map<String, String> loadAttributes(Node domNode) throws DOMException {
        Map<String, String> retour = new ConcurrentHashMap<String, String>();
        if ((domNode != null) && domNode.hasAttributes()) {
            NamedNodeMap listAtts = domNode.getAttributes();
            for (int i = 0; i < listAtts.getLength(); i++) {
                String nomAtt = listAtts.item(i).getNodeName().trim();
                String valueAtt = listAtts.item(i).getNodeValue().trim();
                retour.put(nomAtt, valueAtt);
            }
        }
        return retour;
    }

    /**
     * Returns the value of a XML node, ie. the value contained between the opening and the closing tag for this node
     * 
     * @param DOMnode The node to get the value from
     * @return The trimmed value
     */
    public static String getNodeValue(Node DOMnode) {
        String ret = null;

        NodeList potentialChilds = DOMnode.getChildNodes();
        for (int i = 0; i < potentialChilds.getLength(); i++) {
            Node nextNode = potentialChilds.item(i);
            int typeNode = nextNode.getNodeType();

            if (typeNode == Node.TEXT_NODE) {
                ret = nextNode.getNodeValue();
                break;
            }
        }

        if (ret != null) {
            ret = ret.trim();
        }

        return ret;
    }

    /**
     * Replace in xml file amp, left-row, right-row, double-quote and apos.
     * 
     * @param ret The {@link String} in which there are characters to replace.
     * @return The altered {@link String}.
     */
    public static String replaceXMLChars(String ret) {
        String result = ret;
        if (result != null) {
            String[] amp = { "&", "&amp;" };
            String[] leftArrow = { "<", "&lt;" };
            String[] rightArrow = { ">", "&gt;" };
            String[] doubleQuote = { "\"", "&quot;" };
            String[] apos = { "'", "&apos;" };

            result = replace(result, amp[0], amp[1]);
            result = replace(result, leftArrow[0], leftArrow[1]);
            result = replace(result, rightArrow[0], rightArrow[1]);
            result = replace(result, doubleQuote[0], doubleQuote[1]);
            result = replace(result, apos[0], apos[1]);
        }
        return result;
    }

    /**
     * Writes a {@link String} in a {@link BufferedWriter}.
     * 
     * @param bw The {@link BufferedWriter} in which to write.
     * @param s The String to write.
     * @param hasNewLine Whether to add a new line at the end of the {@link String}.
     * @throws IOException If an I/O error occurred.
     */
    public static void write(BufferedWriter bw, String s, boolean hasNewLine) throws IOException {
        bw.write(s, 0, s.length());
        if (hasNewLine) {
            bw.newLine();
        }
    }

    /**
     * Replaces all occurrences of <CODE>toReplace</CODE> in String <CODE>in</CODE> by <CODE>replacement</CODE>.
     * 
     * @param in The {@link String} to alter.
     * @param toReplace The {@link String} to replace.
     * @param replacement The replacement {@link String}.
     * @return The altered {@link String}.
     */
    public static String replace(String in, String toReplace, String replacement) {
        String result = in;
        try {
            int lgDelim = toReplace.length();
            ArrayList<Integer> limitersList = new ArrayList<Integer>();
            StringBuilder finalString;

            int startIdx;
            int listIdx;

            // Looking for the string to replace
            startIdx = 0;
            while (startIdx >= 0) {
                startIdx = in.indexOf(toReplace, startIdx);

                if (startIdx >= 0) {
                    limitersList.add(Integer.valueOf(startIdx));
                    startIdx += lgDelim;
                }
            }

            // Check if there is something to do
            if (limitersList.size() > 0) {
                // Backwards replace
                finalString = new StringBuilder(in);
                listIdx = limitersList.size() - 1;

                while (listIdx >= 0) {
                    startIdx = limitersList.get(listIdx--).intValue();
                    finalString.replace(startIdx, startIdx + lgDelim, replacement);
                }
                result = finalString.toString();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            result = null;
        }
        return result;
    }

    /**
     * Convert xml data into a {@link String}.
     * 
     * @param d The XML {@link Document}.
     * @return A {@link String} value.
     * @throws TransformerException If a problem occurred while trying to transform the {@link Document} into a
     *             {@link String}.
     */
    public static String xmlToString(Document d) throws TransformerException {
        DOMSource domSource = new DOMSource(d);
        StringWriter writer = new StringWriter();
        StreamResult sr = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(domSource, sr);
        return writer.toString();
    }

}
