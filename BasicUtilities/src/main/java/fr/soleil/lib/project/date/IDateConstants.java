/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.date;

public interface IDateConstants {

    // long value for date: 1990-01-01 00:00:00.000
    public static final long REFERENCE_TIME = 631148400000L;

    /** US date format to format labels as dates */
    public static final String US_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
    /** French date format to format labels as dates */
    public static final String FR_DATE_FORMAT = "dd-MM-yyyy HH:mm:ss.SSS";

    /** Date format that is mostly used in log file names */
    public static final String LOG_FILE_NAME_DATE_FORMAT = "yyyyMMdd'T'HHmmss";
    /** Date format that is mostly used in logs */
    public static final String LOG_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";

    public static final String DATE_SEPARATOR = "-";
    public static final String SPACE_SEPARATOR = " ";
    public static final String SLASH_SEPARATOR = "/";

    public static final String YEAR_LONG_FORMAT = "yyyy";
    public static final String YEAR_SHORT_FORMAT = "yy";
    public static final String MONTH_FORMAT = "MM";
    public static final String MONTH_LONG_FORMAT = "MMMM";
    public static final String MONTH_VERY_LONG_FORMAT = "MMMMM";
    public static final String DAY_FORMAT = "dd";
    public static final String DAY_WEEK_FORMAT = "EEE";

    // yyyy-MM-dd
    public static final String DATE_LONG_FORMAT = YEAR_LONG_FORMAT + DATE_SEPARATOR + MONTH_FORMAT + DATE_SEPARATOR
            + DAY_FORMAT;

    // yyyy/MM/dd
    public static final String DATE_CLASSIC_FORMAT = YEAR_LONG_FORMAT + SLASH_SEPARATOR + MONTH_FORMAT + SLASH_SEPARATOR
            + DAY_FORMAT;

    // MM/yy
    public static final String DATE_SHORT_FORMAT = MONTH_FORMAT + SLASH_SEPARATOR + YEAR_SHORT_FORMAT;

    // EEE/dd
    public static final String DATE_DAY_FORMAT = DAY_WEEK_FORMAT + SLASH_SEPARATOR + DAY_FORMAT;

    // MM/dd
    public static final String DATE_DAY_MONTH_FORMAT = MONTH_FORMAT + SLASH_SEPARATOR + DAY_FORMAT;

    // MMMM yy
    public static final String DATE_MONTH_FORMAT = MONTH_LONG_FORMAT + SPACE_SEPARATOR + YEAR_SHORT_FORMAT;

    // dd/MM/yy
    public static final String DATE_WEEK = DAY_FORMAT + SLASH_SEPARATOR + MONTH_FORMAT + SLASH_SEPARATOR
            + YEAR_SHORT_FORMAT;

    // EEE dd
    public static final String DATE_DAY = DAY_WEEK_FORMAT + SPACE_SEPARATOR + DAY_FORMAT;

    public static final String TIME_SEPARATOR = ":";
    public static final String MILLI_SECOND_SEPARATOR = ".";
    public static final String HOUR_FORMAT = "HH";
    public static final String MINUTE_FORMAT = "mm";
    public static final String SECOND_FORMAT = "ss";
    public static final String MILLI_SECOND_FORMAT = "SSS";

    // HH:mm:ss:SSS
    public static final String TIME_LONG_FORMAT = HOUR_FORMAT + TIME_SEPARATOR + MINUTE_FORMAT + TIME_SEPARATOR
            + SECOND_FORMAT + TIME_SEPARATOR + MILLI_SECOND_FORMAT;

    // HH:mm:ss
    public static final String TIME_SHORT_FORMAT = HOUR_FORMAT + TIME_SEPARATOR + MINUTE_FORMAT + TIME_SEPARATOR
            + SECOND_FORMAT;

    // HH:mm
    public static final String HOUR_MINUTE_FORMAT = HOUR_FORMAT + TIME_SEPARATOR + MINUTE_FORMAT;

    // EEE HH:mm
    public static final String TIME_HOUR_12 = DAY_WEEK_FORMAT + SPACE_SEPARATOR + HOUR_FORMAT + TIME_SEPARATOR
            + MINUTE_FORMAT;

    // HH:mm
    public static final String TIME_HOUR = HOUR_FORMAT + TIME_SEPARATOR + MINUTE_FORMAT;

    // HH:mm:ss
    public static final String TIME_SECOND = HOUR_FORMAT + TIME_SEPARATOR + MINUTE_FORMAT + TIME_SEPARATOR
            + SECOND_FORMAT;

    // HH:mm:ss.SSS
    public static final String TIME_MILLI_SECOND = HOUR_FORMAT + TIME_SEPARATOR + MINUTE_FORMAT + TIME_SEPARATOR
            + SECOND_FORMAT + MILLI_SECOND_SEPARATOR + MILLI_SECOND_FORMAT;

    // yyyy/MM/dd HH:mm:ss.SSS
    public static final String DATE_TIME_LONG_FORMAT = DATE_CLASSIC_FORMAT + SPACE_SEPARATOR + TIME_MILLI_SECOND;

    // yyyy/MM/dd HH:mm:ss
    public static final String DATE_TIME_SHORT_FORMAT = DATE_CLASSIC_FORMAT + SPACE_SEPARATOR + TIME_SECOND;

    // dd/MM/yy HH:mm:ss.SSS
    public static final String DATE_TIME_FORMAT = DATE_WEEK + SPACE_SEPARATOR + TIME_MILLI_SECOND;

}
