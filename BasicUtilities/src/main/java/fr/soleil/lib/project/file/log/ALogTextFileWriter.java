/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.file.log;

import java.io.FileWriter;
import java.io.IOException;

import fr.soleil.lib.project.log.LogData;
import fr.soleil.lib.project.resource.MessageManager;

/**
 * An {@link ALogFileWriter} that write the string representation of some {@link LogData} in a file.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public abstract class ALogTextFileWriter extends ALogFileWriter {

    public ALogTextFileWriter(MessageManager messageManager) {
        super(messageManager);
    }

    /**
     * Adds the String representation of some {@link LogData} in a {@link StringBuilder}.
     * 
     * @param builder The {@link StringBuilder}. Must not be <code>null</code>.
     * @param logs The {@link LogData}.
     * @return The {@link StringBuilder}, with expected string appended to it.
     */
    protected abstract StringBuilder appendLogsToStringBuilder(StringBuilder builder, LogData... logs);

    @Override
    protected void writeLogs(FileWriter writer, LogData... logs) throws IOException {
        StringBuilder builder = new StringBuilder();
        appendLogsToStringBuilder(builder, logs);
        try {
            writer.write(builder.toString());
        } finally {
            builder.delete(0, builder.length());
        }
    }

}
