/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.resource.MessageManager;

/**
 * A class used to execute some batch in a separated process. The process outputs can be recovered
 * in some {@link Writer}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class BatchExecutor {

    private static final String QUOT = "\"";
    private static final String OUTPUT = " output";
    private static final String ERROR = " error";
    private static final String NULL_BATCH_ERROR = MessageManager.DEFAULT_MESSAGE_MANAGER
            .getMessage("fr.soleil.lib.project.log.batch.null.error");

    private static final boolean WINDOWS_CASE = "\\".equals(File.separator);

    private String batch;
    private String batchShortName;
    private final List<String> batchParameters;
    private Writer standardOutputWriter;
    private Writer errorOutputWriter;

    /**
     * Default Constructor
     */
    public BatchExecutor() {
        this(null, null);
    }

    /**
     * Constructs this {@link BatchExecutor}, setting the batch to execute
     * 
     * @param batch The batch to execute
     * @see #getBatch()
     * @see #setBatch(String)
     * @see #BatchExecutor(String, List)
     */
    public BatchExecutor(String batch) {
        this(batch, null);
    }

    /**
     * Constructs this {@link BatchExecutor}, setting all its parameters
     * 
     * @param batch The batch to execute
     * @param batchParameters The batch parameters
     * @see #setBatchParameters(List)
     * @see #getBatchParameters()
     * @see #setBatch(String)
     * @see #getBatch()
     */
    public BatchExecutor(String batch, List<String> batchParameters) {
        super();
        setBatch(batch);
        this.batchParameters = new ArrayList<>();
        if (batchParameters != null) {
            this.batchParameters.addAll(batchParameters);
        }
        setStandardOutputWriter(null);
        setErrorOutputWriter(null);
    }

    /**
     * Returns the path of the batch to execute
     * 
     * @return A {@link String}
     */
    public String getBatch() {
        return batch;
    }

    /**
     * Sets the path of the batch to execute
     * 
     * @param batch The path of the batch to execute
     */
    public void setBatch(String batch) {
        this.batch = batch;
        computeShortName();
    }

    /**
     * Returns the batch parameters
     * 
     * @return A {@link String} {@link List}
     */
    public List<String> getBatchParameters() {
        ArrayList<String> parameters = new ArrayList<>();
        synchronized (batchParameters) {
            parameters.addAll(batchParameters);
        }
        return parameters;
    }

    /**
     * Sets the batch parameters
     * 
     * @param batchParameters The parameters to set
     */
    public void setBatchParameters(List<String> batchParameters) {
        synchronized (this.batchParameters) {
            this.batchParameters.clear();
            if (batchParameters != null) {
                this.batchParameters.addAll(batchParameters);
            }
        }
    }

    /**
     * Sets the batch parameters
     * 
     * @param batchParameters The parameters to set
     */
    public void setBatchParameters(String... batchParameters) {
        synchronized (this.batchParameters) {
            this.batchParameters.clear();
            if (batchParameters != null) {
                for (String parameter : batchParameters) {
                    this.batchParameters.add(parameter);
                }
            }
        }
    }

    /**
     * Calculates a short name based on batch name
     */
    protected void computeShortName() {
        batchShortName = null;
        if ((batch != null) && (!batch.trim().isEmpty())) {
            try {
                File temp = new File(batch);
                String name = temp.getName();
                int index = name.lastIndexOf('.');
                if (index > 0) {
                    batchShortName = name.substring(0, index);
                } else {
                    batchShortName = name;
                }
            } catch (Exception e) {
                batchShortName = null;
            }
        }
    }

    /**
     * Returns the batch short name (name without extension)
     * 
     * @return A {@link String}
     */
    public String getBatchShortName() {
        return batchShortName;
    }

    protected static boolean areQuotesNeeded(String batchToLaunch) {
        return (batchToLaunch.indexOf(' ') > -1) && (!batchToLaunch.startsWith(QUOT));
    }

    /**
     * Executes the batch.
     * There might be a known Java issue on windows if batch contains a space in its name.
     * 
     * @return The created {@link Process}.
     * 
     * @see #setBatch(String)
     * @see <a href="https://blogs.oracle.com/thejavatutorials/entry/changes_to_runtime_exec_problems">Solutions to
     *      Issues caused by changes to Runtime.exec </a>
     */
    public Process execute() {
        try {
            return execute(false);
        } catch (IOException e) {
            // Should not happen as parameter is false.
            return null;
        }
    }

    /**
     * Executes the batch. Throws errors that might be thrown at {@link Process} creation.
     * There might be a known Java issue on windows if batch contains a space in its name.
     * 
     * @return The created {@link Process}.
     * 
     * @throws SecurityException - If <code>withErrors</code> is <code>true</code> and a security manager exists and its
     *             checkExecmethod doesn't allow creation of the subprocess
     * @throws IOException - If <code>withErrors</code> is <code>true</code> and an I/O error occurs
     * @throws NullPointerException - If <code>withErrors</code> is <code>true</code> and batch or any of its parameters
     *             is <code>null</code>.
     * @see #setBatch(String)
     * @see #setBatchParameters(List)
     * @see <a href="https://blogs.oracle.com/thejavatutorials/entry/changes_to_runtime_exec_problems">Solutions to
     *      Issues caused by changes to Runtime.exec </a>
     */
    public Process executeWithError() throws IOException, SecurityException, NullPointerException {
        return execute(true);
    }

    /**
     * Treats an {@link Exception}, by throwing it or printing its stack trace.
     * 
     * @param <E> The type of {@link Exception}.
     * @param e The {@link Exception}. Can not be <code>null</code>.
     * @param withErrors Whether to throw <code>e</code>.
     * @param executed Whether batch was correctly executed before obtaining <code>e</code>.
     * @param errorWriter The {@link Writer} in which to print stack trace. Can be <code>null</code>.
     * @throws E is <code>withErrors</code> is <code>true</code>.
     */
    protected <E extends Exception> void treatError(E e, boolean withErrors, boolean executed, Writer errorWriter)
            throws E {
        if (withErrors) {
            throw e;
        } else if ((!executed) && (errorWriter != null)) {
            try {
                PrintWriter pw = new PrintWriter(errorWriter);
                e.printStackTrace(pw);
                errorWriter.flush();
                pw.close();
                errorWriter.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * Executes the batch.
     * There might be a known Java issue on windows if batch contains a space in its name.
     * 
     * @param withErrors Whether to throw encountered error during {@link Runtime#exec(String, String[])}.
     * 
     * @return The created {@link Process}.
     * 
     * @throws SecurityException - If <code>withErrors</code> is <code>true</code> and a security manager exists and its
     *             checkExecmethod doesn't allow creation of the subprocess
     * @throws IOException - If <code>withErrors</code> is <code>true</code> and an I/O error occurs
     * @throws NullPointerException - If <code>withErrors</code> is <code>true</code> and batch or any of its parameters
     *             is <code>null</code>.
     * @see #setBatch(String)
     * @see #setBatchParameters(List)
     * @see <a href="https://blogs.oracle.com/thejavatutorials/entry/changes_to_runtime_exec_problems">Solutions to
     *      Issues caused by changes to Runtime.exec </a>
     */
    protected Process execute(boolean withErrors) throws IOException, SecurityException, NullPointerException {
        Process result = null;
        String batchToLaunch = batch;
        String shortName = batchShortName;
        final Writer outputWriter = standardOutputWriter;
        final Writer errorWriter = errorOutputWriter;
        if (batchToLaunch == null) {
            treatError(new NullPointerException(NULL_BATCH_ERROR), withErrors, false, errorWriter);
        } else {
            batchToLaunch = batchToLaunch.trim();
            if (!batchToLaunch.isEmpty()) {
                batchToLaunch = getAdaptedCommand(batchToLaunch);
                String[] commands;
                synchronized (batchParameters) {
                    commands = new String[batchParameters.size() + 1];
                    int index = 0;
                    commands[index++] = batchToLaunch;
                    for (String command : batchParameters) {
                        commands[index++] = getAdaptedCommand(command);
                    }
                }
                boolean executed = false;
                try {
                    final Process process = Runtime.getRuntime().exec(commands);
                    result = process;
                    executed = true;
                    if (outputWriter != null) {
                        new Thread(shortName + OUTPUT) {
                            @Override
                            public void run() {
                                writeInputStream(process.getInputStream(), outputWriter);
                            }
                        }.start();
                    }
                    if (errorWriter != null) {
                        new Thread(shortName + ERROR) {
                            @Override
                            public void run() {
                                writeInputStream(process.getErrorStream(), errorWriter);
                            }
                        }.start();
                    }
                } catch (IOException e) {
                    // Can't do multi-catch because treatError would be considered as throwing Exception instead of
                    // desired Exception class.
                    treatError(e, withErrors, executed, errorWriter);
                } catch (SecurityException e) {
                    treatError(e, withErrors, executed, errorWriter);
                } catch (NullPointerException e) {
                    treatError(e, withErrors, executed, errorWriter);
                }
            }
        }
        return result;
    }

    protected String getAdaptedCommand(String command) {
        String adaptedCommand = command;
        if ((command != null) && (!command.isEmpty())) {
            boolean quotesNeeded;
            if (command.startsWith(QUOT)) {
                quotesNeeded = false;
            } else if (WINDOWS_CASE) {
                // windows case
                quotesNeeded = command.indexOf(' ') > -1;
            } else {
                // other cases
                quotesNeeded = false;
                char previous = '\u0000';
                for (int i = 0; i < command.length(); i++) {
                    char c = command.charAt(i);
                    if (c == ' ') {
                        if (previous != '\\') {
                            quotesNeeded = true;
                            break;
                        }
                    }
                    previous = c;
                }
            }
            if (quotesNeeded) {
                adaptedCommand = '"' + command + '"';
            }
        }
        return adaptedCommand;
    }

    /**
     * Returns the {@link Writer} used to write the batch standard output
     * 
     * @return A {@link Writer}
     */
    public Writer getStandardOutputWriter() {
        return standardOutputWriter;
    }

    /**
     * Sets the {@link Writer} in which to write the batch standard output
     * 
     * @param standardOutputWriter The desired {@link Writer}
     */
    public void setStandardOutputWriter(Writer standardOutputWriter) {
        this.standardOutputWriter = standardOutputWriter == null ? new PrintWriter(System.out) : standardOutputWriter;
    }

    /**
     * Returns the {@link Writer} used to write the batch error output
     * 
     * @return A {@link Writer}
     */
    public Writer getErrorOutputWriter() {
        return errorOutputWriter;
    }

    /**
     * Sets the {@link Writer} in which to write the batch error output
     * 
     * @param errorOutputWriter The desired {@link Writer}
     */
    public void setErrorOutputWriter(Writer errorOutputWriter) {
        this.errorOutputWriter = errorOutputWriter == null ? new PrintWriter(System.err) : errorOutputWriter;
    }

    /**
     * Writes an {@link InputStream} in a {@link Writer}
     * 
     * @param inputStream The {@link InputStream}
     * @param writer The {@link Writer}
     */
    protected static void writeInputStream(InputStream inputStream, Writer writer) {
        if ((inputStream != null) && (writer != null)) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            try {
                try {
                    String line = ObjectUtils.EMPTY_STRING;
                    while ((line = reader.readLine()) != null) {
                        writer.write(line);
                        writer.write(ObjectUtils.NEW_LINE);
                    }
                    writer.flush();
                } finally {
                    reader.close();
                }
            } catch (IOException e) {
                // nothing particular to do
                e.printStackTrace();
            }

        }
    }

}
