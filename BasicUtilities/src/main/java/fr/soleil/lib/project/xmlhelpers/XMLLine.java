/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.xmlhelpers;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A class used to represent an XML line
 * 
 * @author CLAISSE
 */
public class XMLLine {

    private final String name;
    private Map<String, String> attributes;
    private int tagCategory = 0;

    private static final String OPEN_TAG = "<";
    private static final String CLOSE_TAG = ">";
    private static final String EMPTY_TAG = " /";
    private static final String SPACE = " ";
    private static final String EQUALS = "=";
    private static final String QUOTE = "\"";
    private static final String CLOSING_TAG = "</";

    public static final int EMPTY_TAG_CATEGORY = 0;
    public static final int OPENING_TAG_CATEGORY = 1;
    public static final int CLOSING_TAG_CATEGORY = 2;

    /**
     * Constructor
     * 
     * @param name The name of the line (the xml node name)
     */
    public XMLLine(String name) {
        this.name = name;
        this.attributes = new ConcurrentHashMap<String, String>();
        this.tagCategory = EMPTY_TAG_CATEGORY;
    }

    /**
     * Constructor
     * 
     * @param name The name of the line (the xml node name)
     * @param tagCategory The category of the line (opening, closing, etc.)
     * @see #OPENING_TAG_CATEGORY
     * @see #CLOSING_TAG_CATEGORY
     * @see #EMPTY_TAG_CATEGORY
     */
    public XMLLine(String name, int tagCategory) {
        this.name = name;
        this.attributes = new ConcurrentHashMap<String, String>();
        this.tagCategory = tagCategory;
    }

    /**
     * Constructor
     * 
     * @param name The name of the line (the xml node name)
     * @param attributes The attributes
     */
    public XMLLine(String name, Map<String, String> attributes) {
        this.name = name;
        this.attributes = attributes;
        this.tagCategory = EMPTY_TAG_CATEGORY;
    }

    /**
     * Adds an attribute to the XML line
     * 
     * @param key The name of the attribute.
     * @param value The value of the arrtibute.
     */
    public void setAttribute(String key, String value) {
        attributes.put(key, value);
    }

    /**
     * Returns the value of the attribute which name corresponds to the given key
     * 
     * @param key The attribute name.
     * @return a {@link String}. <code>null</code> if no attribute corresponds to the given key.
     */
    public String getAttribute(String key) {
        String attr;
        if (attributes == null) {
            attr = null;
        } else {
            attr = attributes.get(key);
        }
        return attr;
    }

    /**
     * Appends this {@link XMLLine} {@link String} representation to an {@link Appendable}, and returns it.
     * 
     * @param appendable The {@link Appendable}
     * @return The {@link Appendable}, with the {@link String} representation appended in it.
     *         If the {@link Appendable} was <code>null</code>, returns a new {@link StringBuilder} that contains the
     *         expected {@link String} representation.
     *         It may return <code>null</code> if an {@link IOException} occurred while writing in the
     *         {@link Appendable}
     * @see Appendable#append(CharSequence)
     */
    @SuppressWarnings("unchecked")
    public <T extends Appendable> T toAppendable(T appendable) {
        if (appendable == null) {
            appendable = (T) new StringBuilder();
        }
        try {
            switch (tagCategory) {
                case CLOSING_TAG_CATEGORY:
                    appendable.append(CLOSING_TAG).append(name).append(CLOSE_TAG);
                    break;

                case OPENING_TAG_CATEGORY:
                    appendable.append(OPEN_TAG).append(name);
                    if (attributes != null) {
                        for (String key : attributes.keySet()) {
                            String value = attributes.get(key);
                            appendable.append(SPACE);
                            appendable.append(key);
                            appendable.append(EQUALS);
                            appendable.append(QUOTE);
                            appendable.append(XMLUtils.replaceXMLChars(value));
                            appendable.append(QUOTE);
                        }
                    }
                    appendable.append(CLOSE_TAG);
                    break;

                case EMPTY_TAG_CATEGORY:
                    appendable.append(OPEN_TAG).append(name);
                    if (attributes != null) {
                        for (String key : attributes.keySet()) {
                            String value = attributes.get(key);
                            appendable.append(SPACE);
                            appendable.append(key);
                            appendable.append(EQUALS);
                            appendable.append(QUOTE);
                            appendable.append(XMLUtils.replaceXMLChars(value));
                            appendable.append(QUOTE);
                        }
                    }
                    appendable.append(EMPTY_TAG);
                    appendable.append(CLOSE_TAG);
                    break;

                default:
                    // nothing to do;
            }
        } catch (IOException e) {
            e.printStackTrace();
            appendable = null;
        }
        return appendable;
    }

    @Override
    public String toString() {
        return toAppendable(null).toString();
    }

    /**
     * Returns the name of the line (the node name)
     * 
     * @return a {@link String}
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the attribute list
     * 
     * @return an {@link Map}
     */
    public Map<String, String> getAttributes() {
        return attributes;
    }

    /**
     * Sets this XML line's attributes
     * 
     * @param attributes
     *            the attributes
     */
    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    /**
     * Sets this XML line's attributes if parametered {@link String} is not null
     * 
     * @param attributes
     *            The attributes
     * @param string
     *            The parametered String
     */
    public void setAttributeIfNotNull(String s, String string) {
        if (string != null) {
            setAttribute(s, string);
        }
    }
}
