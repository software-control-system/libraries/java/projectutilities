/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.math;

import java.lang.reflect.Array;
import java.util.Arrays;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A class to interact with number (Object or primitive type) arrays
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class NumberArrayUtils {

    /**
     * Calculates an N-dimension number array's mean, ignoring NaN and infinite values
     * 
     * @param array The array
     * @return A <code>double</code>. <code>NaN</code> if mean could not be calculated.
     */
    public static double getMean(Object array) {
        double mean;
        int[] shape = ArrayUtils.recoverShape(array);
        if (shape == null) {
            mean = MathConst.NAN_FOR_NULL;
        } else if (shape.length == 0) {
            if (array instanceof Number) {
                mean = ((Number) array).doubleValue();
            } else {
                mean = MathConst.NAN_FOR_NULL;
            }
        } else {
            MeanAccumulator accumulator = new MeanAccumulator();
            accumulate(array, accumulator);
            if (accumulator.getCount() < 1) {
                mean = Double.NaN;
            } else {
                mean = accumulator.getSum() / accumulator.getCount();
            }
        }
        return mean;
    }

    private static void accumulate(Object array, MeanAccumulator accumulator) {
        int[] shape = ArrayUtils.recoverShape(array);
        if ((shape != null) && (shape.length > 0)) {
            int length = shape[0];
            if (shape.length == 1) {
                if (array instanceof Number[]) {
                    Number[] nbArray = (Number[]) array;
                    for (Number nb : nbArray) {
                        accumulator.add(nb == null ? MathConst.NAN_FOR_NULL : nb.doubleValue());
                    }
                } else {
                    for (int i = 0; i < length; i++) {
                        accumulator.add(Array.getDouble(array, i));
                    }
                }
            } else {
                for (int i = 0; i < length; i++) {
                    accumulate(Array.get(array, i), accumulator);
                }
            }
        }
    }

    /**
     * Converts a number array of any dimension into another number type array of the same dimension
     * 
     * @param array The array to convert
     * @param expectedClass The expected number type
     * @return The converted number array. If the given array was already of the right type, the result is the given
     *         array. If the given array was <code>null</code> or not an array, the result is <code>null</code>
     */
    public static Object extractArray(Object array, Class<? extends Number> expectedClass) {
        return extractArray(array, expectedClass, false);
    }

    /**
     * Converts a number array of any dimension into another number type array of the same dimension
     * 
     * @param array The array to convert
     * @param expectedClass The expected number type
     * @param copy Whether to systematically return a deep copy (deep clone) of the original array, even if it is of the
     *            expected type.
     * @return The converted number array. If the given array was already of the right type, the result is the given
     *         array. If the given array was <code>null</code> or not an array, the result is <code>null</code>
     */
    public static Object extractArray(Object array, Class<? extends Number> expectedClass, boolean copy) {
        Object result = null;
        if ((array != null) && (array.getClass().isArray())) {
            Class<?> dataType = ArrayUtils.recoverDataType(array);
            if (ObjectUtils.sameObject(dataType, expectedClass) && (!copy)) {
                result = array;
            } else if (ObjectUtils.isNumberClass(dataType)) {
                int[] shape = ArrayUtils.recoverShape(array);
                int rank = ArrayUtils.recoverArrayRank(array.getClass());
                if ((shape != null) && (shape.length > 0)) {
                    if (shape.length > 1) {
                        result = Array.newInstance(expectedClass, shape);
                        copy(array, result, rank, 0, expectedClass, copy);
                    } else {
                        result = extractSingleDimensionArray(array, expectedClass, copy);
                    }
                }
            }
        }
        return result;
    }

    protected static void copy(Object source, Object dest, int rank, int dimIndex,
            Class<? extends Number> expectedClass, boolean copy) {
        if ((source != null) && (dest != null) && (rank > 0) && (dimIndex > -1) && (dimIndex < rank)) {
            int lengthSource = Array.getLength(source);
            int lengthDest = Array.getLength(dest);
            if (lengthDest == lengthSource) {
                if (dimIndex == rank - 2) {
                    for (int i = 0; i < lengthDest; i++) {
                        Array.set(dest, i, extractSingleDimensionArray(Array.get(source, i), expectedClass, copy));
                    }
                } else {
                    for (int i = 0; i < lengthDest; i++) {
                        Object subSource = Array.get(source, i);
                        Object subDest = Array.get(dest, i);
                        if (subSource == null) {
                            if (subDest != null) {
                                Array.set(dest, i, null);
                            }
                        } else {
                            boolean canCopy = true;
                            int subLengthSource = Array.getLength(subSource);
                            int subLengthDest = (subDest == null ? -1 : Array.getLength(subDest));
                            if (subLengthDest != subLengthSource) {
                                int[] shape = ArrayUtils.recoverShape(source);
                                if ((shape == null) || (shape.length == 0)) {
                                    canCopy = false;
                                } else {
                                    subDest = Array.newInstance(expectedClass, shape);
                                }
                            }
                            if (canCopy) {
                                copy(subSource, subDest, rank, dimIndex + 1, expectedClass, copy);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Transforms any number array into another number array, of an expected number class
     * 
     * @param array The number array
     * @param expectedClass The expected number class
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A number array
     */
    protected static Object extractSingleDimensionArray(Object array, Class<? extends Number> expectedClass,
            boolean copy) {
        Object result = null;
        if (Byte.TYPE.equals(expectedClass)) {
            result = extractByteArray(array, copy);
        } else if (Byte.class.equals(expectedClass)) {
            result = extractByteObjectArray(array, copy);
        } else if (Short.TYPE.equals(expectedClass)) {
            result = extractShortArray(array, copy);
        } else if (Short.class.equals(expectedClass)) {
            result = extractShortObjectArray(array, copy);
        } else if (Integer.TYPE.equals(expectedClass)) {
            result = extractIntArray(array, copy);
        } else if (Integer.class.equals(expectedClass)) {
            result = extractIntegerObjectArray(array, copy);
        } else if (Long.TYPE.equals(expectedClass)) {
            result = extractLongArray(array, copy);
        } else if (Long.class.equals(expectedClass)) {
            result = extractLongObjectArray(array, copy);
        } else if (Float.TYPE.equals(expectedClass)) {
            result = extractFloatArray(array, copy);
        } else if (Float.class.equals(expectedClass)) {
            result = extractFloatObjectArray(array, copy);
        } else if (Double.TYPE.equals(expectedClass)) {
            result = extractDoubleArray(array, copy);
        } else if (Double.class.equals(expectedClass)) {
            result = extractDoubleObjectArray(array, copy);
        } else if (Number.class.equals(expectedClass)) {
            result = extractNumberArray(array, copy);
        }
        return result;
    }

    /**
     * Transforms any number array into a <code>byte</code> array
     * 
     * @param array The number array
     * @return A <code>byte[]</code>
     */
    public static byte[] extractByteArray(Object array) {
        return extractByteArray(array, false);
    }

    /**
     * Transforms any number array into a <code>byte</code> array
     * 
     * @param array The number array
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A <code>byte[]</code>
     */
    public static byte[] extractByteArray(Object array, boolean copy) {
        byte[] result = null;
        if (array instanceof byte[]) {
            result = (byte[]) array;
            if (copy) {
                result = result.clone();
            }
        } else if (array instanceof short[]) {
            short[] temp = (short[]) array;
            result = new byte[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (byte) temp[i];
            }
        } else if (array instanceof int[]) {
            int[] temp = (int[]) array;
            result = new byte[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (byte) temp[i];
            }
        } else if (array instanceof long[]) {
            long[] temp = (long[]) array;
            result = new byte[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (byte) temp[i];
            }
        } else if (array instanceof float[]) {
            float[] temp = (float[]) array;
            result = new byte[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (byte) temp[i];
            }
        } else if (array instanceof double[]) {
            double[] temp = (double[]) array;
            result = new byte[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (byte) temp[i];
            }
        } else if (array instanceof Number[]) {
            Number[] temp = (Number[]) array;
            result = new byte[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (temp[i] == null ? 0 : temp[i].byteValue());
            }
        }
        return result;
    }

    /**
     * Transforms any number array into a {@link Byte} array
     * 
     * @param array The number array
     * @return A {@link Byte}<code>[]</code>
     */
    public static Byte[] extractByteObjectArray(Object array) {
        return extractByteObjectArray(array, false);
    }

    /**
     * Transforms any number array into a {@link Byte} array
     * 
     * @param array The number array
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A {@link Byte}<code>[]</code>
     */
    public static Byte[] extractByteObjectArray(Object array, boolean copy) {
        Byte[] result = null;
        if (array instanceof Byte[]) {
            result = (Byte[]) array;
            if (copy) {
                result = result.clone();
            }
        } else if (array instanceof byte[]) {
            byte[] temp = (byte[]) array;
            result = new Byte[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof short[]) {
            short[] temp = (short[]) array;
            result = new Byte[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (byte) temp[i];
            }
        } else if (array instanceof int[]) {
            int[] temp = (int[]) array;
            result = new Byte[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (byte) temp[i];
            }
        } else if (array instanceof long[]) {
            long[] temp = (long[]) array;
            result = new Byte[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (byte) temp[i];
            }
        } else if (array instanceof float[]) {
            float[] temp = (float[]) array;
            result = new Byte[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (byte) temp[i];
            }
        } else if (array instanceof double[]) {
            double[] temp = (double[]) array;
            result = new Byte[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (byte) temp[i];
            }
        } else if (array instanceof Number[]) {
            Number[] temp = (Number[]) array;
            result = new Byte[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (temp[i] == null ? 0 : temp[i].byteValue());
            }
        }
        return result;
    }

    /**
     * Transforms any number array into a <code>short</code> array
     * 
     * @param array The number array
     * @return A <code>short[]</code>
     */
    public static short[] extractShortArray(Object array) {
        return extractShortArray(array, false);
    }

    /**
     * Transforms any number array into a <code>short</code> array
     * 
     * @param array The number array
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A <code>short[]</code>
     */
    public static short[] extractShortArray(Object array, boolean copy) {
        short[] result = null;
        if (array instanceof byte[]) {
            byte[] temp = (byte[]) array;
            result = new short[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof short[]) {
            result = (short[]) array;
            if (copy) {
                result = result.clone();
            }
        } else if (array instanceof int[]) {
            int[] temp = (int[]) array;
            result = new short[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (short) temp[i];
            }
        } else if (array instanceof long[]) {
            long[] temp = (long[]) array;
            result = new short[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (short) temp[i];
            }
        } else if (array instanceof float[]) {
            float[] temp = (float[]) array;
            result = new short[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (short) temp[i];
            }
        } else if (array instanceof double[]) {
            double[] temp = (double[]) array;
            result = new short[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (short) temp[i];
            }
        } else if (array instanceof Number[]) {
            Number[] temp = (Number[]) array;
            result = new short[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (temp[i] == null ? 0 : temp[i].shortValue());
            }
        }
        return result;
    }

    /**
     * Transforms any number array into a {@link Short} array
     * 
     * @param array The number array
     * @return A {@link Short}<code>[]</code>
     */
    public static Short[] extractShortObjectArray(Object array) {
        return extractShortObjectArray(array, false);
    }

    /**
     * Transforms any number array into a {@link Short} array
     * 
     * @param array The number array
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A {@link Short}<code>[]</code>
     */
    public static Short[] extractShortObjectArray(Object array, boolean copy) {
        Short[] result = null;
        if (array instanceof Short[]) {
            result = (Short[]) array;
            if (copy) {
                result = result.clone();
            }
        } else if (array instanceof byte[]) {
            byte[] temp = (byte[]) array;
            result = new Short[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (short) temp[i];
            }
        } else if (array instanceof short[]) {
            short[] temp = (short[]) array;
            result = new Short[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof int[]) {
            int[] temp = (int[]) array;
            result = new Short[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (short) temp[i];
            }
        } else if (array instanceof long[]) {
            long[] temp = (long[]) array;
            result = new Short[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (short) temp[i];
            }
        } else if (array instanceof float[]) {
            float[] temp = (float[]) array;
            result = new Short[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (short) temp[i];
            }
        } else if (array instanceof double[]) {
            double[] temp = (double[]) array;
            result = new Short[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (short) temp[i];
            }
        } else if (array instanceof Number[]) {
            Number[] temp = (Number[]) array;
            result = new Short[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (temp[i] == null ? 0 : temp[i].shortValue());
            }
        }
        return result;
    }

    /**
     * Transforms any number array into an <code>int</code> array
     * 
     * @param array The number array
     * @return A <code>int[]</code>
     */
    public static int[] extractIntArray(Object array) {
        return extractIntArray(array, false);
    }

    /**
     * Transforms any number array into an <code>int</code> array
     * 
     * @param array The number array
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A <code>int[]</code>
     */
    public static int[] extractIntArray(Object array, boolean copy) {
        int[] result = null;
        if (array instanceof byte[]) {
            byte[] temp = (byte[]) array;
            result = new int[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof short[]) {
            short[] temp = (short[]) array;
            result = new int[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof int[]) {
            result = (int[]) array;
            if (copy) {
                result = result.clone();
            }
        } else if (array instanceof long[]) {
            long[] temp = (long[]) array;
            result = new int[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (int) temp[i];
            }
        } else if (array instanceof float[]) {
            float[] temp = (float[]) array;
            result = new int[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (int) temp[i];
            }
        } else if (array instanceof double[]) {
            double[] temp = (double[]) array;
            result = new int[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (int) temp[i];
            }
        } else if (array instanceof Number[]) {
            Number[] temp = (Number[]) array;
            result = new int[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (temp[i] == null ? 0 : temp[i].intValue());
            }
        }
        return result;
    }

    /**
     * Transforms any number array into a {@link Integer} array
     * 
     * @param array The number array
     * @return A {@link Integer}<code>[]</code>
     */
    public static Integer[] extractIntegerObjectArray(Object array) {
        return extractIntegerObjectArray(array, false);
    }

    /**
     * Transforms any number array into a {@link Integer} array
     * 
     * @param array The number array
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A {@link Integer}<code>[]</code>
     */
    public static Integer[] extractIntegerObjectArray(Object array, boolean copy) {
        Integer[] result = null;
        if (array instanceof Integer[]) {
            result = (Integer[]) array;
            if (copy) {
                result = result.clone();
            }
        } else if (array instanceof byte[]) {
            byte[] temp = (byte[]) array;
            result = new Integer[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (int) temp[i];
            }
        } else if (array instanceof short[]) {
            short[] temp = (short[]) array;
            result = new Integer[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (int) temp[i];
            }
        } else if (array instanceof int[]) {
            int[] temp = (int[]) array;
            result = new Integer[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof long[]) {
            long[] temp = (long[]) array;
            result = new Integer[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (int) temp[i];
            }
        } else if (array instanceof float[]) {
            float[] temp = (float[]) array;
            result = new Integer[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (int) temp[i];
            }
        } else if (array instanceof double[]) {
            double[] temp = (double[]) array;
            result = new Integer[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (int) temp[i];
            }
        } else if (array instanceof Number[]) {
            Number[] temp = (Number[]) array;
            result = new Integer[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (temp[i] == null ? 0 : temp[i].intValue());
            }
        }
        return result;
    }

    /**
     * Transforms any number array into a <code>long</code> array
     * 
     * @param array The number array
     * @return A <code>long[]</code>
     */
    public static long[] extractLongArray(Object array) {
        return extractLongArray(array, false);
    }

    /**
     * Transforms any number array into a <code>long</code> array
     * 
     * @param array The number array
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A <code>long[]</code>
     */
    public static long[] extractLongArray(Object array, boolean copy) {
        long[] result = null;
        if (array instanceof byte[]) {
            byte[] temp = (byte[]) array;
            result = new long[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof short[]) {
            short[] temp = (short[]) array;
            result = new long[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof int[]) {
            int[] temp = (int[]) array;
            result = new long[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof long[]) {
            result = (long[]) array;
            if (copy) {
                result = result.clone();
            }
        } else if (array instanceof float[]) {
            float[] temp = (float[]) array;
            result = new long[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (long) temp[i];
            }
        } else if (array instanceof double[]) {
            double[] temp = (double[]) array;
            result = new long[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (long) temp[i];
            }
        } else if (array instanceof Number[]) {
            Number[] temp = (Number[]) array;
            result = new long[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (temp[i] == null ? 0 : temp[i].longValue());
            }
        }
        return result;
    }

    /**
     * Transforms any number array into a {@link Long} array
     * 
     * @param array The number array
     * @return A {@link Long}<code>[]</code>
     */
    public static Long[] extractLongObjectArray(Object array) {
        return extractLongObjectArray(array, false);
    }

    /**
     * Transforms any number array into a {@link Long} array
     * 
     * @param array The number array
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A {@link Long}<code>[]</code>
     */
    public static Long[] extractLongObjectArray(Object array, boolean copy) {
        Long[] result = null;
        if (array instanceof Long[]) {
            result = (Long[]) array;
            if (copy) {
                result = result.clone();
            }
        } else if (array instanceof byte[]) {
            byte[] temp = (byte[]) array;
            result = new Long[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (long) temp[i];
            }
        } else if (array instanceof short[]) {
            short[] temp = (short[]) array;
            result = new Long[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (long) temp[i];
            }
        } else if (array instanceof int[]) {
            int[] temp = (int[]) array;
            result = new Long[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (long) temp[i];
            }
        } else if (array instanceof long[]) {
            long[] temp = (long[]) array;
            result = new Long[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof float[]) {
            float[] temp = (float[]) array;
            result = new Long[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (long) temp[i];
            }
        } else if (array instanceof double[]) {
            double[] temp = (double[]) array;
            result = new Long[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (long) temp[i];
            }
        } else if (array instanceof Number[]) {
            Number[] temp = (Number[]) array;
            result = new Long[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (temp[i] == null ? 0 : temp[i].longValue());
            }
        }
        return result;
    }

    /**
     * Transforms any number array into a <code>float</code> array
     * 
     * @param array The number array
     * @return A <code>float[]</code>
     */
    public static float[] extractFloatArray(Object array) {
        return extractFloatArray(array, false);
    }

    /**
     * Transforms any number array into a <code>float</code> array
     * 
     * @param array The number array
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A <code>float[]</code>
     */
    public static float[] extractFloatArray(Object array, boolean copy) {
        float[] result = null;
        if (array instanceof byte[]) {
            byte[] temp = (byte[]) array;
            result = new float[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof short[]) {
            short[] temp = (short[]) array;
            result = new float[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof int[]) {
            int[] temp = (int[]) array;
            result = new float[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof long[]) {
            long[] temp = (long[]) array;
            result = new float[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof float[]) {
            result = (float[]) array;
            if (copy) {
                result = result.clone();
            }
        } else if (array instanceof double[]) {
            double[] temp = (double[]) array;
            result = new float[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (float) temp[i];
            }
        } else if (array instanceof Number[]) {
            Number[] temp = (Number[]) array;
            result = new float[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (temp[i] == null ? Float.NaN : temp[i].floatValue());
            }
        }
        return result;
    }

    /**
     * Transforms any number array into a {@link Float} array
     * 
     * @param array The number array
     * @return A {@link Float}<code>[]</code>
     */
    public static Float[] extractFloatObjectArray(Object array) {
        return extractFloatObjectArray(array, false);
    }

    /**
     * Transforms any number array into a {@link Float} array
     * 
     * @param array The number array
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A {@link Float}<code>[]</code>
     */
    public static Float[] extractFloatObjectArray(Object array, boolean copy) {
        Float[] result = null;
        if (array instanceof Float[]) {
            result = (Float[]) array;
            if (copy) {
                result = result.clone();
            }
        } else if (array instanceof byte[]) {
            byte[] temp = (byte[]) array;
            result = new Float[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (float) temp[i];
            }
        } else if (array instanceof short[]) {
            short[] temp = (short[]) array;
            result = new Float[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (float) temp[i];
            }
        } else if (array instanceof int[]) {
            int[] temp = (int[]) array;
            result = new Float[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (float) temp[i];
            }
        } else if (array instanceof long[]) {
            long[] temp = (long[]) array;
            result = new Float[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (float) temp[i];
            }
        } else if (array instanceof float[]) {
            float[] temp = (float[]) array;
            result = new Float[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof double[]) {
            double[] temp = (double[]) array;
            result = new Float[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (float) temp[i];
            }
        } else if (array instanceof Number[]) {
            Number[] temp = (Number[]) array;
            result = new Float[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (temp[i] == null ? 0 : temp[i].floatValue());
            }
        }
        return result;
    }

    /**
     * Transforms any number array into a <code>double</code> array
     * 
     * @param array The number array
     * @return A <code>double[]</code>
     */
    public static double[] extractDoubleArray(Object array) {
        return extractDoubleArray(array, false);
    }

    /**
     * Transforms any number array into a <code>double</code> array
     * 
     * @param array The number array
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A <code>double[]</code>
     */
    public static double[] extractDoubleArray(Object array, boolean copy) {
        double[] result = null;
        if (array instanceof byte[]) {
            byte[] temp = (byte[]) array;
            result = new double[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof short[]) {
            short[] temp = (short[]) array;
            result = new double[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof int[]) {
            int[] temp = (int[]) array;
            result = new double[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof long[]) {
            long[] temp = (long[]) array;
            result = new double[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof float[]) {
            float[] temp = (float[]) array;
            result = new double[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof double[]) {
            result = (double[]) array;
            if (copy) {
                result = result.clone();
            }
        } else if (array instanceof Number[]) {
            Number[] temp = (Number[]) array;
            result = new double[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (temp[i] == null ? MathConst.NAN_FOR_NULL : temp[i].doubleValue());
            }
        }
        return result;
    }

    /**
     * Transforms any number array into a {@link Double} array
     * 
     * @param array The number array
     * @return A {@link Double}<code>[]</code>
     */
    public static Double[] extractDoubleObjectArray(Object array) {
        return extractDoubleObjectArray(array, false);
    }

    /**
     * Transforms any number array into a {@link Double} array
     * 
     * @param array The number array
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A {@link Double}<code>[]</code>
     */
    public static Double[] extractDoubleObjectArray(Object array, boolean copy) {
        Double[] result = null;
        if (array instanceof Double[]) {
            result = (Double[]) array;
            if (copy) {
                result = result.clone();
            }
        } else if (array instanceof byte[]) {
            byte[] temp = (byte[]) array;
            result = new Double[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (double) temp[i];
            }
        } else if (array instanceof short[]) {
            short[] temp = (short[]) array;
            result = new Double[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (double) temp[i];
            }
        } else if (array instanceof int[]) {
            int[] temp = (int[]) array;
            result = new Double[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (double) temp[i];
            }
        } else if (array instanceof long[]) {
            long[] temp = (long[]) array;
            result = new Double[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (double) temp[i];
            }
        } else if (array instanceof float[]) {
            float[] temp = (float[]) array;
            result = new Double[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (double) temp[i];
            }
        } else if (array instanceof double[]) {
            double[] temp = (double[]) array;
            result = new Double[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof Number[]) {
            Number[] temp = (Number[]) array;
            result = new Double[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = (temp[i] == null ? 0 : temp[i].doubleValue());
            }
        }
        return result;
    }

    /**
     * Transforms any number array into a {@link Number} array
     * 
     * @param array The number array
     * @return A {@link Number}<code>[]</code>
     */
    public static Number[] extractNumberArray(Object array) {
        return extractNumberArray(array, false);
    }

    /**
     * Transforms any number array into a {@link Number} array
     * 
     * @param array The number array
     * @param copy Whether to systematically return a copy of the original array, even if it is of the expected type.
     * @return A {@link Number}<code>[]</code>
     */
    public static Number[] extractNumberArray(Object array, boolean copy) {
        Number[] result = null;
        if (array instanceof byte[]) {
            byte[] temp = (byte[]) array;
            result = new Number[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof short[]) {
            short[] temp = (short[]) array;
            result = new Number[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof int[]) {
            int[] temp = (int[]) array;
            result = new Number[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof long[]) {
            long[] temp = (long[]) array;
            result = new Number[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof float[]) {
            float[] temp = (float[]) array;
            result = new Number[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof double[]) {
            double[] temp = (double[]) array;
            result = new Number[temp.length];
            for (int i = 0; i < temp.length; i++) {
                result[i] = temp[i];
            }
        } else if (array instanceof Number[]) {
            result = (Number[]) array;
            if (copy) {
                result = result.clone();
            }
        }
        return result;
    }

    /**
     * Calculates whether a given {@link Object} is a number array that can be set at a given
     * position
     * 
     * @param array The {@link Object} to test
     * @param index The position
     * @return A <code>boolean</code> value
     */
    private static boolean isNumberArraySettable(Object array, int index) {
        boolean settable = false;
        if (array != null) {
            Class<?> arrayClass = array.getClass();
            if (arrayClass.isArray() && ObjectUtils.isNumberClass(arrayClass.getComponentType())) {
                settable = (index > -1 && index < Array.getLength(array));
            }
        }
        return settable;
    }

    /**
     * Sets a number array with a given byte value
     * 
     * @param array The number array
     * @param index The index at which to set the value
     * @param value The value to set
     */
    public static void setArray(Object array, int index, byte value) {
        if (isNumberArraySettable(array, index)) {
            setArray(array, array.getClass().getComponentType(), index, value);
        }
    }

    /**
     * Sets a number array with a given short value
     * 
     * @param array The number array
     * @param index The index at which to set the value
     * @param value The value to set
     */
    public static void setArray(Object array, int index, short value) {
        if (isNumberArraySettable(array, index)) {
            setArray(array, array.getClass().getComponentType(), index, value);
        }
    }

    /**
     * Sets a number array with a given int value
     * 
     * @param array The number array
     * @param index The index at which to set the value
     * @param value The value to set
     */
    public static void setArray(Object array, int index, int value) {
        if (isNumberArraySettable(array, index)) {
            setArray(array, array.getClass().getComponentType(), index, value);
        }
    }

    /**
     * Sets a number array with a given long value
     * 
     * @param array The number array
     * @param index The index at which to set the value
     * @param value The value to set
     */
    public static void setArray(Object array, int index, long value) {
        if (isNumberArraySettable(array, index)) {
            setArray(array, array.getClass().getComponentType(), index, value);
        }
    }

    /**
     * Sets a number array with a given float value
     * 
     * @param array The number array
     * @param index The index at which to set the value
     * @param value The value to set
     */
    public static void setArray(Object array, int index, float value) {
        if (isNumberArraySettable(array, index)) {
            setArray(array, array.getClass().getComponentType(), index, value);
        }
    }

    /**
     * Sets a number array with a given double value
     * 
     * @param array The number array
     * @param index The index at which to set the value
     * @param value The value to set
     */
    public static void setArray(Object array, int index, double value) {
        if (isNumberArraySettable(array, index)) {
            setArray(array, array.getClass().getComponentType(), index, value);
        }
    }

    /**
     * Sets a number array with a given {@link Number} value
     * 
     * @param array The number array
     * @param index The index at which to set the value
     * @param value The value to set
     */
    public static void setArray(Object array, int index, Number value) {
        if (isNumberArraySettable(array, index)) {
            setArray(array, array.getClass().getComponentType(), index, value);
        }
    }

    /**
     * Sets a number array with a given byte value
     * 
     * @param array The number array
     * @param numberClass The type of number the array contains
     * @param index The index at which to set the value
     * @param value The value to set
     */
    private static void setArray(Object array, Class<?> numberClass, int index, byte value) {
        if (Byte.TYPE.equals(numberClass)) {
            ((byte[]) array)[index] = value;
        } else if (Byte.class.equals(numberClass) || Number.class.equals(numberClass)) {
            ((Number[]) array)[index] = value;
        } else if (Short.TYPE.equals(numberClass)) {
            ((short[]) array)[index] = value;
        } else if (Short.class.equals(numberClass)) {
            ((Short[]) array)[index] = (short) value;
        } else if (Integer.TYPE.equals(numberClass)) {
            ((int[]) array)[index] = value;
        } else if (Integer.class.equals(numberClass)) {
            ((Integer[]) array)[index] = (int) value;
        } else if (Long.TYPE.equals(numberClass)) {
            ((long[]) array)[index] = value;
        } else if (Long.class.equals(numberClass)) {
            ((Long[]) array)[index] = (long) value;
        } else if (Float.TYPE.equals(numberClass)) {
            ((float[]) array)[index] = value;
        } else if (Float.class.equals(numberClass)) {
            ((Float[]) array)[index] = (float) value;
        } else if (Double.TYPE.equals(numberClass)) {
            ((double[]) array)[index] = value;
        } else if (Double.class.equals(numberClass)) {
            ((Double[]) array)[index] = (double) value;
        }
    }

    /**
     * Sets a number array with a given short value
     * 
     * @param array The number array
     * @param numberClass The type of number the array contains
     * @param index The index at which to set the value
     * @param value The value to set
     */
    private static void setArray(Object array, Class<?> numberClass, int index, short value) {
        if (Short.TYPE.equals(numberClass)) {
            ((short[]) array)[index] = value;
        } else if (Short.class.equals(numberClass) || Number.class.equals(numberClass)) {
            ((Number[]) array)[index] = value;
        } else if (Byte.TYPE.equals(numberClass)) {
            ((byte[]) array)[index] = (byte) value;
        } else if (Byte.class.equals(numberClass)) {
            ((Byte[]) array)[index] = (byte) value;
        } else if (Integer.TYPE.equals(numberClass)) {
            ((int[]) array)[index] = value;
        } else if (Integer.class.equals(numberClass)) {
            ((Integer[]) array)[index] = (int) value;
        } else if (Long.TYPE.equals(numberClass)) {
            ((long[]) array)[index] = value;
        } else if (Long.class.equals(numberClass)) {
            ((Long[]) array)[index] = (long) value;
        } else if (Float.TYPE.equals(numberClass)) {
            ((float[]) array)[index] = value;
        } else if (Float.class.equals(numberClass)) {
            ((Float[]) array)[index] = (float) value;
        } else if (Double.TYPE.equals(numberClass)) {
            ((double[]) array)[index] = value;
        } else if (Double.class.equals(numberClass)) {
            ((Double[]) array)[index] = (double) value;
        }
    }

    /**
     * Sets a number array with a given int value
     * 
     * @param array The number array
     * @param numberClass The type of number the array contains
     * @param index The index at which to set the value
     * @param value The value to set
     */
    private static void setArray(Object array, Class<?> numberClass, int index, int value) {
        if (Integer.TYPE.equals(numberClass)) {
            ((int[]) array)[index] = value;
        } else if (Integer.class.equals(numberClass) || Number.class.equals(numberClass)) {
            ((Number[]) array)[index] = value;
        } else if (Byte.TYPE.equals(numberClass)) {
            ((byte[]) array)[index] = (byte) value;
        } else if (Byte.class.equals(numberClass)) {
            ((Byte[]) array)[index] = (byte) value;
        } else if (Short.TYPE.equals(numberClass)) {
            ((short[]) array)[index] = (short) value;
        } else if (Short.class.equals(numberClass)) {
            ((Short[]) array)[index] = (short) value;
        } else if (Long.TYPE.equals(numberClass)) {
            ((long[]) array)[index] = value;
        } else if (Long.class.equals(numberClass)) {
            ((Long[]) array)[index] = (long) value;
        } else if (Float.TYPE.equals(numberClass)) {
            ((float[]) array)[index] = value;
        } else if (Float.class.equals(numberClass)) {
            ((Float[]) array)[index] = (float) value;
        } else if (Double.TYPE.equals(numberClass)) {
            ((double[]) array)[index] = value;
        } else if (Double.class.equals(numberClass)) {
            ((Double[]) array)[index] = (double) value;
        }
    }

    /**
     * Sets a number array with a given long value
     * 
     * @param array The number array
     * @param numberClass The type of number the array contains
     * @param index The index at which to set the value
     * @param value The value to set
     */
    private static void setArray(Object array, Class<?> numberClass, int index, long value) {
        if (Long.TYPE.equals(numberClass)) {
            ((long[]) array)[index] = value;
        } else if (Long.class.equals(numberClass) || Number.class.equals(numberClass)) {
            ((Number[]) array)[index] = value;
        } else if (Byte.TYPE.equals(numberClass)) {
            ((byte[]) array)[index] = (byte) value;
        } else if (Byte.class.equals(numberClass)) {
            ((Byte[]) array)[index] = (byte) value;
        } else if (Short.TYPE.equals(numberClass)) {
            ((short[]) array)[index] = (short) value;
        } else if (Short.class.equals(numberClass)) {
            ((Short[]) array)[index] = (short) value;
        } else if (Integer.TYPE.equals(numberClass)) {
            ((int[]) array)[index] = (int) value;
        } else if (Integer.class.equals(numberClass)) {
            ((Integer[]) array)[index] = (int) value;
        } else if (Float.TYPE.equals(numberClass)) {
            ((float[]) array)[index] = value;
        } else if (Float.class.equals(numberClass)) {
            ((Float[]) array)[index] = (float) value;
        } else if (Double.TYPE.equals(numberClass)) {
            ((double[]) array)[index] = value;
        } else if (Double.class.equals(numberClass)) {
            ((Double[]) array)[index] = (double) value;
        }
    }

    /**
     * Sets a number array with a given float value
     * 
     * @param array The number array
     * @param numberClass The type of number the array contains
     * @param index The index at which to set the value
     * @param value The value to set
     */
    private static void setArray(Object array, Class<?> numberClass, int index, float value) {
        if (Float.TYPE.equals(numberClass)) {
            ((float[]) array)[index] = value;
        } else if (Float.class.equals(numberClass) || Number.class.equals(numberClass)) {
            ((Number[]) array)[index] = value;
        } else if (Byte.TYPE.equals(numberClass)) {
            ((byte[]) array)[index] = (byte) value;
        } else if (Byte.class.equals(numberClass)) {
            ((Byte[]) array)[index] = (byte) value;
        } else if (Short.TYPE.equals(numberClass)) {
            ((short[]) array)[index] = (short) value;
        } else if (Short.class.equals(numberClass)) {
            ((Short[]) array)[index] = (short) value;
        } else if (Integer.TYPE.equals(numberClass)) {
            ((int[]) array)[index] = (int) value;
        } else if (Integer.class.equals(numberClass)) {
            ((Integer[]) array)[index] = (int) value;
        } else if (Long.TYPE.equals(numberClass)) {
            ((long[]) array)[index] = (long) value;
        } else if (Long.class.equals(numberClass)) {
            ((Long[]) array)[index] = (long) value;
        } else if (Double.TYPE.equals(numberClass)) {
            ((double[]) array)[index] = value;
        } else if (Double.class.equals(numberClass)) {
            ((Double[]) array)[index] = (double) value;
        }
    }

    /**
     * Sets a number array with a given double value
     * 
     * @param array The number array
     * @param numberClass The type of number the array contains
     * @param index The index at which to set the value
     * @param value The value to set
     */
    private static void setArray(Object array, Class<?> numberClass, int index, double value) {
        if (Double.TYPE.equals(numberClass)) {
            ((double[]) array)[index] = value;
        } else if (Double.class.equals(numberClass) || Number.class.equals(numberClass)) {
            ((Number[]) array)[index] = value;
        } else if (Byte.TYPE.equals(numberClass)) {
            ((byte[]) array)[index] = (byte) value;
        } else if (Byte.class.equals(numberClass)) {
            ((Byte[]) array)[index] = (byte) value;
        } else if (Short.TYPE.equals(numberClass)) {
            ((short[]) array)[index] = (short) value;
        } else if (Short.class.equals(numberClass)) {
            ((Short[]) array)[index] = (short) value;
        } else if (Integer.TYPE.equals(numberClass)) {
            ((int[]) array)[index] = (int) value;
        } else if (Integer.class.equals(numberClass)) {
            ((Integer[]) array)[index] = (int) value;
        } else if (Long.TYPE.equals(numberClass)) {
            ((long[]) array)[index] = (long) value;
        } else if (Long.class.equals(numberClass)) {
            ((Long[]) array)[index] = (long) value;
        } else if (Float.TYPE.equals(numberClass)) {
            ((float[]) array)[index] = (float) value;
        } else if (Float.class.equals(numberClass)) {
            ((Float[]) array)[index] = (float) value;
        }
    }

    /**
     * Sets a number array with a given {@link Number} value
     * 
     * @param array The number array
     * @param numberClass The type of number the array contains
     * @param index The index at which to set the value
     * @param value The value to set
     */
    private static void setArray(Object array, Class<?> numberClass, int index, Number value) {
        if (Number.class.equals(numberClass)) {
            ((Number[]) array)[index] = value;
        } else if (Byte.TYPE.equals(numberClass)) {
            if (value != null) {
                ((byte[]) array)[index] = value.byteValue();
            }
        } else if (Byte.class.equals(numberClass)) {
            if ((value == null) || (value instanceof Byte)) {
                ((Byte[]) array)[index] = (Byte) value;
            } else {
                ((Byte[]) array)[index] = value.byteValue();
            }
        } else if (Short.TYPE.equals(numberClass)) {
            if (value != null) {
                ((short[]) array)[index] = value.shortValue();
            }
        } else if (Short.class.equals(numberClass)) {
            if ((value == null) || (value instanceof Short)) {
                ((Short[]) array)[index] = (Short) value;
            } else {
                ((Short[]) array)[index] = value.shortValue();
            }
        } else if (Integer.TYPE.equals(numberClass)) {
            if (value != null) {
                ((int[]) array)[index] = value.intValue();
            }
        } else if (Integer.class.equals(numberClass)) {
            if ((value == null) || (value instanceof Integer)) {
                ((Integer[]) array)[index] = (Integer) value;
            } else {
                ((Integer[]) array)[index] = value.intValue();
            }
        } else if (Long.TYPE.equals(numberClass)) {
            if (value != null) {
                ((long[]) array)[index] = value.longValue();
            }
        } else if (Long.class.equals(numberClass)) {
            if ((value == null) || (value instanceof Long)) {
                ((Long[]) array)[index] = (Long) value;
            } else {
                ((Long[]) array)[index] = value.longValue();
            }
        } else if (Float.TYPE.equals(numberClass)) {
            if (value != null) {
                ((float[]) array)[index] = value.floatValue();
            }
        } else if (Float.class.equals(numberClass)) {
            if ((value == null) || (value instanceof Float)) {
                ((Float[]) array)[index] = (Float) value;
            } else {
                ((Float[]) array)[index] = value.floatValue();
            }
        } else if (Double.TYPE.equals(numberClass)) {
            if (value != null) {
                ((double[]) array)[index] = value.doubleValue();
            }
        } else if (Double.class.equals(numberClass)) {
            if ((value == null) || (value instanceof Double)) {
                ((Double[]) array)[index] = (Double) value;
            } else {
                ((Double[]) array)[index] = value.doubleValue();
            }
        }
    }

    /**
     * Builds a new <code>double[]</code>, of a given length, starting with a minimum, with every
     * element separated by a given step
     * 
     * @param min The minimum
     * @param step The step
     * @param length The length
     * @return A <code>double[]</code>
     */
    public static double[] buildArray(double min, double step, int length) {
        double[] result = new double[length];
        if (length > 0) {
            for (int i = 0; i < length; i++) {
                result[i] = min + i * step;
            }
        }
        return result;
    }

    /**
     * Builds a new <code>double[]</code>, starting with a minimum, ending with a value closest to but under a given
     * maximum (inclusive), with every element separated by a step of <code>1</code>
     * 
     * @param min The minimum
     * @param max The maximum
     * @return A <code>double[]</code>
     * @see #buildArray(double, double, double)
     */
    public static double[] buildArray(double min, double max) {
        return buildArray(min, 1, max);
    }

    /**
     * Builds a new <code>double[]</code>, starting with a minimum, ending with a value closest to but under a given
     * maximum (inclusive), with every element separated by a given step
     * <table border="1" cellspacing="0" cellpadding="0">
     * <caption>Example: </caption>
     * <tr>
     * <th>code</th>
     * <th>result</th>
     * </tr>
     * <tr>
     * <td><code>buildArray(0, 0.5, 2)</code></td>
     * <td><code>[0, 0.5, 1, 1.5, 2]</code></td>
     * </tr>
     * <tr>
     * <td><code>buildArray(0, 0.5, 1.9)</code></td>
     * <td><code>[0, 0.5, 1, 1.5]</code></td>
     * </tr>
     * </table>
     * 
     * @param min The minimum
     * @param step The step
     * @param max The maximum
     * @return A <code>double[]</code>
     */
    public static double[] buildArray(double min, double step, double max) {
        int length = recoverLength(min, max, step);
        double[] result = new double[length];
        for (int i = 0; i < length; i++) {
            result[i] = min + i * step;
        }
        return result;
    }

    /**
     * Recovers the step for a table of a given length, knowing its minimum and maximum values
     * 
     * @param min The minimum value
     * @param max The maximum value
     * @param length The table length
     * @return A <code>double</code>
     */
    public static double recoverStep(double min, double max, int length) {
        double step;
        if (length > 1) {
            step = (max - min) / (length - 1);
        } else {
            step = 0;
        }
        return step;
    }

    /**
     * Recovers the length for a table, knowing its minimum and maximum values, and its step
     * 
     * @param min The minimum value
     * @param max The maximum value
     * @param step The table step
     * @return An <code>int</code>
     */
    public static int recoverLength(double min, double max, double step) {
        int length;
        long bigLength = recoverBigLength(min, max, step);
        if (bigLength > Integer.MAX_VALUE) {
            length = Integer.MAX_VALUE;
        } else {
            length = (int) bigLength;
        }
        return length;
    }

    /**
     * Recovers the length for a table, knowing its minimum and maximum values, and its step.
     * The result is a <code>long</code> in order not to have the {@link Integer#MAX_VALUE} limit
     * 
     * @param min The minimum value
     * @param max The maximum value
     * @param step The table step
     * @return A <code>long</code>
     */
    public static long recoverBigLength(double min, double max, double step) {
        long length;
        double tempVal = Math.abs((max - min) / step);
        long floor = (long) Math.floor(tempVal);
        long round = Math.round(tempVal);
        // testing floating calculation error margin (DATAREDUC-293)
        if (NumberUtils.sameDouble(tempVal, round, NumberUtils.FLOAT_DELTA_BIG_MARGIN)) {
            length = round + 1;
        } else {
            length = floor + 1;
        }
        // floor and round are already limited to Long.MAX_VALUE, but previous test is not.
        if (length < 0) {
            length = Long.MAX_VALUE;
        }
        return length;
    }

    /**
     * Returns a copy of a given array, without any <code>NaN</code> or <code>infinite</code> value, but with all of its
     * values sorted into ascending numerical order.
     * 
     * @param array The array to sort.
     * @return A <code>double[]</code>: A copy of the given array, sorted into ascending numerical order and without any
     *         <code>NaN</code> or <code>infinite</code> value. <code>null</code> if the array was <code>null</code>.
     */
    public static double[] getSortedArrayWithoutNaNOrInfiniteValues(double... array) {
        double[] newArray;
        if (array == null) {
            newArray = null;
        } else {
            newArray = new double[array.length];
            int index = 0;
            for (double value : array) {
                if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                    newArray[index++] = value;
                }
            }
            if (index != array.length) {
                newArray = Arrays.copyOf(newArray, index);
            }
            Arrays.sort(newArray);
        }
        return newArray;
    }

    /**
     * Calculates the median value of an array.
     * 
     * @param array The array.
     * @return A <code>double</code>. <code>NaN</code> if the array is <code>null</code> or empty or contains only
     *         <code>NaN</code> and/or <code>infinite</code> values.
     */
    public static double getMedianValue(double... array) {
        return getMedianValueInSortedArray(getSortedArrayWithoutNaNOrInfiniteValues(array));
    }

    /**
     * Calculates the median value of an already sorted array.
     * 
     * @param sortedArray The array. It must be already sorted into ascending numerical order, and must not contain any
     *            <code>NaN</code> or <code>infinite</code> value. You can obtain such an array by calling
     *            {@link #getSortedArrayWithoutNaNOrInfiniteValues(double...)}
     * @return A <code>double</code>. <code>NaN</code> if the array is null or empty.
     */
    public static double getMedianValueInSortedArray(double... sortedArray) {
        double median;
        if ((sortedArray == null) || (sortedArray.length == 0)) {
            median = Double.NaN;
        } else {
            median = getMedianValueInSortedArrayUnchecked(sortedArray);
        }
        return median;
    }

    /**
     * Calculates the median value of an already sorted not <code>null</code> and not empty array.
     * 
     * @param sortedArray The array. It must be already sorted into ascending numerical order, and must not be
     *            <code>null</code> or empty, or contain any <code>NaN</code> or <code>infinite</code> value.
     * @return A <code>double</code>.
     */
    private static double getMedianValueInSortedArrayUnchecked(double... sortedArray) {
        double median;
        int index = sortedArray.length / 2;
        if (sortedArray.length == 1) {
            median = sortedArray[0];
        } else if (sortedArray.length % 2 == 0) {
            median = (sortedArray[index] + sortedArray[index - 1]) / 2;
        } else {
            median = sortedArray[index];
        }
        return median;
    }

    /**
     * Calculates the best position of a value in an array. This array is expected to be sorted, starting at a minimum,
     * with every element separated by a given step.
     * <p>
     * You can create such an array using {@link #buildArray(double, double, int)}
     * </p>
     * 
     * @param value The value
     * @param min The array minimum value
     * @param step The array step
     * @return An <code>int</code>
     */
    public static int getIndexInArray(double value, double min, double step) {
        return (int) Math.min(Integer.MAX_VALUE, Math.round((value - min) / step));
    }

    /**
     * Returns whether an index is in the range of a given array.
     * 
     * @param index The index
     * @param array The array
     * @return A <code>boolean</code>.
     */
    public static boolean isIndexOf(int index, double... array) {
        return (index > -1) && (index < array.length);
    }

    /**
     * Fills an array with the minimum, maximum, mean, sample standard deviation and Poisson distribution standard
     * deviation and sample standard deviation of another array, at given indexes.
     * 
     * @param statistics The array in which to put the mean, standard deviation and sample standard deviation.
     * @param minIndex The index at which to put the minimum. Can be out of range, in which case minimum value won't be
     *            put.
     * @param maxIndex The index at which to put the maximum. Can be out of range, in which case maximum value won't be
     *            put.
     * @param meanIndex The index at which to put the mean. Can be out of range, in which case mean value won't be put.
     * @param stdDevIndex The index at which to put the standard deviation. Can be out of range, in which case standard
     *            deviation won't be put.
     * @param sampleStdDevIndex The index at which to put the sample standard deviation. Can be out of range, in which
     *            case sample standard deviation won't be put.
     * @param poissonStdDevIndex The index at which to put the standard deviation according to Poisson distribution. Can
     *            be out of range, in which case Poisson standard deviation won't be put.
     * @param countIndex The index at which to put the number of effective values (i.e. not <code>NaN</code> or
     *            <code>Infinite</code>). Can be out of range, in which case effective values count won't be put.
     * @param array The array for which to calculate mean, standard deviation and sample standard deviation.
     */
    private static void fillMinMaxMeanStdDev(double[] statistics, int minIndex, int maxIndex, int meanIndex,
            int stdDevIndex, int sampleStdDevIndex, int poissonStdDevIndex, int countIndex, double... array) {
        if (statistics != null) {
            double sum = 0, squareSum = 0, min = Double.POSITIVE_INFINITY, max = Double.NEGATIVE_INFINITY;
            boolean computeMin = isIndexOf(minIndex, statistics), computeMax = isIndexOf(maxIndex, statistics);
            int count = 0;
            if (array != null) {
                for (double value : array) {
                    if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                        sum += value;
                        squareSum += ObjectUtils.square(value);
                        if (computeMin && (value < min)) {
                            min = value;
                        }
                        if (computeMax && (value > max)) {
                            max = value;
                        }
                        count++;
                    }
                }
            }
            if (isIndexOf(countIndex, statistics)) {
                statistics[countIndex] = count;
            }
            if (count == 0) {
                if (computeMin) {
                    statistics[minIndex] = Double.NaN;
                }
                if (computeMax) {
                    statistics[maxIndex] = Double.NaN;
                }
                if (isIndexOf(meanIndex, statistics)) {
                    statistics[meanIndex] = Double.NaN;
                }
                if (isIndexOf(stdDevIndex, statistics)) {
                    statistics[stdDevIndex] = Double.NaN;
                }
                if (isIndexOf(sampleStdDevIndex, statistics)) {
                    statistics[sampleStdDevIndex] = Double.NaN;
                }
                if (isIndexOf(poissonStdDevIndex, statistics)) {
                    statistics[poissonStdDevIndex] = Double.NaN;
                }
            } else if (count == 1) {
                if (computeMin) {
                    statistics[minIndex] = min;
                }
                if (computeMax) {
                    statistics[maxIndex] = max;
                }
                if (isIndexOf(meanIndex, statistics)) {
                    statistics[meanIndex] = sum;
                }
                if (isIndexOf(stdDevIndex, statistics)) {
                    statistics[stdDevIndex] = 0;
                }
                if (isIndexOf(sampleStdDevIndex, statistics)) {
                    statistics[sampleStdDevIndex] = 0;
                }
                if (isIndexOf(poissonStdDevIndex, statistics)) {
                    statistics[poissonStdDevIndex] = Math.sqrt(sum);
                }
            } else {
                if (computeMin) {
                    statistics[minIndex] = min;
                }
                if (computeMax) {
                    statistics[maxIndex] = max;
                }
                if (isIndexOf(meanIndex, statistics)) {
                    statistics[meanIndex] = sum / count;
                }
                if (isIndexOf(stdDevIndex, statistics)) {
                    statistics[stdDevIndex] = Math.sqrt((squareSum / count) - ObjectUtils.square(sum / count));
                }
                if (isIndexOf(sampleStdDevIndex, statistics)) {
                    statistics[sampleStdDevIndex] = Math
                            .sqrt((count / (count - 1.0d)) * ((squareSum / count) - ObjectUtils.square(sum / count)));
                }
                if (isIndexOf(poissonStdDevIndex, statistics)) {
                    statistics[poissonStdDevIndex] = Math.sqrt(sum / ObjectUtils.square(count));
                }
            }
        }
    }

    /**
     * Fills an array with the minimum, maximum, mean, median, standard deviation, sample standard deviation and Poisson
     * distribution standard deviation of another one.
     * 
     * @param statistics The array in which to put the mean, standard deviation and sample standard deviation.
     * @param minIndex The index at which to put the minimum. Can be out of range, in which case minimum value won't be
     *            put.
     * @param maxIndex The index at which to put the maximum. Can be out of range, in which case maximum value won't be
     *            put.
     * @param meanIndex The index at which to put the mean. Can be out of range, in which case mean value won't be put.
     * @param medianIndex The index at which to put the median. Can be out of range, in which case mean value won't be
     *            put.
     * @param stdDevIndex The index at which to put the standard deviation. Can be out of range, in which case standard
     *            deviation won't be put.
     * @param sampleStdDevIndex The index at which to put the sample standard deviation. Can be out of range, in which
     *            case sample standard deviation won't be put.
     * @param poissonStdDevIndex The index at which to put the standard deviation according to Poisson distribution. Can
     *            be out of range, in which case Poisson standard deviation won't be put.
     * @param countIndex The index at which to put the number of effective values (i.e. not <code>NaN</code> or
     *            <code>Infinite</code>). Can be out of range, in which case effective values count won't be put.
     * @param array The array from which to extract statistics.
     */
    public static void fillStatistics(double[] statistics, int minIndex, int maxIndex, int meanIndex, int medianIndex,
            int stdDevIndex, int sampleStdDevIndex, int poissonStdDevIndex, int countIndex, double... array) {
        if (statistics != null) {
            if (isIndexOf(medianIndex, statistics)) {
                array = getSortedArrayWithoutNaNOrInfiniteValues(array);
                statistics[medianIndex] = getMedianValueInSortedArray(array);
                if (array.length > 0) {
                    if (isIndexOf(minIndex, statistics)) {
                        statistics[minIndex] = array[0];
                        minIndex = -1;
                    }
                    if (isIndexOf(maxIndex, statistics)) {
                        statistics[maxIndex] = array[array.length - 1];
                        maxIndex = -1;
                    }
                }
            }
            // min, max, mean, standard deviation, sample standard deviation
            fillMinMaxMeanStdDev(statistics, minIndex, maxIndex, meanIndex, stdDevIndex, sampleStdDevIndex,
                    poissonStdDevIndex, countIndex, array);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private static class MeanAccumulator {
        double sum;
        long count;

        public MeanAccumulator() {
            sum = 0;
            count = 0;
        }

        public void add(double value) {
            if ((!Double.isNaN(value)) && (!Double.isInfinite(value))) {
                sum += value;
                count++;
            }
        }

        public double getSum() {
            return sum;
        }

        public long getCount() {
            return count;
        }
    }

}
