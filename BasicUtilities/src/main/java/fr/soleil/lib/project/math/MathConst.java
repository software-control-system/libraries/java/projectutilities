/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.math;

/**
 * This class gives some Mathematics constants that can be used in other projects
 * 
 * @author huriez, Rapha&euml;l GIRARDOT
 * 
 */
public interface MathConst {

    /**
     * A <code>double</code> <code>NaN</code> constant used to represent a <code>null</code> value.
     * Don't hesitate to use this if you work with primitive types
     */
    public static final double NAN_FOR_NULL = Double.longBitsToDouble(0x7ff0000bad0000ffL);

    /**
     * A <code>double</code> under the absolute value of which double calculations might become imprecise enough to
     * become dangerous (<code>1e<sup>-12</sup></code>).
     */
    public static final double PRECISION_LIMIT = Math.pow(10, -12);

    /**
     * Returns whether a <code>NaN</code> <code>double</code> value is encoded to represent a <code>null</code> value
     * 
     * @param value The <code>double</code> value to test
     * @return A <code>boolean</code> value. <code>TRUE</code> if this value is a <code>NaN</code> encoded to represent
     *         a <code>null</code>, <code>FALSE</code> otherwise
     */
    public static boolean isNaNForNull(double value) {
        return Double.doubleToRawLongBits(NAN_FOR_NULL) == Double.doubleToRawLongBits(value);
    }

}
