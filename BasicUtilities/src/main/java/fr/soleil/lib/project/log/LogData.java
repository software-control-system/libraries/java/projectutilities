/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.log;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A class used to represent a log (like one of sl4j) data.
 * A log always has a timestamp, level and message.
 * <p>
 * As time resolutions may vary from one system to another, timestamps are always represented as strings.
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LogData {

    protected final String timestamp;
    protected final Level level;
    protected final String message;

    /**
     * Constructor. Builds a new {@link LogData} with given timestamp, level and message.
     * 
     * @param timestamp The timestamp.
     * @param level The level.
     * @param message The message.
     */
    public LogData(String timestamp, Level level, String message) {
        super();
        this.timestamp = timestamp;
        this.level = level;
        this.message = message;
    }

    /**
     * Returns this {@link LogData}'s timestamp.
     * 
     * @return A {@link String}.
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * Returns this {@link LogData}'s level.
     * 
     * @return A {@link Level}.
     */
    public Level getLevel() {
        return level;
    }

    /**
     * Returns this {@link LogData}'s message.
     * 
     * @return A {@link String}.
     */
    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            LogData log = (LogData) obj;
            equals = ObjectUtils.sameObject(timestamp, log.timestamp) && ObjectUtils.sameObject(level, log.level)
                    && ObjectUtils.sameObject(message, log.message);
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0x109;
        int mult = 0xDA7A;
        code = code * mult + ObjectUtils.getHashCode(timestamp);
        code = code * mult + ObjectUtils.getHashCode(level);
        code = code * mult + ObjectUtils.getHashCode(message);
        return code;
    }
}
