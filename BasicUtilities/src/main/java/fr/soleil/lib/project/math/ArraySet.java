/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.math;

import java.lang.reflect.Array;

import fr.soleil.lib.project.math.exception.ArraySetException;

/**
 * This class contains a table of T data of undetermined rank. This class is compatible with
 * primitive types
 * 
 * @author rodriguez, huriez, Rapha&euml;l GIRARDOT
 * 
 * @param <T> The type of data in the table
 */
public class ArraySet<T> {

    private final Object array;
    private final Class<T> typeArray;
    private final int rank;
    private final int length;
    private final int[] shape;

    public ArraySet(Object value, Class<T> type) throws ArraySetException {

        if (!value.getClass().isArray()) {
            throw new ArraySetException("value is not an array");
        } else {

            array = value;
            typeArray = type;

            shape = ArrayUtils.recoverShape(value);
            if (shape == null) {
                rank = 0;
                length = 0;
            } else {
                rank = shape.length;
                length = shape[0];
            }
        }
    }

    public ArraySet(Object value, Class<T> type, int rank) throws ArraySetException {

        if (!value.getClass().isArray()) {
            throw new ArraySetException("value is not an array");
        } else {

            array = value;
            typeArray = type;
            this.rank = rank;

            shape = new int[rank];

            Object tempArray = array;

            for (int i = 0; i < rank; ++i) {
                shape[i] = Array.getLength(tempArray);
                tempArray = Array.get(tempArray, 0);
            }

            if (rank > 0) {
                length = shape[0];
            } else {
                length = 0;
            }

        }
    }

    public Object getArray() {
        return array;
    }

    public Class<T> getType() {
        return typeArray;
    }

    public int getRank() {
        return rank;
    }

    public int getLength() {
        return length;
    }

    public int[] getShape() {
        return (shape == null ? null : shape.clone());
    }

    /**
     * Returns, if possible, the flat (i.e. single dimension) representation of this multiple
     * dimension array.
     * 
     * @return A single dimension array of the expected type. Returns <code>null</code> if this {@link ArraySet}'s shape
     *         is null;
     * @throws ArraySetException If this array is too big to be stored in a single dimension array
     */
    public Object getFlatValues() throws ArraySetException {
        int tempLength = 1;
        for (int curLength : shape) {
            tempLength *= curLength;
            if (tempLength < 0) {
                throw new ArraySetException(ArraySetException.DIMENSION_MESSAGE);
            }
        }
        return ArrayUtils.convertArrayDimensionFromNTo1(array);
    }

    public int getHeight() {
        int result = 0;
        result = shape.length;
        return result;
    }

    public static Object getSubElement(Object array) {
        Object result = null;
        if (array != null && array.getClass().isArray()) {
            result = getSubElement(Array.get(array, 0));
        } else {
            result = array;
        }
        return result;
    }
}
