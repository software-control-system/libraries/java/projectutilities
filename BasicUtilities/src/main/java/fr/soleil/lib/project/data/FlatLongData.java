/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.data;

import fr.soleil.lib.project.math.ArrayUtils;

public class FlatLongData extends AFlatData<long[]> {

    public FlatLongData(int[] dataShape, long[] value) {
        super(dataShape, value);
    }

    public long getDataAt(int... position) {
        int[] shape = getRealShape();
        long[] array = getValue();
        long value;
        if ((array != null) && ArrayUtils.isPosition(shape, position)) {
            int index = ArrayUtils.getFlatIndex(position, shape);
            if ((index > -1) && (index < array.length)) {
                value = array[index];
            } else {
                value = 0;
            }
        } else {
            value = 0;
        }
        return value;
    }
}
