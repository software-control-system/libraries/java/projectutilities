/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.resource;

import java.util.Locale;

public class MessageProvider {

    private static MessageManager MESSAGES;
    private static boolean init = false;

    public static void initMessageManager(String resourcePath, Locale currentLocale) {
        if (!init) {
            MESSAGES = new MessageManager(resourcePath);
            if (currentLocale != null) {
                MESSAGES.initResourceBundle(currentLocale);
            }
            init = true;
        }
    }

    public static String getMessage(String key) {
        return MESSAGES.getMessage(key);
    }

    public static boolean isMessagesManagerInitialized() {
        return (init && MESSAGES.isResourceBundleInitialised());
    }

    public static String getAppMessage(String key) {
        return MESSAGES.getAppMessage(key);
    }

    public static String getLogMessage(String key) {
        return MESSAGES.getLogMessage(key);
    }
}
