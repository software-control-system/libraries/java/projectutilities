/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.log;

/**
 * An {@link Enum} to represent log levels from slf4j
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public enum Level {

    // {@link Enum}s are always comparable, using the declaration order as natural order.
    ALL, TRACE, DEBUG, INFO, WARNING, ERROR;

    // In slf4j, "warning" level is named "WARN"
    private static final String WARN = "WARN";

    private final String lowerCaseName;
    private final String upperCaseName;

    private Level() {
        this.upperCaseName = super.toString();
        this.lowerCaseName = upperCaseName.toLowerCase();
    }

    /**
     * Returns the upper case name of the {@link Level}.
     * 
     * @return A {@link String}.
     */
    public String getUpperCaseName() {
        return upperCaseName;
    }

    /**
     * Returns the lower case name of the {@link Level}.
     * 
     * @return A {@link String}.
     */
    public String getLowerCaseName() {
        return lowerCaseName;
    }

    /**
     * Parses a {@link String} and return the matching {@link Level}.
     * 
     * @param value The {@link String} to parse.
     * @return A {@link Level}. Can be <code>null</code>.
     */
    public static Level parseLevel(String value) {
        Level result = null;
        if (value != null) {
            String trimmed = value.trim();
            for (Level lvl : values()) {
                if (trimmed.equalsIgnoreCase(lvl.toString())) {
                    result = lvl;
                    break;
                }
            }
            // In slf4j, "warning" level is named "WARN"
            if ((result == null) && WARN.equalsIgnoreCase(value)) {
                result = Level.WARNING;
            }
        }
        return result;
    }

}
