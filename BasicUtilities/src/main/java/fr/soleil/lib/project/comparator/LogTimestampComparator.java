/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.comparator;

import fr.soleil.lib.project.log.LogData;

/**
 * An {@link ALogStringComparator} that compares log timestamps.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class LogTimestampComparator extends ALogStringComparator {

    public LogTimestampComparator(boolean descending) {
        super(descending);
    }

    @Override
    protected String extractData(LogData log) {
        return log.getTimestamp();
    }

}
