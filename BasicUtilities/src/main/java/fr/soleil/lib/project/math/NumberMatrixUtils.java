/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.math;

import java.lang.reflect.Array;

/**
 * A class that contains some useful methods to interact with number matrix
 * 
 * @author huriez, Rapha&euml;l GIRARDOT
 */
public class NumberMatrixUtils {

    /**
     * Multiplies a {@link Number} matrix with a {@link Number}
     * 
     * @param <T> The type of {@link Number} in the matrix
     * @param matrix The matrix
     * @param toMultiplyWith The {@link Number} to multiply the matrix with
     * @return The resulting matrix, or <code>null</code> if any parameter of this method was <code>null</code>
     */
    @SuppressWarnings("unchecked")
    public static <T extends Number> T[][] multiplyNumberObjectMatrixWithNumber(T[][] matrix, Number toMultiplyWith) {
        // Note: this method could have been done using Array.set/Array.get, but it is less time
        // consuming doing it the way it is written
        T[][] result = null;
        if ((matrix != null) && (toMultiplyWith != null)) {
            T[][] temp = matrix.clone();
            if (matrix instanceof Byte[][]) {
                for (int y = 0; y < matrix.length; y++) {
                    for (int x = 0; x < matrix[y].length; x++) {
                        if (matrix[y][x] != null) {
                            temp[y][x] = (T) Byte
                                    .valueOf((byte) (toMultiplyWith.byteValue() * (matrix[y][x]).byteValue()));
                        }
                    }
                }
            } else if (matrix instanceof Short[][]) {
                for (int y = 0; y < matrix.length; y++) {
                    for (int x = 0; x < matrix[y].length; x++) {
                        if (matrix[y][x] != null) {
                            temp[y][x] = (T) Short
                                    .valueOf((short) (toMultiplyWith.shortValue() * (matrix[y][x]).shortValue()));
                        }
                    }
                }
            } else if (matrix instanceof Integer[][]) {
                for (int y = 0; y < matrix.length; y++) {
                    for (int x = 0; x < matrix[y].length; x++) {
                        if (matrix[y][x] != null) {
                            temp[y][x] = (T) Integer.valueOf(toMultiplyWith.intValue() * (matrix[y][x]).intValue());
                        }
                    }
                }
            } else if (matrix instanceof Long[][]) {
                for (int y = 0; y < matrix.length; y++) {
                    for (int x = 0; x < matrix[y].length; x++) {
                        if (matrix[y][x] != null) {
                            temp[y][x] = (T) Long.valueOf(toMultiplyWith.longValue() * (matrix[y][x]).longValue());
                        }
                    }
                }
            } else if (matrix instanceof Float[][]) {
                for (int y = 0; y < matrix.length; y++) {
                    for (int x = 0; x < matrix[y].length; x++) {
                        if (matrix[y][x] != null) {
                            temp[y][x] = (T) Float.valueOf(toMultiplyWith.floatValue() * (matrix[y][x]).floatValue());
                        }
                    }
                }
            } else {
                for (int y = 0; y < matrix.length; y++) {
                    for (int x = 0; x < matrix[y].length; x++) {
                        if (matrix[y][x] != null) {
                            temp[y][x] = (T) Double
                                    .valueOf(toMultiplyWith.doubleValue() * (matrix[y][x]).doubleValue());
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Multiplies a number (primitive type or {@link Number}) matrix with a {@link Number}
     * 
     * @param <T> The type of number in the matrix
     * @param matrix The matrix
     * @param toMultiplyWith The {@link Number} to multiply the matrix with
     * @param numberType The {@link Class} that represents the type of number in the matrix
     * @return The resulting matrix, or <code>null</code> if any parameter of this method was <code>null</code>, or if
     *         <code>matrix</code> is not compatible with <code>numberType</code>. The resulting matrix is of the same
     *         type as <code>matrix</code>.
     */
    @SuppressWarnings("unchecked")
    public static <T extends Number> Object[] multiplyNumberMatrixWithNumber(Object[] matrix, Number toMultiplyWith,
            Class<T> numberType) {
        // Note: this method could have been done using Array.set/Array.get, but it is less time
        // consuming doing it the way it is written
        Object[] result = null;
        if ((matrix != null) && (toMultiplyWith != null) && (numberType != null)) {
            Object[] classCheck = (Object[]) Array.newInstance(Array.newInstance(numberType, 0).getClass(), 0);
            if (classCheck.getClass().equals(matrix.getClass())) {
                if (Number.class.isAssignableFrom(numberType)) {
                    result = multiplyNumberObjectMatrixWithNumber((T[][]) matrix, toMultiplyWith);
                } else {
                    result = matrix.clone();
                    if (Byte.TYPE.equals(numberType)) {
                        byte[][] value = (byte[][]) result;
                        for (int y = 0; y < value.length; y++) {
                            for (int x = 0; x < value[y].length; x++) {
                                value[y][x] *= toMultiplyWith.byteValue();
                            }
                        }
                    } else if (Short.TYPE.equals(numberType)) {
                        short[][] value = (short[][]) result;
                        for (int y = 0; y < value.length; y++) {
                            for (int x = 0; x < value[y].length; x++) {
                                value[y][x] *= toMultiplyWith.shortValue();
                            }
                        }
                    } else if (Integer.TYPE.equals(numberType)) {
                        int[][] value = (int[][]) result;
                        for (int y = 0; y < value.length; y++) {
                            for (int x = 0; x < value[y].length; x++) {
                                value[y][x] *= toMultiplyWith.intValue();
                            }
                        }
                    } else if (Long.TYPE.equals(numberType)) {
                        long[][] value = (long[][]) result;
                        for (int y = 0; y < value.length; y++) {
                            for (int x = 0; x < value[y].length; x++) {
                                value[y][x] *= toMultiplyWith.longValue();
                            }
                        }
                    } else if (Float.TYPE.equals(numberType)) {
                        float[][] value = (float[][]) result;
                        for (int y = 0; y < value.length; y++) {
                            for (int x = 0; x < value[y].length; x++) {
                                value[y][x] *= toMultiplyWith.floatValue();
                            }
                        }
                    } else {
                        double[][] value = (double[][]) result;
                        for (int y = 0; y < value.length; y++) {
                            for (int x = 0; x < value[y].length; x++) {
                                value[y][x] *= toMultiplyWith.doubleValue();
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    // TODO RG: implement (and rename) following methods coming from AbstractNumberMatrix

    // public static Number[][] supprime_ligne_colonne(Number[][] m, int l, int c) {
    //
    // int i, j, d, e = 0;
    // Number Temp[][] = new Number[m.length - 1][m.length - 1];
    //
    // for (i = 0; i < m.length; i++) {
    // d = 0;
    // if (i != l) {
    // for (j = 0; j < m.length; j++) {
    // if (j != c) {
    // Temp[e][d] = (m[i][j]).doubleValue();
    // d++;
    // }
    // }
    // e++;
    // }
    // }
    // return Temp;
    // }
    //
    // public double[] multiplication_matrice_avec_vecteur(Number v1[]) {
    // Number[][] m_values = getValue();
    // if (m_values == null) {
    // return null;
    // }
    //
    // double temp_vec[] = new double[m_values.length];
    // double temp = 0;
    // for (int i = 0; i < m_values.length; i++) {
    // for (int j = 0; j < m_values.length; j++) {
    // temp = temp + ((m_values[j][i]).doubleValue() * v1[j].doubleValue());
    // }
    // temp_vec[i] = temp;
    // temp = 0;
    // }
    // return (temp_vec);
    // }
    //
    // public void Matrices_fois_Matrices(Number m1[][]) {
    // Number[][] m_values = getValue();
    // if (m_values == null) {
    // return;
    // }
    //
    // int i, j, k;
    // Number temp[][] = initValue(m1.length, m_values[0].length);
    //
    // for (i = 0; i < m1.length; i = i + 1) {
    // for (j = 0; j < m_values[0].length; j = j + 1) {
    // temp[i][j] = 0;
    // for (k = 0; k < m1[0].length; k = k + 1) {
    // temp[i][j] = (temp[i][j]).doubleValue() + (m1[i][k]).doubleValue()
    // * (m_values[k][j]).doubleValue();
    // }
    // }
    // }
    // m_values = temp;
    // }
    //
    // public void delete_line_column(int l, int c) {
    // Number[][] m_values = getValue();
    // if (m_values == null) {
    // return;
    // }
    //
    // m_values = AbstractNumberMatrix.supprime_ligne_colonne(m_values, l, c);
    // }
    //
    // public static Number puissance(int nombre, int p) {
    // double res = nombre;
    // if (p == 0) {
    // res = 1;
    // }
    // else {
    // for (int i = 0; i < p - 1; i++) {
    // res = res * nombre;
    // }
    // }
    // return (res);
    // }
    //
    // public Number cofacteur(int i, int j) {
    // Number[][] m_values = getValue();
    // if (m_values == null) {
    // return 0;
    // }
    // return AbstractNumberMatrix.cofacteur(m_values, i, j);
    // }
    //
    // public static Number cofacteur(Number A[][], int i, int j) {
    // Number res = 0;
    // Number B[][] = new Number[A.length][];
    // B = AbstractNumberMatrix.supprime_ligne_colonne(A, i, j);
    // res = (AbstractNumberMatrix.puissance(-1, (i + j))).doubleValue()
    // * (AbstractNumberMatrix.determinant(B)).doubleValue();
    // return (res);
    // }
    //
    // public Number determinant() {
    // Number[][] m_values = getValue();
    // if (m_values == null) {
    // return 0;
    // }
    //
    // return AbstractNumberMatrix.determinant(m_values);
    // }
    //
    // public static Number determinant(Number A[][]) {
    // double res = 0;
    // if (A.length == 1) {
    // return (A[0][0]);
    // }
    // else {
    // for (int i = 0; i < A.length; i++) {
    // res = res + AbstractNumberMatrix.cofacteur(A, i, 0).doubleValue()
    // * (A[i][0]).doubleValue();
    // }
    // }
    // return (res);
    //
    // }
    //
    // public void matriceInverse() {
    // Number[][] m_values = getValue();
    // if (m_values == null) {
    // return;
    // }
    //
    // Number Temp[][] = initValue(m_values.length, m_values.length);
    // Number Temp1[][] = initValue(m_values.length, m_values.length);
    //
    // Temp = m_values;
    // double d = 0;
    // double determinant = AbstractNumberMatrix.determinant(m_values).doubleValue();
    // if (determinant != 0) {
    // d = 1 / determinant;
    // for (int i = 0; i < m_values.length; i++) {
    // for (int j = 0; j < m_values.length; j++) {
    // Temp1[j][i] = AbstractNumberMatrix.cofacteur(Temp, i, j);
    // }
    // }
    // }
    // else {
    // System.out.println(" Erreur le determinant de la matrice est 0 ");
    // }
    //
    // m_values = Temp1;
    // multiplication_matrice_avec_nombre1(d);
    //
    // }
    //
    // public void transpose_de_la_matrice() {
    // Number[][] m_values = getValue();
    // if (m_values == null) {
    // return;
    // }
    //
    // Number temp2[][] = initValue(m_values.length, m_values.length);
    // for (int i = 0; i < m_values.length; i++) {
    // for (int j = 0; j < m_values.length; j++) {
    // temp2[j][i] = m_values[i][j];
    // }
    // }
    // m_values = temp2;
    // }
    //
    // public boolean equals(AbstractNumberMatrix matrice) {
    // Number[][] m_values = getValue();
    // Number Temp[][] = matrice.getValue();
    // if (Temp == null || m_values == null || m_values.length != Temp.length) {
    // return false;
    // }
    //
    // int i, j = 0;
    //
    // for (i = 0; i < m_values.length; i++) {
    // for (j = 0; j < m_values.length; j++) {
    // if (Temp[i][j] != m_values[i][j]) {
    // return false;
    // }
    // }
    // }
    // return true;
    // }
    //
    // @Override
    // public AbstractNumberMatrix<T> clone() {
    // return (AbstractNumberMatrix<T>) super.clone();
    // }
    //
    // public static void main(String[] args) throws UnsupportedDataTypeException {
    //
    // AbstractNumberMatrix<Number> matrice = new AbstractNumberMatrix<Number>(Number.class) {
    // @Override
    // protected Class<Number> generateDefaultDataType() {
    // return Number.class;
    // }
    // };
    //
    // System.out.println("***** MATRICE => ARRAY*****");
    //
    // Number[][] values = new Number[2][2];
    // values[0] = new Number[] { 1, 2 };
    // values[1] = new Number[] { 3, 4 };
    // matrice.setValue(values);
    // System.out.println(matrice.getHeight());
    // System.out.println(matrice.getWidth());
    // Number[] flatValues = (Number[]) matrice.getFlatValue();
    // for (int i = 0; i < flatValues.length; i++) {
    // System.out.println("flatvalues[" + i + "]=" + flatValues[i]);
    // }
    //
    // System.out.println("***** ARRAY => MATRICE*****");
    // flatValues = new Number[] { 1, 1, 2, 2, 3, 3 };
    // matrice.setFlatValue(flatValues, 3, 2);
    //
    // flatValues = (Number[]) matrice.getFlatValue();
    // for (int i = 0; i < flatValues.length; i++) {
    // System.out.println("flatvalues[" + i + "]=" + flatValues[i]);
    // }
    //
    // }

    /**
     * Transforms any number matrix into a <code>byte</code> matrix
     * 
     * @param array The number matrix
     * @return A <code>byte[][]</code>
     */
    public static byte[][] extractByteMatrix(Object array) {
        byte[][] result = null;
        if (array instanceof byte[][]) {
            result = (byte[][]) array;
        } else if (array instanceof short[][]) {
            short[][] temp = (short[][]) array;
            result = new byte[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractByteArray(temp[i]);
                }
            }
        } else if (array instanceof int[][]) {
            int[][] temp = (int[][]) array;
            result = new byte[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractByteArray(temp[i]);
                }
            }
        } else if (array instanceof long[][]) {
            long[][] temp = (long[][]) array;
            result = new byte[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractByteArray(temp[i]);
                }
            }
        } else if (array instanceof float[][]) {
            float[][] temp = (float[][]) array;
            result = new byte[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractByteArray(temp[i]);
                }
            }
        } else if (array instanceof double[][]) {
            double[][] temp = (double[][]) array;
            result = new byte[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractByteArray(temp[i]);
                }
            }
        } else if (array instanceof Number[][]) {
            Number[][] temp = (Number[][]) array;
            result = new byte[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractByteArray(temp[i]);
                }
            }
        }
        return result;
    }

    /**
     * Transforms any number matrix into a <code>short</code> matrix
     * 
     * @param array The number matrix
     * @return A <code>short[][]</code>
     */
    public static short[][] extractShortMatrix(Object array) {
        short[][] result = null;
        if (array instanceof byte[][]) {
            byte[][] temp = (byte[][]) array;
            result = new short[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractShortArray(temp[i]);
                }
            }
        } else if (array instanceof short[][]) {
            result = (short[][]) array;
        } else if (array instanceof int[][]) {
            int[][] temp = (int[][]) array;
            result = new short[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractShortArray(temp[i]);
                }
            }
        } else if (array instanceof long[][]) {
            long[][] temp = (long[][]) array;
            result = new short[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractShortArray(temp[i]);
                }
            }
        } else if (array instanceof float[][]) {
            float[][] temp = (float[][]) array;
            result = new short[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractShortArray(temp[i]);
                }
            }
        } else if (array instanceof double[][]) {
            double[][] temp = (double[][]) array;
            result = new short[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractShortArray(temp[i]);
                }
            }
        } else if (array instanceof Number[][]) {
            Number[][] temp = (Number[][]) array;
            result = new short[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractShortArray(temp[i]);
                }
            }
        }
        return result;
    }

    /**
     * Transforms any number matrix into an <code>int</code> matrix
     * 
     * @param array The number matrix
     * @return A <code>int[][]</code>
     */
    public static int[][] extractIntMatrix(Object array) {
        int[][] result = null;
        if (array instanceof byte[][]) {
            byte[][] temp = (byte[][]) array;
            result = new int[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractIntArray(temp[i]);
                }
            }
        } else if (array instanceof short[][]) {
            short[][] temp = (short[][]) array;
            result = new int[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractIntArray(temp[i]);
                }
            }
        } else if (array instanceof int[][]) {
            result = (int[][]) array;
        } else if (array instanceof long[][]) {
            long[][] temp = (long[][]) array;
            result = new int[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractIntArray(temp[i]);
                }
            }
        } else if (array instanceof float[][]) {
            float[][] temp = (float[][]) array;
            result = new int[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractIntArray(temp[i]);
                }
            }
        } else if (array instanceof double[][]) {
            double[][] temp = (double[][]) array;
            result = new int[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractIntArray(temp[i]);
                }
            }
        } else if (array instanceof Number[][]) {
            Number[][] temp = (Number[][]) array;
            result = new int[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractIntArray(temp[i]);
                }
            }
        }
        return result;
    }

    /**
     * Transforms any number matrix into a <code>long</code> matrix
     * 
     * @param array The number matrix
     * @return A <code>long[][]</code>
     */
    public static long[][] extractLongMatrix(Object array) {
        long[][] result = null;
        if (array instanceof byte[][]) {
            byte[][] temp = (byte[][]) array;
            result = new long[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractLongArray(temp[i]);
                }
            }
        } else if (array instanceof short[][]) {
            short[][] temp = (short[][]) array;
            result = new long[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractLongArray(temp[i]);
                }
            }
        } else if (array instanceof int[][]) {
            int[][] temp = (int[][]) array;
            result = new long[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractLongArray(temp[i]);
                }
            }
        } else if (array instanceof long[][]) {
            result = (long[][]) array;
        } else if (array instanceof float[][]) {
            float[][] temp = (float[][]) array;
            result = new long[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractLongArray(temp[i]);
                }
            }
        } else if (array instanceof double[][]) {
            double[][] temp = (double[][]) array;
            result = new long[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractLongArray(temp[i]);
                }
            }
        } else if (array instanceof Number[][]) {
            Number[][] temp = (Number[][]) array;
            result = new long[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractLongArray(temp[i]);
                }
            }
        }
        return result;
    }

    /**
     * Transforms any number matrix into a <code>float</code> matrix
     * 
     * @param array The number matrix
     * @return A <code>float[][]</code>
     */
    public static float[][] extractFloatMatrix(Object array) {
        float[][] result = null;
        if (array instanceof byte[][]) {
            byte[][] temp = (byte[][]) array;
            result = new float[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractFloatArray(temp[i]);
                }
            }
        } else if (array instanceof short[][]) {
            short[][] temp = (short[][]) array;
            result = new float[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractFloatArray(temp[i]);
                }
            }
        } else if (array instanceof int[][]) {
            int[][] temp = (int[][]) array;
            result = new float[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractFloatArray(temp[i]);
                }
            }
        } else if (array instanceof long[][]) {
            long[][] temp = (long[][]) array;
            result = new float[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractFloatArray(temp[i]);
                }
            }
        } else if (array instanceof float[][]) {
            result = (float[][]) array;
        } else if (array instanceof double[][]) {
            double[][] temp = (double[][]) array;
            result = new float[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractFloatArray(temp[i]);
                }
            }
        } else if (array instanceof Number[][]) {
            Number[][] temp = (Number[][]) array;
            result = new float[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractFloatArray(temp[i]);
                }
            }
        }
        return result;
    }

    /**
     * Transforms any number matrix into a <code>double</code> matrix
     * 
     * @param array The number matrix
     * @return A <code>double[][]</code>
     */
    public static double[][] extractDoubleMatrix(Object array) {
        double[][] result = null;
        if (array instanceof byte[][]) {
            byte[][] temp = (byte[][]) array;
            result = new double[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractDoubleArray(temp[i]);
                }
            }
        } else if (array instanceof short[][]) {
            short[][] temp = (short[][]) array;
            result = new double[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractDoubleArray(temp[i]);
                }
            }
        } else if (array instanceof int[][]) {
            int[][] temp = (int[][]) array;
            result = new double[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractDoubleArray(temp[i]);
                }
            }
        } else if (array instanceof long[][]) {
            long[][] temp = (long[][]) array;
            result = new double[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractDoubleArray(temp[i]);
                }
            }
        } else if (array instanceof float[][]) {
            float[][] temp = (float[][]) array;
            result = new double[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractDoubleArray(temp[i]);
                }
            }
        } else if (array instanceof double[][]) {
            result = (double[][]) array;
        } else if (array instanceof Number[][]) {
            Number[][] temp = (Number[][]) array;
            result = new double[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractDoubleArray(temp[i]);
                }
            }
        }
        return result;
    }

    /**
     * Transforms any number matrix into a {@link Number} matrix
     * 
     * @param array The number matrix
     * @return A {@link Number}<code>[][]</code>
     */
    public static Number[][] extractNumberMatrix(Object array) {
        Number[][] result = null;
        if (array instanceof byte[][]) {
            byte[][] temp = (byte[][]) array;
            result = new Number[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractNumberArray(temp[i]);
                }
            }
        } else if (array instanceof short[][]) {
            short[][] temp = (short[][]) array;
            result = new Number[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractNumberArray(temp[i]);
                }
            }
        } else if (array instanceof int[][]) {
            int[][] temp = (int[][]) array;
            result = new Number[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractNumberArray(temp[i]);
                }
            }
        } else if (array instanceof long[][]) {
            long[][] temp = (long[][]) array;
            result = new Number[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractNumberArray(temp[i]);
                }
            }
        } else if (array instanceof float[][]) {
            float[][] temp = (float[][]) array;
            result = new Number[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractNumberArray(temp[i]);
                }
            }
        } else if (array instanceof double[][]) {
            double[][] temp = (double[][]) array;
            result = new Number[temp.length][];
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    result[i] = NumberArrayUtils.extractNumberArray(temp[i]);
                }
            }
        } else if (array instanceof Number[][]) {
            result = (Number[][]) array;
        }
        return result;
    }

}
