/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class used to make regular expression
 * 
 * @author huriez
 */
public class RegexUtils {

    /**
     * Count the number of instance of "regex" into the {@link String} "text".
     * 
     * @param text the original {@link String} where you want to count the model
     * @param regex the pattern we have to count
     * @return the final count for the specified pattern
     */
    public static final int regexOccur(String text, String regex) {
        Matcher matcher = Pattern.compile(regex).matcher(text);
        int occur = 0;
        while (matcher.find()) {
            occur++;
        }
        return occur;
    }
}
