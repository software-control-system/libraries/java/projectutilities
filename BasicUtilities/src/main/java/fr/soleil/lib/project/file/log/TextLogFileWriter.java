/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.file.log;

import fr.soleil.lib.project.log.LogData;
import fr.soleil.lib.project.resource.MessageManager;

public class TextLogFileWriter extends ALogTextFileWriter {

    public static final String TXT = "txt";
    public static final String LOG = "log";

    protected static final String[] EXTENSIONS = { LOG, TXT };

    public TextLogFileWriter(MessageManager messageManager) {
        super(messageManager);
    }

    @Override
    public String[] getManagedFileExtensions() {
        return EXTENSIONS;
    }

    @Override
    protected StringBuilder appendLogsToStringBuilder(StringBuilder builder, LogData... logs) {
        boolean added = false;
        for (LogData log : logs) {
            if (log != null) {
                if (added) {
                    builder.append('\n');
                } else {
                    added = true;
                }
                builder.append(log.getTimestamp()).append('\t').append('[').append(log.getLevel()).append(']')
                        .append('\t').append(log.getMessage());
            }
        }
        return builder;
    }

}
