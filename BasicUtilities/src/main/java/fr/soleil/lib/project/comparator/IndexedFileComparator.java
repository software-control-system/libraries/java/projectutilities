/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.comparator;

import java.io.File;
import java.text.Collator;
import java.util.Comparator;

import fr.soleil.lib.project.IndexedName;

/**
 * A {@link Comparator} that compares {@link File}s which name ends with an index.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class IndexedFileComparator implements Comparator<File> {

    private static final Character POINT = Character.valueOf('.');
    private static final IndexedFileComparator INSTANCE = new IndexedFileComparator();

    public static IndexedFileComparator getInstance() {
        return INSTANCE;
    }

    public IndexedFileComparator() {
        super();
    }

    /**
     * Compares two files according to their names.
     * <p>
     * If <code>f1</code> and <code>f2</code> are files (i.e. not directories) and their names end with digits:
     * <ul>
     * <li>extract digits</li>
     * <li>compare parts before digits</li>
     * <li>if those parts are equal, compare the numbers represented by the digits</li>
     * </ul>
     * otherwise, only compares file names, ignoring potential digits.
     * </p>
     * 
     * @param f1 The first file to be compared.
     * @param f2 The second file to be compared.
     * @return A negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater
     *         than the second.
     */
    @Override
    public int compare(File f1, File f2) {
        String name1, name2;
        IndexedName indexedName1, indexedName2;
        if (f1 == null) {
            name1 = null;
            indexedName1 = null;
        } else {
            name1 = f1.getName();
            if (f1.isDirectory()) {
                indexedName1 = null;
            } else {
                indexedName1 = IndexedName.buildIndexedName(name1, POINT);
            }
        }
        if (f2 == null) {
            name2 = null;
            indexedName2 = null;
        } else {
            name2 = f2.getName();
            if (f2.isDirectory()) {
                indexedName2 = null;
            } else {
                indexedName2 = IndexedName.buildIndexedName(name2, POINT);
            }
        }
        int comp;
        if ((indexedName1 != null) && (indexedName2 != null)) {
            comp = IndexedNameComparator.getInstance().compare(indexedName1, indexedName2);
        } else {
            comp = Collator.getInstance().compare(name1, name2);
        }
        return comp;
    }

}
