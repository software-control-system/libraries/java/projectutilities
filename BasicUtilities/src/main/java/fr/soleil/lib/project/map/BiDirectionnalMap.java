/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.map;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.lib.project.ObjectUtils;

/**
 * Class based on two Maps. Allows to get a value from a key, but also to get a key from a value.
 * Therefore one must be careful not to have a value already assigned to another key.
 * 
 * @author Julien CORCESSIN
 */
public class BiDirectionnalMap<K, V> {

    // ConcurrentHashMap are used for thread safety in getters,
    // but synchronized blocs are used in setters to ensure consistency

    private final Map<K, V> mainMap;
    private final Map<V, K> reverseMap;

    /*
     * Constructors
     */
    public BiDirectionnalMap() {
        mainMap = new ConcurrentHashMap<K, V>();
        reverseMap = new ConcurrentHashMap<V, K>();
    }

    public BiDirectionnalMap(Map<? extends K, ? extends V> map) {
        this();
        putAllNoLock(map);
    }

    public BiDirectionnalMap(int initialCapacity) {
        mainMap = new ConcurrentHashMap<K, V>(initialCapacity);
        reverseMap = new ConcurrentHashMap<V, K>(initialCapacity);
    }

    public BiDirectionnalMap(int initialCapacity, float loadFactor) {
        mainMap = new ConcurrentHashMap<K, V>(initialCapacity, loadFactor);
        reverseMap = new ConcurrentHashMap<V, K>(initialCapacity, loadFactor);
    }

    /*
     * Methods
     */
    public synchronized V put(K key, V value) {
        reverseMap.put(value, key);
        return mainMap.put(key, value);
    }

    public synchronized K putKey(V value, K key) {
        mainMap.put(key, value);
        return reverseMap.put(value, key);
    }

    public V get(K key) {
        return mainMap.get(key);
    }

    public K getKey(V value) {
        return reverseMap.get(value);
    }

    public Set<Entry<K, V>> entrySet() {
        return mainMap.entrySet();
    }

    public synchronized void clear() {
        mainMap.clear();
        reverseMap.clear();
    }

    public synchronized V remove(K key) {
        return removeNoLock(key, mainMap, reverseMap);
    }

    public synchronized K removeFromValue(V value) {
        return removeNoLock(value, reverseMap, mainMap);
    }

    protected <KEY, VALUE> VALUE removeNoLock(KEY key, Map<KEY, VALUE> keyMap, Map<VALUE, KEY> valueMap) {
        VALUE result = keyMap.remove(key);
        KEY tmp = valueMap.get(result);
        if (ObjectUtils.sameObject(key, tmp)) {
            valueMap.remove(result);
        }
        return result;
    }

    public boolean isEmpty() {
        return mainMap.isEmpty() && reverseMap.isEmpty();
    }

    public synchronized void putAll(Map<? extends K, ? extends V> map) {
        putAllNoLock(map);
    }

    protected void putAllNoLock(Map<? extends K, ? extends V> map) {
        if (map != null) {
            for (Entry<? extends K, ? extends V> entry : map.entrySet()) {
                K key = entry.getKey();
                V value = entry.getValue();
                mainMap.put(key, value);
                reverseMap.put(value, key);
            }
        }
    }

    public Collection<K> keys() {
        return reverseMap.values();
    }

    public Collection<V> values() {
        return mainMap.values();
    }

    public Set<K> keySet() {
        return mainMap.keySet();
    }

    public Set<V> valueSet() {
        return reverseMap.keySet();
    }

    public int size() {
        return mainMap.size();
    }
}
