/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.data;

import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.MathConst;

public class FlatDoubleData extends AFlatData<double[]> {

    public FlatDoubleData(int[] dataShape, double[] value) {
        super(dataShape, value);
    }

    public double getDataAt(int... position) {
        int[] shape = getRealShape();
        double[] array = getValue();
        double value;
        if ((array != null) && ArrayUtils.isPosition(shape, position)) {
            int index = ArrayUtils.getFlatIndex(position, shape);
            if ((index > -1) && (index < array.length)) {
                value = array[index];
            } else {
                value = MathConst.NAN_FOR_NULL;
            }
        } else {
            value = MathConst.NAN_FOR_NULL;
        }
        return value;
    }
}
