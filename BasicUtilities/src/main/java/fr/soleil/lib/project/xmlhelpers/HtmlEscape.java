/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.xmlhelpers;

import fr.soleil.lib.project.ObjectUtils;

/**
 * HtmlEscape in Java, which is compatible with utf-8
 * 
 * @author Ulrich Jensen, http://www.htmlescape.net Feel free to get inspired, use or steal this
 *         code and use it in your own projects. License: You have the right to use this code in
 *         your own project or publish it on your own website. If you are going to use this code,
 *         please include the author lines. Use this code at your own risk. The author does not
 *         warrent or assume any legal liability or responsibility for the accuracy, completeness or
 *         usefullness of this program code.
 * @author Rapha&euml;l GIRARDOT (code adapted to use constants, separate methods)
 */
public class HtmlEscape {

    private static final char[] HEX = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
            'f' };
    private static final String HTML_NEW_LINE = "<br />";
    private static final String HTML_NEW_LINE_TRIM = "<br/>";
    private static final String LESS_THAN = "<";
    private static final String HTML_LESS_THAN = "&lt;";
    private static final String GREATER_THAN = ">";
    private static final String HTML_GREATER_THAN = "&gt;";
    private static final String QUOT = "\"";
    private static final String HTML_QUOT = "&quot;";
    private static final String AND = "&";
    private static final String HTML_AND = "&amp;";
    private static final String CAPITAL_AE = "Æ";
    private static final String HTML_CAPITAL_AE = "&AElig;";
    private static final String CAPITAL_A_ACUTE = "Á";
    private static final String HTML_CAPITAL_A_ACUTE = "&Aacute;";
    private static final String CAPITAL_A_CIRC = "Â";
    private static final String HTML_CAPITAL_A_CIRC = "&Acirc;";
    private static final String CAPITAL_A_GRAVE = "À";
    private static final String HTML_CAPITAL_A_GRAVE = "&Agrave;";
    private static final String CAPITAL_A_RING = "Å";
    private static final String HTML_CAPITAL_A_RING = "&Aring;";
    private static final String CAPITAL_A_TILDE = "Ã";
    private static final String HTML_CAPITAL_A_TILDE = "&Atilde;";
    private static final String CAPITAL_A_UML = "Ä";
    private static final String HTML_CAPITAL_A_UML = "&Auml;";
    private static final String CAPITAL_C_CEDIL = "Ç";
    private static final String HTML_CAPITAL_C_CEDIL = "&Ccedil;";
    private static final String CAPITAL_ETH = "Ð";
    private static final String HTML_CAPITAL_ETH = "&ETH;";
    private static final String CAPITAL_E_ACUTE = "É";
    private static final String HTML_CAPITAL_E_ACUTE = "&Eacute;";
    private static final String CAPITAL_E_CIRC = "Ê";
    private static final String HTML_CAPITAL_E_CIRC = "&Ecirc;";
    private static final String CAPITAL_E_GRAVE = "È";
    private static final String HTML_CAPITAL_E_GRAVE = "&Egrave;";
    private static final String CAPITAL_E_UML = "Ë";
    private static final String HTML_CAPITAL_E_UML = "&Euml;";
    private static final String CAPITAL_I_ACUTE = "Í";
    private static final String HTML_CAPITAL_I_ACUTE = "&Iacute;";
    private static final String CAPITAL_I_CIRC = "Î";
    private static final String HTML_CAPITAL_I_CIRC = "&Icirc;";
    private static final String CAPITAL_I_GRAVE = "Ì";
    private static final String HTML_CAPITAL_I_GRAVE = "&Igrave;";
    private static final String CAPITAL_I_UML = "Ï";
    private static final String HTML_CAPITAL_I_UML = "&Iuml;";
    private static final String CAPITAL_N_TILDE = "Ñ";
    private static final String HTML_CAPITAL_N_TILDE = "&Ntilde;";
    private static final String CAPITAL_O_ACUTE = "Ó";
    private static final String HTML_CAPITAL_O_ACUTE = "&Oacute;";
    private static final String CAPITAL_O_CIRC = "Ô";
    private static final String HTML_CAPITAL_O_CIRC = "&Ocirc;";
    private static final String CAPITAL_O_GRAVE = "Ò";
    private static final String HTML_CAPITAL_O_GRAVE = "&Ograve;";
    private static final String CAPITAL_O_SLASH = "Ø";
    private static final String HTML_CAPITAL_O_SLASH = "&Oslash;";
    private static final String CAPITAL_O_TILDE = "Õ";
    private static final String HTML_CAPITAL_O_TILDE = "&Otilde;";
    private static final String CAPITAL_O_UML = "Ö";
    private static final String HTML_CAPITAL_O_UML = "&Ouml;";
    private static final String CAPITAL_THORN = "Þ";
    private static final String HTML_CAPITAL_THORN = "&THORN;";
    private static final String CAPITAL_U_ACUTE = "Ú";
    private static final String HTML_CAPITAL_U_ACUTE = "&Uacute;";
    private static final String CAPITAL_U_CIRC = "Û";
    private static final String HTML_CAPITAL_U_CIRC = "&Ucirc;";
    private static final String CAPITAL_U_GRAVE = "Ù";
    private static final String HTML_CAPITAL_U_GRAVE = "&Ugrave;";
    private static final String CAPITAL_U_UML = "Ü";
    private static final String HTML_CAPITAL_U_UML = "&Uuml;";
    private static final String CAPITAL_Y_ACUTE = "Ý";
    private static final String HTML_CAPITAL_Y_ACUTE = "&Yacute;";
    private static final String A_ACUTE = "á";
    private static final String HTML_A_ACUTE = "&aacute;";
    private static final String A_CIRC = "â";
    private static final String HTML_A_CIRC = "&acirc;";
    private static final String AE = "æ";
    private static final String HTML_AE = "&aelig;";
    private static final String A_GRAVE = "à";
    private static final String HTML_A_GRAVE = "&agrave;";
    private static final String A_RING = "å";
    private static final String HTML_A_RING = "&aring;";
    private static final String A_TILDE = "ã";
    private static final String HTML_A_TILDE = "&atilde;";
    private static final String A_UML = "ä";
    private static final String HTML_A_UML = "&auml;";
    private static final String C_CEDIL = "ç";
    private static final String HTML_C_CEDIL = "&ccedil;";
    private static final String E_ACUTE = "é";
    private static final String HTML_E_ACUTE = "&eacute;";
    private static final String E_CIRC = "ê";
    private static final String HTML_E_CIRC = "&ecirc;";
    private static final String E_GRAVE = "è";
    private static final String HTML_E_GRAVE = "&egrave;";
    private static final String ETH = "ð";
    private static final String HTML_ETH = "&eth;";
    private static final String E_UML = "ë";
    private static final String HTML_E_UML = "&euml;";
    private static final String I_ACUTE = "í";
    private static final String HTML_I_ACUTE = "&iacute;";
    private static final String I_CIRC = "î";
    private static final String HTML_I_CIRC = "&icirc;";
    private static final String I_GRAVE = "ì";
    private static final String HTML_I_GRAVE = "&igrave;";
    private static final String I_UML = "ï";
    private static final String HTML_I_UML = "&iuml;";
    private static final String N_TILDE = "ñ";
    private static final String HTML_N_TILDE = "&ntilde;";
    private static final String O_ACUTE = "ó";
    private static final String HTML_O_ACUTE = "&oacute;";
    private static final String O_CIRC = "ô";
    private static final String HTML_O_CIRC = "&ocirc;";
    private static final String O_GRAVE = "ò";
    private static final String HTML_O_GRAVE = "&ograve;";
    private static final String O_SLASH = "ø";
    private static final String HTML_O_SLASH = "&oslash;";
    private static final String O_TILDE = "õ";
    private static final String HTML_O_TILDE = "&otilde;";
    private static final String O_UML = "ö";
    private static final String HTML_O_UML = "&ouml;";
    private static final String SZLIG = "ß";
    private static final String HTML_SZLIG = "&szlig;";
    private static final String THORN = "þ";
    private static final String HTML_THORN = "&thorn;";
    private static final String U_ACUTE = "ú";
    private static final String HTML_U_ACUTE = "&uacute;";
    private static final String U_CIRC = "û";
    private static final String HTML_U_CIRC = "&ucirc;";
    private static final String U_GRAVE = "ù";
    private static final String HTML_U_GRAVE = "&ugrave;";
    private static final String U_UML = "ü";
    private static final String HTML_U_UML = "&uuml;";
    private static final String Y_ACUTE = "ý";
    private static final String HTML_Y_ACUTE = "&yacute;";
    private static final String Y_UML = "ÿ";
    private static final String HTML_Y_UML = "&yuml;";
    private static final String CENT = "¢";
    private static final String HTML_CENT = "&cent;";
    private static final String HTML_HEX_START = "&#x";
    private static final String HTML_ESCAPE_END = ";";

    /**
     * Escape function, for Html escaping {@link String}s
     * 
     * @param original The original {@link String}
     * @param escapeBr Whether to escape line feeds
     * @param escapeSpecial Whether to escape special characters (accents and so on)
     * @return The escape {@link String}
     */
    public static String escape(String original, boolean escapeBr, boolean escapeSpecial) {
        String result = escapeTags(original);
        if (escapeBr) {
            result = escapeBr(result);
        }
        if (escapeSpecial) {
            result = escapeSpecial(result);
        }
        return result;
    }

    /**
     * Html revert escaping function
     * 
     * @param original The html escaped {@link String}
     * @param unEscapeBr Whether to unEscape line feeds
     * @param unEscapeSpecial Whether to unEscape special characters (accents and so on)
     * @return A {@link String}
     */
    public static String unEscape(String original, boolean unEscapeBr, boolean unEscapeSpecial) {
        String result = unEscapeTags(original);
        if (unEscapeBr) {
            result = unEscapeBr(result);
        }
        if (unEscapeSpecial) {
            result = unEscapeSpecial(result);
        }
        return result;
    }

    private static String escapeTags(String original) {
        StringBuilder out = new StringBuilder();
        if (original != null) {
            char[] chars = original.toCharArray();
            for (char toCheck : chars) {
                boolean found = true;
                switch (toCheck) {
                    case 60:
                        out.append(HTML_LESS_THAN);
                        break; // <
                    case 62:
                        out.append(HTML_GREATER_THAN);
                        break; // >
                    case 34:
                        out.append(HTML_QUOT);
                        break; // "
                    default:
                        found = false;
                        break;
                }
                if (!found) {
                    out.append(toCheck);
                }
            }
        }
        return out.toString();

    }

    private static String unEscapeTags(String original) {
        String result = original;
        if (result == null) {
            result = ObjectUtils.EMPTY_STRING;
        } else {
            result = result.replace(HTML_LESS_THAN, LESS_THAN).replace(HTML_GREATER_THAN, GREATER_THAN)
                    .replace(HTML_QUOT, QUOT);
        }
        return result;

    }

    private static String escapeBr(String original) {
        StringBuilder out = new StringBuilder();
        if (original != null) {
            char[] chars = original.toCharArray();
            for (char toCheck : chars) {
                boolean found = true;
                switch (toCheck) {
                    case '\n':
                        out.append(HTML_NEW_LINE);
                        break; // newline
                    case '\r':
                        break;
                    default:
                        found = false;
                        break;
                }
                if (!found) {
                    out.append(toCheck);
                }
            }
        }
        return out.toString();
    }

    private static String unEscapeBr(String original) {
        String result = original;
        if (result == null) {
            result = ObjectUtils.EMPTY_STRING;
        } else {
            result = result.replace(HTML_NEW_LINE, ObjectUtils.NEW_LINE);
            int index = result.indexOf(HTML_NEW_LINE_TRIM);
            if (index > -1) {
                result = result.replace(HTML_NEW_LINE_TRIM, ObjectUtils.NEW_LINE);
            }
        }
        return result;
    }

    private static String escapeSpecial(String original) {
        StringBuilder out = new StringBuilder();
        if (original != null) {
            char[] chars = original.toCharArray();
            for (char toCheck : chars) {
                boolean found = true;
                switch (toCheck) {
                    case 38:
                        out.append(HTML_AND);
                        break; // &
                    case 198:
                        out.append(HTML_CAPITAL_AE);
                        break; // Æ
                    case 193:
                        out.append(HTML_CAPITAL_A_ACUTE);
                        break; // Á
                    case 194:
                        out.append(HTML_CAPITAL_A_CIRC);
                        break; // Â
                    case 192:
                        out.append(HTML_CAPITAL_A_GRAVE);
                        break; // À
                    case 197:
                        out.append(HTML_CAPITAL_A_RING);
                        break; // Å
                    case 195:
                        out.append(HTML_CAPITAL_A_TILDE);
                        break; // Ã
                    case 196:
                        out.append(HTML_CAPITAL_A_UML);
                        break; // Ä
                    case 199:
                        out.append(HTML_CAPITAL_C_CEDIL);
                        break; // Ç
                    case 208:
                        out.append(HTML_CAPITAL_ETH);
                        break; // Ð
                    case 201:
                        out.append(HTML_CAPITAL_E_ACUTE);
                        break; // É
                    case 202:
                        out.append(HTML_CAPITAL_E_CIRC);
                        break; // Ê
                    case 200:
                        out.append(HTML_CAPITAL_E_GRAVE);
                        break; // È
                    case 203:
                        out.append(HTML_CAPITAL_E_UML);
                        break; // Ë
                    case 205:
                        out.append(HTML_CAPITAL_I_ACUTE);
                        break; // Í
                    case 206:
                        out.append(HTML_CAPITAL_I_CIRC);
                        break; // Î
                    case 204:
                        out.append(HTML_CAPITAL_I_GRAVE);
                        break; // Ì
                    case 207:
                        out.append(HTML_CAPITAL_I_UML);
                        break; // Ï
                    case 209:
                        out.append(HTML_CAPITAL_N_TILDE);
                        break; // Ñ
                    case 211:
                        out.append(HTML_CAPITAL_O_ACUTE);
                        break; // Ó
                    case 212:
                        out.append(HTML_CAPITAL_O_CIRC);
                        break; // Ô
                    case 210:
                        out.append(HTML_CAPITAL_O_GRAVE);
                        break; // Ò
                    case 216:
                        out.append(HTML_CAPITAL_O_SLASH);
                        break; // Ø
                    case 213:
                        out.append(HTML_CAPITAL_O_TILDE);
                        break; // Õ
                    case 214:
                        out.append(HTML_CAPITAL_O_UML);
                        break; // Ö
                    case 222:
                        out.append(HTML_CAPITAL_THORN);
                        break; // Þ
                    case 218:
                        out.append(HTML_CAPITAL_U_ACUTE);
                        break; // Ú
                    case 219:
                        out.append(HTML_CAPITAL_U_CIRC);
                        break; // Û
                    case 217:
                        out.append(HTML_CAPITAL_U_GRAVE);
                        break; // Ù
                    case 220:
                        out.append(HTML_CAPITAL_U_UML);
                        break; // Ü
                    case 221:
                        out.append(HTML_CAPITAL_Y_ACUTE);
                        break; // Ý
                    case 225:
                        out.append(HTML_A_ACUTE);
                        break; // á
                    case 226:
                        out.append(HTML_A_CIRC);
                        break; // â
                    case 230:
                        out.append(HTML_AE);
                        break; // æ
                    case 224:
                        out.append(HTML_A_GRAVE);
                        break; // à
                    case 229:
                        out.append(HTML_A_RING);
                        break; // å
                    case 227:
                        out.append(HTML_A_TILDE);
                        break; // ã
                    case 228:
                        out.append(HTML_A_UML);
                        break; // ä
                    case 231:
                        out.append(HTML_C_CEDIL);
                        break; // ç
                    case 233:
                        out.append(HTML_E_ACUTE);
                        break; // é
                    case 234:
                        out.append(HTML_E_CIRC);
                        break; // ê
                    case 232:
                        out.append(HTML_E_GRAVE);
                        break; // è
                    case 240:
                        out.append(HTML_ETH);
                        break; // ð
                    case 235:
                        out.append(HTML_E_UML);
                        break; // ë
                    case 237:
                        out.append(HTML_I_ACUTE);
                        break; // í
                    case 238:
                        out.append(HTML_I_CIRC);
                        break; // î
                    case 236:
                        out.append(HTML_I_GRAVE);
                        break; // ì
                    case 239:
                        out.append(HTML_I_UML);
                        break; // ï
                    case 241:
                        out.append(HTML_N_TILDE);
                        break; // ñ
                    case 243:
                        out.append(HTML_O_ACUTE);
                        break; // ó
                    case 244:
                        out.append(HTML_O_CIRC);
                        break; // ô
                    case 242:
                        out.append(HTML_O_GRAVE);
                        break; // ò
                    case 248:
                        out.append(HTML_O_SLASH);
                        break; // ø
                    case 245:
                        out.append(HTML_O_TILDE);
                        break; // õ
                    case 246:
                        out.append(HTML_O_UML);
                        break; // ö
                    case 223:
                        out.append(HTML_SZLIG);
                        break; // ß
                    case 254:
                        out.append(HTML_THORN);
                        break; // þ
                    case 250:
                        out.append(HTML_U_ACUTE);
                        break; // ú
                    case 251:
                        out.append(HTML_U_CIRC);
                        break; // û
                    case 249:
                        out.append(HTML_U_GRAVE);
                        break; // ù
                    case 252:
                        out.append(HTML_U_UML);
                        break; // ü
                    case 253:
                        out.append(HTML_Y_ACUTE);
                        break; // ý
                    case 255:
                        out.append(HTML_Y_UML);
                        break; // ÿ
                    case 162:
                        out.append(HTML_CENT);
                        break; // ¢
                    default:
                        found = false;
                        break;
                }
                if (!found) {
                    if (toCheck > 127) {
                        char c = toCheck;
                        int a4 = c % 16;
                        c = (char) (c / 16);
                        int a3 = c % 16;
                        c = (char) (c / 16);
                        int a2 = c % 16;
                        c = (char) (c / 16);
                        int a1 = c % 16;
                        out.append(HTML_HEX_START + HEX[a1] + HEX[a2] + HEX[a3] + HEX[a4] + HTML_ESCAPE_END);
                    } else {
                        out.append(toCheck);
                    }
                }
            }
        }
        return out.toString();
    }

    private static String unEscapeSpecial(String original) {
        String result = original;
        if (result == null) {
            result = ObjectUtils.EMPTY_STRING;
        } else {
            result = result.replace(HTML_AND, AND).replace(HTML_CAPITAL_AE, CAPITAL_AE)
                    .replace(HTML_CAPITAL_A_ACUTE, CAPITAL_A_ACUTE).replace(HTML_CAPITAL_A_CIRC, CAPITAL_A_CIRC)
                    .replace(HTML_CAPITAL_A_GRAVE, CAPITAL_A_GRAVE).replace(HTML_CAPITAL_A_RING, CAPITAL_A_RING)
                    .replace(HTML_CAPITAL_A_TILDE, CAPITAL_A_TILDE).replace(HTML_CAPITAL_A_UML, CAPITAL_A_UML)
                    .replace(HTML_CAPITAL_C_CEDIL, CAPITAL_C_CEDIL).replace(HTML_CAPITAL_ETH, CAPITAL_ETH)
                    .replace(HTML_CAPITAL_E_ACUTE, CAPITAL_E_ACUTE).replace(HTML_CAPITAL_E_CIRC, CAPITAL_E_CIRC)
                    .replace(HTML_CAPITAL_E_GRAVE, CAPITAL_E_GRAVE).replace(HTML_CAPITAL_E_UML, CAPITAL_E_UML)
                    .replace(HTML_CAPITAL_I_ACUTE, CAPITAL_I_ACUTE).replace(HTML_CAPITAL_I_CIRC, CAPITAL_I_CIRC)
                    .replace(HTML_CAPITAL_I_GRAVE, CAPITAL_I_GRAVE).replace(HTML_CAPITAL_I_UML, CAPITAL_I_UML)
                    .replace(HTML_CAPITAL_N_TILDE, CAPITAL_N_TILDE).replace(HTML_CAPITAL_O_ACUTE, CAPITAL_O_ACUTE)
                    .replace(HTML_CAPITAL_O_CIRC, CAPITAL_O_CIRC).replace(HTML_CAPITAL_O_GRAVE, CAPITAL_O_GRAVE)
                    .replace(HTML_CAPITAL_O_SLASH, CAPITAL_O_SLASH).replace(HTML_CAPITAL_O_TILDE, CAPITAL_O_TILDE)
                    .replace(HTML_CAPITAL_O_UML, CAPITAL_O_UML).replace(HTML_CAPITAL_THORN, CAPITAL_THORN)
                    .replace(HTML_CAPITAL_U_ACUTE, CAPITAL_U_ACUTE).replace(HTML_CAPITAL_U_CIRC, CAPITAL_U_CIRC)
                    .replace(HTML_CAPITAL_U_GRAVE, CAPITAL_U_GRAVE).replace(HTML_CAPITAL_U_UML, CAPITAL_U_UML)
                    .replace(HTML_CAPITAL_Y_ACUTE, CAPITAL_Y_ACUTE).replace(HTML_A_ACUTE, A_ACUTE)
                    .replace(HTML_A_CIRC, A_CIRC).replace(HTML_AE, AE).replace(HTML_A_GRAVE, A_GRAVE)
                    .replace(HTML_A_RING, A_RING).replace(HTML_A_TILDE, A_TILDE).replace(HTML_A_UML, A_UML)
                    .replace(HTML_C_CEDIL, C_CEDIL).replace(HTML_E_ACUTE, E_ACUTE).replace(HTML_E_CIRC, E_CIRC)
                    .replace(HTML_E_GRAVE, E_GRAVE).replace(HTML_ETH, ETH).replace(HTML_E_UML, E_UML)
                    .replace(HTML_I_ACUTE, I_ACUTE).replace(HTML_I_CIRC, I_CIRC).replace(HTML_I_GRAVE, I_GRAVE)
                    .replace(HTML_I_UML, I_UML).replace(HTML_N_TILDE, N_TILDE).replace(HTML_O_ACUTE, O_ACUTE)
                    .replace(HTML_O_CIRC, O_CIRC).replace(HTML_O_GRAVE, O_GRAVE).replace(HTML_O_SLASH, O_SLASH)
                    .replace(HTML_O_TILDE, O_TILDE).replace(HTML_O_UML, O_UML).replace(HTML_SZLIG, SZLIG)
                    .replace(HTML_THORN, THORN).replace(HTML_U_ACUTE, U_ACUTE).replace(HTML_U_CIRC, U_CIRC)
                    .replace(HTML_U_GRAVE, U_GRAVE).replace(HTML_U_UML, U_UML).replace(HTML_Y_ACUTE, Y_ACUTE)
                    .replace(HTML_Y_UML, Y_UML).replace(HTML_CENT, CENT);
            int startIndex = result.indexOf(HTML_HEX_START);
            while (startIndex > -1) {
                int stopIndex = result.indexOf(';', startIndex);
                if (stopIndex < startIndex) {
                    break;
                } else {
                    try {
                        String toReplace = result.substring(startIndex, stopIndex + 1);
                        String hex = result.substring(startIndex + 3, stopIndex);
                        char c = (char) Integer.parseInt(hex, 16);
                        result = result.replace(toReplace, String.valueOf(c));
                        startIndex = result.indexOf(HTML_HEX_START);
                    } catch (Exception e) {
                        break;
                    }
                }
            }
        }
        return result;
    }

}
