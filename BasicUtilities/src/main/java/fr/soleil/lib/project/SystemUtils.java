/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * A class used to interact with System properties
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class SystemUtils {

    /**
     * Recovers a property from system properties, or environment variables (if the system property does not exist)
     * 
     * @param propertyName The name of the property to recover
     * @return The expected property, or <code>null</code> if there is no such property
     */
    public static String getSystemProperty(String propertyName) {
        String result = null;
        if (propertyName != null) {
            try {
                result = System.getProperty(propertyName);
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            }
            try {
                if (result == null || result.trim().isEmpty()) {
                    result = System.getenv(propertyName);
                }
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            }
        }
        return result;
    }

    /**
     * Recovers a <code>boolean</code> property from system properties, or environment variables (if the system property
     * does not exist)
     * 
     * @param propertyName The name of the property to recover
     * @param defaultValue The default value to return if the property does not exist or is not a <code>boolean</code>
     *            property
     * @return A <code>boolean</code> value
     */
    public static boolean getSystemBooleanProperty(String propertyName, boolean defaultValue) {
        boolean result = defaultValue;
        String property = getSystemProperty(propertyName);
        if (property != null) {
            try {
                result = Boolean.parseBoolean(property);
            } catch (Exception e) {
                result = defaultValue;
            }
        }
        return result;
    }

    /**
     * Recovers a <code>byte</code> property from system properties, or environment variables (if the system property
     * does not exist)
     * 
     * @param propertyName The name of the property to recover
     * @param defaultValue The default value to return if the property does not exist or is not a <code>byte</code>
     *            property
     * @return A <code>byte</code> value
     */
    public static byte getSystemByteProperty(String propertyName, byte defaultValue) {
        byte result = defaultValue;
        String property = getSystemProperty(propertyName);
        if (property != null) {
            try {
                result = Byte.parseByte(property);
            } catch (Exception e) {
                result = defaultValue;
            }
        }
        return result;
    }

    /**
     * Recovers a <code>short</code> property from system properties, or environment variables (if the system property
     * does not exist)
     * 
     * @param propertyName The name of the property to recover
     * @param defaultValue The default value to return if the property does not exist or is not a <code>short</code>
     *            property
     * @return A <code>short</code> value
     */
    public static short getSystemShortProperty(String propertyName, short defaultValue) {
        short result = defaultValue;
        String property = getSystemProperty(propertyName);
        if (property != null) {
            try {
                result = Short.parseShort(property);
            } catch (Exception e) {
                result = defaultValue;
            }
        }
        return result;
    }

    /**
     * Recovers an <code>int</code> property from system properties, or environment variables (if the system property
     * does not exist)
     * 
     * @param propertyName The name of the property to recover
     * @param defaultValue The default value to return if the property does not exist or is not an <code>int</code>
     *            property
     * @return An <code>int</code> value
     */
    public static int getSystemIntProperty(String propertyName, int defaultValue) {
        int result = defaultValue;
        String property = getSystemProperty(propertyName);
        if (property != null) {
            try {
                result = Integer.parseInt(property);
            } catch (Exception e) {
                result = defaultValue;
            }
        }
        return result;
    }

    /**
     * Recovers a <code>long</code> property from system properties, or environment variables
     * (if the system property does not exist)
     * 
     * @param propertyName The name of the property to recover
     * @param defaultValue The default value to return if the property does not exist or is not a <code>long</code>
     *            property
     * @return A <code>long</code> value
     */
    public static long getSystemLongProperty(String propertyName, long defaultValue) {
        long result = defaultValue;
        String property = getSystemProperty(propertyName);
        if (property != null) {
            try {
                result = Long.parseLong(property);
            } catch (Exception e) {
                result = defaultValue;
            }
        }
        return result;
    }

    /**
     * Recovers a <code>float</code> property from system properties, or environment variables (if the system property
     * does not exist)
     * 
     * @param propertyName The name of the property to recover
     * @param defaultValue The default value to return if the property does not exist or is not a <code>float</code>
     *            property
     * @return A <code>float</code> value
     */
    public static float getSystemFloatProperty(String propertyName, float defaultValue) {
        float result = defaultValue;
        String property = getSystemProperty(propertyName);
        if (property != null) {
            try {
                result = Float.parseFloat(property);
            } catch (Exception e) {
                result = defaultValue;
            }
        }
        return result;
    }

    /**
     * Recovers a <code>double</code> property from system properties, or environment variables (if the system property
     * does not exist)
     * 
     * @param propertyName The name of the property to recover
     * @param defaultValue The default value to return if the property does not exist or is not a <code>double</code>
     *            property
     * @return A <code>double</code> value
     */
    public static double getSystemDoubleProperty(String propertyName, double defaultValue) {
        double result = defaultValue;
        String property = getSystemProperty(propertyName);
        if (property != null) {
            try {
                result = Double.parseDouble(property);
            } catch (Exception e) {
                result = defaultValue;
            }
        }
        return result;
    }

    /**
     * Return the computer full name. <br>
     * 
     * @return The name, or <b>null</b> if the it cannot be found.
     */
    public static String getComputerFullName() {
        // Solution found here :
        // https://stackoverflow.com/questions/7348711/recommended-way-to-get-hostname-in-java/40702767
        String hostName;
        try {
            final InetAddress addr = InetAddress.getLocalHost();
            hostName = addr.getHostName();
        } catch (final UnknownHostException e) {
            hostName = null;
        }
        if (hostName == null || hostName.trim().isEmpty()) {
            hostName = System.getenv("COMPUTERNAME");
        }
        if (hostName == null || hostName.trim().isEmpty()) {
            hostName = System.getenv("HOSTNAME");
        }
        if (hostName == null || hostName.trim().isEmpty()) {
            try (BufferedReader reader = new BufferedReader(
                    new InputStreamReader(Runtime.getRuntime().exec("hostname").getInputStream()))) {
                hostName = reader.readLine();
            } catch (IOException e) {
                // Nothing to do: leave hostName as is (i.e. null).
            }
        }
        return hostName;
    }

}
