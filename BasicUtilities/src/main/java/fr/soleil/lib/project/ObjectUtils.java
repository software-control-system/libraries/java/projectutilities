/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.Reference;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import fr.soleil.lib.project.math.ArrayUtils;

/**
 * A Class that gives some useful methods to work with {@link Object}s.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ObjectUtils {

    public static final String EMPTY_STRING = "";
    public static final String NEW_LINE = "\n";

    /**
     * Compares 2 {@link Object}s to check whether they are equals to each other
     * 
     * @param o1 The 1st {@link Object} to compare
     * @param o2 The 2nd {@link Object} to compare
     * @return A boolean value. <code>TRUE</code> if both {@link Object}s are <code>null</code>, or if their
     *         {@link Object#equals(Object)} method returns <code>TRUE</code>. <code>FALSE</code> otherwise.
     */
    public static boolean sameObject(Object o1, Object o2) {
        boolean result;
        if (o1 == null) {
            result = (o2 == null);
        } else {
            if (o1.getClass().isArray()) {
                result = ArrayUtils.equals(o1, o2);
            } else {
                result = o1.equals(o2);
            }
        }
        return result;
    }

    /**
     * Compares 2 <code>double</code> values, and return whether they are equal.
     * 
     * @param d1 The 1st value
     * @param d2 The 2nd value
     * @return <code>true</code> if either <code>d1 == d2</code> or both <code>d1</code> and <code>d2</code> are
     *         <code>NaN</code>.
     */
    public static boolean sameDouble(double d1, double d2) {
        boolean equals;
        if (Double.isNaN(d1)) {
            equals = Double.isNaN(d2);
        } else {
            equals = (d1 == d2);
        }
        return equals;
    }

    /**
     * Compares 2 <code>float</code> values, and return whether they are equal.
     * 
     * @param f1 The 1st value
     * @param f2 The 2nd value
     * @return <code>true</code> if either <code>f1 == f2</code> or both <code>f1</code> and <code>f2</code> are
     *         <code>NaN</code>.
     */
    public static boolean sameFloat(float f1, float f2) {
        boolean equals;
        if (Float.isNaN(f1)) {
            equals = Float.isNaN(f2);
        } else {
            equals = (f1 == f2);
        }
        return equals;
    }

    /**
     * Returns the square of a value
     * 
     * @param value The value to square
     * @return <code>value&sup2;</code>
     */
    public static double square(double value) {
        return value * value;
    }

    /**
     * Returns the square of a value
     * 
     * @param value The value to square
     * @return <code>value&sup2;</code>
     */
    public static float square(float value) {
        return value * value;
    }

    /**
     * Returns an {@link Object}'s hash code
     * 
     * @param o The {@link Object}
     * @return An int. <code>0</code> if the {@link Object} is <code>null</code> , its {@link Object#hashCode()}
     *         otherwise.
     */
    public static int getHashCode(Object o) {
        int hashCode = 0;
        if (o != null) {
            if (o.getClass().isArray()) {
                hashCode = ArrayUtils.hashCode(o);
            } else {
                hashCode = o.hashCode();
            }
        }
        return hashCode;
    }

    /**
     * Returns an hash code from a boolean
     * 
     * @param b The boolean
     * @return The resulting hash code
     */
    public static int generateHashCode(boolean b) {
        return (b ? 0x600D : 0xBAD);
    }

    /**
     * Returns whether a given {@link Class} represents some number (Object or
     * primitive) values
     * 
     * @param classToTest The {@link Class} to test
     * @return A <code>boolean</code> value. <code>true</code> If the given {@link Class} represents some number values
     */
    public static boolean isNumberClass(Class<?> classToTest) {
        boolean isNumber;
        if (classToTest == null) {
            isNumber = false;
        } else {
            isNumber = (Number.class.isAssignableFrom(classToTest) || Byte.TYPE.equals(classToTest)
                    || Short.TYPE.equals(classToTest) || Integer.TYPE.equals(classToTest)
                    || Long.TYPE.equals(classToTest) || Float.TYPE.equals(classToTest)
                    || Double.TYPE.equals(classToTest));
        }
        return isNumber;
    }

    /**
     * Returns whether 2 different {@link Map}s are equals in content, ignoring
     * key sorting.
     * 
     * @param map1 First {@link Map} to test
     * @param map2 Second {@link Map} to test
     * @param recursive Whether this method should be recursive (to work, for example,
     *            with {@link Map}s of {@link Map}s)
     * @return A <code>boolean</code> value. <code>true</code> if both {@link Map}s are equals in content, ignoring key
     *         sorting, or if
     *         both {@link Map}s are <code>null</code>. <code>false</code> otherwise.
     */
    public static boolean sameMapContent(Map<?, ?> map1, Map<?, ?> map2, boolean recursive) {
        boolean same;
        if (map1 == null) {
            same = (map2 == null);
        } else if (map2 == null) {
            same = false;
        } else if (map1 == map2) {
            same = true;
        } else if (map1.size() == map2.size()) {
            same = true;
            for (Entry<?, ?> entry : map1.entrySet()) {
                Object obj1 = entry.getValue();
                Object obj2 = map2.get(entry.getKey());
                if (!sameObject(obj1, obj2)) {
                    if (recursive && (obj1 instanceof Map<?, ?>) && (obj2 instanceof Map<?, ?>)) {
                        Map<?, ?> child1 = (Map<?, ?>) obj1;
                        Map<?, ?> child2 = (Map<?, ?>) obj2;
                        if (!sameMapContent(child1, child2, recursive)) {
                            same = false;
                            break;
                        }
                    } else {
                        same = false;
                        break;
                    }
                }
            }
        } else {
            same = false;
        }
        return same;
    }

    /**
     * Returns whether 2 different {@link Map}s are equals in content, ignoring
     * key sorting. This method is recursive (works, for example, with {@link Map}s of {@link Map}s)
     * 
     * @param map1 First {@link Map} to test
     * @param map2 Second {@link Map} to test
     * @return A <code>boolean</code> value. <code>true</code> if both {@link Map}s are equals in content, ignoring key
     *         sorting, or if both {@link Map}s are <code>null</code>. <code>false</code> otherwise.
     */
    public static boolean sameMapContent(Map<?, ?> map1, Map<?, ?> map2) {
        return sameMapContent(map1, map2, true);
    }

    /**
     * Recovers an object from a {@link Reference}
     * 
     * @param ref The {@link Reference}
     * @return <code>null</code> if <code>ref</code> is <code>null</code>, its
     *         referenced object otherwise
     */
    public static <T> T recoverObject(Reference<T> ref) {
        T result;
        if (ref == null) {
            result = null;
        } else {
            result = ref.get();
        }
        return result;
    }

    /**
     * Returns the stack trace of a {@link Throwable}
     * 
     * @param t The {@link Throwable}
     * @return A {@link String}. <code>null</code> if <code>t</code> is <code>null</code>.
     */
    public static String printStackTrace(Throwable t) {
        String result;
        if (t == null) {
            result = null;
        } else {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            result = sw.toString().trim();
        }
        return result;
    }

    public static <E> E getFirstElement(Collection<E> list) {
        E element = null;
        if (list != null) {
            for (E e : list) {
                element = e;
                break;
            }
        }
        return element;
    }

}
