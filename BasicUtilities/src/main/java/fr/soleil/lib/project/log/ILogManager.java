/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.log;

/**
 * An interface for something able to manage or display some logs represented by {@link LogData} objects.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface ILogManager {

    /**
     * Returns the logs managed by this {@link ILogManager}.
     * 
     * @return A {@link LogData} array.
     */
    public LogData[] getLogs();

    /**
     * Adds some logs in this {@link ILogManager}.
     * 
     * @param logs The logs to add.
     */
    public void addLogs(LogData... logs);

    /**
     * Sets this {@link ILogManager}'s managed logs.
     * 
     * @param keepLastLogs Whether, in case of log limitations, to keep last logs instead of first ones.
     * @param logs The logs to set.
     */
    public void setLogs(boolean keepLastLogs, LogData... logs);

    /**
     * Sets this {@link ILogManager}'s managed logs.
     * 
     * @param logs The logs to set.
     */
    public void setLogs(LogData... logs);

    /**
     * Removes all this {@link ILogManager}'s managed logs.
     */
    public void clearLogs();

    /**
     * Returns the maximum number of logs managed by this {@link ILogManager}
     * 
     * @return An <code>int</code>.
     */
    public int getMaxLogs();

    /**
     * Sets the maximum number of logs this {@link ILogManager} can manage.
     * 
     * @param maxLogs The maximum number of logs this {@link ILogManager} can manage.
     */
    public void setMaxLogs(int maxLogs);

}
