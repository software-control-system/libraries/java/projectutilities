/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.file;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

import fr.soleil.lib.project.ObjectUtils;

public class FileUtils {

    private static final boolean IS_WINDOWS = System.getProperty("os.name").toLowerCase().startsWith("windows");

    private static final String CMD = "cmd";
    private static final String SLASH_C = "/c";
    private static final String NET = "net";
    private static final String USE = "use";
    private static final String COLON = ":";
    private static final String DF = "df";
    private static final String H = "-h";
    private static final String UNABLE_TO_RUN_NET_USE_ON = "Unable to run 'net use' on ";
    private static final String UNABLE_TO_RUN_DF_H_ON = "Unable to run 'df -h' on ";
    private static final String NETWORK1 = "//";
    private static final String NETWORK2 = "\\\\";

    /**
     * Returns the extension ("." excluded) of a file, lower case (example: For the file named
     * "myPicture.PNG", it will return "png"). If the file is <code>null</code>, or if there is a
     * problem with the file, it will return <code>null</code>. If there is no extension for the
     * file, it will return an empty String.
     * 
     * @param file the file from which to obtain the extension
     * @return a {@link String}
     */
    public static String getExtension(File file) {
        String ext = null;
        if (file != null) {
            try {
                String s = file.getName();
                int i = s.lastIndexOf('.');
                if (i > 0 && i < s.length() - 1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
            } catch (Exception e) {
                ext = null;
            }
        }
        return ext;
    }

    /**
     * Returns the extension ("." excluded) of a file specified by a file path. If the file path is
     * null or if there is no extension for the file, it will return an empty String.
     * 
     * @param filePath the file path from which to obtain the extension
     * @return a {@link String}
     */
    public static String getExtension(String filePath) {
        String extension = null;
        if (filePath != null) {
            extension = getExtension(new File(filePath));
        }
        return extension;
    }

    /**
     * Copies the content of an {@link InputStream} into an {@link OutputStream}.
     * 
     * @param source The {@link InputStream}.
     * @param dest The {@link OutputStream}.
     * @param closeSource Whether to close {@link InputStream} once finished.
     *            <p>
     *            <i>Note that close won't happen if either <code>source</code> or <code>dest</code> is
     *            <code>null</code>.</i>
     *            </p>
     * @param closeDest Whether to close {@link OutputStream} once finished.
     *            <p>
     *            <i>Note that close won't happen if either <code>source</code> or <code>dest</code> is
     *            <code>null</code>.</i>
     *            </p>
     * @return A <code>boolean</code>: whether copy was successful.
     * @throws IOException If a problem occurred.
     */
    public static boolean copy(InputStream source, OutputStream dest, boolean closeSource, boolean closeDest)
            throws IOException {
        IOException error = null;
        boolean copied;
        if ((source == null) || (dest == null)) {
            copied = false;
        } else {
            copied = true;
            byte[] buffer = new byte[10000]; // buffer size
            int length;
            try {
                while ((length = source.read(buffer)) != -1) {
                    dest.write(buffer, 0, length);
                }
            } catch (IOException ex) {
                error = ex;
                copied = false;
            } finally {
                if (closeDest) {
                    try {
                        dest.close();
                    } catch (IOException e) {
                        if (error == null) {
                            error = e;
                        }
                    }
                }
                if (closeSource) {
                    try {
                        source.close();
                    } catch (IOException e) {
                        if (error == null) {
                            error = e;
                        }
                    }
                }
            }
        }
        if (error != null) {
            throw error;
        }
        return copied;
    }

    /**
     * Copies the content of an {@link InputStream} into an {@link OutputStream}, and closes them once finished.
     * <p>
     * <i>Note that close won't happen if either <code>source</code> or <code>dest</code> is <code>null</code>.</i>
     * </p>
     * 
     * @param source The {@link InputStream}.
     * @param dest The {@link OutputStream}.
     * @return A <code>boolean</code>: whether copy was successful.
     * @throws IOException If a problem occurred.
     */
    public static boolean copy(InputStream source, OutputStream dest) throws IOException {
        return copy(source, dest, true, true);
    }

    /**
     * Duplicates a {@link File}, and throws any {@link IOException} that may occur during file duplication.
     * 
     * @param source the source {@link File}, as an {@link InputStream}
     * @param dest the destination {@link File}
     * @param allowOverWrite a boolean to allow or not writing over an existing destination {@link File}.
     *            <code>true</code> to allow overwriting.
     * @return A boolean value: <code>true</code> if the destination File was successfully created
     *         and written, <code>false</code> otherwise
     * @throws IOException If a problem occurred during file duplication
     */
    public static boolean duplicateFileThrowingException(InputStream source, File dest, boolean allowOverWrite)
            throws IOException {
        boolean result = true;
        if ((source == null) || (dest == null) || dest.isDirectory() || (dest.exists() && !allowOverWrite)) {
            result = false;
        } else if (!dest.getParentFile().exists()) {
            if (!dest.getParentFile().mkdirs()) {
                result = false;
            }
        }
        if (result) {
            result = copy(source, new FileOutputStream(dest));
        }
        return result;
    }

    /**
     * Duplicates a {@link File}, without throwing any exception.
     * 
     * @param source the source {@link File}, as an {@link InputStream}
     * @param dest the destination {@link File}
     * @param allowOverWrite a boolean to allow or not writing over an existing destination {@link File}.
     *            <code>true</code> to allow overwriting.
     * @return A boolean value: <code>true</code> if the destination File was successfully created
     *         and written, <code>false</code> otherwise
     */
    public static boolean duplicateFile(InputStream source, File dest, boolean allowOverWrite) {
        boolean result;
        try {
            result = duplicateFileThrowingException(source, dest, allowOverWrite);
        } catch (IOException e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    /**
     * Creates an {@link InputStream} from a {@link String}, that can then be used in
     * {@link #duplicateFileThrowingException(InputStream, File, boolean)} and
     * {@link #duplicateFile(InputStream, File, boolean)}
     * 
     * @param string The {@link String} you want to converrto to {@link InputStream}
     * @return An {@link InputStream}. <code>null</code> if <code>string</code> was <code>null</code>
     */
    public static InputStream stringToInputStream(String string) {
        InputStream result;
        if (string == null) {
            result = null;
        } else {
            result = new ByteArrayInputStream(string.getBytes());
        }
        return result;
    }

    /**
     * Creates a copy of a {@link File} available in project resources
     * 
     * @param packageName The package in which the expected file should be found, separated by '/'
     * @param fileName The expected file name. If too short, the generated file name will start with "tmp" then the file
     *            name (to avoid {@link java.lang.IllegalArgumentException IllegalArgumentException})
     * @param deleteOnExit Whether the copy should be deleted when the virtual machine terminates. If <code>true</code>,
     *            {@link File#deleteOnExit()} is called at the end of this method for the generated {@link File}
     * @return A {@link File}. <code>null</code> when <code>fileName</code> or <code>packageName</code> is
     *         <code>null</code> or empty
     * @throws IOException If a file could not be created
     * @throws SecurityException
     *             If a security manager exists and its <code>{@link
     *             java.lang.SecurityManager#checkWrite(java.lang.String)}</code> method does not allow a file to be
     *             created
     * @see File#createTempFile(String, String)
     * @see File#deleteOnExit()
     */
    public static File createCopyFromResource(String packageName, String fileName, boolean deleteOnExit)
            throws IOException, SecurityException {
        File copy = null;
        if ((packageName != null) && (!packageName.trim().isEmpty()) && (fileName != null)
                && (!fileName.trim().isEmpty())) {
            String adaptedPackageName = (packageName.endsWith("/") ? packageName : packageName + "/");
            String resourceCompleteName = adaptedPackageName + fileName;
            int index = fileName.lastIndexOf('.');
            String name, extension;
            if (index > -1) {
                name = fileName.substring(0, index);
                extension = fileName.substring(index);
            } else {
                name = fileName;
                extension = ObjectUtils.EMPTY_STRING;
            }
            if (name.length() < 3) {
                name = "tmp" + name;
            }
            copy = File.createTempFile(name, extension);
            FileUtils.duplicateFile(FileUtils.class.getResourceAsStream(resourceCompleteName), copy, true);
            if (deleteOnExit) {
                copy.deleteOnExit();
            }
        }
        return copy;
    }

    /**
     * Returns whether 2 file paths represent the same file.
     * 
     * @param path1 The 1st file path to test.
     * @param path2 The 2nd file path to test.
     * @return A <code>boolean</code> value.
     */
    public static boolean samePath(String path1, String path2) {
        boolean samePath;
        if (path1 == null) {
            samePath = (path2 == null);
        } else {
            try {
                File f1 = new File(path1);
                File f2 = new File(path2);
                samePath = (f1.getAbsolutePath().equals(f2.getAbsolutePath()));
            } catch (Exception e) {
                samePath = false;
            }
        }
        return samePath;
    }

    /**
     * Returns whether a file path represents a directory.
     * 
     * @param filePath The file path to test.
     * @return A <code>boolean</code> value.
     */
    public static boolean isDirectory(String filePath) {
        boolean directory;
        if (filePath == null) {
            directory = false;
        } else {
            try {
                directory = new File(filePath).isDirectory();
            } catch (Exception e) {
                directory = false;
            }
        }
        return directory;
    }

    /**
     * Returns whether a given {@link File} is a directory that contains some files of a particular type.
     * 
     * @param directory The {@link File} to test.
     * @param extension The file extension that identifies the expected file type.
     * @return A <code>boolean</code> value.
     */
    public static boolean isDirectoryWithFileType(File directory, String extension) {
        boolean result = false;
        if ((directory != null) && (directory.isDirectory())) {
            for (File child : directory.listFiles()) {
                if (ObjectUtils.sameObject(extension, getExtension(child))) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Reads the file associated to the name passed in parameter.
     * 
     * @param fileName of type String.
     * @return The file content.
     */
    public static String readTextFile(String fileName) {
        String ko = ObjectUtils.EMPTY_STRING;
        String result = ko;
        try {
            File file = new File(fileName);
            if (file.isFile()) {
                result = readTextStream(new FileReader(file));
            }
        } catch (Exception e) {
            result = ko;
        }
        return result;
    }

    /**
     * Reads a text coming from an {@link InputStreamReader} (generally, a {@link FileReader})
     * 
     * @param reader The {@link InputStreamReader}
     * @param ko The default {@link String} to return in case of problem
     * @return A {@link String}. <code>null</code> if <code>reader</code> is <code>null</code>
     * @throws IOException If a problem occurred during reading.
     */
    public static String readTextStream(InputStreamReader reader) throws IOException {
        String result;
        if (reader == null) {
            result = null;
        } else {
            StringBuilder sb = new StringBuilder(5000);
            BufferedReader r = new BufferedReader(reader);
            try {
                String s = r.readLine();
                while (s != null) {
                    sb.append(s).append('\n');
                    s = r.readLine();
                }
                if (sb.length() > 0) {
                    // remove last '\n'
                    sb.deleteCharAt(sb.length() - 1);
                }
            } finally {
                r.close();
            }
            result = sb.toString();
        }
        return result;
    }

    /**
     * Checks whether a drive letter matches a network drive, using the <code>net</code> command to determine what this
     * drive is.
     * <code>net use</code> will return an error for anything which isn't a share.
     * <p>
     * This code is dedicated to windows OS.
     * </p>
     * 
     * @param driveLetter The drive letter.
     * @return A <code>boolean</code>. <code>true</code> if the drive letter matches a network drive.
     * @throws IllegalStateException If there was a problem running the <code>net</code> command.
     */
    private static boolean isWindowsNetworkDrive(char driveLetter) throws IllegalStateException {
        List<String> cmd = Arrays.asList(CMD, SLASH_C, NET, USE, driveLetter + COLON);
        try {
            Process p = new ProcessBuilder(cmd).redirectErrorStream(true).start();
            p.getOutputStream().close();
            StringBuilder consoleOutput = new StringBuilder();
            String line;
            try (BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
                while ((line = in.readLine()) != null) {
                    consoleOutput.append(line);
                }
            }
            return p.waitFor() == 0;
        } catch (Exception e) {
            throw new IllegalStateException(UNABLE_TO_RUN_NET_USE_ON + driveLetter, e);
        }
    }

    /**
     * Checks whether a directory is in fact a network directory, using the <code>df</code> command to determine what
     * this directory is.
     * <p>
     * This code is dedicated to Linux/Unix OS.
     * </p>
     * 
     * @param dir The {@link File} to test.
     * @return A <code>boolean</code>. <code>true</code> if the the directory is on a network.
     * @throws IllegalStateException If there was a problem running the <code>df</code> command.
     */
    private static boolean isLinuxNetworkDirectory(File dir) throws IllegalStateException {
        boolean networkDir = false;
        if ((dir != null) && (dir.isDirectory())) {
            String path = dir.getAbsolutePath();
            List<String> cmd = Arrays.asList(DF, H, path);
            try {
                Process p = new ProcessBuilder(cmd).redirectErrorStream(true).start();
                p.getOutputStream().close();
                String line;
                int lineIndex = 0;
                // On linux, we can use "df -h" as a test for directory being or not on a network.
                // df -f, if it works, will return something like this:
                // Filesystem Size Used Available Capacity Mounted-on
                // corresponding values
                // If the directory is on a network, then the "file system" corresponding value (at line 2) will be:
                // network:path, with path starting with "/"
                // So, the idea is to look for the ":/" pattern at second line.
                try (BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
                    while ((line = in.readLine()) != null) {
                        lineIndex++;
                        if (lineIndex == 2) {
                            boolean colon = false;
                            for (int i = 0; i < line.length(); i++) {
                                char c = line.charAt(i);
                                if (c == ':') {
                                    colon = true;
                                } else if (c == '/') {
                                    if (colon) {
                                        networkDir = true;
                                        break;
                                    } else {
                                        colon = false;
                                    }
                                } else if (Character.isWhitespace(c)) {
                                    break;
                                } else {
                                    colon = false;
                                }
                            }
                        } else {
                            continue;
                        }
                    }
                }
                if (p.waitFor() == 1) {
                    networkDir = false;
                }
            } catch (Exception e) {
                throw new IllegalStateException(UNABLE_TO_RUN_DF_H_ON + path, e);
            }
        }
        return networkDir;
    }

    /**
     * Returns whether a file is on a shared network.
     * <p>
     * On Windows OS, the test will be run using "<code>net use</code>".
     * </p>
     * <p>
     * On Linux/Unix OS, the test will be run using "<code>df -h</code>".
     * </p>
     * 
     * @param f The file to test.
     * @return A <code>boolean</code>. <code>true</code> if the file is on a shared network.
     * @throws IllegalStateException If there was a problem running the expected command on the corresponding OS.
     */
    public static boolean isNetworkFile(File f) throws IllegalStateException {
        boolean network = false;
        while ((f != null) && (!f.isDirectory())) {
            f = f.getParentFile();
        }
        if (f != null) {
            String path = f.getAbsolutePath();
            if ((path != null) && (path.length() > 1)) {
                if (path.startsWith(NETWORK1) || path.startsWith(NETWORK2)) {
                    // Path starting with // or \\ is considered as a network path.
                    network = true;
                } else if (IS_WINDOWS) {
                    if (path.charAt(1) == ':') {
                        network = isWindowsNetworkDrive(path.charAt(0));
                    } else {
                        // Unusual case which probably means network.
                        network = true;
                    }
                } else {
                    // Linux/Unix case.
                    network = isLinuxNetworkDirectory(f);
                }
            }
        }
        return network;
    }

}
