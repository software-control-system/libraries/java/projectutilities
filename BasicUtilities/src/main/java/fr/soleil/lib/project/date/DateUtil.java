/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.date;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicLong;

public final class DateUtil implements IDateConstants {

    private static final String AT_ZERO_H = " 00:00:00";
    private static final String ZERO_H = "00:00:00";
    private static final String ZERO_H_1 = "0:00:00";
    private static final String AND_ZERO_M = ":00:00";
    private static final String ZERO_M = "00:00";
    private static final String ZERO_M_1 = "0:00";
    private static final String ZERO_S = ":00";
    private static final String DOUBLE_ZERO = "00";
    private static final String ZERO_MS = ".000";
    private static final String ZERO = "0";

    private static final String D = "d ";
    private static final String H = "h ";
    private static final String MN = "mn ";
    private static final String S = "s ";
    private static final String MS = "ms";
    private static final String MS_CLOSED = "ms)";
    private static final String MS_SPACE = "ms ";
    private static final String NS = "ns";
    private static final String NS_CLOSED = "ns)";
    private static final String PARENTHESIS = " (";

    private static final DateTimeFormatter FR_FORMAT = DateTimeFormatter.ofPattern(FR_DATE_FORMAT);
    private static final DateTimeFormatter US_FORMAT = DateTimeFormatter.ofPattern(US_DATE_FORMAT);

    /**
     * Recovers the best {@link DateTimeFormatter} for given date pattern.
     * 
     * @param pattern The date pattern.
     * @return A {@link DateTimeFormatter}
     */
    public static DateTimeFormatter getDateTimeFormatter(String pattern) {
        DateTimeFormatter formatter;
        String dateFormat = pattern;
        if ((dateFormat == null) || dateFormat.trim().isEmpty()) {
            dateFormat = US_DATE_FORMAT;
        }
        switch (dateFormat) {
            case FR_DATE_FORMAT:
                formatter = FR_FORMAT;
                break;
            case US_DATE_FORMAT:
                formatter = US_FORMAT;
                break;
            default:
                formatter = DateTimeFormatter.ofPattern(pattern);
                break;
        }
        return formatter;
    }

    /**
     * Casts the given long (number of milliseconds since January 1, 1970) into a string according given
     * {@link DateTimeFormatter}.
     * 
     * @param milli The date in millisecond to cast.
     * @param formatter The {@link DateTimeFormatter}.
     * @return A string representing the date into the given pattern format.
     */
    public static String milliToString(final long milli, final DateTimeFormatter formatter) {
        return formatter.format(Instant.ofEpochMilli(milli).atZone(ZoneId.systemDefault()).toLocalDateTime());
    }

    /**
     * Casts the given long (number of milliseconds since January 1, 1970) into a string that follows the given pattern.
     * 
     * @param milli The date in millisecond to cast.
     * @param pattern The target pattern.
     * @return a string representing the date into the given pattern format.
     */
    public static String milliToString(final long milli, final String pattern) {
        return milliToString(milli, getDateTimeFormatter(pattern));
    }

    /**
     * Casts a string format date (dd-MM-yyyy HH:mm:ss or yyyy-MM-dd HH:mm:ss) into long (number of milliseconds since
     * January 1, 1970) according given {@link DateTimeFormatter}.
     * 
     * @param milli The date in millisecond to cast.
     * @param formatter The {@link DateTimeFormatter}.
     * @return A <code>long</code> representing the date as the number of milliseconds since January 1, 1970.
     */
    public static long stringToMilli(final String date, final DateTimeFormatter formatter) {
        return LocalDateTime.parse(date, formatter).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    /**
     * Casts a string format date (dd-MM-yyyy HH:mm:ss or yyyy-MM-dd HH:mm:ss) into long (number of milliseconds since
     * January 1, 1970) according given date pattern.
     * 
     * @param milli The date in millisecond to cast.
     * @param pattern The date pattern.
     * @return A <code>long</code> representing the date as the number of milliseconds since January 1, 1970.
     */
    public static long stringToMilli(final String date, final String pattern) {
        return stringToMilli(date, getDateTimeFormatter(pattern));
    }

    /**
     * Casts a string format date (dd-MM-yyyy HH:mm:ss or yyyy-MM-dd HH:mm:ss) into long (number of milliseconds since
     * January 1, 1970).
     * 
     * @param date The date to parse.
     * @return A <code>long</code> representing the date as the number of milliseconds since January 1, 1970.
     * @throws DateTimeParseException If a problem occurred while trying to parse date.
     */
    public static long stringToMilli(final String date) throws DateTimeParseException {
        long result = -1;
        if (date != null) {
            final boolean isFr = date.indexOf(DATE_SEPARATOR) != 4;
            final int dateLength = date.length();
            final int toTheDayLength = 10; // yyyy-MM-dd or dd-MM-yyyy
            final StringBuilder dateBuilder = new StringBuilder(date);
            switch (dateLength) {
                case toTheDayLength:
                    dateBuilder.append(AT_ZERO_H);
                    break;
                case toTheDayLength + 1:
                    dateBuilder.append(ZERO_H);
                    break;
                case toTheDayLength + 2:
                    dateBuilder.append(ZERO_H_1);
                    break;
                case toTheDayLength + 3:
                    dateBuilder.append(AND_ZERO_M);
                    break;
                case toTheDayLength + 4:
                    dateBuilder.append(ZERO_M);
                    break;
                case toTheDayLength + 5:
                    dateBuilder.append(ZERO_M_1);
                    break;
                case toTheDayLength + 6:
                    dateBuilder.append(ZERO_S);
                    break;
                case toTheDayLength + 7:
                    dateBuilder.append(DOUBLE_ZERO);
                    break;
                case toTheDayLength + 8:
                    dateBuilder.append(ZERO);
                    break;
            }
            if (date.indexOf(MILLI_SECOND_SEPARATOR) == -1) {
                dateBuilder.append(ZERO_MS);
            } else {
                int indexOfMilli = date.lastIndexOf('.') + 1;
                int count = date.length() - indexOfMilli;
                // append missing "0" to be sure to have milliseconds encoded on 3 digits
                while (count < 3) {
                    dateBuilder.append(ZERO);
                    count++;
                }
            }
            final String dateToParse = dateBuilder.toString();
            try {
                String dateFormat = isFr ? FR_DATE_FORMAT : US_DATE_FORMAT;
                LocalDateTime.parse(dateToParse, getDateTimeFormatter(dateFormat)).atZone(ZoneId.systemDefault())
                        .toInstant().toEpochMilli();
            } catch (final DateTimeParseException e1) {
                try {
                    final Timestamp ts = Timestamp.valueOf(dateToParse);
                    result = ts.getTime();
                } catch (final Exception e) {
                    throw e1;
                }
            }
        }
        return result;
    }

    /**
     * Returns a date string in the French date format (dd-MM-yyyy HH:mm:ss).
     * 
     * @param date The date string.
     * @return The date string in the French date format (dd-MM-yyyy HH:mm:ss).
     * @throws ParseException If a problem occurred while trying to parse date.
     */
    public static String stringDateToFrenchStringDate(final String date) throws ParseException {
        // date = completeDate ( date );
        final long longDate = stringToMilli(date == null ? null : date.trim());
        return milliToString(longDate, FR_DATE_FORMAT);
    }

    public static String longToFrenchStringDate(long date) {
        String formattedDate = milliToString(date, FR_FORMAT);
        if (formattedDate.lastIndexOf(MILLI_SECOND_SEPARATOR) > -1) {
            while (formattedDate.charAt(formattedDate.length() - 1) == '0') {
                formattedDate = formattedDate.substring(0, formattedDate.length() - 1);
            }
            if (formattedDate.charAt(formattedDate.length() - 1) == '.') {
                formattedDate = formattedDate + ZERO;
            }
        }
        return formattedDate;
    }

    public static String dateToFrenchStringDate(Date date) {
        return longToFrenchStringDate(date.getTime());
    }

    public static String oracleToMySQL(final String string) {
        String result;
        try {
            final DateHour dh = new DateHour(string, DateHour.FORMAT_DATE_ORACLE);
            result = dh.toString(DateHour.FORMAT_DATE_MYSQL);
        } catch (final ParseException e) {
            result = null;
        }
        return result;
    }

    /**
     * Appends the string representation (in days, hours, minutes, seconds and milliseconds) of an elapsed time to a
     * {@link StringBuilder}.
     * 
     * @param builder The {@link StringBuilder}. If <code>null</code>, a new one is created and returned.
     * @param elapsedTime The elapsed time in milliseconds.
     * @return A {@link StringBuilder}, never <code>null</code>.
     */
    public static StringBuilder elapsedTimeToStringBuilder(StringBuilder builder, long elapsedTime) {
        return elapsedTimeToStringBuilder(builder, elapsedTime, true);
    }

    /**
     * Appends the string representation (in days, hours, minutes, seconds and milliseconds) of an elapsed time to a
     * {@link StringBuilder}.
     * 
     * @param builder The {@link StringBuilder}. If <code>null</code>, a new one is created and returned.
     * @param elapsedTime The elapsed time in milliseconds.
     * @param allowMilliseconds Whether to allow the displaying of milliseconds.
     * @return A {@link StringBuilder}, never <code>null</code>.
     */
    public static StringBuilder elapsedTimeToStringBuilder(StringBuilder builder, long elapsedTime,
            boolean allowMilliseconds) {
        if (builder == null) {
            builder = new StringBuilder();
        }

        // Conversion constants
        long milliSecond = 1000;
        long milliMinute = 60000;
        long milliHour = 3600000;
        long milliDay = 86400000;

        // Calculate the different parts of elapsed time
        long milliseconds = elapsedTime;
        long days = milliseconds / milliDay;
        milliseconds -= days * milliDay;
        long hours = milliseconds / milliHour;
        milliseconds -= hours * milliHour;
        long minutes = milliseconds / milliMinute;
        milliseconds -= minutes * milliMinute;
        long seconds = milliseconds / milliSecond;
        milliseconds -= seconds * milliSecond;
        boolean space = false, added = false;
        if (!allowMilliseconds) {
            if (milliseconds >= 500) {
                seconds++;
                if (seconds == 60) {
                    seconds = 0;
                    minutes++;
                    if (minutes == 60) {
                        minutes = 0;
                        hours++;
                        if (hours == 24) {
                            hours = 0;
                            days++;
                        }
                    }
                }
            }
            milliseconds = 0;
        }
        if (days > 0) {
            builder.append(days).append(D);
            space = true;
            added = true;
        }
        if (hours > 0) {
            builder.append(hours).append(H);
            space = true;
            added = true;
        }
        if (minutes > 0) {
            builder.append(minutes).append(MN);
            space = true;
            added = true;
        }
        if ((seconds > 0) || ((!added) && (!allowMilliseconds))) {
            builder.append(seconds).append(S);
            space = true;
            added = true;
        }
        if ((milliseconds > 0) || (!added)) {
            builder.append(milliseconds).append(MS);
            space = false;
        }
        if (space) {
            if (builder.charAt(builder.length() - 1) == ' ') {
                builder.deleteCharAt(builder.length() - 1);
            }
        }
        return builder;
    }

    /**
     * Appends a title and the string representation (in days, hours, minutes, seconds and milliseconds) of an elapsed
     * time, represented as an {@link AtomicLong}, to a {@link StringBuilder}.
     * 
     * @param title The title.
     * @param elapsedTime The elapsed time in milliseconds.
     * @param builder The {@link StringBuilder} to which to append title and elapsed time. Can be <code>null</code>, in
     *            which case a new {@link StringBuilder} will be returned.
     * @return A {@link StringBuilder}, never <code>null</code>.
     */
    public static StringBuilder elapsedTimeToStringBuilder(String title, AtomicLong elapsedTime,
            StringBuilder builder) {
        StringBuilder tracer = builder;
        if (tracer == null) {
            tracer = new StringBuilder();
        }
        if (elapsedTime != null) {
            if (title != null) {
                tracer.append(title);
            }
            long time = elapsedTime.get();
            elapsedTimeToStringBuilder(tracer, time);
            if (time > 999) {
                tracer.append(PARENTHESIS).append(time).append(MS_CLOSED);
            }
        }
        return tracer;
    }

    /**
     * Appends the string representation (in days, hours, minutes, seconds, milliseconds and nanoseconds) of an elapsed
     * time to a {@link StringBuilder}.
     * 
     * @param builder The {@link StringBuilder}. If <code>null</code>, a new one is created and returned.
     * @param elapsedTime The elapsed time in nanoseconds.
     * @return A {@link StringBuilder}, never <code>null</code>.
     */
    public static StringBuilder elapsedNanoTimeToStringBuilder(StringBuilder builder, long elapsedTime) {
        if (builder == null) {
            builder = new StringBuilder();
        }

        // Conversion constants
        long nanoMilli = 1000000L;
        long nanoSecond = 1000000000L;
        long nanoMinute = 60000000000L;
        long nanoHour = 3600000000000L;
        long nanoDay = 86400000000000L;
        long nanoseconds = elapsedTime;

        // Calculate the different parts of elapsed time
        long days = nanoseconds / nanoDay;
        nanoseconds -= days * nanoDay;
        long hours = nanoseconds / nanoHour;
        nanoseconds -= hours * nanoHour;
        long minutes = nanoseconds / nanoMinute;
        nanoseconds -= minutes * nanoMinute;
        long seconds = nanoseconds / nanoSecond;
        nanoseconds -= seconds * nanoSecond;
        long milliseconds = nanoseconds / nanoMilli;
        nanoseconds -= milliseconds * nanoMilli;
        boolean space = false, added = false;
        if (days > 0) {
            builder.append(days).append(D);
            space = true;
            added = true;
        }
        if (hours > 0) {
            builder.append(hours).append(H);
            space = true;
            added = true;
        }
        if (minutes > 0) {
            builder.append(minutes).append(MN);
            space = true;
            added = true;
        }
        if (seconds > 0) {
            builder.append(seconds).append(S);
            space = true;
            added = true;
        }
        if (milliseconds > 0) {
            builder.append(milliseconds).append(MS_SPACE);
            space = true;
            added = true;
        }
        if ((nanoseconds > 0) || (!added)) {
            builder.append(nanoseconds).append(NS);
            space = false;
        }
        if (space) {
            if (builder.charAt(builder.length() - 1) == ' ') {
                builder.deleteCharAt(builder.length() - 1);
            }
        }
        return builder;
    }

    /**
     * Appends a title and the string representation (in days, hours, minutes, seconds, milliseconds and nanoseconds) of
     * an elapsed time, represented as an {@link AtomicLong}, to a {@link StringBuilder}.
     * 
     * @param title The title.
     * @param elapsedTime The elapsed time in nanoseconds.
     * @param builder The {@link StringBuilder} to which to append title and elapsed time. Can be <code>null</code>, in
     *            which case a new {@link StringBuilder} will be returned.
     * @return A {@link StringBuilder}, never <code>null</code>.
     */
    public static StringBuilder elapsedNanoTimeToStringBuilder(String title, AtomicLong elapsedTime,
            StringBuilder builder) {
        StringBuilder tracer = builder;
        if (tracer == null) {
            tracer = new StringBuilder();
        }
        if (elapsedTime != null) {
            if (title != null) {
                tracer.append(title);
            }
            long time = elapsedTime.get();
            elapsedNanoTimeToStringBuilder(tracer, time);
            if (time > 999999) {
                tracer.append(PARENTHESIS).append(time).append(NS_CLOSED);
            }
        }
        return tracer;
    }

    /**
     * Returns the offset of given time zone from UTC at the specified date.
     * 
     * @param date The date.
     * @param timeZone The time zone.
     * @return A <code>byte</code>: the offset, expressed in hours.
     */
    public static byte getUTCOffset(long date, TimeZone timeZone) {
        TimeZone zone = timeZone == null ? TimeZone.getDefault() : timeZone;
        return zone == null ? 0 : (byte) (zone.getOffset(date) / 3600000);
    }

    /**
     * Returns the offset of default time zone from UTC at the specified date.
     * 
     * @param date The date.
     * @return A <code>byte</code>: the offset, expressed in hours.
     */
    public static byte getUTCOffset(long date) {
        return getUTCOffset(date, TimeZone.getDefault());
    }

    /**
     * Gets the date rounded to given precision.
     * 
     * @param date The date, expressed in milliseconds since January 1, 1970, 00:00:00 GMT.
     * @param precision The precision to which to round the date (1000 for seconds, 60000 for minutes, etc.)
     * @param utcOffset Your offset from UTC, expressed in hours
     * @return The date rounded to closest precision.
     *         <div style="border: 1px solid gray">
     *         Examples where <code>utcOffset</code> matches your timezone date offset:
     *         <hr style="color: gray" />
     *         <ul>
     *         <li>If <code>precision</code> is <code>1000</code> (i.e. precise to seconds):</code>
     *         <ul>
     *         <li><code>2022-09-01 15:28:02.525</code> will be rounded to <code>2022-09-01 15:28:03.000</code></li>
     *         <li><code>2022-09-12 17:00:05.017</code> will be rounded to <code>2022-09-12 17:00:05.000</code></li>
     *         </ul>
     *         </li>
     *         <li>If <code>precision</code> is <code>60000</code> (i.e. precise to minutes):</code>
     *         <ul>
     *         <li><code>2022-09-07 16:27:30.612</code> will be rounded to <code>2022-09-07 16:28:00.000</code></li>
     *         <li><code>2022-09-23 14:00:05.017</code> will be rounded to <code>2022-09-23 14:00:00.000</code></li>
     *         </ul>
     *         </li>
     *         </ul>
     *         </div>
     * @see #getUTCOffset(long)
     * @see #getUTCOffset(long, TimeZone)
     */
    public static long getRoundedDate(long date, long precision, byte utcOffset) {
        long adaptedDate;
        // Convert hours to milliseconds.
        long offset = 3600000L * utcOffset;
        // Take account of the offset from UTC in order to round the date.
        // Tests proved (date + offset) should be used, not (date - offset).
        long modulus = (date + offset) % precision;
        if (modulus < precision / 2) {
            adaptedDate = date - modulus;
        } else {
            adaptedDate = date + precision - modulus;
        }
        return adaptedDate;
    }

    /**
     * Gets the date rounded to given precision.
     * 
     * @param date The date, expressed in milliseconds since January 1, 1970, 00:00:00 GMT.
     * @param precision The precision to which to round the date (1000 for seconds, 60000 for minutes, etc.)
     * @return The date rounded to closest precision.
     *         <div style="border: 1px solid gray">
     *         Examples:
     *         <hr style="color: gray" />
     *         <ul>
     *         <li>If <code>precision</code> is <code>1000</code> (i.e. precise to seconds):</code>
     *         <ul>
     *         <li><code>2022-09-01 15:28:02.525</code> will be rounded to <code>2022-09-01 15:28:03.000</code></li>
     *         <li><code>2022-09-12 17:00:05.017</code> will be rounded to <code>2022-09-12 17:00:05.000</code></li>
     *         </ul>
     *         </li>
     *         <li>If <code>precision</code> is <code>60000</code> (i.e. precise to minutes):</code>
     *         <ul>
     *         <li><code>2022-09-07 16:27:30.612</code> will be rounded to <code>2022-09-07 16:28:00.000</code></li>
     *         <li><code>2022-09-23 14:00:05.017</code> will be rounded to <code>2022-09-23 14:00:00.000</code></li>
     *         </ul>
     *         </li>
     *         </ul>
     *         </div>
     */
    public static long getRoundedDate(long date, long precision) {
        return getRoundedDate(date, precision, getUTCOffset(date));
    }

}
