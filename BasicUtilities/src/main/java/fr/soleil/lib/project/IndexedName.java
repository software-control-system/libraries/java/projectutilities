/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project;

/**
 * A class used to associated a name with an index
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class IndexedName {

    private final String name;
    private final int index;
    private final boolean useIndex;

    public IndexedName(String name, int index, boolean useIndex) {
        super();
        this.name = name;
        this.index = index;
        this.useIndex = useIndex;
    }

    /**
     * Returns the name (without index)
     * 
     * @return A {@link String}
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the index
     * 
     * @return An <code>int</code>
     */
    public int getIndex() {
        return index;
    }

    /**
     * Returns whether the index should be used (whether it really means something)
     * 
     * @return A <code>boolean</code>
     */
    public boolean isUseIndex() {
        return useIndex;
    }

    /**
     * Builds an {@link IndexedName}, based on a given name.
     * <p>
     * To do so, it will:
     * <ul>
     * <li>Parse the <code>name</code> between its first and last character (or the last occurrence of
     * <code>lastCharacter</code>)</li>
     * <li>In this substring, extract the index from the last characters, as long as they represent digits</li>
     * <li>Extract the name from the previous characters</li>
     * <li>Return the corresponding {@link IndexedName}</li>
     * </ul>
     * </p>
     * 
     * @param name The name from which to build an {@link IndexedName}
     * @param lastCharacter The character of which to search the last occurrence in the name. If <code>null</code>, the
     *            whole name will be used (last occurrence will be <code>name.length()</code>
     * @return An {@link IndexedName}. <code>null</code> if <code>name</code> is <code>null</code>.
     */
    public static IndexedName buildIndexedName(String name, Character lastCharacter) {
        IndexedName result;
        if (name == null) {
            result = null;
        } else {
            boolean useIdx;
            int start, end, idx;
            if (lastCharacter == null) {
                end = name.length();
            } else {
                end = name.lastIndexOf(lastCharacter.charValue());
            }
            if (end > 0) {
                start = end - 1;
                while ((start > -1) && Character.isDigit(name.charAt(start))) {
                    start--;
                }
                if ((start > -2) && Character.isDigit(name.charAt(start + 1))) {
                    start++;
                } else {
                    start = -1;
                }
                if (start > -1) {
                    idx = Integer.parseInt(name.substring(start, end));
                    name = name.substring(0, start);
                    useIdx = true;
                } else {
                    useIdx = false;
                    idx = -1;
                }
            } else {
                idx = -1;
                useIdx = false;
            }
            result = new IndexedName(name, idx, useIdx);
        }
        return result;
    }
}
