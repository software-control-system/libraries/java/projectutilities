/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project;

/**
 * An interface that can be used to manage cancelable things, like {@link Thread}s
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public interface ICancelable {

    /**
     * Sets this {@link ICancelable} as canceled.
     * 
     * @param canceled a boolean value. <code>TRUE</code> to cancel this {@link ICancelable}
     */
    public void setCanceled(boolean canceled);

    /**
     * Returns whether this {@link ICancelable} is canceled.
     * 
     * @return a boolean value
     */
    public boolean isCanceled();
}
