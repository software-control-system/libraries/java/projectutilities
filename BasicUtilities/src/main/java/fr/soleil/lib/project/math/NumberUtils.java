/*
 * This file is part of BasicUtilities.
 * 
 * BasicUtilities is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * BasicUtilities is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with BasicUtilities. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package fr.soleil.lib.project.math;

import java.util.Arrays;

/**
 * A class to interact with numbers
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class NumberUtils implements MathConst {

    /**
     * The precise error margin used to take account of floating values precision error.
     */
    public static final double FLOAT_DELTA_PRECISE_MARGIN = 1e-13;

    /**
     * The big error margin used to take account of floating values precision error.
     */
    public static final double FLOAT_DELTA_BIG_MARGIN = 1e-10;

    protected static final char NEGATIVE_POW = '\u207b';
    protected static final char POW0 = '\u2070';
    protected static final char POW1 = '\u00b9';
    protected static final char POW2 = '\u00b2';
    protected static final char POW3 = '\u00b3';
    protected static final char POW4 = '\u2074';
    protected static final char POW5 = '\u2075';
    protected static final char POW6 = '\u2076';
    protected static final char POW7 = '\u2077';
    protected static final char POW8 = '\u2078';
    protected static final char POW9 = '\u2079';

    protected static final char NEGATIVE_SUB = '\u208b';
    protected static final char SUB0 = '\u2080';
    protected static final char SUB1 = '\u2081';
    protected static final char SUB2 = '\u2082';
    protected static final char SUB3 = '\u2083';
    protected static final char SUB4 = '\u2084';
    protected static final char SUB5 = '\u2085';
    protected static final char SUB6 = '\u2086';
    protected static final char SUB7 = '\u2087';
    protected static final char SUB8 = '\u2088';
    protected static final char SUB9 = '\u2089';

    /**
     * Returns the affine function value of a given input value, knowing 2 points of the affine function.
     * 
     * @param x The input value.
     * @param x1 The 1st point x value.
     * @param y1 The 1st point y value.
     * @param x2 The 2nd point x value.
     * @param y2 The 2nd point y value.
     * @return A <code>double</code>.
     */
    public static double getAffineFunctionValue(double x, double x1, double y1, double x2, double y2) {
        // Taylor-Young formula
        return y1 + (x - x1) * (y2 - y1) / (x2 - x1);
    }

    /**
     * Return whether 2 <code>double</code> are at a distance of less than given error margin and should be considered
     * equals.
     * 
     * @param val1 The 1st <code>double</code> value.
     * @param val2 The 2nd <code>double</code> value.
     * @param errorMargin The error margin. It is recommended to use either {@link #FLOAT_DELTA_PRECISE_MARGIN} or
     *            {@link #FLOAT_DELTA_BIG_MARGIN}.
     * @return A <code>boolean</code>.
     */
    public static boolean sameDouble(double val1, double val2, double errorMargin) {
        return Math.abs(val1 - val2) < errorMargin;
    }

    /**
     * Returns the best <code>double</code> value between a value and its rounded value, taking accound of floating
     * precision error.
     * 
     * @param value The value.
     * @param roundedValue The rounded value.
     * @param preciseErrorMargin Whether to use precise error margin. If <code>false</code>, the big error margin will
     *            be used.
     * @return A <code>double</code>.
     */
    public static double getDouble(double value, double roundedValue, boolean preciseErrorMargin) {
        double result;
        if (Double.isNaN(value) || Double.isInfinite(value)) {
            result = value;
        } else {
            if (sameDouble(value, roundedValue,
                    preciseErrorMargin ? FLOAT_DELTA_PRECISE_MARGIN : FLOAT_DELTA_BIG_MARGIN)) {
                result = roundedValue;
            } else {
                result = value;
            }
        }
        return result;
    }

    /**
     * Returns the best <code>double</code> value between a value and its rounded value, taking accound of floating
     * precision error.
     * 
     * @param value The value.
     * @param preciseErrorMargin Whether to use precise error margin. If <code>false</code>, the big error margin will
     *            be used.
     * @return A <code>double</code>.
     */
    public static double getDouble(double value, boolean preciseErrorMargin) {
        return getDouble(value, Math.round(value), preciseErrorMargin);
    }

    /**
     * Returns the floor value of a value, taking account of potential rounding errors on double variables.
     * 
     * @param value The value to floor.
     * @return A <code>double</code>.
     * @see Math#floor(double)
     */
    public static double floor(double value) {
        double floor;
        double round = Math.rint(value);
        if (Math.abs(round - value) < FLOAT_DELTA_PRECISE_MARGIN) {
            floor = round;
        } else {
            floor = Math.floor(value);
        }
        return floor;
    }

    protected static char[] addChar(char c, char[] array) {
        int position = array.length;
        array = Arrays.copyOf(array, position + 1);
        array[position] = c;
        return array;
    }

    protected static char[] addExponent(char[] array, int exponent, boolean add0And1) {
        if (exponent < 0) {
            array = addChar(NEGATIVE_POW, array);
            exponent = -exponent;
        }
        switch (exponent) {
            case 0:
                if (add0And1) {
                    array = addChar(POW0, array);
                }
                break;
            case 1:
                if (add0And1) {
                    array = addChar(POW1, array);
                }
                break;
            case 2:
                array = addChar(POW2, array);
                break;
            case 3:
                array = addChar(POW3, array);
                break;
            case 4:
                array = addChar(POW4, array);
                break;
            case 5:
                array = addChar(POW5, array);
                break;
            case 6:
                array = addChar(POW6, array);
                break;
            case 7:
                array = addChar(POW7, array);
                break;
            case 8:
                array = addChar(POW8, array);
                break;
            case 9:
                array = addChar(POW9, array);
                break;
            default:
                int log10 = (int) floor(Math.log10(exponent));
                int nearest10Pow = (int) Math.pow(10, log10);
                int div = exponent / nearest10Pow;
                int rest = exponent % nearest10Pow;
                array = addExponent(array, div, true);
                int log10rest = (int) floor(Math.log10(rest));
                int start = array.length;
                int newLength = (log10 - 1) - log10rest;
                if (newLength > 0) {
                    array = Arrays.copyOf(array, start + newLength);
                    for (int i = 0; i < newLength; i++) {
                        array[start + i] = POW0;
                    }
                }
                array = addExponent(array, rest, true);
                break;
        }
        return array;
    }

    protected static char[] addSubscript(char[] array, int subscript) {
        if (subscript < 0) {
            array = addChar(NEGATIVE_SUB, array);
            subscript = -subscript;
        }
        switch (subscript) {
            case 0:
                array = addChar(SUB0, array);
                break;
            case 1:
                array = addChar(SUB1, array);
                break;
            case 2:
                array = addChar(SUB2, array);
                break;
            case 3:
                array = addChar(SUB3, array);
                break;
            case 4:
                array = addChar(SUB4, array);
                break;
            case 5:
                array = addChar(SUB5, array);
                break;
            case 6:
                array = addChar(SUB6, array);
                break;
            case 7:
                array = addChar(SUB7, array);
                break;
            case 8:
                array = addChar(SUB8, array);
                break;
            case 9:
                array = addChar(SUB9, array);
                break;
            default:
                int log10 = (int) floor(Math.log10(subscript));
                int nearest10Pow = (int) Math.pow(10, log10);
                int div = subscript / nearest10Pow;
                int rest = subscript % nearest10Pow;
                array = addSubscript(array, div);
                int log10rest = (int) floor(Math.log10(rest));
                int start = array.length;
                int newLength = (log10 - 1) - log10rest;
                if (newLength > 0) {
                    array = Arrays.copyOf(array, start + newLength);
                    for (int i = 0; i < newLength; i++) {
                        array[start + i] = SUB0;
                    }
                }
                array = addSubscript(array, rest);
                break;
        }
        return array;
    }

    /**
     * Returns the string representation of a variable powered at a given exponent.
     * 
     * @param variable The variable. Can be <code>null</code>, in which case only the string representation of the
     *            exponent power will be returned.
     * @param exponent The exponent.
     * 
     * @return A {@link String}.
     *         <p>
     *         Examples if <b><code>variable</code></b> is "X":
     *         <ul>
     *         <li>If <b><code>exponent</code></b> is <code>-153</code>, it will return "X<sup>-153</sup>"</li>
     *         <li>If <b><code>exponent</code></b> is <code>0</code>, it will return ""</li>
     *         <li>If <b><code>exponent</code></b> is <code>1</code>, it will return "X"</li>
     *         </p>
     */
    public static String exponentToString(String variable, int exponent) {
        return exponentToString(variable, exponent, false);
    }

    protected static char[] toCharArray(String str) {
        char[] array;
        if (str == null) {
            array = new char[0];
        } else {
            array = str.toCharArray();
        }
        return array;
    }

    /**
     * Returns the string representation of a variable powered at a given exponent.
     * 
     * @param variable The variable. Can be <code>null</code>, in which case only the string representation of the
     *            exponent power will be returned.
     * @param exponent The exponent.
     * @param forceWriting0And1 Whether to force writing the power of 0 and 1.
     * 
     * @return A {@link String}.
     *         <p>
     *         Examples if <b><code>variable</code></b> is "X":
     *         <ul>
     *         <li>If <b><code>exponent</code></b> is <code>-153</code>, it will return "X<sup>-153</sup>"</li>
     *         <li>If <b><code>exponent</code></b> is <code>0</code>
     *         <ul>
     *         <li>if <b><code>forceWriting0And1</code></b> is <code>true</code>, it will return "X<sup>0</sup>"</li>
     *         <li>if <b><code>forceWriting0And1</code></b> is <code>false</code>, it will return ""</li>
     *         </ul>
     *         </li>
     *         <li>If <b><code>exponent</code></b> is <code>1</code>
     *         <ul>
     *         <li>if <b><code>forceWriting0And1</code></b> is <code>true</code>, it will return "X<sup>1</sup>"</li>
     *         <li>if <b><code>forceWriting0And1</code></b> is <code>false</code>, it will return "X"</li>
     *         </ul>
     *         </li>
     *         </p>
     */
    public static String exponentToString(String variable, int exponent, boolean forceWriting0And1) {
        if ((exponent == 0) && (!forceWriting0And1)) {
            variable = null;
        }
        return new String(addExponent(toCharArray(variable), exponent, forceWriting0And1));
    }

    /**
     * Returns the string representation of a variable with given integer as subscript.
     * 
     * @param variable The variable. Can be <code>null</code>, in which case only the string representation of the
     *            subscript will be returned.
     * @param subscript The integer to write as subscript.
     * @return A {@link String}.
     *         <p>
     *         Examples if <b><code>variable</code></b> is "X":
     *         <ul>
     *         <li>If <b><code>subscript</code></b> is <code>-153</code>, it will return "X<sub>-153</sub>"</li>
     *         <li>If <b><code>subscript</code></b> is <code>0</code>, it will return "X<sub>0</sub>"</li>
     *         <li>If <b><code>subscript</code></b> is <code>1</code>, it will return "X<sub>1</sub>"</li>
     *         </p>
     */
    public static String subscriptToString(String variable, int subscript) {
        return new String(addSubscript(toCharArray(variable), subscript));
    }
}
