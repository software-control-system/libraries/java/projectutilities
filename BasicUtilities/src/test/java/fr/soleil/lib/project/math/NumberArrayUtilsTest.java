/*******************************************************************************
 * Copyright (c) 2008-2021 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.lib.project.math;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * This class tests {@link NumberArrayUtils#extractArray(Object, Class)} method, by filling an
 * array with numbers, converting and backward converting it, and comparing final result with
 * original value.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
@RunWith(Parameterized.class)
public class NumberArrayUtilsTest {

    private static final int TEST_COUNT = 200;
    private static final int SHAPE_MAX_LENGTH = 5;
    private static final int DIM_MAX_SIZE = 20;

    private final int[] shape;

    public NumberArrayUtilsTest(int[] shape) {
        this.shape = shape;
    }

    @Parameterized.Parameters
    public static List<Object[]> getParameters() {
        List<Object[]> result = new ArrayList<Object[]>();
        for (int i = 0; i < TEST_COUNT; i++) {
            int[] shape = new int[new Random().nextInt(SHAPE_MAX_LENGTH) + 1];
            for (int dimIndex = 0; dimIndex < shape.length; dimIndex++) {
                shape[dimIndex] = new Random().nextInt(DIM_MAX_SIZE + 1);
            }
            result.add(new Object[] { shape });
        }
        return result;
    }

    private static void initLongArray(Object array) {
        if (array instanceof long[]) {
            long[] lArray = (long[]) array;
            for (int i = 0; i < lArray.length; i++) {
                lArray[i] = new Random().nextInt();
            }
        }
    }

    @Test
    public void extractArrayTest() {
        if ((shape != null) && (shape.length > 0)) {
            Object array1 = Array.newInstance(Long.TYPE, shape);
            initLongArray(array1);
            Object array2 = NumberArrayUtils.extractArray(array1, Double.TYPE);
            Object array3 = NumberArrayUtils.extractArray(array2, Long.TYPE);
            assertThat("should be equal", true, equalTo(ArrayUtils.equals(array1, array3)));
        }
    }

}
