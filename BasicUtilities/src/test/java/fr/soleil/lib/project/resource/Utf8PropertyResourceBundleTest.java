/*******************************************************************************
 * Copyright (c) 2008-2021 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.lib.project.resource;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Locale;

import org.junit.Test;

/**
 * A class to test Utf8PropertyResourceBundle
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class Utf8PropertyResourceBundleTest {

    private static final String VALUE_TO_TEST = "££µµ€€∑∑éé";
    private static final String KEY_TO_TEST = "My.Value.To.Test";

    @Test
    public void getResourceTest() throws IOException {
        Utf8PropertyResourceBundle bundle = new Utf8PropertyResourceBundle(
                "fr.soleil.lib.project.resource.UTF8Resource", Locale.getDefault());
        assertThat(VALUE_TO_TEST, equalTo(bundle.getString(KEY_TO_TEST)));
    }

}
