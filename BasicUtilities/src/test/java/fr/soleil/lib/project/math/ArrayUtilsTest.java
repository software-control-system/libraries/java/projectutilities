/*******************************************************************************
 * Copyright (c) 2008-2021 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.lib.project.math;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * This class tests {@link ArrayUtils#convertArrayDimensionFrom1ToN(Object, Class, int...)} and
 * {@link ArrayUtils#convertArrayDimensionFromNTo1(Object, Class, int...)} methods, by filling an
 * array with numbers, converting and backward converting it, and comparing final result with
 * original value.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
@RunWith(Parameterized.class)
public class ArrayUtilsTest {

    private static final int TEST_COUNT = 200;
    private static final int SHAPE_MAX_LENGTH = 5;
    private static final int DIM_MAX_SIZE = 20;

    private final int[] shape;

    public ArrayUtilsTest(int[] shape) {
        this.shape = shape;
    }

    @Parameterized.Parameters
    public static List<Object[]> getParameters() {
        List<Object[]> result = new ArrayList<Object[]>();
        for (int i = 0; i < TEST_COUNT; i++) {
            int[] shape = new int[new Random().nextInt(SHAPE_MAX_LENGTH) + 1];
            for (int dimIndex = 0; dimIndex < shape.length; dimIndex++) {
                shape[dimIndex] = new Random().nextInt(DIM_MAX_SIZE + 1);
            }
            result.add(new Object[] { shape });
        }
        return result;
    }

    @Test
    public void intArrayConversionTest() {
        int length = 1;
        for (int curLength : shape) {
            length *= curLength;
        }
        int[] flatArray = new int[length];
        for (int i = 0; i < length; i++) {
            flatArray[i] = i;
        }
        int[] flatArray2 = flatArray.clone();
        Object multiDimArray = ArrayUtils.convertArrayDimensionFrom1ToN(flatArray2, shape);
        flatArray2 = (int[]) ArrayUtils.convertArrayDimensionFromNTo1(multiDimArray);
        assertThat(flatArray, equalTo(flatArray2));
    }

    @Test
    public void doubleArrayConversionTest() {
        int length = 1;
        for (int curLength : shape) {
            length *= curLength;
        }
        double[] flatArray = new double[length];
        for (int i = 0; i < length; i++) {
            flatArray[i] = new Random().nextDouble();
        }
        Object multiDimArray = ArrayUtils.convertArrayDimensionFrom1ToN(flatArray, shape);
        double[] flatArray2 = (double[]) ArrayUtils.convertArrayDimensionFromNTo1(multiDimArray);
        assertThat(flatArray, equalTo(flatArray2));
    }

    @Test
    public void equalTest() {
        int length = 1;
        for (int curLength : shape) {
            length *= curLength;
        }
        double[] flatArray = new double[length];
        for (int i = 0; i < length; i++) {
            flatArray[i] = new Random().nextDouble();
        }
        double[] flatArray2 = flatArray.clone();
        assertThat(true, equalTo(ArrayUtils.equals(flatArray, flatArray2)));
        Object multiDimArray = ArrayUtils.convertArrayDimensionFrom1ToN(flatArray, shape);
        Object multiDimArray2 = ArrayUtils.convertArrayDimensionFrom1ToN(flatArray2, shape);
        assertThat("should be equal", true, equalTo(ArrayUtils.equals(multiDimArray, multiDimArray2)));
        if (length > 0) {
            double[] flatArray3 = new double[length];
            double[] flatArray4 = new double[length];
            for (int i = 0; i < length; i++) {
                flatArray3[i] = i;
                flatArray4[i] = length - i;
            }
            assertThat("should not be equal", false, equalTo(ArrayUtils.equals(flatArray3, flatArray4)));
        }
    }

    @Test
    public void flatIndexTest() {
        boolean canTest = (shape.length > 0);
        for (int i = 0; i < shape.length; i++) {
            if (shape[i] <= 0) {
                canTest = false;
                break;
            }
        }
        if (canTest) {
            int length = 1;
            for (int curLength : shape) {
                length *= curLength;
            }
            int[] flatArray = new int[length];
            for (int i = 0; i < length; i++) {
                flatArray[i] = i;
            }
            Object multiDimArray = ArrayUtils.convertArrayDimensionFrom1ToN(flatArray, shape);
            // test 1st index
            int[] position = new int[shape.length];
            Object currentArray = multiDimArray;
            for (int i = 0; i < position.length - 1; i++) {
                currentArray = ((Object[]) currentArray)[position[i]];
            }
            assertThat("should be same value at start", flatArray[ArrayUtils.getFlatIndex(position, shape)],
                    equalTo(((int[]) currentArray)[position[position.length - 1]]));
            // test last index
            for (int i = 0; i < position.length; i++) {
                position[i] = shape[i] - 1;
            }
            currentArray = multiDimArray;
            for (int i = 0; i < position.length - 1; i++) {
                currentArray = ((Object[]) currentArray)[position[i]];
            }
            assertThat("should be same value at end", flatArray[ArrayUtils.getFlatIndex(position, shape)],
                    equalTo(((int[]) currentArray)[position[position.length - 1]]));
            // test middle index
            for (int i = 0; i < position.length; i++) {
                position[i] = shape[i] / 2;
            }
            currentArray = multiDimArray;
            for (int i = 0; i < position.length - 1; i++) {
                currentArray = ((Object[]) currentArray)[position[i]];
            }
            assertThat("should be same value at half", flatArray[ArrayUtils.getFlatIndex(position, shape)],
                    equalTo(((int[]) currentArray)[position[position.length - 1]]));
        } else {
            // fake test
            assertThat(true, equalTo(true));
        }
    }

    @Test
    public void copyTest() {
        int length = 1;
        for (int curLength : shape) {
            length *= curLength;
        }
        int[] flatArray = new int[length];
        for (int i = 0; i < length; i++) {
            flatArray[i] = i;
        }
        Object multiDimArray = ArrayUtils.convertArrayDimensionFrom1ToN(flatArray, shape);
        Object copy = ArrayUtils.getCopy(multiDimArray);
        Object copy2 = Array.newInstance(ArrayUtils.recoverDataType(multiDimArray), shape);
        ArrayUtils.copyArray(multiDimArray, copy2);
        assertThat("should be equivalent arrays for getCopy", true, equalTo(ArrayUtils.equals(multiDimArray, copy)));
        assertThat("should be equivalent arrays for copyArray", true, equalTo(ArrayUtils.equals(multiDimArray, copy2)));
        if (shape.length > 0) {
            assertThat("should not be the same reference", false, equalTo(multiDimArray == copy));
        }
    }

    @Test
    public void rankTest() {
        Object array = Array.newInstance(Integer.TYPE, shape);
        int length = shape.length;
        assertThat("Should be of rank " + length, length, equalTo(ArrayUtils.recoverArrayRank(array.getClass())));
    }
}
