/*******************************************************************************
 * Copyright (c) 2008-2021 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.lib.project.math;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * JUnit test class for {@link UnsignedConverter}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class UnsignedConverterTest {

    /**
     * Constructor
     */
    public UnsignedConverterTest() {
        super();
    }

    /**
     * Tests unsigned byte conversion
     */
    @Test
    public void unsignedByteConversionTest() {
        for (short value = -1; value > -1 * Byte.MAX_VALUE; value--) {
            assertEquals(UnsignedConverter.convertUnsignedByte((byte) value), Byte.MAX_VALUE - Byte.MIN_VALUE + value
                    + (short) 1);
        }
    }

    /**
     * Tests unsigned short conversion
     */
    @Test
    public void unsignedShortConversionTest() {
        for (int value = -1; value > -1 * Short.MAX_VALUE; value--) {
            assertEquals(UnsignedConverter.convertUnsignedShort((short) value), Short.MAX_VALUE - Short.MIN_VALUE
                    + value + 1);
        }
    }

    /**
     * Tests unsigned int conversion
     */
    @Test
    public void unsignedIntConversionTest() {
        for (long value = -1; value > -1L * Short.MAX_VALUE; value--) {
            assertEquals(UnsignedConverter.convertUnsignedInt((int) value), (long) Integer.MAX_VALUE
                    - (long) Integer.MIN_VALUE + value + 1L);
        }
        assertEquals(UnsignedConverter.convertUnsignedInt(Integer.MIN_VALUE), Integer.MAX_VALUE + 1L);
        assertEquals(UnsignedConverter.convertUnsignedInt(0), 0);
        assertEquals(UnsignedConverter.convertUnsignedInt(30), 30);
        assertEquals(UnsignedConverter.convertUnsignedInt(Integer.MAX_VALUE), Integer.MAX_VALUE);
    }

}
