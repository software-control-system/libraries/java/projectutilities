/*******************************************************************************
 * Copyright (c) 2008-2021 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.lib.project.file;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import java.io.File;

import org.junit.Test;

public class FileUtilsTest {

    private static final String FAKE_FILE_PATH = "." + File.separator + "MyFileToTest.TxT";

    @Test
    public void extensionTest() {
        File f = new File(FAKE_FILE_PATH);
        String path = f.getAbsolutePath();
        assertThat("test extension by path", "txt", equalTo(FileUtils.getExtension(path)));
        assertThat("test extension by file", "txt", equalTo(FileUtils.getExtension(f)));
    }

    @Test
    public void samePathTest() {
        File f = new File(FAKE_FILE_PATH);
        assertThat(true, equalTo(FileUtils.samePath(FAKE_FILE_PATH, f.getAbsolutePath())));
    }

    @Test
    public void directoryTest() {
        assertThat(true, equalTo(FileUtils.isDirectory(".")));
        assertThat(false, equalTo(FileUtils.isDirectory(FAKE_FILE_PATH)));
    }

}
