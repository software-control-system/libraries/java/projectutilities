/*******************************************************************************
 * Copyright (c) 2008-2021 SOLEIL Synchrotron
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.lib.project;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * JUnit test class for {@link ObjectUtils}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ObjectUtilsTest {

    @Test
    public void isNumberClassTest() {
        assertEquals("'Number' class should be considered as a number class", true,
                ObjectUtils.isNumberClass(Number.class));
        assertEquals("'Byte' class should be considered as a number class", true, ObjectUtils.isNumberClass(Byte.class));
        assertEquals("'Short' class should be considered as a number class", true,
                ObjectUtils.isNumberClass(Short.class));
        assertEquals("'Integer' class should be considered as a number class", true,
                ObjectUtils.isNumberClass(Integer.class));
        assertEquals("'Long' class should be considered as a number class", true, ObjectUtils.isNumberClass(Long.class));
        assertEquals("'Float' class should be considered as a number class", true,
                ObjectUtils.isNumberClass(Float.class));
        assertEquals("'Double' class should be considered as a number class", true,
                ObjectUtils.isNumberClass(Double.class));
        assertEquals("'byte' class should be considered as a number class", true, ObjectUtils.isNumberClass(byte.class));
        assertEquals("'short' class should be considered as a number class", true,
                ObjectUtils.isNumberClass(short.class));
        assertEquals("'int' class should be considered as a number class", true, ObjectUtils.isNumberClass(int.class));
        assertEquals("'long' class should be considered as a number class", true, ObjectUtils.isNumberClass(long.class));
        assertEquals("'float' class should be considered as a number class", true,
                ObjectUtils.isNumberClass(float.class));
        assertEquals("'double' class should be considered as a number class", true,
                ObjectUtils.isNumberClass(double.class));
    }

}
